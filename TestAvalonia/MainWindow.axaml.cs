using Avalonia.Controls;

namespace TestAvalonia
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent(); 
            if (this.VisualRoot != null)
            {
                this.VisualRoot.Renderer.DrawFps = true;
            }

        }
    }
}