﻿using Avalonia.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml;

namespace TestAvalonia
{
    public class BaseType
    {
        public const int EStyle2 = 1;
        public const int EStyle1 = 2;
    }
    public class Base
    {
        public string ext { get; set; }
        public int Type { get; set; }
        public int id;
    }
    public class EStyle2 : Base
    {
        [JsonInclude]
        public Pt fld;
        public int id;
    }
    public class EStyle1: Base
    {
        [JsonInclude]
        public Pt fld;
        public string Scope { get; set; }
        public string Name { get; set; }
    }
    public class Element
    {
        [JsonInclude]
        public Pt fld;
        public int id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int Age { get; set; }
        public double Size { get; set; }
        public EStyle1 Style { get; set; }
    }
    public struct Pt
    {
        [JsonInclude]
        public int x;
        [JsonInclude]
        public int y;
        public int z { get; set; }
    }
    public struct Id
    {
        public int Value { get; set; }
        public Id() { }
        public Id(int value)
        {
            this.Value = value;
        }
    }
    internal class Test
    {
      
        public static void Test1()
        {
            JsonSerializerOptions options = new()
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                ReferenceHandler = ReferenceHandler.Preserve,
                WriteIndented = true
            };
            var styles = new List<EStyle1>();
            for(var i = 0; i < 10; i++)
            {
                var style = new EStyle1 { Scope = "scope" +i.ToString()};
                styles.Add(style);
            }
            var eleList = new List<Element> ();
            var random = new Random ();
            for (var i = 0; i < 100; i++)
            {
                var style = styles[(int)random.NextDouble() * 1000];
                var pt = new Pt() { x = 2, y = 3 };
                var ele = new Element { Name = "N"+ i.ToString(), Style = style, Age = 3, Value = "#x", Size = 0 };
                ele.fld = pt;
                ele.fld.x = 50;
                ele.fld.y = 50;
                eleList.Add(ele);
            }
           
            var st = DateTime.Now;
            string json = JsonSerializer.Serialize(eleList, options);
            Debug.Print((DateTime.Now - st).TotalSeconds.ToString());
            st = DateTime.Now;
            eleList = JsonSerializer.Deserialize<List<Element>>(json, options);
            Debug.Print((DateTime.Now - st).TotalSeconds.ToString());
        }


        public static void Test2()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<t c=\"3\"><c/></t>");
            doc.NodeInserting += (s, e) => {
                Debug.Print(e.Node.GetType().ToString()+"-"+ e.Action.ToString());
            };
            doc.NodeInserted += (s, e) => {
                Debug.Print(e.Node.GetType().FullName + "-" + e.Action.ToString());
            };
    
            doc.NodeChanged += (s, e) => {
                Debug.Print(e.Node.GetType().FullName + "-" + e.Action.ToString());
            };
            doc.NodeChanging += (s, e) => {
                Debug.Print(e.Node.GetType().FullName + "-" + e.Action.ToString());
            };
            doc.NodeRemoving += (s, e) => {
                Debug.Print(e.Node.GetType().FullName + "-" + e.Action.ToString());
            };
            doc.NodeRemoved += (s, e) => {
                Debug.Print(e.Node.GetType().FullName + "-" + e.Action.ToString());
            };
            doc.DocumentElement.Attributes["c"].Value = "4";
            doc.DocumentElement.SetAttribute("c", "4");
        }

        public static void Test3()
        {
            var count = 100000000;
            var sum = 0;
            var st = DateTime.Now;
            Base estyle = new EStyle1 { Type = BaseType.EStyle1 };
            for (var i = 0; i < count; i++)
            {
                //对比1：is运算符 整数运算 效率基本一致
                //if(estyle is EStyle2) 
                //{
                //    sum++;
                //}
                //对比2：在Debuger模式下getset属性比Field慢四倍,在Release模式下一样
                estyle.Type++;
            }
            var dt1 = (DateTime.Now - st).TotalSeconds;
            st = DateTime.Now;
            sum = 0;
            for (var i = 0; i < count; i++)
            {
                //if(estyle.Type== BaseType.EStyle2)
                //{
                //    sum++;
                //}
                estyle.id++;
            }
            var dt2 = (DateTime.Now - st).TotalSeconds;
            var times = dt1 / dt2;
            Console.WriteLine(times.ToString());
        }

        public static void Test4()
        {
            Dictionary<int, int> dictInt=new Dictionary<int, int>();
            Dictionary<Id,int> dictStrut=new Dictionary<Id,int>();

            var count = 20000000;
            var sum = 0;
            var st = DateTime.Now;
            var dictValue = 0;
            for (var i = 0; i < count; i++)
            {
                dictInt.Add(i, i);
            }
            var dt1 = (DateTime.Now - st).TotalSeconds;
            st = DateTime.Now;
            sum = 0;
            for (var i = 0; i < count; i++)
            {
                dictStrut.Add(new Id(i), i);
            }
            var dt2 = (DateTime.Now - st).TotalSeconds;
            var times = dt1 / dt2;
            Console.WriteLine(times.ToString());
            //Debug下整数Key Add性能是结构的3倍
            //Release下整数Key Add性能是结构的2.2倍

            st = DateTime.Now;
            for (var i = 0; i < count; i++)
            {
                dictValue = dictInt[i];
            }
            var ids = dictStrut.Keys.ToArray();
            dt1 = (DateTime.Now - st).TotalSeconds;
            st = DateTime.Now;
            sum = 0;
            for (var i = 0; i < count; i++)
            {
                dictValue = dictStrut[ids[i]];
            }
            dt2 = (DateTime.Now - st).TotalSeconds;
            times = dt1 / dt2;
            dictValue++;
            Console.WriteLine(times.ToString());
            //Debug下整数Key 取值性能是结构的5倍
            //Release下整数Key 取值性能是结构的7倍

        }

        public static void Test5()
        {
            Dictionary<int, int> dictInt = new Dictionary<int, int>();
            List<int> listInt = new List<int>();

            var count = 20000000;
            var sum = 0;
            var st = DateTime.Now;
            var dictValue = 0;
            for (var i = 0; i < count; i++)
            {
                dictInt.Add(i, i);
            }
            var dt1 = (DateTime.Now - st).TotalSeconds;
            st = DateTime.Now;
            sum = 0;
            for (var i = 0; i < count; i++)
            {
                listInt.Add( i);
            }
            var dt2 = (DateTime.Now - st).TotalSeconds;
            var times = dt1 / dt2;
            Console.WriteLine(times.ToString());
            //Debug 整数Dict Add比list慢的3-4倍
            //Release下整数Key Add性能是结构的4-5倍

            st = DateTime.Now;
            for (var i = 0; i < count; i++)
            {
                dictValue = dictInt[i];
            }
            dt1 = (DateTime.Now - st).TotalSeconds;
            st = DateTime.Now;
            sum = 0;
            for (var i = 0; i < count; i++)
            {
                dictValue = listInt[i];
            }
            dt2 = (DateTime.Now - st).TotalSeconds;
            times = dt1 / dt2;
            dictValue++;
            Console.WriteLine(times.ToString());
            Console.WriteLine(dictValue.ToString());
            //Debug 整数Dict 取值比list慢的3-4倍
            //Release下整数Key 取值性能是结构的9-10倍
        }
    }
}
