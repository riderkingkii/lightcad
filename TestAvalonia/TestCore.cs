﻿using Avalonia.Controls;
using LightCAD.Core;
using LightCAD.Core.Elements;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml;

namespace TestAvalonia
{
    public class Ele
    {
        public string Style { get; set; }
    }
    public class Tst
    {
        public ElementCollection Elements { get; set; }
        public Tst() { 
            this.Elements = new ElementCollection();
        }
    }
    internal class TestCore
    {
        public static void TextStyleJsonConverter()
        {
            var ele = new Ele();
            ele.Style = "";
            var json = JsonSerializer.Serialize(ele);
            Console.WriteLine(json);
            ele = JsonSerializer.Deserialize<Ele>(json);
        }
        public static void DocumentSaveLoad()
        {
            var options = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                WriteIndented = true
            };
            //var tst = new Tst();
            //var circle = new Circle();
            //tst.Elements.Add(circle);
            //var tmp = JsonSerializer.Serialize(tst, options);
            //tst = JsonSerializer.Deserialize<Tst>(tmp, options);
            //return;

            //var doc = Document.Create(@"C:\temp\test.lcad");
            //Document.SetCurrent(doc);

            //doc.ModelSpace.IsFireChangedEvent = true;
            //var circle1=doc.ModelSpace.AddCircle( new Vector2d(100, 100), 100);
            //var defStyle = doc.StyleManager.GetDefault();
            //doc.StyleManager.SetStyle(circle1, defStyle);
            //var circle2 = doc.ModelSpace.AddCircle(new Vector2d(200, 200), 200);
            //doc.StyleManager.SetStyle(circle2, defStyle);
            //var circle3 = doc.ModelSpace.AddCircle(new Vector2d(300, 300), 300);
            //doc.StyleManager.SetStyle(circle3, defStyle.Clone( colorFrom: ValueFrom.BySelf,color: "#FF0000"));

            //var json = Document.ToJson(doc,false);
            //doc = Document.FromJson(json);
        }
    }
}
