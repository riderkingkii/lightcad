﻿using Avalonia;
using System;

namespace TestAvalonia
{
    internal class Program
    {
        //[STAThread]
        //public static void Main(string[] args)
        //{
        //    //Test.Test1();
        //    //Test.Test2();
        //    //Test.Test3();
        //    Test.Test4();
        //    //Test.Test5();

        //    //TestCore.TextStyleJsonConverter();
        //    //TestCore.DocumentSaveLoad();
        //}


        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        [STAThread]
        public static void Main(string[] args) => BuildAvaloniaApp()
            .StartWithClassicDesktopLifetime(args);

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
        {
            return AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToTrace();
        }
            
    }
}