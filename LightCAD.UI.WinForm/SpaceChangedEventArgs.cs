﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public class SpaceChangedEventArgs : EventArgs
    {
        public SpaceChangedEventArgs(bool isModel, int paperIndex)
        {
            this.IsModel = isModel;
            this.PaperIndex = paperIndex;
        }
        public bool IsModel { get; set; }
        public int PaperIndex { get; set; }
    }
}
