﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class ModelView3DControl : UserControl
    {
        public ModelView3DControl()
        {
            InitializeComponent();
            this.skGL.PaintSurface += SkGL_PaintSurface;
        }

        private void SkGL_PaintSurface(object? sender, SkiaSharp.Views.Desktop.SKPaintGLSurfaceEventArgs e)
        {
            var canvas = e.Surface.Canvas;
            var p = new SKPoint(skGL.Width / 2, skGL.Height / 2);
            using (var pen = new SKPaint { Color = SKColors.White })
            {
                canvas.DrawText("HELLO View3D", p, pen);
            }
        }
    }
}
