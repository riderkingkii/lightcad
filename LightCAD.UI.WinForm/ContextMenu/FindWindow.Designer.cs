﻿namespace LightCAD.UI.ContextMenu
{
    partial class FindWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new System.Windows.Forms.Label();
            cb_FindText = new System.Windows.Forms.ComboBox();
            cb_replaceText = new System.Windows.Forms.ComboBox();
            label2 = new System.Windows.Forms.Label();
            cb_Location = new System.Windows.Forms.ComboBox();
            label3 = new System.Windows.Forms.Label();
            lvFindResult = new System.Windows.Forms.ListView();
            ch_Location = new System.Windows.Forms.ColumnHeader();
            ch_ObjType = new System.Windows.Forms.ColumnHeader();
            ch_Text = new System.Windows.Forms.ColumnHeader();
            cbListResult = new System.Windows.Forms.CheckBox();
            btn_help = new System.Windows.Forms.Button();
            btn_finish = new System.Windows.Forms.Button();
            btn_find = new System.Windows.Forms.Button();
            btn_replaceAll = new System.Windows.Forms.Button();
            btn_replace = new System.Windows.Forms.Button();
            btnPanel = new System.Windows.Forms.Panel();
            btnPanel.SuspendLayout();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(15, 15);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(68, 17);
            label1.TabIndex = 0;
            label1.Text = "查找内容：";
            // 
            // cb_FindText
            // 
            cb_FindText.FormattingEnabled = true;
            cb_FindText.Location = new System.Drawing.Point(15, 40);
            cb_FindText.Name = "cb_FindText";
            cb_FindText.Size = new System.Drawing.Size(326, 25);
            cb_FindText.TabIndex = 1;
            // 
            // cb_replaceText
            // 
            cb_replaceText.FormattingEnabled = true;
            cb_replaceText.Location = new System.Drawing.Point(12, 99);
            cb_replaceText.Name = "cb_replaceText";
            cb_replaceText.Size = new System.Drawing.Size(326, 25);
            cb_replaceText.TabIndex = 3;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(12, 74);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(56, 17);
            label2.TabIndex = 2;
            label2.Text = "替换为：";
            // 
            // cb_Location
            // 
            cb_Location.FormattingEnabled = true;
            cb_Location.Location = new System.Drawing.Point(419, 40);
            cb_Location.Name = "cb_Location";
            cb_Location.Size = new System.Drawing.Size(161, 25);
            cb_Location.TabIndex = 5;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(419, 15);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(68, 17);
            label3.TabIndex = 4;
            label3.Text = "查找位置：";
            // 
            // lvFindResult
            // 
            lvFindResult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { ch_Location, ch_ObjType, ch_Text });
            lvFindResult.Location = new System.Drawing.Point(12, 159);
            lvFindResult.Name = "lvFindResult";
            lvFindResult.Size = new System.Drawing.Size(562, 67);
            lvFindResult.TabIndex = 7;
            lvFindResult.UseCompatibleStateImageBehavior = false;
            lvFindResult.View = System.Windows.Forms.View.Details;
            lvFindResult.Visible = false;
            // 
            // ch_Location
            // 
            ch_Location.Text = "位置";
            ch_Location.Width = 120;
            // 
            // ch_ObjType
            // 
            ch_ObjType.Text = "对象类型";
            ch_ObjType.Width = 120;
            // 
            // ch_Text
            // 
            ch_Text.Text = "文字";
            ch_Text.Width = 400;
            // 
            // cbListResult
            // 
            cbListResult.AutoSize = true;
            cbListResult.Location = new System.Drawing.Point(15, 132);
            cbListResult.Name = "cbListResult";
            cbListResult.Size = new System.Drawing.Size(75, 21);
            cbListResult.TabIndex = 6;
            cbListResult.Text = "列出结果";
            cbListResult.UseVisualStyleBackColor = true;
            // 
            // btn_help
            // 
            btn_help.AutoSize = true;
            btn_help.Location = new System.Drawing.Point(519, 5);
            btn_help.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_help.Name = "btn_help";
            btn_help.Size = new System.Drawing.Size(73, 27);
            btn_help.TabIndex = 9;
            btn_help.Text = "帮助";
            btn_help.UseVisualStyleBackColor = true;
            // 
            // btn_finish
            // 
            btn_finish.AutoSize = true;
            btn_finish.Location = new System.Drawing.Point(419, 5);
            btn_finish.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_finish.Name = "btn_finish";
            btn_finish.Size = new System.Drawing.Size(73, 27);
            btn_finish.TabIndex = 10;
            btn_finish.Text = "完成";
            btn_finish.UseVisualStyleBackColor = true;
            // 
            // btn_find
            // 
            btn_find.AutoSize = true;
            btn_find.Location = new System.Drawing.Point(319, 5);
            btn_find.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_find.Name = "btn_find";
            btn_find.Size = new System.Drawing.Size(73, 27);
            btn_find.TabIndex = 11;
            btn_find.Text = "查找";
            btn_find.UseVisualStyleBackColor = true;
            // 
            // btn_replaceAll
            // 
            btn_replaceAll.AutoSize = true;
            btn_replaceAll.Location = new System.Drawing.Point(219, 5);
            btn_replaceAll.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_replaceAll.Name = "btn_replaceAll";
            btn_replaceAll.Size = new System.Drawing.Size(73, 27);
            btn_replaceAll.TabIndex = 12;
            btn_replaceAll.Text = "全部替换";
            btn_replaceAll.UseVisualStyleBackColor = true;
            // 
            // btn_replace
            // 
            btn_replace.AutoSize = true;
            btn_replace.Location = new System.Drawing.Point(119, 5);
            btn_replace.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_replace.Name = "btn_replace";
            btn_replace.Size = new System.Drawing.Size(73, 27);
            btn_replace.TabIndex = 13;
            btn_replace.Text = "替换";
            btn_replace.UseVisualStyleBackColor = true;
            // 
            // btnPanel
            // 
            btnPanel.AutoSize = true;
            btnPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnPanel.Controls.Add(btn_help);
            btnPanel.Controls.Add(btn_replace);
            btnPanel.Controls.Add(btn_finish);
            btnPanel.Controls.Add(btn_replaceAll);
            btnPanel.Controls.Add(btn_find);
            btnPanel.Location = new System.Drawing.Point(-2, 232);
            btnPanel.Name = "btnPanel";
            btnPanel.Size = new System.Drawing.Size(594, 35);
            btnPanel.TabIndex = 14;
            // 
            // FindWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            AutoSize = true;
            AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            ClientSize = new System.Drawing.Size(604, 277);
            Controls.Add(btnPanel);
            Controls.Add(lvFindResult);
            Controls.Add(cbListResult);
            Controls.Add(cb_Location);
            Controls.Add(label3);
            Controls.Add(cb_replaceText);
            Controls.Add(label2);
            Controls.Add(cb_FindText);
            Controls.Add(label1);
            Name = "FindWindow";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "查找和替换";
            btnPanel.ResumeLayout(false);
            btnPanel.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_FindText;
        private System.Windows.Forms.ComboBox cb_replaceText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_Location;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvFindResult;
        private System.Windows.Forms.CheckBox cbListResult;
        private System.Windows.Forms.ColumnHeader ch_Location;
        private System.Windows.Forms.ColumnHeader ch_ObjType;
        private System.Windows.Forms.ColumnHeader ch_Text;
        private System.Windows.Forms.Button btn_help;
        private System.Windows.Forms.Button btn_finish;
        private System.Windows.Forms.Button btn_find;
        private System.Windows.Forms.Button btn_replaceAll;
        private System.Windows.Forms.Button btn_replace;
        private System.Windows.Forms.Panel btnPanel;
    }
}