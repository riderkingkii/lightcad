﻿using LightCAD.Core;
using LightCAD.Drawing.Actions;
using LightCAD.Drawing.Actions.Action;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace LightCAD.UI.ContextMenu
{
    public partial class OperatorValueControl : UserControl
    {
        LcDocument lcDocument;

        private DropDownControl operatorValueDropDownControl;
        private TextBox operatorValueTextBox;
        private ComboBox operatorValueComboBox;

        private string selectOperatorValue = "";
        public string SelectOperatorValue { get { return selectOperatorValue; } }

        public OperatorValueControl(LcDocument lcDocument)
        {
            this.lcDocument = lcDocument;

            InitializeComponent();
        }

        public void ShowComponent(string feature)
        {
            FeatureEnum featureEnum = EnumExtensions.GetEnumValueFromDescription<FeatureEnum>(feature);
            FeatureValueEnum featureValueEnum = EnumExtensions.GetEnumValueFromDescription<FeatureValueEnum>(feature);
            operatorValueTextBox ??= new TextBox();
            operatorValueComboBox ??= new ComboBox();

            operatorValueTextBox.TextChanged += operatorValueTextBox_TextChanged;
            operatorValueComboBox.Items.Clear();
            operatorValueComboBox.SelectedIndexChanged += operatorValueComboBox_SelectedIndexChanged;

            operatorValueTextBox.Size = Size;
            operatorValueComboBox.Size = Size;

            operatorValueTextBox.Hide();
            operatorValueComboBox.Hide();
            if (operatorValueDropDownControl != null)
            {
                operatorValueDropDownControl.Hide();
            }

            Controls.Add(operatorValueTextBox);
            Controls.Add(operatorValueComboBox);

            switch (featureEnum)
            {
                case FeatureEnum.ColorDropDown:
                    operatorValueDropDownControl = operatorValueDropDownControl is ColorDropDown ? operatorValueDropDownControl : new ColorDropDown(lcDocument);
                    var colorDropDownControl = (ColorDropDown)operatorValueDropDownControl;
                    // 使用已有的颜色选择控件
                    InitDropDownControl(operatorValueDropDownControl);
                    colorDropDownControl.InitColors(lcDocument);
                    if (colorDropDownControl.SelectItem == "")
                    {
                        selectOperatorValue = ValueFrom.ByLayer;
                    }
                    else
                    {
                        selectOperatorValue = colorDropDownControl.SelectItem;
                    }
                    break;
                case FeatureEnum.ComboBox:
                    if (FeatureValueEnum.Layer == featureValueEnum)
                    {
                        InitLayer();
                    }
                    else if (FeatureValueEnum.PrintStyle == featureValueEnum)
                    {
                        InitPrint();
                    }
                    else if (FeatureValueEnum.Material == featureValueEnum)
                    {
                        InitMaterial();
                    }
                    operatorValueComboBox.Show();
                    break;
                case FeatureEnum.LineTypeDropDown:
                    operatorValueDropDownControl = operatorValueDropDownControl is LineTypeDropDown ? operatorValueDropDownControl : new LineTypeDropDown(lcDocument);
                    var lineTypeDropDownControl = (LineTypeDropDown)operatorValueDropDownControl;
                    // 使用已有的线型选择控件
                    InitDropDownControl(lineTypeDropDownControl);
                    lineTypeDropDownControl.InitLineType(lcDocument);
                    selectOperatorValue = lineTypeDropDownControl.SelectItem;
                    break;
                case FeatureEnum.LineWidthDropDown:
                    operatorValueDropDownControl = operatorValueDropDownControl is LineWidthDropDown ? operatorValueDropDownControl : new LineWidthDropDown(lcDocument);
                    var lineWidthDropDownControl = (LineWidthDropDown)operatorValueDropDownControl;
                    // 使用已有的线型选择控件
                    InitDropDownControl(lineWidthDropDownControl);
                    lineWidthDropDownControl.InitLineWidth(lcDocument);
                    selectOperatorValue = lineWidthDropDownControl.SelectItem;
                    break;
                default:
                    operatorValueTextBox.Show();
                    break;
            }
        }

        private void InitDropDownControl<T>(T dropDownControl) where T : DropDownControl
        {
            dropDownControl.AnchorSize = Size;
            dropDownControl.BackColor = Color.White;
            dropDownControl.DockSide = DropDownControl.eDockSide.Left;
            dropDownControl.ForeColor = Color.Black;
            dropDownControl.Margin = new Padding(3, 3, 3, 3);
            dropDownControl.Size = Size;
            dropDownControl.TabIndex = 1;
            dropDownControl.OnItemClick += OnDropDownControlItemClick;
            dropDownControl.Show();
            Controls.Add(dropDownControl);
        }

        private void InitLayer()
        {
            foreach (var layer in lcDocument.Layers)
            {
                operatorValueComboBox.Items.Add(layer.Name);
            }
            operatorValueComboBox.SelectedIndex = 0;
            selectOperatorValue = operatorValueComboBox.SelectedItem.ToString();
        }

        private void InitPrint()
        {
            operatorValueComboBox.Items.AddRange(new string[] { "ByLayer", "ByBlock", "普通", "Normal" });
            operatorValueComboBox.SelectedIndex = 0;
            selectOperatorValue = operatorValueComboBox.SelectedItem.ToString();
        }

        private void InitMaterial()
        {
            operatorValueComboBox.Items.AddRange(new string[] { "ByBlock", "ByLayer", "全局" });
            operatorValueComboBox.SelectedIndex = 0;
            selectOperatorValue = operatorValueComboBox.SelectedItem.ToString();
        }

        private void operatorValueComboBox_SelectedIndexChanged(object? sender, EventArgs e)
        {
            selectOperatorValue = operatorValueComboBox.SelectedItem.ToString();
        }

        private void OnDropDownControlItemClick(string item)
        {
            selectOperatorValue = item;
        }

        private void operatorValueTextBox_TextChanged(object? sender, EventArgs e)
        {
            selectOperatorValue = operatorValueTextBox.Text;
        }
    }
}
