﻿using LightCAD.Drawing.Actions;
using LightCAD.Drawing.Actions.Action;
using LightCAD.Runtime.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace LightCAD.UI.ContextMenu
{
    public partial class QuickSelectWindow : FormBase, IQuickSelectWindow
    {
        public Type WinType => this.GetType();
        public string CurrentAction { get; set; }
        public bool IsActive => true;

        public QuickSelectAction quickSelectAction;

        private OperatorValueControl operatorValueControl;


        public QuickSelectWindow()
        {
            InitializeComponent();
        }

        public QuickSelectWindow(params object[] args) : this()
        {
            if (args != null && args.Length > 0)
            {
                quickSelectAction = (QuickSelectAction)args[0];
                CurrentAction = "OK";

                if (quickSelectAction.elementCount == 0)
                {
                    Close();
                    MessageBox.Show("当前的图形中没有任何对象。", "快速选择 - 未找到对象", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // 初始化快速选择项
                initCmbApply(quickSelectAction.isSelectedElement);
                initCmbObjType(quickSelectAction.elementTypes.ToList());
                initLboxFeature(quickSelectAction.GetFeatures());
                initCmbOperator();
                initCmbOperatorValue();

                radioInclude.Checked = true;
            }
        }

        private void confirmbtn_Click(object sender, System.EventArgs e)
        {
            if (quickSelectAction != null)
            {
                quickSelectAction.selectApply = cmbApply.SelectedItem?.ToString();
                quickSelectAction.selectType = cmbObjType.SelectedItem?.ToString();
                quickSelectAction.selectFeature = lboxFeature.SelectedItem?.ToString();
                quickSelectAction.selectOperator = cmbOperator.SelectedItem?.ToString();
                quickSelectAction.selectOperatorValue = operatorValueControl.SelectOperatorValue;
                quickSelectAction.selectApplyNew = radioInclude.Checked;
                quickSelectAction.selectAdditional = cboxAdditional.Checked;
            }

            DialogResult = DialogResult.OK;
        }

        private void cancelbtn_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void helpbtn_Click(object sender, System.EventArgs e)
        {

        }

        private void initCmbApply(bool current = false)
        {
            cmbApply.SelectedIndexChanged += CmbApply_SelectedIndexChanged;

            cmbApply.Items.Clear();
            cmbApply.Items.Add(ApplyRangeEnum.All.GetDescription());
            if (current)
            {
                cmbApply.Items.Add(ApplyRangeEnum.Selected.GetDescription());
            }
            cmbApply.SelectedIndex = cmbApply.Items.Count - 1;
        }

        private void CmbApply_SelectedIndexChanged(object? sender, EventArgs e)
        {
            if (cmbApply.SelectedItem != null)
            {
                if (cmbApply.SelectedItem?.ToString() == ApplyRangeEnum.All.GetDescription())
                {
                    initCmbObjType(quickSelectAction.AllElementTypes.ToList());
                }
                else
                {
                    initCmbObjType(quickSelectAction.elementTypes.ToList());
                }
            }
        }

        private void initCmbObjType(List<string> items)
        {
            if (items.Count != 1)
            {
                items.Insert(0, AllTypeEnum.All.GetDescription());
            }

            cmbObjType.DataSource = items.Distinct().ToList();
            cmbObjType.SelectedIndexChanged += cmbObjType_SelectedIndexChanged;
        }

        private void cmbObjType_SelectedIndexChanged(object? sender, EventArgs e)
        {
            if (cmbObjType.SelectedItem.ToString() == AllTypeEnum.All.GetDescription())
            {
                lboxFeature.DataSource = quickSelectAction.GetFeatures();
            }
            else
            {
                lboxFeature.DataSource = quickSelectAction.GetFeatures(cmbObjType.SelectedItem.ToString());
            }
        }

        private void initLboxFeature(List<string> features)
        {
            lboxFeature.DataSource = features;
            lboxFeature.SelectedIndexChanged += lboxFeature_SelectedIndexChanged;
        }

        private void lboxFeature_SelectedIndexChanged(object? sender, EventArgs e)
        {
            // 获取所选项的值
            if (lboxFeature.SelectedIndex != -1)
            {
                quickSelectAction.selectFeature = lboxFeature.SelectedItem.ToString();
                operatorValueControl.ShowComponent(quickSelectAction.selectFeature);
            }
        }

        private void initCmbOperator()
        {
            Dictionary<OperatorEnum, string> operatorEnum = EnumExtensions.GetAllDescriptions<OperatorEnum>();
            cmbOperator.DataSource = operatorEnum.Values.ToList();
        }

        private void initCmbOperatorValue()
        {
            operatorValueControl = new OperatorValueControl(quickSelectAction.lcDocument);
            operatorValueControl.Margin = new Padding(3, 3, 3, 3);
            operatorValueControl.Size = new Size(170, 25);
            operatorValueControl.Location = new Point(112, 253);
            operatorValueControl.ShowComponent("");
            Controls.Add(operatorValueControl);
        }
    }
}
