﻿using LightCAD.Drawing.Actions.Action;
using LightCAD.Runtime.Interface;
using System;
using System.Drawing;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace LightCAD.UI.ContextMenu
{
    public partial class FindWindow : FormBase, IFindWindow
    {
        public Type WinType => this.GetType();
        public string CurrentAction { get; set; }
        public bool IsActive => true;

        public FindAction findAction;

        public FindWindow()
        {
            InitializeComponent();
        }

        public FindWindow(params object[] args) : this()
        {
            if (args != null && args.Length > 0)
            {
                findAction = (FindAction)args[0];
                CurrentAction = "OK";

                this.Height -= lvFindResult.Height;
                btnPanel.Location = new Point(btnPanel.Location.X, btnPanel.Location.Y - lvFindResult.Height);
                cbListResult.CheckedChanged += cbListResult_CheckedChanged;
            }
        }

        private void cbListResult_CheckedChanged(object sender, EventArgs e)
        {
            lvFindResult.Visible = cbListResult.Checked;

            if (cbListResult.Checked)
            {
                this.Height += lvFindResult.Height;
                btnPanel.Location = new Point(btnPanel.Location.X, btnPanel.Location.Y + lvFindResult.Height);
            }
            else
            {
                this.Height -= lvFindResult.Height;
                btnPanel.Location = new Point(btnPanel.Location.X, btnPanel.Location.Y - lvFindResult.Height);
            }
        }
    }
}
