﻿namespace LightCAD.UI.ContextMenu
{
    partial class QuickSelectWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new System.Windows.Forms.Label();
            cmbApply = new System.Windows.Forms.ComboBox();
            cmbObjType = new System.Windows.Forms.ComboBox();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            lboxFeature = new System.Windows.Forms.ListBox();
            cmbOperator = new System.Windows.Forms.ComboBox();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            groupBox1 = new System.Windows.Forms.GroupBox();
            radioExclude = new System.Windows.Forms.RadioButton();
            radioInclude = new System.Windows.Forms.RadioButton();
            cboxAdditional = new System.Windows.Forms.CheckBox();
            helpbtn = new System.Windows.Forms.Button();
            btn_cancel = new System.Windows.Forms.Button();
            btn_confirm = new System.Windows.Forms.Button();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // label1
            // 
            label1.Location = new System.Drawing.Point(36, 27);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(70, 18);
            label1.TabIndex = 0;
            label1.Text = "应用到：";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbApply
            // 
            cmbApply.FormattingEnabled = true;
            cmbApply.Location = new System.Drawing.Point(112, 25);
            cmbApply.Name = "cmbApply";
            cmbApply.Size = new System.Drawing.Size(170, 25);
            cmbApply.TabIndex = 1;
            // 
            // cmbObjType
            // 
            cmbObjType.FormattingEnabled = true;
            cmbObjType.Location = new System.Drawing.Point(112, 65);
            cmbObjType.Name = "cmbObjType";
            cmbObjType.Size = new System.Drawing.Size(170, 25);
            cmbObjType.TabIndex = 3;
            // 
            // label2
            // 
            label2.Location = new System.Drawing.Point(36, 67);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(70, 18);
            label2.TabIndex = 2;
            label2.Text = "对象类型：";
            label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            label3.Location = new System.Drawing.Point(36, 107);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(70, 18);
            label3.TabIndex = 4;
            label3.Text = "特性：";
            label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lboxFeature
            // 
            lboxFeature.FormattingEnabled = true;
            lboxFeature.ItemHeight = 17;
            lboxFeature.Location = new System.Drawing.Point(112, 107);
            lboxFeature.Name = "lboxFeature";
            lboxFeature.Size = new System.Drawing.Size(170, 89);
            lboxFeature.TabIndex = 5;
            // 
            // cmbOperator
            // 
            cmbOperator.FormattingEnabled = true;
            cmbOperator.Location = new System.Drawing.Point(112, 213);
            cmbOperator.Name = "cmbOperator";
            cmbOperator.Size = new System.Drawing.Size(170, 25);
            cmbOperator.TabIndex = 7;
            // 
            // label4
            // 
            label4.Location = new System.Drawing.Point(36, 215);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(70, 18);
            label4.TabIndex = 6;
            label4.Text = "运算符：";
            label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            label5.Location = new System.Drawing.Point(36, 255);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(70, 18);
            label5.TabIndex = 8;
            label5.Text = "值：";
            label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(radioExclude);
            groupBox1.Controls.Add(radioInclude);
            groupBox1.Location = new System.Drawing.Point(10, 284);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(312, 71);
            groupBox1.TabIndex = 10;
            groupBox1.TabStop = false;
            groupBox1.Text = "如何应用：";
            // 
            // radioExclude
            // 
            radioExclude.AutoSize = true;
            radioExclude.Location = new System.Drawing.Point(26, 44);
            radioExclude.Name = "radioExclude";
            radioExclude.Size = new System.Drawing.Size(134, 21);
            radioExclude.TabIndex = 1;
            radioExclude.TabStop = true;
            radioExclude.Text = "排除在新选择集之外";
            radioExclude.UseVisualStyleBackColor = true;
            // 
            // radioInclude
            // 
            radioInclude.AutoSize = true;
            radioInclude.Location = new System.Drawing.Point(26, 22);
            radioInclude.Name = "radioInclude";
            radioInclude.Size = new System.Drawing.Size(122, 21);
            radioInclude.TabIndex = 0;
            radioInclude.TabStop = true;
            radioInclude.Text = "包括在新选择集中";
            radioInclude.UseVisualStyleBackColor = true;
            // 
            // cboxAdditional
            // 
            cboxAdditional.AutoSize = true;
            cboxAdditional.Location = new System.Drawing.Point(17, 361);
            cboxAdditional.Name = "cboxAdditional";
            cboxAdditional.Size = new System.Drawing.Size(123, 21);
            cboxAdditional.TabIndex = 11;
            cboxAdditional.Text = "附加到当前选择集";
            cboxAdditional.UseVisualStyleBackColor = true;
            // 
            // helpbtn
            // 
            helpbtn.Location = new System.Drawing.Point(247, 388);
            helpbtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            helpbtn.Name = "helpbtn";
            helpbtn.Size = new System.Drawing.Size(73, 25);
            helpbtn.TabIndex = 14;
            helpbtn.Text = "帮助(H)";
            helpbtn.UseVisualStyleBackColor = true;
            helpbtn.Click += helpbtn_Click;
            // 
            // btn_cancel
            // 
            btn_cancel.Location = new System.Drawing.Point(132, 388);
            btn_cancel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_cancel.Name = "btn_cancel";
            btn_cancel.Size = new System.Drawing.Size(73, 25);
            btn_cancel.TabIndex = 13;
            btn_cancel.Text = "取消";
            btn_cancel.UseVisualStyleBackColor = true;
            btn_cancel.Click += cancelbtn_Click;
            // 
            // btn_confirm
            // 
            btn_confirm.Location = new System.Drawing.Point(15, 388);
            btn_confirm.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_confirm.Name = "btn_confirm";
            btn_confirm.Size = new System.Drawing.Size(73, 25);
            btn_confirm.TabIndex = 12;
            btn_confirm.Text = "确认";
            btn_confirm.UseVisualStyleBackColor = true;
            btn_confirm.Click += confirmbtn_Click;
            // 
            // QuickSelectWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(334, 421);
            Controls.Add(helpbtn);
            Controls.Add(btn_cancel);
            Controls.Add(btn_confirm);
            Controls.Add(cboxAdditional);
            Controls.Add(groupBox1);
            Controls.Add(label5);
            Controls.Add(cmbOperator);
            Controls.Add(label4);
            Controls.Add(lboxFeature);
            Controls.Add(label3);
            Controls.Add(cmbObjType);
            Controls.Add(label2);
            Controls.Add(cmbApply);
            Controls.Add(label1);
            Name = "QuickSelectWindow";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "快速选择";
            TopMost = true;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbApply;
        private System.Windows.Forms.ComboBox cmbObjType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lboxFeature;
        private System.Windows.Forms.ComboBox cmbOperator;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button helpbtn;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_confirm;
        private System.Windows.Forms.RadioButton radioExclude;
        private System.Windows.Forms.RadioButton radioInclude;
        private System.Windows.Forms.CheckBox cboxAdditional;
    }
}