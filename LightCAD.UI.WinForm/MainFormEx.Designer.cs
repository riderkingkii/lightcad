﻿namespace LightCAD.UI
{
    partial class MainFormEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFormEx));
            statusBar = new System.Windows.Forms.StatusStrip();
            imageList = new System.Windows.Forms.ImageList(components);
            contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(components);
            ssddfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            sdfasdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            vS2015BlueTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015BlueTheme();
            vS2015LightTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015LightTheme();
            vS2015DarkTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015DarkTheme();
            vsToolStripExtender1 = new WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender(components);
            pnlTabMenu = new System.Windows.Forms.Panel();
            TabMenu = new TabMenuControl();
            titleBar = new System.Windows.Forms.Panel();
            label1 = new System.Windows.Forms.Label();
            lbDrop = new System.Windows.Forms.Label();
            TitleTab = new TitleTabControl();
            pnlLogoButton = new System.Windows.Forms.Panel();
            logoMenuControl = new System.Windows.Forms.MenuStrip();
            LogoMenu = new System.Windows.Forms.ToolStripMenuItem();
            lbClose = new System.Windows.Forms.Label();
            lbMax = new System.Windows.Forms.Label();
            lbMin = new System.Windows.Forms.Label();
            ToolTips = new System.Windows.Forms.ToolTip(components);
            lbDrop_Menu = new System.Windows.Forms.ContextMenuStrip(components);
            下拉菜单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            contextMenuStrip1.SuspendLayout();
            pnlTabMenu.SuspendLayout();
            titleBar.SuspendLayout();
            pnlLogoButton.SuspendLayout();
            logoMenuControl.SuspendLayout();
            lbDrop_Menu.SuspendLayout();
            SuspendLayout();
            // 
            // statusBar
            // 
            statusBar.BackColor = System.Drawing.Color.Black;
            statusBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            statusBar.Location = new System.Drawing.Point(1, 697);
            statusBar.Name = "statusBar";
            statusBar.Size = new System.Drawing.Size(1278, 22);
            statusBar.SizingGrip = false;
            statusBar.TabIndex = 4;
            // 
            // imageList
            // 
            imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            imageList.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("imageList.ImageStream");
            imageList.TransparentColor = System.Drawing.Color.Transparent;
            imageList.Images.SetKeyName(0, "");
            imageList.Images.SetKeyName(1, "");
            imageList.Images.SetKeyName(2, "");
            imageList.Images.SetKeyName(3, "");
            imageList.Images.SetKeyName(4, "");
            imageList.Images.SetKeyName(5, "");
            imageList.Images.SetKeyName(6, "");
            imageList.Images.SetKeyName(7, "");
            imageList.Images.SetKeyName(8, "");
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { ssddfToolStripMenuItem, sdfasdfToolStripMenuItem });
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new System.Drawing.Size(131, 52);
            // 
            // ssddfToolStripMenuItem
            // 
            ssddfToolStripMenuItem.Name = "ssddfToolStripMenuItem";
            ssddfToolStripMenuItem.Size = new System.Drawing.Size(130, 24);
            ssddfToolStripMenuItem.Text = "ssddf";
            // 
            // sdfasdfToolStripMenuItem
            // 
            sdfasdfToolStripMenuItem.Name = "sdfasdfToolStripMenuItem";
            sdfasdfToolStripMenuItem.Size = new System.Drawing.Size(130, 24);
            sdfasdfToolStripMenuItem.Text = "sdfasdf";
            // 
            // dockPanel
            // 
            dockPanel.BackColor = System.Drawing.SystemColors.Control;
            dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            dockPanel.DockBackColor = System.Drawing.Color.FromArgb(41, 57, 85);
            dockPanel.DockBottomPortion = 150D;
            dockPanel.DockLeftPortion = 200D;
            dockPanel.DockRightPortion = 200D;
            dockPanel.DockTopPortion = 150D;
            dockPanel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            dockPanel.Location = new System.Drawing.Point(1, 120);
            dockPanel.Name = "dockPanel";
            dockPanel.Padding = new System.Windows.Forms.Padding(6);
            dockPanel.RightToLeftLayout = true;
            dockPanel.ShowAutoHideContentOnHover = false;
            dockPanel.Size = new System.Drawing.Size(1278, 577);
            dockPanel.TabIndex = 0;
            dockPanel.Theme = vS2015BlueTheme1;
            // 
            // vsToolStripExtender1
            // 
            vsToolStripExtender1.DefaultRenderer = null;
            // 
            // pnlTabMenu
            // 
            pnlTabMenu.AutoSize = true;
            pnlTabMenu.Controls.Add(TabMenu);
            pnlTabMenu.Dock = System.Windows.Forms.DockStyle.Top;
            pnlTabMenu.Location = new System.Drawing.Point(1, 36);
            pnlTabMenu.Name = "pnlTabMenu";
            pnlTabMenu.Size = new System.Drawing.Size(1278, 84);
            pnlTabMenu.TabIndex = 11;
            // 
            // TabMenu
            // 
            TabMenu.AutoSize = true;
            TabMenu.BackColor = System.Drawing.Color.Transparent;
            TabMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            TabMenu.Location = new System.Drawing.Point(0, 0);
            TabMenu.Margin = new System.Windows.Forms.Padding(0);
            TabMenu.Name = "TabMenu";
            TabMenu.Size = new System.Drawing.Size(1278, 84);
            TabMenu.TabIndex = 28;
            TabMenu.TabMgr = null;
            // 
            // titleBar
            // 
            titleBar.BackColor = System.Drawing.Color.FromArgb(222, 225, 230);
            titleBar.Controls.Add(label1);
            titleBar.Controls.Add(lbDrop);
            titleBar.Controls.Add(TitleTab);
            titleBar.Controls.Add(pnlLogoButton);
            titleBar.Controls.Add(lbClose);
            titleBar.Controls.Add(lbMax);
            titleBar.Controls.Add(lbMin);
            titleBar.Dock = System.Windows.Forms.DockStyle.Top;
            titleBar.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            titleBar.Location = new System.Drawing.Point(1, 1);
            titleBar.Margin = new System.Windows.Forms.Padding(0);
            titleBar.Name = "titleBar";
            titleBar.Padding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            titleBar.Size = new System.Drawing.Size(1278, 35);
            titleBar.TabIndex = 12;
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            label1.BackColor = System.Drawing.Color.Transparent;
            label1.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label1.Image = Properties.Resources.User;
            label1.Location = new System.Drawing.Point(1032, 2);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(48, 30);
            label1.TabIndex = 28;
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDrop
            // 
            lbDrop.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            lbDrop.BackColor = System.Drawing.Color.Transparent;
            lbDrop.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            lbDrop.Location = new System.Drawing.Point(1080, 2);
            lbDrop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbDrop.Name = "lbDrop";
            lbDrop.Size = new System.Drawing.Size(48, 30);
            lbDrop.TabIndex = 27;
            lbDrop.Text = "▽";
            lbDrop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TitleTab
            // 
            TitleTab.AutoSize = true;
            TitleTab.Dock = System.Windows.Forms.DockStyle.Left;
            TitleTab.Location = new System.Drawing.Point(40, 1);
            TitleTab.Name = "TitleTab";
            TitleTab.Size = new System.Drawing.Size(34, 34);
            TitleTab.TabIndex = 26;
            // 
            // pnlLogoButton
            // 
            pnlLogoButton.Controls.Add(logoMenuControl);
            pnlLogoButton.Dock = System.Windows.Forms.DockStyle.Left;
            pnlLogoButton.Location = new System.Drawing.Point(0, 1);
            pnlLogoButton.Margin = new System.Windows.Forms.Padding(0);
            pnlLogoButton.Name = "pnlLogoButton";
            pnlLogoButton.Size = new System.Drawing.Size(40, 34);
            pnlLogoButton.TabIndex = 25;
            // 
            // logoMenuControl
            // 
            logoMenuControl.GripMargin = new System.Windows.Forms.Padding(0);
            logoMenuControl.ImageScalingSize = new System.Drawing.Size(32, 32);
            logoMenuControl.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { LogoMenu });
            logoMenuControl.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            logoMenuControl.Location = new System.Drawing.Point(0, 0);
            logoMenuControl.Name = "logoMenuControl";
            logoMenuControl.Padding = new System.Windows.Forms.Padding(0);
            logoMenuControl.Size = new System.Drawing.Size(40, 34);
            logoMenuControl.TabIndex = 0;
            // 
            // LogoMenu
            // 
            LogoMenu.AutoSize = false;
            LogoMenu.Image = Properties.Resources.LcadLogo32;
            LogoMenu.Name = "LogoMenu";
            LogoMenu.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            LogoMenu.Size = new System.Drawing.Size(40, 34);
            // 
            // lbClose
            // 
            lbClose.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            lbClose.BackColor = System.Drawing.Color.Transparent;
            lbClose.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            lbClose.Location = new System.Drawing.Point(1224, 1);
            lbClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbClose.Name = "lbClose";
            lbClose.Size = new System.Drawing.Size(48, 30);
            lbClose.TabIndex = 24;
            lbClose.Text = "✕";
            lbClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMax
            // 
            lbMax.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            lbMax.BackColor = System.Drawing.Color.Transparent;
            lbMax.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            lbMax.Location = new System.Drawing.Point(1176, 1);
            lbMax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbMax.Name = "lbMax";
            lbMax.Size = new System.Drawing.Size(48, 30);
            lbMax.TabIndex = 23;
            lbMax.Text = "「」";
            lbMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMin
            // 
            lbMin.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            lbMin.BackColor = System.Drawing.Color.Transparent;
            lbMin.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            lbMin.Location = new System.Drawing.Point(1128, 1);
            lbMin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbMin.Name = "lbMin";
            lbMin.Size = new System.Drawing.Size(48, 30);
            lbMin.TabIndex = 22;
            lbMin.Text = "  一";
            lbMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDrop_Menu
            // 
            lbDrop_Menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            lbDrop_Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { 下拉菜单ToolStripMenuItem });
            lbDrop_Menu.Name = "lbDrop_Menu";
            lbDrop_Menu.Size = new System.Drawing.Size(139, 28);
            // 
            // 下拉菜单ToolStripMenuItem
            // 
            下拉菜单ToolStripMenuItem.Name = "下拉菜单ToolStripMenuItem";
            下拉菜单ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            下拉菜单ToolStripMenuItem.Text = "下拉菜单";
            // 
            // MainFormEx
            // 
            ClientSize = new System.Drawing.Size(1280, 720);
            Controls.Add(dockPanel);
            Controls.Add(statusBar);
            Controls.Add(pnlTabMenu);
            Controls.Add(titleBar);
            DoubleBuffered = true;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            IsMdiContainer = true;
            MainMenuStrip = logoMenuControl;
            Name = "MainFormEx";
            Padding = new System.Windows.Forms.Padding(1);
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "DockSample";
            Closing += MainForm_Closing;
            Load += MainForm_Load;
            SizeChanged += MainForm_SizeChanged;
            contextMenuStrip1.ResumeLayout(false);
            pnlTabMenu.ResumeLayout(false);
            pnlTabMenu.PerformLayout();
            titleBar.ResumeLayout(false);
            titleBar.PerformLayout();
            pnlLogoButton.ResumeLayout(false);
            pnlLogoButton.PerformLayout();
            logoMenuControl.ResumeLayout(false);
            logoMenuControl.PerformLayout();
            lbDrop_Menu.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }
        #endregion

        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.StatusStrip statusBar;
        private WeifenLuo.WinFormsUI.Docking.VS2015LightTheme vS2015LightTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2015BlueTheme vS2015BlueTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2015DarkTheme vS2015DarkTheme1;

        private WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender vsToolStripExtender1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ssddfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sdfasdfToolStripMenuItem;

        private System.Windows.Forms.Panel pnlTabMenu;
        public System.Windows.Forms.Panel titleBar;
        private System.Windows.Forms.Label lbClose;
        private System.Windows.Forms.Label lbMax;
        private System.Windows.Forms.Label lbMin;
        private System.Windows.Forms.Panel pnlLogoButton;
        private System.Windows.Forms.ToolTip ToolTips;
        private System.Windows.Forms.MenuStrip logoMenuControl;
        public System.Windows.Forms.ToolStripMenuItem LogoMenu;
        public TitleTabControl TitleTab;
        public TabMenuControl TabMenu;
        private System.Windows.Forms.Label lbDrop;
        private System.Windows.Forms.ContextMenuStrip lbDrop_Menu;
        private System.Windows.Forms.ToolStripMenuItem 下拉菜单ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
    }
}