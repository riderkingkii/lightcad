﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.UI
{
    public class ActiveChangedEventArgs : EventArgs
    {
        public bool IsActive { get; set; }
        public ActiveChangedEventArgs(bool isActive)
        {
            this.IsActive = isActive;
        }
    }
}
