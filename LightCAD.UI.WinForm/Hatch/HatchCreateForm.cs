﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Hatch
{
    public partial class HatchCreateForm : FormBase, IWindow
    {
        public HatchCreateForm()
        {
            InitializeComponent();
        }

        public bool IsActive => true;

        public Type WinType => this.GetType();
    }
}
