﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class PaperSpaceControl : UserControl
    {
        internal static OpenTK.Graphics.GraphicsMode graphicsMode;
        public event EventHandler SkRender;
        public PaperSpaceControl()
        {

            graphicsMode = new OpenTK.Graphics.GraphicsMode(new OpenTK.Graphics.ColorFormat(8, 8, 8, 8), 24, 8);

            InitializeComponent();

            this.VisibleChanged += PaperSpaceControl_VisibleChanged;
            this.skglControl1.Name = this.Name;
            this.skglControl1.PaintSurface += SkglControl1_PaintSurface;
            this.timer1.Tick += Timer1_Tick;
            this.timer1.Enabled = true;
        }

        public string PaperName
        {
            get
            {
                return this.skglControl1.Name;
            }
            set
            {
                this.skglControl1.Name = value;
            }
        }
        private bool inRender;
        private void SkglControl1_PaintSurface(object? sender, SkiaSharp.Views.Desktop.SKPaintGLSurfaceEventArgs e)
        {
            inRender = true;
            try
            {
                SkRender?.Invoke(sender, e);
            }
            finally
            {
                inRender = false;
            }
        }

        private void PaperSpaceControl_VisibleChanged(object? sender, EventArgs e)
        {
        }


        private void Timer1_Tick(object? sender, EventArgs e)
        {
            if (inRender) return;
            this.skglControl1.Invalidate();
        }
        public Dictionary<string, Bounds> GetViewports()
        {
            var vports = new Dictionary<string, Bounds>();
            vports.Add(this.Name, this.Bounds.ToRuntime());
            return vports;
        }
    }
}
