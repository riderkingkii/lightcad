﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public class SkMouseEventArgs : EventArgs
    {
        public SkMouseEventArgs(string type,MouseEventArgs args)
        {
            this.Type = type;
            this.Args = args;
        }
        public string Type { get; set; }
        public MouseEventArgs Args { get; set; }
    }
}
