using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace LightCAD.UI
{
    public partial class ToolWindow : DockContent
    {
        public ToolWindow()
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;
        }
    }
}