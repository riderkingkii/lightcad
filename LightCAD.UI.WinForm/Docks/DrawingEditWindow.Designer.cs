﻿using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI
{
    partial class DrawingEditWindow
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            pnlDocArea = new Panel();
            pnlDrawingEditor = new Panel();
            skglControl1 = new SkiaSharp.Views.Desktop.SKGLControl();
            CommandBar = new CommandBar();
            HeaderBar = new ToolStrip();
            statusStrip1 = new StatusStrip();
            toolStripStatusLabel1 = new ToolStripStatusLabel();
            contextMenu = new ContextMenuStrip(components);
            功能1ToolStripMenuItem = new ToolStripMenuItem();
            功能2ToolStripMenuItem = new ToolStripMenuItem();
            pnlDocArea.SuspendLayout();
            pnlDrawingEditor.SuspendLayout();
            statusStrip1.SuspendLayout();
            contextMenu.SuspendLayout();
            SuspendLayout();
            // 
            // pnlDocArea
            // 
            pnlDocArea.Controls.Add(pnlDrawingEditor);
            pnlDocArea.Dock = DockStyle.Fill;
            pnlDocArea.Location = new Point(0, 27);
            pnlDocArea.Margin = new Padding(3, 4, 3, 4);
            pnlDocArea.Name = "pnlDocArea";
            pnlDocArea.Size = new Size(942, 352);
            pnlDocArea.TabIndex = 9;
            // 
            // pnlDrawingEditor
            // 
            pnlDrawingEditor.Controls.Add(skglControl1);
            pnlDrawingEditor.Dock = DockStyle.Fill;
            pnlDrawingEditor.Location = new Point(0, 0);
            pnlDrawingEditor.Name = "pnlDrawingEditor";
            pnlDrawingEditor.Size = new Size(942, 352);
            pnlDrawingEditor.TabIndex = 12;
            // 
            // skglControl1
            // 
            skglControl1.BackColor = Color.Black;
            skglControl1.Dock = DockStyle.Fill;
            skglControl1.Location = new Point(0, 0);
            skglControl1.Margin = new Padding(4, 4, 4, 4);
            skglControl1.Name = "skglControl1";
            skglControl1.Size = new Size(942, 352);
            skglControl1.TabIndex = 0;
            skglControl1.VSync = true;
            // 
            // CommandBar
            // 
            CommandBar.Dock = DockStyle.Bottom;
            CommandBar.Location = new Point(0, 379);
            CommandBar.Margin = new Padding(3, 4, 3, 4);
            CommandBar.Name = "CommandBar";
            CommandBar.Size = new Size(942, 72);
            CommandBar.TabIndex = 10;
            // 
            // HeaderBar
            // 
            HeaderBar.AutoSize = false;
            HeaderBar.GripStyle = ToolStripGripStyle.Hidden;
            HeaderBar.ImageScalingSize = new Size(24, 24);
            HeaderBar.Location = new Point(0, 0);
            HeaderBar.Name = "HeaderBar";
            HeaderBar.Size = new Size(942, 27);
            HeaderBar.TabIndex = 0;
            HeaderBar.Text = "toolStrip1";
            // 
            // statusStrip1
            // 
            statusStrip1.ImageScalingSize = new Size(20, 20);
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripStatusLabel1 });
            statusStrip1.Location = new Point(0, 451);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Padding = new Padding(1, 0, 11, 0);
            statusStrip1.Size = new Size(942, 22);
            statusStrip1.TabIndex = 0;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            toolStripStatusLabel1.Size = new Size(131, 17);
            toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // contextMenu
            // 
            contextMenu.ImageScalingSize = new Size(20, 20);
            contextMenu.Items.AddRange(new ToolStripItem[] { 功能1ToolStripMenuItem, 功能2ToolStripMenuItem });
            contextMenu.Name = "contextMenu";
            contextMenu.Size = new Size(108, 48);
            // 
            // 功能1ToolStripMenuItem
            // 
            功能1ToolStripMenuItem.Name = "功能1ToolStripMenuItem";
            功能1ToolStripMenuItem.Size = new Size(107, 22);
            功能1ToolStripMenuItem.Text = "功能1";
            // 
            // 功能2ToolStripMenuItem
            // 
            功能2ToolStripMenuItem.Name = "功能2ToolStripMenuItem";
            功能2ToolStripMenuItem.Size = new Size(107, 22);
            功能2ToolStripMenuItem.Text = "功能2";
            // 
            // DrawingEditWindow
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(942, 473);
            Controls.Add(pnlDocArea);
            Controls.Add(HeaderBar);
            Controls.Add(CommandBar);
            Controls.Add(statusStrip1);
            Margin = new Padding(3, 4, 3, 4);
            Name = "DrawingEditWindow";
            pnlDocArea.ResumeLayout(false);
            pnlDrawingEditor.ResumeLayout(false);
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            contextMenu.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn colTitle;
        private DataGridViewTextBoxColumn colValue;
        private Panel pnlDocArea;
        private CommandBar CommandBar;
        private ToolStripButton tbi_UCS;
        private ToolStripButton tbi_Plan;
        private Panel pnlDrawingEditor;
        private StatusStrip StatusBar;
        public ToolStrip HeaderBar;
        private StatusStrip statusStrip1;
        private SkiaSharp.Views.Desktop.SKGLControl skglControl1;
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem 功能1ToolStripMenuItem;
        private ToolStripMenuItem 功能2ToolStripMenuItem;
        private ToolStripStatusLabel toolStripStatusLabel1;
    }
}
