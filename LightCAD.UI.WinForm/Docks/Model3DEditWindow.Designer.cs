﻿namespace LightCAD.UI
{
    partial class Model3DEditWindow
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Model3DEditWindow));
            dgvLevel = new System.Windows.Forms.DataGridView();
            LevelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            LevelEvelation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            btnSelectAllLevel = new System.Windows.Forms.Button();
            LvlPanel = new System.Windows.Forms.Panel();
            panel2 = new System.Windows.Forms.Panel();
            HeaderBar = new System.Windows.Forms.ToolStrip();
            btnLevelShow = new System.Windows.Forms.ToolStripButton();
            btnSectionFrame = new System.Windows.Forms.ToolStripButton();
            btnSync3DViewport = new System.Windows.Forms.ToolStripButton();
            toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            cube = new System.Windows.Forms.ToolStripMenuItem();
            cuboid = new System.Windows.Forms.ToolStripMenuItem();
            cone = new System.Windows.Forms.ToolStripMenuItem();
            cylinder = new System.Windows.Forms.ToolStripMenuItem();
            revolve = new System.Windows.Forms.ToolStripMenuItem();
            loft = new System.Windows.Forms.ToolStripMenuItem();
            extrude = new System.Windows.Forms.ToolStripMenuItem();
            ellipsoid = new System.Windows.Forms.ToolStripMenuItem();
            sphere = new System.Windows.Forms.ToolStripMenuItem();
            prism = new System.Windows.Forms.ToolStripMenuItem();
            move = new System.Windows.Forms.ToolStripButton();
            toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            revit = new System.Windows.Forms.ToolStripMenuItem();
            su = new System.Windows.Forms.ToolStripMenuItem();
            lightCAD = new System.Windows.Forms.ToolStripMenuItem();
            CommandBar = new CommandBar();
            statusStrip1 = new System.Windows.Forms.StatusStrip();
            edit = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)dgvLevel).BeginInit();
            LvlPanel.SuspendLayout();
            panel2.SuspendLayout();
            HeaderBar.SuspendLayout();
            SuspendLayout();
            // 
            // dgvLevel
            // 
            dgvLevel.AllowUserToAddRows = false;
            dgvLevel.AllowUserToResizeColumns = false;
            dgvLevel.AllowUserToResizeRows = false;
            dgvLevel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            dgvLevel.BackgroundColor = System.Drawing.SystemColors.Control;
            dgvLevel.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dgvLevel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvLevel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { LevelName, LevelEvelation });
            dgvLevel.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dgvLevel.Location = new System.Drawing.Point(0, 25);
            dgvLevel.Name = "dgvLevel";
            dgvLevel.RowHeadersVisible = false;
            dgvLevel.RowHeadersWidth = 51;
            dgvLevel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dgvLevel.RowTemplate.Height = 25;
            dgvLevel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgvLevel.Size = new System.Drawing.Size(106, 0);
            dgvLevel.TabIndex = 0;
            dgvLevel.SelectionChanged += dgvLevel_SelectionChanged;
            // 
            // LevelName
            // 
            LevelName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            LevelName.DataPropertyName = "Name";
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            LevelName.DefaultCellStyle = dataGridViewCellStyle5;
            LevelName.HeaderText = "名称";
            LevelName.MinimumWidth = 6;
            LevelName.Name = "LevelName";
            LevelName.ReadOnly = true;
            LevelName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LevelEvelation
            // 
            LevelEvelation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            LevelEvelation.DataPropertyName = "Elevation";
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            LevelEvelation.DefaultCellStyle = dataGridViewCellStyle6;
            LevelEvelation.HeaderText = "层高";
            LevelEvelation.MinimumWidth = 6;
            LevelEvelation.Name = "LevelEvelation";
            LevelEvelation.ReadOnly = true;
            LevelEvelation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnSelectAllLevel
            // 
            btnSelectAllLevel.Location = new System.Drawing.Point(0, 0);
            btnSelectAllLevel.Name = "btnSelectAllLevel";
            btnSelectAllLevel.Size = new System.Drawing.Size(55, 23);
            btnSelectAllLevel.TabIndex = 1;
            btnSelectAllLevel.Text = "全选";
            btnSelectAllLevel.UseVisualStyleBackColor = true;
            btnSelectAllLevel.Click += btnSelectAllLevel_Click;
            // 
            // LvlPanel
            // 
            LvlPanel.BackColor = System.Drawing.Color.White;
            LvlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            LvlPanel.Controls.Add(dgvLevel);
            LvlPanel.Controls.Add(panel2);
            LvlPanel.Location = new System.Drawing.Point(0, 31);
            LvlPanel.Name = "LvlPanel";
            LvlPanel.Size = new System.Drawing.Size(56, 23);
            LvlPanel.TabIndex = 2;
            LvlPanel.Visible = false;
            // 
            // panel2
            // 
            panel2.Controls.Add(btnSelectAllLevel);
            panel2.Dock = System.Windows.Forms.DockStyle.Top;
            panel2.Location = new System.Drawing.Point(0, 0);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(54, 32);
            panel2.TabIndex = 3;
            // 
            // HeaderBar
            // 
            HeaderBar.AutoSize = false;
            HeaderBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            HeaderBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            HeaderBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnLevelShow, btnSectionFrame, btnSync3DViewport, toolStripDropDownButton1, move, toolStripDropDownButton2, edit });
            HeaderBar.Location = new System.Drawing.Point(0, 0);
            HeaderBar.Name = "HeaderBar";
            HeaderBar.Size = new System.Drawing.Size(765, 27);
            HeaderBar.TabIndex = 11;
            HeaderBar.Text = "toolStrip1";
            // 
            // btnLevelShow
            // 
            btnLevelShow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnLevelShow.Image = (System.Drawing.Image)resources.GetObject("btnLevelShow.Image");
            btnLevelShow.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLevelShow.Name = "btnLevelShow";
            btnLevelShow.Size = new System.Drawing.Size(60, 24);
            btnLevelShow.Text = "楼层过滤";
            btnLevelShow.Click += btnLevelShow_Click;
            // 
            // btnSectionFrame
            // 
            btnSectionFrame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnSectionFrame.Image = (System.Drawing.Image)resources.GetObject("btnSectionFrame.Image");
            btnSectionFrame.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSectionFrame.Name = "btnSectionFrame";
            btnSectionFrame.Size = new System.Drawing.Size(60, 24);
            btnSectionFrame.Text = "剖切视图";
            btnSectionFrame.Click += btnSectionFrame_Click;
            // 
            // btnSync3DViewport
            // 
            btnSync3DViewport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnSync3DViewport.Image = (System.Drawing.Image)resources.GetObject("btnSync3DViewport.Image");
            btnSync3DViewport.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSync3DViewport.Name = "btnSync3DViewport";
            btnSync3DViewport.Size = new System.Drawing.Size(60, 24);
            btnSync3DViewport.Text = "同步视口";
            btnSync3DViewport.Click += btnSync3DViewport_Click;
            // 
            // toolStripDropDownButton1
            // 
            toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { cube, cuboid, cone, cylinder, revolve, loft, extrude, ellipsoid, sphere, prism });
            toolStripDropDownButton1.Image = (System.Drawing.Image)resources.GetObject("toolStripDropDownButton1.Image");
            toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            toolStripDropDownButton1.Size = new System.Drawing.Size(69, 24);
            toolStripDropDownButton1.Text = "基本元素";
            // 
            // cube
            // 
            cube.Name = "cube";
            cube.Size = new System.Drawing.Size(112, 22);
            cube.Text = "立方体";
            cube.Click += CommandBtn_Click;
            // 
            // cuboid
            // 
            cuboid.Name = "cuboid";
            cuboid.Size = new System.Drawing.Size(112, 22);
            cuboid.Text = "长方体";
            cuboid.Click += CommandBtn_Click;
            // 
            // cone
            // 
            cone.Name = "cone";
            cone.Size = new System.Drawing.Size(112, 22);
            cone.Text = "圆锥";
            cone.Click += CommandBtn_Click;
            // 
            // cylinder
            // 
            cylinder.Name = "cylinder";
            cylinder.Size = new System.Drawing.Size(112, 22);
            cylinder.Text = "圆柱";
            cylinder.Click += CommandBtn_Click;
            // 
            // revolve
            // 
            revolve.Name = "revolve";
            revolve.Size = new System.Drawing.Size(112, 22);
            revolve.Text = "旋转体";
            revolve.Click += CommandBtn_Click;
            // 
            // loft
            // 
            loft.Name = "loft";
            loft.Size = new System.Drawing.Size(112, 22);
            loft.Text = "放样体";
            loft.Click += CommandBtn_Click;
            // 
            // extrude
            // 
            extrude.Name = "extrude";
            extrude.Size = new System.Drawing.Size(112, 22);
            extrude.Text = "拉伸体";
            extrude.Click += CommandBtn_Click;
            // 
            // ellipsoid
            // 
            ellipsoid.Name = "ellipsoid";
            ellipsoid.Size = new System.Drawing.Size(112, 22);
            ellipsoid.Text = "椭圆体";
            ellipsoid.Click += CommandBtn_Click;
            // 
            // sphere
            // 
            sphere.Name = "sphere";
            sphere.Size = new System.Drawing.Size(112, 22);
            sphere.Text = "球体";
            sphere.Click += CommandBtn_Click;
            // 
            // prism
            // 
            prism.Name = "prism";
            prism.Size = new System.Drawing.Size(112, 22);
            prism.Text = "棱柱";
            prism.Click += CommandBtn_Click;
            // 
            // move
            // 
            move.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            move.Image = (System.Drawing.Image)resources.GetObject("move.Image");
            move.ImageTransparentColor = System.Drawing.Color.Magenta;
            move.Name = "move";
            move.Size = new System.Drawing.Size(36, 24);
            move.Text = "移动";
            move.Click += CommandBtn_Click;
            // 
            // toolStripDropDownButton2
            // 
            toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { revit, su, lightCAD });
            toolStripDropDownButton2.Image = (System.Drawing.Image)resources.GetObject("toolStripDropDownButton2.Image");
            toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            toolStripDropDownButton2.Size = new System.Drawing.Size(81, 24);
            toolStripDropDownButton2.Text = "相机控制器";
            // 
            // revit
            // 
            revit.Name = "revit";
            revit.Size = new System.Drawing.Size(129, 22);
            revit.Text = "Revit";
            revit.Click += ControlsChange;
            // 
            // su
            // 
            su.Name = "su";
            su.Size = new System.Drawing.Size(129, 22);
            su.Text = "Su";
            su.Click += ControlsChange;
            // 
            // lightCAD
            // 
            lightCAD.Name = "lightCAD";
            lightCAD.Size = new System.Drawing.Size(129, 22);
            lightCAD.Text = "LightCAD";
            lightCAD.Click += ControlsChange;
            // 
            // CommandBar
            // 
            CommandBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            CommandBar.Location = new System.Drawing.Point(0, 373);
            CommandBar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            CommandBar.Name = "CommandBar";
            CommandBar.Size = new System.Drawing.Size(765, 26);
            CommandBar.TabIndex = 13;
            // 
            // statusStrip1
            // 
            statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            statusStrip1.Location = new System.Drawing.Point(0, 399);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 11, 0);
            statusStrip1.Size = new System.Drawing.Size(765, 22);
            statusStrip1.TabIndex = 12;
            statusStrip1.Text = "statusStrip1";
            // 
            // edit
            // 
            edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            edit.Image = (System.Drawing.Image)resources.GetObject("edit.Image");
            edit.ImageTransparentColor = System.Drawing.Color.Magenta;
            edit.Name = "edit";
            edit.Size = new System.Drawing.Size(60, 24);
            edit.Text = "开始编辑";
            edit.Click += CommandBtn_Click;
            // 
            // Model3DEditWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(765, 421);
            Controls.Add(HeaderBar);
            Controls.Add(CommandBar);
            Controls.Add(statusStrip1);
            Controls.Add(LvlPanel);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "Model3DEditWindow";
            ((System.ComponentModel.ISupportInitialize)dgvLevel).EndInit();
            LvlPanel.ResumeLayout(false);
            panel2.ResumeLayout(false);
            HeaderBar.ResumeLayout(false);
            HeaderBar.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLevel;
        private System.Windows.Forms.Button btnSelectAllLevel;
        private System.Windows.Forms.Panel LvlPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn LevelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LevelEvelation;
        public System.Windows.Forms.ToolStrip HeaderBar;
        private CommandBar CommandBar;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton btnLevelShow;
        private System.Windows.Forms.ToolStripButton btnSectionFrame;
        private System.Windows.Forms.ToolStripButton btnSync3DViewport;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem cube;
        private System.Windows.Forms.ToolStripMenuItem cuboid;
        private System.Windows.Forms.ToolStripMenuItem cone;
        private System.Windows.Forms.ToolStripButton move;
        private System.Windows.Forms.ToolStripMenuItem cylinder;
        private System.Windows.Forms.ToolStripMenuItem revolve;
        private System.Windows.Forms.ToolStripMenuItem loft;
        private System.Windows.Forms.ToolStripMenuItem extrude;
        private System.Windows.Forms.ToolStripMenuItem ellipsoid;
        private System.Windows.Forms.ToolStripMenuItem sphere;
        private System.Windows.Forms.ToolStripMenuItem prism;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem revit;
        private System.Windows.Forms.ToolStripMenuItem su;
        private System.Windows.Forms.ToolStripMenuItem lightCAD;
        private System.Windows.Forms.ToolStripButton edit;
    }
}
