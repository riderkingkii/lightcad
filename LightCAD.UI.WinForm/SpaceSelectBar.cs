﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class SpaceSelectBar : UserControl
    {
        public event EventHandler SpaceChanged;
        public SpaceSelectBar()
        {
            InitializeComponent();
        }

        private void btnModel_Click(object sender, EventArgs e)
        {
            SpaceChanged?.Invoke(sender, new SpaceChangedEventArgs(true, -1));
        }

        private void btnPaper1_Click(object sender, EventArgs e)
        {
            SpaceChanged?.Invoke(sender, new SpaceChangedEventArgs(false, 0));
        }

        private void btnPaper2_Click(object sender, EventArgs e)
        {
            SpaceChanged?.Invoke(sender, new SpaceChangedEventArgs(false, 1));
        }
    }
}
