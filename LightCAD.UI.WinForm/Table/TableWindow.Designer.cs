﻿namespace LightCAD.UI.Table
{
    partial class TableWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox2 = new System.Windows.Forms.GroupBox();
            radioByWindow = new System.Windows.Forms.RadioButton();
            radioByPoint = new System.Windows.Forms.RadioButton();
            groupBox1 = new System.Windows.Forms.GroupBox();
            label4 = new System.Windows.Forms.Label();
            numRowHeight = new System.Windows.Forms.NumericUpDown();
            label3 = new System.Windows.Forms.Label();
            numRowCount = new System.Windows.Forms.NumericUpDown();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            numColWidth = new System.Windows.Forms.NumericUpDown();
            numColCount = new System.Windows.Forms.NumericUpDown();
            groupBox3 = new System.Windows.Forms.GroupBox();
            cmbRowStyle3 = new System.Windows.Forms.ComboBox();
            cmbRowStyle2 = new System.Windows.Forms.ComboBox();
            cmbRowStyle1 = new System.Windows.Forms.ComboBox();
            label7 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            helpbtn = new System.Windows.Forms.Button();
            btn_cancel = new System.Windows.Forms.Button();
            btn_confirm = new System.Windows.Forms.Button();
            groupBox2.SuspendLayout();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numRowHeight).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numRowCount).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numColWidth).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numColCount).BeginInit();
            groupBox3.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(radioByWindow);
            groupBox2.Controls.Add(radioByPoint);
            groupBox2.Location = new System.Drawing.Point(12, 12);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new System.Drawing.Size(340, 80);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "插入方式";
            // 
            // radioByWindow
            // 
            radioByWindow.AutoSize = true;
            radioByWindow.Location = new System.Drawing.Point(12, 49);
            radioByWindow.Name = "radioByWindow";
            radioByWindow.Size = new System.Drawing.Size(74, 21);
            radioByWindow.TabIndex = 1;
            radioByWindow.TabStop = true;
            radioByWindow.Text = "指定窗口";
            radioByWindow.UseVisualStyleBackColor = true;
            // 
            // radioByPoint
            // 
            radioByPoint.AutoSize = true;
            radioByPoint.Location = new System.Drawing.Point(12, 22);
            radioByPoint.Name = "radioByPoint";
            radioByPoint.Size = new System.Drawing.Size(86, 21);
            radioByPoint.TabIndex = 0;
            radioByPoint.TabStop = true;
            radioByPoint.Text = "指定插入点";
            radioByPoint.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(numRowHeight);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(numRowCount);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label1);
            groupBox1.Controls.Add(numColWidth);
            groupBox1.Controls.Add(numColCount);
            groupBox1.Location = new System.Drawing.Point(12, 106);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(340, 100);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            groupBox1.Text = "列和行设置";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(189, 69);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(44, 17);
            label4.TabIndex = 7;
            label4.Text = "行高：";
            // 
            // numRowHeight
            // 
            numRowHeight.Location = new System.Drawing.Point(254, 67);
            numRowHeight.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            numRowHeight.Name = "numRowHeight";
            numRowHeight.Size = new System.Drawing.Size(80, 23);
            numRowHeight.TabIndex = 6;
            numRowHeight.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(6, 69);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(68, 17);
            label3.TabIndex = 5;
            label3.Text = "数据行数：";
            // 
            // numRowCount
            // 
            numRowCount.Location = new System.Drawing.Point(80, 67);
            numRowCount.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            numRowCount.Name = "numRowCount";
            numRowCount.Size = new System.Drawing.Size(80, 23);
            numRowCount.TabIndex = 4;
            numRowCount.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(188, 28);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(44, 17);
            label2.TabIndex = 3;
            label2.Text = "列宽：";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(6, 27);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(44, 17);
            label1.TabIndex = 2;
            label1.Text = "列数：";
            // 
            // numColWidth
            // 
            numColWidth.DecimalPlaces = 1;
            numColWidth.Increment = new decimal(new int[] { 5, 0, 0, 65536 });
            numColWidth.Location = new System.Drawing.Point(254, 25);
            numColWidth.Maximum = new decimal(new int[] { 1215752191, 23, 0, 0 });
            numColWidth.Name = "numColWidth";
            numColWidth.Size = new System.Drawing.Size(80, 23);
            numColWidth.TabIndex = 1;
            // 
            // numColCount
            // 
            numColCount.Location = new System.Drawing.Point(80, 25);
            numColCount.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            numColCount.Name = "numColCount";
            numColCount.Size = new System.Drawing.Size(80, 23);
            numColCount.TabIndex = 0;
            numColCount.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(cmbRowStyle3);
            groupBox3.Controls.Add(cmbRowStyle2);
            groupBox3.Controls.Add(cmbRowStyle1);
            groupBox3.Controls.Add(label7);
            groupBox3.Controls.Add(label6);
            groupBox3.Controls.Add(label5);
            groupBox3.Location = new System.Drawing.Point(12, 212);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new System.Drawing.Size(339, 141);
            groupBox3.TabIndex = 3;
            groupBox3.TabStop = false;
            groupBox3.Text = "设置单元样式";
            // 
            // cmbRowStyle3
            // 
            cmbRowStyle3.FormattingEnabled = true;
            cmbRowStyle3.Location = new System.Drawing.Point(179, 103);
            cmbRowStyle3.Name = "cmbRowStyle3";
            cmbRowStyle3.Size = new System.Drawing.Size(121, 25);
            cmbRowStyle3.TabIndex = 8;
            // 
            // cmbRowStyle2
            // 
            cmbRowStyle2.FormattingEnabled = true;
            cmbRowStyle2.Location = new System.Drawing.Point(179, 65);
            cmbRowStyle2.Name = "cmbRowStyle2";
            cmbRowStyle2.Size = new System.Drawing.Size(121, 25);
            cmbRowStyle2.TabIndex = 7;
            // 
            // cmbRowStyle1
            // 
            cmbRowStyle1.FormattingEnabled = true;
            cmbRowStyle1.Location = new System.Drawing.Point(179, 26);
            cmbRowStyle1.Name = "cmbRowStyle1";
            cmbRowStyle1.Size = new System.Drawing.Size(121, 25);
            cmbRowStyle1.TabIndex = 6;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(6, 106);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(128, 17);
            label7.TabIndex = 5;
            label7.Text = "所有其他行单元样式：";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(6, 66);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(104, 17);
            label6.TabIndex = 4;
            label6.Text = "第二行单元样式：";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(6, 29);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(104, 17);
            label5.TabIndex = 3;
            label5.Text = "第一行单元样式：";
            // 
            // helpbtn
            // 
            helpbtn.Location = new System.Drawing.Point(257, 404);
            helpbtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            helpbtn.Name = "helpbtn";
            helpbtn.Size = new System.Drawing.Size(73, 25);
            helpbtn.TabIndex = 17;
            helpbtn.Text = "帮助(H)";
            helpbtn.UseVisualStyleBackColor = true;
            // 
            // btn_cancel
            // 
            btn_cancel.Location = new System.Drawing.Point(142, 404);
            btn_cancel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_cancel.Name = "btn_cancel";
            btn_cancel.Size = new System.Drawing.Size(73, 25);
            btn_cancel.TabIndex = 16;
            btn_cancel.Text = "取消";
            btn_cancel.UseVisualStyleBackColor = true;
            btn_cancel.Click += btn_cancel_Click;
            // 
            // btn_confirm
            // 
            btn_confirm.Location = new System.Drawing.Point(25, 404);
            btn_confirm.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn_confirm.Name = "btn_confirm";
            btn_confirm.Size = new System.Drawing.Size(73, 25);
            btn_confirm.TabIndex = 15;
            btn_confirm.Text = "确认";
            btn_confirm.UseVisualStyleBackColor = true;
            btn_confirm.Click += btn_confirm_Click;
            // 
            // TableWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(364, 441);
            Controls.Add(helpbtn);
            Controls.Add(btn_cancel);
            Controls.Add(btn_confirm);
            Controls.Add(groupBox3);
            Controls.Add(groupBox1);
            Controls.Add(groupBox2);
            Name = "TableWindow";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = " ";
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numRowHeight).EndInit();
            ((System.ComponentModel.ISupportInitialize)numRowCount).EndInit();
            ((System.ComponentModel.ISupportInitialize)numColWidth).EndInit();
            ((System.ComponentModel.ISupportInitialize)numColCount).EndInit();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button helpbtn;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_confirm;
        private System.Windows.Forms.RadioButton radioByPoint;
        private System.Windows.Forms.RadioButton radioByWindow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numColWidth;
        private System.Windows.Forms.NumericUpDown numColCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numRowHeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numRowCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbRowStyle3;
        private System.Windows.Forms.ComboBox cmbRowStyle2;
        private System.Windows.Forms.ComboBox cmbRowStyle1;
    }
}