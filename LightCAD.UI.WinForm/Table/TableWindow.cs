﻿using LightCAD.Drawing.Actions;
using LightCAD.Drawing.Actions.Action;
using LightCAD.Runtime.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LightCAD.UI.Table
{
    public partial class TableWindow : FormBase, ITableWindow
    {
        public Type WinType => this.GetType();
        public string CurrentAction { get; set; }
        public bool IsActive => true;

        public TableAction tableAction;

        public TableWindow()
        {
            InitializeComponent();
        }

        public TableWindow(params object[] args) : this()
        {
            if (args != null && args.Length > 0)
            {
                tableAction = (TableAction)args[0];
                CurrentAction = "OK";

                radioByPoint.CheckedChanged += RadioByPoint_CheckedChanged;
                numColCount.Value = tableAction.columnCount;
                numColWidth.Value = (decimal)tableAction.columnWidth;
                numRowCount.Value = tableAction.rowCount;
                numRowHeight.Value = tableAction.rowHeight;
                initCmbRowStyle();
            }
        }

        private void RadioByPoint_CheckedChanged(object? sender, EventArgs e)
        {
            if (radioByPoint.Checked)
            {
                tableAction.insertWay = TableInsertWay.BP;
                numColWidth.Enabled = true;
                numRowCount.Enabled = true;
            }
            else
            {
                tableAction.insertWay = TableInsertWay.BW;
                numColWidth.Enabled = false;
                numRowCount.Enabled = false;
            }
        }

        private void initCmbRowStyle()
        {
            Dictionary<CellType, string> cellTypeEnum = EnumExtensions.GetAllDescriptions<CellType>();
            cmbRowStyle1.DataSource = cellTypeEnum.Values.ToList();
            cmbRowStyle1.SelectedIndex = 0;

            cmbRowStyle2.DataSource = cellTypeEnum.Values.ToList();
            cmbRowStyle2.SelectedIndex = 1;

            cmbRowStyle3.DataSource = cellTypeEnum.Values.ToList();
            cmbRowStyle3.SelectedIndex = 2;
        }

        private void btn_confirm_Click(object sender, EventArgs e)
        {
            if (tableAction != null)
            {
                tableAction.columnCount = (int)numColCount.Value;
                tableAction.columnWidth = (double)numColWidth.Value;
                tableAction.rowCount = (int)numRowCount.Value;
                tableAction.rowHeight = (int)numRowHeight.Value;

                var rowStyle1 = EnumExtensions.GetEnumValueFromDescription<CellType>(cmbRowStyle1.SelectedItem?.ToString());
                var rowStyle2 = EnumExtensions.GetEnumValueFromDescription<CellType>(cmbRowStyle2.SelectedItem?.ToString());
                var rowStyle3 = EnumExtensions.GetEnumValueFromDescription<CellType>(cmbRowStyle3.SelectedItem?.ToString());
                tableAction.cellTypes = new CellType[] { rowStyle1, rowStyle2, rowStyle3 };
            }

            DialogResult = DialogResult.OK;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
