﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.UI
{
    public class TitleTabItem
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public int Width { get; set; }
        public Image Icon { get; set; }
        public bool Closable { get; set; }
        public bool Movable { get; set; }
        public object Tag { get; set; }
    }

}
