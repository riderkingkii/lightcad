﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class TitleTabItemControl : UserControl
    {
        public const int MaxWidth = 150;

        private bool isSelected;

        public event EventHandler ItemClick;
        public event EventHandler ItemClose;
        public TitleTabItem TabItem { get; set; }

        public bool HasRightDivider { get; set; }

        public TitleTabItemControl()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.picLeft.Click += Item_Click;
            this.picIcon.Click += Item_Click;
            this.lblText.Click += Item_Click;
            this.picClose.Click += PicClose_Click;
            this.picClose.MouseEnter += PicClose_MouseEnter;
            this.picClose.MouseLeave += PicClose_MouseLeave;
            this.Disposed += TitleTabItemControl_Disposed;
        }
        public TitleTabItemControl(TitleTabItem tabItem) : this()
        {
            this.TabItem = tabItem;
            ResetItem();
        }
        private void TitleTabItemControl_Disposed(object? sender, EventArgs e)
        {
            if (this.IsDisposed) return;

            this.picLeft.Click -= Item_Click;
            this.picIcon.Click -= Item_Click;
            this.lblText.Click -= Item_Click;
            this.picClose.Click -= PicClose_Click;
            this.picClose.MouseEnter -= PicClose_MouseEnter;
            this.picClose.MouseLeave -= PicClose_MouseLeave;
            this.Disposed -= TitleTabItemControl_Disposed;
        }
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                if (this.isSelected != value)
                {
                    this.isSelected = value;
                    this.ResetItem();
                }
            }
        }
        private void PicClose_MouseLeave(object? sender, EventArgs e)
        {
            this.picClose.Image = Properties.TabResources.Close;
        }

        private void PicClose_MouseEnter(object? sender, EventArgs e)
        {
            this.picClose.Image = Properties.TabResources.CloseHover;
        }



        private void PicClose_Click(object? sender, EventArgs e)
        {
            ItemClose?.Invoke(this, EventArgs.Empty);
        }

        private void Item_Click(object? sender, EventArgs e)
        {
            ItemClick?.Invoke(this, EventArgs.Empty);
        }

        public void ResetItem()
        {
            if (this.isSelected)
            {
                this.picLeft.Image = Properties.TabResources.Left;
                this.pnlTab.BackgroundImage = Properties.TabResources.Center;

                this.picRight.Image = Properties.TabResources.Right;
            }
            else
            {
                this.picLeft.Image = Properties.TabResources.InactiveLeft;
                this.pnlTab.BackgroundImage = Properties.TabResources.Background;
                if (HasRightDivider)
                    this.picRight.Image = Properties.TabResources.InactiveRight;
                else
                    this.picRight.Image = Properties.TabResources.InactiveRightNoDivider;
            }
            if (TabItem == null)
            {
                this.Invalidate();
                return;
            }



            this.picIcon.Image = TabItem.Icon;
            this.picIcon.Visible = (this.TabItem.Icon != null);

            var text = TabItem.Text ?? "";
            if (text.Length > 0)
            {
                text = UIUtils.SubString(this.CreateGraphics(), this.Font, text, MaxWidth);
            }
            this.lblText.Text = TabItem.Text;

            this.picClose.Visible = this.TabItem.Closable;

            if (this.TabItem.Width > 0)
            {
                this.lblText.AutoSize = false;
                this.lblText.Width = this.TabItem.Width;
            }
            else
            {
                this.lblText.AutoSize = true;
                this.lblText.Width = 0;
            }

            this.Invalidate();
        }

    }
}
