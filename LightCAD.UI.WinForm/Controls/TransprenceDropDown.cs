﻿using Avalonia.Controls;
using LightCAD.Core;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Controls
{
    public partial class TransprenceDropDown : DropDownControl
    {
        private List<string> transprenceList = new List<string>() { ValueFrom.ByLayer, ValueFrom.ByBlock, "透明度值" };
        private List<LcElement> lcElementsList = new List<LcElement>();
        private LcDocument lcDocument;
        private string selectTransprence;

        public TransprenceDropDown()
        {
            InitializeComponent();
            InitializeDropDown(itemsPanel);
        }

        private void TranspranceItemControl_ItemClick(object? sender, string e)
        {
            OpenDropDown();
        }

        public void InitTransprence(LcDocument lcDocument)
        {
            this.lcDocument = lcDocument;
            itemsPanel.Controls.Clear();
            foreach (var transprence in transprenceList)
            {
                if (transprence == ValueFrom.ByLayer)
                {
                    this.selectTransprence = lcDocument.Layers.First(x => x.IsStatus).Transparency.ToString();
                    var transprenceItem = new TransparenceItemControl(transprence);
                    transprenceItem.ItemClick += TransprenceItem_ItemClick;
                    itemsPanel.Controls.Add(transprenceItem);
                }
                else
                {
                    var transprenceItem = new TransparenceItemControl(transprence);
                    transprenceItem.ItemClick += TransprenceItem_ItemClick;
                    itemsPanel.Controls.Add(transprenceItem);
                }
            }
        }

        private void TransprenceItem_ItemClick(object? sender, string e)
        {
            if (sender != null && e is string)
            {
                switch (e)
                {
                    case "ByLayer":
                        this.selectTransprence = lcDocument.Layers.First(x => x.IsStatus).Transparency.ToString();
                        break;
                    case "ByBlock":
                        this.selectTransprence = "0";
                        break;
                    case "透明度值":
                        this.selectTransprence = lcDocument.Transprence;
                        if (this.selectTransprence == ValueFrom.ByLayer || this.selectTransprence == ValueFrom.ByBlock)
                        {
                            this.selectTransprence = "0";
                        }
                        break;
                    default:
                        break;
                }               
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void dropDownControl1_Load(object sender, EventArgs e)
        {

        }
    }


}
