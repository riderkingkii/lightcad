﻿namespace LightCAD.UI
{
    partial class CmdLogInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtLog = new System.Windows.Forms.RichTextBox();
            SuspendLayout();
            // 
            // txtLog
            // 
            txtLog.BackColor = System.Drawing.SystemColors.ControlDark;
            txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLog.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtLog.Location = new System.Drawing.Point(0, 0);
            txtLog.Name = "txtLog";
            txtLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            txtLog.Size = new System.Drawing.Size(1198, 383);
            txtLog.TabIndex = 0;
            txtLog.Text = "";
            // 
            // CmdLogInfoForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.SystemColors.ControlDark;
            ClientSize = new System.Drawing.Size(1198, 383);
            Controls.Add(txtLog);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "CmdLogInfoForm";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            Text = "PopupInfoForm";
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.RichTextBox txtLog;
    }
}