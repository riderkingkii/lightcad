﻿using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI.Controls
{
    partial class LineTypeItemControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLineTypeName = new Label();
            this.SuspendLayout();
            // 
            // lblLineTypeName
            // 
            this.lblLineTypeName.BackColor = Color.White;
            this.lblLineTypeName.Location = new Point(2, 3);
            this.lblLineTypeName.Name = "lblLineTypeName";
            this.lblLineTypeName.Size = new Size(131, 28);
            this.lblLineTypeName.TabIndex = 0;
            this.lblLineTypeName.TextAlign = ContentAlignment.MiddleLeft;
            this.lblLineTypeName.Click += new System.EventHandler(this.lblLineTypeName_Click);
            // 
            // LineTypeItemControl
            // 
            this.AutoScaleDimensions = new SizeF(9F, 20F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add(this.lblLineTypeName);
            this.Name = "LineTypeItemControl";
            this.Size = new Size(155, 28);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLineTypeName;
    }
}
