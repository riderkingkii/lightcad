﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace LightCAD.UI
{
    public interface IHeaderItem
    {
    }
    public interface ITabPanel
    {
        event EventHandler ItemClick;
        TabButtonGroupRuntime CreateButtonGroup(TabButtonGroup btnGroup);
    }
    public interface ITabButton
    {
        void ResetButton(TabButton button);
    }
    public interface ITableLayout
    {
        void SetColumn(object button, int colum);
        void SetColumnSpan(object button, int columnSpan);
        void AddColumnDefinition(object width);
        void SetRow(object button, int row);
        void SetRowSpan(object button, int rowSpan);
        void AddRowDefinition(object height);

        void AddControl(object ctrl);
        int Width { get; set; }
        int Height { get; set; }
        int ColumnCount { get; set; }
    }
    public class TabItem
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string ShortcutKey { get; set; }

        public List<TabButtonGroup> ButtonGroups { get; set; }
    }

    public class TabButtonGroup
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public List<TabButton> Buttons { get; set; }
    }
    public class TabButton
    {
        /// <summary>
        /// 按钮键值名称，如果作为命令，命名格式为：xxx_CommandName
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 按钮的显示文本
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 按钮图标，32*32大小
        /// </summary>
        public Image Icon { get; set; }
        /// <summary>
        /// 按钮的行为是否是命令
        /// </summary>
        public bool IsCommand { get; set; }
        /// <summary>
        /// 按钮命令的名称，如果不设置名称，将Name作为命令发出
        /// </summary>
        public string CommandName { get; set; }

        /// <summary>
        /// 按钮提示
        /// </summary>
        public string Tooltip { get; set; }

        /// <summary>
        /// 是否频繁使用的
        /// </summary>
        public bool IsFreqUsed { get; set; }

        public int Width { get; set; }

        /// <summary>
        /// 小尺寸按钮占一行，一般按钮占两行
        /// </summary>
        public bool IsSmallSize { get; set; }
        /// <summary>
        /// 按钮不作为事件时的，点击事件处理函数
        /// </summary>
        public EventHandler Handler { get; set; }
        /// <summary>
        /// 只用于下拉，自身无事件
        /// </summary>
        public bool OnlyDropDown { get; set; }

        /// <summary>
        /// 下拉按钮集合
        /// </summary>
        public List<TabButton> DropDowns { get; set; } = new List<TabButton>();

        public int GetWidth()
        {
            if (Width == 110)
            {

            }
            if (Width != 0) return Width;
            if (IsSmallSize)
                return 88;
            else
                return 64;
        }
    }

    public class TabItemRuntime
    {
        public TabItem Item { get; set; }
        public bool IsSelected { get; set; }
        public bool IsVisible { get; set; }
        public int Index { get; set; }
        public IHeaderItem Header { get; set; }
        public ITabPanel TabPanel { get; set; }
        public List<TabButtonGroupRuntime> ButtonGroupRts { get; set; }

        public TabButtonGroup GetGroup(string name)
        {
            return null;
        }
        public TabButtonGroupRuntime GetGroupControl(string name)
        {
            return null;
        }
        public TabButtonGroupRuntime AddGroup(TabButtonGroup buttonGroup)
        {
            if (this.ButtonGroupRts == null)
            {
                this.ButtonGroupRts = new List<TabButtonGroupRuntime>();
            }
            if (Item.ButtonGroups == null)
            {
                Item.ButtonGroups = new List<TabButtonGroup>();
            }
            if (Item.ButtonGroups.Contains(buttonGroup))
            {
                return this.ButtonGroupRts.FirstOrDefault((item) => item.ButtonGroup == buttonGroup);
            }
            Item.ButtonGroups.Add(buttonGroup);
            var btnGroupCtrl = this.TabPanel.CreateButtonGroup(buttonGroup);
            this.ButtonGroupRts.Add(btnGroupCtrl);

            return btnGroupCtrl;

        }
        public void RemoveGroup(TabButtonGroup buttonGroup)
        {

        }
        public void SetGroupVisible(TabButtonGroup buttonGroup, bool visible)
        {

        }
        public void SetGroupDisable(TabButtonGroup buttonGroup, bool disabled)
        {

        }
    }

    public class TabButtonGroupRuntime
    {
        public TabButtonGroup ButtonGroup { get; set; }
        public List<TabButtonControl> ButtonControls { get; set; }
        public ITableLayout TableLayout { get; set; }
    }
    public class TabButtonControl
    {
        public TabButton Button { get; set; }
        public ITabButton ButtonControl { get; set; }
    }
    public class TabMenuManager
    {

        private List<TabItem> tabItems { get; } = new List<TabItem>();
        private List<TabItemRuntime> tabItemRts { get; } = new List<TabItemRuntime>();

        private ReadOnlyCollection<TabItem> readonlyTabItems;
        private ReadOnlyCollection<TabItemRuntime> readonlyTabItemCtrls;

        public static Func<ThemeBase> GetTheme;
        public TabMenuControl Control { get; }
        public TabMenuManager(TabMenuControl tabMenuControl)
        {
            tabMenuControl.TabMgr = this;
            Control = tabMenuControl;
        }
        public ReadOnlyCollection<TabItem> TabItems
        {
            get
            {
                if (readonlyTabItems == null)
                {
                    readonlyTabItems = new ReadOnlyCollection<TabItem>(this.tabItems);
                }
                return readonlyTabItems;
            }
        }
        public ReadOnlyCollection<TabItemRuntime> TabItemControls
        {
            get
            {
                if (readonlyTabItemCtrls == null)
                {
                    readonlyTabItemCtrls = new ReadOnlyCollection<TabItemRuntime>(this.tabItemRts);
                }
                return readonlyTabItemCtrls;
            }
        }

        public TabItem GetTab(string tabName)
        {
            return this.tabItems.FirstOrDefault((item) => item.Name == tabName);
        }
        public TabItemRuntime GetTabControl(string tabName)
        {
            return this.tabItemRts.FirstOrDefault((item) => item.Item.Name == tabName);
        }
        public TabItemRuntime AddTab(TabItem tabItem)
        {
            var itemRt = this.Control.CreateTab(tabItem);
            this.tabItems.Add(tabItem);
            this.tabItemRts.Add(itemRt);

            if (tabItem.ButtonGroups == null || tabItem.ButtonGroups.Count == 0)
                return itemRt;

            itemRt.ButtonGroupRts = new List<TabButtonGroupRuntime>();

            for (var i = 0; i < tabItem.ButtonGroups.Count; i++)
            {
                var btnGroup = tabItem.ButtonGroups[i];
                var btnGroupRt = itemRt.TabPanel.CreateButtonGroup(btnGroup);
                itemRt.ButtonGroupRts.Add(btnGroupRt);
                //btnGroupCtrl.ButtonControls = new List<TabButtonControl>();
                //for (var j = 0; j < btnGroup.Buttons.Count; j++)
                //{

                //}
            }
            return itemRt;
        }

        public void RemoveTab(TabItem item)
        {

        }
        public void SetTabVisible(TabItem item, bool visible)
        {

        }
        public TabItem GetSelectedTab()
        {
            return null;
        }
        public void SetTabSelected(TabItem item)
        {
            this.Control.HeaderBar.SelectedItem = item.Name;
            this.Control.RibbonTab.SelectTab(item.Name);
        }

    }
}
