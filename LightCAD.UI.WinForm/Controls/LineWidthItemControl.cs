﻿using LightCAD.Core;
using netDxf.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Controls
{
    public partial class LineWidthItemControl : UserControl
    {
        public event EventHandler<string> ItemClick;
        public LineWeight LineWidth { get; set; }
        public LineWidthItemControl()
        {
            InitializeComponent();
        }

        public LineWidthItemControl(LineWeight linewidth)
        {
            InitializeComponent();
            this.LineWidth = linewidth;
            this.lblLineWidth.Text = GetLineWidthName(linewidth);
            this.MouseLeave += (s, e) => {
                this.lblLineWidth.BackColor = Color.White;
            };
            this.MouseHover += (s, e) => {
                this.lblLineWidth.BackColor = Color.LightBlue;
            };
            foreach (Control c in this.Controls)
            {
                c.MouseHover += C_MouseHover;
                c.MouseLeave += C_MouseLeave;
            }
        }

        private void C_MouseLeave(object? sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }

        private void C_MouseHover(object? sender, EventArgs e)
        {
            this.OnMouseHover(e);
        }

        public void Reset(LineWeight linewidth, List<LcElement> lcElement)
        {
            if (lcElement != null && lcElement.Count > 0)
            {
                foreach (var element in lcElement)
                {
                    element.LineWeight = linewidth.ToString();
                }
            }
            this.LineWidth = linewidth;
            this.lblLineWidth.Text = GetLineWidthName(linewidth);
        }

        private void lblLineWidth_Click(object sender, EventArgs e)
        {
            this.ItemClick?.Invoke(this.LineWidth, null);
        }

        public string GetLineWidthName(LineWeight linewidth)
        {
            string linewidthname = "ByLayer";
            try
            {
                switch (linewidth)
                {
                    case LineWeight.ByLayer:
                        linewidthname = ("ByLayer");
                        break;
                    case LineWeight.ByBlock:
                        linewidthname = ("ByBlock");
                        break;
                    case LineWeight.Default:
                        linewidthname = ("默认");
                        break;
                    case LineWeight.LW000:
                        linewidthname = ("0.00mm");
                        break;
                    case LineWeight.LW005:
                        linewidthname = ("0.05mm");
                        break;
                    case LineWeight.LW009:
                        linewidthname = ("0.09mm");
                        break;
                    case LineWeight.LW013:
                        linewidthname = ("0.13mm");
                        break;
                    case LineWeight.LW015:
                        linewidthname = ("0.15mm");
                        break;
                    case LineWeight.LW018:
                        linewidthname = ("0.18mm");
                        break;
                    case LineWeight.LW020:
                        linewidthname = ("0.20mm");
                        break;
                    case LineWeight.LW025:
                        linewidthname = ("0.25mm");
                        break;
                    case LineWeight.LW030:
                        linewidthname = ("0.30mm");
                        break;
                    case LineWeight.LW035:
                        linewidthname = ("0.35mm");
                        break;
                    case LineWeight.LW040:
                        linewidthname = ("0.40mm");
                        break;
                    case LineWeight.LW050:
                        linewidthname = ("0.50mm");
                        break;
                    case LineWeight.LW053:
                        linewidthname = ("0.53mm");
                        break;
                    case LineWeight.LW060:
                        linewidthname = ("0.60mm");
                        break;
                    case LineWeight.LW070:
                        linewidthname = ("0.70mm");
                        break;
                    case LineWeight.LW080:
                        linewidthname = ("0.80mm");
                        break;
                    case LineWeight.LW090:
                        linewidthname = ("0.90mm");
                        break;
                    case LineWeight.LW100:
                        linewidthname = ("1.00mm");
                        break;
                    case LineWeight.LW106:
                        linewidthname = ("1.06mm");
                        break;
                    case LineWeight.LW120:
                        linewidthname = ("1.20mm");
                        break;
                    case LineWeight.LW140:
                        linewidthname = ("1.40mm");
                        break;
                    case LineWeight.LW158:
                        linewidthname = ("1.58mm");
                        break;
                    case LineWeight.LW200:
                        linewidthname = ("2.00mm");
                        break;
                    case LineWeight.LW211:
                        linewidthname = ("2.11mm");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                return linewidthname;
            }
            return linewidthname;
        }



        //private void InitializeComponent()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // LineWidthItemControl
        //    // 
        //    this.Name = "LineWidthItemControl";
        //    this.Size = new System.Drawing.Size(250, 30);
        //    this.ResumeLayout(false);
        //}
    }
}
