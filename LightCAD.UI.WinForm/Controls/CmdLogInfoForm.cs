﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class CmdLogInfoForm : Form, ICmdLogControl
    {
        public CmdLogInfo Host { get; }

        public CmdLogInfoForm()
        {
            InitializeComponent();
        }
        public CmdLogInfoForm(CmdLogInfo host)
        {
            InitializeComponent();
            this.Host = host;
        }

        public void SetLog(string log)
        {
            this.txtLog.Clear();
            this.txtLog.Text = log;
        }

        public string GetLog()
        {
            return this.txtLog.Text;
        }
    }
}
