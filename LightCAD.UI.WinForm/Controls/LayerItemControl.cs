﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static LightCAD.UI.DropDownControl;
using LightCAD.Core;

namespace LightCAD.UI
{
    public partial class LayerItemControl : UserControl
    {
        public event EventHandler<string> ItemClick;
        public LcLayer Layer { get; set; }
        private bool IsLock = false;
        public LayerItemControl()
        {
            InitializeComponent();
        }

        public LayerItemControl(LcLayer layer, bool isLock = false)
        {
            InitializeComponent();
            IsLock = isLock;
            this.Layer = layer;
            this.picOpenClose.Image = Layer.IsOff ? Properties.Resources.LayerClose : Properties.Resources.LayerOpen;
            this.picFreeFrozen.Image = this.Layer.IsFrozen ? Properties.Resources.LayerFrozen : Properties.Resources.LayerFree;
            this.picUnlockLocked.Image = this.Layer.IsLocked ? Properties.Resources.LayerLocked : Properties.Resources.LayerUnlock;
            Bitmap bmp = new Bitmap(20, 20);
            Graphics g = Graphics.FromImage(bmp);
            UInt32 uintValue = Convert.ToUInt32(this.Layer.Color);
            SolidBrush b = new SolidBrush(GetColorByUint(uintValue));//这里修改颜色
            g.FillRectangle(b, 0, 0, 20, 20);
            System.Drawing.Rectangle rec = new Rectangle(1, 1, 20, 20);
            g.DrawRectangle(new Pen(System.Drawing.Color.Black, 1), 0, 0, 19, 19);
            this.picColor.Image = bmp;
            this.lblName.Text = Layer.Name;
            if (!isLock)
            {
                this.MouseLeave += (s, e) =>
                {
                    this.lblName.BackColor = Color.White;
                };
                this.MouseHover += (s, e) =>
                {
                    this.lblName.BackColor = Color.LightBlue;
                };
                foreach (Control c in this.Controls)
                {
                    c.MouseHover += C_MouseHover;
                    c.MouseLeave += C_MouseLeave;
                }
            }            
        }
    
        private void C_MouseLeave(object? sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }

        private void C_MouseHover(object? sender, EventArgs e)
        {
            this.OnMouseHover(e);
        }

        public void MouseHoverChanged(bool flag)
        {
            this.lblName.BackColor = flag ? Color.Blue : Color.White;
        }
        public void Reset(LcLayer layer, List<LcElement> lcElement)
        {
            if (lcElement != null && lcElement.Count > 0)
            {
                foreach (var element in lcElement)
                {
                    element.Layer = layer.Name;
                }
            }
            this.Layer = layer;
            this.picOpenClose.Image = Layer.IsOff ? Properties.Resources.LayerClose : Properties.Resources.LayerOpen;
            this.picFreeFrozen.Image = this.Layer.IsFrozen ? Properties.Resources.LayerFrozen : Properties.Resources.LayerFree;
            this.picUnlockLocked.Image = this.Layer.IsLocked ? Properties.Resources.LayerLocked : Properties.Resources.LayerUnlock;
            Bitmap bmp = new Bitmap(20, 20);
            Graphics g = Graphics.FromImage(bmp);
            UInt32 uintValue = Convert.ToUInt32(this.Layer.Color);
            SolidBrush b = new SolidBrush(GetColorByUint(uintValue));//这里修改颜色
            g.FillRectangle(b, 0, 0, 20, 20);
            System.Drawing.Rectangle rec = new Rectangle(1, 1, 20, 20);
            g.DrawRectangle(new Pen(System.Drawing.Color.Black, 1), 0, 0, 19, 19);
            this.picColor.Image = bmp;
            this.lblName.Text = Layer.Name;
        }
        private void picOpenClose_Click(object sender, EventArgs e)
        {
            if (IsLock)
            {
                this.ItemClick?.Invoke(this.Layer, "Off");
                return;
            }
            this.Layer.IsOff = !this.Layer.IsOff;
            picOpenClose.Image = this.Layer.IsOff ? Properties.Resources.LayerClose : Properties.Resources.LayerOpen;
            this.ItemClick?.Invoke(this.Layer, "Off");
        }

        private void picFreeFrozen_Click(object sender, EventArgs e)
        {
            if (IsLock)
            {
                this.ItemClick?.Invoke(this.Layer, "Frozen");
                return;
            }
            this.Layer.IsFrozen = !this.Layer.IsFrozen;
            picFreeFrozen.Image = this.Layer.IsFrozen ? Properties.Resources.LayerFrozen : Properties.Resources.LayerFree;
            this.ItemClick?.Invoke(this.Layer, "Frozen");
        }

        private void picVportFreeFrozen_Click(object sender, EventArgs e)
        {

        }

        private void picUnlockLocked_Click(object sender, EventArgs e)
        {
            if (IsLock)
            {
                this.ItemClick?.Invoke(this.Layer, "Lock");
                return;
            }
            this.Layer.IsLocked = !this.Layer.IsLocked;
            picUnlockLocked.Image = this.Layer.IsLocked ? Properties.Resources.LayerLocked : Properties.Resources.LayerUnlock;
            this.ItemClick?.Invoke(this.Layer, "Lock");
        }

        private void picColor_Click(object sender, EventArgs e)
        {
            if (IsLock)
            {
                this.ItemClick?.Invoke(this.Layer, "Color");
                return;
            }
            this.Layer.IsLocked = !this.Layer.IsLocked;

            this.ItemClick?.Invoke(this.Layer, "Color");
        }
        private Color GetColorByUint(uint uintValue)
        {
            Color colorConverted = System.Drawing.Color.FromArgb((byte)((uintValue >> 24) & 255), (ushort)((uintValue >> 16) & 255), (ushort)((uintValue >> 8) & 255), (ushort)(uintValue & 255));
            return colorConverted;
        }
        private void lblName_Click(object sender, EventArgs e)
        {
            if (IsLock)
            {
                this.ItemClick?.Invoke(this.Layer, "Name");
                return;
            }
            this.ItemClick?.Invoke(this.Layer, "Name");
        }     
    }
}
