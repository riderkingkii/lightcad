﻿namespace LightCAD.UI
{
    partial class HeaderMenuControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Menu = new System.Windows.Forms.MenuStrip();
            btnMenuCollapser = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // Menu
            // 
            Menu.BackColor = System.Drawing.Color.Transparent;
            Menu.Dock = System.Windows.Forms.DockStyle.None;
            Menu.GripMargin = new System.Windows.Forms.Padding(0);
            Menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            Menu.Location = new System.Drawing.Point(0, 0);
            Menu.Name = "Menu";
            Menu.Padding = new System.Windows.Forms.Padding(6, 6, 0, 0);
            Menu.Size = new System.Drawing.Size(158, 30);
            Menu.TabIndex = 0;
            Menu.Text = "menuStrip1";
            // 
            // btnMenuCollapser
            // 
            btnMenuCollapser.BackColor = System.Drawing.Color.Transparent;
            btnMenuCollapser.Dock = System.Windows.Forms.DockStyle.Right;
            btnMenuCollapser.FlatAppearance.BorderSize = 0;
            btnMenuCollapser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnMenuCollapser.Image = Properties.Resources.Collapse;
            btnMenuCollapser.Location = new System.Drawing.Point(464, 0);
            btnMenuCollapser.Margin = new System.Windows.Forms.Padding(0);
            btnMenuCollapser.Name = "btnMenuCollapser";
            btnMenuCollapser.Size = new System.Drawing.Size(36, 32);
            btnMenuCollapser.TabIndex = 1;
            btnMenuCollapser.UseVisualStyleBackColor = false;
            btnMenuCollapser.Click += btnMenuCollapser_Click;
            // 
            // HeaderMenuControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.Transparent;
            Controls.Add(btnMenuCollapser);
            Controls.Add(Menu);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "HeaderMenuControl";
            Size = new System.Drawing.Size(500, 32);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.Button btnMenuCollapser;
    }
}
