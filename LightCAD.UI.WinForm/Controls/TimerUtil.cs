﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.UI
{
    public class TimerTask
    {
        public Task Task { get; set; }
        public CancellationTokenSource CTS { get; set; }

        public void Cancel()
        {
            CTS.Cancel();
        }
    }
    public static class TimerUtil
    {
     
        public static TimerTask SetTimeout(Action action, int interval)
        {
            var cts = new CancellationTokenSource();
            var task = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(interval);
                if (cts.IsCancellationRequested) return;
                action();
            }, cts.Token);
            return new TimerTask { Task = task, CTS = cts };
        }
    }
}
