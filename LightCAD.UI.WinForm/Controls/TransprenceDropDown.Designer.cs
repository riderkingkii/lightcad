﻿namespace LightCAD.UI.Controls
{
    partial class TransprenceDropDown
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {

            transparenceItemControl = new TransparenceItemControl();
            itemsPanel = new System.Windows.Forms.FlowLayoutPanel();
            txtTransparenceValue = new System.Windows.Forms.TextBox();
            lbltransparence = new System.Windows.Forms.Label();
            lblvalue = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // lblvalue
            // 
            lblvalue.AutoSize = true;
            lblvalue.BackColor = System.Drawing.Color.Transparent;
            lblvalue.Location = new System.Drawing.Point(248, 3);
            lblvalue.Name = "lblvalue";
            lblvalue.Size = new System.Drawing.Size(54, 17);
            lblvalue.TabIndex = 3;
            lblvalue.Text = "(0 ~ 90)";
            // 
            // transparenceItemControl
            // 
            transparenceItemControl.BackColor = System.Drawing.Color.Transparent;
            transparenceItemControl.Location = new System.Drawing.Point(3, 0);
            transparenceItemControl.Name = "transparenceItemControl";
            transparenceItemControl.Size = new System.Drawing.Size(85, 24);
            transparenceItemControl.TabIndex = 0;
            // 
            // itemsPanel
            // 
            itemsPanel.Location = new System.Drawing.Point(3, 24);
            itemsPanel.Name = "itemsPanel";
            itemsPanel.Size = new System.Drawing.Size(85, 150);
            itemsPanel.TabIndex = 4;
            // 
            // txtTransparenceValue
            // 
            txtTransparenceValue.Location = new System.Drawing.Point(142, 2);
            txtTransparenceValue.Name = "txtTransparenceValue";
            txtTransparenceValue.Size = new System.Drawing.Size(100, 23);
            txtTransparenceValue.TabIndex = 2;
            // 
            // lbltransparence
            // 
            lbltransparence.BackColor = System.Drawing.Color.Transparent;
            lbltransparence.Location = new System.Drawing.Point(92, 5);
            lbltransparence.Name = "lbltransparence";
            lbltransparence.Size = new System.Drawing.Size(44, 20);
            lbltransparence.TabIndex = 1;
            lbltransparence.Text = "透明度";
            // 
            // TranspranceDropDown
            // 
            AnchorSize = new System.Drawing.Size(328, 24);
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            Controls.Add(lbltransparence);
            Controls.Add(lblvalue);
            Controls.Add(txtTransparenceValue);
            Controls.Add(itemsPanel);
            Controls.Add(transparenceItemControl);
            Name = "TranspranceDropDown";
            Size = new System.Drawing.Size(328, 174);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TransparenceItemControl transparenceItemControl;
        private System.Windows.Forms.FlowLayoutPanel itemsPanel;
        private System.Windows.Forms.TextBox txtTransparenceValue;
        private System.Windows.Forms.Label lblvalue;
        private System.Windows.Forms.Label lbltransparence;
    }
}
