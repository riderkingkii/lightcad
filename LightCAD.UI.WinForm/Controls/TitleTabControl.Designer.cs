﻿namespace LightCAD.UI
{
    partial class TitleTabControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            pnlTabs = new System.Windows.Forms.Panel();
            panel1 = new System.Windows.Forms.Panel();
            picAdd = new System.Windows.Forms.PictureBox();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)picAdd).BeginInit();
            SuspendLayout();
            // 
            // pnlTabs
            // 
            pnlTabs.AutoSize = true;
            pnlTabs.Dock = System.Windows.Forms.DockStyle.Left;
            pnlTabs.Location = new System.Drawing.Point(0, 0);
            pnlTabs.Name = "pnlTabs";
            pnlTabs.Size = new System.Drawing.Size(0, 34);
            pnlTabs.TabIndex = 1;
            // 
            // panel1
            // 
            panel1.Controls.Add(picAdd);
            panel1.Dock = System.Windows.Forms.DockStyle.Left;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(34, 34);
            panel1.TabIndex = 2;
            // 
            // picAdd
            // 
            picAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            picAdd.Image = Properties.TabResources.Add;
            picAdd.Location = new System.Drawing.Point(0, 0);
            picAdd.Name = "picAdd";
            picAdd.Size = new System.Drawing.Size(34, 34);
            picAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            picAdd.TabIndex = 0;
            picAdd.TabStop = false;
            // 
            // TitleTabControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            AutoSize = true;
            Controls.Add(panel1);
            Controls.Add(pnlTabs);
            Name = "TitleTabControl";
            Size = new System.Drawing.Size(795, 34);
            panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)picAdd).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.Panel pnlTabs;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picAdd;
    }
}
