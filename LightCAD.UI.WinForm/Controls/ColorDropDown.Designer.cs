﻿using netDxf;
using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI
{
    partial class ColorDropDown
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            picColorPalette = new PictureBox();
            itemsPanel = new FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)picColorPalette).BeginInit();
            SuspendLayout();
            // 
            // picColorPalette
            // 
            picColorPalette.Image = Properties.Resources.Color;
            picColorPalette.Location = new Point(2, 3);
            picColorPalette.Margin = new Padding(2, 3, 2, 3);
            picColorPalette.Name = "picColorPalette";
            picColorPalette.Size = new Size(19, 20);
            picColorPalette.TabIndex = 0;
            picColorPalette.TabStop = false;
            // 
            // itemsPanel
            // 
            itemsPanel.AutoScroll = true;
            itemsPanel.FlowDirection = FlowDirection.TopDown;
            itemsPanel.Location = new Point(23, 27);
            itemsPanel.Margin = new Padding(2, 3, 2, 3);
            itemsPanel.Name = "itemsPanel";
            itemsPanel.Size = new Size(183, 161);
            itemsPanel.TabIndex = 0;
            itemsPanel.WrapContents = false;
            // 
            // ColorDropDown
            // 
            AnchorSize = new Size(208, 24);
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(picColorPalette);
            Controls.Add(itemsPanel);
            ForeColor = Color.Black;
            Margin = new Padding(3, 4, 3, 4);
            Name = "ColorDropDown";
            Size = new Size(208, 184);
            ((System.ComponentModel.ISupportInitialize)picColorPalette).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private PictureBox picColorPalette;
        private FlowLayoutPanel itemsPanel;
    }
}
