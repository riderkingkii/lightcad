﻿namespace LightCAD.UI
{
    partial class PopupInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblInfo = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // lblInfo
            // 
            lblInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            lblInfo.Location = new System.Drawing.Point(0, 0);
            lblInfo.Name = "lblInfo";
            lblInfo.Size = new System.Drawing.Size(260, 79);
            lblInfo.TabIndex = 0;
            lblInfo.Text = "INFO";
            lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PopupInfoForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(260, 79);
            Controls.Add(lblInfo);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Name = "PopupInfoForm";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            Text = "PopupInfoForm";
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Label lblInfo;
    }
}