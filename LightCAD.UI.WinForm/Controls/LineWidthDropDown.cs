﻿using Avalonia.Controls;
using CefSharp.DevTools.CSS;
using LightCAD.Core;
using LightCAD.Runtime;
using LightCAD.UI.Controls;
using netDxf.Tables;
using Svg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class LineWidthDropDown : DropDownControl
    {
        private LcDocument lcDocument;
        private LineWeight selectlinewidth;
        private LineWidthItemControl linewidthItemControl;
        private List<LcElement> lcElementsList = new List<LcElement>();

        // 外部获取选中线宽
        public string SelectItem { get { return selectlinewidth.ToString(); } }

        public LineWidthDropDown()
        {
            InitializeComponent();
            InitializeDropDown(itemsPanel);
        }
        public LineWidthDropDown(LcDocument lcDocument) : this()
        {
            this.lcDocument = lcDocument;
            linewidthItemControl = new LineWidthItemControl(selectlinewidth);
            linewidthItemControl.Location = new Point(23, 0);
            linewidthItemControl.Name = "layerItemControl";
            linewidthItemControl.Size = new Size(155, 24);
            linewidthItemControl.TabIndex = 0;
            linewidthItemControl.ItemClick += LineWidthItemControl_Click;
            this.Controls.Add(linewidthItemControl);
            InitLineWidth(lcDocument);
        }


        private void LineWidthItemControl_Click(object? sender, string e)
        {
            OpenDropDown();
        }

        public void ResetSelectLineWidth(SelectedEventArgs args)
        {
            this.linewidthItemControl.Visible = true;
            if (!args.Selected)
            {
                lcElementsList.Clear();
                this.linewidthItemControl.Reset(selectlinewidth, null);
            }
            else
            {
                var lcElements = args.Elements;
                lcElementsList.AddRange(lcElements);
                if (lcElementsList.GroupBy(n => n.Layer).Count() > 1)
                {
                    selectlinewidth = LineWeight.Default;
                    this.linewidthItemControl.Visible = false;
                }
                else
                {
                    //var selectLayer = lcDocument.Layers.First(n => n.Name == lcElements.First().Layer);
                    selectlinewidth = GetLineType(lcElements.First().LineWeight);
                    this.linewidthItemControl.Reset(selectlinewidth, null);

                }
            }
        }

        public void InitLineWidth(LcDocument lcDocument)
        {
            itemsPanel.Controls.Clear();
            foreach (int myCode in Enum.GetValues(typeof(LineWeight)))
            {
                LineWeight enumvalue = new LineWeight();
                string strName = Enum.GetName(typeof(LineWeight), myCode);//获取名称
                switch (strName)
                {
                    case "ByLayer":
                        enumvalue = LineWeight.ByLayer;
                        break;
                    case "ByBlock":
                        enumvalue = LineWeight.ByBlock;
                        break;
                    case "Default":
                        enumvalue = LineWeight.Default;
                        break;
                    case "LW000":
                        enumvalue = LineWeight.LW000;
                        break;
                    case "LW005":
                        enumvalue = LineWeight.LW005;
                        break;
                    case "LW009":
                        enumvalue = LineWeight.LW009;
                        break;
                    case "LW013":
                        enumvalue = LineWeight.LW013;
                        break;
                    case "LW015":
                        enumvalue = LineWeight.LW015;
                        break;
                    case "LW018":
                        enumvalue = LineWeight.LW018;
                        break;
                    case "LW020":
                        enumvalue = LineWeight.LW020;
                        break;
                    case "LW025":
                        enumvalue = LineWeight.LW025;
                        break;
                    case "LW030":
                        enumvalue = LineWeight.LW030;
                        break;
                    case "LW035":
                        enumvalue = LineWeight.LW035;
                        break;
                    case "LW040":
                        enumvalue = LineWeight.LW040;
                        break;
                    case "LW050":
                        enumvalue = LineWeight.LW050;
                        break;
                    case "LW053":
                        enumvalue = LineWeight.LW053;
                        break;
                    case "LW060":
                        enumvalue = LineWeight.LW060;
                        break;
                    case "LW070":
                        enumvalue = LineWeight.LW070;
                        break;
                    case "LW080":
                        enumvalue = LineWeight.LW080;
                        break;
                    case "LW090":
                        enumvalue = LineWeight.LW090;
                        break;
                    case "LW100":
                        enumvalue = LineWeight.LW100;
                        break;
                    case "LW106":
                        enumvalue = LineWeight.LW106;
                        break;
                    case "LW120":
                        enumvalue = LineWeight.LW120;
                        break;
                    case "LW140":
                        enumvalue = LineWeight.LW140;
                        break;
                    case "LW158":
                        enumvalue = LineWeight.LW158;
                        break;
                    case "LW200":
                        enumvalue = LineWeight.LW200;
                        break;
                    case "LW211":
                        enumvalue = LineWeight.LW211;
                        break;
                    default:
                        break;
                }
                LineWidthItemControl linetype = new LineWidthItemControl(enumvalue);
                linetype.ItemClick += LineWidthItem_ItemClick;
                itemsPanel.Controls.Add(linetype);
            }
            this.Refresh();
        }

        private void LineWidthItem_ItemClick(object? sender, string e)
        {
            if (sender != null && sender is LineWeight)
            {
                selectlinewidth = (LineWeight)sender;
                this.linewidthItemControl.Reset(selectlinewidth, lcElementsList);

                ItemClickEvent(SelectItem);
            }
        }

        public LineWeight GetLineType(string strName)
        {
            var linewidth = LineWeight.Default;
            switch (strName)
            {
                case "ByLayer":
                    linewidth = LineWeight.ByLayer;
                    break;
                case "ByBlock":
                    linewidth = LineWeight.ByBlock;
                    break;
                case "Default":
                    linewidth = LineWeight.Default;
                    break;
                case "LW000":
                    linewidth = LineWeight.LW000;
                    break;
                case "LW005":
                    linewidth = LineWeight.LW005;
                    break;
                case "LW009":
                    linewidth = LineWeight.LW009;
                    break;
                case "LW013":
                    linewidth = LineWeight.LW013;
                    break;
                case "LW015":
                    linewidth = LineWeight.LW015;
                    break;
                case "LW018":
                    linewidth = LineWeight.LW018;
                    break;
                case "LW020":
                    linewidth = LineWeight.LW020;
                    break;
                case "LW025":
                    linewidth = LineWeight.LW025;
                    break;
                case "LW030":
                    linewidth = LineWeight.LW030;
                    break;
                case "LW035":
                    linewidth = LineWeight.LW035;
                    break;
                case "LW040":
                    linewidth = LineWeight.LW040;
                    break;
                case "LW050":
                    linewidth = LineWeight.LW050;
                    break;
                case "LW053":
                    linewidth = LineWeight.LW053;
                    break;
                case "LW060":
                    linewidth = LineWeight.LW060;
                    break;
                case "LW070":
                    linewidth = LineWeight.LW070;
                    break;
                case "LW080":
                    linewidth = LineWeight.LW080;
                    break;
                case "LW090":
                    linewidth = LineWeight.LW090;
                    break;
                case "LW100":
                    linewidth = LineWeight.LW100;
                    break;
                case "LW106":
                    linewidth = LineWeight.LW106;
                    break;
                case "LW120":
                    linewidth = LineWeight.LW120;
                    break;
                case "LW140":
                    linewidth = LineWeight.LW140;
                    break;
                case "LW158":
                    linewidth = LineWeight.LW158;
                    break;
                case "LW200":
                    linewidth = LineWeight.LW200;
                    break;
                case "LW211":
                    linewidth = LineWeight.LW211;
                    break;
                default:
                    break;
            }
            return linewidth;
        }

    }
}
