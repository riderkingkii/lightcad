﻿using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI
{
    partial class LineTypeDropDown
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            picLineType = new PictureBox();
            this.itemsPanel = new FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)picLineType).BeginInit();
            this.SuspendLayout();
            // 
            // picLineType
            // 
            picLineType.Image = Properties.Resources.LineType;
            picLineType.BorderStyle = BorderStyle.None;
            picLineType.Location = new Point(2, 3);
            picLineType.Margin = new Padding(2, 3, 2, 3);
            picLineType.Name = "picLineType";
            picLineType.Size = new Size(19, 20);
            picLineType.TabStop = false;
            // 
            // itemsPanel
            // 
            this.itemsPanel.AutoScroll = true;
            this.itemsPanel.FlowDirection = FlowDirection.TopDown;
            this.itemsPanel.Location = new Point(23, 27);
            this.itemsPanel.Margin = new Padding(2, 3, 2, 3);
            this.itemsPanel.Name = "itemsPanel";
            this.itemsPanel.Size = new Size(183, 161);
            this.itemsPanel.TabIndex = 0;
            this.itemsPanel.WrapContents = false;
            // 
            // LineTypeDropDown
            // 
            this.AnchorSize = new Size(208, 24);
            this.AutoScaleDimensions = new SizeF(7F, 17F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add(picLineType);
            this.Controls.Add(this.itemsPanel);
            this.ForeColor = Color.Black;
            this.Margin = new Padding(3, 4, 3, 4);
            this.Name = "LineTypeDropDown";
            this.Size = new Size(208, 184);
            ((System.ComponentModel.ISupportInitialize)picLineType).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private PictureBox picLineType;
        private FlowLayoutPanel itemsPanel;
    }
}
