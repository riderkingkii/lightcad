﻿namespace LightCAD.UI.Controls
{
    partial class TransparenceItemControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            picTransparence = new System.Windows.Forms.PictureBox();
            lblName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)picTransparence).BeginInit();
            SuspendLayout();
            // 
            // picTransparence
            //
            picTransparence.Image = Properties.Resources.LayerManage;
            picTransparence.Location = new System.Drawing.Point(2, 2);
            picTransparence.Name = "picTransparence";
            picTransparence.Size = new System.Drawing.Size(20, 20);
            picTransparence.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            picTransparence.TabIndex = 0;
            picTransparence.TabStop = false;
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            lblName.Location = new System.Drawing.Point(28, 2);
            lblName.Name = "labelName";
            lblName.Size = new System.Drawing.Size(50, 20);
            lblName.TabIndex = 1;
            lblName.Text = "label1";
            lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblName.Click += labelName_Click;
            // 
            // TransparenceItemControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(lblName);
            Controls.Add(picTransparence);
            Name = "TransparenceItemControl";
            Size = new System.Drawing.Size(85, 24);
            Load += TransparenceItemControl_Load;
            ((System.ComponentModel.ISupportInitialize)picTransparence).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.PictureBox picTransparence;
        private System.Windows.Forms.Label lblName;
    }
}
