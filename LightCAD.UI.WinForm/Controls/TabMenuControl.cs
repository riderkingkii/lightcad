﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class TabMenuControl : UserControl
    {
        public event EventHandler ItemClick;

        private bool isRibbonVisible = true;
        public TabMenuManager TabMgr { get; set; }
        public TabMenuControl()
        {
            InitializeComponent();

            this.Load += TabMenuControl_Load;
            this.HeaderBar.ItemClick += HeaderBar_ItemClick;
            this.HeaderBar.ItemDropDown += HeaderBar_ItemDropDown;
            this.HeaderBar.CollapserClick += HeaderBar_CollapserClick;
            this.RibbonTab.ItemClick += RibbonTab_ItemClick;
        }

        private void RibbonTab_ItemClick(object? sender, EventArgs e)
        {
            this.ItemClick?.Invoke(sender, e);
        }

        private void TabMenuControl_Load(object sender, EventArgs e)
        {

            this.HeaderBar.Height = 32;
            this.RibbonTab.Appearance = TabAppearance.FlatButtons;
            this.RibbonTab.ItemSize = new Size(60, 0);
            this.RibbonTab.SizeMode = TabSizeMode.Fixed;
            this.RibbonTab.Height = 64;
        }
        private void HeaderBar_ItemDropDown(object sender, EventArgs e)
        {
            var headerItem = sender as HeaderItem;
            if (isRibbonVisible)
            {
                if (headerItem.Tag == null)
                {
                    headerItem.Tag = headerItem.DropDown;
                }
                headerItem.DropDown = null;
            }
            else
            {
                if (headerItem.Tag != null)
                {
                    headerItem.DropDown = (ToolStripDropDown)headerItem.Tag;
                }
            }
        }
        private void HeaderBar_ItemClick(object sender, EventArgs e)
        {
            var headerItem = sender as HeaderItem;
            if (isRibbonVisible)
            {
                this.RibbonTab.SelectTab(headerItem.Name);
            }

        }



        private void HeaderBar_CollapserClick(object sender, EventArgs e)
        {
            var btn = sender as Button;
            if (isRibbonVisible)
            {
                this.RibbonTab.Visible = false;
                isRibbonVisible = false;
                btn.Image = Properties.Resources.Expand;
                this.Height = this.HeaderBar.Height;
            }
            else
            {
                this.RibbonTab.Visible = true;
                isRibbonVisible = true;
                btn.Image = Properties.Resources.Collapse;
                this.Height = this.HeaderBar.Height + this.RibbonTab.Height;
            }
        }

        internal TabItemRuntime CreateTab(TabItem item)
        {
            var itemRt = new TabItemRuntime();
            itemRt.Item = item;
            itemRt.Header = this.HeaderBar.AddItem(item.Name, item.Text, item.ShortcutKey);
            itemRt.TabPanel = this.RibbonTab.AddPage(item.Name, item.Text);
            return itemRt;
        }

        internal void CreateTabMenu(IHeaderItem headerItem)
        {
            var hitem = headerItem as HeaderItem;
            var tab = this.TabMgr.GetTab(hitem.Name);
            foreach (var btnGrp in tab.ButtonGroups)
            {
                var freqUseds = GetFreqUseds(btnGrp);
                foreach (var fbtn in freqUseds)
                {
                    var fmitem = new ToolStripMenuItem();
                    fmitem.Text = fbtn.Name;
                    hitem.DropDownItems.Add(fmitem);
                }
                var mitem = new ToolStripMenuItem();
                mitem.Text = btnGrp.Text;
                foreach (var btn in btnGrp.Buttons)
                {
                    if (freqUseds.Contains(btn)) continue;

                    var subMitem = new ToolStripMenuItem();
                    subMitem.Text = btn.Name;
                    mitem.DropDownItems.Add(subMitem);

                    if (btn.DropDowns.Count > 0)
                    {
                        foreach (var ddBtn in btn.DropDowns)
                        {
                            if (freqUseds.Contains(ddBtn)) continue;

                            var ssMitem = new ToolStripMenuItem();
                            ssMitem.Text = ddBtn.Name;
                            subMitem.DropDownItems.Add(ssMitem);
                        }
                    }
                }
                hitem.DropDownItems.Add(mitem);
            }
        }


        private List<TabButton> GetFreqUseds(TabButtonGroup btnGrp)
        {
            var list = new List<TabButton>();
            foreach (var btn in btnGrp.Buttons)
            {
                if (btn.IsFreqUsed)
                    list.Add(btn);
                if (btn.DropDowns.Count > 0)
                {
                    foreach (var ddBtn in btn.DropDowns)
                    {
                        if (ddBtn.IsFreqUsed)
                            list.Add(ddBtn);
                    }
                }
            }
            return list;
        }
    }
}
