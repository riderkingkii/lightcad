﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.UI
{
    public class TitleTabEventArgs : EventArgs
    {
        public string Type { get; set; }
        public TitleTabItem TabItem { get; set; }
        public TitleTabItemControl TabItemControl { get; set; }
        public bool IsCancel { get; set; }
    }
}
