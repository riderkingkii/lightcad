﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class HistoryCmdInfoForm : Form, IHistoryCmdControl
    {
        private Action<string> executeAction;
        public HistoryCmdInfo Host { get; }

        public HistoryCmdInfoForm()
        {
            InitializeComponent();
        }
        public HistoryCmdInfoForm(HistoryCmdInfo host)
        {
            InitializeComponent();
            this.Host = host;
        }

        private void lbCmds_MouseClick(object sender, MouseEventArgs e)
        {
            string cmd = (string)lbCmds.SelectedItem;
            if (cmd == null)
                return;

            if (this.executeAction != null)
            {
                executeAction(cmd);
            }
            this.Host.Hide();
        }

        public void AttachExecuteAction(Action<string> action)
        {
            this.executeAction = action;
        }
    }
}
