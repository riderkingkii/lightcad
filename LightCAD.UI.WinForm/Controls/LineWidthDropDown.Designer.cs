﻿using LightCAD.Core;
using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI
{
    partial class LineWidthDropDown
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            picLineWidth = new PictureBox();
            this.itemsPanel = new FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)picLineWidth).BeginInit();
            this.SuspendLayout();
            // 
            // picLineWidth
            // 
            picLineWidth.Image = Properties.Resources.LineWidth;
            picLineWidth.BorderStyle = BorderStyle.None;
            picLineWidth.Location = new Point(2, 3);
            picLineWidth.Margin = new Padding(2, 3, 2, 3);
            picLineWidth.Name = "picLineWidth";
            picLineWidth.Size = new Size(19, 20);
            picLineWidth.TabStop = false;
            // 
            // itemsPanel
            // 
            this.itemsPanel.AutoScroll = true;
            this.itemsPanel.FlowDirection = FlowDirection.TopDown;
            this.itemsPanel.Location = new Point(23, 27);
            this.itemsPanel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.itemsPanel.Name = "itemsPanel";
            this.itemsPanel.Size = new System.Drawing.Size(183, 235);
            this.itemsPanel.TabIndex = 0;
            this.itemsPanel.WrapContents = false;
            // 
            // LineWidthDropDown
            // 
            this.AnchorSize = new Size(208, 21);
            this.AutoScaleDimensions = new SizeF(7F, 20F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add(picLineWidth);
            this.Controls.Add(this.itemsPanel);
            this.ForeColor = Color.Black;
            this.Margin = new Padding(3, 4, 3, 4);
            this.Name = "LineWidthDropDown";
            this.Size = new System.Drawing.Size(208, 262);
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox picLineWidth;
        private FlowLayoutPanel itemsPanel;
    }
}
