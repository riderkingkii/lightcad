﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{

    public partial class HeaderMenuControl : UserControl
    {
        public event EventHandler ItemClick;
        public event EventHandler ItemDropDown;
        public event EventHandler CollapserClick;
        private string selectedItem;
        public HeaderMenuControl()
        {
            InitializeComponent();
            this.Load += HeaderMenuControl_Load;
        }

        private void HeaderMenuControl_Load(object sender, EventArgs e)
        {
            this.Menu.MaximumSize = new Size(this.Width, 32);
        }



        //private void Item_MouseEnter(object sender, EventArgs e)
        //{
        //    var item = (sender as HeaderItem);
        //    Debug.Print("MouseEnter");
        //    if (HeaderItem.IsMenuDropDown)
        //    {
        //        if (item.MenuStrip == null)
        //        {
        //            var tabMenu = this.Parent as TabMenuControl;
        //            item.MenuStrip = tabMenu.CreateTabMenu(item.Name);
        //        }
        //        item.MenuStrip?.Show(item, new Point(0, Height), ToolStripDropDownDirection.BelowRight);
        //    }

        //    if (item.Name == SelectedItem) return;
        //    item.BackColor = ThemeColors.LightDark.Color;

        //}

        //private void Label_MouseMove(object sender, MouseEventArgs e)
        //{
        //    Debug.Print("MouseMove");
        //}

        //private void Item_MouseLeave(object sender, EventArgs e)
        //{
        //    var item = (sender as Label);
        //    if (item.Name == SelectedItem) return;

        //    item.BackColor = ThemeColors.DarkHeader.Color;
        //}
        private void Item_Click(object sender, EventArgs e)
        {
            var item = (sender as HeaderItem);
            SetItemSelected(item.Name);
            this.ItemClick?.Invoke(item, EventArgs.Empty);
        }
        //private void Item_MouseClick(object sender, MouseEventArgs e)
        //{
        //    var item = (sender as HeaderItem);
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        this.ItemClick?.Invoke(item, EventArgs.Empty);
        //    }
        //}
        private void SetItemSelected(string selitem)
        {
            foreach (ToolStripMenuItem item in this.Menu.Items)
            {
                if (selitem == item.Name)
                {
                    item.BackColor = Color.WhiteSmoke;
                    this.selectedItem = selitem;
                }
                else
                {
                    item.BackColor = Color.White;
                }
            }
        }

        public string SelectedItem
        {
            get { return this.selectedItem; }
            set
            {
                this.selectedItem = value;
                this.SetItemSelected(value);
            }
        }

        private void btnMenuCollapser_Click(object sender, EventArgs e)
        {
            this.CollapserClick?.Invoke(sender, EventArgs.Empty);
        }

        public HeaderItem AddItem(string name, string text, string shortcutKey)
        {
            //var label = new HeaderItem();
            //label.Name = name;
            //label.BackColor = ThemeColors.DarkHeader.Color;
            //label.Text = text + (string.IsNullOrEmpty(altKey) ? string.Empty : $"({altKey})");
            //label.AutoSize = true;
            //label.Margin = new Padding(1, 0, 1, 0);
            //label.Padding = new Padding(10, 5, 10, 5);
            //label.ForeColor = ThemeColors.HeaderText.Color;
            //label.Dock = DockStyle.Left;

            //label.Click += Item_Click;
            //label.MouseEnter += Item_MouseEnter;
            //label.MouseLeave += Item_MouseLeave;
            //label.MouseClick += Item_MouseClick;
            //label.MouseMove += Label_MouseMove;

            var headerItem = new HeaderItem();
            headerItem.Name = name;

            var txtSufix = "";
            if (!string.IsNullOrEmpty(shortcutKey))
            {
                headerItem.ShortcutKeyDisplayString = shortcutKey;
                headerItem.ShowShortcutKeys = true;
                headerItem.ShortcutKeys = GetShortcutKeys(shortcutKey, out txtSufix);
            }
            headerItem.Text = text + txtSufix;
            headerItem.Click += Item_Click;
            headerItem.DropDownOpening += HeaderItem_DropDownOpening;
            headerItem.Height = 26;
            headerItem.Width = (text.Length == 2) ? 64 : 80;
            headerItem.AutoSize = false;
            if (this.Menu.Items.Count == 0)
                headerItem.Margin = new Padding(10, 0, 5, 0);
            else
                headerItem.Margin = new Padding(5, 0, 5, 0);

            this.Menu.Items.Add(headerItem);

            return headerItem;
        }

        private Keys GetShortcutKeys(string shortcutKey, out string txtSufix)
        {
            txtSufix = "";
            try
            {
                var parts = shortcutKey.Split('-');
                Enum.TryParse<Keys>(parts[0], true, out var key1);
                Enum.TryParse<Keys>(parts[1], true, out var key2);
                if (key2 != Keys.None)
                {
                    txtSufix = $"({key2})";
                }
                return key1 | key2;
            }
            catch
            {
                return Keys.None;
            }

        }

        private void HeaderItem_DropDownOpening(object sender, EventArgs e)
        {
            var headerItem = sender as HeaderItem;
            ItemDropDown?.Invoke(headerItem, EventArgs.Empty);
        }

        public void RemoveItem(HeaderItem headerItem)
        {
            this.Menu.Items.Remove(headerItem);
        }
    }
}
