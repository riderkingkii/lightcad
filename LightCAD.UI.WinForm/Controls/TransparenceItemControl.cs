﻿using LightCAD.Core;
using LightCAD.Drawing;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace LightCAD.UI.Controls
{
    public partial class TransparenceItemControl : UserControl
    {
        public event EventHandler<string> ItemClick;
        private string wayName;
        public TransparenceItemControl()
        {
            InitializeComponent();
        }

        public TransparenceItemControl(string wayName, bool isLook = false)
        {
            InitializeComponent();
            switch(wayName)
            {
                case ValueFrom.ByLayer:
                    this.picTransparence.Image = Properties.Resources.LayerManage;
                    this.lblName.Text = wayName;
                    break;
                case ValueFrom.ByBlock:
                    break;

            }
        }

        private void TransparenceItemControl_Load(object sender, EventArgs e)
        {

        }

        private void labelName_Click(object sender, EventArgs e)
        {

        }
    }
}
