﻿using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI
{
    partial class LayerItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {           
            picOpenClose = new PictureBox();
            picFreeFrozen = new PictureBox();
            picVportFreeFrozen = new PictureBox();
            picUnlockLocked = new PictureBox();
            picColor = new PictureBox();
            lblName = new Label();
            ((System.ComponentModel.ISupportInitialize)picOpenClose).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picFreeFrozen).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picVportFreeFrozen).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picUnlockLocked).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picColor).BeginInit();
            SuspendLayout();          
            // 
            // picOpenClose
            // 
            picOpenClose.Image = Properties.Resources.LayerOpen;
            picOpenClose.Location = new Point(1, 1);
            picOpenClose.Margin = new Padding(2, 3, 2, 3);
            picOpenClose.Name = "picOpenClose";
            picOpenClose.Size = new Size(19, 20);
            picOpenClose.SizeMode = PictureBoxSizeMode.CenterImage;
            picOpenClose.TabIndex = 1;
            picOpenClose.TabStop = false;
            picOpenClose.Click += picOpenClose_Click;
            // 
            // picFreeFrozen
            // 
            picFreeFrozen.Image = Properties.Resources.LayerFree;
            picFreeFrozen.Location = new Point(24, 1);
            picFreeFrozen.Margin = new Padding(2, 3, 2, 3);
            picFreeFrozen.Name = "picFreeFrozen";
            picFreeFrozen.Size = new Size(19, 20);
            picFreeFrozen.SizeMode = PictureBoxSizeMode.CenterImage;
            picFreeFrozen.TabIndex = 2;
            picFreeFrozen.TabStop = false;
            picFreeFrozen.Click += picFreeFrozen_Click;
            // 
            // picVportFreeFrozen
            // 
            picVportFreeFrozen.Image = Properties.Resources.LayerVportFree;
            picVportFreeFrozen.Location = new Point(47, 1);
            picVportFreeFrozen.Margin = new Padding(2, 3, 2, 3);
            picVportFreeFrozen.Name = "picVportFreeFrozen";
            picVportFreeFrozen.Size = new Size(19, 20);
            picVportFreeFrozen.SizeMode = PictureBoxSizeMode.CenterImage;
            picVportFreeFrozen.TabIndex = 3;
            picVportFreeFrozen.TabStop = false;
            picVportFreeFrozen.Click += picVportFreeFrozen_Click;
            // 
            // picUnlockLocked
            // 
            picUnlockLocked.Image = Properties.Resources.LayerUnlock;
            picUnlockLocked.Location = new Point(71, 1);
            picUnlockLocked.Margin = new Padding(2, 3, 2, 3);
            picUnlockLocked.Name = "picUnlockLocked";
            picUnlockLocked.Size = new Size(19, 20);
            picUnlockLocked.SizeMode = PictureBoxSizeMode.CenterImage;
            picUnlockLocked.TabIndex = 4;
            picUnlockLocked.TabStop = false;
            picUnlockLocked.Click += picUnlockLocked_Click;
            // 
            // picColor
            // 
            picColor.BackColor = Color.White;
            picColor.BorderStyle = BorderStyle.FixedSingle;
            picColor.Location = new Point(96, 3);
            picColor.Margin = new Padding(2, 3, 2, 3);
            picColor.Name = "picColor";
            picColor.Size = new Size(16, 17);
            picColor.TabIndex = 5;
            picColor.TabStop = false;
            picColor.Click += picColor_Click;
            // 
            // lblName
            // 
            lblName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblName.BackColor = Color.White;
            lblName.Location = new Point(117, 1);
            lblName.Margin = new Padding(2, 0, 2, 0);
            lblName.Name = "lblName";
            lblName.Size = new Size(159, 20);
            lblName.TabIndex = 6;
            lblName.Text = "0";
            lblName.TextAlign = ContentAlignment.MiddleLeft;
            lblName.Click += lblName_Click;
            // 
            // LayerItemControl
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(lblName);
            Controls.Add(picColor);
            Controls.Add(picUnlockLocked);
            Controls.Add(picVportFreeFrozen);
            Controls.Add(picFreeFrozen);
            Controls.Add(picOpenClose);
            ForeColor = Color.Black;
            Margin = new Padding(3, 4, 3, 4);
            Name = "LayerItemControl";
            Size = new Size(278, 22);
            ((System.ComponentModel.ISupportInitialize)picOpenClose).EndInit();
            ((System.ComponentModel.ISupportInitialize)picFreeFrozen).EndInit();
            ((System.ComponentModel.ISupportInitialize)picVportFreeFrozen).EndInit();
            ((System.ComponentModel.ISupportInitialize)picUnlockLocked).EndInit();
            ((System.ComponentModel.ISupportInitialize)picColor).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private PictureBox picOpenClose;
        private PictureBox picFreeFrozen;
        private PictureBox picVportFreeFrozen;
        private PictureBox picUnlockLocked;
        private PictureBox picColor;
        private Label lblName;
    }
}
