﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.UI
{
    public static class ThemeColors
    {
        public static SolidBrush DropDownArrow = new SolidBrush(Color.FromArgb(255, 217, 217, 217));
        public static SolidBrush HeaderText = new SolidBrush(Color.FromArgb(255, 245, 245, 245));
        public static SolidBrush DarkButton = new SolidBrush(Color.FromArgb(255, 39, 47, 58));
        public static SolidBrush DarkHeader = new SolidBrush(Color.FromArgb(255, 34, 41, 51));
        public static SolidBrush LightDark = new SolidBrush(Color.FromArgb(255, 59, 68, 83));
         public static SolidBrush GroupGap = new SolidBrush(Color.FromArgb(255, 0xD0, 0xD0, 0xD0));
   }
}
