﻿using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI
{
    partial class LayerDropDown
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnLayerManager = new Button();
            itemsPanel = new FlowLayoutPanel();
            SuspendLayout();
            // 
            // btnLayerManager
            // 
            btnLayerManager.FlatStyle = FlatStyle.Flat;
            btnLayerManager.FlatAppearance.BorderSize = 0;
            btnLayerManager.Image = Properties.Resources.LayerManage;
            btnLayerManager.Location = new Point(2, 2);
            btnLayerManager.Margin = new Padding(2, 3, 2, 3);
            btnLayerManager.Name = "btnLayerManager";
            btnLayerManager.Size = new Size(22, 22);
            btnLayerManager.TabIndex = 0;
            btnLayerManager.TabStop = false;
            // 
            // itemsPanel
            // 
            itemsPanel.AutoScroll = true;
            itemsPanel.FlowDirection = FlowDirection.TopDown;
            itemsPanel.Location = new Point(26, 23);
            itemsPanel.Margin = new Padding(2, 3, 2, 3);
            itemsPanel.Name = "itemsPanel";
            itemsPanel.Size = new Size(306, 137);
            itemsPanel.TabIndex = 1;
            itemsPanel.WrapContents = false;
            // 
            // LayerDropDown
            // 
            AnchorSize = new Size(334, 24);
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(btnLayerManager);
            Controls.Add(itemsPanel);
            ForeColor = Color.Black;
            Margin = new Padding(3, 4, 3, 4);
            Name = "LayerDropDown";
            Size = new Size(334, 156);
            ResumeLayout(false);
        }

        #endregion

        private Button btnLayerManager;
        private FlowLayoutPanel itemsPanel;
    }
}
