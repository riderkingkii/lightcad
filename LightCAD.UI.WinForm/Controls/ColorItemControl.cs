﻿using LightCAD.Core;
using LightCAD.Drawing;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class ColorItemControl : UserControl
    {
        public event EventHandler<string> ItemClick;
        private string colorName;
        public ColorItemControl()
        {
            InitializeComponent();
        }
        public ColorItemControl(string colorName, bool isLock = false, string byLayerColor = "")
        {
            InitializeComponent();
            Color sysColor = new Color();
            switch (colorName)
            {
                case ValueFrom.ByLayer:
                    if (byLayerColor != "")
                    {
                        var layerColor = Convert.ToUInt32(byLayerColor);
                        sysColor = GetColorByUint(layerColor);
                    }
                    else
                        sysColor = Color.White;
                    break;
                case ValueFrom.ByBlock:
                    sysColor = Color.Black;
                    break;
                case "红":
                    sysColor = Color.Red;
                    break;
                case "黄":
                    sysColor = Color.Yellow;
                    break;
                case "绿":
                    sysColor = Color.Lime;
                    break;
                case "青":
                    sysColor = Color.Aqua;
                    break;
                case "蓝":
                    sysColor = Color.Blue;
                    break;
                case "洋红":
                    sysColor = Color.Magenta;
                    break;
                case "白":
                    sysColor = Color.White;
                    break;
                case "选择颜色...":
                    sysColor = Color.White;
                    break;
                default:
                    ///更改完元素颜色后重新选中程序会崩溃，暂时关闭
                    //string[] strings = colorName.Split(',');
                    //UInt32 selectColor = RGBFromUint(255, Convert.ToByte(strings[0]), Convert.ToByte(strings[1]), Convert.ToByte(strings[2]));
                    //sysColor = GetColorByUint(selectColor);
                    break;
            }
            //绘制纯色颜色块
            Bitmap bmp = new Bitmap(20, 20);
            Graphics g = Graphics.FromImage(bmp);
            UInt32 uintValue = LayerManageWindow.RGBFromSystem(sysColor);
            SolidBrush b = new SolidBrush(GetColorByUint(uintValue));//这里修改颜色
            g.FillRectangle(b, 0, 0, 20, 20);
            System.Drawing.Rectangle rec = new Rectangle(1, 1, 20, 20);
            g.DrawRectangle(new Pen(System.Drawing.Color.Black, 1), 0, 0, 19, 19);
            this.picColor.Image = bmp;
            this.colorName = colorName;
            this.lblName.Text = colorName;
            if (!isLock)
            {
                this.MouseLeave += (s, e) =>
                {
                    this.lblName.BackColor = Color.White;
                };
                this.MouseHover += (s, e) =>
                {
                    this.lblName.BackColor = Color.LightBlue;
                };
                foreach (Control c in this.Controls)
                {
                    c.MouseHover += C_MouseHover;
                    c.MouseLeave += C_MouseLeave;
                }
            }
 
        }
        private void C_MouseLeave(object? sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }

        private void C_MouseHover(object? sender, EventArgs e)
        {
            this.OnMouseHover(e);
        }
        private void lblName_Click(object sender, EventArgs e)
        {
            //创建ItemClick的委托
            this.ItemClick?.Invoke(this, colorName);
        }
        /// <summary>
        /// 重新定义元素颜色值
        /// </summary>
        /// <param name="color"></param>
        /// <param name="lcElement"></param>
        /// <param name="colorName"></param>
        public void Reset(string color, List<LcElement> lcElement, string colorName)
        {
            if (lcElement != null && lcElement.Count > 0)
            {
                foreach (var element in lcElement)
                {
                    if (colorName == ValueFrom.ByLayer || colorName == ValueFrom.ByBlock)
                        element.Color = colorName;
                    else
                        element.Color = color.ToString();
                }
            }
            if (color != ValueFrom.ByLayer && color != ValueFrom.ByBlock && color != "")
            {
                Bitmap bmp = new Bitmap(20, 20);
                Graphics g = Graphics.FromImage(bmp);
                UInt32 uintValue = Convert.ToUInt32(color);
                SolidBrush b = new SolidBrush(GetColorByUint(uintValue));//这里修改颜色
                g.FillRectangle(b, 0, 0, 20, 20);
                System.Drawing.Rectangle rec = new Rectangle(1, 1, 20, 20);
                g.DrawRectangle(new Pen(System.Drawing.Color.Black, 1), 0, 0, 19, 19);
                this.picColor.Image = bmp;
            }
            switch (colorName)
            {
                case "4294901760":
                    colorName = "红";
                    break;
                case "4294967040":
                    colorName = "黄";
                    break;
                case "4278255360":
                    colorName = "绿";
                    break;
                case "4278255615":
                    colorName = "青";
                    break;
                case "4278190335":
                    colorName = "蓝";
                    break;
                case "4294902015":
                    colorName = "洋红";
                    break;
                case "4294967295":
                    colorName = "白";
                    break;
                default:

                    break;

            }
            this.lblName.Text = colorName;
        }
        /// <summary>
        /// uint值转Color值
        /// </summary>
        /// <param name="uintValue"></param>
        /// <returns></returns>
        public static Color GetColorByUint(uint uintValue)
        {
            Color colorConverted = System.Drawing.Color.FromArgb((byte)((uintValue >> 24) & 255), (ushort)((uintValue >> 16) & 255), (ushort)((uintValue >> 8) & 255), (ushort)(uintValue & 255));
            return colorConverted;
        }
        /// <summary>
        /// byte类型RGB转uint10进制数
        /// </summary>
        /// <param name="alpha"></param>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="bule"></param>
        /// <returns></returns>
        public static uint RGBFromUint(byte alpha, byte red, byte green, byte bule)
        {
            return (uint)(alpha << 24 | (red << 16) | (green << 8) | (bule));
        }
    }
}
