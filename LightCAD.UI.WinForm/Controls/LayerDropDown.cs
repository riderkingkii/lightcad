﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LightCAD.Runtime;
using LightCAD.Core;
using System.Reflection.Metadata;
using netDxf.Tables;

namespace LightCAD.UI
{
    public partial class LayerDropDown : DropDownControl
    {
        private bool isUpdating;
        public static Action<List<LcElement>> propertyChangedAction;
        private LcDocument lcDocument;
        private LcLayer selectLayer;
        private LayerItemControl layerItemControl;
        private List<LcElement> lcElementsList = new List<LcElement>();
        public LayerDropDown()
        {
            isUpdating = true;
            InitializeComponent();
            InitializeDropDown(itemsPanel);
            isUpdating = false;
            this.Load += LayerDropDown_Load;
        }
        public LayerDropDown(LcDocument lcDocument) : this()
        {
            this.selectLayer = lcDocument.Layers.First();
            layerItemControl = new LayerItemControl(this.selectLayer, true);
            layerItemControl.ForeColor = Color.Black;
            layerItemControl.Location = new Point(26, 2);
            layerItemControl.Margin = new Padding(4, 4, 4, 4);
            layerItemControl.Name = "layerItemControl1";
            layerItemControl.Size = new Size(278, 20);
            layerItemControl.TabIndex = 1;
            layerItemControl.ItemClick += LayerItemControl_Click;
            this.Controls.Add(layerItemControl);
            InitLayers(lcDocument);
            propertyChangedAction = PropertyChanged;
            //this.layerItemControl.Reset();
            this.btnLayerManager.MouseEnter += btnLayerManager_MouseEnter;
            this.btnLayerManager.MouseLeave += btnLayerManager_MouseLeave;
        }


        private void LayerItemControl_Click(object? sender, string e)
        {
            OpenDropDown();
        }

        public void ResetSelectLayer(SelectedEventArgs args)
        {
            this.layerItemControl.Visible = true;
            if (!args.Selected)
            {
                lcElementsList.Clear();
                this.selectLayer = lcDocument.Layers.First(n => n.IsStatus);
                this.layerItemControl.Reset(selectLayer, null);
            }
            else
            {
                var lcElements = args.Elements;
                lcElementsList.AddRange(lcElements);
                if (lcElementsList.GroupBy(n => n.Layer).Count() > 1)
                {
                    this.selectLayer = null;
                    this.layerItemControl.Visible = false;
                }
                else
                {
                    this.selectLayer = lcDocument.Layers.First(n => n.Name == lcElements.First().Layer);
                    this.layerItemControl.Reset(selectLayer, null);

                }
            }
        }
        private void PropertyChanged(List<LcElement> lcElements)
        {

        }
        public void InitLayers(LcDocument lcDocument)
        {
            this.lcDocument = lcDocument;
            itemsPanel.Controls.Clear();
            //layerCollection = DocumentManager.Current.Layers;
            foreach (LcLayer item in lcDocument.Layers)
            {
                var layerItem = new LayerItemControl(item);
                layerItem.ItemClick += LayerItem_ItemClick;
                itemsPanel.Controls.Add(layerItem);
                if (item.IsStatus)
                {
                    this.selectLayer = item;
                    layerItemControl.Reset(this.selectLayer, null);
                }
            }
            this.Refresh();
            //for (var i = 0; i < lcDocument.Layers.Count; i++)
            //{
            //    LcLayer lcLayer = lcDocument.Layers[i];
            //    var layerItem = new LayerItemControl(lcLayer);
            //    layerItem.ItemClick += LayerItem_ItemClick;
            //    itemsPanel.Controls.Add(layerItem);
            //}
        }

        private void LayerItem_ItemClick(object? sender, string e)
        {
            if (sender != null && sender is LcLayer)
            {
                var cl = sender as LcLayer;
                switch (e)
                {
                    case "Name":
                        this.selectLayer = cl;
                        if (lcElementsList.Count == 0)
                        {
                            foreach (LcLayer lcLayer in this.lcDocument.Layers)
                            {
                                lcLayer.IsStatus = this.selectLayer.Name == lcLayer.Name;
                            }
                        }
                        this.layerItemControl.Reset(selectLayer, lcElementsList);
                        break;
                    default:
                        if (cl.Name == this.selectLayer.Name)
                        {
                            this.layerItemControl.Reset(selectLayer, null);
                        }
                        break;
                }
            }

        }

        private void LayerDropDown_Load(object? sender, EventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (isUpdating || this.DropState != eDropState.Dropped) return;

            this.CloseDropDown();
            this.Text = this.Stage + " Stage";
        }


        public string Stage
        {
            get { return ""; }
        }

        private void btnLayerManager_MouseEnter(object? sender, EventArgs e)
        {
            this.btnLayerManager.FlatAppearance.BorderColor = Color.Gray;
            this.btnLayerManager.FlatAppearance.BorderSize = 1;
        }

        private void btnLayerManager_MouseLeave(object? sender, EventArgs e)
        {
            this.btnLayerManager.FlatAppearance.BorderSize = 0;
        }

    }
}
