﻿using Avalonia.Controls;
using LightCAD.Core;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class ColorDropDown : DropDownControl
    {
        private List<string> colorList = new List<string>() { ValueFrom.ByLayer, ValueFrom.ByBlock, "红", "黄", "绿", "青", "蓝", "洋红", "白" };
        private ColorItemControl colorItemControl;
        private List<LcElement> lcElementsList = new List<LcElement>();
        private LcDocument lcDocument;
        private string selectColor;//主界面下拉框选中的颜色
        private string colorName = "";//
        private string mainColor = "";//主界面ByLayer的颜色

        // 外部获取选中颜色
        public string SelectItem { get { return colorName; } }

        public ColorDropDown()
        {
            InitializeComponent();
            InitializeDropDown(itemsPanel);

        }
        public ColorDropDown(LcDocument lcDocument) : this()
        {

            this.selectColor = lcDocument.Layers.First(x => x.IsStatus).Color.ToString();
            mainColor = this.selectColor;
            colorItemControl = new ColorItemControl("ByLayer", true, this.selectColor);
            colorItemControl.Location = new Point(23, 0);
            colorItemControl.Name = "colorItemControl1";
            colorItemControl.Size = new Size(155, 24);
            colorItemControl.TabIndex = 0;
            colorItemControl.ItemClick += ColorItemControl_ItemClick;
            this.Controls.Add(colorItemControl);
            InitColors(lcDocument);

        }

        private void ColorItemControl_ItemClick(object? sender, string e)
        {
            OpenDropDown();
        }

        public void InitColors(LcDocument lcDocument)
        {
            this.lcDocument = lcDocument;
            itemsPanel.Controls.Clear();
            foreach (var color in colorList)
            {
                if (color == ValueFrom.ByLayer)
                {
                    this.selectColor = lcDocument.Layers.First(x => x.IsStatus).Color.ToString();
                    mainColor = this.selectColor;
                    var colorItem = new ColorItemControl(color, false, this.selectColor);
                    colorItem.ItemClick += ColorItem_ItemClick;
                    itemsPanel.Controls.Add(colorItem);
                    colorItemControl.Reset(this.selectColor, null, color);
                }
                else
                {
                    var colorItem = new ColorItemControl(color);
                    colorItem.ItemClick += ColorItem_ItemClick;
                    itemsPanel.Controls.Add(colorItem);
                }
            }
            var colorItemSelect = new ColorItemControl("选择颜色...");
            colorItemSelect.ItemClick += ColorItem_ItemClick;
            itemsPanel.Controls.Add(colorItemSelect);
            this.Refresh();
        }

        private void ColorItem_ItemClick(object? sender, string e)
        {
            if (sender != null && e is string)
            {
                switch (e)
                {
                    case "ByLayer":
                        this.selectColor = lcDocument.Layers.First(n => n.IsStatus).Color.ToString();
                        break;
                    case "ByBlock":
                        this.selectColor = ValueFrom.ByBlock;
                        break;
                    case "红":
                        this.selectColor = LayerManageWindow.RGBFromSystem(Color.Red).ToString();
                        break;
                    case "黄":
                        this.selectColor = LayerManageWindow.RGBFromSystem(Color.Yellow).ToString();
                        break;
                    case "绿":
                        this.selectColor = LayerManageWindow.RGBFromSystem(Color.Lime).ToString();
                        break;
                    case "青":
                        this.selectColor = LayerManageWindow.RGBFromSystem(Color.Aqua).ToString();
                        break;
                    case "蓝":
                        this.selectColor = LayerManageWindow.RGBFromSystem(Color.Blue).ToString();
                        break;
                    case "洋红":
                        this.selectColor = LayerManageWindow.RGBFromSystem(Color.Magenta).ToString();
                        break;
                    case "白":
                        this.selectColor = LayerManageWindow.RGBFromSystem(Color.White).ToString();
                        break;
                    case "选择颜色...":
                        //下拉框触发颜色选择器 并重新刷新下拉框列表值
                        ColorDialog colorDialog = new ColorDialog();
                        if (colorDialog.ShowDialog() == DialogResult.OK)
                        {
                            this.selectColor = LayerManageWindow.RGBFromSystem(colorDialog.Color).ToString();
                            e = colorDialog.Color.R + "," + colorDialog.Color.G + "," + colorDialog.Color.B;
                            colorList.Add(e);
                        }
                        InitColors(lcDocument);
                        break;
                    default:
                        ///更改完元素颜色后重新选中程序会崩溃，暂时关闭
                        //if (e != null)
                        //{
                        //    string[] strings = e.Split(',');
                        //    UInt32 selectColor = ColorItemControl.RGBFromUint(255, Convert.ToByte(strings[0]), Convert.ToByte(strings[1]), Convert.ToByte(strings[2]));
                        //    this.selectColor = selectColor.ToString();
                        //}
                        break;
                }
                if (lcElementsList.Count == 0)
                {
                    colorName = e;
                    mainColor = this.selectColor;
                    if (colorName == ValueFrom.ByLayer || colorName == ValueFrom.ByBlock)
                    {
                        this.selectColor = mainColor = lcDocument.Layers.First(n => n.IsStatus).Color.ToString();
                    }
                    lcDocument.Color = this.selectColor;
                }
                this.colorItemControl.Reset(selectColor, lcElementsList, e);

                ItemClickEvent(SelectItem);
            }

        }
        /// <summary>
        /// 重新定义主界面Control的值
        /// </summary>
        /// <param name="args"></param>
        public void ResetSelectColor(SelectedEventArgs args)
        {
            if (colorName == "" || colorName == ValueFrom.ByLayer)
            {
                this.selectColor = lcDocument.Layers.First(n => n.IsStatus).Color.ToString();
                //this.selectColor = mainColor;
                colorName = ValueFrom.ByLayer;
            }

            this.colorItemControl.Visible = true;
            if (!args.Selected)
            {
                if (this.selectColor != mainColor)
                    this.selectColor = mainColor;
                lcElementsList.Clear();
                //this.selectColor = lcDocument.Layers.First(n => n.IsStatus).Color.ToString();
                this.colorItemControl.Reset(selectColor, null, colorName);
            }
            else
            {
                var lcElements = args.Elements;
                lcElementsList.AddRange(lcElements);
                if (lcElementsList.GroupBy(n => n.Layer).Count() > 1)
                {
                    this.selectColor = "";
                    this.colorItemControl.Visible = false;
                }
                else
                {
                    this.selectColor = lcElements.First().GetColorValue().ToString();
                    this.colorItemControl.Reset(this.selectColor, null, lcElements.First().Color.ToString());

                }
            }
        }
    }
}
