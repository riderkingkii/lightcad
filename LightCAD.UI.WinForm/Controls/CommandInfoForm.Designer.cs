﻿namespace LightCAD.UI
{
    partial class CommandInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lbCmds = new System.Windows.Forms.ListBox();
            SuspendLayout();
            // 
            // lbCmds
            // 
            lbCmds.BackColor = System.Drawing.SystemColors.ControlDark;
            lbCmds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            lbCmds.Dock = System.Windows.Forms.DockStyle.Fill;
            lbCmds.ForeColor = System.Drawing.Color.White;
            lbCmds.FormattingEnabled = true;
            lbCmds.IntegralHeight = false;
            lbCmds.ItemHeight = 17;
            lbCmds.Location = new System.Drawing.Point(0, 0);
            lbCmds.Name = "lbCmds";
            lbCmds.Size = new System.Drawing.Size(329, 130);
            lbCmds.TabIndex = 1;
            lbCmds.MouseClick += lbCmds_MouseClick;
            // 
            // CommandInfoForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.SystemColors.ControlDark;
            ClientSize = new System.Drawing.Size(329, 130);
            Controls.Add(lbCmds);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "CommandInfoForm";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            Text = "PopupInfoForm";
            ResumeLayout(false);
        }

        #endregion

        public System.Windows.Forms.ListBox lbCmds;
    }
}