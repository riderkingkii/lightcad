﻿using Avalonia.Controls;
using LightCAD.Core;
using LightCAD.Runtime;
using LightCAD.UI.Controls;
using netDxf.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class LineTypeDropDown : DropDownControl
    {
        private List<LcElement> lcElementsList = new List<LcElement>();
        private LcDocument lcDocument;
        private LcLineType selectLineType;
        private LineTypeItemControl linetypeItemControl;

        // 外部获取选中线型
        public string SelectItem{ get { return selectLineType.LineTypeName; } }

        public LineTypeDropDown()
        {
            InitializeComponent();
            InitializeDropDown(itemsPanel);
        }
        public LineTypeDropDown(LcDocument lcDocument):this()
        {
            this.lcDocument = lcDocument;
            selectLineType = lcDocument.LineTypes.FirstOrDefault();
            linetypeItemControl = new LineTypeItemControl(selectLineType);
            linetypeItemControl.Location = new Point(23, 0);
            linetypeItemControl.Name = "layerItemControl";
            linetypeItemControl.Size = new Size(155, 24);
            linetypeItemControl.TabIndex = 0;
            linetypeItemControl.ItemClick += LineTypeItemControl_Click;
            this.Controls.Add(linetypeItemControl);
            InitLineType(lcDocument);
        }

        private void LineTypeItemControl_Click(object? sender, string e)
        {
            OpenDropDown();
        }

        public void InitLineType(LcDocument lcDocument)
        {
            List<LcLineType> linetypes = lcDocument.LineTypes.ToList();
            itemsPanel.Controls.Clear();
            foreach (var linetypeitem in linetypes)
            {
                var linetype = new LineTypeItemControl(linetypeitem);
                linetype.ItemClick += LineTypeItem_ItemClick;
                itemsPanel.Controls.Add(linetype);
            }
            this.Refresh();
        }
        private void LineTypeItem_ItemClick(object? sender, string e) {
            if (sender != null && sender is LcLineType)
            {
                selectLineType = sender as LcLineType;
                switch (e)
                {
                    case "ByLayer":
                        break;
                    default:
                        break;
                }
                this.linetypeItemControl.Reset(selectLineType, lcElementsList);

                ItemClickEvent(SelectItem);
            }
        }

        public void ResetSelectLineType(SelectedEventArgs args)
        {
            this.linetypeItemControl.Visible = true;
            if (!args.Selected)
            {
                lcElementsList.Clear();
                //var selectLayer = lcDocument.Layers.First(n => n.IsStatus);
                //selectLineType = lcDocument.LineTypes.FirstOrDefault(x => x.LineTypeName == selectLayer.LineTypeName);
                this.linetypeItemControl.Reset(selectLineType, null);
            }
            else
            {
                var lcElements = args.Elements;
                lcElementsList.AddRange(lcElements);
                if (lcElementsList.GroupBy(n => n.Layer).Count() > 1)
                {
                    selectLineType = new LcLineType();
                    this.linetypeItemControl.Visible = false;
                }
                else
                {
                    //var selectLayer = lcDocument.Layers.First(n => n.Name == lcElements.First().Layer);
                    selectLineType = lcDocument.LineTypes.FirstOrDefault(x => x.LineTypeName == lcElements.First().LineType);
                    this.linetypeItemControl.Reset(selectLineType, null);

                }
            }
        }
    }
}
