﻿namespace LightCAD.UI
{
    partial class TitleTabItemControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            picLeft = new System.Windows.Forms.PictureBox();
            pnlTab = new System.Windows.Forms.Panel();
            picClose = new System.Windows.Forms.PictureBox();
            lblText = new System.Windows.Forms.Label();
            picIcon = new System.Windows.Forms.PictureBox();
            picRight = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)picLeft).BeginInit();
            pnlTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)picClose).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picIcon).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picRight).BeginInit();
            SuspendLayout();
            // 
            // picLeft
            // 
            picLeft.Dock = System.Windows.Forms.DockStyle.Left;
            picLeft.Image = Properties.TabResources.Left;
            picLeft.Location = new System.Drawing.Point(0, 0);
            picLeft.Name = "picLeft";
            picLeft.Size = new System.Drawing.Size(14, 34);
            picLeft.TabIndex = 0;
            picLeft.TabStop = false;
            // 
            // pnlTab
            // 
            pnlTab.AutoSize = true;
            pnlTab.BackgroundImage = Properties.TabResources.Center;
            pnlTab.Controls.Add(picClose);
            pnlTab.Controls.Add(lblText);
            pnlTab.Controls.Add(picIcon);
            pnlTab.Dock = System.Windows.Forms.DockStyle.Left;
            pnlTab.Location = new System.Drawing.Point(14, 0);
            pnlTab.Name = "pnlTab";
            pnlTab.Size = new System.Drawing.Size(91, 34);
            pnlTab.TabIndex = 1;
            // 
            // picClose
            // 
            picClose.BackColor = System.Drawing.Color.Transparent;
            picClose.Dock = System.Windows.Forms.DockStyle.Left;
            picClose.Image = Properties.TabResources.Close;
            picClose.Location = new System.Drawing.Point(75, 0);
            picClose.Name = "picClose";
            picClose.Size = new System.Drawing.Size(16, 34);
            picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            picClose.TabIndex = 2;
            picClose.TabStop = false;
            // 
            // lblText
            // 
            lblText.AutoSize = true;
            lblText.BackColor = System.Drawing.Color.Transparent;
            lblText.Dock = System.Windows.Forms.DockStyle.Left;
            lblText.Location = new System.Drawing.Point(32, 0);
            lblText.Name = "lblText";
            lblText.Padding = new System.Windows.Forms.Padding(2, 7, 2, 7);
            lblText.Size = new System.Drawing.Size(43, 34);
            lblText.TabIndex = 1;
            lblText.Text = "标题";
            // 
            // picIcon
            // 
            picIcon.BackColor = System.Drawing.Color.Transparent;
            picIcon.Dock = System.Windows.Forms.DockStyle.Left;
            picIcon.Location = new System.Drawing.Point(0, 0);
            picIcon.Name = "picIcon";
            picIcon.Size = new System.Drawing.Size(32, 34);
            picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            picIcon.TabIndex = 0;
            picIcon.TabStop = false;
            // 
            // picRight
            // 
            picRight.Dock = System.Windows.Forms.DockStyle.Left;
            picRight.Image = Properties.TabResources.Right;
            picRight.Location = new System.Drawing.Point(105, 0);
            picRight.Name = "picRight";
            picRight.Size = new System.Drawing.Size(14, 34);
            picRight.TabIndex = 2;
            picRight.TabStop = false;
            // 
            // TitleTabItemControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            AutoSize = true;
            BackColor = System.Drawing.Color.Transparent;
            Controls.Add(picRight);
            Controls.Add(pnlTab);
            Controls.Add(picLeft);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "TitleTabItemControl";
            Size = new System.Drawing.Size(216, 34);
            ((System.ComponentModel.ISupportInitialize)picLeft).EndInit();
            pnlTab.ResumeLayout(false);
            pnlTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)picClose).EndInit();
            ((System.ComponentModel.ISupportInitialize)picIcon).EndInit();
            ((System.ComponentModel.ISupportInitialize)picRight).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.PictureBox picClose;
        public System.Windows.Forms.Label lblText;
        public System.Windows.Forms.PictureBox picLeft;
        public System.Windows.Forms.PictureBox picRight;
        public System.Windows.Forms.Panel pnlTab;
    }
}
