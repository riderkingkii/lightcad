﻿namespace LightCAD.UI
{
    partial class ColorItemControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            picColor = new System.Windows.Forms.PictureBox();
            lblName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)picColor).BeginInit();
            SuspendLayout();
            // 
            // picColor
            // 
            picColor.BackColor = System.Drawing.Color.White;
            picColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            picColor.Location = new System.Drawing.Point(2, 3);
            picColor.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            picColor.Name = "picColor";
            picColor.Size = new System.Drawing.Size(16, 17);
            picColor.TabIndex = 6;
            picColor.TabStop = false;
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            lblName.BackColor = System.Drawing.Color.White;
            lblName.Location = new System.Drawing.Point(22, 3);
            lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(131, 17);
            lblName.TabIndex = 7;
            lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblName.Click += lblName_Click;
            // 
            // ColorItemControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(lblName);
            Controls.Add(picColor);
            Name = "ColorItemControl";
            Size = new System.Drawing.Size(155, 24);
            ((System.ComponentModel.ISupportInitialize)picColor).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.PictureBox picColor;
        private System.Windows.Forms.Label lblName;
    }
}
