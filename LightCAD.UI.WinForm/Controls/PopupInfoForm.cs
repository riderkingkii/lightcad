﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class PopupInfoForm : Form, IPopupControl
    {
        public PopupInfoForm()
        {
            InitializeComponent();
        }
        public PopupInfoForm(PopupInfo host)
        {
            InitializeComponent();
            this.Host = host;
        }
        public PopupInfo Host { get; }

        public void SetInfo(string info)
        {
            lblInfo.Text = info;
        }
    }
}
