﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Controls
{
    public partial class LineTypeItemControl : UserControl
    {
        public event EventHandler<string> ItemClick;
        public LcLineType LineType { get; set; }
        public LineTypeItemControl()
        {
            InitializeComponent();
        }

        public LineTypeItemControl(LcLineType linetype)
        {
            InitializeComponent();
            this.LineType = linetype;
            this.lblLineTypeName.Text = linetype?.LineTypeName;
            this.MouseLeave += (s, e) => {
                this.lblLineTypeName.BackColor = Color.White;
            };
            this.MouseHover += (s, e) => {
                this.lblLineTypeName.BackColor = Color.LightBlue;
            };
            foreach (Control c in this.Controls)
            {
                c.MouseHover += C_MouseHover;
                c.MouseLeave += C_MouseLeave;
            }
        }

        private void C_MouseLeave(object? sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }

        private void C_MouseHover(object? sender, EventArgs e)
        {
            this.OnMouseHover(e);
        }

        public void Reset(LcLineType linetype, List<LcElement> lcElement)
        {
            if (lcElement != null && lcElement.Count > 0)
            {
                foreach (var element in lcElement)
                {
                    element.LineType = linetype.LineTypeName;
                }
            }
            this.LineType = linetype;
            this.lblLineTypeName.Text = linetype?.LineTypeName;
        }

        private void lblLineTypeName_Click(object sender, EventArgs e)
        {
            this.ItemClick?.Invoke(this.LineType, null);
        }
    }
}
