﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public class RibbonTabControl : TabControl
    {
        public event EventHandler ItemClick;
        public RibbonTabPage AddPage(string tabName, string tabText)
        {

            var tp = new RibbonTabPage();
            tp.Name = tabName;
            tp.Text = tabText;
            tp.BackColor = Color.WhiteSmoke;
            tp.Padding = new Padding(10, 0, 10, 0);
            tp.ItemClick += Tp_ItemClick;
            this.TabPages.Add(tp);
            return tp;
        }

        private void Tp_ItemClick(object? sender, EventArgs e)
        {
            ItemClick?.Invoke(sender, e);
        }

        [Browsable(false)]
        public override Rectangle DisplayRectangle
        {
            get
            {
                return new Rectangle(0, this.ItemSize.Height, this.Width, this.Height - this.ItemSize.Height);
            }
        }
    }

    public class RibbonTableLayout : TableLayoutPanel, ITableLayout
    {
        public void AddControl(object ctrl)
        {
            this.Controls.Add((Control)ctrl);
        }

        public void AddColumnDefinition(object width)
        {
            this.ColumnStyles.Add(new ColumnStyle
            {
                SizeType = SizeType.Absolute,
                Width = (int)width
            });
        }

        public void AddRowDefinition(object height)
        {
            this.RowStyles.Add(new RowStyle
            {
                SizeType = SizeType.Absolute,
                Height = (int)height
            });
        }

        public void SetColumn(object ctrl, int colum)
        {
            base.SetColumn((Control)ctrl, colum);
        }

        public void SetColumnSpan(object ctrl, int columnSpan)
        {
            base.SetColumnSpan((Control)ctrl, columnSpan);
        }

        public void SetRow(object ctrl, int row)
        {
            base.SetRow((Control)ctrl, row);
        }

        public void SetRowSpan(object ctrl, int rowSpan)
        {
            base.SetRowSpan((Control)ctrl, rowSpan);
        }
    }
    public class RibbonTabPage : TabPage, ITabPanel
    {
        public event EventHandler ItemClick;

        public TabButtonGroupRuntime CreateButtonGroup(TabButtonGroup btnGroup)
        {
            var btnGroupControl = new TabButtonGroupRuntime();
            btnGroupControl.ButtonGroup = btnGroup;

            var tableLayout = new RibbonTableLayout();
            tableLayout.AutoSize = true;
            tableLayout.RowCount = 2;
            tableLayout.AddRowDefinition(32);
            tableLayout.AddRowDefinition(32);
            tableLayout.Margin = new Padding(-1);
            tableLayout.Dock = DockStyle.Left;
            //tableLayout.BackColor = ThemeColors.LightDark.Color;
            this.Controls.Add(tableLayout);
            tableLayout.BringToFront();

            btnGroupControl.TableLayout = tableLayout;

            var tblLyt = (ITableLayout)tableLayout;
            var colWidths = new List<int>();
            int tmpWidth = 0;
            int columCount = 0, rowIdx = 0;
            for (var i = 0; i < btnGroup.Buttons.Count; i++)
            {
                var btn = btnGroup.Buttons[i];
                var btnCtrl = new TabButtonControl();
                btnCtrl.Button = btn;

                var ribbonBtn = new RibbonButton();
                ribbonBtn.Margin = new Padding(-1);//**这里Margin是起作用的！！！
                ribbonBtn.Click += RibbonBtn_Click;
                ribbonBtn.DropDownItemClick += RibbonBtn_DropDownItemClick;
                ribbonBtn.Width = btn.GetWidth();

                tblLyt.AddControl(ribbonBtn);
                ribbonBtn.ResetButton(btn);


                if (btn.IsSmallSize)
                {
                    ribbonBtn.Height = 32;
                    if (rowIdx == 0)
                    {
                        tmpWidth = btn.GetWidth();
                        tblLyt.SetColumn(ribbonBtn, columCount);
                        tblLyt.SetRow(ribbonBtn, rowIdx);
                        rowIdx = 1;
                    }
                    else
                    {
                        //小的在上面 布局小的，宽度的增加以较宽的为准
                        colWidths.Add(Math.Max(btn.GetWidth(), tmpWidth));
                        tblLyt.SetColumn(ribbonBtn, columCount);
                        tblLyt.SetRow(ribbonBtn, rowIdx);
                        rowIdx = 0;
                        columCount++;
                        tmpWidth = 0;
                    }

                }
                else
                {
                    ribbonBtn.Height = 64;
                    //小的在上面 布局大的，以宽度大的为准
                    colWidths.Add(Math.Max(btn.GetWidth(), tmpWidth));
                    rowIdx = 0;
                    tblLyt.SetColumn(ribbonBtn, columCount);
                    tblLyt.SetRow(ribbonBtn, rowIdx);
                    tblLyt.SetRowSpan(ribbonBtn, 2);
                    columCount++;
                    tmpWidth = 0;
                }
            }

            tblLyt.ColumnCount = columCount + 1;
            var gap = new Panel();
            gap.Width = 2;
            gap.Margin = new Padding(-1);
            gap.Padding = new Padding(-1);
            gap.BackColor = ThemeColors.GroupGap.Color;
            tblLyt.AddControl(gap);
            tblLyt.SetColumn(gap, columCount);
            tblLyt.SetRow(gap, 0);
            tblLyt.SetRowSpan(gap, 2);


            int sumWidth = 0;
            colWidths.Add(gap.Width);//组之间的间隙
            foreach (var colWidth in colWidths)
            {
                tblLyt.AddColumnDefinition(colWidth);
                sumWidth += colWidth;
            }
            tblLyt.Width = sumWidth;
            return btnGroupControl;
        }

        private void RibbonBtn_DropDownItemClick(object sender, EventArgs e)
        {
            ItemClick?.Invoke(sender, e);
        }

        private void RibbonBtn_Click(object sender, EventArgs e)
        {
            var rbtn = sender as RibbonButton;
            if (!rbtn.Button.IsCommand && rbtn.Button.Handler!=null)
            {
                rbtn.Button.Handler(rbtn, EventArgs.Empty);
            }
            ItemClick?.Invoke(sender, e);
        }
    }

}
