﻿namespace LightCAD.UI
{
    partial class TabMenuControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            RibbonTab = new RibbonTabControl();
            HeaderBar = new HeaderMenuControl();
            SuspendLayout();
            // 
            // RibbonTab
            // 
            RibbonTab.Dock = System.Windows.Forms.DockStyle.Top;
            RibbonTab.ItemSize = new System.Drawing.Size(60, 0);
            RibbonTab.Location = new System.Drawing.Point(0, 32);
            RibbonTab.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            RibbonTab.Name = "RibbonTab";
            RibbonTab.SelectedIndex = 0;
            RibbonTab.Size = new System.Drawing.Size(963, 64);
            RibbonTab.TabIndex = 2;
            // 
            // HeaderBar
            // 
            HeaderBar.BackColor = System.Drawing.Color.White;
            HeaderBar.Dock = System.Windows.Forms.DockStyle.Top;
            HeaderBar.Location = new System.Drawing.Point(0, 0);
            HeaderBar.Margin = new System.Windows.Forms.Padding(0);
            HeaderBar.Name = "HeaderBar";
            HeaderBar.SelectedItem = "";
            HeaderBar.Size = new System.Drawing.Size(963, 32);
            HeaderBar.TabIndex = 3;
            // 
            // TabMenuControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            AutoSize = true;
            BackColor = System.Drawing.Color.White;
            Controls.Add(RibbonTab);
            Controls.Add(HeaderBar);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "TabMenuControl";
            Size = new System.Drawing.Size(963, 96);
            ResumeLayout(false);
        }

        #endregion
        public HeaderMenuControl HeaderBar;
        public RibbonTabControl RibbonTab;
    }
}
