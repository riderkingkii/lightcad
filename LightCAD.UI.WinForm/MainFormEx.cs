using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;


namespace LightCAD.UI
{
    public partial class MainFormEx : Form
    {
        private bool m_bSaveLayout = true;
        private DeserializeDockContent m_deserializeDockContent;
        private DummySolutionExplorer m_solutionExplorer;
        private DummyPropertyWindow m_propertyWindow;
        private DummyToolbox m_toolbox;
        private DummyOutputWindow m_outputWindow;
        private DummyTaskList m_taskList;
        private bool _showSplash;
        private SplashScreen _splashScreen;

        public MainFormEx()
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;
            InitTitleBarHandler();
        }

        #region 标题条窗口处理

        private void InitTitleBarHandler()
        {
            this.MouseMove += new MouseEventHandler(Form_MouseMove);

            this.lbMin.MouseEnter += new EventHandler(btn_MouseEnter);
            this.lbMin.MouseLeave += new EventHandler(btn_MouseLeave);
            this.lbMin.Click += lbMin_Click;

            this.lbMax.MouseEnter += new EventHandler(btn_MouseEnter);
            this.lbMax.MouseLeave += new EventHandler(btn_MouseLeave);
            this.lbMax.Click += lbMax_Click;

            this.lbClose.MouseEnter += new EventHandler(btn_MouseEnter);
            this.lbClose.Click += lbClose_Click;
            this.lbClose.MouseLeave += new EventHandler(btn_MouseLeave);

            this.titleBar.MouseDoubleClick += titleBar_MouseDoubleClick;
            this.titleBar.MouseDown += titleBar_MouseDown;
            this.titleBar.MouseMove += titleBar_MouseMove;

            this.lbDrop.MouseEnter += new EventHandler(btn_MouseEnter);
            this.lbDrop.Click += lbDrop_Click;
            this.lbDrop.MouseLeave += new EventHandler(btn_MouseLeave);

        }



        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            //Q3502120
            if (e.Button == MouseButtons.None)
            {
                if (e.Location.X >= this.Width - 5)
                {
                    if (e.Location.Y > this.Height - 5)
                        this.Cursor = Cursors.SizeNWSE;
                    else
                        this.Cursor = Cursors.SizeWE;
                }
                else if (e.Location.Y >= this.Height - 5)
                    this.Cursor = Cursors.SizeNS;
                else
                    this.Cursor = Cursors.Arrow;
            }
            else
            {
                if (this.Cursor == Cursors.SizeNWSE)
                {
                    this.Width = MousePosition.X - this.Left + 1;
                    this.Height = MousePosition.Y - this.Top + 1;
                }
                else if (this.Cursor == Cursors.SizeWE)
                    this.Width = MousePosition.X - this.Left;
                else if (this.Cursor == Cursors.SizeNS)
                    this.Height = MousePosition.Y - this.Top;
            }
        }

        private Point mousePoint = new Point();
        private void titleBar_MouseMove(object sender, MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    this.WindowState = FormWindowState.Normal;
                    return;
                }
                this.Top = Control.MousePosition.Y - mousePoint.Y;
                this.Left = Control.MousePosition.X - mousePoint.X;
            }
        }
        private void titleBar_MouseDown(object sender, MouseEventArgs e)
        {
            base.OnMouseDown(e);
            this.mousePoint.X = e.X;
            this.mousePoint.Y = e.Y;
        }
        private void titleBar_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            WindowMax();
        }

        private void WindowMax()
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.MaximumSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
                this.WindowState = FormWindowState.Maximized;
            }
            this.Invalidate();
        }
        private void lbMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void lbMax_Click(object sender, EventArgs e)
        {
            WindowMax();
        }
        private void lbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void lbDrop_Click(object? sender, EventArgs e)
        {
            lbDrop_Menu.Show(lbDrop, new Point(0, lbDrop.Height), ToolStripDropDownDirection.BelowRight);
        }

        private void btn_MouseEnter(object sender, EventArgs e)
        {
            if (((Label)sender) == lbClose)
                ((Label)sender).BackColor = Color.Tomato;
            else
                ((Label)sender).BackColor = Color.FromArgb(255, 200, 200, 200);

        }
        private void btn_MouseLeave(object sender, EventArgs e)
        {
            ((Label)sender).BackColor = Color.Transparent;
        }
        #endregion

        #region SplashScreen
        public void SetSplashScreen(Action callback)
        {

            _showSplash = true;
            _splashScreen = new SplashScreen();

            ResizeSplash();
            _splashScreen.Visible = true;
            _splashScreen.TopMost = true;

            Timer _timer = new Timer();
            _timer.Tick += (sender, e) =>
            {
                _splashScreen.Visible = false;
                _timer.Enabled = false;
                _showSplash = false;
                callback();
            };
            _timer.Interval = 2000;
            _timer.Enabled = true;
        }

        private void ResizeSplash()
        {
            if (_showSplash)
            {

                var centerXMain = (this.Location.X + this.Width) / 2.0;
                var LocationXSplash = Math.Max(0, centerXMain - (_splashScreen.Width / 2.0));

                var centerYMain = (this.Location.Y + this.Height) / 2.0;
                var LocationYSplash = Math.Max(0, centerYMain - (_splashScreen.Height / 2.0));

                _splashScreen.Location = new Point((int)Math.Round(LocationXSplash), (int)Math.Round(LocationYSplash));
            }
        }

        #endregion

        #region MainForm Events
        private void MainForm_Load(object sender, System.EventArgs e)
        {
        }


        private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }


        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            ResizeSplash();
            this.Refresh();//解决在CreateParams时 最大化时会不刷新主窗体背景的BUG
        }

        /// <summary>
        /// 阻止TitleTabControl在操作时的闪动,但在最大化时会不刷新主窗体背景
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion
    }


}