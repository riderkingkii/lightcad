﻿namespace LightCAD.UI.Building
{
    partial class InitLevelsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lbDX = new System.Windows.Forms.Label();
            textBasementCount = new System.Windows.Forms.TextBox();
            la = new System.Windows.Forms.Label();
            lbDS = new System.Windows.Forms.Label();
            textGroundCount = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            lbWD = new System.Windows.Forms.Label();
            textRoofCount = new System.Windows.Forms.TextBox();
            label4 = new System.Windows.Forms.Label();
            btnOK = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // lbDX
            // 
            lbDX.AutoSize = true;
            lbDX.ForeColor = System.Drawing.Color.Red;
            lbDX.Location = new System.Drawing.Point(235, 18);
            lbDX.Name = "lbDX";
            lbDX.Size = new System.Drawing.Size(104, 17);
            lbDX.TabIndex = 6;
            lbDX.Text = "请输入地下楼层数";
            lbDX.Visible = false;
            // 
            // textBasementCount
            // 
            textBasementCount.Location = new System.Drawing.Point(86, 12);
            textBasementCount.Name = "textBasementCount";
            textBasementCount.Size = new System.Drawing.Size(143, 23);
            textBasementCount.TabIndex = 5;
            textBasementCount.TextChanged += textBasementCount_TextChanged;
            // 
            // la
            // 
            la.AutoSize = true;
            la.Location = new System.Drawing.Point(12, 15);
            la.Name = "la";
            la.Size = new System.Drawing.Size(68, 17);
            la.TabIndex = 4;
            la.Text = "地下楼层：";
            // 
            // lbDS
            // 
            lbDS.AutoSize = true;
            lbDS.ForeColor = System.Drawing.Color.Red;
            lbDS.Location = new System.Drawing.Point(235, 47);
            lbDS.Name = "lbDS";
            lbDS.Size = new System.Drawing.Size(104, 17);
            lbDS.TabIndex = 9;
            lbDS.Text = "请输入地上楼层数";
            lbDS.Visible = false;
            // 
            // textGroundCount
            // 
            textGroundCount.Location = new System.Drawing.Point(86, 41);
            textGroundCount.Name = "textGroundCount";
            textGroundCount.Size = new System.Drawing.Size(143, 23);
            textGroundCount.TabIndex = 8;
            textGroundCount.TextChanged += textGroundCount_TextChanged;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(12, 44);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(68, 17);
            label2.TabIndex = 7;
            label2.Text = "地上楼层：";
            // 
            // lbWD
            // 
            lbWD.AutoSize = true;
            lbWD.ForeColor = System.Drawing.Color.Red;
            lbWD.Location = new System.Drawing.Point(235, 76);
            lbWD.Name = "lbWD";
            lbWD.Size = new System.Drawing.Size(104, 17);
            lbWD.TabIndex = 12;
            lbWD.Text = "请输入屋顶楼层数";
            lbWD.Visible = false;
            // 
            // textRoofCount
            // 
            textRoofCount.Location = new System.Drawing.Point(86, 70);
            textRoofCount.Name = "textRoofCount";
            textRoofCount.Size = new System.Drawing.Size(143, 23);
            textRoofCount.TabIndex = 11;
            textRoofCount.TextChanged += textRoofCount_TextChanged;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(12, 73);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(68, 17);
            label4.TabIndex = 10;
            label4.Text = "屋顶楼层：";
            // 
            // btnOK
            // 
            btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOK.Location = new System.Drawing.Point(265, 114);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(75, 30);
            btnOK.TabIndex = 13;
            btnOK.Text = "确定";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += btnOK_Click;
            // 
            // InitLevelsWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(352, 156);
            Controls.Add(btnOK);
            Controls.Add(lbWD);
            Controls.Add(textRoofCount);
            Controls.Add(label4);
            Controls.Add(lbDS);
            Controls.Add(textGroundCount);
            Controls.Add(label2);
            Controls.Add(lbDX);
            Controls.Add(textBasementCount);
            Controls.Add(la);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "InitLevelsWindow";
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "初始化楼层";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label lbDX;
        private System.Windows.Forms.TextBox textBasementCount;
        private System.Windows.Forms.Label la;
        private System.Windows.Forms.Label lbDS;
        private System.Windows.Forms.TextBox textGroundCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbWD;
        private System.Windows.Forms.TextBox textRoofCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOK;
    }
}