﻿namespace LightCAD.UI
{
    partial class BuildingSettingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuildingSettingWindow));
            btnOK = new System.Windows.Forms.Button();
            dgvLevels = new System.Windows.Forms.DataGridView();
            LevelId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvLevelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvAHeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvSHeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvAElevation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvSElevation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvLevelType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            panel1 = new System.Windows.Forms.Panel();
            listBuildings = new System.Windows.Forms.ListBox();
            panel3 = new System.Windows.Forms.Panel();
            toolStrip1 = new System.Windows.Forms.ToolStrip();
            btnInitLevel = new System.Windows.Forms.ToolStripButton();
            btnEditHeight = new System.Windows.Forms.ToolStripButton();
            btnSetFirstHeight = new System.Windows.Forms.ToolStripButton();
            btnInsertLevels = new System.Windows.Forms.ToolStripButton();
            btnRechristenLevel = new System.Windows.Forms.ToolStripButton();
            btnDelLevel = new System.Windows.Forms.ToolStripButton();
            panel4 = new System.Windows.Forms.Panel();
            toolStrip2 = new System.Windows.Forms.ToolStrip();
            btnAddbuilding = new System.Windows.Forms.ToolStripButton();
            btnDelbuilding = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)dgvLevels).BeginInit();
            panel1.SuspendLayout();
            panel3.SuspendLayout();
            toolStrip1.SuspendLayout();
            panel4.SuspendLayout();
            toolStrip2.SuspendLayout();
            SuspendLayout();
            // 
            // btnOK
            // 
            btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOK.Location = new System.Drawing.Point(730, 20);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(75, 30);
            btnOK.TabIndex = 0;
            btnOK.Text = "确定";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += btnOK_Click;
            // 
            // dgvLevels
            // 
            dgvLevels.AllowUserToAddRows = false;
            dgvLevels.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dgvLevels.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dgvLevels.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dgvLevels.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dgvLevels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvLevels.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { LevelId, dgvLevelName, dgvAHeight, dgvSHeight, dgvAElevation, dgvSElevation, dgvLevelType });
            dgvLevels.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvLevels.Location = new System.Drawing.Point(3, 28);
            dgvLevels.Name = "dgvLevels";
            dgvLevels.ReadOnly = true;
            dgvLevels.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dgvLevels.RowsDefaultCellStyle = dataGridViewCellStyle3;
            dgvLevels.RowTemplate.Height = 25;
            dgvLevels.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgvLevels.Size = new System.Drawing.Size(622, 349);
            dgvLevels.TabIndex = 3;
            dgvLevels.CellFormatting += dgvLevels_CellFormatting;
            // 
            // LevelId
            // 
            LevelId.DataPropertyName = "Id";
            LevelId.HeaderText = "Id";
            LevelId.Name = "LevelId";
            LevelId.ReadOnly = true;
            LevelId.Visible = false;
            // 
            // dgvLevelName
            // 
            dgvLevelName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dgvLevelName.DataPropertyName = "Name";
            dgvLevelName.HeaderText = "楼层名";
            dgvLevelName.Name = "dgvLevelName";
            dgvLevelName.ReadOnly = true;
            // 
            // dgvAHeight
            // 
            dgvAHeight.DataPropertyName = "Height";
            dgvAHeight.HeaderText = "建筑层高(毫米)";
            dgvAHeight.Name = "dgvAHeight";
            dgvAHeight.ReadOnly = true;
            dgvAHeight.Width = 120;
            // 
            // dgvSHeight
            // 
            dgvSHeight.DataPropertyName = "S_Height";
            dgvSHeight.HeaderText = "结构层高";
            dgvSHeight.Name = "dgvSHeight";
            dgvSHeight.ReadOnly = true;
            // 
            // dgvAElevation
            // 
            dgvAElevation.DataPropertyName = "Elevation";
            dgvAElevation.HeaderText = "建筑标高";
            dgvAElevation.Name = "dgvAElevation";
            dgvAElevation.ReadOnly = true;
            // 
            // dgvSElevation
            // 
            dgvSElevation.DataPropertyName = "S_Elevation";
            dgvSElevation.HeaderText = "结构标高";
            dgvSElevation.Name = "dgvSElevation";
            dgvSElevation.ReadOnly = true;
            // 
            // dgvLevelType
            // 
            dgvLevelType.DataPropertyName = "Type";
            dgvLevelType.HeaderText = "楼层类型";
            dgvLevelType.Name = "dgvLevelType";
            dgvLevelType.ReadOnly = true;
            // 
            // panel1
            // 
            panel1.Controls.Add(btnOK);
            panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            panel1.Location = new System.Drawing.Point(3, 383);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(828, 67);
            panel1.TabIndex = 4;
            // 
            // listBuildings
            // 
            listBuildings.BackColor = System.Drawing.SystemColors.Control;
            listBuildings.Dock = System.Windows.Forms.DockStyle.Fill;
            listBuildings.FormattingEnabled = true;
            listBuildings.ItemHeight = 17;
            listBuildings.Location = new System.Drawing.Point(3, 28);
            listBuildings.Name = "listBuildings";
            listBuildings.Size = new System.Drawing.Size(194, 349);
            listBuildings.TabIndex = 6;
            listBuildings.SelectedIndexChanged += listBuildings_SelectedIndexChanged;
            // 
            // panel3
            // 
            panel3.Controls.Add(dgvLevels);
            panel3.Controls.Add(toolStrip1);
            panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            panel3.Location = new System.Drawing.Point(203, 3);
            panel3.Name = "panel3";
            panel3.Padding = new System.Windows.Forms.Padding(3);
            panel3.Size = new System.Drawing.Size(628, 380);
            panel3.TabIndex = 7;
            // 
            // toolStrip1
            // 
            toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnInitLevel, btnEditHeight, btnSetFirstHeight, btnInsertLevels, btnRechristenLevel, btnDelLevel });
            toolStrip1.Location = new System.Drawing.Point(3, 3);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.Size = new System.Drawing.Size(622, 25);
            toolStrip1.TabIndex = 4;
            toolStrip1.Text = "toolStrip1";
            // 
            // btnInitLevel
            // 
            btnInitLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnInitLevel.Image = (System.Drawing.Image)resources.GetObject("btnInitLevel.Image");
            btnInitLevel.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnInitLevel.Name = "btnInitLevel";
            btnInitLevel.Size = new System.Drawing.Size(72, 22);
            btnInitLevel.Text = "初始化楼层";
            btnInitLevel.Click += btnInitLevel_Click;
            // 
            // btnEditHeight
            // 
            btnEditHeight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnEditHeight.Image = (System.Drawing.Image)resources.GetObject("btnEditHeight.Image");
            btnEditHeight.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnEditHeight.Name = "btnEditHeight";
            btnEditHeight.Size = new System.Drawing.Size(60, 22);
            btnEditHeight.Text = "调整层高";
            btnEditHeight.Click += btnEditHeight_Click;
            // 
            // btnSetFirstHeight
            // 
            btnSetFirstHeight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnSetFirstHeight.Image = (System.Drawing.Image)resources.GetObject("btnSetFirstHeight.Image");
            btnSetFirstHeight.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnSetFirstHeight.Name = "btnSetFirstHeight";
            btnSetFirstHeight.Size = new System.Drawing.Size(84, 22);
            btnSetFirstHeight.Text = "设置首层标高";
            btnSetFirstHeight.Click += btnSetFirstHeight_Click;
            // 
            // btnInsertLevels
            // 
            btnInsertLevels.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnInsertLevels.Image = (System.Drawing.Image)resources.GetObject("btnInsertLevels.Image");
            btnInsertLevels.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnInsertLevels.Name = "btnInsertLevels";
            btnInsertLevels.Size = new System.Drawing.Size(60, 22);
            btnInsertLevels.Text = "插入楼层";
            btnInsertLevels.Click += btnInsertLevels_Click;
            // 
            // btnRechristenLevel
            // 
            btnRechristenLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnRechristenLevel.Enabled = false;
            btnRechristenLevel.Image = (System.Drawing.Image)resources.GetObject("btnRechristenLevel.Image");
            btnRechristenLevel.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnRechristenLevel.Name = "btnRechristenLevel";
            btnRechristenLevel.Size = new System.Drawing.Size(72, 22);
            btnRechristenLevel.Text = "重命名楼层";
            // 
            // btnDelLevel
            // 
            btnDelLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnDelLevel.Image = (System.Drawing.Image)resources.GetObject("btnDelLevel.Image");
            btnDelLevel.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnDelLevel.Name = "btnDelLevel";
            btnDelLevel.Size = new System.Drawing.Size(60, 22);
            btnDelLevel.Text = "删除楼层";
            btnDelLevel.Click += btnDelLevel_Click;
            // 
            // panel4
            // 
            panel4.Controls.Add(listBuildings);
            panel4.Controls.Add(toolStrip2);
            panel4.Dock = System.Windows.Forms.DockStyle.Left;
            panel4.Location = new System.Drawing.Point(3, 3);
            panel4.Name = "panel4";
            panel4.Padding = new System.Windows.Forms.Padding(3);
            panel4.Size = new System.Drawing.Size(200, 380);
            panel4.TabIndex = 5;
            // 
            // toolStrip2
            // 
            toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnAddbuilding, btnDelbuilding });
            toolStrip2.Location = new System.Drawing.Point(3, 3);
            toolStrip2.Name = "toolStrip2";
            toolStrip2.Size = new System.Drawing.Size(194, 25);
            toolStrip2.TabIndex = 7;
            toolStrip2.Text = "toolStrip2";
            // 
            // btnAddbuilding
            // 
            btnAddbuilding.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnAddbuilding.Image = (System.Drawing.Image)resources.GetObject("btnAddbuilding.Image");
            btnAddbuilding.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAddbuilding.Name = "btnAddbuilding";
            btnAddbuilding.Size = new System.Drawing.Size(60, 22);
            btnAddbuilding.Text = "新增单体";
            btnAddbuilding.Click += btnAddbuilding_Click;
            // 
            // btnDelbuilding
            // 
            btnDelbuilding.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            btnDelbuilding.Image = (System.Drawing.Image)resources.GetObject("btnDelbuilding.Image");
            btnDelbuilding.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnDelbuilding.Name = "btnDelbuilding";
            btnDelbuilding.Size = new System.Drawing.Size(60, 22);
            btnDelbuilding.Text = "移除单体";
            btnDelbuilding.Click += btnDelbuilding_Click;
            // 
            // BuildingSettingWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(834, 453);
            Controls.Add(panel3);
            Controls.Add(panel4);
            Controls.Add(panel1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "BuildingSettingWindow";
            Padding = new System.Windows.Forms.Padding(3);
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "单体设置";
            Load += BuildingSettingWindow_Load;
            ((System.ComponentModel.ISupportInitialize)dgvLevels).EndInit();
            panel1.ResumeLayout(false);
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            toolStrip1.ResumeLayout(false);
            toolStrip1.PerformLayout();
            panel4.ResumeLayout(false);
            panel4.PerformLayout();
            toolStrip2.ResumeLayout(false);
            toolStrip2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DataGridView dgvLevels;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox listBuildings;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btnAddbuilding;
        private System.Windows.Forms.ToolStripButton btnDelbuilding;
        private System.Windows.Forms.ToolStripButton btnInitLevel;
        private System.Windows.Forms.ToolStripButton btnEditHeight;
        private System.Windows.Forms.ToolStripButton btnSetFirstHeight;
        private System.Windows.Forms.ToolStripButton btnInsertLevels;
        private System.Windows.Forms.ToolStripButton btnRechristenLevel;
        private System.Windows.Forms.ToolStripButton btnDelLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn LevelId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvLevelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvAHeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvSHeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvAElevation;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvSElevation;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvLevelType;
    }
}