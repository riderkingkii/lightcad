﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Building
{
    public partial class AddBuildingWindow : Form
    {
        List<string> buildingNames = new List<string>();
        BuildingCollection lcBuildings;
        LcDocument lcDocument;
        public AddBuildingWindow(LcDocument lcDocument)
        {
            InitializeComponent();
            this.lcBuildings = lcDocument.Buildings;
            buildingNames = lcBuildings.Select(c => c.Name).ToList();
            this.lcDocument = lcDocument;
        }

        private void textName_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.textName.Text))
            {
                if (buildingNames.Contains(this.textName.Text))
                {
                    this.textName.ForeColor = Color.Red;
                    lbText.Text = "已存在单体名称";
                    lbText.Visible=true;
                }
                else
                {
                    this.textName.ForeColor = Color.Black;
                    lbText.Visible = false;

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.textName.Text))
            {
                if (!buildingNames.Contains(this.textName.Text))
                {
                    LcBuilding lcBuilding = lcDocument.CreateObject<LcBuilding>();
                    lcBuilding.Name = this.textName.Text;
                    this.lcBuildings.Add(lcBuilding);
                    this.Close();
                    return;
                }
            }
            lbText.Text = "请输入正确的单体名";
            lbText.Visible = true;
        }
    }
}
