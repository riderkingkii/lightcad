﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Building
{
    public partial class SetLevelHeightWindow : Form
    {
        public SetLevelHeightWindow(string text)
        {
            InitializeComponent();
            this.Text = "设置" + text;
            this.lbText.Text = text + "高度：";
        }

        private void textHeight_TextChanged(object sender, EventArgs e)
        {
            if (!double.TryParse(this.textHeight.Text, out Height))
            {
                this.textHeight.ForeColor = Color.Red;
            }
            else
            {
                if (Height < 0)
                    this.textHeight.ForeColor = Color.Red;
                else
                    this.textHeight.ForeColor = Color.Black;
            }
        }
        public double Height;
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Height >= 0)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }

        }
    }
}
