﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Building
{
    public partial class InitLevelsWindow : Form
    {
        LcDocument LcDocument;
        LcBuilding LcBuilding;
        public InitLevelsWindow(LcDocument lcDocument, LcBuilding lcBuilding)
        {
            InitializeComponent();
            this.LcDocument = lcDocument;   
            this.LcBuilding = lcBuilding;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            int BasementCount;
            if (int.TryParse(this.textBasementCount.Text, out BasementCount))
            {
                if (BasementCount<0)
                {
                    return;
                }
                int GroundCount;
                if (int.TryParse(this.textGroundCount.Text, out GroundCount))
                {
                    if (GroundCount < 0)
                    {
                        return;
                    }
                    int RoofCount;
                    if (int.TryParse(this.textRoofCount.Text, out RoofCount))
                    {
                        if (RoofCount < 0)
                        {
                            return;
                        }
                        LcLevelCollection lcLevels = new LcLevelCollection();   
                        for (int i = BasementCount; i > 0; i--)
                        {
                            LcLevel lcLevel = LcDocument.CreateObject<LcLevel>();
                            lcLevel.Name = $"B{i}F";
                            lcLevel.Index = -i;
                            lcLevel.Type = LcLevelType.BFloor;
                            lcLevels.Add(lcLevel);
                        }
                        for (int i = 1; i <= GroundCount; i++)
                        {
                            LcLevel lcLevel = LcDocument.CreateObject<LcLevel>();
                            lcLevel.Name = $"{i}F";
                            lcLevel.Index = i;
                            lcLevel.Type = LcLevelType.Floor;

                            lcLevels.Add(lcLevel);
                        }
                        for (int i = 1; i <= RoofCount; i++)
                        {
                            LcLevel lcLevel = LcDocument.CreateObject<LcLevel>();
                            lcLevel.Name = $"R{i}F";
                            lcLevel.Index = GroundCount+i;
                            lcLevel.Type = LcLevelType.RFloor;
                            lcLevels.Add(lcLevel);
                        }
                        LcBuilding.Levels= lcLevels;
                        this.Close();
                    }
                }
            }
        }

        private void textBasementCount_TextChanged(object sender, EventArgs e)
        {
            int BasementCount;
            if (!int.TryParse(this.textBasementCount.Text, out BasementCount))
            {
                this.lbDX.Visible = true;
                this.lbDX.Text = "请输入阿拉伯数字";
                this.lbDX.ForeColor = Color.Red;

            }
            else
            {
                if (BasementCount < 0)
                {
                    this.lbDX.Visible = true;
                    this.lbDX.Text = "请输入正值";
                    this.lbDX.ForeColor = Color.Red;
                    return;
                }
                this.lbDX.Visible = true;
                this.lbDX.Text = "√";
                this.lbDX.ForeColor = Color.Green;
            }
        }

        private void textGroundCount_TextChanged(object sender, EventArgs e)
        {
            int GroundCount;
            if (!int.TryParse(this.textGroundCount.Text, out GroundCount))
            {
                this.lbDS.Visible = true;
                this.lbDS.Text = "请输入阿拉伯数字";
                this.lbDS.ForeColor = Color.Red;

            }
            else
            {
                if (GroundCount < 0)
                {
                    this.lbDS.Visible = true;
                    this.lbDS.Text = "请输入正值";
                    this.lbDS.ForeColor = Color.Red;
                    return;
                }
                this.lbDS.Visible = true;
                this.lbDS.Text = "√";
                this.lbDS.ForeColor = Color.Green;
            }
        }

        private void textRoofCount_TextChanged(object sender, EventArgs e)
        {
            int RoofCount;
            if (!int.TryParse(this.textRoofCount.Text, out RoofCount))
            {
                this.lbWD.Visible = true;
                this.lbWD.Text = "请输入阿拉伯数字";
                this.lbWD.ForeColor = Color.Red;

            }
            else
            {
                if (RoofCount < 0)
                {
                    this.lbWD.Visible = true;
                    this.lbWD.Text = "请输入正值";
                    this.lbWD.ForeColor = Color.Red;
                    return;
                }
                this.lbWD.Visible = true;
                this.lbWD.Text = "√";
                this.lbWD.ForeColor = Color.Green;
            }
        }
    }
}
