﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Building
{
    public partial class InsertLevelWindow : Form
    {
        LcDocument lcDocument; LcBuilding lcBuilding; LcLevel selectLLevel;
        public InsertLevelWindow(LcDocument lcDocument, LcBuilding lcBuilding, LcLevel selectLLevel)
        {
            InitializeComponent();
            this.lcDocument = lcDocument;
            this.lcBuilding = lcBuilding;
            this.selectLLevel = selectLLevel;
            InitCbLevelType();
            this.cbInsetLevelType.SelectedText = selectLLevel.Type.ToString();

        }
        public void InitCbLevelType()
        {
            Dictionary<string, LcLevelType> pairs = new Dictionary<string, LcLevelType>
            {
                { "地下", LcLevelType.BFloor },
                { "屋顶", LcLevelType.RFloor },
                { "地上", LcLevelType.Floor }
            };
            this.cbInsetLevelType.DataSource = pairs;
            this.cbInsetLevelType.DisplayMember = "Key";
        }
        private void InsertLevelWindow_Load(object sender, EventArgs e)
        {
            InitcbLevel();
        }
        public void InitcbLevel()
        {
            LcLevelType lcLevelType = (LcLevelType)this.cbInsetLevelType.SelectedValue;
            this.cbInsetLevel.DataSource = lcBuilding.Levels.Where(x => x.Type == lcLevelType).ToList();
            this.cbInsetLevel.DisplayMember = "Name";


        }

        private void textInsetCount_TextChanged(object sender, EventArgs e)
        {
            int count;
            if (!int.TryParse(this.textInsetCount.Text, out count))
            {

                this.textInsetCount.ForeColor = Color.Red;

            }
            else
            {
                if (count < 0)
                {
                    this.textInsetCount.ForeColor = Color.Red;
                    return;
                }
                this.textInsetCount.ForeColor = Color.Black;
            }
        }

        private void textHeight_TextChanged(object sender, EventArgs e)
        {
            int height;
            if (!int.TryParse(this.textHeight.Text, out height))
            {

                this.textHeight.ForeColor = Color.Red;

            }
            else
            {
                if (height < 0)
                {
                    this.textHeight.ForeColor = Color.Red;
                    return;
                }
                this.textHeight.ForeColor = Color.Black;
            }
        }

        private void bntOk_Click(object sender, EventArgs e)
        {
            int count;
            if (!int.TryParse(this.textInsetCount.Text, out count))
            {
                int height;
                if (!int.TryParse(this.textHeight.Text, out height))
                {
                    LcLevelType lcLevelType = (LcLevelType)this.cbInsetLevelType.SelectedValue;
                    for (int i = 1; i <= count; i++)
                    {
                        LcLevel newLevel = lcDocument.CreateObject<LcLevel>();
                        newLevel.Height = height;
                        newLevel.Index = selectLLevel.Index + i;
                        switch (lcLevelType)
                        {
                            case LcLevelType.RFloor:
                                newLevel.Name = selectLLevel.Name;
                                break;
                            case LcLevelType.Floor:
                                break;
                            case LcLevelType.BFloor:
                                break;
                            default:
                                break;
                        }
                    }


                }
            }
        }
    }
}
