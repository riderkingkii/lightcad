﻿namespace LightCAD.UI
{
    partial class DrawingFrameWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DrawingFrameWindow));
            btnOk = new System.Windows.Forms.Button();
            panel1 = new System.Windows.Forms.Panel();
            panel2 = new System.Windows.Forms.Panel();
            panel4 = new System.Windows.Forms.Panel();
            userGroup = new System.Windows.Forms.GroupBox();
            textWidth = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            textHeight = new System.Windows.Forms.TextBox();
            btnSelectPoint = new System.Windows.Forms.Button();
            label3 = new System.Windows.Forms.Label();
            modelGroup = new System.Windows.Forms.GroupBox();
            modelFrame = new System.Windows.Forms.ComboBox();
            label1 = new System.Windows.Forms.Label();
            panel3 = new System.Windows.Forms.Panel();
            listView1 = new System.Windows.Forms.ListView();
            panel5 = new System.Windows.Forms.Panel();
            label4 = new System.Windows.Forms.Label();
            cbBuildings = new System.Windows.Forms.ComboBox();
            chisUser = new System.Windows.Forms.CheckBox();
            label6 = new System.Windows.Forms.Label();
            imageList1 = new System.Windows.Forms.ImageList(components);
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            panel4.SuspendLayout();
            userGroup.SuspendLayout();
            modelGroup.SuspendLayout();
            panel3.SuspendLayout();
            panel5.SuspendLayout();
            SuspendLayout();
            // 
            // btnOk
            // 
            btnOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOk.Location = new System.Drawing.Point(447, 14);
            btnOk.Name = "btnOk";
            btnOk.Size = new System.Drawing.Size(80, 30);
            btnOk.TabIndex = 0;
            btnOk.Text = "确定";
            btnOk.UseVisualStyleBackColor = true;
            btnOk.Click += btnOk_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(btnOk);
            panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            panel1.Location = new System.Drawing.Point(5, 229);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(539, 56);
            panel1.TabIndex = 1;
            // 
            // panel2
            // 
            panel2.Controls.Add(panel4);
            panel2.Controls.Add(panel3);
            panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Location = new System.Drawing.Point(5, 5);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(539, 224);
            panel2.TabIndex = 2;
            // 
            // panel4
            // 
            panel4.Controls.Add(userGroup);
            panel4.Controls.Add(modelGroup);
            panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            panel4.Location = new System.Drawing.Point(0, 128);
            panel4.Name = "panel4";
            panel4.Padding = new System.Windows.Forms.Padding(3);
            panel4.Size = new System.Drawing.Size(539, 96);
            panel4.TabIndex = 25;
            // 
            // userGroup
            // 
            userGroup.Controls.Add(textWidth);
            userGroup.Controls.Add(label2);
            userGroup.Controls.Add(textHeight);
            userGroup.Controls.Add(btnSelectPoint);
            userGroup.Controls.Add(label3);
            userGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            userGroup.Location = new System.Drawing.Point(273, 3);
            userGroup.Name = "userGroup";
            userGroup.Size = new System.Drawing.Size(263, 90);
            userGroup.TabIndex = 30;
            userGroup.TabStop = false;
            userGroup.Text = "自定义";
            // 
            // textWidth
            // 
            textWidth.Location = new System.Drawing.Point(108, 17);
            textWidth.Name = "textWidth";
            textWidth.Size = new System.Drawing.Size(100, 23);
            textWidth.TabIndex = 16;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 23);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(64, 17);
            label2.TabIndex = 17;
            label2.Text = "宽度(W)：";
            // 
            // textHeight
            // 
            textHeight.Location = new System.Drawing.Point(108, 46);
            textHeight.Name = "textHeight";
            textHeight.Size = new System.Drawing.Size(100, 23);
            textHeight.TabIndex = 18;
            // 
            // btnSelectPoint
            // 
            btnSelectPoint.Location = new System.Drawing.Point(213, 22);
            btnSelectPoint.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnSelectPoint.Name = "btnSelectPoint";
            btnSelectPoint.Size = new System.Drawing.Size(41, 47);
            btnSelectPoint.TabIndex = 20;
            btnSelectPoint.Text = "框选范围";
            btnSelectPoint.UseVisualStyleBackColor = true;
            btnSelectPoint.Click += btnSelectPoint_Click;
            btnSelectPoint.MouseHover += btnSelectPoint_MouseHover;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(16, 52);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(61, 17);
            label3.TabIndex = 19;
            label3.Text = "高度(H)：";
            // 
            // modelGroup
            // 
            modelGroup.Controls.Add(modelFrame);
            modelGroup.Controls.Add(label1);
            modelGroup.Dock = System.Windows.Forms.DockStyle.Left;
            modelGroup.Location = new System.Drawing.Point(3, 3);
            modelGroup.Name = "modelGroup";
            modelGroup.Size = new System.Drawing.Size(270, 90);
            modelGroup.TabIndex = 28;
            modelGroup.TabStop = false;
            modelGroup.Text = "模板";
            // 
            // modelFrame
            // 
            modelFrame.FormattingEnabled = true;
            modelFrame.Location = new System.Drawing.Point(108, 22);
            modelFrame.Name = "modelFrame";
            modelFrame.Size = new System.Drawing.Size(100, 25);
            modelFrame.TabIndex = 24;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 22);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(88, 17);
            label1.TabIndex = 23;
            label1.Text = "图框模板(M)：";
            // 
            // panel3
            // 
            panel3.Controls.Add(listView1);
            panel3.Controls.Add(panel5);
            panel3.Dock = System.Windows.Forms.DockStyle.Top;
            panel3.Location = new System.Drawing.Point(0, 0);
            panel3.Name = "panel3";
            panel3.Padding = new System.Windows.Forms.Padding(3);
            panel3.Size = new System.Drawing.Size(539, 128);
            panel3.TabIndex = 29;
            // 
            // listView1
            // 
            listView1.CheckBoxes = true;
            listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            listView1.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            listView1.Location = new System.Drawing.Point(254, 3);
            listView1.Name = "listView1";
            listView1.Size = new System.Drawing.Size(282, 122);
            listView1.TabIndex = 32;
            listView1.TileSize = new System.Drawing.Size(30, 30);
            listView1.UseCompatibleStateImageBehavior = false;
            listView1.View = System.Windows.Forms.View.List;
            // 
            // panel5
            // 
            panel5.Controls.Add(label4);
            panel5.Controls.Add(cbBuildings);
            panel5.Controls.Add(chisUser);
            panel5.Controls.Add(label6);
            panel5.Dock = System.Windows.Forms.DockStyle.Left;
            panel5.Location = new System.Drawing.Point(3, 3);
            panel5.Name = "panel5";
            panel5.Padding = new System.Windows.Forms.Padding(3);
            panel5.Size = new System.Drawing.Size(251, 122);
            panel5.TabIndex = 33;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(6, 13);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(60, 17);
            label4.TabIndex = 29;
            label4.Text = "单体(B)：";
            // 
            // cbBuildings
            // 
            cbBuildings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cbBuildings.FormattingEnabled = true;
            cbBuildings.Location = new System.Drawing.Point(69, 7);
            cbBuildings.Name = "cbBuildings";
            cbBuildings.Size = new System.Drawing.Size(100, 25);
            cbBuildings.TabIndex = 28;
            cbBuildings.SelectedIndexChanged += cbBuildings_SelectedIndexChanged;
            // 
            // chisUser
            // 
            chisUser.AutoSize = true;
            chisUser.Checked = true;
            chisUser.CheckState = System.Windows.Forms.CheckState.Checked;
            chisUser.Location = new System.Drawing.Point(72, 53);
            chisUser.Name = "chisUser";
            chisUser.Size = new System.Drawing.Size(87, 21);
            chisUser.TabIndex = 25;
            chisUser.Text = "自定义尺寸";
            chisUser.UseVisualStyleBackColor = true;
            chisUser.CheckedChanged += chisUser_CheckedChanged;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(192, 13);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(58, 17);
            label6.TabIndex = 31;
            label6.Text = "楼层(L)：";
            // 
            // imageList1
            // 
            imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            imageList1.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("imageList1.ImageStream");
            imageList1.TransparentColor = System.Drawing.Color.Transparent;
            imageList1.Images.SetKeyName(0, "LC.jpg");
            // 
            // DrawingFrameWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(549, 290);
            Controls.Add(panel2);
            Controls.Add(panel1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "DrawingFrameWindow";
            Padding = new System.Windows.Forms.Padding(5);
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "插入图框";
            panel1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel4.ResumeLayout(false);
            userGroup.ResumeLayout(false);
            userGroup.PerformLayout();
            modelGroup.ResumeLayout(false);
            modelGroup.PerformLayout();
            panel3.ResumeLayout(false);
            panel5.ResumeLayout(false);
            panel5.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSelectPoint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textHeight;
        private System.Windows.Forms.TextBox textWidth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox frameName;
        private System.Windows.Forms.CheckBox chisUser;
        private System.Windows.Forms.ComboBox modelFrame;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox userGroup;
        private System.Windows.Forms.GroupBox modelGroup;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbBuildings;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ImageList imageList1;
    }
}