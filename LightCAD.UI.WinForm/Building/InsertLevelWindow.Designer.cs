﻿namespace LightCAD.UI.Building
{
    partial class InsertLevelWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            bntOk = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            textInsetCount = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            cbInsetLevel = new System.Windows.Forms.ComboBox();
            cbInsetLevelType = new System.Windows.Forms.ComboBox();
            label3 = new System.Windows.Forms.Label();
            textHeight = new System.Windows.Forms.TextBox();
            label4 = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // bntOk
            // 
            bntOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            bntOk.Location = new System.Drawing.Point(138, 148);
            bntOk.Name = "bntOk";
            bntOk.Size = new System.Drawing.Size(75, 30);
            bntOk.TabIndex = 0;
            bntOk.Text = "确定";
            bntOk.UseVisualStyleBackColor = true;
            bntOk.Click += bntOk_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(17, 14);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(68, 17);
            label1.TabIndex = 1;
            label1.Text = "插入数量：";
            // 
            // textInsetCount
            // 
            textInsetCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            textInsetCount.Location = new System.Drawing.Point(91, 12);
            textInsetCount.Name = "textInsetCount";
            textInsetCount.Size = new System.Drawing.Size(121, 23);
            textInsetCount.TabIndex = 2;
            textInsetCount.TextChanged += textInsetCount_TextChanged;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(17, 74);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(68, 17);
            label2.TabIndex = 3;
            label2.Text = "插入楼层：";
            // 
            // cbInsetLevel
            // 
            cbInsetLevel.FormattingEnabled = true;
            cbInsetLevel.Location = new System.Drawing.Point(91, 72);
            cbInsetLevel.Name = "cbInsetLevel";
            cbInsetLevel.Size = new System.Drawing.Size(121, 25);
            cbInsetLevel.TabIndex = 4;
            // 
            // cbInsetLevelType
            // 
            cbInsetLevelType.FormattingEnabled = true;
            cbInsetLevelType.Items.AddRange(new object[] { "地下", "地上", "屋顶" });
            cbInsetLevelType.Location = new System.Drawing.Point(91, 41);
            cbInsetLevelType.Name = "cbInsetLevelType";
            cbInsetLevelType.Size = new System.Drawing.Size(121, 25);
            cbInsetLevelType.TabIndex = 6;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(17, 43);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(68, 17);
            label3.TabIndex = 5;
            label3.Text = "插入位置：";
            // 
            // textHeight
            // 
            textHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            textHeight.Location = new System.Drawing.Point(91, 103);
            textHeight.Name = "textHeight";
            textHeight.Size = new System.Drawing.Size(121, 23);
            textHeight.TabIndex = 8;
            textHeight.TextChanged += textHeight_TextChanged;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(17, 105);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(44, 17);
            label4.TabIndex = 7;
            label4.Text = "层高：";
            // 
            // InsertLevelWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(230, 195);
            Controls.Add(textHeight);
            Controls.Add(label4);
            Controls.Add(cbInsetLevelType);
            Controls.Add(label3);
            Controls.Add(cbInsetLevel);
            Controls.Add(label2);
            Controls.Add(textInsetCount);
            Controls.Add(label1);
            Controls.Add(bntOk);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "InsertLevelWindow";
            Padding = new System.Windows.Forms.Padding(5);
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "插入楼层";
            Load += InsertLevelWindow_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button bntOk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textInsetCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbInsetLevel;
        private System.Windows.Forms.ComboBox cbInsetLevelType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textHeight;
        private System.Windows.Forms.Label label4;
    }
}