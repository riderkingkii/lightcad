﻿namespace LightCAD.UI.Building
{
    partial class AddBuildingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            la = new System.Windows.Forms.Label();
            textName = new System.Windows.Forms.TextBox();
            button1 = new System.Windows.Forms.Button();
            lbText = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // la
            // 
            la.AutoSize = true;
            la.Location = new System.Drawing.Point(19, 25);
            la.Name = "la";
            la.Size = new System.Drawing.Size(68, 17);
            la.TabIndex = 0;
            la.Text = "单体名称：";
            // 
            // textName
            // 
            textName.Location = new System.Drawing.Point(93, 22);
            textName.Name = "textName";
            textName.Size = new System.Drawing.Size(143, 23);
            textName.TabIndex = 1;
            textName.TextChanged += textName_TextChanged;
            // 
            // button1
            // 
            button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            button1.Location = new System.Drawing.Point(161, 96);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(75, 30);
            button1.TabIndex = 2;
            button1.Text = "确定";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // lbText
            // 
            lbText.AutoSize = true;
            lbText.ForeColor = System.Drawing.Color.Red;
            lbText.Location = new System.Drawing.Point(99, 49);
            lbText.Name = "lbText";
            lbText.Size = new System.Drawing.Size(92, 17);
            lbText.TabIndex = 3;
            lbText.Text = "已存在单体名称";
            lbText.Visible = false;
            // 
            // AddBuildingWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(249, 138);
            Controls.Add(lbText);
            Controls.Add(button1);
            Controls.Add(textName);
            Controls.Add(la);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "AddBuildingWindow";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "增加单体";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label la;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbText;
    }
}