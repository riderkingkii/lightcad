﻿using LightCAD.Core;
using LightCAD.Drawing.Actions;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LightCAD.MathLib;

namespace LightCAD.UI
{
    public partial class DrawingFrameWindow : FormBase, IFrameDefWindow
    {
        public DrawingFrameWindow()
        {
            InitializeComponent();

        }
        public double width { get { return Box == null ? 0 : ((Box2)Box).Width; } }
        public double hight { get { return Box == null ? 0 : ((Box2)Box).Height; } }
        private DrawingFrameAction drawingFrameAction;

        public DrawingFrameWindow(params object[] args) : this()
        {
            if (args != null && args.Length > 0)
            {
                this.drawingFrameAction = (DrawingFrameAction)args[0];
            }
            this.chisUser.Checked = !this.chisUser.Checked;
            this.chisUser.Checked = true;
            InitBuilding();
        }
        public LcBuilding LcBuilding { get; set; }
        public List<LcLevel> LcLevels { get; set; }
        public void InitBuilding()
        {
            LcDocument doc = drawingFrameAction.LcDocument;
            this.cbBuildings.DataSource = doc.Buildings;
            this.cbBuildings.DisplayMember = "Name";
        }
        //获取楼层信息
        public void InitLevel()
        {
            this.listView1.View = System.Windows.Forms.View.List;
            this.listView1.Items.Clear();
            //this.listView1.SmallImageList = this.imageList1;
            this.listView1.BeginUpdate();

            foreach (var lcLevel in this.LcBuilding.Levels)
            {

                ListViewItem lvi = new ListViewItem();
                //lvi.ImageIndex = 0;
                lvi.Text = lcLevel.Name;
                lvi.Tag = lcLevel;
                this.listView1.Items.Add(lvi);
            }

            this.listView1.EndUpdate();
            //this.listView1.DataSource = this.LcBuilding.Levels;
            //this.listView1.DisplayMember = "Name";
        }
        public string CurrentAction { get; set; }
        public List<LcElement> Elements { get; set; }
        public string FrameName { get; set; }

        public bool IsActive => true;

        public Type WinType => this.GetType();

        public Dictionary<string, object> FrameProperties { get; set; }
        /// <summary>
        /// 图纸二维盒子
        /// </summary>
        public Box2 Box
        {
            get
            {

                if (!double.TryParse(this.textWidth.Text, out double w))
                {
                    w = 0;
                }
                if (!double.TryParse(this.textHeight.Text, out double h))
                {
                    h = 0;
                }
                return new Box2((Vector2)BasePoint, w, h);
            }
            set
            {

                if (value == null)
                {
                    this.textWidth.Text = "0";
                    this.textHeight.Text = "0";
                    BasePoint = new Vector2(0, 0);
                }
                else
                {
                    this.textWidth.Text = value.Width.ToString();
                    this.textHeight.Text = value.Height.ToString();
                    this.BasePoint = value.Min;
                }
            }
        }
        public Vector2 BasePoint { get; set; }


        private void btnOk_Click(object sender, EventArgs e)
        {
            LcLevels = new List<LcLevel>();
            foreach (ListViewItem listViewItem in this.listView1.CheckedItems)
            {
                LcLevels.Add(listViewItem.Tag as LcLevel);
            }
            SetFrameName();
            if (BasePoint == null)
            {
                MessageBox.Show("请框选范围");
                return;
            }
            this.CurrentAction = "OK";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnSelectPoint_Click(object sender, EventArgs e)
        {
            this.CurrentAction = "SelectPoint";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void chisUser_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chisUser.Checked)
            {
                this.modelGroup.Enabled = false;
                this.userGroup.Enabled = true;
            }
            else
            {
                this.modelGroup.Enabled = true;
                this.userGroup.Enabled = false;
            }
        }
        private void btnSelectPoint_MouseHover(object sender, EventArgs e)
        {
            // 创建the ToolTip 
            ToolTip toolTip1 = new ToolTip();
            // 设置显示样式
            toolTip1.AutoPopDelay = 5000;//提示信息的可见时间
            toolTip1.InitialDelay = 500;//事件触发多久后出现提示
            toolTip1.ReshowDelay = 500;//指针从一个控件移向另一个控件时，经过多久才会显示下一个提示框
            toolTip1.ShowAlways = true;//是否显示提示框
            //  设置伴随的对象.
            toolTip1.SetToolTip(btnSelectPoint, "从图纸中框选范围/输入宽高选择定位点");//设置提示按钮和提示内容
        }

        private void cbBuildings_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedBuilding = (LcBuilding)this.cbBuildings.SelectedValue;
            if (this.LcBuilding != selectedBuilding)
            {
                this.LcBuilding = selectedBuilding;
                InitLevel();
            }
            SetFrameName();
        }

        private void cbLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var selectedLcLevel = (LcLevel)this.listView1.SelectedItems;
            //this.LcLevel = selectedLcLevel;
            //SetFrameName();

        }

        private void frameName_TextChanged(object sender, EventArgs e)
        {
            SetFrameName();
        }
        private void SetFrameName()
        {
            if (this.LcBuilding == null || LcLevels == null || LcLevels.Count == 0) return;
            this.FrameName = $"{this.LcBuilding.Name}_{LcLevels.FirstOrDefault().Name}-{LcLevels.LastOrDefault().Name}";

        }
    }
}
