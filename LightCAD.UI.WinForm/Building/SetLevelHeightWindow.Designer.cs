﻿namespace LightCAD.UI.Building
{
    partial class SetLevelHeightWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnOk = new System.Windows.Forms.Button();
            textHeight = new System.Windows.Forms.TextBox();
            lbText = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // btnOk
            // 
            btnOk.Location = new System.Drawing.Point(111, 64);
            btnOk.Name = "btnOk";
            btnOk.Size = new System.Drawing.Size(75, 23);
            btnOk.TabIndex = 0;
            btnOk.Text = "确定";
            btnOk.UseVisualStyleBackColor = true;
            btnOk.Click += btnOk_Click;
            // 
            // textHeight
            // 
            textHeight.Location = new System.Drawing.Point(86, 12);
            textHeight.Name = "textHeight";
            textHeight.Size = new System.Drawing.Size(100, 23);
            textHeight.TabIndex = 1;
            textHeight.TextChanged += textHeight_TextChanged;
            // 
            // lbText
            // 
            lbText.AutoSize = true;
            lbText.Location = new System.Drawing.Point(12, 15);
            lbText.Name = "lbText";
            lbText.Size = new System.Drawing.Size(68, 17);
            lbText.TabIndex = 2;
            lbText.Text = "楼层高度：";
            // 
            // SetLevelHeightWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(224, 106);
            Controls.Add(lbText);
            Controls.Add(textHeight);
            Controls.Add(btnOk);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "SetLevelHeightWindow";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "设置层高";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox textHeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbText;
    }
}