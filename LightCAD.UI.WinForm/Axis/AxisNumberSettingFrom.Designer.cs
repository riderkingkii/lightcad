﻿namespace LightCAD.UI.Axis
{
    partial class AxisNumberSettingFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel1 = new System.Windows.Forms.Panel();
            btnInsertAxisNum = new System.Windows.Forms.Button();
            panel2 = new System.Windows.Forms.Panel();
            tbRadius = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            textFirstAxisNum = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            cbOrderingRule = new System.Windows.Forms.CheckedListBox();
            cbLabelDir = new System.Windows.Forms.CheckedListBox();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(btnInsertAxisNum);
            panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            panel1.Location = new System.Drawing.Point(0, 162);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(436, 60);
            panel1.TabIndex = 0;
            // 
            // btnInsertAxisNum
            // 
            btnInsertAxisNum.Location = new System.Drawing.Point(349, 18);
            btnInsertAxisNum.Name = "btnInsertAxisNum";
            btnInsertAxisNum.Size = new System.Drawing.Size(75, 30);
            btnInsertAxisNum.TabIndex = 0;
            btnInsertAxisNum.Text = "插入轴号";
            btnInsertAxisNum.UseVisualStyleBackColor = true;
            btnInsertAxisNum.Click += btnInsertAxisNum_Click;
            // 
            // panel2
            // 
            panel2.Controls.Add(tbRadius);
            panel2.Controls.Add(label2);
            panel2.Controls.Add(textFirstAxisNum);
            panel2.Controls.Add(label1);
            panel2.Controls.Add(cbOrderingRule);
            panel2.Controls.Add(cbLabelDir);
            panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Location = new System.Drawing.Point(0, 0);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(436, 162);
            panel2.TabIndex = 1;
            // 
            // tbRadius
            // 
            tbRadius.Location = new System.Drawing.Point(209, 39);
            tbRadius.Name = "tbRadius";
            tbRadius.Size = new System.Drawing.Size(100, 23);
            tbRadius.TabIndex = 5;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(135, 42);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(68, 17);
            label2.TabIndex = 4;
            label2.Text = "轴圈半径：";
            // 
            // textFirstAxisNum
            // 
            textFirstAxisNum.Location = new System.Drawing.Point(209, 10);
            textFirstAxisNum.Name = "textFirstAxisNum";
            textFirstAxisNum.Size = new System.Drawing.Size(100, 23);
            textFirstAxisNum.TabIndex = 3;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(135, 13);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(68, 17);
            label1.TabIndex = 2;
            label1.Text = "起始轴号：";
            // 
            // cbOrderingRule
            // 
            cbOrderingRule.BackColor = System.Drawing.SystemColors.Control;
            cbOrderingRule.Dock = System.Windows.Forms.DockStyle.Bottom;
            cbOrderingRule.FormattingEnabled = true;
            cbOrderingRule.Location = new System.Drawing.Point(120, 86);
            cbOrderingRule.Name = "cbOrderingRule";
            cbOrderingRule.Size = new System.Drawing.Size(316, 76);
            cbOrderingRule.TabIndex = 1;
            cbOrderingRule.ItemCheck += cbOrderingRule_ItemCheck;
            // 
            // cbLabelDir
            // 
            cbLabelDir.BackColor = System.Drawing.SystemColors.Control;
            cbLabelDir.Dock = System.Windows.Forms.DockStyle.Left;
            cbLabelDir.FormattingEnabled = true;
            cbLabelDir.Location = new System.Drawing.Point(0, 0);
            cbLabelDir.Name = "cbLabelDir";
            cbLabelDir.Size = new System.Drawing.Size(120, 162);
            cbLabelDir.TabIndex = 0;
            cbLabelDir.ItemCheck += cbLabelDir_ItemCheck;
            // 
            // AxisNumberSettingFrom
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(436, 222);
            Controls.Add(panel2);
            Controls.Add(panel1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "AxisNumberSettingFrom";
            ShowIcon = false;
            ShowInTaskbar = false;
            Text = "轴号设置";
            Load += AxisNumberSettingFrom_Load;
            panel1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckedListBox cbLabelDir;
        private System.Windows.Forms.TextBox tbRadius;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textFirstAxisNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox cbOrderingRule;
        private System.Windows.Forms.Button btnInsertAxisNum;
    }
}