﻿using LightCAD.Core;
using LightCAD.Drawing.Actions;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Axis
{
    public partial class AxisNumberSettingFrom : FormBase, IAxisLineManageFrom
    {
        public AxisNumberSettingFrom()
        {
            InitializeComponent();
            this.cbLabelDir.Items.Clear();
            this.cbLabelDir.Items.Add("双侧标注");
            this.cbLabelDir.Items.Add("单侧标注");
            this.cbLabelDir.Items.Add("对侧标注");
            this.cbLabelDir.CheckOnClick = true;

            this.cbOrderingRule.Items.Clear();
            this.cbOrderingRule.Items.Add("1A=>2A=>3A");
            this.cbOrderingRule.Items.Add("1A=>1B=>1C");
            this.cbOrderingRule.CheckOnClick = true;
            this.cbLabelDir.SetItemChecked(0, true);
            this.cbOrderingRule.SetItemChecked(0, true);
        }
        private AxisGridAction axisLineAction;
        public AxisNumberSettingFrom(params object[] args) : this()
        {
            if (args != null && args.Length > 0)
            {
                this.axisLineAction = (AxisGridAction)args[0];
            }
        }

        public string CurrentAction { get; set; }
        public LcAxisGrid lcAxisGrid { get; set; }

        public bool IsActive => true;

        public Type WinType => this.GetType();
        private double GetRadius()
        {
            double redius = 0;
            double.TryParse(tbRadius.Text, out redius);
            return redius;
        }
        public double Radius { get; set; }
        public string FirstAxisNum { get; set; }
        public string OrderingRule { get; set; }
        public string AxisNumType { get; set; }

        private void AxisNumberSettingFrom_Load(object sender, EventArgs e)
        {



        }

        private void btnInsertAxisNum_Click(object sender, EventArgs e)
        {
            var dirChecked = cbLabelDir.CheckedItems[0];
            this.AxisNumType = dirChecked.ToString();
            OrderingRule = this.cbOrderingRule.CheckedItems[0].ToString();
            Radius = this.GetRadius();
            FirstAxisNum = this.textFirstAxisNum.Text;
            this.CurrentAction = "InsertAxisNum";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cbLabelDir_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < cbLabelDir.Items.Count; i++)
            {
                if (i != e.Index)//除去触发SelectedIndexChanged事件以外的选中项都处于未选中状态
                {
                    cbLabelDir.SetItemCheckState(i, System.Windows.Forms.CheckState.Unchecked);

                }

            }


        }

        private void cbOrderingRule_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < cbOrderingRule.Items.Count; i++)
            {
                if (i != e.Index)//除去触发SelectedIndexChanged事件以外的选中项都处于未选中状态
                {
                    cbOrderingRule.SetItemCheckState(i, System.Windows.Forms.CheckState.Unchecked);

                }

            }
        }
    }
}
