﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.UI.Axis
{
    public class AxisLineDrawData
    {
        public string Id { get; set; }
        /// <summary>
        /// 间隔
        /// </summary>
        public double Spacing { get; set; }
        /// <summary>
        /// 长度
        /// </summary>
        public double Length { get; set; }
        /// <summary>
        /// 绘画类型
        /// </summary>
        public DrawDirection Direction { get; set; }

        public int Count { get; set; }

    }
    public class AxisLineDrawDataCollection : KeyedCollection<string, AxisLineDrawData>
    {
        public AxisLineDrawDataCollection() { }

        protected override string GetKeyForItem(AxisLineDrawData item)
        {
            return item.Id;
        }
    }
    public enum DrawDirection
    {
        /// <summary>
        /// 上开
        /// </summary>
        Top,
        /// <summary>
        /// 下开
        /// </summary>
        Bottom,
        /// <summary>
        /// 左进
        /// </summary>
        Left, 
        /// <summary>
        /// 右进
        /// </summary>
        Right,
        /// <summary>
        /// 夹角
        /// </summary>
        Angle,
        /// <summary>
        /// 进深
        /// </summary>
        depth
    }
}
