﻿namespace LightCAD.UI.Axis
{
    partial class AxisManageFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tabControl1 = new System.Windows.Forms.TabControl();
            tabPage1 = new System.Windows.Forms.TabPage();
            panel2 = new System.Windows.Forms.Panel();
            dgvAxisLIneList = new System.Windows.Forms.DataGridView();
            dgvSpacing = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dgvCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            panel4 = new System.Windows.Forms.Panel();
            ListSpacing = new System.Windows.Forms.ListBox();
            panel6 = new System.Windows.Forms.Panel();
            rbRight = new System.Windows.Forms.RadioButton();
            rbleft = new System.Windows.Forms.RadioButton();
            rbBottom = new System.Windows.Forms.RadioButton();
            rbTop = new System.Windows.Forms.RadioButton();
            panel5 = new System.Windows.Forms.Panel();
            panel1 = new System.Windows.Forms.Panel();
            axisDrawPenFrom1 = new AxisDrawPenFrom();
            panel3 = new System.Windows.Forms.Panel();
            btnInsertAxisLine = new System.Windows.Forms.Button();
            textHeight = new System.Windows.Forms.TextBox();
            textWidth = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            button1 = new System.Windows.Forms.Button();
            tabPage2 = new System.Windows.Forms.TabPage();
            tabControl1.SuspendLayout();
            tabPage1.SuspendLayout();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvAxisLIneList).BeginInit();
            panel6.SuspendLayout();
            panel1.SuspendLayout();
            panel3.SuspendLayout();
            SuspendLayout();
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(tabPage1);
            tabControl1.Controls.Add(tabPage2);
            tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            tabControl1.Location = new System.Drawing.Point(0, 0);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new System.Drawing.Size(682, 408);
            tabControl1.TabIndex = 0;
            tabControl1.SelectedIndexChanged += tabControl1_SelectedIndexChanged;
            // 
            // tabPage1
            // 
            tabPage1.Controls.Add(panel2);
            tabPage1.Controls.Add(panel5);
            tabPage1.Controls.Add(panel1);
            tabPage1.Controls.Add(panel3);
            tabPage1.Location = new System.Drawing.Point(4, 26);
            tabPage1.Name = "tabPage1";
            tabPage1.Padding = new System.Windows.Forms.Padding(3);
            tabPage1.Size = new System.Drawing.Size(674, 378);
            tabPage1.TabIndex = 0;
            tabPage1.Text = "直线轴网";
            tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            panel2.Controls.Add(dgvAxisLIneList);
            panel2.Controls.Add(panel4);
            panel2.Controls.Add(ListSpacing);
            panel2.Controls.Add(panel6);
            panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Location = new System.Drawing.Point(339, 3);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(332, 326);
            panel2.TabIndex = 1;
            // 
            // dgvAxisLIneList
            // 
            dgvAxisLIneList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvAxisLIneList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { dgvSpacing, dgvLength, dgvCount, Id });
            dgvAxisLIneList.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvAxisLIneList.Location = new System.Drawing.Point(0, 45);
            dgvAxisLIneList.Name = "dgvAxisLIneList";
            dgvAxisLIneList.RowTemplate.Height = 25;
            dgvAxisLIneList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgvAxisLIneList.Size = new System.Drawing.Size(253, 281);
            dgvAxisLIneList.TabIndex = 0;
            dgvAxisLIneList.VirtualMode = true;
            dgvAxisLIneList.CellEndEdit += dgvAxisLIneList_CellEndEdit;
            dgvAxisLIneList.UserDeletingRow += dgvAxisLIneList_UserDeletingRow;
            // 
            // dgvSpacing
            // 
            dgvSpacing.DataPropertyName = "Spacing";
            dgvSpacing.HeaderText = "间隔";
            dgvSpacing.Name = "dgvSpacing";
            dgvSpacing.Width = 60;
            // 
            // dgvLength
            // 
            dgvLength.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dgvLength.DataPropertyName = "Length";
            dgvLength.HeaderText = "长度";
            dgvLength.Name = "dgvLength";
            // 
            // dgvCount
            // 
            dgvCount.DataPropertyName = "Count";
            dgvCount.HeaderText = "数量";
            dgvCount.Name = "dgvCount";
            dgvCount.Width = 60;
            // 
            // Id
            // 
            Id.DataPropertyName = "Id";
            Id.HeaderText = "Id";
            Id.Name = "Id";
            Id.ReadOnly = true;
            Id.Visible = false;
            // 
            // panel4
            // 
            panel4.Dock = System.Windows.Forms.DockStyle.Right;
            panel4.Location = new System.Drawing.Point(253, 45);
            panel4.Name = "panel4";
            panel4.Size = new System.Drawing.Size(16, 281);
            panel4.TabIndex = 2;
            // 
            // ListSpacing
            // 
            ListSpacing.Dock = System.Windows.Forms.DockStyle.Right;
            ListSpacing.FormattingEnabled = true;
            ListSpacing.ItemHeight = 17;
            ListSpacing.Location = new System.Drawing.Point(269, 45);
            ListSpacing.Name = "ListSpacing";
            ListSpacing.Size = new System.Drawing.Size(63, 281);
            ListSpacing.TabIndex = 1;
            ListSpacing.MouseDoubleClick += ListSpacing_MouseDoubleClick;
            // 
            // panel6
            // 
            panel6.Controls.Add(rbRight);
            panel6.Controls.Add(rbleft);
            panel6.Controls.Add(rbBottom);
            panel6.Controls.Add(rbTop);
            panel6.Dock = System.Windows.Forms.DockStyle.Top;
            panel6.Location = new System.Drawing.Point(0, 0);
            panel6.Name = "panel6";
            panel6.Size = new System.Drawing.Size(332, 45);
            panel6.TabIndex = 3;
            // 
            // rbRight
            // 
            rbRight.AutoSize = true;
            rbRight.Location = new System.Drawing.Point(185, 10);
            rbRight.Name = "rbRight";
            rbRight.Size = new System.Drawing.Size(50, 21);
            rbRight.TabIndex = 3;
            rbRight.Text = "右进";
            rbRight.UseVisualStyleBackColor = true;
            rbRight.CheckedChanged += rbRight_CheckedChanged;
            // 
            // rbleft
            // 
            rbleft.AutoSize = true;
            rbleft.Location = new System.Drawing.Point(129, 10);
            rbleft.Name = "rbleft";
            rbleft.Size = new System.Drawing.Size(50, 21);
            rbleft.TabIndex = 2;
            rbleft.Text = "左进";
            rbleft.UseVisualStyleBackColor = true;
            rbleft.CheckedChanged += rbleft_CheckedChanged;
            // 
            // rbBottom
            // 
            rbBottom.AutoSize = true;
            rbBottom.Location = new System.Drawing.Point(73, 10);
            rbBottom.Name = "rbBottom";
            rbBottom.Size = new System.Drawing.Size(50, 21);
            rbBottom.TabIndex = 1;
            rbBottom.Text = "下开";
            rbBottom.UseVisualStyleBackColor = true;
            rbBottom.CheckedChanged += rbBottom_CheckedChanged;
            // 
            // rbTop
            // 
            rbTop.AutoSize = true;
            rbTop.Checked = true;
            rbTop.Location = new System.Drawing.Point(17, 9);
            rbTop.Name = "rbTop";
            rbTop.Size = new System.Drawing.Size(50, 21);
            rbTop.TabIndex = 0;
            rbTop.TabStop = true;
            rbTop.Text = "上开";
            rbTop.UseVisualStyleBackColor = true;
            rbTop.CheckedChanged += rbTop_CheckedChanged;
            // 
            // panel5
            // 
            panel5.Dock = System.Windows.Forms.DockStyle.Left;
            panel5.Location = new System.Drawing.Point(323, 3);
            panel5.Name = "panel5";
            panel5.Size = new System.Drawing.Size(16, 326);
            panel5.TabIndex = 3;
            // 
            // panel1
            // 
            panel1.BackColor = System.Drawing.Color.Black;
            panel1.Controls.Add(axisDrawPenFrom1);
            panel1.Dock = System.Windows.Forms.DockStyle.Left;
            panel1.Location = new System.Drawing.Point(3, 3);
            panel1.MaximumSize = new System.Drawing.Size(320, 320);
            panel1.MinimumSize = new System.Drawing.Size(320, 320);
            panel1.Name = "panel1";
            panel1.Padding = new System.Windows.Forms.Padding(10);
            panel1.Size = new System.Drawing.Size(320, 320);
            panel1.TabIndex = 0;
            // 
            // axisDrawPenFrom1
            // 
            axisDrawPenFrom1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            axisDrawPenFrom1.Dock = System.Windows.Forms.DockStyle.Fill;
            axisDrawPenFrom1.Location = new System.Drawing.Point(10, 10);
            axisDrawPenFrom1.Name = "axisDrawPenFrom1";
            axisDrawPenFrom1.Size = new System.Drawing.Size(300, 300);
            axisDrawPenFrom1.TabIndex = 0;
            axisDrawPenFrom1.Paint += axisDrawPenFrom1_Paint;
            // 
            // panel3
            // 
            panel3.Controls.Add(btnInsertAxisLine);
            panel3.Controls.Add(textHeight);
            panel3.Controls.Add(textWidth);
            panel3.Controls.Add(label2);
            panel3.Controls.Add(label1);
            panel3.Controls.Add(button1);
            panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            panel3.Location = new System.Drawing.Point(3, 329);
            panel3.Name = "panel3";
            panel3.Size = new System.Drawing.Size(668, 46);
            panel3.TabIndex = 0;
            // 
            // btnInsertAxisLine
            // 
            btnInsertAxisLine.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnInsertAxisLine.Location = new System.Drawing.Point(579, 11);
            btnInsertAxisLine.Name = "btnInsertAxisLine";
            btnInsertAxisLine.Size = new System.Drawing.Size(75, 23);
            btnInsertAxisLine.TabIndex = 7;
            btnInsertAxisLine.Text = "插入轴网";
            btnInsertAxisLine.UseVisualStyleBackColor = true;
            btnInsertAxisLine.Click += btnInsertAxisLine_Click;
            // 
            // textHeight
            // 
            textHeight.Location = new System.Drawing.Point(193, 5);
            textHeight.Name = "textHeight";
            textHeight.Size = new System.Drawing.Size(54, 23);
            textHeight.TabIndex = 6;
            textHeight.Text = "0";
            // 
            // textWidth
            // 
            textWidth.Location = new System.Drawing.Point(64, 6);
            textWidth.Name = "textWidth";
            textWidth.Size = new System.Drawing.Size(61, 23);
            textWidth.TabIndex = 5;
            textWidth.Text = "0";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(140, 9);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(47, 17);
            label2.TabIndex = 4;
            label2.Text = "总进深:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(14, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(47, 17);
            label1.TabIndex = 3;
            label1.Text = "总开间:";
            // 
            // button1
            // 
            button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            button1.Location = new System.Drawing.Point(336, 6);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(110, 23);
            button1.TabIndex = 0;
            button1.Text = "清空当页数据";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // tabPage2
            // 
            tabPage2.Location = new System.Drawing.Point(4, 26);
            tabPage2.Name = "tabPage2";
            tabPage2.Padding = new System.Windows.Forms.Padding(3);
            tabPage2.Size = new System.Drawing.Size(674, 378);
            tabPage2.TabIndex = 1;
            tabPage2.Text = "弧线轴网";
            tabPage2.UseVisualStyleBackColor = true;
            // 
            // AxisManageFrom
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(682, 408);
            Controls.Add(tabControl1);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "AxisManageFrom";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "绘制轴网";
            Load += AxisManageFrom_Load;
            Paint += AxisManageFrom_Paint;
            tabControl1.ResumeLayout(false);
            tabPage1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvAxisLIneList).EndInit();
            panel6.ResumeLayout(false);
            panel6.PerformLayout();
            panel1.ResumeLayout(false);
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private AxisDrawPenFrom axisDrawPenFrom1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textHeight;
        private System.Windows.Forms.TextBox textWidth;
        private System.Windows.Forms.DataGridView dgvAxisLIneList;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListBox ListSpacing;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton rbRight;
        private System.Windows.Forms.RadioButton rbleft;
        private System.Windows.Forms.RadioButton rbBottom;
        private System.Windows.Forms.RadioButton rbTop;
        private System.Windows.Forms.Button btnInsertAxisLine;
        private System.Windows.Forms.BindingSource axisLineDrawDataBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn spacingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lengthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvSpacing;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.Button button1;
    }
}