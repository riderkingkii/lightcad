using LightCAD.Core;
using LightCAD.Core.Element3d;
using LightCAD.Core.Elements;
using LightCAD.Drawing.Actions;
using LightCAD.MathLib;
using LightCAD.Model;
using LightCAD.Runtime;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace LightCAD.UI
{
    public partial class LoftDefSelect : Form, ILoftDefWindow
    {
        public Bounds EditorBounds => new Bounds();
        public string Uuid { get; set; }
        public string Name { get; set; }

        public bool IsActive => true;

        public Type WinType => this.GetType();

        public string CurrentAction { get; set; }
        public LoftDefSelect()
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;
            LoadList();
        }
        private void LoadList()
        {
            this.listBox1.SelectionMode = SelectionMode.One;
            List<BoxItem> items = ComponentManager.GetCptDefs("放样体", "放样体").Select(n => new BoxItem { Id = n.Uuid, Name = n.Name }).ToList();
            this.listBox1.DataSource = items;
            this.listBox1.DisplayMember = "Name";
            this.listBox1.ValueMember = "Id";
        }
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("请选择拉伸体定义");
                return;
            }
            Uuid = listBox1.SelectedValue.ToString();
            Name = listBox1.SelectedItem.ToString();
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnCreateLoftDef_Click(object sender, EventArgs e)
        {
            SectionsForm sectionsForm = new SectionsForm();
            if (sectionsForm.ShowDialog() == DialogResult.OK)
            {
                LcLoftDef loftDef = new LcLoftDef(Guid.NewGuid().ToString(), "放样体", sectionsForm.SectionId);
                loftDef.Name = sectionsForm.SectionName;
                ComponentManager.AddCptDefs(loftDef);
                LoadList();
            }
        }

        private void btnAddSection_Click(object sender, EventArgs e)
        {
            var loftDefId = listBox1.SelectedValue.ToString();
            var loftDef = ComponentManager.GetCptDef(loftDefId) as LcLoftDef;
            SectionsForm sectionsForm = new SectionsForm();
            if (sectionsForm.ShowDialog() == DialogResult.OK)
            {
                var oldSecId = loftDef.TypeParameters["SectionId"].ToString();
                loftDef.TypeParameters["SectionId"] = oldSecId + "," + sectionsForm.SectionId;
            }
        }
    }
}