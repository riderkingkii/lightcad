﻿namespace LightCAD.UI
{
    partial class ModelView3DControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            skGL = new SkiaSharp.Views.Desktop.SKGLControl();
            SuspendLayout();
            // 
            // skGL
            // 
            skGL.BackColor = System.Drawing.Color.Black;
            skGL.Dock = System.Windows.Forms.DockStyle.Fill;
            skGL.Location = new System.Drawing.Point(0, 0);
            skGL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            skGL.Name = "skGL";
            skGL.Size = new System.Drawing.Size(788, 503);
            skGL.TabIndex = 0;
            skGL.VSync = true;
            // 
            // ModelView3DControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(skGL);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "ModelView3DControl";
            Size = new System.Drawing.Size(788, 503);
            ResumeLayout(false);
        }

        #endregion

        private SkiaSharp.Views.Desktop.SKGLControl skGL;
    }
}
