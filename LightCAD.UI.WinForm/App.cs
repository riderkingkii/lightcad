﻿using LightCAD.Core;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Weikio.PluginFramework.Abstractions;

namespace LightCAD.UI
{
    public class App : IApp
    {
        public static App Current { get; private set; }

        static App()
        {
            Current = new App();
        }

        private string[] args;
        private IUISystem uiSystem;

        public IWindow ActiveWindow { get; private set; }
        public async void Initialize(string[] args)
        {
            uiSystem = new UISystem();
            AppRuntime.InitUiSystem(uiSystem);//优先初始化，以方便显示异常
            LcDocument.RegistElementTypes(BuiltinElementType.All);
            MaterialManager.Initilize();
            SectionManager.Initilize();
            LightCAD.Drawing.Actions.Loader.Initilize();
            LightCAD.Component.Actions.ComponentActionLoader.Initilize();
            QdArch.ArchPlugin.Loaded();
            QdStruct.StructPlugin.Loaded();
            QdWater.WaterPlugin.Loaded();
            LightCAD.Core.ComponentManager.LoadCptBuiltinDefs();
            await PluginManager.LoadPlugins();

        }
        public void OnUIInitilized(IMainUI mainUI)
        {
            ActiveWindow = (IWindow)mainUI;
            this.ActiveWindow = (IWindow)mainUI;
            AppRuntime.Initilize(this,  mainUI, args);

        }

        private List<MainUIWindow> MainWinList = new List<MainUIWindow>();
        public MainUIWindow CreateMainWindow(LcDocument doc=null)
        {
            var mainWin = new MainUIWindow(doc!=null);
            mainWin.FormClosed += MainWin_FormClosed;
            mainWin.Show();
            MainWinList.Add(mainWin);
            if(doc != null)
            {
                mainWin.OpenDrawingTab(doc);
            }
            return mainWin;
        }
        public bool IsExist { get; private set; }
        private void MainWin_FormClosed(object? sender, FormClosedEventArgs e)
        {
            var mainWin = sender as MainUIWindow;
            MainWinList.Remove(mainWin);
            if (MainWinList.Count == 0)
            {
                this.IsExist = true;
                Application.Exit();
                
            }
        }
    }
}
