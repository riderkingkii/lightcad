﻿using LightCAD.Core;
using Svg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.LineType
{
    public partial class LineWidthWindow : Form
    {
        public LineWeight SelectLineWidth { get; set; }
        public LineWidthWindow()
        {
            InitializeComponent();
        }

        public LineWidthWindow(LineWeight oldlinewidth)
        {
            InitializeComponent();
            this.oldtext.Text = GetLineWidthName(oldlinewidth);
            this.newtext.Text = GetLineWidthName(oldlinewidth);
            SelectLineWidth = oldlinewidth;
        }

        private void LineWidthWindow_Load(object sender, EventArgs e)
        {
            RefreshList();

            //var sysIndex = this.LineWidthList.FindItemWithText(SelectLineWidth);
           
        }
        public void RefreshList()
        {
            this.LineWidthList.FullRowSelect = true;
            LineWidthList.Items.Clear();
            foreach (int myCode in Enum.GetValues(typeof(LineWeight))) {
                string strName = Enum.GetName(typeof(LineWeight), myCode);//获取名称
                ListViewItem viewitem = new ListViewItem();
                switch (strName)
                {
                    case "ByLayer":
                        continue;
                        viewitem.Text = ("ByLayer");
                        viewitem.SubItems.Add(LineWeight.ByLayer.ToString());
                        break;
                    case "ByBlock":
                        continue;
                        viewitem.Text = ("ByBlock");
                        viewitem.SubItems.Add(LineWeight.ByBlock.ToString());
                        break;
                    case "Default":
                        viewitem.Text = ("默认");
                        viewitem.SubItems.Add(LineWeight.Default.ToString());
                        break;
                    case "LW000":
                        viewitem.Text = ("0.00mm");
                        viewitem.SubItems.Add(LineWeight.LW000.ToString());
                        break;
                    case "LW005":
                        viewitem.Text = ("0.05mm");
                        viewitem.SubItems.Add(LineWeight.LW005.ToString());
                        break;
                    case "LW009":
                        viewitem.Text = ("0.09mm");
                        viewitem.SubItems.Add(LineWeight.LW009.ToString());
                        break;
                    case "LW013":
                        viewitem.Text = ("0.13mm");
                        viewitem.SubItems.Add(LineWeight.LW013.ToString());
                        break;
                    case "LW015":
                        viewitem.Text = ("0.15mm");
                        viewitem.SubItems.Add(LineWeight.LW015.ToString());
                        break;
                    case "LW018":
                        viewitem.Text = ("0.18mm");
                        viewitem.SubItems.Add(LineWeight.LW018.ToString());
                        break;
                    case "LW020":
                        viewitem.Text = ("0.20mm");
                        viewitem.SubItems.Add(LineWeight.LW020.ToString());
                        break;
                    case "LW025":
                        viewitem.Text = ("0.25mm");
                        viewitem.SubItems.Add(LineWeight.LW025.ToString());
                        break;
                    case "LW030":
                        viewitem.Text = ("0.30mm");
                        viewitem.SubItems.Add(LineWeight.LW030.ToString());
                        break;
                    case "LW035":
                        viewitem.Text = ("0.35mm");
                        viewitem.SubItems.Add(LineWeight.LW035.ToString());
                        break;
                    case "LW040":
                        viewitem.Text = ("0.40mm");
                        viewitem.SubItems.Add(LineWeight.LW040.ToString());
                        break;
                    case "LW050":
                        viewitem.Text = ("0.50mm");
                        viewitem.SubItems.Add(LineWeight.LW050.ToString());
                        break;
                    case "LW053":
                        viewitem.Text = ("0.53mm");
                        viewitem.SubItems.Add(LineWeight.LW053.ToString());
                        break;
                    case "LW060":
                        viewitem.Text = ("0.60mm");
                        viewitem.SubItems.Add(LineWeight.LW060.ToString());
                        break;
                    case "LW070":
                        viewitem.Text = ("0.70mm");
                        viewitem.SubItems.Add(LineWeight.LW070.ToString());
                        break;
                    case "LW080":
                        viewitem.Text = ("0.80mm");
                        viewitem.SubItems.Add(LineWeight.LW080.ToString());
                        break;
                    case "LW090":
                        viewitem.Text = ("0.90mm");
                        viewitem.SubItems.Add(LineWeight.LW090.ToString());
                        break;
                    case "LW100":
                        viewitem.Text = ("1.00mm");
                        viewitem.SubItems.Add(LineWeight.LW100.ToString());
                        break;
                    case "LW106":
                        viewitem.Text = ("1.06mm");
                        viewitem.SubItems.Add(LineWeight.LW106.ToString());
                        break;
                    case "LW120":
                        viewitem.Text = ("1.20mm");
                        viewitem.SubItems.Add(LineWeight.LW120.ToString());
                        break;
                    case "LW140":
                        viewitem.Text = ("1.40mm");
                        viewitem.SubItems.Add(LineWeight.LW140.ToString());
                        break;
                    case "LW158":
                        viewitem.Text = ("1.58mm");
                        viewitem.SubItems.Add(LineWeight.LW158.ToString());
                        break;
                    case "LW200":
                        viewitem.Text = ("2.00mm");
                        viewitem.SubItems.Add(LineWeight.LW200.ToString());
                        break;
                    case "LW211":
                        viewitem.Text = ("2.11mm");
                        viewitem.SubItems.Add(LineWeight.LW211.ToString());
                        break;
                    default:
                        break;
                }
                this.LineWidthList.Items.Add(viewitem);
            }
        }

        public string GetLineWidthName(LineWeight linewidth) {
            string linewidthname = "默认";
            try
            {
                switch (linewidth)
                {
                    case LineWeight.ByLayer:
                        linewidthname = ("ByLayer");
                        break;
                    case LineWeight.ByBlock:
                        linewidthname = ("ByBlock");
                        break;
                    case LineWeight.Default:
                        linewidthname = ("默认");
                        break;
                    case LineWeight.LW000:
                        linewidthname = ("0.00mm");
                        break;
                    case LineWeight.LW005:
                        linewidthname = ("0.05mm");
                        break;
                    case LineWeight.LW009:
                        linewidthname = ("0.09mm");
                        break;
                    case LineWeight.LW013:
                        linewidthname = ("0.13mm");
                        break;
                    case LineWeight.LW015:
                        linewidthname = ("0.15mm");
                        break;
                    case LineWeight.LW018:
                        linewidthname = ("0.18mm");
                        break;
                    case LineWeight.LW020:
                        linewidthname = ("0.20mm");
                        break;
                    case LineWeight.LW025:
                        linewidthname = ("0.25mm");
                        break;
                    case LineWeight.LW030:
                        linewidthname = ("0.30mm");
                        break;
                    case LineWeight.LW035:
                        linewidthname = ("0.35mm");
                        break;
                    case LineWeight.LW040:
                        linewidthname = ("0.40mm");
                        break;
                    case LineWeight.LW050:
                        linewidthname = ("0.50mm");
                        break;
                    case LineWeight.LW053:
                        linewidthname = ("0.53mm");
                        break;
                    case LineWeight.LW060:
                        linewidthname = ("0.60mm");
                        break;
                    case LineWeight.LW070:
                        linewidthname = ("0.70mm");
                        break;
                    case LineWeight.LW080:
                        linewidthname = ("0.80mm");
                        break;
                    case LineWeight.LW090:
                        linewidthname = ("0.90mm");
                        break;
                    case LineWeight.LW100:
                        linewidthname = ("1.00mm");
                        break;
                    case LineWeight.LW106:
                        linewidthname = ("1.06mm");
                        break;
                    case LineWeight.LW120:
                        linewidthname = ("1.20mm");
                        break;
                    case LineWeight.LW140:
                        linewidthname = ("1.40mm");
                        break;
                    case LineWeight.LW158:
                        linewidthname = ("1.58mm");
                        break;
                    case LineWeight.LW200:
                        linewidthname = ("2.00mm");
                        break;
                    case LineWeight.LW211:
                        linewidthname = ("2.11mm");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                return linewidthname;
            }
            return linewidthname;
        }

        private void cancelbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LineWidthList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ListView.SelectedIndexCollection indexes = this.LineWidthList.SelectedIndices;
                if (indexes.Count > 0)
                {
                    int index = indexes[0];
                    string listname = this.LineWidthList.Items[index].SubItems[0].Text;//获取第一列的值
                    string listenumit = this.LineWidthList.Items[index].SubItems[1].Text;//获取第二列的值

                    Enum lineweight = Enum.Parse(typeof(LineWeight), listenumit) as Enum;
                    //this.newtext.Text= GetLineWidthName((LineWeight)lineweight);
                    this.newtext.Text = listname;
                    SelectLineWidth = (LineWeight)lineweight;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("操作失败！\n" + ex.Message, "提示", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void confirmbtn_Click(object sender, EventArgs e)
        {
            if (LineWidthList.SelectedItems.Count == 0)
                return;
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
