﻿using LightCAD.Core;
using LightCAD.Drawing.Actions;
using LightCAD.Runtime.Interface;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace LightCAD.UI.LineType
{
    public partial class LineTypeWindow : Form
    {
        public List<LcLineType> linetypelist { get; set; } = new List<LcLineType>();

        public LcDocument lcdoc;
        public LineTypeWindow()
        {
            InitializeComponent();
        }
        public LineTypeWindow(LcDocument doc, string linetypename)
        {
            InitializeComponent();
            lcdoc = doc;
            //LoadLinToPath("C:\\Users\\杜翔\\Desktop\\acad.lin");
            this.linetypelist.AddRange(doc.LineTypes);
            RefreshList();
            int sysIndex = this.linetypelist.FindIndex(item => item.LineTypeName.Equals(linetypename));
            if (sysIndex >= 0)
            {
                this.LineTypeList.Items[sysIndex].Selected = true;
            }
        }

        public LcLineType LineType { get; set; }
        private void SelectLineType_Load(object sender, EventArgs e)
        {

        }

        public void RefreshList()
        {
            this.LineTypeList.FullRowSelect = true;
            LineTypeList.Items.Clear();
            foreach (var item in linetypelist)
            {
                if (item.LineTypeName == "ByLayer" || item.LineTypeName == "ByBlock") {
                    continue;
                }
                ListViewItem viewitem = new ListViewItem(item.LineTypeName);
                viewitem.SubItems.Add(item.LineTypeDefinition);
                viewitem.SubItems.Add(item.LineTypeExplain);
                viewitem.SubItems.Add(item.Id.ToString());
                this.LineTypeList.Items.Add(viewitem);
            }
        }


        private void cancelbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loadbtn_Click(object sender, EventArgs e)
        {
            LoadLineTypeWindow loadlinetype = new LoadLineTypeWindow();
            if (loadlinetype.ShowDialog() == DialogResult.OK)
            {
                var selectlinetype = loadlinetype.SelectLineType;
                foreach (var item in selectlinetype)
                {
                    var entity = linetypelist.Where(x => x.LineTypeName == item.LineTypeName || x.LineTypeDefinition == item.LineTypeDefinition).FirstOrDefault();
                    if (entity != null)
                        continue;
                    linetypelist.Add(item);
                    lcdoc.LineTypes.Add(item);
                }
                RefreshList();
            }
        }

        private void confirmbtn_Click(object sender, EventArgs e)
        {
            if (LineTypeList.SelectedItems.Count == 0)
                return;
            else
            {
                string LineTypeName = LineTypeList.SelectedItems[0].SubItems[0].Text;
                string LineTypeDefinition = LineTypeList.SelectedItems[0].SubItems[1].Text;
                string LineTypeExplain = LineTypeList.SelectedItems[0].SubItems[2].Text;
                string LineTypeId = LineTypeList.SelectedItems[0].SubItems[3].Text;
                LcLineType lclinetype = new LcLineType();
                lclinetype.Id = int.Parse(LineTypeId);
                lclinetype.LineTypeName = LineTypeName;
                lclinetype.LineTypeExplain = LineTypeExplain;
                lclinetype.LineTypeDefinition = LineTypeDefinition;
                LineType = lclinetype;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
