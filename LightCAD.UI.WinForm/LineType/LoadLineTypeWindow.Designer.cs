﻿namespace LightCAD.UI.LineType
{
    partial class LoadLineTypeWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Loadbtn = new System.Windows.Forms.Button();
            this.pathtext = new System.Windows.Forms.TextBox();
            this.titlelable = new System.Windows.Forms.Label();
            this.linetypelist = new System.Windows.Forms.ListView();
            this.LineTypeColumn = new System.Windows.Forms.ColumnHeader();
            this.ExplainColumn = new System.Windows.Forms.ColumnHeader();
            this.confirmbtn = new System.Windows.Forms.Button();
            this.cancelbtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Loadbtn
            // 
            this.Loadbtn.Location = new System.Drawing.Point(12, 12);
            this.Loadbtn.Name = "Loadbtn";
            this.Loadbtn.Size = new System.Drawing.Size(94, 29);
            this.Loadbtn.TabIndex = 0;
            this.Loadbtn.Text = "文件(F)...";
            this.Loadbtn.UseVisualStyleBackColor = true;
            this.Loadbtn.Click += new System.EventHandler(this.Loadbtn_Click);
            // 
            // pathtext
            // 
            this.pathtext.Location = new System.Drawing.Point(112, 12);
            this.pathtext.Name = "pathtext";
            this.pathtext.Size = new System.Drawing.Size(370, 27);
            this.pathtext.TabIndex = 1;
            // 
            // titlelable
            // 
            this.titlelable.AutoSize = true;
            this.titlelable.Location = new System.Drawing.Point(12, 64);
            this.titlelable.Name = "titlelable";
            this.titlelable.Size = new System.Drawing.Size(69, 20);
            this.titlelable.TabIndex = 2;
            this.titlelable.Text = "可用线型";
            // 
            // linetypelist
            // 
            this.linetypelist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LineTypeColumn,
            this.ExplainColumn});
            this.linetypelist.Location = new System.Drawing.Point(12, 87);
            this.linetypelist.Name = "linetypelist";
            this.linetypelist.Size = new System.Drawing.Size(538, 200);
            this.linetypelist.TabIndex = 3;
            this.linetypelist.UseCompatibleStateImageBehavior = false;
            this.linetypelist.View = System.Windows.Forms.View.Details;
            // 
            // LineTypeColumn
            // 
            this.LineTypeColumn.Text = "线型";
            this.LineTypeColumn.Width = 220;
            // 
            // ExplainColumn
            // 
            this.ExplainColumn.Text = "说明";
            this.ExplainColumn.Width = 500;
            // 
            // confirmbtn
            // 
            this.confirmbtn.Location = new System.Drawing.Point(112, 300);
            this.confirmbtn.Name = "confirmbtn";
            this.confirmbtn.Size = new System.Drawing.Size(94, 29);
            this.confirmbtn.TabIndex = 4;
            this.confirmbtn.Text = "确定";
            this.confirmbtn.UseVisualStyleBackColor = true;
            this.confirmbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // cancelbtn
            // 
            this.cancelbtn.Location = new System.Drawing.Point(233, 300);
            this.cancelbtn.Name = "cancelbtn";
            this.cancelbtn.Size = new System.Drawing.Size(94, 29);
            this.cancelbtn.TabIndex = 5;
            this.cancelbtn.Text = "取消";
            this.cancelbtn.UseVisualStyleBackColor = true;
            this.cancelbtn.Click += new System.EventHandler(this.cancelbtn_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(350, 300);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(94, 29);
            this.button3.TabIndex = 6;
            this.button3.Text = "帮助(H)...";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // LoadLineTypeWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 340);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.cancelbtn);
            this.Controls.Add(this.confirmbtn);
            this.Controls.Add(this.linetypelist);
            this.Controls.Add(this.titlelable);
            this.Controls.Add(this.pathtext);
            this.Controls.Add(this.Loadbtn);
            this.Name = "LoadLineTypeWindow";
            this.Text = "加载或重载线型";
            this.Load += new System.EventHandler(this.LoadLineTypeWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Loadbtn;
        private System.Windows.Forms.TextBox pathtext;
        private System.Windows.Forms.Label titlelable;
        private System.Windows.Forms.ListView linetypelist;
        private System.Windows.Forms.Button confirmbtn;
        private System.Windows.Forms.Button cancelbtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ColumnHeader LineTypeColumn;
        private System.Windows.Forms.ColumnHeader ExplainColumn;
    }
}