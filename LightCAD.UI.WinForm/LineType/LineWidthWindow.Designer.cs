﻿namespace LightCAD.UI.LineType
{
    partial class LineWidthWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LineWidthList = new System.Windows.Forms.ListView();
            this.LineWidth = new System.Windows.Forms.ColumnHeader();
            this.panel1 = new System.Windows.Forms.Panel();
            this.newtext = new System.Windows.Forms.Label();
            this.oldtext = new System.Windows.Forms.Label();
            this.newtitle = new System.Windows.Forms.Label();
            this.oldtitle = new System.Windows.Forms.Label();
            this.confirmbtn = new System.Windows.Forms.Button();
            this.cancelbtn = new System.Windows.Forms.Button();
            this.helpbtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "线宽：";
            // 
            // LineWidthList
            // 
            this.LineWidthList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LineWidth});
            this.LineWidthList.FullRowSelect = true;
            this.LineWidthList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.LineWidthList.Location = new System.Drawing.Point(12, 32);
            this.LineWidthList.Name = "LineWidthList";
            this.LineWidthList.Size = new System.Drawing.Size(333, 210);
            this.LineWidthList.TabIndex = 1;
            this.LineWidthList.UseCompatibleStateImageBehavior = false;
            this.LineWidthList.View = System.Windows.Forms.View.Details;
            this.LineWidthList.SelectedIndexChanged += new System.EventHandler(this.LineWidthList_SelectedIndexChanged);
            // 
            // LineWidth
            // 
            this.LineWidth.Text = "";
            this.LineWidth.Width = 300;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.newtext);
            this.panel1.Controls.Add(this.oldtext);
            this.panel1.Controls.Add(this.newtitle);
            this.panel1.Controls.Add(this.oldtitle);
            this.panel1.Location = new System.Drawing.Point(12, 258);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(333, 68);
            this.panel1.TabIndex = 2;
            // 
            // newtext
            // 
            this.newtext.AutoSize = true;
            this.newtext.Location = new System.Drawing.Point(72, 38);
            this.newtext.Name = "newtext";
            this.newtext.Size = new System.Drawing.Size(0, 20);
            this.newtext.TabIndex = 3;
            // 
            // oldtext
            // 
            this.oldtext.AutoSize = true;
            this.oldtext.Location = new System.Drawing.Point(72, 7);
            this.oldtext.Name = "oldtext";
            this.oldtext.Size = new System.Drawing.Size(0, 20);
            this.oldtext.TabIndex = 2;
            // 
            // newtitle
            // 
            this.newtitle.AutoSize = true;
            this.newtitle.Location = new System.Drawing.Point(12, 38);
            this.newtitle.Name = "newtitle";
            this.newtitle.Size = new System.Drawing.Size(54, 20);
            this.newtitle.TabIndex = 1;
            this.newtitle.Text = "新的：";
            // 
            // oldtitle
            // 
            this.oldtitle.AutoSize = true;
            this.oldtitle.Location = new System.Drawing.Point(12, 7);
            this.oldtitle.Name = "oldtitle";
            this.oldtitle.Size = new System.Drawing.Size(54, 20);
            this.oldtitle.TabIndex = 0;
            this.oldtitle.Text = "旧的：";
            // 
            // confirmbtn
            // 
            this.confirmbtn.Location = new System.Drawing.Point(25, 338);
            this.confirmbtn.Name = "confirmbtn";
            this.confirmbtn.Size = new System.Drawing.Size(94, 29);
            this.confirmbtn.TabIndex = 3;
            this.confirmbtn.Text = "确定";
            this.confirmbtn.UseVisualStyleBackColor = true;
            this.confirmbtn.Click += new System.EventHandler(this.confirmbtn_Click);
            // 
            // cancelbtn
            // 
            this.cancelbtn.Location = new System.Drawing.Point(145, 338);
            this.cancelbtn.Name = "cancelbtn";
            this.cancelbtn.Size = new System.Drawing.Size(94, 29);
            this.cancelbtn.TabIndex = 4;
            this.cancelbtn.Text = "取消";
            this.cancelbtn.UseVisualStyleBackColor = true;
            this.cancelbtn.Click += new System.EventHandler(this.cancelbtn_Click);
            // 
            // helpbtn
            // 
            this.helpbtn.Location = new System.Drawing.Point(260, 338);
            this.helpbtn.Name = "helpbtn";
            this.helpbtn.Size = new System.Drawing.Size(94, 29);
            this.helpbtn.TabIndex = 5;
            this.helpbtn.Text = "帮助(H)";
            this.helpbtn.UseVisualStyleBackColor = true;
            // 
            // LineWidthWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 379);
            this.Controls.Add(this.helpbtn);
            this.Controls.Add(this.cancelbtn);
            this.Controls.Add(this.confirmbtn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LineWidthList);
            this.Controls.Add(this.label1);
            this.Name = "LineWidthWindow";
            this.Text = "线宽";
            this.Load += new System.EventHandler(this.LineWidthWindow_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView LineWidthList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label newtext;
        private System.Windows.Forms.Label oldtext;
        private System.Windows.Forms.Label newtitle;
        private System.Windows.Forms.Label oldtitle;
        private System.Windows.Forms.Button confirmbtn;
        private System.Windows.Forms.Button cancelbtn;
        private System.Windows.Forms.Button helpbtn;
        private System.Windows.Forms.ColumnHeader LineWidth;
    }
}