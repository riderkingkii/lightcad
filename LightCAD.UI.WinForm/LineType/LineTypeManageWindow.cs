﻿using LightCAD.Core;
using LightCAD.Drawing.Actions;
using LightCAD.Runtime.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace LightCAD.UI.LineType
{
    public partial class LineTypeManageWindow : FormBase, ILineTypeManageWindow
    {
        public LineTypeManageAction linetypeManageAction;
        public string CurrentAction { get; set; }
        public bool IsActive => true;
        public Type WinType => this.GetType();
        public LineTypeManageWindow()
        {
            InitializeComponent();
        }
        public LineTypeManageWindow(params object[] args) : this()
        {
            if (args != null && args.Length > 0)
            {
                this.linetypeManageAction = (LineTypeManageAction)args[0];
                this.CurrentAction = "OK";
            }
        }

        private void LineTypeManageWindow_Load(object sender, EventArgs e)
        {
            List<string> list = new List<string>();
            list.Add("显示所有线型");
            list.Add("显示所有使用的线型");
            list.Add("显示所有依赖于外部参照的线型");
            this.showlinetypecombox.DataSource = list;
            this.showlinetypecombox.SelectedIndex = 0;
        }

        private void showlinetypecombox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectvalue = this.showlinetypecombox.SelectedValue.ToString();
            bool check = this.reversalbox.Checked;
            GetLineTypes(selectvalue, check);
        }

        public List<LcLineType> GetLineTypes(string selectname,bool check)
        {
            List<LcLineType> lineTypes = new List<LcLineType>();
            var doc = linetypeManageAction.lcDocument;
            var layerlist = doc.Layers.ToList();
            var linetypelist = doc.LineTypes.ToList();
            switch (selectname)
            {
                case "显示所有线型":
                    if (check)
                    {
                        linetypelist = new List<LcLineType>();
                    }
                    break;
                case "显示所有使用的线型":
                    List<string> userlinetype = layerlist.Select(t => t.LineTypeName).ToList();

                    if (check)
                    {
                        linetypelist = linetypelist.Where(x => !userlinetype.Contains(x.LineTypeName)).ToList();
                    }
                    else {
                        linetypelist = linetypelist.Where(x => userlinetype.Contains(x.LineTypeName)).ToList();
                    }
                    break;
                case "显示所有依赖于外部参照的线型":
                    break;
                default:
                    break;
            }
            RefreshList(linetypelist);//当前线型
            string newlinetypename = layerlist.Where(x => x.IsStatus == true).FirstOrDefault()?.LineTypeName;
            int sysIndex = linetypelist.FindIndex(item => item.LineTypeName.Equals(newlinetypename));
            if (sysIndex >= 0)
            {
                this.linetypelistview.Items[sysIndex].Selected = true;
            }
            return lineTypes;
        }

        public void RefreshList(List<LcLineType> linetypelist)
        {
            linetypelistview.Items.Clear();
            foreach (var item in linetypelist)
            {
                ListViewItem viewitem = new ListViewItem(item.LineTypeName);
                viewitem.SubItems.Add(item.LineTypeDefinition);
                viewitem.SubItems.Add(item.LineTypeExplain);
                viewitem.SubItems.Add(item.Id.ToString());
                this.linetypelistview.Items.Add(viewitem);
            }
        }

        private void reversalbox_CheckedChanged(object sender, EventArgs e)
        {
            string selectvalue = this.showlinetypecombox.SelectedValue.ToString();
            bool check = this.reversalbox.Checked;
            GetLineTypes(selectvalue, check);
        }

        private void loadbtn_Click(object sender, EventArgs e)
        {
            LoadLineTypeWindow loadlinetype = new LoadLineTypeWindow();
            var doc = linetypeManageAction.lcDocument;
            var linetypelist = doc.LineTypes.ToList();
            if (loadlinetype.ShowDialog() == DialogResult.OK)
            {
                var selectlinetype = loadlinetype.SelectLineType;
                foreach (var item in selectlinetype)
                {
                    var entity = linetypelist.Where(x => x.LineTypeName == item.LineTypeName || x.LineTypeDefinition == item.LineTypeDefinition).FirstOrDefault();
                    if (entity != null)
                        continue;
                    doc.LineTypes.Add(item);
                    ListViewItem viewitem = new ListViewItem(item.LineTypeName);
                    viewitem.SubItems.Add(item.LineTypeDefinition);
                    viewitem.SubItems.Add(item.LineTypeExplain);
                    viewitem.SubItems.Add(item.Id.ToString());
                    this.linetypelistview.Items.Add(viewitem);
                }
            }
        }

        private void cancelbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void confirmbtn_Click(object sender, EventArgs e)
        {
            var doc = linetypeManageAction.lcDocument;
            var linetypes = linetypeManageAction.lcDocument.LineTypes;
            var layers = linetypeManageAction.lcDocument.Layers;
            if (linetypelistview.SelectedItems.Count == 0)
            { 
            }
            else
            {
                string LineTypeName = linetypelistview.SelectedItems[0].SubItems[0].Text;
                string LineTypeDefinition = linetypelistview.SelectedItems[0].SubItems[1].Text;
                string LineTypeExplain = linetypelistview.SelectedItems[0].SubItems[2].Text;
                string LineTypeId = linetypelistview.SelectedItems[0].SubItems[3].Text;
                SetLineType(LineTypeName);
            }
            this.Close();
        }

        /// <summary>
        /// 根据线性名给当前图层赋线型
        /// </summary>
        /// <param name="linetypename"></param>
        public void SetLineType(string linetypename)
        {
            var doc = linetypeManageAction.lcDocument;
            var linetype = linetypeManageAction.lcDocument.LineTypes.Where(x=>x.LineTypeName== linetypename).FirstOrDefault();
            var layer = linetypeManageAction.lcDocument.Layers.Where(x => x.IsStatus == true).FirstOrDefault();
            if (linetype == null)
            {
                return;
            }
            if (layer == null)
            {
                return;
            }
            layer.LineTypeId = linetype.Id;
            layer.LineTypeName = linetype.LineTypeName;
            this.newlisttype.Text = "当前线型：" + linetype.LineTypeName;
        }

        private void deletebtn_Click(object sender, EventArgs e)
        {
            var doc = linetypeManageAction.lcDocument;
            var linetypes = linetypeManageAction.lcDocument.LineTypes;
            var layers = linetypeManageAction.lcDocument.Layers;
            if (linetypelistview.SelectedItems.Count == 0)
                return;
            else
            {
                string LineTypeName = linetypelistview.SelectedItems[0].SubItems[0].Text;
                string LineTypeDefinition = linetypelistview.SelectedItems[0].SubItems[1].Text;
                string LineTypeExplain = linetypelistview.SelectedItems[0].SubItems[2].Text;
                string LineTypeId = linetypelistview.SelectedItems[0].SubItems[3].Text;
                switch (LineTypeName)
                {
                    case "ByLayer":
                        ShowMassage();
                        break;
                    case "ByBlock":
                        ShowMassage();
                        break;
                    case "Continuous":
                        ShowMassage();
                        break;
                    default:
                        //当前线型
                        var currentlinetype = layers.Where(x => x.IsStatus == true).Select(x => x.LineTypeName).FirstOrDefault();
                        //所有线型
                        List<string> alluserlinetype = layers.Select(x => x.LineTypeName).ToList();
                        if (LineTypeName == currentlinetype || alluserlinetype.Contains(LineTypeName))
                        {
                            ShowMassage();
                        }
                        else {
                            var linetypeentity = linetypes.Where(x => x.LineTypeName.Equals(LineTypeName)).FirstOrDefault();
                            if (linetypeentity != null)
                            {
                                int sysIndex = linetypes.ToList().FindIndex(item => item.LineTypeName.Equals(LineTypeName));
                                linetypes.Remove(linetypeentity);
                                linetypelistview.Items[sysIndex].Remove();
                            }
                        }
                        break;
                }
            }
        }

        public void ShowMassage() {
            DialogResult Warning = System.Windows.Forms.MessageBox.Show($"未删除选择的线型。\n\n不能删除下列线型：\n" +
                                                                                   "\tByLayer、ByBlock和Continuous线型\n" +
                                                                                   "\t当前线型\n" +
                                                                                   "\t依赖外部参照的线型\n" +
                                                                                   "\t图层或对象参照的线型",
                                 "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        private void showdetailbtn_Click(object sender, EventArgs e)
        {

        }

        private void currentbtn_Click(object sender, EventArgs e)
        {
            if (linetypelistview.SelectedItems.Count == 0)
                return;
            else
            {
                string LineTypeName = linetypelistview.SelectedItems[0].SubItems[0].Text;
                string LineTypeDefinition = linetypelistview.SelectedItems[0].SubItems[1].Text;
                string LineTypeExplain = linetypelistview.SelectedItems[0].SubItems[2].Text;
                string LineTypeId = linetypelistview.SelectedItems[0].SubItems[3].Text;
                SetLineType(LineTypeName);
            }
        }

        private void linetypelistview_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (linetypelistview.SelectedItems.Count == 0)
            //    return;
            //else
            //{
            //    string LineTypeName = linetypelistview.SelectedItems[0].SubItems[0].Text;
            //    this.newlisttype.Text = "当前线型：" + LineTypeName;
            //}
        }
    }
}
