﻿namespace LightCAD.UI.LineType
{
    partial class LineTypeWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.LineTypeList = new System.Windows.Forms.ListView();
            this.LineTypeName = new System.Windows.Forms.ColumnHeader();
            this.Appearance = new System.Windows.Forms.ColumnHeader();
            this.Explain = new System.Windows.Forms.ColumnHeader();
            this.confirmbtn = new System.Windows.Forms.Button();
            this.cancelbtn = new System.Windows.Forms.Button();
            this.loadbtn = new System.Windows.Forms.Button();
            this.helpbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Location = new System.Drawing.Point(12, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(99, 20);
            this.Title.TabIndex = 0;
            this.Title.Text = "已加载的线型";
            // 
            // LineTypeList
            // 
            this.LineTypeList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LineTypeName,
            this.Appearance,
            this.Explain});
            this.LineTypeList.Location = new System.Drawing.Point(12, 32);
            this.LineTypeList.Name = "LineTypeList";
            this.LineTypeList.Size = new System.Drawing.Size(529, 206);
            this.LineTypeList.TabIndex = 1;
            this.LineTypeList.UseCompatibleStateImageBehavior = false;
            this.LineTypeList.View = System.Windows.Forms.View.Details;
            // 
            // LineTypeName
            // 
            this.LineTypeName.Text = "线型";
            this.LineTypeName.Width = 180;
            // 
            // Appearance
            // 
            this.Appearance.Text = "外观";
            this.Appearance.Width = 150;
            // 
            // Explain
            // 
            this.Explain.Text = "说明";
            this.Explain.Width = 400;
            // 
            // confirmbtn
            // 
            this.confirmbtn.Location = new System.Drawing.Point(46, 259);
            this.confirmbtn.Name = "confirmbtn";
            this.confirmbtn.Size = new System.Drawing.Size(94, 29);
            this.confirmbtn.TabIndex = 2;
            this.confirmbtn.Text = "确定";
            this.confirmbtn.UseVisualStyleBackColor = true;
            this.confirmbtn.Click += new System.EventHandler(this.confirmbtn_Click);
            // 
            // cancelbtn
            // 
            this.cancelbtn.Location = new System.Drawing.Point(165, 259);
            this.cancelbtn.Name = "cancelbtn";
            this.cancelbtn.Size = new System.Drawing.Size(94, 29);
            this.cancelbtn.TabIndex = 3;
            this.cancelbtn.Text = "取消";
            this.cancelbtn.UseVisualStyleBackColor = true;
            this.cancelbtn.Click += new System.EventHandler(this.cancelbtn_Click);
            // 
            // loadbtn
            // 
            this.loadbtn.Location = new System.Drawing.Point(285, 259);
            this.loadbtn.Name = "loadbtn";
            this.loadbtn.Size = new System.Drawing.Size(94, 29);
            this.loadbtn.TabIndex = 4;
            this.loadbtn.Text = "加载(L)...";
            this.loadbtn.UseVisualStyleBackColor = true;
            this.loadbtn.Click += new System.EventHandler(this.loadbtn_Click);
            // 
            // helpbtn
            // 
            this.helpbtn.Location = new System.Drawing.Point(405, 259);
            this.helpbtn.Name = "helpbtn";
            this.helpbtn.Size = new System.Drawing.Size(94, 29);
            this.helpbtn.TabIndex = 5;
            this.helpbtn.Text = "帮助(H)";
            this.helpbtn.UseVisualStyleBackColor = true;
            // 
            // LineTypeWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 300);
            this.Controls.Add(this.helpbtn);
            this.Controls.Add(this.loadbtn);
            this.Controls.Add(this.cancelbtn);
            this.Controls.Add(this.confirmbtn);
            this.Controls.Add(this.LineTypeList);
            this.Controls.Add(this.Title);
            this.Name = "LineTypeWindow";
            this.Text = "选择线型";
            this.Load += new System.EventHandler(this.SelectLineType_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.ListView LineTypeList;
        private System.Windows.Forms.Button confirmbtn;
        private System.Windows.Forms.Button cancelbtn;
        private System.Windows.Forms.Button loadbtn;
        private System.Windows.Forms.Button helpbtn;
        private System.Windows.Forms.ColumnHeader LineTypeName;
        private System.Windows.Forms.ColumnHeader Appearance;
        private System.Windows.Forms.ColumnHeader Explain;
    }
}