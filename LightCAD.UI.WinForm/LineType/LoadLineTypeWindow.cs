﻿using LightCAD.Core;
using LightCAD.Drawing.Actions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.DataFormats;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;

namespace LightCAD.UI.LineType
{
    public partial class LoadLineTypeWindow : Form
    {
        public List<LcLineType> SelectLineType { get; set; } = new List<LcLineType>();
        public LoadLineTypeWindow()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in this.linetypelist.SelectedItems)
            {
                byte[] buffer = Guid.NewGuid().ToByteArray();
                int lcid = (int)BitConverter.ToInt64(buffer, 0);
                LcLineType linetypeEntity = new LcLineType();
                linetypeEntity.Id = lcid;
                linetypeEntity.LineTypeName = item.SubItems[0].Text;
                linetypeEntity.LineTypeExplain = item.SubItems[1].Text;
                linetypeEntity.LineTypeDefinition = item.SubItems[2].Text;
                SelectLineType.Add(linetypeEntity);
            }
            this.DialogResult = DialogResult.OK;
        }

        private void Loadbtn_Click(object sender, EventArgs e)
        {
            List<LcLineType> lineTypes = new List<LcLineType>();
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;//该值确定是否可以选择多个文件
            dialog.Title = "选择线型文件";
            dialog.Filter = "线型文件(*.lin)|*.lin";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = dialog.FileName;
                this.pathtext.Text = path;
                if (File.Exists(path))
                {
                    //逐行读取
                    string[] lines = File.ReadAllLines(path);
                    if (lines.Length <= 1) return;
                    for (int i = 0; i < lines.Length - 1; i++)
                    {
                        if (lines[i].StartsWith("*") && lines[i + 1].StartsWith("A,"))
                        {
                            LcLineType linetypeEntity = new LcLineType();
                            string[] nameandexplain = lines[i].TrimStart('*').Split(',');
                            linetypeEntity.LineTypeName = nameandexplain?[0];
                            linetypeEntity.LineTypeExplain = nameandexplain?[1];
                            linetypeEntity.LineTypeDefinition = lines[i + 1];
                            lineTypes.Add(linetypeEntity);
                        }
                    }
                }

                foreach (var item in lineTypes)
                {
                    ListViewItem viewitem = new ListViewItem(item.LineTypeName);
                    viewitem.SubItems.Add(item.LineTypeExplain);
                    viewitem.SubItems.Add(item.LineTypeDefinition);
                    this.linetypelist.Items.Add(viewitem);
                }
            }
        }

        private void LoadLineTypeWindow_Load(object sender, EventArgs e)
        {
            this.linetypelist.MultiSelect = true;
        }

        private void cancelbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
