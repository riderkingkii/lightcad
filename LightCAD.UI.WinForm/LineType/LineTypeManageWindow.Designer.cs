﻿namespace LightCAD.UI.LineType
{
    partial class LineTypeManageWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadbtn = new System.Windows.Forms.Button();
            this.deletebtn = new System.Windows.Forms.Button();
            this.currentbtn = new System.Windows.Forms.Button();
            this.showdetailbtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.showlinetypecombox = new System.Windows.Forms.ComboBox();
            this.reversalbox = new System.Windows.Forms.CheckBox();
            this.newlisttype = new System.Windows.Forms.TextBox();
            this.linetypelistview = new System.Windows.Forms.ListView();
            this.LineTypeName = new System.Windows.Forms.ColumnHeader();
            this.Appearance = new System.Windows.Forms.ColumnHeader();
            this.Explain = new System.Windows.Forms.ColumnHeader();
            this.confirmbtn = new System.Windows.Forms.Button();
            this.cancelbtn = new System.Windows.Forms.Button();
            this.helpbtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadbtn
            // 
            this.loadbtn.Location = new System.Drawing.Point(628, 12);
            this.loadbtn.Name = "loadbtn";
            this.loadbtn.Size = new System.Drawing.Size(94, 29);
            this.loadbtn.TabIndex = 1;
            this.loadbtn.Text = "加载(L)...";
            this.loadbtn.UseVisualStyleBackColor = true;
            this.loadbtn.Click += new System.EventHandler(this.loadbtn_Click);
            // 
            // deletebtn
            // 
            this.deletebtn.Location = new System.Drawing.Point(728, 12);
            this.deletebtn.Name = "deletebtn";
            this.deletebtn.Size = new System.Drawing.Size(94, 29);
            this.deletebtn.TabIndex = 2;
            this.deletebtn.Text = "删除";
            this.deletebtn.UseVisualStyleBackColor = true;
            this.deletebtn.Click += new System.EventHandler(this.deletebtn_Click);
            // 
            // currentbtn
            // 
            this.currentbtn.Location = new System.Drawing.Point(628, 47);
            this.currentbtn.Name = "currentbtn";
            this.currentbtn.Size = new System.Drawing.Size(94, 29);
            this.currentbtn.TabIndex = 3;
            this.currentbtn.Text = "当前(C)";
            this.currentbtn.UseVisualStyleBackColor = true;
            this.currentbtn.Click += new System.EventHandler(this.currentbtn_Click);
            // 
            // showdetailbtn
            // 
            this.showdetailbtn.Location = new System.Drawing.Point(728, 47);
            this.showdetailbtn.Name = "showdetailbtn";
            this.showdetailbtn.Size = new System.Drawing.Size(94, 29);
            this.showdetailbtn.TabIndex = 4;
            this.showdetailbtn.Text = "显示细节(D)";
            this.showdetailbtn.UseVisualStyleBackColor = true;
            this.showdetailbtn.Click += new System.EventHandler(this.showdetailbtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.showlinetypecombox);
            this.groupBox1.Controls.Add(this.reversalbox);
            this.groupBox1.Location = new System.Drawing.Point(12, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 82);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "线型过滤器";
            // 
            // showlinetypecombox
            // 
            this.showlinetypecombox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.showlinetypecombox.FormattingEnabled = true;
            this.showlinetypecombox.Location = new System.Drawing.Point(15, 34);
            this.showlinetypecombox.Name = "showlinetypecombox";
            this.showlinetypecombox.Size = new System.Drawing.Size(366, 28);
            this.showlinetypecombox.TabIndex = 1;
            this.showlinetypecombox.SelectedIndexChanged += new System.EventHandler(this.showlinetypecombox_SelectedIndexChanged);
            // 
            // reversalbox
            // 
            this.reversalbox.AutoSize = true;
            this.reversalbox.Location = new System.Drawing.Point(406, 38);
            this.reversalbox.Name = "reversalbox";
            this.reversalbox.Size = new System.Drawing.Size(120, 24);
            this.reversalbox.TabIndex = 0;
            this.reversalbox.Text = "反转过滤器(I)";
            this.reversalbox.UseVisualStyleBackColor = true;
            this.reversalbox.CheckedChanged += new System.EventHandler(this.reversalbox_CheckedChanged);
            // 
            // newlisttype
            // 
            this.newlisttype.Location = new System.Drawing.Point(12, 97);
            this.newlisttype.Name = "newlisttype";
            this.newlisttype.ReadOnly = true;
            this.newlisttype.Size = new System.Drawing.Size(810, 27);
            this.newlisttype.TabIndex = 6;
            this.newlisttype.Text = "当前线型：";
            // 
            // linetypelistview
            // 
            this.linetypelistview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LineTypeName,
            this.Appearance,
            this.Explain});
            this.linetypelistview.FullRowSelect = true;
            this.linetypelistview.Location = new System.Drawing.Point(12, 130);
            this.linetypelistview.Name = "linetypelistview";
            this.linetypelistview.Size = new System.Drawing.Size(810, 354);
            this.linetypelistview.TabIndex = 7;
            this.linetypelistview.UseCompatibleStateImageBehavior = false;
            this.linetypelistview.View = System.Windows.Forms.View.Details;
            this.linetypelistview.SelectedIndexChanged += new System.EventHandler(this.linetypelistview_SelectedIndexChanged);
            // 
            // LineTypeName
            // 
            this.LineTypeName.Text = "线型";
            this.LineTypeName.Width = 150;
            // 
            // Appearance
            // 
            this.Appearance.Text = "外观";
            this.Appearance.Width = 200;
            // 
            // Explain
            // 
            this.Explain.Text = "说明";
            this.Explain.Width = 400;
            // 
            // confirmbtn
            // 
            this.confirmbtn.Location = new System.Drawing.Point(528, 490);
            this.confirmbtn.Name = "confirmbtn";
            this.confirmbtn.Size = new System.Drawing.Size(94, 29);
            this.confirmbtn.TabIndex = 8;
            this.confirmbtn.Text = "确认";
            this.confirmbtn.UseVisualStyleBackColor = true;
            this.confirmbtn.Click += new System.EventHandler(this.confirmbtn_Click);
            // 
            // cancelbtn
            // 
            this.cancelbtn.Location = new System.Drawing.Point(628, 490);
            this.cancelbtn.Name = "cancelbtn";
            this.cancelbtn.Size = new System.Drawing.Size(94, 29);
            this.cancelbtn.TabIndex = 9;
            this.cancelbtn.Text = "取消";
            this.cancelbtn.UseVisualStyleBackColor = true;
            this.cancelbtn.Click += new System.EventHandler(this.cancelbtn_Click);
            // 
            // helpbtn
            // 
            this.helpbtn.Location = new System.Drawing.Point(728, 490);
            this.helpbtn.Name = "helpbtn";
            this.helpbtn.Size = new System.Drawing.Size(94, 29);
            this.helpbtn.TabIndex = 10;
            this.helpbtn.Text = "帮助(H)";
            this.helpbtn.UseVisualStyleBackColor = true;
            // 
            // LineTypeManageWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 531);
            this.Controls.Add(this.helpbtn);
            this.Controls.Add(this.cancelbtn);
            this.Controls.Add(this.confirmbtn);
            this.Controls.Add(this.linetypelistview);
            this.Controls.Add(this.newlisttype);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.showdetailbtn);
            this.Controls.Add(this.currentbtn);
            this.Controls.Add(this.deletebtn);
            this.Controls.Add(this.loadbtn);
            this.Name = "LineTypeManageWindow";
            this.Text = "线型管理器";
            this.Load += new System.EventHandler(this.LineTypeManageWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadbtn;
        private System.Windows.Forms.Button deletebtn;
        private System.Windows.Forms.Button currentbtn;
        private System.Windows.Forms.Button showdetailbtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox showlinetypecombox;
        private System.Windows.Forms.CheckBox reversalbox;
        private System.Windows.Forms.TextBox newlisttype;
        private System.Windows.Forms.ListView linetypelistview;
        private System.Windows.Forms.Button confirmbtn;
        private System.Windows.Forms.Button cancelbtn;
        private System.Windows.Forms.Button helpbtn;
        private System.Windows.Forms.ColumnHeader LineTypeName;
        private System.Windows.Forms.ColumnHeader Appearance;
        private System.Windows.Forms.ColumnHeader Explain;
    }
}