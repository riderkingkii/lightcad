using LightCAD.Core;
using LightCAD.Core.Element3d;
using LightCAD.Core.Elements;
using LightCAD.Drawing.Actions;
using LightCAD.MathLib;
using LightCAD.Model;
using LightCAD.Runtime;
using OpenTK;
using QdArch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace LightCAD.UI
{
    public partial class RoofRefSelect : Form, IRoofRefWindow
    {
        public Bounds EditorBounds => new Bounds();
        public string Uuid { get; set; }

        public bool IsActive => true;

        public Type WinType => this.GetType();

        public string CurrentAction { get; set; }
        public RoofRefSelect()
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;
            LoadList();
        }
        private void LoadList()
        {
            this.listBox1.SelectionMode = SelectionMode.One;
            List<BoxItem> items = ComponentManager.GetCptDefs("�ݶ�", "���ݶ�").Select(n => new BoxItem { Id = n.Uuid, Name = n.Name }).ToList();
            this.listBox1.DataSource = items;
            this.listBox1.DisplayMember = "Name";
            this.listBox1.ValueMember = "Id";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("��ѡ�����ݶ�����");
                return;
            }
            Uuid = listBox1.SelectedValue.ToString();
            Name = listBox1.SelectedItem.ToString();
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SectionsForm sectionsForm = new SectionsForm();
            if (sectionsForm.ShowDialog() == DialogResult.OK)
            {
                var def = ComponentManager.GetCptDefs("�ݶ�", "���ݶ�").FirstOrDefault(n => n.TypeParameters.GetValue<string>("SectionId") == sectionsForm.SectionId);
                if (def != null)
                {
                    MessageBox.Show("��ǰ�������ݶ������Ѵ���");
                    this.listBox1.SelectedValue = def.Uuid;
                    return;
                }
                QdRoofRef RoofRef = new QdRoofRef(Guid.NewGuid().ToString(), "���ݶ�", "���ݶ�", sectionsForm.SectionId);
                RoofRef.Name = sectionsForm.SectionName;
                ComponentManager.AddCptDefs(RoofRef);
                LoadList();
            }
        }
    }
}