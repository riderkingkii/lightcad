﻿namespace LightCAD.UI
{
    partial class PaperSpaceControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            timer1 = new System.Windows.Forms.Timer(components);
            skglControl1 = new SkiaSharp.Views.Desktop.SKGLControl(PaperSpaceControl.graphicsMode);
            SuspendLayout();
            // 
            // timer1
            // 
            timer1.Interval = 16;
            // 
            // skglControl1
            // 
            skglControl1.BackColor = System.Drawing.Color.Black;
            skglControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            skglControl1.ForeColor = System.Drawing.Color.Black;
            skglControl1.Location = new System.Drawing.Point(0, 0);
            skglControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            skglControl1.Name = "skglControl1";
            skglControl1.Size = new System.Drawing.Size(998, 652);
            skglControl1.TabIndex = 0;
            skglControl1.VSync = true;
            // 
            // PaperSpaceControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(skglControl1);
            Name = "PaperSpaceControl";
            Size = new System.Drawing.Size(998, 652);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        internal SkiaSharp.Views.Desktop.SKGLControl skglControl1;
    }
}
