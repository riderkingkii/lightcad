﻿using LightCAD.Core;
using LightCAD.Drawing.Actions;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LightCAD.MathLib;
namespace LightCAD.UI
{
    public partial class BlockDefWindow : FormBase, IBlockDefWindow
    {
        public Type WinType => this.GetType();

        public string CurrentAction { get; set; }

        private BlockAction blockAction;
        private List<LcElement> elements = new List<LcElement>();
        public BlockDefWindow()
        {
            InitializeComponent();
        }
        public BlockDefWindow(params object[] args):this()
        {
            if (args != null && args.Length > 0)
            {
                this.blockAction = (BlockAction)args[0];
            }
        }
        public Vector2 BasePoint
        {
            get
            {
                if (!double.TryParse(this.txtX.Text, out double x))
                {
                    x = 0;
                }
                if (!double.TryParse(this.txtY.Text, out double y))
                {
                    y = 0;
                }
                return new Vector2(x, y);
            }
            set
            {
                if (value == null)
                {
                    this.txtX.Text = "0";
                    this.txtY.Text = "0";
                }
                else
                {
                    this.txtX.Text = value.X.ToString();
                    this.txtY.Text = value.Y.ToString();
                }
            }
        }

        public List<LcElement> Elements
        {
            get
            {
                return this.elements;
            }
            set
            {
                this.elements.Clear();
                this.elements.AddRange(value);
                this.lblSelectInfo.Text = $"已选择【{this.elements.Count}】个元素.";
            }
        }

        public string BlockName
        {
            get { return this.cboName.Text; }
            set { this.cboName.Text = value; }
        }

        public bool IsActive => true;

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.CurrentAction = "OK";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.CurrentAction = "Cancel";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnSelectPoint_Click(object sender, EventArgs e)
        {
            this.CurrentAction = "SelectPoint";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void lblSelectPoint_Click(object sender, EventArgs e)
        {
            this.CurrentAction = "SelectPoint";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnSelectObjects_Click(object sender, EventArgs e)
        {

            this.CurrentAction = "SelectObjects";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void lblSelectObjects_Click(object sender, EventArgs e)
        {
            this.CurrentAction = "SelectObjects";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
