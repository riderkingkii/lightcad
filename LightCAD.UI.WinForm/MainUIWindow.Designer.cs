﻿using System.Drawing;
using System.Windows.Forms;

namespace LightCAD.UI
{
    partial class MainUIWindow
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUIWindow));
            headerCtxMenu = new ContextMenuStrip(components);
            mui_CloseFile = new ToolStripMenuItem();
            mui_NewWindow = new ToolStripMenuItem();
            imageList = new ImageList(components);
            contextMenuStrip1 = new ContextMenuStrip(components);
            ssddfToolStripMenuItem = new ToolStripMenuItem();
            sdfasdfToolStripMenuItem = new ToolStripMenuItem();
            vS2015BlueTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015BlueTheme();
            vS2015LightTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015LightTheme();
            vS2015DarkTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015DarkTheme();
            vsToolStripExtender1 = new WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender(components);
            ToolTips = new ToolTip(components);
            lbMessage_Menu = new ContextMenuStrip(components);
            tsiSync3DViewport = new ToolStripMenuItem();
            toolStripSeparator1 = new ToolStripSeparator();
            titleBar = new Panel();
            lbSettings = new Label();
            TitleTab = new TitleTabControl();
            lbUser = new Label();
            lbMessage = new Label();
            lbMin = new Label();
            lbMax = new Label();
            lbClose = new Label();
            TabMenu = new TabMenuControl();
            dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            headerCtxMenu.SuspendLayout();
            contextMenuStrip1.SuspendLayout();
            lbMessage_Menu.SuspendLayout();
            titleBar.SuspendLayout();
            SuspendLayout();
            // 
            // headerCtxMenu
            // 
            headerCtxMenu.ImageScalingSize = new Size(20, 20);
            headerCtxMenu.Items.AddRange(new ToolStripItem[] { mui_CloseFile, mui_NewWindow });
            headerCtxMenu.Name = "headerCtxMenu";
            headerCtxMenu.Size = new Size(137, 48);
            headerCtxMenu.Opening += headerCtxMenu_Opening;
            headerCtxMenu.Opened += headerCtxMenu_Opened;
            headerCtxMenu.ItemClicked += headerCtxMenu_ItemClicked;
            // 
            // mui_CloseFile
            // 
            mui_CloseFile.Name = "mui_CloseFile";
            mui_CloseFile.Size = new Size(136, 22);
            mui_CloseFile.Text = "关闭文件";
            // 
            // mui_NewWindow
            // 
            mui_NewWindow.Name = "mui_NewWindow";
            mui_NewWindow.Size = new Size(136, 22);
            mui_NewWindow.Text = "新窗口显示";
            // 
            // imageList
            // 
            imageList.ColorDepth = ColorDepth.Depth8Bit;
            imageList.ImageStream = (ImageListStreamer)resources.GetObject("imageList.ImageStream");
            imageList.TransparentColor = Color.Transparent;
            imageList.Images.SetKeyName(0, "");
            imageList.Images.SetKeyName(1, "");
            imageList.Images.SetKeyName(2, "");
            imageList.Images.SetKeyName(3, "");
            imageList.Images.SetKeyName(4, "");
            imageList.Images.SetKeyName(5, "");
            imageList.Images.SetKeyName(6, "");
            imageList.Images.SetKeyName(7, "");
            imageList.Images.SetKeyName(8, "");
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.ImageScalingSize = new Size(20, 20);
            contextMenuStrip1.Items.AddRange(new ToolStripItem[] { ssddfToolStripMenuItem, sdfasdfToolStripMenuItem });
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new Size(120, 48);
            // 
            // ssddfToolStripMenuItem
            // 
            ssddfToolStripMenuItem.Name = "ssddfToolStripMenuItem";
            ssddfToolStripMenuItem.Size = new Size(119, 22);
            ssddfToolStripMenuItem.Text = "ssddf";
            // 
            // sdfasdfToolStripMenuItem
            // 
            sdfasdfToolStripMenuItem.Name = "sdfasdfToolStripMenuItem";
            sdfasdfToolStripMenuItem.Size = new Size(119, 22);
            sdfasdfToolStripMenuItem.Text = "sdfasdf";
            // 
            // vsToolStripExtender1
            // 
            vsToolStripExtender1.DefaultRenderer = null;
            // 
            // lbMessage_Menu
            // 
            lbMessage_Menu.ImageScalingSize = new Size(20, 20);
            lbMessage_Menu.Items.AddRange(new ToolStripItem[] { tsiSync3DViewport, toolStripSeparator1 });
            lbMessage_Menu.Name = "lbDrop_Menu";
            lbMessage_Menu.Size = new Size(173, 32);
            // 
            // tsiSync3DViewport
            // 
            tsiSync3DViewport.Name = "tsiSync3DViewport";
            tsiSync3DViewport.Size = new Size(172, 22);
            tsiSync3DViewport.Text = "同步图纸模型视口";
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new Size(169, 6);
            // 
            // titleBar
            // 
            titleBar.BackColor = Color.FromArgb(222, 225, 230);
            titleBar.Controls.Add(lbSettings);
            titleBar.Controls.Add(TitleTab);
            titleBar.Controls.Add(lbUser);
            titleBar.Controls.Add(lbMessage);
            titleBar.Controls.Add(lbMin);
            titleBar.Controls.Add(lbMax);
            titleBar.Controls.Add(lbClose);
            titleBar.Dock = DockStyle.Top;
            titleBar.Font = new Font("微软雅黑", 9F, FontStyle.Regular, GraphicsUnit.Point);
            titleBar.Location = new Point(2, 2);
            titleBar.Margin = new Padding(0);
            titleBar.Name = "titleBar";
            titleBar.Padding = new Padding(0, 1, 0, 0);
            titleBar.Size = new Size(1116, 30);
            titleBar.TabIndex = 30;
            // 
            // lbSettings
            // 
            lbSettings.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            lbSettings.BackColor = Color.Transparent;
            lbSettings.Font = new Font("微软雅黑", 15F, FontStyle.Regular, GraphicsUnit.Point);
            lbSettings.Image = Properties.Resources.Setup;
            lbSettings.Location = new Point(923, 2);
            lbSettings.Name = "lbSettings";
            lbSettings.Size = new Size(37, 26);
            lbSettings.TabIndex = 29;
            lbSettings.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // TitleTab
            // 
            TitleTab.AutoSize = true;
            TitleTab.Dock = DockStyle.Left;
            TitleTab.Location = new Point(0, 1);
            TitleTab.Margin = new Padding(2, 3, 2, 3);
            TitleTab.Name = "TitleTab";
            TitleTab.Size = new Size(26, 29);
            TitleTab.TabIndex = 26;
            // 
            // lbUser
            // 
            lbUser.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            lbUser.BackColor = Color.Transparent;
            lbUser.Font = new Font("微软雅黑", 13.8F, FontStyle.Regular, GraphicsUnit.Point);
            lbUser.Image = Properties.Resources.User;
            lbUser.Location = new Point(886, 2);
            lbUser.Name = "lbUser";
            lbUser.Size = new Size(37, 26);
            lbUser.TabIndex = 28;
            lbUser.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lbMessage
            // 
            lbMessage.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            lbMessage.BackColor = Color.Transparent;
            lbMessage.Font = new Font("微软雅黑", 15F, FontStyle.Regular, GraphicsUnit.Point);
            lbMessage.Image = Properties.Resources.Message;
            lbMessage.Location = new Point(960, 2);
            lbMessage.Name = "lbMessage";
            lbMessage.Size = new Size(37, 26);
            lbMessage.TabIndex = 27;
            lbMessage.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lbMin
            // 
            lbMin.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            lbMin.BackColor = Color.Transparent;
            lbMin.Font = new Font("微软雅黑", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lbMin.Location = new Point(998, 1);
            lbMin.Name = "lbMin";
            lbMin.Size = new Size(37, 26);
            lbMin.TabIndex = 22;
            lbMin.Text = "  一";
            lbMin.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lbMax
            // 
            lbMax.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            lbMax.BackColor = Color.Transparent;
            lbMax.Font = new Font("微软雅黑", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lbMax.Location = new Point(1035, 1);
            lbMax.Name = "lbMax";
            lbMax.Size = new Size(37, 26);
            lbMax.TabIndex = 23;
            lbMax.Text = "「」";
            lbMax.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lbClose
            // 
            lbClose.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            lbClose.BackColor = Color.Transparent;
            lbClose.Font = new Font("微软雅黑", 9F, FontStyle.Bold, GraphicsUnit.Point);
            lbClose.Location = new Point(1072, 1);
            lbClose.Name = "lbClose";
            lbClose.Size = new Size(37, 26);
            lbClose.TabIndex = 24;
            lbClose.Text = "✕";
            lbClose.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // TabMenu
            // 
            TabMenu.AutoSize = true;
            TabMenu.BackColor = Color.WhiteSmoke;
            TabMenu.Dock = DockStyle.Top;
            TabMenu.Location = new Point(2, 32);
            TabMenu.Margin = new Padding(0);
            TabMenu.Name = "TabMenu";
            TabMenu.Padding = new Padding(0, 0, 0, 3);
            TabMenu.Size = new Size(1116, 99);
            TabMenu.TabIndex = 31;
            TabMenu.TabMgr = null;
            // 
            // dockPanel
            // 
            dockPanel.BackColor = SystemColors.Control;
            dockPanel.Dock = DockStyle.Fill;
            dockPanel.DockBackColor = Color.FromArgb(238, 238, 242);
            dockPanel.DockBottomPortion = 150D;
            dockPanel.DockLeftPortion = 200D;
            dockPanel.DockRightPortion = 200D;
            dockPanel.DockTopPortion = 150D;
            dockPanel.Font = new Font("Tahoma", 11F, FontStyle.Regular, GraphicsUnit.World);
            dockPanel.Location = new Point(2, 131);
            dockPanel.Margin = new Padding(0, 3, 0, 0);
            dockPanel.Name = "dockPanel";
            dockPanel.RightToLeftLayout = true;
            dockPanel.ShowAutoHideContentOnHover = false;
            dockPanel.Size = new Size(1116, 547);
            dockPanel.TabIndex = 29;
            dockPanel.Theme = vS2015LightTheme1;
            // 
            // MainUIWindow
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Silver;
            ClientSize = new Size(1120, 680);
            Controls.Add(dockPanel);
            Controls.Add(TabMenu);
            Controls.Add(titleBar);
            DoubleBuffered = true;
            FormBorderStyle = FormBorderStyle.None;
            Icon = (Icon)resources.GetObject("$this.Icon");
            IsMdiContainer = true;
            Margin = new Padding(2, 3, 2, 3);
            Name = "MainUIWindow";
            Padding = new Padding(2);
            StartPosition = FormStartPosition.CenterScreen;
            Text = "LightCAD";
            headerCtxMenu.ResumeLayout(false);
            contextMenuStrip1.ResumeLayout(false);
            lbMessage_Menu.ResumeLayout(false);
            titleBar.ResumeLayout(false);
            titleBar.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private ContextMenuStrip headerCtxMenu;
        private ToolStripMenuItem mui_CloseFile;
        private ToolStripMenuItem mui_NewWindow;
        private ImageList imageList;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem ssddfToolStripMenuItem;
        private ToolStripMenuItem sdfasdfToolStripMenuItem;
        private WeifenLuo.WinFormsUI.Docking.VS2015BlueTheme vS2015BlueTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2015LightTheme vS2015LightTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2015DarkTheme vS2015DarkTheme1;
        private WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender vsToolStripExtender1;
        private ToolTip ToolTips;
        private ContextMenuStrip lbMessage_Menu;
        private ToolStripMenuItem 下拉菜单ToolStripMenuItem;
        public Panel titleBar;
        private Label lbUser;
        private Label lbMessage;
        public TitleTabControl TitleTab;
        private Label lbClose;
        private Label lbMax;
        private Label lbMin;
        public TabMenuControl TabMenu;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        private Label lbSettings;
        private ToolStripMenuItem tsiSync3DViewport;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem 双窗口同屏并列ToolStripMenuItem;
        private ToolStripMenuItem 绘图模型并列显示ToolStripMenuItem;
        private ToolStripMenuItem 绘图视图并列显示ToolStripMenuItem;
        private ToolStripMenuItem 模型视图并列显示ToolStripMenuItem;
    }
}
