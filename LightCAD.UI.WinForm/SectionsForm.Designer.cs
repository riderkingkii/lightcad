namespace LightCAD.UI
{
    partial class SectionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            panel1 = new System.Windows.Forms.Panel();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            panel3 = new System.Windows.Forms.Panel();
            listBox1 = new System.Windows.Forms.ListBox();
            picTip = new System.Windows.Forms.PictureBox();
            contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(components);
            ������ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ɾ��ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            panel2 = new System.Windows.Forms.Panel();
            lbBoxSize = new System.Windows.Forms.Label();
            btnDelete = new System.Windows.Forms.Button();
            button2 = new System.Windows.Forms.Button();
            button1 = new System.Windows.Forms.Button();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picTip).BeginInit();
            contextMenuStrip1.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(pictureBox1);
            panel1.Controls.Add(panel3);
            panel1.Controls.Add(listBox1);
            panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Padding = new System.Windows.Forms.Padding(20);
            panel1.Size = new System.Drawing.Size(534, 333);
            panel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            pictureBox1.BackColor = System.Drawing.Color.Black;
            pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            pictureBox1.Location = new System.Drawing.Point(155, 20);
            pictureBox1.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            pictureBox1.Size = new System.Drawing.Size(359, 293);
            pictureBox1.TabIndex = 2;
            pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            panel3.Dock = System.Windows.Forms.DockStyle.Left;
            panel3.Location = new System.Drawing.Point(125, 20);
            panel3.Name = "panel3";
            panel3.Size = new System.Drawing.Size(30, 293);
            panel3.TabIndex = 3;
            // 
            // listBox1
            // 
            listBox1.Dock = System.Windows.Forms.DockStyle.Left;
            listBox1.FormattingEnabled = true;
            listBox1.ItemHeight = 17;
            listBox1.Location = new System.Drawing.Point(20, 20);
            listBox1.Name = "listBox1";
            listBox1.Size = new System.Drawing.Size(105, 293);
            listBox1.TabIndex = 1;
            listBox1.SelectedValueChanged += listBox1_SelectedValueChanged;
            listBox1.MouseDown += listBox1_MouseDown;
            listBox1.MouseLeave += listBox1_MouseLeave;
            listBox1.MouseMove += listBox1_MouseMove;
            // 
            // picTip
            // 
            picTip.BackColor = System.Drawing.Color.Black;
            picTip.Location = new System.Drawing.Point(82, 256);
            picTip.Name = "picTip";
            picTip.Size = new System.Drawing.Size(82, 63);
            picTip.TabIndex = 4;
            picTip.TabStop = false;
            picTip.Visible = false;
            picTip.WaitOnLoad = true;
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { ������ToolStripMenuItem, ɾ��ToolStripMenuItem });
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new System.Drawing.Size(113, 48);
            // 
            // ������ToolStripMenuItem
            // 
            ������ToolStripMenuItem.Name = "������ToolStripMenuItem";
            ������ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            ������ToolStripMenuItem.Text = "������";
            ������ToolStripMenuItem.Click += ������ToolStripMenuItem_Click;
            // 
            // ɾ��ToolStripMenuItem
            // 
            ɾ��ToolStripMenuItem.Name = "ɾ��ToolStripMenuItem";
            ɾ��ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            ɾ��ToolStripMenuItem.Text = "ɾ��";
            ɾ��ToolStripMenuItem.Click += ɾ��ToolStripMenuItem_Click;
            // 
            // panel2
            // 
            panel2.Controls.Add(lbBoxSize);
            panel2.Controls.Add(btnDelete);
            panel2.Controls.Add(button2);
            panel2.Controls.Add(button1);
            panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            panel2.Location = new System.Drawing.Point(0, 333);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(534, 47);
            panel2.TabIndex = 2;
            // 
            // lbBoxSize
            // 
            lbBoxSize.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            lbBoxSize.Location = new System.Drawing.Point(154, 15);
            lbBoxSize.Name = "lbBoxSize";
            lbBoxSize.Size = new System.Drawing.Size(197, 23);
            lbBoxSize.TabIndex = 3;
            lbBoxSize.Text = "label1";
            // 
            // btnDelete
            // 
            btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnDelete.Location = new System.Drawing.Point(20, 12);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new System.Drawing.Size(75, 23);
            btnDelete.TabIndex = 2;
            btnDelete.Text = "ɾ��";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // button2
            // 
            button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            button2.Location = new System.Drawing.Point(447, 12);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(75, 23);
            button2.TabIndex = 1;
            button2.Text = "ȡ��";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // button1
            // 
            button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            button1.Location = new System.Drawing.Point(357, 12);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(75, 23);
            button1.TabIndex = 0;
            button1.Text = "ȷ��";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // SectionsForm
            // 
            ClientSize = new System.Drawing.Size(534, 380);
            Controls.Add(picTip);
            Controls.Add(panel1);
            Controls.Add(panel2);
            Name = "SectionsForm";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "ѡ������";
            panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)picTip).EndInit();
            contextMenuStrip1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbBoxSize;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ������ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ɾ��ToolStripMenuItem;
        private System.Windows.Forms.PictureBox picTip;
    }
}