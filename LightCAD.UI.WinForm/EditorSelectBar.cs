﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class EditorSelectBar : UserControl
    {
        public string EditName { get; set; }
        public string ContextMenuItemName { get; set; }

        public event EventHandler TwinDisplay;
        public event EventHandler EditChanged;
        public event EventHandler ContextMenuClick;
        public event EventHandler SyncViewportClick;
        public event EventHandler SectionFrameClick;
        public EditorSelectBar()
        {
            InitializeComponent();
            this.Load += EditorSelectBar_Load;
        }

        private void EditorSelectBar_Load(object? sender, EventArgs e)
        {
            this.cboWin1.SelectedIndex = 0;
            this.cboWin2.SelectedIndex = 2;
        }

        private void SelecctButton(Button btn)
        {
            btnDrawing.BackColor = Color.White;
            btnPaper.BackColor = Color.White;
            btnModel3D.BackColor = Color.White;
            btnView3D.BackColor = Color.White;
            btn.BackColor = Color.LightBlue;
        }

        private void btnDrawing_Click(object sender, EventArgs e)
        {
            this.EditName = "Drawing";
            SelecctButton(btnDrawing);
            EditChanged?.Invoke(this, EventArgs.Empty);
        }

        private void btnModel3D_Click(object sender, EventArgs e)
        {
            this.EditName = "Model3D";
            SelecctButton(btnModel3D);
            EditChanged?.Invoke(this, EventArgs.Empty);
        }

        private void btnView3D_Click(object sender, EventArgs e)
        {
            this.EditName = "View3D";
            SelecctButton(btnView3D);
            EditChanged?.Invoke(this, EventArgs.Empty);

        }

        private void btnPaper_Click(object sender, EventArgs e)
        {
            this.EditName = "Paper";
            SelecctButton(btnPaper);
            EditChanged?.Invoke(this, EventArgs.Empty);

        }

        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.ContextMenuItemName = e.ClickedItem.Name;
            ContextMenuClick?.Invoke(this, EventArgs.Empty);
        }

        private void contextMenu_Opened(object sender, EventArgs e)
        {
            var btn = contextMenu.SourceControl as Button;
            btn.PerformClick();
        }
        internal void HideAll()
        {
            btnDrawing.Visible = false;
            btnPaper.Visible = false;
            btnModel3D.Visible = false;
            btnView3D.Visible = false;
        }

        internal void SetVisible(string editorName, bool visible)
        {
            switch (editorName)
            {
                case "Paper":
                    btnPaper.Visible = visible;
                    break;
                case "Model3D":
                    btnModel3D.Visible = visible;
                    break;
                case "View3D":
                    btnView3D.Visible = visible;
                    break;
            }
        }

        private string GetTwinName(ComboBox cbo)
        {
            if (cbo.Text == "绘图") return "Drawing";
            else if (cbo.Text == "图纸") return "Paper";
            else if (cbo.Text == "模型") return "Model3D";
            else if (cbo.Text == "视图") return "View3D";
            return null;

        }
        public string TwinName1 { get; set; }
        public string TwinName2 { get; set; }
        private void btnTwinDisplay_Click(object sender, EventArgs e)
        {
            if (cboWin1.Text == cboWin2.Text)
            {
                if (cboWin1.Text == "绘图") btnDrawing.PerformClick();
                else if (cboWin1.Text == "图纸") btnPaper.PerformClick();
                else if (cboWin1.Text == "模型") btnModel3D.PerformClick();
                else if (cboWin1.Text == "视图") btnView3D.PerformClick();
            }
            else
            {
                TwinName1 = GetTwinName(cboWin1);
                TwinName2 = GetTwinName(cboWin2);
                TwinDisplay?.Invoke(this, EventArgs.Empty);
            }
        }

        private void btnSync3DViewport_Click(object sender, EventArgs e)
        {
            SyncViewportClick?.Invoke(this, e);
        }

        private void btnSectionFrame_Click(object sender, EventArgs e)
        {
            SectionFrameClick?.Invoke(this, e);
        }
    }
}
