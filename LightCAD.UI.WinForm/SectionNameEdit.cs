using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Drawing.Actions;
using LightCAD.MathLib;
using LightCAD.Model;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Color = System.Drawing.Color;
using Point = System.Drawing.Point;

namespace LightCAD.UI
{
    public partial class SectionNameEdit : Form
    {

        private LcSection Section;
        public SectionNameEdit(LcSection section)
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;
            Section= section;
            this.textBox1.Text = Section.Name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBox1.Text.Trim()))
            {
                MessageBox.Show("名称不能为空！");
                return;
            }
            if (SectionManager.Sections.Any(n=>n.Key!= Section.Uuid&&n.Value.Name== this.textBox1.Text.Trim()))
            {
                MessageBox.Show("轮廓名称已存在！");
                return;
            }
            Section.Name = this.textBox1.Text.Trim();
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }


    }
}