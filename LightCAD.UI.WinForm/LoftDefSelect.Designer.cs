namespace LightCAD.UI
{
    partial class LoftDefSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel2 = new System.Windows.Forms.Panel();
            btnCreateLoftDef = new System.Windows.Forms.Button();
            btnCancel = new System.Windows.Forms.Button();
            btnConfirm = new System.Windows.Forms.Button();
            listBox1 = new System.Windows.Forms.ListBox();
            panel1 = new System.Windows.Forms.Panel();
            btnAddSection = new System.Windows.Forms.Button();
            panel2.SuspendLayout();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // panel2
            // 
            panel2.Controls.Add(btnAddSection);
            panel2.Controls.Add(btnCreateLoftDef);
            panel2.Controls.Add(btnCancel);
            panel2.Controls.Add(btnConfirm);
            panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            panel2.Location = new System.Drawing.Point(0, 312);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(357, 46);
            panel2.TabIndex = 2;
            // 
            // btnCreateLoftDef
            // 
            btnCreateLoftDef.Location = new System.Drawing.Point(12, 6);
            btnCreateLoftDef.Name = "btnCreateLoftDef";
            btnCreateLoftDef.Size = new System.Drawing.Size(107, 23);
            btnCreateLoftDef.TabIndex = 2;
            btnCreateLoftDef.Text = "创建放样体定义";
            btnCreateLoftDef.UseVisualStyleBackColor = true;
            btnCreateLoftDef.Click += btnCreateLoftDef_Click;
            // 
            // btnCancel
            // 
            btnCancel.Location = new System.Drawing.Point(291, 6);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(57, 23);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "取消";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // btnConfirm
            // 
            btnConfirm.Location = new System.Drawing.Point(236, 6);
            btnConfirm.Name = "btnConfirm";
            btnConfirm.Size = new System.Drawing.Size(49, 23);
            btnConfirm.TabIndex = 0;
            btnConfirm.Text = "确定";
            btnConfirm.UseVisualStyleBackColor = true;
            btnConfirm.Click += btnConfirm_Click;
            // 
            // listBox1
            // 
            listBox1.Dock = System.Windows.Forms.DockStyle.Left;
            listBox1.FormattingEnabled = true;
            listBox1.ItemHeight = 17;
            listBox1.Location = new System.Drawing.Point(20, 20);
            listBox1.Name = "listBox1";
            listBox1.Size = new System.Drawing.Size(314, 272);
            listBox1.TabIndex = 1;
            // 
            // panel1
            // 
            panel1.Controls.Add(listBox1);
            panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Padding = new System.Windows.Forms.Padding(20);
            panel1.Size = new System.Drawing.Size(357, 312);
            panel1.TabIndex = 1;
            // 
            // btnAddSection
            // 
            btnAddSection.Location = new System.Drawing.Point(125, 6);
            btnAddSection.Name = "btnAddSection";
            btnAddSection.Size = new System.Drawing.Size(71, 23);
            btnAddSection.TabIndex = 3;
            btnAddSection.Text = "添加轮廓";
            btnAddSection.UseVisualStyleBackColor = true;
            btnAddSection.Click += btnAddSection_Click;
            // 
            // LoftDefSelect
            // 
            ClientSize = new System.Drawing.Size(357, 358);
            Controls.Add(panel1);
            Controls.Add(panel2);
            Name = "LoftDefSelect";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "选择放样定义 ";
            panel2.ResumeLayout(false);
            panel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnCreateLoftDef;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAddSection;
    }
}