﻿namespace LightCAD.UI
{
    partial class PropertyControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            dgvProps = new System.Windows.Forms.DataGridView();
            colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            colValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            cboEleTypes = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)dgvProps).BeginInit();
            SuspendLayout();
            // 
            // dgvProps
            // 
            dgvProps.AllowUserToAddRows = false;
            dgvProps.AllowUserToDeleteRows = false;
            dgvProps.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            dgvProps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvProps.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { colTitle, colValue });
            dgvProps.Location = new System.Drawing.Point(0, 34);
            dgvProps.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            dgvProps.Name = "dgvProps";
            dgvProps.RowHeadersVisible = false;
            dgvProps.RowHeadersWidth = 51;
            dgvProps.RowTemplate.Height = 29;
            dgvProps.Size = new System.Drawing.Size(370, 519);
            dgvProps.TabIndex = 1;
            // 
            // colTitle
            // 
            colTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            colTitle.DataPropertyName = "DisplayName";
            colTitle.HeaderText = "属性名称";
            colTitle.MinimumWidth = 6;
            colTitle.Name = "colTitle";
            colTitle.ReadOnly = true;
            // 
            // colValue
            // 
            colValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            colValue.HeaderText = "属性数值";
            colValue.MinimumWidth = 6;
            colValue.Name = "colValue";
            // 
            // cboEleTypes
            // 
            cboEleTypes.FormattingEnabled = true;
            cboEleTypes.Location = new System.Drawing.Point(3, 3);
            cboEleTypes.Name = "cboEleTypes";
            cboEleTypes.Size = new System.Drawing.Size(174, 25);
            cboEleTypes.TabIndex = 2;
            cboEleTypes.SelectedIndexChanged += cboEleTypes_SelectedIndexChanged;
            // 
            // PropertyCotrol
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(cboEleTypes);
            Controls.Add(dgvProps);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "PropertyCotrol";
            Size = new System.Drawing.Size(370, 553);
            ((System.ComponentModel.ISupportInitialize)dgvProps).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.DataGridView dgvProps;
        private System.Windows.Forms.ComboBox cboEleTypes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValue;
    }
}
