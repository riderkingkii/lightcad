﻿namespace LightCAD.UI
{
    partial class PropertyControl2
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            cboEleTypes = new System.Windows.Forms.ComboBox();
            propertyGrid = new System.Windows.Forms.PropertyGrid();
            btnEditCptDef = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // cboEleTypes
            // 
            cboEleTypes.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            cboEleTypes.FormattingEnabled = true;
            cboEleTypes.Location = new System.Drawing.Point(4, 7);
            cboEleTypes.Name = "cboEleTypes";
            cboEleTypes.Size = new System.Drawing.Size(275, 25);
            cboEleTypes.TabIndex = 2;
            cboEleTypes.SelectedIndexChanged += cboEleTypes_SelectedIndexChanged;
            // 
            // propertyGrid
            // 
            propertyGrid.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            propertyGrid.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            propertyGrid.Location = new System.Drawing.Point(4, 34);
            propertyGrid.Name = "propertyGrid";
            propertyGrid.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            propertyGrid.Size = new System.Drawing.Size(346, 475);
            propertyGrid.TabIndex = 3;
            propertyGrid.ToolbarVisible = false;
            propertyGrid.PropertyValueChanged += propertyGrid_PropertyValueChanged;
            // 
            // btnEditCptDef
            // 
            btnEditCptDef.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnEditCptDef.Location = new System.Drawing.Point(285, 6);
            btnEditCptDef.Name = "btnEditCptDef";
            btnEditCptDef.Size = new System.Drawing.Size(65, 25);
            btnEditCptDef.TabIndex = 4;
            btnEditCptDef.Text = "编辑类型";
            btnEditCptDef.UseVisualStyleBackColor = true;
            btnEditCptDef.Click += btnEditCptDef_Click;
            // 
            // PropertyControl2
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(354, 514);
            Controls.Add(btnEditCptDef);
            Controls.Add(propertyGrid);
            Controls.Add(cboEleTypes);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "PropertyControl2";
            Padding = new System.Windows.Forms.Padding(4);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.ComboBox cboEleTypes;
        private System.Windows.Forms.PropertyGrid propertyGrid;
        private System.Windows.Forms.Button btnEditCptDef;
    }
}
