﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class CptDefPropertyControl : Form
    {
        private IComponentInstance Cpt;
        public CptDefPropertyControl(IComponentInstance cpt)
        {
            InitializeComponent();
            this.Cpt = cpt;
            BindProperties();
        }

        public void BindProperties()
        {
            PropertyManageCls pmc = new PropertyManageCls();
            var cptDef = this.Cpt.Definition;
            foreach (var def in cptDef.TypeParameters.Definition)
            {
                var name = def.Name;
                var value = cptDef.TypeParameters[name] ?? "（空）";

                var obj = new PropObject(name, value)
                {
                    Category = "类型参数",
                    DisplayName = def.DisplayName,
                };

                var type = def.DataType;

                switch (type)
                {
                    case LcDataType.String:
                        obj.Converter = new StringConverter();
                        break;
                    case LcDataType.Float:
                        obj.Converter = new SingleConverter();
                        break;
                    case LcDataType.Double:
                        obj.Converter = new DoubleConverter();
                        break;
                    case LcDataType.Int:
                        obj.Converter = new Int32Converter();
                        break;
                    case LcDataType.Long:
                        obj.Converter = new Int64Converter();
                        break;
                    case LcDataType.Bool:
                        obj.Converter = new BooleanConverter();
                        break;
                    case LcDataType.DateTime:
                        obj.Converter = new DateTimeConverter();
                        break;
                    case LcDataType.Point2d:
                    case LcDataType.Point3d:
                    case LcDataType.CurveArray:
                    case LcDataType.Curve2d:
                    default:
                        continue;
                }
                bool isOk = pmc.Add(obj);


                //if (def.DataType == LcDataType.CurveArray)
                //{
                //    //obj.Value = Color.Red;
                //    obj.Converter = new DropDownListConverter(value as Curve2d[]);
                //}
            }

            this.propertyGrid.SelectedObject = pmc;
            this.propertyGrid.Refresh();
        }

        private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            string oldValue = e.OldValue.ToString();
            string newValue = e.ChangedItem.Value.ToString();
            if (newValue == oldValue) //未修改值
                return;

            string propName = e.ChangedItem.Label;
            (this.Cpt as LcElement)?.OnPropertyChangedBefore(propName, null, null);
            this.Cpt.Definition.TypeParameters[propName] = newValue;
            (this.Cpt as LcElement)?.OnPropertyChangedAfter(propName, null, null);
        }
    }
}
