﻿namespace LightCAD.UI
{
    partial class RefEditSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tvwBlockRef = new System.Windows.Forms.TreeView();
            picTip = new System.Windows.Forms.PictureBox();
            btnCancel = new System.Windows.Forms.Button();
            btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)picTip).BeginInit();
            SuspendLayout();
            // 
            // tvwBlockRef
            // 
            tvwBlockRef.Location = new System.Drawing.Point(12, 12);
            tvwBlockRef.Name = "tvwBlockRef";
            tvwBlockRef.Size = new System.Drawing.Size(304, 339);
            tvwBlockRef.TabIndex = 0;
            // 
            // picTip
            // 
            picTip.Location = new System.Drawing.Point(322, 12);
            picTip.Name = "picTip";
            picTip.Size = new System.Drawing.Size(389, 339);
            picTip.TabIndex = 1;
            picTip.TabStop = false;
            // 
            // btnCancel
            // 
            btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnCancel.Location = new System.Drawing.Point(611, 392);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(100, 28);
            btnCancel.TabIndex = 11;
            btnCancel.Text = "取消";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOK.Location = new System.Drawing.Point(500, 392);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(100, 28);
            btnOK.TabIndex = 10;
            btnOK.Text = "确定";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // RefEditSelector
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(723, 432);
            Controls.Add(btnCancel);
            Controls.Add(btnOK);
            Controls.Add(picTip);
            Controls.Add(tvwBlockRef);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "RefEditSelector";
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "在位编辑选择";
            ((System.ComponentModel.ISupportInitialize)picTip).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TreeView tvwBlockRef;
        private System.Windows.Forms.PictureBox picTip;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}