using LightCAD.Core;
using LightCAD.Core.Element3d;
using LightCAD.Core.Elements;
using LightCAD.Drawing.Actions;
using LightCAD.MathLib;
using LightCAD.Model;
using LightCAD.Runtime;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace LightCAD.UI
{
    public partial class ExtrudeDefSelect : Form, IExtrudeDefWindow
    {
        public Bounds EditorBounds => new Bounds();
        public string Uuid { get; set; }
        public string Name { get; set; }
        public double TopScale { get; set; }
        public double BottomScale { get; set; }

        public bool IsActive => true;

        public Type WinType => this.GetType();

        public string CurrentAction { get; set; }
        public ExtrudeDefSelect()
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;
            LoadList();
        }
        private void LoadList()
        {
            this.listBox1.SelectionMode = SelectionMode.One;
            List<BoxItem> items= ComponentManager.GetCptDefs("拉伸体", "拉伸体").Select(n => new BoxItem { Id = n.Uuid, Name = n.Name }).ToList();
            this.listBox1.DataSource = items;
            this.listBox1.DisplayMember = "Name";
            this.listBox1.ValueMember = "Id";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("请选择拉伸体定义");
                return;
            }
            if (!(double.TryParse(textBox1.Text.Trim(), out var bottomRatio) && bottomRatio > 0))
            {
                MessageBox.Show("请设置正确的缩放比例");
                return;
            }
            if (!(double.TryParse(textBox2.Text.Trim(), out var topRatio) && topRatio > 0))
            {
                MessageBox.Show("请设置正确的缩放比例");
                return;
            }
            Uuid = listBox1.SelectedValue.ToString();
            Name = listBox1.SelectedItem.ToString();
            TopScale = topRatio;
            BottomScale = bottomRatio;
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SectionsForm sectionsForm = new SectionsForm();
            if( sectionsForm.ShowDialog()==DialogResult.OK )
            {
                var def = ComponentManager.GetCptDefs("拉伸体", "拉伸体").FirstOrDefault(n => n.TypeParameters.GetValue<string>("SectionId") == sectionsForm.SectionId);
                if (def!=null)
                {
                    MessageBox.Show("当前轮廓拉伸体定义已存在");
                    this.listBox1.SelectedValue= def.Uuid;
                    return;
                }
                LcExtrudeDef extrudeDef = new LcExtrudeDef(Guid.NewGuid().ToString(),"拉伸体", sectionsForm.SectionId);
                extrudeDef.Name = sectionsForm.SectionName;
                ComponentManager.AddCptDefs(extrudeDef);
                LoadList();
            }
        }
    }
    public class BoxItem
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}