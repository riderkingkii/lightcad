﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.UI
{
    public static class TabMenuCreator
    {
        public static EventHandler WindowHandler { get; set; }
        public static void CreateDrawing(TabMenuControl tabMenu)
        {
            var file = new TabItem
            {
                Name = "File",
                Text = "文件",
                ShortcutKey = "ALT-F",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Name="Basic",
                        Text="基本功能",
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="New",
                                Text="新建",
                                IsCommand=true,
                                Icon= Properties.FileResources.NewFile
                            },
                            new TabButton{
                                Name="Open",
                                Text="打开",
                                IsCommand=true,
                                Icon= Properties.FileResources.OpenFile
                            },
                            new TabButton{
                                Name="Save",
                                Text="保存",
                                IsCommand=true,
                                Icon= Properties.FileResources.SaveFile
                            },
                             new TabButton{
                                Name="SaveAs",
                                Text="另存",
                                IsCommand=true,
                                Icon= Properties.FileResources.SaveAsFile
                            },
                            new TabButton{
                                Name="Close",
                                Text="关闭",
                                IsCommand=true,
                                Icon= Properties.FileResources.CloseFile
                           }
                        }
                    },
                    new TabButtonGroup {
                        Name="Print",
                        Text="打印功能",
                        Buttons = new List<TabButton>
                        {

                            new TabButton{
                                Name="PlotterManager",
                                Text="绘图仪设置",
                                IsCommand=true,
                                Icon= Properties.FileResources.PlotterManager,
                                IsSmallSize=true,
                                Width=135
                            },
                            new TabButton{
                                Name="StylesManager",
                                Text="打印样式设置",
                                IsCommand=true,
                                Icon= Properties.FileResources.StylesManager,
                                IsSmallSize=true,
                                Width=135
                            },
                            new TabButton{
                                Name="PageSetup",
                                Text="页面设置",
                                IsCommand=true,
                                Icon= Properties.FileResources.PageSetup,
                                IsSmallSize=true,
                                Width=110
                            },
                             new TabButton{
                                Name="Preview",
                                Text="打印预览",
                                IsCommand=true,
                                Icon= Properties.FileResources.Preview,
                                IsSmallSize=true,
                                Width=110
                            },
                            new TabButton{
                                Name="Plot",
                                Text="打印",
                                IsCommand=true,
                                Icon= Properties.FileResources.Plot
                           }
                        }
                    },
                    new TabButtonGroup
                    {
                        Name="Other",
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="DwgProps",
                                Text="图纸属性",
                                IsCommand=true,
                                Icon= Properties.FileResources.DwgProps,
                                IsSmallSize=true,
                                Width=120
                            },
                            new TabButton{
                                Name="Purge",
                                Text="图纸清理",
                                IsCommand=true,
                                Icon= Properties.FileResources.Purge,
                                IsSmallSize=true,
                                Width=120,
                                DropDowns= new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Audit",
                                        Text="图纸核查",
                                        IsCommand=true,
                                        Icon= Properties.FileResources.Audit,
                                    },
                                    new TabButton
                                    {
                                        Name="Recover",
                                        Text="图纸修复",
                                        IsCommand=true,
                                        Icon= Properties.FileResources.Recover,
                                    },
                                }
                            },
                        }
                    }
                }
            };
            var drawingEdit = new TabItem
            {
                Name = "DrawingEdit",
                Text = "编辑",
                ShortcutKey = "ALT-E",
                ButtonGroups=new List<TabButtonGroup>
                {
                    new TabButtonGroup
                    {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="CutClip",
                                Text="剪切",
                                Icon= Properties.Resources.ClipCut,
                                IsCommand=true,
                            },
                            new TabButton{
                                Name="CopyClip",
                                Text="复制",
                                Icon= Properties.Resources.ClipCopy,
                                IsCommand=true,
                                DropDowns=new List<TabButton> {
                                    new TabButton{
                                        Name="CopyBase",
                                        Text="基点复制",
                                        Icon= Properties.Resources.ClipCopyBase,
                                        IsCommand=true,
                                    },
                                }
                            }, 
                            new TabButton{
                                Name="PasteClip",
                                Text="黏贴",
                                Icon= Properties.Resources.ClipPaste,
                                IsCommand=true,
                                DropDowns=new List<TabButton>
                                {
                                     new TabButton{
                                        Name="PasteBlock",
                                        Text="黏贴为块",
                                        Icon= Properties.Resources.ClipPasteBlock,
                                        IsCommand=true,
                                        IsSmallSize=true,
                                     },
                                    new TabButton{
                                        Name="PasteSpec",
                                        Text="选择黏贴",
                                        Icon= Properties.Resources.ClipPasteSpec,
                                        IsCommand=true,
                                        IsSmallSize=true,
                                    },
                                }
                            },
                            new TabButton
                            {
                                Name="SelAll",
                                Text="全选",
                                Icon= Properties.Resources.SelAll,
                                IsCommand=true,
                            },
                            new TabButton
                            {
                                Name="QuickSelect",
                                Text="快速选择",
                                Icon= Properties.Resources.QSelect,
                                IsCommand=true,
                            },
                            new TabButton
                            {
                                Name="Find",
                                Text="查找",
                                Icon= Properties.Resources.Find,
                                IsCommand=true,
                            }
                        }
                    },
                    new TabButtonGroup
                    {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Pan",
                                Text="平移",
                                Icon= Properties.Resources.Pan,
                                IsCommand=true,
                                DropDowns=new List<TabButton>
                                {
                                    new TabButton{
                                        Name="Pan_B",
                                        Text="基点平移",
                                        Icon= Properties.Resources.Pan_B,
                                        IsCommand=true,
                                    },
                                }
                            },
                            new TabButton{
                                Name="Zoom",
                                Text="缩放",
                                Icon= Properties.Resources.zoom,
                                IsCommand=true,
                                DropDowns=new List<TabButton>
                                {
                                    new TabButton{
                                        Name="Zoom_W",
                                        Text="窗口",
                                        Icon= Properties.Resources.Zoom_W,
                                        IsCommand=true,
                                    },
                                    new TabButton{
                                        Name="Zoom_O",
                                        Text="对象",
                                        Icon= Properties.Resources.Zoom_O,
                                        IsCommand=true,
                                    },
                                    new TabButton{
                                        Name="Zoom_A",
                                        Text="全部",
                                        Icon= Properties.Resources.Zoom_A,
                                        IsCommand=true,
                                    },

                                    new TabButton{
                                        Name="Zoom_E",
                                        Text="范围",
                                        Icon= Properties.Resources.Zoom_E,
                                        IsCommand=true,
                                    },
                                }
                            },
                        },
                    },
                    new TabButtonGroup
                    {
                        Buttons=new List<TabButton>
                        {
                            new TabButton
                            {
                                Name="Ucs",
                                Text="用户坐标系",
                                Icon= Properties.Resources.ucs,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=128
                            },
                            new TabButton
                            {
                                Name="Wcs",
                                Text="世界坐标系",
                                Icon= Properties.Resources.Wcs,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=128
                            },
                            new TabButton
                            {
                                Name="Plan",
                                Text="放正坐标系",
                                Icon= Properties.Resources.plan,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=128
                            },
                            new TabButton
                            {
                                Name="Plan_R",
                                Text="恢复坐标系",
                                Icon= Properties.Resources.Plan_R,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=128
                            }
                        }
                    }
                }
            };
            var drawingElement = new TabItem
            {
                Name = "DrawingElement",
                Text = "绘图",
                ShortcutKey = "ALT-D",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Line",
                                Text="直线",
                                Icon= Properties.Resources.Line,
                                IsCommand=true,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Ray",
                                        Text="射线",
                                        Icon= Properties.Resources.Ray,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="XLine",
                                        Text="构造线",
                                        Icon= Properties.Resources.XLine,
                                        IsCommand=true,
                                    },
                                     new TabButton
                                    {
                                        Name="MLine",
                                        Text="多线",
                                        Icon= Properties.Resources.MLine,
                                        IsCommand=true,
                                    },
                                }
                            },
                            new TabButton{
                                Name="PLine",
                                Text="多段线",
                                Icon= Properties.Resources.PLine,
                                IsCommand=true,
                            },
                            new TabButton{
                                Name="Rectang",
                                Text="矩形",
                                Icon= Properties.Resources.Rectang,
                                IsCommand=true,
                                IsSmallSize=true
                            },
                            new TabButton{
                                Name="Polygon",
                                Text="多边形",
                                Icon= Properties.Resources.Polygon,
                                IsCommand=true,
                                IsSmallSize=true
                            },
                            new TabButton
                            {
                                Name="Circle",
                                Text="圆",
                                Icon= Properties.Resources.Circle_CR,
                                IsCommand=true,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Circle_CD",
                                        Text="圆心,直径",
                                        Icon= Properties.Resources.Circle_CD,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Circle_2P",
                                        Text="两点画圆",
                                        Icon= Properties.Resources.Circle_2P,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Circle_3P",
                                        Text="三点画圆",
                                        Icon= Properties.Resources.Circle_3P,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="__Separator"
                                    },
                                    new TabButton
                                    {
                                        Name="Circle_T",
                                        Text="相切,相切,半径",
                                        Icon= Properties.Resources.Circle_T,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Circle_TTT",
                                        Text="相切,相切,相切",
                                        Icon= Properties.Resources.Circle_T3,
                                        IsCommand=true,
                                    },
                                }
                            },
                            new TabButton
                            {
                                Name="Arc",
                                Text="圆弧",
                                Icon= Properties.Resources.Arc_3P,
                                IsCommand=true,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Arc_PCE",
                                        Text="起点,圆心,端点",
                                        Icon= Properties.Resources.Arc_PCE,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_PCA",
                                        Text="起点,圆心,角度",
                                        Icon= Properties.Resources.Arc_PCA,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_PCL",
                                        Text="起点,圆心,长度",
                                        Icon= Properties.Resources.Arc_PCL,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="__Separator",
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_PEA",
                                        Text="起点,端点,角度",
                                        Icon= Properties.Resources.Arc_PEA,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_PED",
                                        Text="起点,端点,方向",
                                        Icon= Properties.Resources.Arc_PED,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_PER",
                                        Text="起点,端点,半径",
                                        Icon= Properties.Resources.Arc_PER,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="__Separator",
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_CPE",
                                        Text="圆心,起点,端点",
                                        Icon= Properties.Resources.Arc_CPE,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_CPA",
                                        Text="圆心,起点,角度",
                                        Icon= Properties.Resources.Arc_CPA,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_CPL",
                                        Text="圆心,起点,长度",
                                        Icon= Properties.Resources.Arc_CPL,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="__Separator",
                                    },
                                    new TabButton
                                    {
                                        Name="Arc_CONTINUE",
                                        Text="继续",
                                        Icon= Properties.Resources.Arc_CONTINUE,
                                        IsCommand=true,
                                    },
                                }
                            },
                            new TabButton{
                                Name="Spline",
                                Text="样条曲线",
                                Icon= Properties.Resources.Spline,
                                IsSmallSize=true,
                                Width=120,
                                DropDowns=new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Spline_FIT",
                                        Text="拟合点",
                                        Icon= Properties.Resources.Spline_FIT,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Spline_CV",
                                        Text="控制点",
                                        Icon= Properties.Resources.Spline_CV,
                                        IsCommand=true,
                                    }
                                }
                            },
                            new TabButton{
                                Name="Ellipse",
                                Text="椭圆",
                                Icon= Properties.Resources.Ellipse,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=120,
                                DropDowns=new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Ellipse_C",
                                        Text="圆心",
                                        Icon= Properties.Resources.Ellipse_C,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Ellipse",
                                        Text="轴心,端点",
                                        Icon= Properties.Resources.Ellipse,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Ellipse_A",
                                        Text="椭圆弧",
                                        Icon= Properties.Resources.Ellipse_A,
                                        IsCommand=true,
                                    }
                                }
                            },
             
                        }
                    },
                    new TabButtonGroup {
                        Buttons=new List<TabButton>
                        {
                            new TabButton{
                                Name="Line",
                                Text="文字",
                                Icon= Properties.Resources.Line,
                                IsCommand=true,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Ray",
                                        Text="单行文本",
                                        Icon= Properties.Resources.Ray,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="XLine",
                                        Text="多行文本",
                                        Icon= Properties.Resources.XLine,
                                        IsCommand=true,
                                    }
                                }
                             },
                            new TabButton
                            {
                                Name="Table UI",
                                Text="表格",
                                Icon= Properties.Resources.Table,
                                IsCommand=true,
                            }
                        }
                    },
                    new TabButtonGroup {
                        Buttons=new List<TabButton>
                        {
                            new TabButton{
                                Name="Block",
                                Text="创建块",
                                Icon= Properties.Resources.block_create,
                                IsCommand=true,
                            },
                            new TabButton{
                                Name="BlockEdit",
                                Text="编辑块",
                                Icon= Properties.Resources.block_edit,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            },
                            new TabButton{
                                Name="WBlock",
                                Text="写入块",
                                Icon= Properties.Resources.block_write,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            },
                        }
                    },
                    new TabButtonGroup {
                        Buttons=new List<TabButton>
                        {
                            new TabButton{
                                Name="Section",
                                Text="创建轮廓",
                                Icon= Properties.Resources.block_create,
                                IsCommand=true,
                            },
                            new TabButton{
                                Name="SectionEdit",
                                Text="编辑轮廓",
                                Icon= Properties.Resources.block_edit,
                                IsCommand=true, 
                            },
                        }
                    },
                    new TabButtonGroup {
                        Buttons=new List<TabButton>
                        {
                            new TabButton{
                                Name="Group",
                                Text="创建组",
                                Icon= Properties.Resources.group_create,
                                IsCommand=true,
                            },
                            new TabButton{
                                Name="GroupEdit",
                                Text="编辑组",
                                Icon= Properties.Resources.group_edit,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            },
                             new TabButton{
                                Name="UnGroup",
                                Text="分解组",
                                Icon= Properties.Resources.group_cancel,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                             },
                        }
                    },
                    new TabButtonGroup{
                       Buttons=new List<TabButton>
                       { 
                           new TabButton{
                                Name="XOpen",
                                Text="打开外参",
                                Icon= Properties.Resources.XOpen,
                                IsCommand=true,
                                Width=78
                            },
                            new TabButton{
                                Name="RefEdit",
                                Text="在位编辑",
                                Icon= Properties.Resources.RefEdit,
                                IsCommand=true,
                                Width=78
                            },
                            new TabButton{
                                Name="RefClose",
                                Text="结束在位编辑",
                                Icon= Properties.Resources.RefClose,
                                IsCommand=true,
                                IsSmallSize = true,
                                Width=140
                            },
                            new TabButton{
                                Name="XSave",
                                Text="保存外部参照",
                                Icon= Properties.Resources.XSave,
                                IsCommand=true,
                                IsSmallSize = true,
                                Width=140
                            },
                       }
                    }
                }
            };
            var tool = new TabItem
            {
                Name = "Tool",
                Text = "工具",
                ShortcutKey = "ALT-T",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton
                            {
                                Name="Measure",
                                Text="测量",
                                Icon= Properties.Resources.DimLinear,
                                IsCommand=true,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Measure_DISTANCE",
                                        Text="距离",
                                        Icon= Properties.Resources.DimLinear,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Measure_RADIUS",
                                        Text="半径",
                                        Icon= Properties.Resources.DimRadius,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Measure_ANGLE",
                                        Text="角度",
                                        Icon= Properties.Resources.DimAngular,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="Measure_AREA",
                                        Text="面积",
                                        Icon= Properties.Resources.DimOrdinate,
                                        IsCommand=true,
                                    }
                                }
                            },

                            new TabButton
                            {
                                Name="Clip",
                                Text="裁剪",
                                Icon= Properties.Resources.DimLinear,
                                IsCommand=true,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="Clip_SINGLE",
                                        Text="距离",
                                        Icon= Properties.Resources.DimLinear,
                                        IsCommand=true,
                                    },
                                }
                            },
                            new TabButton
                            {
                                Name="DrawingOrder",
                                Text="绘图次序",
                                Icon= Properties.Resources.DrawingOrder_Pre,
                                IsCommand=true,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton
                                    {
                                        Name="DrawingOrder_PRE",
                                        Text="前置",
                                        Icon= Properties.Resources.DrawingOrder_Pre,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="DrawingOrder_POST",
                                        Text="后置",
                                        Icon= Properties.Resources.DrawingOrder_Post,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="DrawingOrder_UP",
                                        Text="置于对象之上",
                                        Icon= Properties.Resources.DrawingOrder_UP,
                                        IsCommand=true,
                                    },
                                    new TabButton
                                    {
                                        Name="DrawingOrder_DOWN",
                                        Text="置于对象之下",
                                        Icon= Properties.Resources.DrawingOrder_Down,
                                        IsCommand=true,
                                    },
                                }
                            }
                        }
                    }
                }
            };
            var drawingDim = new TabItem
            {
                Name = "DrawingDim",
                Text = "标注",
                ShortcutKey = "ALT-N",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="DimLinear",
                                Text="线性",
                                Icon= Properties.Resources.DimLinear,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="DimAligned",
                                Text="对齐",
                                Icon= Properties.Resources.DimAligned,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                           new TabButton{
                                Name="DimArc",
                                Text="弧长",
                                Icon= Properties.Resources.DimArc,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="DimOrdinate",
                                Text="坐标",
                                Icon= Properties.Resources.DimOrdinate,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="DimRadius",
                                Text="半径",
                                Icon= Properties.Resources.DimRadius,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="DimJogged",
                                Text="折弯",
                                Icon= Properties.Resources.DimJogged,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="DimDiameter",
                                Text="直径",
                                Icon= Properties.Resources.DimDiameter,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="DimAngular",
                                Text="角度",
                                Icon= Properties.Resources.DimAngular,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                        }
                    }
                }
            };


            var drawingModify = new TabItem
            {
                Name = "DrawingModify",
                Text = "修改",
                ShortcutKey = "ALT-E",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Erase",
                                Text="删除",
                                Icon= Properties.Resources.Erase,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="Copy",
                                Text="复制",
                                Icon= Properties.Resources.Copy,
                                IsCommand=true,
                                 IsSmallSize=true,
                           },
                           new TabButton{
                                Name="Mirror",
                                Text="镜像",
                                Icon= Properties.Resources.Mirror,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="Offset",
                                Text="偏移",
                                Icon= Properties.Resources.Offset,
                                IsCommand=true,
                                IsSmallSize=true,
                           },
                             new TabButton{
                                Name="ArrayRect",
                                Text="矩形阵列",
                                Icon= Properties.Resources.ArrayRect,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=120,
                                DropDowns = new List<TabButton>
                                {
                                    new TabButton{
                                        Name="ArrayPath",
                                        Text="路径阵列",
                                        Icon= Properties.Resources.ArrayPath,
                                        IsCommand=true,
                                    },
                                    new TabButton{
                                        Name="ArrayPolar",
                                        Text="环形阵列",
                                        Icon= Properties.Resources.ArrayPolar,
                                        IsCommand=true,
                                    },
                                }
                             },
                             new TabButton{
                                Name="OverKill",
                                Text="删除重复",
                                Icon= Properties.Resources.OverKill,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=120,
                             },
                        }
                    },
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Move",
                                Text="移动",
                                Icon= Properties.Resources.Move,
                                IsCommand=true
                            },
                            new TabButton{
                                Name="Rotate",
                                Text="旋转",
                                Icon= Properties.Resources.Rotate,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="Scale",
                                Text="缩放",
                                Icon= Properties.Resources.Scale,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="Stretch",
                                Text="拉伸",
                                Icon= Properties.Resources.Stretch,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="Lengthen",
                                Text="拉长",
                                Icon= Properties.Resources.Lengthen,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                        }
                    },
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Trim",
                                Text="修剪",
                                Icon= Properties.Resources.Trim,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                            new TabButton{
                                Name="Extend",
                                Text="延伸",
                                Icon= Properties.Resources.Extend,
                                IsCommand=true,
                                IsSmallSize=true,
                            },

                            new TabButton{
                                Name="Break",
                                Text="打断",
                                Icon= Properties.Resources.Break,
                                IsCommand=true,
                                IsSmallSize=true,
                            },

                            new TabButton{
                                Name="Join",
                                Text="合并",
                                Icon= Properties.Resources.Join,
                                IsCommand=true,
                                IsSmallSize=true,
                            },
                        }
                    },
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Explode",
                                Text="分解",
                                Icon= Properties.Resources.Explode,
                                IsCommand=true,
                            },
                        }
                    }
              }
            };

            var management = new TabItem
            {
                Name = "Management",
                Text = "管理",
                ShortcutKey = "ALT-G",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Building",
                                Text="建筑管理",
                                Icon= Properties.Resources.Building,
                                IsCommand=true,
                                Width=80
                            }, 
                            new TabButton{
                                Name="Drawing",
                                Text="插入图纸",
                                Icon= Properties.Resources.Drawing,
                                IsCommand=true,
                                Width=80
                            },
                            new TabButton{
                                Name="BasePoint",
                                Text="插入基点",
                                Icon= Properties.Resources.BasePoint,
                                IsCommand=true,
                                Width=80
                            }
                        }
                    },
                    new TabButtonGroup
                    {
                        Buttons=new List<TabButton>
                        {
                            new TabButton{
                                Name="BlockMgr",
                                Text="块管理",
                                Icon= Properties.Resources.block_mgr,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            },
                            new TabButton{
                                Name="GroupMgr",
                                Text="组管理",
                                Icon= Properties.Resources.group_manage,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            }
                        }
                    }

                }
            };

            var archMajor = new TabItem
            {
                Name = "ArchMajor",
                Text = "建筑",
                ShortcutKey = "ALT-A",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Wall",
                                Text="墙",
                                Icon= Properties.Resources.wall,
                                IsCommand=true,
                            },
                            new TabButton{
                                Name="Door",
                                Text="门",
                                Icon= Properties.Resources.door,
                                IsCommand=true,
                           },
                            new TabButton{
                                Name="Window",
                                Text="窗",
                                Icon= Properties.Resources.window,
                                IsCommand=true,
                            },
                            new TabButton{
                                Name="Slab",
                                Text="楼板",
                                Icon= Properties.Resources.Slab,
                                IsCommand=true,
                           },
                            new TabButton{
                                Name="Stair",
                                Text="楼梯",
                                Icon= Properties.Resources.Stair,
                                IsCommand=true,
                           },
                           new TabButton{
                                Name="Ramp",
                                Text="坡道",
                                Icon= Properties.Resources.Ramp,
                                IsCommand=true,
                           },
                           new TabButton{
                                Name="Roof",
                                Text="屋顶",
                                Icon= Properties.Resources.Roof,
                                IsCommand=true,
                           }
                        }
                    }
                }

            };

            var structMajor = new TabItem
            {
                Name = "StructMajor",
                Text = "结构",
                ShortcutKey = "ALT-S",
            };
            var waterMajor = new TabItem
            {
                Name = "WaterMajor",
                Text = "给排水",
                ShortcutKey = "ALT-W",
            };
            var hvacMajor = new TabItem
            {
                Name = "HvacMajor",
                Text = "暖通",
                ShortcutKey = "ALT-V",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="Duct",
                                Text="风管",
                                Icon= Properties.Resources.Duct,
                                IsCommand=true,
                            },
                                new TabButton{
                                Name="DuctConnect",
                                Text="弯头",
                                Icon= Properties.Resources.DuctConnect,
                                IsCommand=true,
                            },
                        }
                    }
                }
            };
            var elecMajor = new TabItem
            {
                Name = "ElecMajor",
                Text = "电气",
                ShortcutKey = "ALT-C",
            };


            var component = new TabItem
            {
                Name = "Component",
                Text = "构件",
                ShortcutKey = "ALT-C",
                ButtonGroups = new List<TabButtonGroup>
                {
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="LocalLibrary",
                                Text="本地构件库",
                                Icon= Properties.Resources.ComLibLocal,
                                IsCommand=true,
                                Width=110
                            },
                            new TabButton{
                                Name="CloudLibrary",
                                Text="云端构件库",
                                Icon= Properties.Resources.ComLibCloud,
                                IsCommand=true,
                                Width=110
                            },
                        }
                    },
                    new TabButtonGroup
                    {
                        Buttons=new List<TabButton>
                        {
                            new TabButton{
                                Name="NewComponent",
                                Text="新建",
                                Icon= Properties.Resources.ComNew,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            },
                            new TabButton{
                                Name="SaveComponent",
                                Text="保存",
                                Icon= Properties.Resources.ComSave,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            },
                            new TabButton{
                                Name="ApplyComponent",
                                Text="应用",
                                Icon= Properties.Resources.ComApply,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            },
                            new TabButton{
                                Name="CloseComponent",
                                Text="关闭",
                                Icon= Properties.Resources.ComClose,
                                IsCommand=true,
                                IsSmallSize=true,
                                Width=96
                            }
                        }
                    }

                }
            };


            var bimApp = new TabItem
            {
                Name = "BimApp",
                Text = "BIM应用",
                ShortcutKey = "ALT-B",
            };
            var extent = new TabItem
            {
                Name = "Extent",
                Text = "扩展",
                ShortcutKey = "ALT-X",
            };
            var window = new TabItem
            {
                Name = "Window",
                Text = "窗口",
                ShortcutKey = "ALT-W",
                ButtonGroups = new List<TabButtonGroup>
                {
                      new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="DrawingNewViewport",
                                Text="新绘图视口",
                                Icon= Properties.Resources.DrawingNewViewport,
                                Handler=WindowHandler,
                                Width=110
                            },
                            new TabButton{
                                Name="ModelNewViewport",
                                Text="新模型视口",
                                Icon= Properties.Resources.ModelNewViewport,
                                Handler=WindowHandler,
                                Width=110
                           }
                        }
                    },
                    new TabButtonGroup {
                        Buttons = new List<TabButton>
                        {
                            new TabButton{
                                Name="DrawingModelSideBySide",
                                Text="图模并列",
                                Icon= Properties.Resources.Win2Sidebyside,
                                Handler=WindowHandler,
                                Width=72
                            },
                            new TabButton{
                                Name="DrawingModelNormal",
                                Text="图模正常",
                                Icon= Properties.Resources.Win2Normal,
                                Handler=WindowHandler,
                                Width=72
                           }
                        }
                    },
                  
                }
            };

            var help = new TabItem
            {
                Name = "Help",
                Text = "帮助",
                ShortcutKey = "ALT-H",
            };

            var tabMgr = new TabMenuManager(tabMenu);
            var tabItems = new List<TabItem>
            {
               file,//文件
               drawingEdit,//编辑 视图 插入
               drawingElement,//绘图
               drawingDim,//标注
               drawingModify,//修改
               management,
               tool,//工具
               archMajor,//建筑专业
               structMajor,//结构专业
               waterMajor,//给排水专业
               hvacMajor,//暖通专业
               elecMajor,//电器专业
               component,//构件
               bimApp,//BIM应用功能
               extent,//各类扩展功能
               window,//窗口布局相关功能
               help//帮助相关功能
            };
            foreach (var tabItem in tabItems)
            {
                tabMgr.AddTab(tabItem);
            }
            tabMgr.SetTabSelected(file);
        }
    }
}
