﻿using LightCAD.Core.Elements;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public partial class RefEditSelector : Form, IRefEditSelector
    {
        public RefEditSelector()
        {
            InitializeComponent();
        }
        public Type WinType => this.GetType();
        public LcBlockRef Source { get; set; }
        public LcBlockRef[] RefLinks { get; set; }
        public LcBlockRef Target { get; set; }

        public bool IsActive => true;
    }
}
