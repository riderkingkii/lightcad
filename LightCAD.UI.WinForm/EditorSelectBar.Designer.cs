﻿namespace LightCAD.UI
{
    partial class EditorSelectBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            btnDrawing = new System.Windows.Forms.Button();
            btnModel3D = new System.Windows.Forms.Button();
            contextMenu = new System.Windows.Forms.ContextMenuStrip(components);
            mui_NewWindow = new System.Windows.Forms.ToolStripMenuItem();
            mui_OtherWindow = new System.Windows.Forms.ToolStripMenuItem();
            mui_MainWindow = new System.Windows.Forms.ToolStripMenuItem();
            btnView3D = new System.Windows.Forms.Button();
            btnPaper = new System.Windows.Forms.Button();
            cboWin1 = new System.Windows.Forms.ComboBox();
            cboWin2 = new System.Windows.Forms.ComboBox();
            btnTwinDisplay = new System.Windows.Forms.Button();
            panel1 = new System.Windows.Forms.Panel();
            btnSectionFrame = new System.Windows.Forms.Button();
            btnSync3DViewport = new System.Windows.Forms.Button();
            panel2 = new System.Windows.Forms.Panel();
            pnlRight = new System.Windows.Forms.Panel();
            contextMenu.SuspendLayout();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // btnDrawing
            // 
            btnDrawing.Dock = System.Windows.Forms.DockStyle.Left;
            btnDrawing.Location = new System.Drawing.Point(0, 0);
            btnDrawing.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            btnDrawing.Name = "btnDrawing";
            btnDrawing.Size = new System.Drawing.Size(76, 29);
            btnDrawing.TabIndex = 0;
            btnDrawing.Text = "绘图";
            btnDrawing.UseVisualStyleBackColor = true;
            btnDrawing.Click += btnDrawing_Click;
            // 
            // btnModel3D
            // 
            btnModel3D.BackColor = System.Drawing.Color.White;
            btnModel3D.ContextMenuStrip = contextMenu;
            btnModel3D.Dock = System.Windows.Forms.DockStyle.Left;
            btnModel3D.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            btnModel3D.Location = new System.Drawing.Point(152, 0);
            btnModel3D.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            btnModel3D.Name = "btnModel3D";
            btnModel3D.Size = new System.Drawing.Size(76, 29);
            btnModel3D.TabIndex = 1;
            btnModel3D.Text = "模型";
            btnModel3D.UseVisualStyleBackColor = false;
            btnModel3D.Click += btnModel3D_Click;
            // 
            // contextMenu
            // 
            contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { mui_NewWindow, mui_OtherWindow, mui_MainWindow });
            contextMenu.Name = "contextMenu";
            contextMenu.Size = new System.Drawing.Size(169, 76);
            contextMenu.Opening += contextMenu_Opening;
            contextMenu.Opened += contextMenu_Opened;
            contextMenu.ItemClicked += contextMenu_ItemClicked;
            // 
            // mui_NewWindow
            // 
            mui_NewWindow.Name = "mui_NewWindow";
            mui_NewWindow.Size = new System.Drawing.Size(168, 24);
            mui_NewWindow.Text = "新窗口显示";
            // 
            // mui_OtherWindow
            // 
            mui_OtherWindow.Name = "mui_OtherWindow";
            mui_OtherWindow.Size = new System.Drawing.Size(168, 24);
            mui_OtherWindow.Text = "其他窗口显示";
            // 
            // mui_MainWindow
            // 
            mui_MainWindow.Name = "mui_MainWindow";
            mui_MainWindow.Size = new System.Drawing.Size(168, 24);
            mui_MainWindow.Text = "主窗口显示";
            // 
            // btnView3D
            // 
            btnView3D.BackColor = System.Drawing.Color.White;
            btnView3D.ContextMenuStrip = contextMenu;
            btnView3D.Dock = System.Windows.Forms.DockStyle.Left;
            btnView3D.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            btnView3D.Location = new System.Drawing.Point(228, 0);
            btnView3D.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            btnView3D.Name = "btnView3D";
            btnView3D.Size = new System.Drawing.Size(76, 29);
            btnView3D.TabIndex = 2;
            btnView3D.Text = "视图";
            btnView3D.UseVisualStyleBackColor = false;
            btnView3D.Click += btnView3D_Click;
            // 
            // btnPaper
            // 
            btnPaper.BackColor = System.Drawing.Color.White;
            btnPaper.ContextMenuStrip = contextMenu;
            btnPaper.Dock = System.Windows.Forms.DockStyle.Left;
            btnPaper.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            btnPaper.Location = new System.Drawing.Point(76, 0);
            btnPaper.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            btnPaper.Name = "btnPaper";
            btnPaper.Size = new System.Drawing.Size(76, 29);
            btnPaper.TabIndex = 3;
            btnPaper.Text = "图纸";
            btnPaper.UseVisualStyleBackColor = false;
            btnPaper.Click += btnPaper_Click;
            // 
            // cboWin1
            // 
            cboWin1.BackColor = System.Drawing.Color.White;
            cboWin1.Dock = System.Windows.Forms.DockStyle.Right;
            cboWin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboWin1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cboWin1.FormattingEnabled = true;
            cboWin1.Items.AddRange(new object[] { "绘图", "图纸", "模型", "视图" });
            cboWin1.Location = new System.Drawing.Point(0, 0);
            cboWin1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            cboWin1.Name = "cboWin1";
            cboWin1.Size = new System.Drawing.Size(80, 28);
            cboWin1.TabIndex = 4;
            // 
            // cboWin2
            // 
            cboWin2.BackColor = System.Drawing.Color.White;
            cboWin2.Dock = System.Windows.Forms.DockStyle.Right;
            cboWin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboWin2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            cboWin2.FormattingEnabled = true;
            cboWin2.Items.AddRange(new object[] { "绘图", "图纸", "模型", "视图" });
            cboWin2.Location = new System.Drawing.Point(80, 0);
            cboWin2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            cboWin2.Name = "cboWin2";
            cboWin2.Size = new System.Drawing.Size(80, 28);
            cboWin2.TabIndex = 5;
            // 
            // btnTwinDisplay
            // 
            btnTwinDisplay.Dock = System.Windows.Forms.DockStyle.Right;
            btnTwinDisplay.Location = new System.Drawing.Point(160, 0);
            btnTwinDisplay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            btnTwinDisplay.Name = "btnTwinDisplay";
            btnTwinDisplay.Size = new System.Drawing.Size(80, 29);
            btnTwinDisplay.TabIndex = 6;
            btnTwinDisplay.Text = "窗口并列";
            btnTwinDisplay.UseVisualStyleBackColor = true;
            btnTwinDisplay.Click += btnTwinDisplay_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(btnSectionFrame);
            panel1.Controls.Add(btnSync3DViewport);
            panel1.Controls.Add(panel2);
            panel1.Controls.Add(pnlRight);
            panel1.Dock = System.Windows.Forms.DockStyle.Right;
            panel1.Location = new System.Drawing.Point(310, 0);
            panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(805, 29);
            panel1.TabIndex = 7;
            // 
            // btnSectionFrame
            // 
            btnSectionFrame.Dock = System.Windows.Forms.DockStyle.Right;
            btnSectionFrame.Location = new System.Drawing.Point(390, 0);
            btnSectionFrame.Name = "btnSectionFrame";
            btnSectionFrame.Size = new System.Drawing.Size(75, 29);
            btnSectionFrame.TabIndex = 3;
            btnSectionFrame.Text = "剖切框";
            btnSectionFrame.UseVisualStyleBackColor = true;
            btnSectionFrame.Click += btnSectionFrame_Click;
            // 
            // btnSync3DViewport
            // 
            btnSync3DViewport.Dock = System.Windows.Forms.DockStyle.Right;
            btnSync3DViewport.Location = new System.Drawing.Point(465, 0);
            btnSync3DViewport.Margin = new System.Windows.Forms.Padding(4);
            btnSync3DViewport.Name = "btnSync3DViewport";
            btnSync3DViewport.Size = new System.Drawing.Size(96, 29);
            btnSync3DViewport.TabIndex = 2;
            btnSync3DViewport.Text = "同步视口";
            btnSync3DViewport.UseVisualStyleBackColor = true;
            btnSync3DViewport.Click += btnSync3DViewport_Click;
            // 
            // panel2
            // 
            panel2.AutoSize = true;
            panel2.Controls.Add(cboWin1);
            panel2.Controls.Add(cboWin2);
            panel2.Controls.Add(btnTwinDisplay);
            panel2.Dock = System.Windows.Forms.DockStyle.Right;
            panel2.Location = new System.Drawing.Point(561, 0);
            panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            panel2.Name = "panel2";
            panel2.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
            panel2.Size = new System.Drawing.Size(244, 29);
            panel2.TabIndex = 1;
            // 
            // pnlRight
            // 
            pnlRight.AutoSize = true;
            pnlRight.Dock = System.Windows.Forms.DockStyle.Right;
            pnlRight.Location = new System.Drawing.Point(805, 0);
            pnlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            pnlRight.Name = "pnlRight";
            pnlRight.Size = new System.Drawing.Size(0, 29);
            pnlRight.TabIndex = 0;
            // 
            // EditorSelectBar
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(panel1);
            Controls.Add(btnView3D);
            Controls.Add(btnModel3D);
            Controls.Add(btnPaper);
            Controls.Add(btnDrawing);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "EditorSelectBar";
            Size = new System.Drawing.Size(1115, 29);
            contextMenu.ResumeLayout(false);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel2.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Button btnModel3D;
        private System.Windows.Forms.Button btnView3D;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem mui_NewWindow;
        private System.Windows.Forms.ToolStripMenuItem mui_OtherWindow;
        private System.Windows.Forms.ToolStripMenuItem mui_MainWindow;
        private System.Windows.Forms.Button btnPaper;
        internal System.Windows.Forms.Button btnDrawing;
        private System.Windows.Forms.ComboBox cboWin1;
        private System.Windows.Forms.ComboBox cboWin2;
        private System.Windows.Forms.Button btnTwinDisplay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlRight;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSync3DViewport;
        private System.Windows.Forms.Button btnSectionFrame;
    }
}
