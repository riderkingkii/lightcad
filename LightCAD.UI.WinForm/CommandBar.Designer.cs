﻿namespace LightCAD.UI
{
    partial class CommandBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = new System.ComponentModel.Container();

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            lblPrompt = new System.Windows.Forms.Label();
            pnlBottom = new System.Windows.Forms.Panel();
            btnCollapse = new System.Windows.Forms.Button();
            btnLog = new System.Windows.Forms.Button();
            pnlText = new System.Windows.Forms.Panel();
            txtInput = new System.Windows.Forms.TextBox();
            btnHelp = new System.Windows.Forms.Button();
            pnlPrompt = new System.Windows.Forms.FlowLayoutPanel();
            btnCmdHis = new System.Windows.Forms.Button();
            txtInfo = new System.Windows.Forms.RichTextBox();
            pnlInfo = new System.Windows.Forms.Panel();
            pnlBottom.SuspendLayout();
            pnlText.SuspendLayout();
            pnlPrompt.SuspendLayout();
            pnlInfo.SuspendLayout();
            SuspendLayout();
            // 
            // lblPrompt
            // 
            lblPrompt.AutoSize = true;
            lblPrompt.Location = new System.Drawing.Point(0, 7);
            lblPrompt.Margin = new System.Windows.Forms.Padding(0);
            lblPrompt.Name = "lblPrompt";
            lblPrompt.Size = new System.Drawing.Size(0, 17);
            lblPrompt.TabIndex = 1;
            lblPrompt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlBottom
            // 
            pnlBottom.BackColor = System.Drawing.Color.White;
            pnlBottom.Controls.Add(btnCollapse);
            pnlBottom.Controls.Add(btnLog);
            pnlBottom.Controls.Add(pnlText);
            pnlBottom.Controls.Add(btnHelp);
            pnlBottom.Controls.Add(pnlPrompt);
            pnlBottom.Controls.Add(btnCmdHis);
            pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            pnlBottom.Location = new System.Drawing.Point(0, 45);
            pnlBottom.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            pnlBottom.Name = "pnlBottom";
            pnlBottom.Size = new System.Drawing.Size(762, 26);
            pnlBottom.TabIndex = 2;
            // 
            // btnCollapse
            // 
            btnCollapse.Cursor = System.Windows.Forms.Cursors.Hand;
            btnCollapse.Dock = System.Windows.Forms.DockStyle.Right;
            btnCollapse.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(224, 224, 224);
            btnCollapse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnCollapse.ForeColor = System.Drawing.SystemColors.GrayText;
            btnCollapse.Location = new System.Drawing.Point(678, 0);
            btnCollapse.Margin = new System.Windows.Forms.Padding(0);
            btnCollapse.Name = "btnCollapse";
            btnCollapse.Size = new System.Drawing.Size(28, 26);
            btnCollapse.TabIndex = 6;
            btnCollapse.Text = "▼";
            btnCollapse.UseVisualStyleBackColor = true;
            btnCollapse.Click += btnCollapse_Click;
            // 
            // btnLog
            // 
            btnLog.Cursor = System.Windows.Forms.Cursors.Hand;
            btnLog.Dock = System.Windows.Forms.DockStyle.Right;
            btnLog.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(224, 224, 224);
            btnLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnLog.Image = Properties.Resources.CmdLog;
            btnLog.Location = new System.Drawing.Point(706, 0);
            btnLog.Margin = new System.Windows.Forms.Padding(0);
            btnLog.Name = "btnLog";
            btnLog.Size = new System.Drawing.Size(28, 26);
            btnLog.TabIndex = 5;
            btnLog.UseVisualStyleBackColor = true;
            btnLog.Click += btnLog_Click;
            // 
            // pnlText
            // 
            pnlText.Controls.Add(txtInput);
            pnlText.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlText.Location = new System.Drawing.Point(37, 0);
            pnlText.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            pnlText.Name = "pnlText";
            pnlText.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            pnlText.Size = new System.Drawing.Size(697, 26);
            pnlText.TabIndex = 4;
            // 
            // txtInput
            // 
            txtInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtInput.Dock = System.Windows.Forms.DockStyle.Fill;
            txtInput.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtInput.Location = new System.Drawing.Point(0, 4);
            txtInput.Margin = new System.Windows.Forms.Padding(0);
            txtInput.Name = "txtInput";
            txtInput.Size = new System.Drawing.Size(697, 16);
            txtInput.TabIndex = 0;
            txtInput.TextChanged += txtInput_TextChanged;
            // 
            // btnHelp
            // 
            btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            btnHelp.Dock = System.Windows.Forms.DockStyle.Right;
            btnHelp.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(224, 224, 224);
            btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnHelp.Image = Properties.Resources.CmdHelp;
            btnHelp.Location = new System.Drawing.Point(734, 0);
            btnHelp.Margin = new System.Windows.Forms.Padding(0);
            btnHelp.Name = "btnHelp";
            btnHelp.Size = new System.Drawing.Size(28, 26);
            btnHelp.TabIndex = 3;
            btnHelp.UseVisualStyleBackColor = true;
            btnHelp.Click += btnHelp_Click;
            // 
            // pnlPrompt
            // 
            pnlPrompt.AutoSize = true;
            pnlPrompt.Controls.Add(lblPrompt);
            pnlPrompt.Dock = System.Windows.Forms.DockStyle.Left;
            pnlPrompt.Location = new System.Drawing.Point(37, 0);
            pnlPrompt.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            pnlPrompt.Name = "pnlPrompt";
            pnlPrompt.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
            pnlPrompt.Size = new System.Drawing.Size(0, 26);
            pnlPrompt.TabIndex = 1;
            // 
            // btnCmdHis
            // 
            btnCmdHis.Cursor = System.Windows.Forms.Cursors.Hand;
            btnCmdHis.Dock = System.Windows.Forms.DockStyle.Left;
            btnCmdHis.Enabled = false;
            btnCmdHis.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(224, 224, 224);
            btnCmdHis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnCmdHis.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            btnCmdHis.Image = Properties.Resources.CmdHistory;
            btnCmdHis.Location = new System.Drawing.Point(0, 0);
            btnCmdHis.Margin = new System.Windows.Forms.Padding(0);
            btnCmdHis.Name = "btnCmdHis";
            btnCmdHis.Size = new System.Drawing.Size(37, 26);
            btnCmdHis.TabIndex = 2;
            btnCmdHis.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btnCmdHis.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnCmdHis.UseVisualStyleBackColor = true;
            btnCmdHis.Click += btnCmdHis_Click;
            // 
            // txtInfo
            // 
            txtInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            txtInfo.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txtInfo.Location = new System.Drawing.Point(3, 1);
            txtInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtInfo.Name = "txtInfo";
            txtInfo.ReadOnly = true;
            txtInfo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            txtInfo.Size = new System.Drawing.Size(759, 43);
            txtInfo.TabIndex = 3;
            txtInfo.Text = "";
            // 
            // pnlInfo
            // 
            pnlInfo.Controls.Add(txtInfo);
            pnlInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInfo.Location = new System.Drawing.Point(0, 0);
            pnlInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            pnlInfo.Name = "pnlInfo";
            pnlInfo.Padding = new System.Windows.Forms.Padding(3, 1, 0, 1);
            pnlInfo.Size = new System.Drawing.Size(762, 45);
            pnlInfo.TabIndex = 1;
            // 
            // CommandBar
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(pnlInfo);
            Controls.Add(pnlBottom);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "CommandBar";
            Size = new System.Drawing.Size(762, 71);
            pnlBottom.ResumeLayout(false);
            pnlBottom.PerformLayout();
            pnlText.ResumeLayout(false);
            pnlText.PerformLayout();
            pnlPrompt.ResumeLayout(false);
            pnlPrompt.PerformLayout();
            pnlInfo.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Label lblPrompt;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.Button btnCmdHis;
        private System.Windows.Forms.RichTextBox txtInfo;
        private System.Windows.Forms.Panel pnlText;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.FlowLayoutPanel pnlPrompt;
        private System.Windows.Forms.Button btnCollapse;
        private System.Windows.Forms.Panel pnlInfo;
    }
}
