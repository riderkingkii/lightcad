﻿namespace LightCAD.UI
{
    partial class LayerManageWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            listBox1 = new System.Windows.Forms.ListBox();
            dgvLayerList = new System.Windows.Forms.DataGridView();
            IsStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            IsOpen = new System.Windows.Forms.DataGridViewImageColumn();
            IsFrozen = new System.Windows.Forms.DataGridViewImageColumn();
            IsLock = new System.Windows.Forms.DataGridViewImageColumn();
            Color = new System.Windows.Forms.DataGridViewImageColumn();
            Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            LineType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            LineWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Transparency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            PrintStyle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            IsPrint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            NewViewportFrozen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Instructions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            btnAdd = new System.Windows.Forms.Button();
            btnDel = new System.Windows.Forms.Button();
            button1 = new System.Windows.Forms.Button();
            btnCurrent = new System.Windows.Forms.Button();
            panel1 = new System.Windows.Forms.Panel();
            panel2 = new System.Windows.Forms.Panel();
            panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)dgvLayerList).BeginInit();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            panel3.SuspendLayout();
            SuspendLayout();
            // 
            // listBox1
            // 
            listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            listBox1.FormattingEnabled = true;
            listBox1.ItemHeight = 17;
            listBox1.Location = new System.Drawing.Point(20, 0);
            listBox1.Name = "listBox1";
            listBox1.Size = new System.Drawing.Size(180, 378);
            listBox1.TabIndex = 0;
            // 
            // dgvLayerList
            // 
            dgvLayerList.AllowUserToResizeRows = false;
            dgvLayerList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dgvLayerList.BackgroundColor = System.Drawing.SystemColors.Control;
            dgvLayerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvLayerList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { IsStatus, name, IsOpen, IsFrozen, IsLock, Color, Column1, LineType, LineWidth, Transparency, PrintStyle, IsPrint, NewViewportFrozen, Instructions });
            dgvLayerList.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvLayerList.Location = new System.Drawing.Point(0, 0);
            dgvLayerList.Name = "dgvLayerList";
            dgvLayerList.RowHeadersVisible = false;
            dgvLayerList.RowHeadersWidth = 51;
            dgvLayerList.RowTemplate.Height = 25;
            dgvLayerList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgvLayerList.Size = new System.Drawing.Size(1233, 378);
            dgvLayerList.TabIndex = 1;
            dgvLayerList.CellClick += dataGridView1_CellClick;
            dgvLayerList.CellDoubleClick += dgvLayerList_CellDoubleClick;
            dgvLayerList.CellFormatting += dgvLayerList_CellFormatting;
            dgvLayerList.CellValueChanged += dgvLayerList_CellValueChanged;
            dgvLayerList.EditingControlShowing += dgvLayerList_EditingControlShowing;
            dgvLayerList.SelectionChanged += dgvLayerList_SelectionChanged;
            // 
            // IsStatus
            // 
            IsStatus.DataPropertyName = "IsStatus";
            IsStatus.HeaderText = "状态";
            IsStatus.MinimumWidth = 6;
            IsStatus.Name = "IsStatus";
            IsStatus.ReadOnly = true;
            // 
            // name
            // 
            name.DataPropertyName = "Name";
            name.HeaderText = "名称";
            name.MinimumWidth = 6;
            name.Name = "name";
            // 
            // IsOpen
            // 
            IsOpen.DataPropertyName = "IsOff";
            IsOpen.HeaderText = "开";
            IsOpen.MinimumWidth = 6;
            IsOpen.Name = "IsOpen";
            IsOpen.ReadOnly = true;
            IsOpen.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            IsOpen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IsFrozen
            // 
            IsFrozen.DataPropertyName = "IsFrozen";
            IsFrozen.HeaderText = "冻结";
            IsFrozen.MinimumWidth = 6;
            IsFrozen.Name = "IsFrozen";
            IsFrozen.ReadOnly = true;
            IsFrozen.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            IsFrozen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IsLock
            // 
            IsLock.DataPropertyName = "IsLocked";
            IsLock.HeaderText = "锁定";
            IsLock.MinimumWidth = 6;
            IsLock.Name = "IsLock";
            IsLock.ReadOnly = true;
            IsLock.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            IsLock.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Color
            // 
            Color.HeaderText = "颜色";
            Color.MinimumWidth = 6;
            Color.Name = "Color";
            Color.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            Color.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column1
            // 
            Column1.DataPropertyName = "Color";
            Column1.HeaderText = "颜色";
            Column1.MinimumWidth = 6;
            Column1.Name = "Column1";
            Column1.ReadOnly = true;
            Column1.Visible = false;
            // 
            // LineType
            // 
            LineType.DataPropertyName = "LineTypeName";
            LineType.HeaderText = "线型";
            LineType.MinimumWidth = 6;
            LineType.Name = "LineType";
            LineType.ReadOnly = true;
            LineType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // LineWidth
            // 
            LineWidth.DataPropertyName = "LineWeight";
            LineWidth.HeaderText = "线宽";
            LineWidth.MinimumWidth = 6;
            LineWidth.Name = "LineWidth";
            LineWidth.ReadOnly = true;
            // 
            // Transparency
            // 
            Transparency.DataPropertyName = "Transparency";
            Transparency.HeaderText = "透明度";
            Transparency.MinimumWidth = 6;
            Transparency.Name = "Transparency";
            Transparency.ReadOnly = true;
            // 
            // PrintStyle
            // 
            PrintStyle.HeaderText = "打印样式";
            PrintStyle.MinimumWidth = 6;
            PrintStyle.Name = "PrintStyle";
            // 
            // IsPrint
            // 
            IsPrint.DataPropertyName = "IsPlottable";
            IsPrint.HeaderText = "打印";
            IsPrint.MinimumWidth = 6;
            IsPrint.Name = "IsPrint";
            // 
            // NewViewportFrozen
            // 
            NewViewportFrozen.DataPropertyName = "ViewportVisibilityDefault";
            NewViewportFrozen.HeaderText = "新视口冻结";
            NewViewportFrozen.MinimumWidth = 6;
            NewViewportFrozen.Name = "NewViewportFrozen";
            // 
            // Instructions
            // 
            Instructions.HeaderText = "说明";
            Instructions.MinimumWidth = 6;
            Instructions.Name = "Instructions";
            // 
            // btnAdd
            // 
            btnAdd.Location = new System.Drawing.Point(200, 20);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new System.Drawing.Size(78, 25);
            btnAdd.TabIndex = 2;
            btnAdd.Text = "新建图层";
            btnAdd.UseVisualStyleBackColor = true;
            btnAdd.Click += btnAdd_Click;
            // 
            // btnDel
            // 
            btnDel.Location = new System.Drawing.Point(360, 20);
            btnDel.Name = "btnDel";
            btnDel.Size = new System.Drawing.Size(78, 25);
            btnDel.TabIndex = 3;
            btnDel.Text = "删除图层";
            btnDel.UseVisualStyleBackColor = true;
            btnDel.Click += btnDel_Click;
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(280, 20);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(78, 25);
            button1.TabIndex = 4;
            button1.Text = "在所有视口中都被冻结的新图层视口";
            button1.UseVisualStyleBackColor = true;
            // 
            // btnCurrent
            // 
            btnCurrent.Location = new System.Drawing.Point(440, 20);
            btnCurrent.Name = "btnCurrent";
            btnCurrent.Size = new System.Drawing.Size(78, 25);
            btnCurrent.TabIndex = 5;
            btnCurrent.Text = "置为当前";
            btnCurrent.UseVisualStyleBackColor = true;
            btnCurrent.Click += btnCurrent_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(btnAdd);
            panel1.Controls.Add(btnCurrent);
            panel1.Controls.Add(btnDel);
            panel1.Controls.Add(button1);
            panel1.Dock = System.Windows.Forms.DockStyle.Top;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(1453, 52);
            panel1.TabIndex = 6;
            // 
            // panel2
            // 
            panel2.Controls.Add(listBox1);
            panel2.Dock = System.Windows.Forms.DockStyle.Left;
            panel2.Location = new System.Drawing.Point(0, 52);
            panel2.Name = "panel2";
            panel2.Padding = new System.Windows.Forms.Padding(20, 0, 0, 20);
            panel2.Size = new System.Drawing.Size(200, 398);
            panel2.TabIndex = 7;
            // 
            // panel3
            // 
            panel3.Controls.Add(dgvLayerList);
            panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            panel3.Location = new System.Drawing.Point(200, 52);
            panel3.Name = "panel3";
            panel3.Padding = new System.Windows.Forms.Padding(0, 0, 20, 20);
            panel3.Size = new System.Drawing.Size(1253, 398);
            panel3.TabIndex = 8;
            // 
            // LayerManageWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            AutoScroll = true;
            AutoSize = true;
            ClientSize = new System.Drawing.Size(1453, 450);
            Controls.Add(panel3);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Name = "LayerManageWindow";
            Text = "图层管理器";
            Load += LayerManageWindow_Load;
            ((System.ComponentModel.ISupportInitialize)dgvLayerList).EndInit();
            panel1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel3.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGridView dgvLayerList;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewImageColumn IsOpen;
        private System.Windows.Forms.DataGridViewImageColumn IsFrozen;
        private System.Windows.Forms.DataGridViewImageColumn IsLock;
        private System.Windows.Forms.DataGridViewImageColumn Color;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineType;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn Transparency;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrintStyle;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewViewportFrozen;
        private System.Windows.Forms.DataGridViewTextBoxColumn Instructions;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCurrent;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}