﻿using LightCAD.Drawing.Actions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI.Layer
{
    public partial class TransparencyWindow : Form
    {
        private LayerManageAction layerManageAction;
        private string layerName;
        public TransparencyWindow(LayerManageAction layerManageAction, double transparency, string layer)
        {
            InitializeComponent();
            comboBox1.Text = transparency.ToString();
            this.layerManageAction = layerManageAction;
            this.layerName = layer;
            btnOk.DialogResult = DialogResult.OK;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            foreach (var item in layerManageAction.lcDocument.Layers)
            {
                if (item.Name == layerName)
                {
                    item.Transparency = Convert.ToDouble(comboBox1.Text);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
