using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Drawing.Actions;
using LightCAD.MathLib;
using LightCAD.Model;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Color = System.Drawing.Color;
using Point = System.Drawing.Point;

namespace LightCAD.UI
{
    public partial class SectionsForm : Form, ISectionWindow
    {
        public bool IsActive => true;

        public Type WinType => this.GetType();

        public string CurrentAction { get; set; }

        public string SectionName { get; set; }
        public string SectionId { get; set; }

        public SectionsForm()
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;
            this.listBox1.SelectionMode = SelectionMode.One;
            LoadItems();
        }
        public SectionsForm(params object[] args) : this()
        {

        }
        private void LoadItems()
        {
            var items = SectionManager.Sections.Select(n => new BoxItem { Name = n.Value.Name, Id = n.Key }).ToList();
            this.listBox1.ValueMember = "Id";
            this.listBox1.DisplayMember = "Name";
            this.listBox1.DataSource = items.OrderBy(n => n.Name).ToList();
            listBox1.MouseWheel += ListBox1_MouseWheel;
        }

        private void ListBox1_MouseWheel(object? sender, MouseEventArgs e)
        {
            listBox1_MouseMove(sender, new MouseEventArgs( MouseButtons.None,0,MousePosition.X-listBox1.Location.X, MousePosition.Y - listBox1.Location.Y, 0)) ;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("请选择轮廓");
                return;
            }
            SectionId = listBox1.SelectedValue.ToString();
            SectionName = listBox1.GetItemText(listBox1.SelectedItem);
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("请选择轮廓");
                return;
            }
            if (MessageBox.Show($"确认删除轮廓{listBox1.GetItemText(listBox1.SelectedItem)}", "确认删除", MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                return;
            }
            SectionId = listBox1.SelectedValue.ToString();
            var def = ComponentManager.GetCptDefs("拉伸体", "拉伸体").FirstOrDefault(n => n.TypeParameters.GetValue<string>("SectionId") == SectionId);
            if (def != null)
            {
                MessageBox.Show("当前轮廓已创建拉伸体定义,不允许删除！");
                return;
            }
            SectionManager.RemoveSection(SectionId);
            LoadItems();
        }
        private double GetScale(Box2 box, PictureBox pic)
        {
            double scaleW = pic.Width / box.Width;
            double scaleH = pic.Height / box.Height;
            return (scaleW > scaleH ? scaleH : scaleW) * 0.8;
        }
        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedValue != null)
            {
                SectionId = listBox1.SelectedValue.ToString();
                var section = SectionManager.Sections[SectionId];
                try
                {
                    LoadPic(pictureBox1, section);
                    var maxX = section.BoundingBox.Max.X;
                    var maxY = section.BoundingBox.Max.Y;
                    var minX = section.BoundingBox.Min.X;
                    var minY = section.BoundingBox.Min.Y;
                    this.lbBoxSize.Text = $"宽度：{Math.Round(maxX - minX, 2)}，高度:{Math.Round(maxY - minY, 2)}";
                }
                catch (System.Exception ex)
                {
                }

            }
        }


        private void listBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var index = listBox1.IndexFromPoint(e.Location);
                if (index > 0)
                {
                    listBox1.SelectedIndex = index;
                    this.contextMenuStrip1.Show(listBox1, e.Location);

                }
            }

        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnDelete_Click(null, null);
        }

        private void 重命名ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SectionId = listBox1.SelectedValue.ToString();
            var section = SectionManager.Sections[SectionId];
            SectionNameEdit nameEdit = new SectionNameEdit(section);
            if (nameEdit.ShowDialog() == DialogResult.OK)
            {
                SectionManager.ResetSectionJson();
                LoadItems();
            }
        }
        private void LoadPic(PictureBox pic, LcSection section)
        {
            pic.Refresh();
            Pen pen = new Pen(Color.White, 2);
            var picCenter = new Vector2(pic.Width / 2, pic.Height / 2);
            var boxSideLength = Math.Max(pic.Width, pic.Height);
            var boxShowSideLen = boxSideLength / 5 * 4;
            try
            {
                var image = new Bitmap(pic.Width, Height);
                Graphics g = Graphics.FromImage(image);
                var maxX = section.BoundingBox.Max.X;
                var maxY = section.BoundingBox.Max.Y;
                var minX = section.BoundingBox.Min.X;
                var minY = section.BoundingBox.Min.Y;
                var center = section.BoundingBox.Center;
                var scale = GetScale(section.BoundingBox, pic);
                foreach (var curve in section.Segments)
                {
                    if (curve is Line2d line)
                    {
                        var startX = line.Start.X;
                        var startY = line.Start.Y;
                        var endX = line.End.X;
                        var endY = line.End.Y;
                        g.DrawLine(pen, (float)((startX - center.X) * scale + picCenter.X), (float)((center.Y - startY) * scale + picCenter.Y), (float)((endX - center.X) * scale + picCenter.X), (float)((center.Y - endY) * scale + picCenter.Y));

                    }
                    else
                    {
                        var lines = curve.GetPoints(32);
                        for (var i = 0; i < lines.Length - 1; i++)
                        {
                            var startX = lines[i].X;
                            var startY = lines[i].Y;
                            var endX = lines[i + 1].X;
                            var endY = lines[i + 1].Y;
                            g.DrawLine(pen, (float)((startX - center.X) * scale + picCenter.X), (float)((center.Y - startY) * scale + picCenter.Y), (float)((endX - center.X) * scale + picCenter.X), (float)((center.Y - endY) * scale + picCenter.Y));
                        }
                    }
                }
                g.Dispose();
                pen.Dispose();
                pic.BackColor = Color.Black;
                pic.Image = image;
            }
            catch (System.Exception ex)
            {
            }
        }
        private void listBox1_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                var index = listBox1.IndexFromPoint(e.Location);
                if (index > -1 && index != listBox1.SelectedIndex)
                {
                    var section = SectionManager.Sections[(listBox1.Items[index] as BoxItem).Id];
                    picTip.Location = new Point(e.Location.X + listBox1.Location.X + 5, e.Location.Y + listBox1.Location.Y + 5);
                    if (picTip.Tag?.ToString() == index.ToString())
                        return;
                    LoadPic(picTip, section);
                    picTip.Visible = true;
                    picTip.Tag = index;
                }
                else
                {
                    picTip.Tag = null;
                    picTip.Image = null;
                    picTip.Visible = false;
                }
            }
            catch
            {

            }
        }

        private void listBox1_MouseLeave(object sender, EventArgs e)
        {
            picTip.Visible = false;
        }
    }
}