﻿using LightCAD.Runtime;
using LightCAD.UI.Axis;
using LightCAD.UI.ContextMenu;
using LightCAD.UI.LineType;
using LightCAD.UI.Properties;
using LightCAD.UI.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightCAD.UI
{
    public class UISystem : IUISystem
    {
        public static Dictionary<string, Type> WindowMap = new Dictionary<string, Type>
        {
            {"MainUIWindow",typeof(MainUIWindow) },
            {"SectionsForm",typeof(SectionsForm) },
            {"ExtrudeDefSelect",typeof(ExtrudeDefSelect) },
            {"RoofRefSelect",typeof(RoofRefSelect) },
            {"LoftDefSelect",typeof(LoftDefSelect) },
            {"BlockDefWindow",typeof(BlockDefWindow) },
            {"DrawingFrameWindow",typeof(DrawingFrameWindow) },
            {"BuildingSettingWindow",typeof(BuildingSettingWindow) },
            {"LayerManageWindow",typeof(LayerManageWindow) },
            {"LineTypeManageWindow",typeof(LineTypeManageWindow) },
            {"AxisManageFrom",typeof(AxisManageFrom) },
            {"AxisNumberSettingFrom",typeof(AxisNumberSettingFrom) },
            {"QuickSelectWindow",typeof(QuickSelectWindow) },
            {"FindWindow",typeof(FindWindow) },
            {"TableWindow",typeof(TableWindow) },

        };
        public string Name => "WinForm";

        public  async Task<string[]?> OpenFileDialog(bool allowMultiple)
        {
            TaskCompletionSource<string[]?> tc = new TaskCompletionSource<string[]?>();
            // 新线程
            Thread t = new Thread(() =>
            {
                var dlg = new OpenFileDialog();
                dlg.Multiselect = allowMultiple;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    if (allowMultiple)
                        tc.SetResult(dlg.FileNames); 
                    else
                        tc.SetResult(new string[] { dlg.FileName });
                }
                else
                {
                    tc.SetResult(null);
                }                // 这句话是必须的，设置Task的运算结果
                // 但由于此处不需要结果，故用null
                
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            // 新线程启动后，将Task实例返回
            // 以便支持 await 操作符
            return await tc.Task;
        }

        public async Task<string?> SaveFileDialog(string initialFileName = null, string initialFilter = null, string initialDirectory=null)
        {
          
            TaskCompletionSource<string?> tc = new TaskCompletionSource<string?>();
            // 新线程
            Thread t = new Thread(() =>
            {
                var dlg = new SaveFileDialog();
                dlg.InitialDirectory = initialDirectory;
                dlg.FileName = initialFileName==null?"图纸.ldwg":initialFileName;
                dlg.Filter = initialFilter == null ? "Drawing files (*.ldwg)|*.ldwg" : initialFilter;//过滤条件rawing files (*.ldwg)|*.ldwg|(*.dwg)|*.dwg|(*.pdf)|*.pdf
                dlg.FilterIndex = 0;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    tc.SetResult( dlg.FileName);
                }
                else
                {
                    tc.SetResult(null);
                }

            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            // 新线程启动后，将Task实例返回
            // 以便支持 await 操作符
            return await tc.Task;

        }

        public object NewClipboardDataObject()
        {
            return  new DataObject();
        }
        public void SetClipboardData(object dataObject, string format,object value)
        {
            (dataObject as DataObject).SetData(format, value);
        }
        public void SetToClipboard(object dataObject)
        {
            Clipboard.SetDataObject(dataObject,true,3,10);
        }

        public async Task<object> GetClipboardData(string format)
        {
            //为了统一接口，此处模拟Avalonia的异步处理
            object data = null;
            var dataObject = Clipboard.GetDataObject();
            if (dataObject != null)
            {
                data = dataObject.GetData(format);
            }
            return await Task<object>.Run(() => {
                return data;
            });
        }


        public IWindow CreateWindow(string winName, params object[] args)
        {
            var type = WindowMap[winName];
            var winObj = (IWindow)Activator.CreateInstance(type, args);
            return winObj;
        }
        public void ShowWindow( IWindow window,IWindow owner=null)
        {
            (window as Form).Show((Form)owner);
        }

        public LcDialogResult ShowDialog(IWindow window, IWindow owner = null)
        {
            var result = (window as Form).ShowDialog((Form)owner);
            return (LcDialogResult)(result);
        }

        public void ShowMessageBox(string message)
        {
            MessageBox.Show(message);
        }

        public void ShowException(Exception ex)
        {
            MessageBox.Show(ex.Message);
        }
    }
}
