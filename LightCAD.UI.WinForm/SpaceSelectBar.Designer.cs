﻿namespace LightCAD.UI
{
    partial class SpaceSelectBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            btnModel = new System.Windows.Forms.Button();
            btnPaper1 = new System.Windows.Forms.Button();
            btnPaper2 = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // btnModel
            // 
            btnModel.Dock = System.Windows.Forms.DockStyle.Left;
            btnModel.Location = new System.Drawing.Point(0, 0);
            btnModel.Name = "btnModel";
            btnModel.Size = new System.Drawing.Size(76, 25);
            btnModel.TabIndex = 0;
            btnModel.Text = "模型";
            btnModel.UseVisualStyleBackColor = true;
            btnModel.Click += btnModel_Click;
            // 
            // btnPaper1
            // 
            btnPaper1.BackColor = System.Drawing.Color.White;
            btnPaper1.Dock = System.Windows.Forms.DockStyle.Left;
            btnPaper1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            btnPaper1.Location = new System.Drawing.Point(76, 0);
            btnPaper1.Name = "btnPaper1";
            btnPaper1.Size = new System.Drawing.Size(76, 25);
            btnPaper1.TabIndex = 1;
            btnPaper1.Text = "图纸1";
            btnPaper1.UseVisualStyleBackColor = false;
            btnPaper1.Click += btnPaper1_Click;
            // 
            // btnPaper2
            // 
            btnPaper2.BackColor = System.Drawing.Color.White;
            btnPaper2.Dock = System.Windows.Forms.DockStyle.Left;
            btnPaper2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            btnPaper2.Location = new System.Drawing.Point(152, 0);
            btnPaper2.Name = "btnPaper2";
            btnPaper2.Size = new System.Drawing.Size(76, 25);
            btnPaper2.TabIndex = 2;
            btnPaper2.Text = "图纸2";
            btnPaper2.UseVisualStyleBackColor = false;
            btnPaper2.Click += btnPaper2_Click;
            // 
            // SpaceSelectBar
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(btnPaper2);
            Controls.Add(btnPaper1);
            Controls.Add(btnModel);
            Name = "SpaceSelectBar";
            Size = new System.Drawing.Size(1092, 25);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btnModel;
        private System.Windows.Forms.Button btnPaper1;
        private System.Windows.Forms.Button btnPaper2;
    }
}
