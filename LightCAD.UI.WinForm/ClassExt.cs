﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FMouseButton = System.Windows.Forms.MouseButtons;

namespace LightCAD.UI
{
    public static class ClassExt
    {
        public static Bounds ToRuntime(this Rectangle rect)
        {
            return new Bounds(rect.Left, rect.Top, rect.Width, rect.Height);
        }
        public static MouseEventRuntime ToRuntime(this MouseEventArgs e)
        {
            var me = new MouseEventRuntime();
            me.Clicks = e.Clicks;
            me.KeyModifiers = GetKeyModifiers();
            me.X = e.X;
            me.Y = e.Y;
            me.DeltaY = e.Delta;
            bool leftDown = (e.Button & FMouseButton.Left) == FMouseButton.Left;
            bool middleDown = (e.Button & FMouseButton.Middle) == FMouseButton.Middle;
            bool rightDown = (e.Button & FMouseButton.Right) == FMouseButton.Right;

            var mb = LcMouseButtons.None;
            if(leftDown) mb |= LcMouseButtons.Left; 
            if (middleDown) mb |= LcMouseButtons.Middle;
            if (rightDown) mb |= LcMouseButtons.Right;
            me.Button = mb;
            return me;
        }
        public static LcKeyModifiers GetKeyModifiers()
        {
            bool shiftDown = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;
            bool ctrlDown = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            bool altDown = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

            var m = LcKeyModifiers.None;
            if (shiftDown) m |= LcKeyModifiers.Shift;
            if (ctrlDown) m |= LcKeyModifiers.Control;
            if (altDown) m |= LcKeyModifiers.Alt;
            return m;
        }
        public static KeyEventRuntime ToRuntime(this KeyEventArgs e)
        {
            var ke = new KeyEventRuntime();
            ke.KeyCode = (byte)e.KeyCode;
            ke.KeyModifiers = GetKeyModifiers();
            return ke;

        }
    }
}
