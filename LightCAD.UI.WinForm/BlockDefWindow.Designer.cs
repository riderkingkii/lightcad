﻿namespace LightCAD.UI
{
    partial class BlockDefWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new System.Windows.Forms.Label();
            groupBox1 = new System.Windows.Forms.GroupBox();
            lblSelectPoint = new System.Windows.Forms.Label();
            txtX = new System.Windows.Forms.TextBox();
            txtY = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            btnSelectPoint = new System.Windows.Forms.Button();
            cboName = new System.Windows.Forms.ComboBox();
            groupBox2 = new System.Windows.Forms.GroupBox();
            lblSelectInfo = new System.Windows.Forms.Label();
            rdoDelete = new System.Windows.Forms.RadioButton();
            rdoConvBlock = new System.Windows.Forms.RadioButton();
            lblSelectObjects = new System.Windows.Forms.Label();
            btnSelectObjects = new System.Windows.Forms.Button();
            txtDescr = new System.Windows.Forms.TextBox();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            cboUnit = new System.Windows.Forms.ComboBox();
            btnOK = new System.Windows.Forms.Button();
            btnCancel = new System.Windows.Forms.Button();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(18, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(65, 20);
            label1.TabIndex = 1;
            label1.Text = "名称(N):";
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(lblSelectPoint);
            groupBox1.Controls.Add(txtX);
            groupBox1.Controls.Add(txtY);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(btnSelectPoint);
            groupBox1.Location = new System.Drawing.Point(18, 73);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(250, 173);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            groupBox1.Text = "基点";
            // 
            // lblSelectPoint
            // 
            lblSelectPoint.AutoSize = true;
            lblSelectPoint.Location = new System.Drawing.Point(74, 52);
            lblSelectPoint.Name = "lblSelectPoint";
            lblSelectPoint.Size = new System.Drawing.Size(74, 20);
            lblSelectPoint.TabIndex = 6;
            lblSelectPoint.Text = "拾取点(K)";
            lblSelectPoint.Click += lblSelectPoint_Click;
            // 
            // txtX
            // 
            txtX.Enabled = false;
            txtX.Location = new System.Drawing.Point(51, 100);
            txtX.Name = "txtX";
            txtX.Size = new System.Drawing.Size(176, 27);
            txtX.TabIndex = 4;
            txtX.Text = "0";
            // 
            // txtY
            // 
            txtY.Enabled = false;
            txtY.Location = new System.Drawing.Point(51, 136);
            txtY.Name = "txtY";
            txtY.Size = new System.Drawing.Size(176, 27);
            txtY.TabIndex = 3;
            txtY.Text = "0";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(23, 143);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(22, 20);
            label3.TabIndex = 2;
            label3.Text = "Y:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(22, 103);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(23, 20);
            label2.TabIndex = 1;
            label2.Text = "X:";
            // 
            // btnSelectPoint
            // 
            btnSelectPoint.Location = new System.Drawing.Point(21, 40);
            btnSelectPoint.Name = "btnSelectPoint";
            btnSelectPoint.Size = new System.Drawing.Size(47, 48);
            btnSelectPoint.TabIndex = 14;
            btnSelectPoint.UseVisualStyleBackColor = true;
            btnSelectPoint.Click += btnSelectPoint_Click;
            // 
            // cboName
            // 
            cboName.FormattingEnabled = true;
            cboName.Location = new System.Drawing.Point(18, 39);
            cboName.Name = "cboName";
            cboName.Size = new System.Drawing.Size(358, 28);
            cboName.TabIndex = 3;
            cboName.Text = "TEST";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(lblSelectInfo);
            groupBox2.Controls.Add(rdoDelete);
            groupBox2.Controls.Add(rdoConvBlock);
            groupBox2.Controls.Add(lblSelectObjects);
            groupBox2.Controls.Add(btnSelectObjects);
            groupBox2.Location = new System.Drawing.Point(274, 73);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new System.Drawing.Size(250, 173);
            groupBox2.TabIndex = 3;
            groupBox2.TabStop = false;
            groupBox2.Text = "对象";
            // 
            // lblSelectInfo
            // 
            lblSelectInfo.AutoSize = true;
            lblSelectInfo.Location = new System.Drawing.Point(16, 101);
            lblSelectInfo.Name = "lblSelectInfo";
            lblSelectInfo.Size = new System.Drawing.Size(142, 20);
            lblSelectInfo.TabIndex = 12;
            lblSelectInfo.Text = "已选择【0】个元素.";
            // 
            // rdoDelete
            // 
            rdoDelete.AutoSize = true;
            rdoDelete.Location = new System.Drawing.Point(125, 134);
            rdoDelete.Name = "rdoDelete";
            rdoDelete.Size = new System.Drawing.Size(60, 24);
            rdoDelete.TabIndex = 11;
            rdoDelete.Text = "删除";
            rdoDelete.UseVisualStyleBackColor = true;
            // 
            // rdoConvBlock
            // 
            rdoConvBlock.AutoSize = true;
            rdoConvBlock.Checked = true;
            rdoConvBlock.Location = new System.Drawing.Point(17, 134);
            rdoConvBlock.Name = "rdoConvBlock";
            rdoConvBlock.Size = new System.Drawing.Size(90, 24);
            rdoConvBlock.TabIndex = 10;
            rdoConvBlock.TabStop = true;
            rdoConvBlock.Text = "转换为块";
            rdoConvBlock.UseVisualStyleBackColor = true;
            // 
            // lblSelectObjects
            // 
            lblSelectObjects.AutoSize = true;
            lblSelectObjects.Location = new System.Drawing.Point(70, 50);
            lblSelectObjects.Name = "lblSelectObjects";
            lblSelectObjects.Size = new System.Drawing.Size(88, 20);
            lblSelectObjects.TabIndex = 8;
            lblSelectObjects.Text = "选择对象(T)";
            lblSelectObjects.Click += lblSelectObjects_Click;
            // 
            // btnSelectObjects
            // 
            btnSelectObjects.Location = new System.Drawing.Point(17, 38);
            btnSelectObjects.Name = "btnSelectObjects";
            btnSelectObjects.Size = new System.Drawing.Size(47, 48);
            btnSelectObjects.TabIndex = 13;
            btnSelectObjects.UseVisualStyleBackColor = true;
            btnSelectObjects.Click += btnSelectObjects_Click;
            // 
            // txtDescr
            // 
            txtDescr.Location = new System.Drawing.Point(18, 275);
            txtDescr.Multiline = true;
            txtDescr.Name = "txtDescr";
            txtDescr.Size = new System.Drawing.Size(506, 61);
            txtDescr.TabIndex = 4;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(18, 252);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(43, 20);
            label7.TabIndex = 5;
            label7.Text = "说明:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(394, 9);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(58, 20);
            label8.TabIndex = 6;
            label8.Text = "块单位:";
            // 
            // cboUnit
            // 
            cboUnit.FormattingEnabled = true;
            cboUnit.Location = new System.Drawing.Point(394, 39);
            cboUnit.Name = "cboUnit";
            cboUnit.Size = new System.Drawing.Size(130, 28);
            cboUnit.TabIndex = 7;
            cboUnit.Text = "毫米";
            // 
            // btnOK
            // 
            btnOK.Location = new System.Drawing.Point(313, 356);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(100, 28);
            btnOK.TabIndex = 8;
            btnOK.Text = "确定";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += btnOK_Click;
            // 
            // btnCancel
            // 
            btnCancel.Location = new System.Drawing.Point(424, 356);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(100, 28);
            btnCancel.TabIndex = 9;
            btnCancel.Text = "取消";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // BlockDefWindow
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(542, 400);
            Controls.Add(btnCancel);
            Controls.Add(btnOK);
            Controls.Add(cboUnit);
            Controls.Add(label8);
            Controls.Add(label7);
            Controls.Add(txtDescr);
            Controls.Add(groupBox2);
            Controls.Add(cboName);
            Controls.Add(groupBox1);
            Controls.Add(label1);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "BlockDefWindow";
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "块定义";
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboName;
        private System.Windows.Forms.Label lblSelectPoint;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblSelectObjects;
        private System.Windows.Forms.RadioButton rdoDelete;
        private System.Windows.Forms.RadioButton rdoConvBlock;
        private System.Windows.Forms.Label lblSelectInfo;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboUnit;
        private System.Windows.Forms.Button btnSelectPoint;
        private System.Windows.Forms.Button btnSelectObjects;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}