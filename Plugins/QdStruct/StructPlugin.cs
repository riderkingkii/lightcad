﻿
using LightCAD.Drawing.Actions;

namespace QdStruct
{

    public class StructPlugin //: ILcPlugin
    {
        public void InitUI()
        {
        }

        public static void Loaded()
        {
            LcDocument.RegistElementTypes(StructElementType.All);
            LcRuntime.RegistAssemblies.Add("QdStruct");

            LcDocument.ElementActions.Add(StructElementType.ShearWall, new ShearWallAction());
            LcDocument.Element3dActions.Add(StructElementType.ShearWall, new ShearWall3dAction());

            LcDocument.ElementActions.Add(StructElementType.Beam, new BeamAction());
            LcDocument.Element3dActions.Add(StructElementType.Beam, new Beam3dAction());

            LcDocument.ElementActions.Add(StructElementType.Column, new ColumnAction());
            LcDocument.Element3dActions.Add(StructElementType.Column, new Column3dAction());
        }
    }
}
