﻿using LightCAD.Drawing.Actions;

namespace QdStruct
{
    [CommandClass]
    public class StructCmds
    {
        [CommandMethod(Name = "ShearWALL", ShortCuts = "SW")]
        public CommandResult DrawShearWALL(IDocumentEditor docEditor, string[] args)
        {
            var wallAction = new ShearWallAction(docEditor);
            wallAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "BEAM", ShortCuts = "BM")]
        public CommandResult DrawBEAM(IDocumentEditor docEditor, string[] args)
        {
            var beamAction = new BeamAction(docEditor);
            beamAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Column", ShortCuts = "COL")]
        public CommandResult DrawColumn(IDocumentEditor docEditor, string[] args)
        {
            var columnAction = new ColumnAction(docEditor);
            columnAction.ExecCreate(args);
            return CommandResult.Succ();
        }
    }
}
