﻿using netDxf.Objects;
using System.Diagnostics.CodeAnalysis;
using Constants = LightCAD.Runtime.Constants;

namespace QdStruct
{
    public class ShearWallAction : DirectComponentAction
    {

        public ShearWallAction():base() { }
        public ShearWallAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：ShearWall");
            //ElementActions.w = new LineAction();
            //LcDocument.ElementActions.Add(BuiltinElementType.Line, ElementActions.Line);
        }


        private static readonly LcCreateMethod[] CreateMethods;
        private List<Vector2> points;

        private Vector2 firstPoint { get; set; }

        private Vector2 secondPoint { get; set; }

        private PointInputer inputer { get; set; }
        static ShearWallAction()
        {
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "CreateShearWall",
                Description = "创建结构墙",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep { Name = "Step0", Options = "指定第一个点:" },
                    new LcCreateStep { Name = "Step1", Options = "直墙下一点或 [弧墙(A)/矩形画墙(R)/闭合(C)/回退(U)]<另一段>:" },
                    new LcCreateStep { Name = "Step2", Options = "指定下一个点或[放弃(U)]:" },
                }
            };
        }



        public async void ExecCreate(string[] args = null)
        {
            //DocumentManager.CurrentRecorder.BeginAction("ShearWall");

            this.points = new List<Vector2>();
            this.StartCreating();
            this.inputer = new PointInputer(this.docEditor);
            var curMethod = CreateMethods[0];

            Step0:
            var step0 = curMethod.Steps[0];
            var result0 = await inputer.Execute(step0.Options);
            //var result0 = await inputer.Execute(step0.Options);

            if (inputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }

            // zcb: 增加Res0为空的判定
            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }

            if (result0.ValueX == null)
            {
                if (result0.Option != null)
                {
                    goto Step0;
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step0;
                }
            }

            this.firstPoint = (Vector2)result0.ValueX;

            Step1:
            var step1 = curMethod.Steps[1];
            if (this.points.Count >= 2)
            {
                step1 = curMethod.Steps[2];
            }

            var result1 = await inputer.Execute(step1.Options);
            if (inputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }

     

            if (result1.Option != null)
            {
                if (result1.Option == "U") { }
                else if (result1.Option == "C")
                {
                    goto End;
                }
                else if (result1.Option == " ")
                {
                    goto End;
                }
                else if (result1.ValueX != null)
                {
                    Vector2 pointB = (Vector2)result1.ValueX;
                    Vector2 direction = (pointB - firstPoint).Normalize();
                    result1.ValueX = firstPoint + direction * double.Parse(result1.Option);
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step1;
                }
            }

            this.secondPoint = (Vector2)result1.ValueX;


            CreateShearWall(firstPoint, secondPoint,200,ComponentManager.GetCptDef<QdShearWallDef>(ShearWallAttribute.BuiltinUuid));
            firstPoint = secondPoint;

            End:
            this.inputer = null;
            this.EndCreating();

            //if (this.points.Count > 0)
            //{
            //    DocumentManager.CurrentRecorder.EndAction();
            //}
            //else
            //{
            //    DocumentManager.CurrentRecorder.CancleAction();
            //}
        }
        public override void Draw(SKCanvas canvas, LcElement element, Vector2 offset)
        {
            Matrix3 matrix = Matrix3.GetTranslate(offset);
            var wall = element as QdShearWall;
            var mstart = matrix.MultiplyPoint(wall.BaseLine.Start);
            var mend = matrix.MultiplyPoint(wall.BaseLine.End);
            var start = this.vportRt.ConvertWcsToScr(mstart).ToSKPoint();
            var end = this.vportRt.ConvertWcsToScr(mend).ToSKPoint();

            using (var elePen = new SKPaint { Color = SKColors.Gray, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 10, 10, 10, 10 }, 0) })
            {
                canvas.DrawLine(start, end, elePen);
            }
            var curves = wall.Curves[0].Curve2ds.ToList();
            foreach (var item in curves)
            {
                Line2d line2D = item as Line2d;
                if (line2D == null)
                {
                    continue;
                }
                mstart = matrix.MultiplyPoint(line2D.Start);
                mend = matrix.MultiplyPoint(line2D.End);
                start = this.vportRt.ConvertWcsToScr(mstart).ToSKPoint();
                end = this.vportRt.ConvertWcsToScr(mend).ToSKPoint();
                var pen = this.GetDrawPen(element);
                if (pen == Constants.defaultPen)
                {
                    using (var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
                    {
                        canvas.DrawLine(start, end, elePen);
                    }
                }
                else
                {
                    canvas.DrawLine(start, end, pen);
                }
            }


        }
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var group = element as QdShearWall;
            var grips = new List<ControlGrip>();
            var gripCenter = new ControlGrip
            {
                Element = group,
                Name = "Center",
                Position = group.BoundingBox.Center,

            };
            grips.Add(gripCenter);

            var gripStart = new ControlGrip
            {
                Element = group,
                Name = "Start",
                Position = group.BaseLine.Start
            };
            grips.Add(gripStart);
            var gripEnd = new ControlGrip
            {
                Element = group,
                Name = "End",
                Position = group.BaseLine.End
            };
            grips.Add(gripEnd);



            return grips.ToArray();
        }
        private string _gripName;
        private Vector2 _position;
        private Vector2 _endDrag;
        private QdShearWall qdShearWall;
        private List<QdShearWall> fShearWalls;
        public override void DrawDragGrip(SKCanvas canvas)
        {

            if (qdShearWall == null) return;
            if (_gripName == "Center")
            {
                Matrix3 matrix3d = Matrix3.GetMove(qdShearWall.BoundingBox.Center, _position);
                Line2d line = new Line2d();
                line.Start = matrix3d.MultiplyPoint(qdShearWall.BaseLine.Start);
                line.End = matrix3d.MultiplyPoint(qdShearWall.BaseLine.End);
                Draw(canvas, line);
            }
            else
            {
                Line2d line = new Line2d();
                fShearWalls = new List<QdShearWall>();
                line = new Line2d();
                Vector2 po;
                if (_gripName == "Start")
                {
                    line.Start = _position;
                    line.End = qdShearWall.BaseLine.End;
                    po = qdShearWall.BaseLine.Start;
                }
                else
                {
                    line.Start = qdShearWall.BaseLine.Start;
                    line.End = _position;
                    po = qdShearWall.BaseLine.End;
                }
                Draw(canvas, line);
                foreach (ControlGrip item in this.vportRt.ElementGrips)
                {
                    if (item.Position.Similarity(po))
                    {
                        QdShearWall w = item.Element as QdShearWall;
                        if (w != qdShearWall)
                        {

                            if (item.Name == "Start")
                            {
                                line = new Line2d();
                                line.Start = _position;
                                line.End = w.BaseLine.End;
                                Draw(canvas, line);
                            }
                            else
                                if (item.Name == "End")
                            {
                                line = new Line2d();
                                line.Start = w.BaseLine.Start;
                                line.End = _position;
                                Draw(canvas, line);
                            }
                        }
                    }
                }

            }



        }
    
        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            qdShearWall = element as QdShearWall;

            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {

                if (gripName == "Center")
                {
                    Matrix3 matrix3d = Matrix3.GetMove(qdShearWall.BoundingBox.Center, _position);
                    Line2d line = new Line2d();
                    line.Start = matrix3d.MultiplyPoint(qdShearWall.BaseLine.Start);
                    line.End = matrix3d.MultiplyPoint(qdShearWall.BaseLine.End);
                    qdShearWall.BaseCurve = line;
                    qdShearWall.CreateShapeCurves();
                    DisposeShearWalls(qdShearWall, false);


                    var args = new PropertyChangedEventArgs
                    {
                        Object = qdShearWall,
                        PropertyName = "",
                        OldValue = null,
                        NewValue = null,
                    };
                    qdShearWall.ResetCache();
                    // this.PropertyChangedAfter?.Invoke(this, args);
                    qdShearWall.OnPropertyChangedAfter("", null, null);
                    //RestShearWalls(qdShearWall);
                }
                else
                {
                    List<QdShearWall> changewalls = new List<QdShearWall>();
                    Line2d line = new Line2d();
                    fShearWalls = new List<QdShearWall>();
                    line = new Line2d();
                    Vector2 po;
                    if (_gripName == "Start")
                    {
                        line.Start = _position;
                        line.End = qdShearWall.BaseLine.End;
                        po = qdShearWall.BaseLine.Start;
                        qdShearWall.BaseCurve = line;
                    }
                    else
                    {
                        line.Start = qdShearWall.BaseLine.Start;
                        line.End = _position;
                        po = qdShearWall.BaseLine.End;
                        qdShearWall.BaseCurve = line;
                    }
                    changewalls.Add(qdShearWall);
                    foreach (ControlGrip item in this.vportRt.ElementGrips)
                    {
                        if (item.Position.Similarity(po))
                        {
                            QdShearWall w = item.Element as QdShearWall;
                            if (w != qdShearWall)
                            {
                                if (item.Name == "Start")
                                {
                                    line = new Line2d();
                                    line.Start = _position;
                                    line.End = w.BaseLine.End;
                                    w.BaseCurve = line;
                                    changewalls.Add(w);
                                }
                                else
                                    if (item.Name == "End")
                                {
                                    line = new Line2d();
                                    line.Start = w.BaseLine.Start;
                                    line.End = _position;
                                    w.BaseCurve = line;
                                    changewalls.Add(w);
                                }
                            }
                        }
                    }
                    foreach (QdShearWall item in changewalls)
                    {
                        item.CreateShapeCurves();
                    }
                    foreach (QdShearWall item in changewalls)
                    {
                        DisposeShearWalls(item, false);
                        var args = new PropertyChangedEventArgs
                        {
                            Object = item,
                            PropertyName = "",
                            OldValue = null,
                            NewValue = null,
                        };
                        item.ResetCache();
                        /*this.docRt.Document.OnPropertyChangedAfter(args)*/;
                        item.OnPropertyChangedAfter("", null, null);
                    }
                }
            }
        }
        public void Draw(SKCanvas canvas, Line2d line)
        {


            var sk_p = this.vportRt.ConvertWcsToScr(line.End).ToSKPoint();
            var start = this.vportRt.ConvertWcsToScr(line.Start).ToSKPoint();
            using (var elePen = new SKPaint { Color = SKColors.Yellow, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 10, 10, 30, 10 }, 0) })
            {
                canvas.DrawLine(start, sk_p, elePen);
            }

            foreach (var item in QdShearWall.GetShapeCurves(line, 5))
            {
                Line2d line2D = item as Line2d;
                //mstart = matrix.MultiplyPoint(line2D.Start);
                //mend = matrix.MultiplyPoint(line2D.End);
                start = this.vportRt.ConvertWcsToScr(line2D.Start).ToSKPoint();
                var end = this.vportRt.ConvertWcsToScr(line2D.End).ToSKPoint();
                using (var elePen = new SKPaint { Color = SKColors.Gray, IsStroke = true })
                {
                    canvas.DrawLine(start, end, elePen);
                }

            }
        }
        public override void DrawTemp(SKCanvas canvas)
        {
            if (this.firstPoint == null)
            {
                return;
            }

            var mp = this.vportRt.PointerMovedPosition.ToVector2d();
            var wcs_mp = this.vportRt.ConvertScrToWcs(mp);
            var sk_p = this.vportRt.ConvertWcsToScr(wcs_mp).ToSKPoint();
            var start = this.vportRt.ConvertWcsToScr(this.firstPoint).ToSKPoint();
            using (var elePen = new SKPaint { Color = SKColors.Yellow, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 10, 10, 30, 10 }, 0) })
            {
                canvas.DrawLine(start, sk_p, elePen);
            }
            Line2d baseline = new Line2d();
            baseline.Start = this.firstPoint;
            baseline.End = wcs_mp;
            foreach (var item in QdShearWall.GetShapeCurves(baseline, 200))
            {
                Line2d line2D = item as Line2d;
                //mstart = matrix.MultiplyPoint(line2D.Start);
                //mend = matrix.MultiplyPoint(line2D.End);
                start = this.vportRt.ConvertWcsToScr(line2D.Start).ToSKPoint();
                var end = this.vportRt.ConvertWcsToScr(line2D.End).ToSKPoint();
                using (var elePen = new SKPaint { Color = SKColors.Gray, IsStroke = true })
                {
                    canvas.DrawLine(start, end, elePen);
                }

            }

        }
        private bool CreateShearWall(Vector2 sp, Vector2 ep, double width,QdShearWallDef wallDef)
        {
            Line2d line2D = new Line2d();
            line2D.Start = sp;
            line2D.End = ep;
            var doc = this.docRt.Document;
            var wall = new QdShearWall(wallDef);
            wall.Initilize(doc);
            wall.BaseCurve = line2D;
            // wall.Curves[0].Curve2ds.Add(line2D);
            wall.Width = width;
            wall.Height = 3000;
            wall.Bottom = 0;
            wall.CreateShapeCurves();
            DisposeShearWalls(wall);

            this.docRt.Action.ClearSelects();
            return true;
        }
        private void DisposeShearWalls(QdShearWall wall, bool Create = true)
        {
            List<QdShearWall> swalls = new List<QdShearWall>();
            List<QdShearWall> ewalls = new List<QdShearWall>();
            if (Create)
                this.docRt.Document.ModelSpace.InsertElement(wall);
        }

        private void changepoint(List<Curve2d> nc, List<Curve2d> oc, Vector2 jp, Vector2 yp, List<Vector2> nps, List<Vector2> ops, Vector2 np, Vector2 op, double n, double o)
        {
            int nj = 0;
            int ny = 0;
            int oj = 0;
            int oy = 0;
            if (Vector2.Distance(nps[0], np) < Vector2.Distance(nps[1], np))
            {
                if (n == 1)
                {
                    nj = 0;
                }
                else
                {
                    ny = 0;
                }
            }
            else
            {
                if (n == 1)
                {
                    nj = 1;
                }
                else
                {
                    ny = 1;
                }
            }
            if (Vector2.Distance(nps[2], np) < Vector2.Distance(nps[3], np))
            {
                if (n == 1)
                {
                    ny = 2;

                }
                else
                {
                    nj = 2;
                }

            }
            else
            {

                if (n == 1)
                {
                    ny = 3;

                }
                else
                {
                    nj = 3;
                }
            }

            if (Vector2.Distance(ops[0], op) < Vector2.Distance(ops[1], op))
            {
                if (o == 1)
                {
                    oj = 0;
                }
                else
                {
                    oy = 0;
                }
            }
            else
            {
                if (o == 1)
                {
                    oj = 1;
                }
                else
                {
                    oy = 1;
                }
            }
            if (Vector2.Distance(ops[2], op) < Vector2.Distance(ops[3], op))
            {
                if (o == 1)
                {
                    oy = 2;

                }
                else
                {
                    oj = 2;
                }

            }
            else
            {

                if (o == 1)
                {
                    oy = 3;

                }
                else
                {
                    oj = 3;
                }
            }

            foreach (Line2d item in oc)
            {
                if (item.Start.Similarity(ops[oj]) || item.End.Similarity(ops[oj]))
                {
                    if (item.Start.Similarity(ops[oj]))
                        item.Start = jp;
                    else
                        item.End = jp;
                }
                if (item.Start.Similarity(ops[oy]) || item.End.Similarity(ops[oy]))
                {
                    if (item.Start.Similarity(ops[oy]))
                        item.Start = yp;
                    else
                        item.End = yp;
                }
            }

            foreach (Line2d item in nc)
            {
                if (item.Start.Similarity(nps[nj]) || item.End.Similarity(nps[nj]))
                {
                    if (item.Start.Similarity(nps[nj]))
                        item.Start = jp;
                    else
                        item.End = jp;
                }
                if (item.Start.Similarity(nps[ny]) || item.End.Similarity(nps[ny]))
                {
                    if (item.Start.Similarity(nps[ny]))
                        item.Start = yp;
                    else
                        item.End = yp;
                }
            }
            nps[nj] = jp;
            ops[oj] = jp;
            nps[ny] = yp;
            ops[oy] = yp;
        }
        private void removelinebyPoint(List<Curve2d> nc, List<Curve2d> oc, Vector2 np, Vector2 op)
        {
            List<Line2d> reline2d = new List<Line2d>();
            reline2d = new List<Line2d>();
            foreach (Line2d item in nc)
            {
                if (Line2d.PointInLine(item, np))
                {
                    reline2d.Add(item);
                    break;
                }
            }
            foreach (var item in reline2d)
                nc.Remove(item);
            reline2d = new List<Line2d>();
            foreach (Line2d item in oc)
            {
                if (Line2d.PointInLine(item, op))
                {
                    reline2d.Add(item);
                    break;
                }
            }
            foreach (var item in reline2d)
                oc.Remove(item);
        }
        private void CornerShearWalls(QdShearWall nwall, QdShearWall owall,double corsswall=1)
        {
            List<Line2d> reline2d = new List<Line2d>();

            Line2d nline1 = new Line2d { Start = nwall.Points[0], End = nwall.Points[1] };
            Line2d nline2 = new Line2d { Start = nwall.Points[2], End = nwall.Points[3] };
            Line2d oline1 = new Line2d { Start = owall.Points[0], End = owall.Points[1] };
            Line2d oline2 = new Line2d { Start = owall.Points[2], End = owall.Points[3] };


             if (corsswall == 1)
            {
                #region
                if (nwall.BaseLine.Start.Similarity(owall.BaseLine.Start))
                {
                    if (Line2d.ParallelLine(nline1, oline1))
                    {
                        if (nwall.Width == owall.Width)
                        {
                            removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.Start, owall.BaseLine.Start);
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.Start, owall.BaseLine.Start);
                    if (Line2d.CrossLine(nline1, oline1))
                    {
                        if (!Line2d.CrossLine(nline1, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 1, 1);

                        }
                        else
                        {
                            Vector2 p2 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline1);
                            if (Vector2.Distance(p1, nwall.BaseLine.Start) > Vector2.Distance(p2, nwall.BaseLine.Start))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 1, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 1, 2);
                            }

                        }
                    }
                    else if (Line2d.CrossLine(nline1, oline2))
                    {
                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 1, 2);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.Start) > Vector2.Distance(p2, nwall.BaseLine.Start))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 1, 2);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 2, 2);
                            }
                        }

                    }
                    else if (Line2d.CrossLine(nline2, oline1))
                    {
                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 2, 1);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.Start) > Vector2.Distance(p2, nwall.BaseLine.Start))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 2, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 2, 2);
                            }
                        }
                    }
                    else if (Line2d.CrossLine(nline2, oline2))
                    {
                        Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                        Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                        changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.Start, 2, 2);
                    }
                }
                else if (nwall.BaseLine.Start.Similarity(owall.BaseLine.End))
                {
                    if (Line2d.ParallelLine(nline1, oline1))
                    {
                        if (nwall.Width == owall.Width)
                        {
                            removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.Start, owall.BaseLine.End);
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.Start, owall.BaseLine.End);


                    if (Line2d.CrossLine(nline1, oline1))
                    {

                        if (!Line2d.CrossLine(nline1, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 1, 1);

                        }
                        else
                        {
                            Vector2 p2 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline1);
                            if (Vector2.Distance(p1, nwall.BaseLine.Start) > Vector2.Distance(p2, nwall.BaseLine.Start))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 1, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 1, 2);
                            }

                        }

                    }
                    else if (Line2d.CrossLine(nline1, oline2))
                    {
                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 1, 2);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.Start) > Vector2.Distance(p2, nwall.BaseLine.Start))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 1, 2);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 2, 2);
                            }
                        }

                    }
                    else if (Line2d.CrossLine(nline2, oline1))
                    {
                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 2, 1);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.Start) > Vector2.Distance(p2, nwall.BaseLine.Start))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 2, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 2, 2);
                            }
                        }
                    }
                    else if (Line2d.CrossLine(nline2, oline2))
                    {
                        Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                        Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                        changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.Start, owall.BaseLine.End, 2, 2);
                    }
                }
                else if (nwall.BaseLine.End.Similarity(owall.BaseLine.Start))
                {
                    if (Line2d.ParallelLine(nline1, oline1))
                    {
                        if (nwall.Width == owall.Width)
                        {
                            removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.End, owall.BaseLine.Start);
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.End, owall.BaseLine.Start);

                    if (Line2d.CrossLine(nline1, oline1))
                    {

                        if (!Line2d.CrossLine(nline1, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 1, 1);

                        }
                        else
                        {
                            Vector2 p2 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline1);
                            if (Vector2.Distance(p1, nwall.BaseLine.End) > Vector2.Distance(p2, nwall.BaseLine.End))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 1, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 1, 2);
                            }

                        }

                    }
                    else if (Line2d.CrossLine(nline1, oline2))
                    {


                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 1, 2);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.End) > Vector2.Distance(p2, nwall.BaseLine.End))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 1, 2);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 2, 2);
                            }
                        }

                    }
                    else if (Line2d.CrossLine(nline2, oline1))
                    {

                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 2, 1);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.End) > Vector2.Distance(p2, nwall.BaseLine.End))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 2, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 2, 2);
                            }
                        }
                    }
                    else if (Line2d.CrossLine(nline2, oline2))
                    {
                        Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                        Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                        changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.Start, 2, 2);
                    }
                }
                else
                {
                    if (Line2d.ParallelLine(nline1, oline1))
                    {
                        if (nwall.Width == owall.Width)
                        {
                            removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.End, owall.BaseLine.End);
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.End, owall.BaseLine.End);


                    if (Line2d.CrossLine(nline1, oline1))
                    {

                        if (!Line2d.CrossLine(nline1, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 1, 1);

                        }
                        else
                        {
                            Vector2 p2 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline1);
                            if (Vector2.Distance(p1, nwall.BaseLine.End) > Vector2.Distance(p2, nwall.BaseLine.End))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 1, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 1, 2);
                            }

                        }


                    }
                    else if (Line2d.CrossLine(nline1, oline2))
                    {

                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 1, 2);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline1, oline2);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.End) > Vector2.Distance(p2, nwall.BaseLine.End))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline1, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline2, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 1, 2);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 2, 2);
                            }
                        }

                    }
                    else if (Line2d.CrossLine(nline2, oline1))
                    {


                        if (!Line2d.CrossLine(nline2, oline2))
                        {
                            Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                            changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 2, 1);
                        }
                        else
                        {
                            Vector2 p1 = Line2d.GetCrossVector(nline2, oline1);
                            Vector2 p2 = Line2d.GetCrossVector(nline2, oline2);
                            if (Vector2.Distance(p1, nwall.BaseLine.End) > Vector2.Distance(p2, nwall.BaseLine.End))
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline1);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline2);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 2, 1);
                            }
                            else
                            {
                                Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                                Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                                changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 2, 2);
                            }
                        }
                    }
                    else if (Line2d.CrossLine(nline2, oline2))
                    {
                        Vector2 jp = Line2d.GetCrossVector(nline2, oline2);
                        Vector2 yp = Line2d.GetCrossVector(nline1, oline1);
                        changepoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, jp, yp, nwall.Points, owall.Points, nwall.BaseLine.End, owall.BaseLine.End, 2, 2);
                    }
                }
                #endregion
            }
           else
            {
                if (nwall.BaseLine.Start.Similarity(owall.BaseLine.Start))
                {
                        removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.Start, owall.BaseLine.Start);
                }
                if (nwall.BaseLine.Start.Similarity(owall.BaseLine.End))
                {
                        removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.Start, owall.BaseLine.End);
                }
                if (nwall.BaseLine.End.Similarity(owall.BaseLine.Start))
                {
                        removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.End, owall.BaseLine.Start);
                }
                if (nwall.BaseLine.End.Similarity(owall.BaseLine.End))
                {
                        removelinebyPoint(nwall.Curves[0].Curve2ds, owall.Curves[0].Curve2ds, nwall.BaseLine.End, owall.BaseLine.End);
                }
                foreach (Line2d item in nwall.Curves[0].Curve2ds)
                {
                    foreach (Line2d item2 in owall.Curves[0].Curve2ds)
                    {
                        if(Line2d.CrossLine(item,item2))
                        {
                            Vector2 crossVector2 = Line2d.GetCrossVector(item, item2);
                            if (Vector2.Distance(item.Start, crossVector2) < Vector2.Distance(item.End, crossVector2))//临时用交点 ，后续可以改为两个墙对应的点 和外轮廓每个线段比较
                            {
                                item.Start = crossVector2;
                            }
                            else
                            {
                                item.End = crossVector2;
                            }
                            if (Vector2.Distance(item2.Start, crossVector2) < Vector2.Distance(item2.End, crossVector2))//临时用交点 ，后续可以改为两个墙对应的点 和外轮廓每个线段比较
                            {
                                item2.Start = crossVector2;
                            }
                            else
                            {
                                item2.End = crossVector2;
                            }
                        }
                    }
                }
            }
        
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nc"></param>
        /// <param name="oc"></param>
        /// <param name="jp"></param>
        public void ChangeCurvesByCorssPoint(List<Curve2d> nc, List<Curve2d> oc,Vector2 jp)
        {
            foreach (Curve2d item in nc)
            {
                if(Line2d.PointInLine(item as Line2d,jp))
                {
                    Line2d line2D = item as Line2d;
                    if (Vector2.Distance(line2D.Start, jp) < Vector2.Distance(line2D.End, jp))//临时用交点 ，后续可以改为两个墙对应的点 和外轮廓每个线段比较
                    {
                        line2D.Start = jp;
                    }
                    else
                    {
                        line2D.End = jp;
                    }
                    break;
                 }
            }
            foreach (Curve2d item in oc)
            {
                if (Line2d.PointInLine(item as Line2d, jp))
                {
                    Line2d line2D = item as Line2d;
                    if (Vector2.Distance(line2D.Start, jp) < Vector2.Distance(line2D.End, jp))//临时用交点 ，后续可以改为两个墙对应的点 和外轮廓每个线段比较
                    {
                        line2D.Start = jp;
                    }
                    else
                    {
                        line2D.End = jp;
                    }
                    break;
                }
            }
        }
        private void AdheringShearWall(QdShearWall nwall, QdShearWall owall, Vector2 cp)
        {
            double sc = Vector2.Distance(cp, nwall.BaseLine.Start);
            double ec = Vector2.Distance(cp, nwall.BaseLine.End);
            if (sc < ec)
            {
            }
            else
            {

            }
        }
        private void CrossShearWall(QdShearWall nwall, QdShearWall owall)
        {

            Line2d nline1 = new Line2d { Start = nwall.Points[0], End = nwall.Points[1] };
            Line2d nline2 = new Line2d { Start = nwall.Points[2], End = nwall.Points[3] };
            Line2d oline1 = new Line2d { Start = owall.Points[0], End = owall.Points[1] };
            Line2d oline2 = new Line2d { Start = owall.Points[2], End = owall.Points[3] };
            Dictionary<Line2d, List<Line2d>> dls = new Dictionary<Line2d, List<Line2d>>();
            foreach (Line2d item in nwall.Curves[0].Curve2ds)
            {
                if (Line2d.CrossLine(item, oline1) && Line2d.CrossLine(item, oline2))
                {
                    dls.Add(item, Line2d.BreakLineBy2line(item, oline1, oline2));

                }
            }
            foreach (var item in dls)
            {
                nwall.Curves[0].Curve2ds.Remove(item.Key);
                nwall.Curves[0].Curve2ds.AddRange(item.Value);
            }
            dls = new Dictionary<Line2d, List<Line2d>>();
            foreach (Line2d item in owall.Curves[0].Curve2ds)
            {
                if (Line2d.CrossLine(item, nline1) && Line2d.CrossLine(item, nline2))
                {
                    dls.Add(item, Line2d.BreakLineBy2line(item, nline1, nline2));

                }
            }
            foreach (var item in dls)
            {
                owall.Curves[0].Curve2ds.Remove(item.Key); owall.Curves[0].Curve2ds.AddRange(item.Value);
            }
        }
        private void ConnectShearWall(QdShearWall nwall, QdShearWall owall, Vector2 p)
        {
            Line2d nline1 = new Line2d { Start = nwall.Points[0], End = nwall.Points[1] };
            Line2d nline2 = new Line2d { Start = nwall.Points[2], End = nwall.Points[3] };
            Line2d oline1 = new Line2d { Start = owall.Points[0], End = owall.Points[1] };
            Line2d oline2 = new Line2d { Start = owall.Points[2], End = owall.Points[3] };
            List<Line2d> removes = new List<Line2d>();
            foreach (Line2d item in nwall.Curves[0].Curve2ds)
            {
                if (Line2d.PointInLine(item, p))
                {
                    removes.Add(item);
                }

            }
            foreach (Line2d item in removes)
                nwall.Curves[0].Curve2ds.Remove(item);



            Dictionary<Line2d, List<Line2d>> dls = new Dictionary<Line2d, List<Line2d>>();
            foreach (Line2d item in owall.Curves[0].Curve2ds)
            {
                if (Line2d.CrossLine(item, nline1) && Line2d.CrossLine(item, nline2))
                {
                    dls.Add(item, Line2d.BreakLineBy2line(item, nline1, nline2));

                }
            }
            foreach (var item in dls)
            {
                owall.Curves[0].Curve2ds.Remove(item.Key); owall.Curves[0].Curve2ds.AddRange(item.Value);
            }


            foreach (Line2d item in nwall.Curves[0].Curve2ds)
            {
                if (Line2d.CrossLine(item, oline1))
                {
                    Vector2 cp = Line2d.GetCrossVector(item, oline1);
                    double sc = Vector2.Distance(cp, item.Start);
                    double ec = Vector2.Distance(cp, item.End);
                    if (sc < ec)
                    {
                        item.Start = cp;
                    }
                    else
                    {
                        item.End = cp;
                    }

                }
                if (Line2d.CrossLine(item, oline2))
                {
                    Vector2 cp = Line2d.GetCrossVector(item, oline2);
                    double sc = Vector2.Distance(cp, item.Start);
                    double ec = Vector2.Distance(cp, item.End);
                    if (sc < ec)
                    {
                        item.Start = cp;
                    }
                    else
                    {
                        item.End = cp;
                    }
                }
            }

            if (Line2d.CrossLine(nline1, oline1))
            {
                Vector2 cp = Line2d.GetCrossVector(nline1, oline1);
                double sc = Vector2.Distance(cp, nline1.Start);
                double ec = Vector2.Distance(cp, nline1.End);
                if (sc < ec)
                {
                    nwall.Points[0] = cp;
                }
                else
                {
                    nwall.Points[1] = cp;
                }
            }
            else if (Line2d.CrossLine(nline1, oline2))
            {
                Vector2 cp = Line2d.GetCrossVector(nline1, oline2);
                double sc = Vector2.Distance(cp, nline1.Start);
                double ec = Vector2.Distance(cp, nline1.End);
                if (sc < ec)
                {
                    nwall.Points[0] = cp;
                }
                else
                {
                    nwall.Points[1] = cp;
                }
            }
            if (Line2d.CrossLine(nline2, oline1))
            {
                Vector2 cp = Line2d.GetCrossVector(nline2, oline1);
                double sc = Vector2.Distance(cp, nline2.Start);
                double ec = Vector2.Distance(cp, nline2.End);
                if (sc < ec)
                {
                    nwall.Points[2] = cp;
                }
                else
                {
                    nwall.Points[3] = cp;
                }
            }
            else if (Line2d.CrossLine(nline2, oline2))
            {
                Vector2 cp = Line2d.GetCrossVector(nline2, oline2);
                double sc = Vector2.Distance(cp, nline2.Start);
                double ec = Vector2.Distance(cp, nline2.End);
                if (sc < ec)
                {
                    nwall.Points[2] = cp;
                }
                else
                {
                    nwall.Points[3] = cp;
                }
            }
            if (Line2d.CrossLine(nwall.BaseLine, oline1))
            {
                Vector2 cp = Line2d.GetCrossVector(nwall.BaseLine, oline1);
                double sc = Vector2.Distance(cp, nwall.BaseLine.Start);
                double ec = Vector2.Distance(cp, nwall.BaseLine.End);
                if (sc < ec)
                {
                    Line2d basel = new Line2d();
                    basel.Start = cp;
                    basel.End = nwall.BaseLine.End;
                    nwall.BaseCurve = basel;

                }
                else
                {
                    Line2d basel = new Line2d();
                    basel.Start = nwall.BaseLine.Start;
                    basel.End = cp;
                    nwall.BaseCurve = basel;

                }
            }
            else if (Line2d.CrossLine(nwall.BaseLine, oline2))
            {
                Vector2 cp = Line2d.GetCrossVector(nwall.BaseLine, oline2);
                double sc = Vector2.Distance(cp, nwall.BaseLine.Start);
                double ec = Vector2.Distance(cp, nwall.BaseLine.End);
                if (sc < ec)
                {
                    Line2d basel = new Line2d();
                    basel.Start = cp;
                    basel.End = nwall.BaseLine.End;
                    nwall.BaseCurve = basel;

                }
                else
                {
                    Line2d basel = new Line2d();
                    basel.Start = nwall.BaseLine.Start;
                    basel.End = cp;
                    nwall.BaseCurve = basel;

                }
            }
        }
        public override SnapPointResult SnapPoint(SnapRuntime snapRt, LcElement element, Vector2 point, bool forRef, Vector2 PrePoint = null)
        {

            var maxDistance = vportRt.GetSnapMaxDistance();
            QdShearWall wall = element as QdShearWall;
            var sscur = SnapSettings.Current;
            var result = new SnapPointResult { Element = element };
            var line2d = wall.BaseCurve;
            var line = wall.BaseLine;
            if (sscur.ObjectOn)
            {

                if (sscur.PointType.Has(SnapPointType.Endpoint))
                {
                    if ((point - line.Start).Length() <= maxDistance)
                    {
                        result.Point = line.Start;
                        result.Name = "Start";
                        result.Curves.Add(new SnapRefCurve(SnapPointType.Endpoint, line2d));
                    }
                    else if ((point - line.End).Length() <= maxDistance)
                    {
                        result.Point = line.End;
                        result.Name = "End";
                        result.Curves.Add(new SnapRefCurve(SnapPointType.Endpoint, line2d));
                    }
                }
                if (sscur.PointType.Has(SnapPointType.Midpoint) && result.Point == null)
                {
                    var cp = (line.Start + line.End) / 2;
                    if ((point - cp).Length() <= maxDistance)
                    {
                        result.Point = cp;
                        result.Name = "Mid";
                        result.Curves.Add(new SnapRefCurve(SnapPointType.Midpoint, line2d));
                    }
                }
                //forRef时只捕捉端点，中点等，如果
                if (!forRef && result.Point == null)
                {
                    var refPoints = snapRt.GetRefPoints(element);
                    var lineLength = (line.End - line.Start).Length();
                    var distance = GeoUtils.PointToLineDistance(line.Start, line.End, point, out Vector2 nearest, out double dirDistance);
                    if (sscur.PointType.Has(SnapPointType.Nearest))
                    {
                        //最近点必须在线段内进行测试
                        if (distance < maxDistance && dirDistance > 0 && dirDistance < lineLength)
                        {
                            result.Point = nearest;
                            result.Name = "Nearest";
                            result.Curves.Add(new SnapRefCurve(SnapPointType.Nearest, line2d));
                        }
                    }
                    //进行延长线上点捕捉，需要元素上有参考点
                    if (sscur.PointType.Has(SnapPointType.ExtensionLine) && refPoints.Count > 0 && result.Point == null)
                    {
                        if (distance < maxDistance)
                        {
                            result.Point = nearest;
                            result.Name = "Extension";
                            result.Curves.Add(new SnapRefCurve(SnapPointType.ExtensionLine, line2d));
                        }
                    }
                }
            }

            if (result.Point != null)
                return result;
            else
                return null;
        }
        public override List<PropertyObserver> GetPropertyObservers()
        {
            //return base.GetPropertyObservers();
            return new List<PropertyObserver>() {       
             new PropertyObserver()
            {
                Name = "Width",
                DisplayName = "墙宽",
                CategoryName = "Geometry",
                CategoryDisplayName = "几何图形",
                PropType=PropertyType.Double,
                Getter = (ele) => (ele as QdShearWall).Parameters.GetValue<double>("Width"),
                Setter = (ele, value) =>
                {
                    var wall = (ele as QdShearWall);
                    if (!double.TryParse(value.ToString(),out var width)||width<0)
                        return;
                    wall.OnPropertyChangedBefore("Width",wall.Parameters.GetValue<double>("Width"),width);
                    wall.Parameters.SetValue("Width",width );
                    wall.CreateShapeCurves();
                    wall.ResetCache();
                    wall.OnPropertyChangedAfter("Width",wall.Parameters.GetValue<double>("Width"),width);
                }
            }
            };
        }
    }
}
