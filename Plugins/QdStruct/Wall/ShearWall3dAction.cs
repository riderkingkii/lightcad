﻿using Shape = LightCAD.Three.Shape;

namespace QdStruct
{
    public  class ShearWall3dAction:ComponentInstance3dAction
    {
        public override List<Object3D> Render(IComponentInstance cptIns)
        {
            var wall = cptIns as QdShearWall;
            var wallDef=wall.Definition as QdShearWallDef;
            var wallLine = wall.BaseLine;
            var wallBot = wall.Bottom;
            var start = (wallLine.Start).ToThree(wallBot);
            var end = (wallLine.End).ToThree(wallBot);
            var lineVec = new Vector3().SubVectors(end, start);
            var wallLen = lineVec.Length();
            var wallHeight = 3000;
            var wallWidth = wall.Width;
            var wallWidthHlf= wallWidth/2;
            var xAxis = lineVec.Clone().Normalize();
            var yAxis = VectorUtil.ZAxis.Clone();
            var zAxis = new Vector3().CrossVectors(xAxis,yAxis).Normalize();
            var coodMat = new Matrix4();
            coodMat.MakeBasis(xAxis, yAxis, zAxis);
            coodMat.SetPosition(start);
            var start2 = new Vector2(start.X, start.Y);
            var wallAxis2 = new Vector2(xAxis.X, xAxis.Y);
            Shape getShearWallRect(Vector2 wStart,Vector2 wEnd) 
            {
                var minx= wStart.Clone().Sub(start2).Dot(wallAxis2);
                var maxx = wEnd.Clone().Sub(start2).Dot(wallAxis2);
                return ShapeUtil.Rect(minx, 0, maxx, wallHeight);
            }
            //var wallShape2 = Rect(0, 0, wallLen, wallHeight);
            Vector2 ls ,le,rs,re;
            //wall.Points[0].ToThree().cross(wallAxis2)
            if (Vector2.Cross(wallLine.End- wallLine.Start, wall.Points[0] - wallLine.Start) < 0)
            {
                ls = wall.Points[3];
                le = wall.Points[2];
                rs = wall.Points[0];
                re = wall.Points[1];
            }
            else 
            {
                ls = wall.Points[0];
                le = wall.Points[1];
                rs = wall.Points[3];
                re = wall.Points[2];
            }
            var frontShape2 = getShearWallRect(rs, re);
            var backShape2 = getShearWallRect(ls, le);
            
            //if (wall.AssociateElements.Count > 0)
            //{
            //    for (int i = 0; i < wall.AssociateElements.Count; i++)
            //    {
            //        var assEle = wall.AssociateElements[i];
            //        var qdEle = assEle.Element;
            //        double holeBot=0, holeHeight=0;
            //        Vector2 sp=new Vector2(),ep=new Vector2();
            //        if (qdEle is QdDoorInstance)
            //        {
            //            var door = (QdDoorInstance)qdEle;
            //            sp = door.StartPoint;
            //            ep = door.EndPoint;
            //            holeBot = door.Bottom;
            //            holeHeight = door.Height;
            //        }
            //        else if (qdEle is QdWindowInstance)
            //        {
            //            var window = (QdWindowInstance)qdEle;
            //            sp = window.StartPoint;
            //            ep = window.EndPoint;
            //            holeBot = window.Bottom;
            //            holeHeight = window.Height;
            //        }
            //        else
            //            continue;
            //        var ds2 = sp.Clone();
            //        var de2 = ep.Clone();
            //        var sx = ds2.Sub(start2).Dot(wallAxis2);
            //        var ex = de2.Sub(start2).Dot(wallAxis2);
            //        var miny = Math.Max(holeBot - wall.Bottom, 0);
            //        var maxy = holeHeight + miny;
            //        if (sx > ex)
            //        {
            //            var temp = sx;
            //            sx = ex;
            //            ex = temp;
            //        }
            //        var shape = ShapeUtil.Rect(sx, miny, ex, maxy);
            //        frontShape2.holes.Push(shape);
            //        backShape2.holes.Push(shape);
            //    }
            //}
            var frontShape = new CoordShape3(frontShape2, coodMat, wallWidthHlf);
            var backShape = new CoordShape3(backShape2, coodMat, -wallWidthHlf);
            var geoData = GeoUtil.GetMappedShapeGeometryData(frontShape, backShape);
            var geo = geoData.GetBufferGeometry();
            geo.computeVertexNormals();
            geo.SetUV();
            var mats= wallDef.Solid3dProvider.AssignMaterialsFunc(cptIns, null);
            var wallMesh=new Mesh(geo, mats.Select(m=>RenderMaterialManager.GetRenderMaterial(m.Uuid)).ToArray());
            var result = new List<Object3D>() { wallMesh};
            return result;
        }
        
    }
}