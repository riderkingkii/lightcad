﻿using LightCAD.Three;
using static LightCAD.Three.WebGLMorphtargets;

namespace QdStruct
{
    public class ColumnAttribute : CptAttribute
    {
        public ColumnAttribute(string category, string subCategorys) : base(category, subCategorys)
        {

        }
        public const string BuiltinUuid = "{adf732c1-87c8-464e-a966-05620c68362a}";

        public override LcComponentDefinition GetBuiltinCpt(string subCategory)
        {
            return new QdColumnDef(BuiltinUuid,"内置", "结构柱");
        }
    }
    [ColumnAttribute("结构柱", "结构柱")]
    public class QdColumnDef : LcComponentDefinition
    {
        static QdColumnDef()
        {
            CptTypeParamsDef<QdColumnDef>.ParamsDef = new ParameterSetDefinition {
                new ParameterDefinition { DataType = LcDataType.String, Name = "MaterialId", DisplayName = "材质", },
            };
        }
        public QdColumnDef()
        {
            this.TypeParameters = new LcParameterSet(CptTypeParamsDef<QdColumnDef>.ParamsDef);
        }
        public QdColumnDef(string uuid, string name, string subCategory) : base(uuid, name, "结构柱", subCategory, new LcParameterSet(CptTypeParamsDef<QdColumnDef>.ParamsDef))
        {
            this.Parameters = new ParameterSetDefinition
            {
                new ParameterDefinition(){ Name = "Lines",DisplayName="柱面轮廓", DataType=LcDataType.CurveArray },
                new ParameterDefinition(){ Name = "Height",DisplayName="高度", DataType=LcDataType.Double },
                new ParameterDefinition(){ Name = "Bottom",DisplayName="底高度", DataType=LcDataType.Double }, 
             };
            var mat = MaterialManager.GetMaterial(MaterialManager.ConcreteUuid);
            this.Solid3dProvider = new Solid3dProvider(name) { AssignMaterialsFunc = (c, s) => new LcMaterial[] { mat } };
            this.Curve2dProvider = new Curve2dProvider(name)
            {
                GetCurve2dsFunc = (pset) =>
                {
                    var curve2ds = new List<Curve2d>();
                    var lines = pset.GetValue<Curve2d[]>("Lines");
 
                    return new Curve2dGroupCollection() { new Curve2dGroup()
                        {
                            Curve2ds = lines.ToListEx()
                        }
                    };
                }
            };

        }
    }
    public class QdColumnInstance : LcComponentInstance
    {
        public double Height { get => Parameters.GetValue<double>("Height"); set => Set("Height", value); }
        public double Bottom { get => Parameters.GetValue<double>("Bottom"); set => Set("Bottom", value); }
        public Curve2d[] Lines { get => Parameters.GetValue<Curve2d[]>("Lines"); set => Set("Lines", value); }

        public QdColumnInstance(QdColumnDef wallDef) : base(wallDef)
        {
            this.Type = StructElementType.Column;
        }
        public void Set(string name, object val)
        {
            OnPropertyChangedBefore(name, this.Parameters[name], val);
            this.Parameters.SetValue(name, val);
            OnPropertyChangedAfter(name, this.Parameters[name], val);
        }
        public override Box2 GetBoundingBox()
        {
            if (this.Parameters == null || this.Parameters.Values.Length == 0|| Lines.Length == 0)
            {
                return Box2.Empty;
            }
            var ps = Lines.SelectMany(n => {
                if (n is Line2d line)
                    return new List<Vector2> { line.Start, line.End };
                else
                {
                    var arc = n as Arc2d;
                    return arc.GetPoints(32).ToList();
                }
            }).ToList();
            var min = new Vector2(ps.Min(n => n.X), ps.Min(n => n.Y));
            var max = new Vector2(ps.Max(n => n.X), ps.Max(n => n.Y));
            return new Box2(min, max);

        }
 
 
    }
}