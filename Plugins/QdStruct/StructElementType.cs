﻿using QdStruct;
using static QdStruct.QdBeamDef;

namespace QdStruct
{
    public static class StructElementType
    {
        public static ElementType ShearWall = new ElementType
        {                           
            Guid = Guid.ParseExact("{03417459-CC2B-75C0-BD7A-675E1850AE8D}", "B").ToLcGuid(),
            Name = "ShearWall",
            DispalyName = "结构墙",
            ClassType= typeof(QdShearWall)
        };
        public static ElementType Beam = new ElementType
        {
            Guid = Guid.ParseExact("{148c57ef-09b2-4490-9f60-2d98706f62b8}", "B").ToLcGuid(),
            Name = "Beam",
            DispalyName = "结构梁",
            ClassType = typeof(QdBeamInstance)
        };
        public static ElementType Column = new ElementType
        {
            Guid = Guid.ParseExact("{390bd93e-ce8d-40a5-86e8-10185d340588}", "B").ToLcGuid(),
            Name = "Column",
            DispalyName = "结构柱",
            ClassType = typeof(QdColumnInstance)
        };
        public static ElementType[] All = new ElementType[]
        {
            ShearWall,Beam,Column
        };
    }
}
