﻿namespace QdStruct
{
    public enum StructCategory
    {
        Unknown,
        Wall,
        Door,
        Window,
        Stair
    }
    public interface IEnumItem
    {
         string Name { get; }
        object GetValue();
         string DisplayName { get; }
    }
    public class StructCategoryItem: IEnumItem
    {
        public string Name => Value.ToString();
        public StructCategory Value { get; set; }
        public string DisplayName { get; set; }
        public StructCategoryItem(StructCategory value, string displayName)
        {
            Value = value;
            DisplayName = displayName;
        }

        public object GetValue()
        {
            return this;
        }
    }
    public static class StructCategoryExt
    {
        private static List<StructCategoryItem> _itemList;
        public static List<StructCategoryItem> ItemList
        {
            get
            {
                if (_itemList == null)
                {
                    _itemList = new List<StructCategoryItem>();
                    var values=Enum.GetValues(typeof(StructCategory));
                    for(var i=0; i< values.Length; i++)
                    {
                        var value = (StructCategory)values.GetValue(i);
                        _itemList.Add(new StructCategoryItem(value, value.ToDisplay()));
                    }
                }
                return _itemList;
            }
        }
        
        public static string ToDisplay(this StructCategory category)
        {
            switch (category)
            {
                case StructCategory.Wall: return "墙";
                case StructCategory.Door: return "门";
                case StructCategory.Window: return "窗";
                case StructCategory.Stair: return "楼梯";
                case StructCategory.Unknown: return "未知";
                default:
                    return string.Empty;
            }
        }
    }
}
