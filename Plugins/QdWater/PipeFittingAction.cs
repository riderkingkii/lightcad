﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Drawing;
using LightCAD.Drawing.Actions;
using LightCAD.Runtime;
using netDxf.Entities;
using SkiaSharp;
using LightCAD.MathLib;
using System;
using System.Collections.Generic;

namespace QdWater
{
    public class PipeFittingAction : ElementAction
    {
        public PipeFittingAction() { }
        public PipeFittingAction(IDocumentEditor docEditor)
        : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Line");
        }
        static PipeFittingAction()
        {
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "CreatePipeFittingAction",
                Description = "创建水管管件",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep { Name = "Step0", Options = "指定第一个点:" },

                }
            };
        }
        internal static void Initilize()
        {
            WaterActions.PipeFittingAction = new PipeFittingAction();
            LcDocument.ElementActions.Add(WaterElementType.PipeFitting, WaterActions.PipeFittingAction);
        }
        private static readonly LcCreateMethod[] CreateMethods;

        public async void ExecCreate(string[] args = null)
        {


        }
        public override void Draw(SKCanvas canvas, LcElement element, Vector2 offset)
        {
        }

        public void DrawArc(SKCanvas canvas, SKPaint elePen, Vector2 Point_O, Vector2 Point_A, Vector2 Point_B)
        {
            var center = this.vportRt.ConvertWcsToScr(Point_O).ToSKPoint();
            var start = this.vportRt.ConvertWcsToScr(Point_A).ToSKPoint();
            var end = this.vportRt.ConvertWcsToScr(Point_B).ToSKPoint();

            double radius = Math.Sqrt(Math.Pow(center.X - end.X, 2) + Math.Pow(center.Y - end.Y, 2));
            float x1 = (float)(center.X - radius);
            float y1 = (float)(center.Y - radius);
            float x2 = (float)(center.X + radius);
            float y2 = (float)(center.Y + radius);
            SKRect skrect = new SKRect(x1, y1, x2, y2);


            //与X轴的夹角
            double angle = Math.Atan2(Point_A.Y - Point_O.Y, Point_A.X - Point_O.X);


            double A_Angle = angle * 180 / Math.PI;

            angle = Math.Atan2(Point_B.Y - Point_O.Y, Point_B.X - Point_O.X);

            double B_Angle = angle *  180 / Math.PI;
            var a = (B_Angle > A_Angle ? A_Angle : -A_Angle);
            var b = (B_Angle > A_Angle ? (B_Angle - A_Angle) : (A_Angle - B_Angle));
            while (b < -180)
            {
                b += 180;
            }
            while (b > 180)
            {
                b -= 180;
            }
            canvas.DrawArc(skrect, (float)a, (float)b, false, elePen);


            //LcArc arc = new LcArc();
            //arc.Startp = Point_A;
            //arc.Endp = Point_B;
            //arc.Midp = new Vector2d(skrect.MidX, skrect.MidY);
            //arc.LoadArcStatsProperty();
            //canvas.DrawArc(skrect, (float)arc.StartAngle, (float)arc.MoveAngle, false, elePen);

        }
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var group = element as LcPipeFitting;
            var grips = new List<ControlGrip>();
            var gripCenter = new ControlGrip
            {
                Element = group,
                Name = "Center",
                Position = group.CentralPoint,

            };
            grips.Add(gripCenter);
            return grips.ToArray();
        }
    }
}