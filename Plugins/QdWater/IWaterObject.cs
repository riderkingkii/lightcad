﻿using LightCAD.Core;
using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QdWater
{
    public interface IWaterObject
    {
        public long Id { get; set; }
        public WaterObjectType WaterType { get;  }
        public Dictionary<long, LcElement> LinkElement { get; set; }
        public SortedDictionary<int, WaterConnector> Connectors { get; set; }
        public List<Line2d> Outline { get; set; }
    }

    public enum WaterObjectType
    {
        Pipe,
        PipeFitting
    }
    public class WaterConnector
    {
        public Vector2 CenterPoint { get; set; }
        public double Bottom { get; set; }
    }
}
