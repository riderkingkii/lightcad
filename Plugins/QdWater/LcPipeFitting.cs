﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace QdWater
{
    internal class LcPipeFitting : LcElement, IWaterObject
    {
        public double Bottom { get; set; }
        public LcPipeFitting()
        {
            this.Type = WaterElementType.PipeFitting;
        }
        public Vector2 CentralPoint { get; set; }
        public WaterObjectType WaterType { get { return WaterObjectType.PipeFitting; }  }
        public Dictionary<long, LcElement> LinkElement { get; set; }
        public SortedDictionary<int, WaterConnector> Connectors { get; set; }
        public List<Line2d> Outline { get; set; }

        public override LcPipeFitting Clone()
        {
            LcPipeFitting lcPipeFitting = new LcPipeFitting();
            return lcPipeFitting;
        }
        public override Box2 GetBoundingBox()
        {
            return new Box2(CentralPoint - new Vector2(50, 50), CentralPoint + new Vector2(50, 50));
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            return new Box2().SetFromPoints(matrix.MultiplyPoint(CentralPoint - new Vector2(50, 50)), matrix.MultiplyPoint(CentralPoint + new Vector2(50, 50)));
        }
        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                //如果元素盒子，与多边形盒子不相交，那就可能不相交
                return false;
            }
            return GeoUtils.IsPolygonIntersectLine(testPoly.Points, CentralPoint, CentralPoint)
                || GeoUtils.IsPolygonContainsLine(testPoly.Points, CentralPoint, CentralPoint);
        }

        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsPolygonContainsLine(testPoly.Points, CentralPoint, CentralPoint);
        }
    }
}
