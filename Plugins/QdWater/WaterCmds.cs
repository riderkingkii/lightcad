﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QdWater
{
    [CommandClass]
    public class WaterCmds
    {
        [CommandMethod(Name = "WPipe", ShortCuts = "WP")]
        public CommandResult DrawPipe(IDocumentEditor docEditor, string[] args)
        {
            var wallAction = new PipeAction(docEditor);
            wallAction.ExecCreate(args);
            return CommandResult.Succ();
        }
       
    }
}
