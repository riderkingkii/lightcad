﻿using LightCAD.Core;
using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QdWater
{

    public class WaterPlugin 
    {
        public void InitUI()
        {
        }
        public static void Loaded()
        {
            LcDocument.RegistElementTypes(WaterElementType.All);
            LcRuntime.RegistAssemblies.Add("QdWater");
            LcDocument.ElementActions.Add(WaterElementType.Pipe, new PipeAction());
            LcDocument.ElementActions.Add(WaterElementType.PipeFitting, new PipeFittingAction());
            LcDocument.Element3dActions.Add(WaterElementType.Pipe, new Pipe3dAction());
       
        }
    }
}
