﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QdWater
{
    public static class WaterElementType
    {
        public static ElementType Pipe = new ElementType
        {                           
            Guid = Guid.ParseExact("{C9F194A9-95DC-ADE9-E1C1-7DDEA335293A}", "B").ToLcGuid(),
            Name = "Pipe",
            DispalyName = "管道",
            ClassType= typeof(LcPipe)
        };
        public static ElementType PipeFitting = new ElementType
        {
            Guid = Guid.ParseExact("{00C61E53-7B9C-2E2B-0319-A19009637A5D}", "B").ToLcGuid(),
            Name = "PipeFitting",
            DispalyName = "管道附件",
            ClassType = typeof(LcPipeFitting)
        };
        public static ElementType[] All = new ElementType[]
        {
            Pipe,PipeFitting
        };
    }
}
