﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Drawing;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QdWater
{
    /// <summary>
    /// 元素活动的静态对象，用于渲染，避免新创建对象
    /// </summary>
    public static class WaterActions
    {
        public static ElementAction PipeAction { get; set; }
        public static ElementAction PipeFittingAction { get; set; }
    }
}
