﻿using Constants = LightCAD.Runtime.Constants;

namespace QdArch
{
    public partial class SlabAction : ElementAction
    {
        public enum EmbedType
        {
            Hole,
            UpSlab,
            DownSlab
        };

        private static readonly LcCreateMethod[] CreateMethods;

        private List<Vector2> outPoints;
        private List<List<Vector2>> holes;
        private Dictionary<EmbedType, List<QdUpDownSlab>> upDownSlabs;
        private Vector2 firstPoint;
        private Vector2 secondPoint;
        private PointInputer pointInputer;
        private CmdTextInputer cmdTextInputer;
        private List<LcLine> tempLines;

        private static SKPaint upPen = new SKPaint { Color = SKColors.Red, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 3, 3 }, 10) };
        private static SKPaint downPen = new SKPaint { Color = SKColors.Orange, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 3, 3 }, 10) };

        public SlabAction() { }

        public SlabAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Slab");
        }

        static SlabAction()
        {
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "CreateSlab",
                Description = "创建楼板",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep { Name = "Step0", Options = "指定轮廓第一个点:" },
                    new LcCreateStep { Name = "Step1", Options = "指定轮廓下一点或 [结束(E)]" },
                    new LcCreateStep { Name = "Step2", Options = "选择创建 [洞口(H)/升板(U)/降板(D)]" },
                    new LcCreateStep { Name = "Step3", Options = "指定洞口(升板/降板)第一个点或 [放弃(U)]" },
                    new LcCreateStep { Name = "Step4", Options = "指定洞口(升板/降板)下一个点或 [结束(E)]" },
                    new LcCreateStep { Name = "Step5", Options = "设置降板(升板)底高度" },
                    new LcCreateStep { Name = "Step6", Options = "指定板厚<200>或 [放弃(U)]" }
                }
            };
        }

        public async void ExecCreate(string[] args = null)
        {
            this.tempLines = new List<LcLine>();
            this.outPoints = new List<Vector2>();
            this.holes = new List<List<Vector2>>();
            this.upDownSlabs = new Dictionary<EmbedType, List<QdUpDownSlab>>()
            {
                { EmbedType.UpSlab, new List<QdUpDownSlab>() },
                { EmbedType.DownSlab, new List<QdUpDownSlab>() },
            };
            this.StartCreating();
            this.pointInputer = new PointInputer(this.docEditor);
            this.cmdTextInputer = new CmdTextInputer(this.docEditor);
            var curMethod = CreateMethods[0];

            var doc = this.docRt.Document;
        Step0: //选择外轮廓第一个点
            var step0 = curMethod.Steps[0];
            var result0 = await pointInputer.Execute(step0.Options);
            if (pointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }

            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }

            if (result0.ValueX == null)
            {
                if (result0.Option != null)
                {
                    goto Step0;
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step0;
                }
            }

            this.firstPoint = (Vector2)result0.ValueX;
            this.outPoints.Add(this.firstPoint);

        Step1: //绘制外轮廓
            var step1 = curMethod.Steps[1];
            var result1 = await pointInputer.Execute(step1.Options);
            if (pointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }

            if (result1.ValueX == null)
            {
                if (result1.Option == "C")
                {
                    goto End;
                }
                else if (result1.Option != null)
                {
                    this.commandCtrl.WriteInfo(SR.PointError);
                    goto Step2;
                }
                else
                {
                    goto Step2;
                }
            }

            this.secondPoint = (Vector2)result1.ValueX;
            this.outPoints.Add(secondPoint);
            AppendTempLine(this.firstPoint, this.secondPoint);
            if (this.outPoints.Count > 2 && this.secondPoint.Equals(this.outPoints[0])) //首位相连直接进入下一步
            {
                firstPoint = null;
                goto Step2;
            }
            firstPoint = secondPoint;
            goto Step1;

        Step2: //选择绘制内部洞口/升板/降板, 或跳过
            var curEmbedType = EmbedType.Hole;
            var step2 = curMethod.Steps[2];
            var result2 = await cmdTextInputer.Execute(step2.Options);
            if (cmdTextInputer.isCancelled)
            {
                goto Step6;
            }
            if (result2 == null)
            {
                goto Step6;
            }
            if (string.IsNullOrEmpty(result2.ValueX))
            {
                goto Step2;
            }
            switch (result2.ValueX.ToUpper())
            {
                case "H":
                    curEmbedType = EmbedType.Hole;
                    break;
                case "U":
                    curEmbedType = EmbedType.UpSlab;
                    break;
                case "D":
                    curEmbedType = EmbedType.DownSlab;
                    break;
                default:
                    break;
            }

        Step3: //选择洞口/升板/降板第一个点
            var step3 = curMethod.Steps[3];
            var curInnerLine = new List<Vector2>();
            var result3 = await pointInputer.Execute(step3.Options);
            if (pointInputer.isCancelled)
            {
                goto Step2;
            }
            if (result3 == null)
            {
                goto Step2;
            }

            if (result3.ValueX == null)
            {
                if (result3.Option != null)
                {
                    goto Step3;
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step3;
                }
            }

            this.firstPoint = (Vector2)result3.ValueX;
            curInnerLine.Add(this.firstPoint);

        Step4: //绘制洞口/升板/降板外轮廓
            var step4 = curMethod.Steps[4];
            var result4 = await pointInputer.Execute(step4.Options);
            if (pointInputer.isCancelled)
            {
                goto Step2;
            }
            if (result4 == null)
            {
                goto Step2;
            }

            if (result4.ValueX == null)
            {
                if (result4.Option != null)
                {
                    goto Step4;
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step4;
                }
            }

            this.secondPoint = (Vector2)result4.ValueX;
            curInnerLine.Add(this.secondPoint);
            AppendTempLine(this.firstPoint, this.secondPoint);
            if (curInnerLine.Count > 3 && this.secondPoint.Equals(curInnerLine[0])) //首位相连直接进入下一步
            {
                this.firstPoint = null;
                if (curEmbedType == EmbedType.Hole)
                {
                    this.holes.Add(curInnerLine);
                }
                else if(curEmbedType == EmbedType.UpSlab || curEmbedType == EmbedType.DownSlab)
                {
                    goto Step5;
                }
                goto Step2;
            }

            firstPoint = secondPoint;
            goto Step4;

        Step5: //如果是升板/降板, 需要设置底高度
            var step5 = curMethod.Steps[5];
            var result5 = await cmdTextInputer.Execute(step5.Options);
            if (cmdTextInputer.isCancelled)
            {
                goto Step2;
            }
            if (result5 == null || string.IsNullOrEmpty(result5.ValueX))
            {
                goto Step2;
            }

            if (!double.TryParse(result5.ValueX, out double bottom))
            {
                goto Step5;
            }


            var embedSlab = new QdUpDownSlab(curInnerLine)
            {
                Bottom = bottom
            };
            this.upDownSlabs[curEmbedType].Add(embedSlab);
            goto Step2;

        Step6: //设置楼板厚度

            var step6 = curMethod.Steps[6];
            var result6 = await cmdTextInputer.Execute(step6.Options);
            double thickness = 200; //默认值

            if (cmdTextInputer.isCancelled)
            {
                //goto Step2;
                thickness = 200; //默认值
            }
            else
            {
                if (result6 == null || string.IsNullOrEmpty(result6.ValueX))
                {
                    //goto Step6;
                    thickness = 200;
                }
                else
                {
                    if (!double.TryParse(result6.ValueX, out thickness))
                    {
                        //goto Step6;
                        thickness = 200;
                    }
                }
            }

            this.ClearTempLine();
            CreateSlab(thickness);

        End:
            if (this.outPoints.Count < 4) //小于三个点无法创建轮廓，轮廓需要手动闭合，所以将最后一个点设置为开始点
            {
                //ERROR: 无法创建
            }
            this.pointInputer = null;
            this.EndCreating();
        }

        public override void Cancel()
        {
            base.Cancel();
            this.ClearTempLine();
            this.outPoints.Clear();
            this.holes.Clear();
            this.upDownSlabs.Clear();
            this.vportRt.SetCreateDrawer(null);
        }

        public void AppendTempLine(Vector2 sp, Vector2 ep)
        {
            var line = this.docRt.Document.CreateObject<LcLine>();
            line.Start = sp;
            line.End = ep;
            this.tempLines.Add(line);

            this.vportRt.ActiveElementSet.InsertElement(line);
        }

        public void ClearTempLine()
        {
            foreach (var line in this.tempLines)
            {
                this.vportRt.ActiveElementSet.RemoveElement(line);
            }
        }

        public void CreateSlab(double thickness = 100)
        {
            var doc = this.docRt.Document;
            var slab = new QdSlab(ComponentManager.GetCptDef<QdSlabDef>(SlabAttribute.BuiltinUuid));
            slab.Initilize(doc);
            slab.Outline = this.outPoints ?? new List<Vector2>();
            slab.ResetBoundingBox();
            slab.DragPoint = slab.BoundingBox.Center;
            foreach (var hole in this.holes)
            {
                slab.Holes.Add(hole);
            }
            foreach (var kvp in this.upDownSlabs)
            {
                if (kvp.Key == EmbedType.UpSlab)
                {
                    slab.UpSlabs = kvp.Value;
                }
                else if(kvp.Key == EmbedType.DownSlab)
                {
                    slab.DownSlabs = kvp.Value;
                }
            }
            slab.Thickness = thickness;
            doc.ModelSpace.InsertElement(slab);
            this.docRt.Action.ClearSelects();
        }

        public override void DrawAuxLines(SKCanvas canvas)
        {
            if (firstPoint != null)
            {
                var mp = this.vportRt.PointerMovedPosition.ToVector2d();
                var wcs_mp = this.vportRt.ConvertScrToWcs(mp);

                var sk_pre = this.vportRt.ConvertWcsToScr(firstPoint).ToSKPoint();
                var sk_p = this.vportRt.ConvertWcsToScr(wcs_mp).ToSKPoint();
                //辅助元素的颜色 
                canvas.DrawLine(sk_pre, sk_p, LightCAD.Runtime.Constants.auxElementPen);
            }
        }

        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var slab = element as QdSlab;
            var pen = this.GetDrawPen(slab);
            DrawSlab(canvas, slab, matrix, pen);
        }


        public void DrawSlab(SKCanvas canvas, QdSlab slab, Matrix3 matrix, SKPaint pen)
        {
            for (int i = 0; i < slab.Outline.Count - 1; i++)
            {
                var sp = slab.Outline[i];
                var ep = slab.Outline[i + 1];
                var msp = matrix.MultiplyPoint(sp);
                var mep = matrix.MultiplyPoint(ep);

                var ssp = this.vportRt.ConvertWcsToScr(msp).ToSKPoint();
                var sep = this.vportRt.ConvertWcsToScr(mep).ToSKPoint();

                if (pen == Constants.defaultPen)
                {
                    using (var elePen = new SKPaint { Color = new SKColor(slab.GetColorValue()), IsStroke = true })
                    {

                        canvas.DrawLine(ssp, sep, elePen);
                    }
                }
                else
                {
                    canvas.DrawLine(ssp, sep, pen);
                }
            }

            foreach (var innerPoints in slab.Holes)
            {
                for (int i = 0; i < innerPoints.Count - 1; i++)
                {
                    var sp = innerPoints[i];
                    var ep = innerPoints[i + 1];
                    var msp = matrix.MultiplyPoint(sp);
                    var mep = matrix.MultiplyPoint(ep);

                    var ssp = this.vportRt.ConvertWcsToScr(msp).ToSKPoint();
                    var sep = this.vportRt.ConvertWcsToScr(mep).ToSKPoint();

                    if (pen == Constants.defaultPen)
                    {
                        using (var elePen = new SKPaint { Color = new SKColor(slab.GetColorValue()), IsStroke = true })
                        {
                            canvas.DrawLine(ssp, sep, elePen);
                        }
                    }
                    else
                    {
                        canvas.DrawLine(ssp, sep, pen);
                    }
                }
            }

            foreach (var embedSlab in slab.UpSlabs)
            {
                for (int i = 0; i < embedSlab.Outline.Count - 1; i++)
                {
                    var sp = embedSlab.Outline[i];
                    var ep = embedSlab.Outline[i + 1];
                    var msp = matrix.MultiplyPoint(sp);
                    var mep = matrix.MultiplyPoint(ep);

                    var ssp = this.vportRt.ConvertWcsToScr(msp).ToSKPoint();
                    var sep = this.vportRt.ConvertWcsToScr(mep).ToSKPoint();

                    if (pen == Constants.defaultPen)
                    {
                        canvas.DrawLine(ssp, sep, upPen);
                    }
                    else
                    {
                        canvas.DrawLine(ssp, sep, pen);
                    }
                }
            }

            foreach (var embedSlab in slab.DownSlabs)
            {
                for (int i = 0; i < embedSlab.Outline.Count - 1; i++)
                {
                    var sp = embedSlab.Outline[i];
                    var ep = embedSlab.Outline[i + 1];
                    var msp = matrix.MultiplyPoint(sp);
                    var mep = matrix.MultiplyPoint(ep);

                    var ssp = this.vportRt.ConvertWcsToScr(msp).ToSKPoint();
                    var sep = this.vportRt.ConvertWcsToScr(mep).ToSKPoint();

                    if (pen == Constants.defaultPen)
                    {
                        canvas.DrawLine(ssp, sep, downPen);
                    }
                    else
                    {
                        canvas.DrawLine(ssp, sep, pen);
                    }
                }
            }

            //var box2d = slab.BoundingBox;
            //var rectPen = new SKPaint { Color = SKColors.Green, IsStroke = true };

            //var lt = this.vportRt.ConvertWcsToScr(box2d.LeftTop).ToSKPoint();
            //var rt = this.vportRt.ConvertWcsToScr(box2d.RightTop).ToSKPoint();
            //var lb = this.vportRt.ConvertWcsToScr(box2d.LeftBottom).ToSKPoint();
            //var rb = this.vportRt.ConvertWcsToScr(box2d.RightBottom).ToSKPoint();
            //canvas.DrawLine(lt, rt, rectPen);
            //canvas.DrawLine(rt, rb, rectPen);
            //canvas.DrawLine(rb, lb, rectPen);
            //canvas.DrawLine(lb, lt, rectPen);
        }

        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var slab = element as QdSlab;
            var grips = new List<ControlGrip>();

            var gripCenter = new ControlGrip
            {
                Element = slab,
                Name = "Center",
                Position = slab.DragPoint
            };
            grips.Add(gripCenter);

            for (int i = 0; i < slab.Outline.Count; i++)
            {
                var point = slab.Outline[i];
                var grip = new ControlGrip
                {
                    Element = slab,
                    Name = $"Outline_{i}",
                    Position = point
                };
                grips.Add(grip);
            }

            for (int i = 0; i < slab.Holes.Count; i++)
            {
                var hole = slab.Holes[i];
                for (int j = 0; j < hole.Count - 1; j++)
                {
                    var point = hole[j];
                    var grip = new ControlGrip
                    {
                        Element = slab,
                        Name = $"Hole_{i}_{j}",
                        Position = point
                    };
                    grips.Add(grip);
                }
            }
            for (int i = 0; i < slab.UpSlabs.Count; i++)
            {
                var embedSlab = slab.UpSlabs[i];
                for (int j = 0; j < embedSlab.Outline.Count - 1; j++)
                {
                    var point = embedSlab.Outline[j];
                    var grip = new ControlGrip
                    {
                        Element = slab,
                        Name = $"UpSlab_{i}_{j}",
                        Position = point
                    };
                    grips.Add(grip);
                }
            }
            for (int i = 0; i < slab.DownSlabs.Count; i++)
            {
                var embedSlab = slab.DownSlabs[i];
                for (int j = 0; j < embedSlab.Outline.Count - 1; j++)
                {
                    var point = embedSlab.Outline[j];
                    var grip = new ControlGrip
                    {
                        Element = slab,
                        Name = $"DownSlab_{i}_{j}",
                        Position = point
                    };
                    grips.Add(grip);
                }
            }
            return grips.ToArray();
        }

        private QdSlab _slab;
        private string _gripName;
        private Vector2 _position;

        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            var slab = element as QdSlab;
            _slab = slab;
            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {
                if (gripName == "Center")
                {
                    var offset = position - slab.DragPoint;
                    slab.TranslationByOffset(offset);
                }
                else if (_gripName.StartsWith("Outline"))
                {
                    var idx = int.Parse(_gripName.Split('_')[1]);
                    slab.Outline[idx] = position;
                }
                else if (_gripName.StartsWith("Hole"))
                {
                    var i = int.Parse(_gripName.Split('_')[1]);
                    var j = int.Parse(_gripName.Split('_')[2]);

                    slab.Holes[i][j] = position;
                }
                else if (_gripName.StartsWith("UpSlab"))
                {
                    var i = int.Parse(_gripName.Split('_')[1]);
                    var j = int.Parse(_gripName.Split('_')[2]);

                    slab.UpSlabs[i].Outline[j] = position;
                }
                else if (_gripName.StartsWith("DownSlab"))
                {
                    var i = int.Parse(_gripName.Split('_')[1]);
                    var j = int.Parse(_gripName.Split('_')[2]);

                    slab.DownSlabs[i].Outline[j] = position;
                }
            }
        }

        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_slab == null)
                return;

            if (_gripName == "Center")
            {
                var offset = _position - _slab.DragPoint;
                var matrix = Matrix3.GetTranslate(offset);
                DrawSlab(canvas, _slab, matrix, Constants.draggingPen);
            }
            else if (_gripName.StartsWith("Outline"))
            {
                var idx = int.Parse(_gripName.Split('_')[1]);
                DrawTempLine(canvas, _slab.Outline, idx);
            }
            else if (_gripName.StartsWith("Hole"))
            {
                var i = int.Parse(_gripName.Split('_')[1]);
                var j = int.Parse(_gripName.Split('_')[2]);

                DrawTempLine(canvas, _slab.Holes[i], j);
            }
            else if (_gripName.StartsWith("UpSlab"))
            {
                var i = int.Parse(_gripName.Split('_')[1]);
                var j = int.Parse(_gripName.Split('_')[2]);

                DrawTempLine(canvas, _slab.UpSlabs[i].Outline, j);
            }
            else if (_gripName.StartsWith("DownSlab"))
            {
                var i = int.Parse(_gripName.Split('_')[1]);
                var j = int.Parse(_gripName.Split('_')[2]);

                DrawTempLine(canvas, _slab.DownSlabs[i].Outline, j);
            }
        }

        public void DrawTempLine(SKCanvas canvas, List<Vector2> points, int idx, SKPaint pen = null)
        {
            if (pen == null)
                pen = Constants.draggingPen;

            var bfIdx = idx - 1;
            var nxIdx = idx + 1;
            if (bfIdx < 0)
                bfIdx = points.Count - 1;
            if (nxIdx > points.Count - 1)
                nxIdx = 0;

            var sp = points[bfIdx];
            var cp = _position;
            var ep = points[nxIdx];

            var ssp = this.vportRt.ConvertWcsToScr(sp).ToSKPoint();
            var scp = this.vportRt.ConvertWcsToScr(cp).ToSKPoint();
            var sep = this.vportRt.ConvertWcsToScr(ep).ToSKPoint();
            canvas.DrawLine(ssp, scp, pen);
            canvas.DrawLine(scp, sep, pen);
        }
    }
}