﻿
namespace QdArch
{

    public enum DoorCategory
    {
        /// <summary>
        /// 平开门
        /// </summary>
        SwingDoor,
        /// <summary>
        /// 推拉门
        /// </summary>
        SlidingDoor,
        /// <summary>
        /// 卷帘门
        /// </summary>
        RollerDoor,
        /// <summary>
        /// 密闭门
        /// </summary>
        AirtightDoor,
        /// <summary>
        /// 弹簧门
        /// </summary>
        SpringDoor,
        /// <summary>
        /// 折叠门
        /// </summary>
        FoldingDoor,
        /// <summary>
        /// 其他门
        /// </summary>
        OtherDoor,
    }
    public class DoorCategoryItem:IEnumItem
    {
        public string Name => Value.ToString();
        public DoorCategory Value { get; set; }
        public string DisplayName { get; set; }
        public DoorCategoryItem(DoorCategory value, string displayName)
        {
            Value = value;
            DisplayName = displayName;
        }

        public object GetValue()
        {
            return this;
        }
    }
    public static class DoorCategoryExt
    {
        private static List<DoorCategoryItem> _itemList;
        public static List<DoorCategoryItem> ItemList
        {
            get
            {
                if (_itemList == null)
                {
                    _itemList = new List<DoorCategoryItem>();
                    var values = Enum.GetValues(typeof(DoorCategory));
                    for (var i = 0; i < values.Length; i++)
                    {
                        var value = (DoorCategory)values.GetValue(i);
                        _itemList.Add(new DoorCategoryItem(value, value.ToDisplay()));
                    }
                }
                return _itemList;
            }
        }

        public static string ToDisplay(this DoorCategory category)
        {
            switch (category)
            {
                case DoorCategory.SwingDoor: return "平开门";
                case DoorCategory.SlidingDoor: return "推拉门";
                case DoorCategory.RollerDoor: return "卷帘门";
                case DoorCategory.AirtightDoor: return "密闭门";
                case DoorCategory.SpringDoor: return "弹簧门";
                case DoorCategory.FoldingDoor: return "折叠门";
                case DoorCategory.OtherDoor: return "其他门";
                default:
                    return string.Empty;

            }
        }
    }
}
