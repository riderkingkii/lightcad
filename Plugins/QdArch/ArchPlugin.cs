﻿
using LightCAD.Drawing.Actions;

namespace QdArch
{

    public class ArchPlugin //: ILcPlugin
    {
        public void InitUI()
        {
        }

        public static void Loaded()
        {
            LcDocument.RegistElementTypes(ArchElementType.All);
            LcRuntime.RegistAssemblies.Add("QdArch");

            LcDocument.ElementActions.Add(ArchElementType.Door, new DoorAction());
            LcDocument.Element3dActions.Add(ArchElementType.Door, new Door3dAction());

            LcDocument.ElementActions.Add(ArchElementType.Window, new WindowAction());
            LcDocument.Element3dActions.Add(ArchElementType.Window, new Window3dAction());

            LcDocument.ElementActions.Add(ArchElementType.Stair, new StairAction());
            LcDocument.Element3dActions.Add(ArchElementType.Stair, new Stair3dAction());

            LcDocument.ElementActions.Add(ArchElementType.Wall, new WallAction());
            LcDocument.Element3dActions.Add(ArchElementType.Wall, new Wall3dAction());

            LcDocument.ElementActions.Add(ArchElementType.Ramp, new RampAction());
            LcDocument.Element3dActions.Add(ArchElementType.Ramp, new Ramp3dAction());

            LcDocument.ElementActions.Add(ArchElementType.Slab, new SlabAction());
            LcDocument.Element3dActions.Add(ArchElementType.Slab, new Slab3dAction());

            LcDocument.ElementActions.Add(ArchElementType.Roof, new RoofAction());
            LcDocument.Element3dActions.Add(ArchElementType.Roof, new Roof3dAction());
        }
    }
}
