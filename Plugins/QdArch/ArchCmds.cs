﻿namespace QdArch
{
    [CommandClass]
    public class ArchCmds
    {
        [CommandMethod(Name = "WALL", ShortCuts = "W")]
        public CommandResult DrawWall(IDocumentEditor docEditor, string[] args)
        {
            var wallAction = new WallAction(docEditor);
            wallAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "DOOR", ShortCuts = "D")]
        public CommandResult DrawDoor(IDocumentEditor docEditor, string[] args)
        {
            var doorAction = new DoorAction(docEditor);
            doorAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "WINDOW", ShortCuts = "WIN")]
        public CommandResult DrawWindow(IDocumentEditor docEditor, string[] args)
        {
            var windowAction = new WindowAction(docEditor);
            windowAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        //[CommandMethod(Name = "PIPE", ShortCuts = "PP")]
        //public CommandResult DrawWindPipe(IDocumentEditor docEditor, string[] args)
        //{
        //    var windowAction = new WindPipeAction(docEditor);
        //    windowAction.ExecCreate(args);
        //    return CommandResult.Succ();
        //}

        [CommandMethod(Name = "STAIR", ShortCuts = "S")]
        public CommandResult DrawStair(IDocumentEditor docEditor, string[] args)
        {
            var stairAction = new StairAction(docEditor);
            stairAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Ramp", ShortCuts = "R")]
        public CommandResult DrawRamp(IDocumentEditor docEditor, string[] args)
        {
            var rampAction = new RampAction(docEditor);
            rampAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Roof", ShortCuts = "ROF")]
        public CommandResult DrawRoof(IDocumentEditor docEditor, string[] args)
        {
            var roofAction = new RoofAction(docEditor);
            roofAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "SLAB", ShortCuts = "S")]
        public CommandResult DrawSlab(IDocumentEditor docEditor, string[] args)
        {
            var slabAction = new SlabAction(docEditor);
            slabAction.ExecCreate(args);
            return CommandResult.Succ();
        }
    }
}
