﻿namespace QdArch
{
    public static class ArchElementType
    {
        public static ElementType Wall = new ElementType
        {                           
            Guid = Guid.ParseExact("{30B6C97E-BEE2-4D92-B0AD-CB7F41027A29}", "B").ToLcGuid(),
            Name = "Wall",
            DispalyName = "墙",
            ClassType= typeof(QdWall)
        };
        public static ElementType Door = new ElementType
        {
            Guid = Guid.ParseExact("{D95B6A67-A3D3-496B-B174-E177646ACF71}", "B").ToLcGuid(),
            Name = "Door",
            DispalyName = "门",
            ClassType = typeof(QdDoorInstance)
        };
        public static ElementType Window = new ElementType
        {
            Guid = Guid.ParseExact("{6F172C09-CA0C-4CD6-9684-3666C31D3E67}", "B").ToLcGuid(),
            Name = "Window",
            DispalyName = "窗",
            ClassType = typeof(QdWindowInstance)
        };
        public static ElementType Slab = new ElementType
        {
            Guid = Guid.ParseExact("{177A324C-EB75-4B5D-89F0-B8B915632BB9}", "B").ToLcGuid(),
            Name = "Slab",
            DispalyName = "楼板",
            ClassType = typeof(QdSlab)
        };

        //public static ElementType WindPipe = new ElementType
        //{
        //    Guid = Guid.ParseExact("{5ACF25DA-A10B-4D5D-93EC-C145777B8399}", "B").ToLcGuid(),
        //    Name = "WindPipe",
        //    DispalyName = "风管",
        //    ClassType = typeof(QdWindPipe)
        //};
        public static ElementType Stair = new ElementType
        {
            Guid = Guid.ParseExact("{E6620B68-B414-564F-85EC-A60485AE8A4C}", "B").ToLcGuid(),
            Name = "Stair",
            DispalyName = "楼梯",
            ClassType = typeof(QdStairInstance)
        };
        public static ElementType Ramp = new ElementType
        {
            Guid = Guid.ParseExact("{d8f5bea4-c19a-4538-a6b3-b2017c2c74b4}", "B").ToLcGuid(),
            Name = "Ramp",
            DispalyName = "坡道",
            ClassType = typeof(QdRampRef)
        };
        public static ElementType Roof = new ElementType
        {
            Guid = Guid.ParseExact("{1d19c709-1330-4554-9901-82d7722767c9}", "B").ToLcGuid(),
            Name = "Roof",
            DispalyName = "坡道",
            ClassType = typeof(QdRampRef)
        };
        public static ElementType[] All = new ElementType[]
        {
            Wall,Door,Window,Stair,Slab,Ramp,Roof
        };
    }
}
