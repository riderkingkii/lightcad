﻿
using Avalonia.Controls;
using Constants = LightCAD.Runtime.Constants;

namespace QdArch
{
    public partial class WindowAction : ComponentInstance2dAction
    {
        private ElementInputer ElementInputer { get; set; }

        public WindowAction() { }
        public WindowAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Window");
        }

        public async void ExecCreate(string[] args = null)
        {
            //1. 选择元素
            //2. 选择门窗类型
            this.ElementInputer = new ElementInputer(this.docEditor);
        Step0:
            var result = await this.ElementInputer.Execute("选择墙体:"); 
            if (this.ElementInputer.isCancelled)
            {
                goto End;
            }
            if (result == null || result.ValueX == null || !(result.ValueX is QdWall))
            {
                goto Step0;
            }

        Step1:
            string windowName = "ThreeLine";
            var instP = (Vector2)result.Extent;
            var wall = (QdWall)result.ValueX;
            var wallLine = wall.BaseLine;
            var isLineRight = IsRight(wallLine.Start, wallLine.End, instP);

            var windowDef = ComponentManager.GetCptDef<QdWindowDef>("窗", "普通窗", QdWindowAttribute.BuiltinUuid);

            double thickness = wall.Width;
            double width = 600.0;
            double height = 1500.0;
            double bottom = 600.0;
            bool isLeft = false;
            bool isNear = isLineRight;
            var projectP = wallLine.Projection(instP);

            var window = new QdWindowInstance(thickness, width, height, bottom, isLeft, isNear, 45, true, windowDef);
            window.Initilize(this.docRt.Document);
            window.WindowDef = windowDef;
            window.Wall = wall;
            window.Position.Set(projectP.X, projectP.Y, 0);

            var wallLineDir = wallLine.Dir;

            var isRight = !(bool)window.Parameters["IsLeft"]; //是否在插入点右侧
            var isUp = !(bool)window.Parameters["IsNear"]; //是否在墙线上方，即墙线左侧

            //第三个点的旋转角度， 经观察发现三个点在半径相同的同一个圆上，圆心为doorRef.InsertPoint
            double angle = wallLineDir.Angle();
            window.Rotation.Z = angle;

            double angle45 = Math.PI / 4;
            if (isRight && isUp)
            {
                angle += angle45;
            }
            else if (!isRight && isUp)
            {
                angle += Math.PI / 2 + angle45;
            }
            else if (!isRight && !isUp)
            {
                angle += Math.PI + angle45;
            }
            else if (isRight && !isUp)
            {
                angle += Math.PI * 1.5 + angle45;
            }

            var lineDir = Vector2.RotateInRadian(wallLineDir, angle);
            var lineOffset = lineDir * (window.Width / 2);

            wall.OnPropertyChangedBefore(nameof(wall.AssociateElements), wall.AssociateElements, wall.AssociateElements);
            wall.AddLcComponent(window, nameof(QdWindowInstance), AssociateType.Cross);
            wall.OnPropertyChangedAfter(nameof(wall.AssociateElements), wall.AssociateElements, wall.AssociateElements);
            this.DrawWindow(window);

        End:
            this.ElementInputer = null;
            this.EndCreating();
        }

        public override void Draw(SKCanvas canvas, LcElement element, Vector2 offset)
        {
            var window = element as QdWindowInstance;
            //var wallLine = window.Wall.BaseLine;
            var curves = window.Curves[0].Curve2ds;

            var pen = this.GetDrawPen(element);
            if (pen == Constants.defaultPen)
            {
                //pen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true };
                pen = new SKPaint { Color = SKColors.Orange, IsStroke = true, /*PathEffect = Constants.SelectedEffect*/ };
            }

            DrawWindowCurves(canvas, curves, pen);
        }


        public void DrawWindow(QdWindowInstance window)
        {
            this.docRt.Document.ModelSpace.InsertElement(window);
            this.docRt.Action.ClearSelects();
        }

        #region Grip
        private QdWindowInstance _window;
        private string _gripName;
        private Vector2 _position;

        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var windowRef = element as QdWindowInstance;
            var grips = new List<ControlGrip>();

            var gripCenter = new ControlGrip
            {
                Element = windowRef,
                Name = "Center",
                Position = windowRef.Position.ToVector2()
            };
            grips.Add(gripCenter);

            //var wallLine = windowRef.Wall.BaseLine;
            var gripLeft = new ControlGrip
            {
                Element = windowRef,
                Name = "Left",
                Position = windowRef.StartPoint
            };
            grips.Add(gripLeft);

            var gripRight = new ControlGrip
            {
                Element = windowRef,
                Name = "Right",
                Position = windowRef.EndPoint
            };
            grips.Add(gripRight);

            
            //var gripOpenDir = new ControlGrip
            //{
            //    Element = windowRef,
            //    Name = "Control",
            //    Position = windowRef.ControlPoint
            //};
            //grips.Add(gripOpenDir);

            return grips.ToArray();
        }

        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            var window = element as QdWindowInstance;
            _window = window;
            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {

                if (gripName == "Center")
                {
                    window.OnPropertyChangedBefore("Position", null, null);
                    position = window.Wall.BaseLine.Projection(position);
                    var offset = position - window.Position.ToVector2();
                    window.Position.Set(position.X, position.Y, 0);
                    window.ResetCache();

                    window.Wall.OnPropertyChangedBefore(nameof(window.Wall.AssociateElements), window.Wall.AssociateElements, window.Wall.AssociateElements);
                    window.Wall.UpdateLcComponent(window, nameof(QdWindowInstance), AssociateType.Cross);
                    window.Wall.OnPropertyChangedAfter(nameof(window.Wall.AssociateElements), window.Wall.AssociateElements, window.Wall.AssociateElements);
                    window.OnPropertyChangedAfter("Position", null, null);

                }
                else if (gripName == "Right")
                {
                    window.OnPropertyChangedBefore("Position", null, null);
                    position = window.Wall.BaseLine.Projection(_position);
                    var offset = (_position - window.EndPoint) / 2;

                    window.Position.Add(offset.X, offset.Y, 0);
                    window.EndPoint.Set(_position.X, _position.Y);
                    window.Width = Vector2.Distance(window.EndPoint, window.StartPoint);
                    window.ResetCache();

                    window.Wall.OnPropertyChangedBefore(nameof(window.Wall.AssociateElements), window.Wall.AssociateElements, window.Wall.AssociateElements);
                    window.Wall.UpdateLcComponent(window, nameof(QdWindowInstance), AssociateType.Cross);
                    window.Wall.OnPropertyChangedAfter(nameof(window.Wall.AssociateElements), window.Wall.AssociateElements, window.Wall.AssociateElements);
                    window.OnPropertyChangedAfter("Position", null, null);
                }
                else if (gripName == "Left")
                {
                    window.OnPropertyChangedBefore("Position", null, null);
                    position = window.Wall.BaseLine.Projection(_position);
                    var offset = (_position - window.StartPoint) / 2;

                    window.Position.Add(offset.X, offset.Y, 0);
                    window.StartPoint.Set(_position.X, _position.Y);
                    window.Width = Vector2.Distance(window.EndPoint, window.StartPoint);
                    window.ResetCache();

                    window.Wall.OnPropertyChangedBefore(nameof(window.Wall.AssociateElements), window.Wall.AssociateElements, window.Wall.AssociateElements);
                    window.Wall.UpdateLcComponent(window, nameof(QdWindowInstance), AssociateType.Cross);
                    window.Wall.OnPropertyChangedAfter(nameof(window.Wall.AssociateElements), window.Wall.AssociateElements, window.Wall.AssociateElements);

                    window.OnPropertyChangedAfter("Position", null, null);
                }
            }

        }

        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_window == null)
                return;

            var window = new QdWindowInstance(_window.WindowDef);
            window.Copy((QdWindowInstance)_window);
            var wallLine = window.Wall.BaseLine;
            if (_gripName == "Center")
            {
                _position = window.Wall.BaseLine.Projection(_position);
                //var offset = _position - window.Position.ToVector2();
                window.Position.Set(_position.X, _position.Y, 0);
            }
            else if (_gripName == "Right")
            {
                _position = window.Wall.BaseLine.Projection(_position);
                var offset = (_position - window.EndPoint) / 2;

                window.Position.Add(offset.X, offset.Y, 0);
                window.EndPoint.Set(_position.X, _position.Y);
                window.Width = Vector2.Distance(window.EndPoint, window.Position.ToVector2()) * 2;
                window.ResetCache();
            }
            else if (_gripName == "Left")
            {
                _position = window.Wall.BaseLine.Projection(_position);
                var offset = (_position - window.StartPoint) / 2;

                window.Position.Add(offset.X, offset.Y, 0);
                window.StartPoint.Set(_position.X, _position.Y);
                window.Width = Vector2.Distance(window.StartPoint, window.Position.ToVector2()) * 2;
                window.ResetCache();
            }

            var curves = window.Curves[0].Curve2ds;
            DrawWindowCurves(canvas, curves, Constants.draggingPen);
        }
        #endregion

        public void DrawWindowCurves(SKCanvas canvas, List<Curve2d> curves, SKPaint pen)
        {

            foreach (var curve in curves)
            {
                switch (curve.Type)
                {
                    case Curve2dType.Line2d:
                        {
                            var line = curve as Line2d;
                            var wsp = line.Start;
                            var wep = line.End;

                            var ssp = this.vportRt.ConvertWcsToScr(wsp).ToSKPoint();
                            var sep = this.vportRt.ConvertWcsToScr(wep).ToSKPoint();
                            canvas.DrawLine(ssp, sep, pen);
                            break;
                        }
                    case Curve2dType.Arc2d:
                        {
                            var arc = curve as Arc2d;

                            var wcenter = arc.Center;
                            var scenter = this.vportRt.ConvertWcsToScr(wcenter).ToSKPoint();

                            var wrect = new Box2(wcenter.X - arc.Radius, wcenter.Y - arc.Radius, wcenter.X + arc.Radius, wcenter.Y + arc.Radius);
                            var plt = this.vportRt.ConvertWcsToScr(wrect.LeftTop);
                            var prb = this.vportRt.ConvertWcsToScr(wrect.RightBottom);
                            var scrRect = new SKRect((float)plt.X, (float)plt.Y, (float)prb.X, (float)prb.Y);
                            double startAngle = Utils.RadianToDegree(arc.StartAngle); //绘制弧线使用的是角度不是弧度
                            double endAngle = Utils.RadianToDegree(arc.EndAngle); //绘制弧线使用的是角度不是弧度
                            var sweep = endAngle - startAngle;
                            //Skia绘制圆弧是按照顺时针的，与数学逻辑相反
                            canvas.DrawArc(scrRect, -(float)(Utils.RadianToDegree(arc.EndAngle)), (float)sweep, false, pen);
                            break;
                        }
                    case Curve2dType.Polyline2d:
                        {
                            var pline = curve as Polyline2d;
                            var scrPoints = pline.Points.Select((p) => p.ToSKPoint()).ToArray();
                            canvas.DrawPoints(SKPointMode.Polygon, scrPoints, pen);
                            break;
                        }
                    case Curve2dType.Polygon2d:
                        {
                            var polygon = curve as Polygon2d;
                            var scrPoints = polygon.Points.Select((p) => p.ToSKPoint()).ToList();
                            scrPoints.Add(scrPoints[0]);
                            canvas.DrawPoints(SKPointMode.Polygon, scrPoints.ToArray(), pen);
                            break;
                        }
                    default:
                        break;
                }
            }
        }

        public bool IsRight(Vector2 start, Vector2 end, Vector2 p)
        {
            //Vsub(D,A,B) D.x=A.x-B.x; D.y=A.y-B.y
            Vector2 v1 = start - end;
            Vector2 v2 = p - start;

            double f = v1.X * v2.Y - v2.X * v1.Y;
            return f >= 0;
        }
        public override SnapPointResult SnapPoint(SnapRuntime snapRt, LcElement element, Vector2 point, bool forRef, Vector2 PrePoint = null)
        {

            var maxDistance = vportRt.GetSnapMaxDistance();
            QdWindowInstance window = element as QdWindowInstance;
            var sscur = SnapSettings.Current;
            var result = new SnapPointResult { Element = element };
            Curve2d line2d = new Line2d(window.StartPoint, window.EndPoint);
            var line = new LcLine(window.StartPoint, window.EndPoint);
            if (sscur.ObjectOn)
            {

                if (sscur.PointType.Has(SnapPointType.Endpoint))
                {
                    if ((point - line.Start).Length() <= maxDistance)
                    {
                        result.Point = line.Start;
                        result.Name = "Start";
                        result.Curves.Add(new SnapRefCurve(SnapPointType.Endpoint, line2d));
                    }
                    else if ((point - line.End).Length() <= maxDistance)
                    {
                        result.Point = line.End;
                        result.Name = "End";
                        result.Curves.Add(new SnapRefCurve(SnapPointType.Endpoint, line2d));
                    }
                }
                if (sscur.PointType.Has(SnapPointType.Midpoint) && result.Point == null)
                {
                    var cp = (line.Start + line.End) / 2;
                    if ((point - cp).Length() <= maxDistance)
                    {
                        result.Point = cp;
                        result.Name = "Mid";
                        result.Curves.Add(new SnapRefCurve(SnapPointType.Midpoint, line2d));
                    }
                }
                //forRef时只捕捉端点，中点等，如果
                if (!forRef && result.Point == null)
                {
                    var refPoints = snapRt.GetRefPoints(element);
                    var lineLength = (line.End - line.Start).Length();
                    var distance = GeoUtils.PointToLineDistance(line.Start, line.End, point, out Vector2 nearest, out double dirDistance);
                    if (sscur.PointType.Has(SnapPointType.Nearest))
                    {
                        //最近点必须在线段内进行测试
                        if (distance < maxDistance && dirDistance > 0 && dirDistance < lineLength)
                        {
                            result.Point = nearest;
                            result.Name = "Nearest";
                            result.Curves.Add(new SnapRefCurve(SnapPointType.Nearest, line2d));
                        }
                    }
                    //进行延长线上点捕捉，需要元素上有参考点
                    if (sscur.PointType.Has(SnapPointType.ExtensionLine) && refPoints.Count > 0 && result.Point == null)
                    {
                        if (distance < maxDistance)
                        {
                            result.Point = nearest;
                            result.Name = "Extension";
                            result.Curves.Add(new SnapRefCurve(SnapPointType.ExtensionLine, line2d));
                        }
                    }
                }
            }

            if (result.Point != null)
                return result;
            else
                return null;
        }
        public override List<PropertyObserver> GetPropertyObservers()
        {
            //return base.GetPropertyObservers();
            return new List<PropertyObserver>() {
             new PropertyObserver()
            {
                Name = "Width",
                DisplayName = "宽度",
                CategoryName = "Geometry",
                CategoryDisplayName = "几何图形",
                PropType=PropertyType.Double,
                Getter = (ele) => (ele as QdWindowInstance).Parameters.GetValue<double>("Width"),
                Setter = (ele, value) =>
                {
                    var window = (ele as QdWindowInstance);
                    if (!double.TryParse(value.ToString(),out var width)||width<0)
                        return;
                    //window.Wall.OnPropertyChangedBefore("Window",null,null);
                    window.OnPropertyChangedBefore("Width",window.Parameters.GetValue<double>("Width"),width);
                    window.Width=width;
                    var wallLine = window.Wall.BaseLine;
                    var wallLineDir = wallLine.Dir;
                    var offset = wallLineDir * (window.Width / 2);
                    window.ResetCache();
                    window.Wall.UpdateLcComponent(window,nameof(QdWindowInstance),AssociateType.Cross);
                    window.OnPropertyChangedAfter("Width",window.Parameters.GetValue<double>("Width"),width);
                    //window.Wall.OnPropertyChangedAfter("Window",null,null);

                }
            },
              new PropertyObserver()
            {
                Name = "Height",
                DisplayName = "高度",
                CategoryName = "Geometry",
                CategoryDisplayName = "几何图形",
                PropType=PropertyType.Double,
                Getter = (ele) => (ele as QdWindowInstance).Parameters.GetValue<double>("Height"),
                Setter = (ele, value) =>
                {
                    var window = (ele as QdWindowInstance);
                    if (!double.TryParse(value.ToString(),out var height)||height<0)
                        return;
                    window.OnPropertyChangedBefore("Height",window.Parameters.GetValue<double>("Height"),height);
                    window.Parameters.SetValue("Height",height );
                    window.ResetCache();
                    window.OnPropertyChangedAfter("Height",window.Parameters.GetValue<double>("Height"),height);
                }
            },
              new PropertyObserver()
            {
                Name = "Bottom",
                DisplayName = "底高度",
                CategoryName = "Geometry",
                CategoryDisplayName = "几何图形",
                PropType=PropertyType.Double,
                Getter = (ele) => (ele as QdWindowInstance).Parameters.GetValue<double>("Bottom"),
                Setter = (ele, value) =>
                {
                    var window = (ele as QdWindowInstance);
                    if (!double.TryParse(value.ToString(),out var bottom)||bottom<0)
                        return;
                    window.OnPropertyChangedBefore("Bottom",window.Parameters.GetValue<double>("Bottom"),bottom);
                    window.Parameters.SetValue("Bottom",bottom );
                    window.ResetCache();
                    window.OnPropertyChangedAfter("Bottom",window.Parameters.GetValue<double>("Bottom"),bottom);

                }
            }
            };
        }
    }
}
