﻿namespace QdArch
{
 

    public enum WindowCategory
    {
        /// <summary>
        /// 一般窗
        /// </summary>
        NormalWindow,
    }
    public class WindowCategoryItem: IEnumItem
    {
        public string Name => Value.ToString();
        public WindowCategory Value { get; set; }
        public string DisplayName { get; set; }
        public WindowCategoryItem(WindowCategory value, string displayName)
        {
            Value = value;
            DisplayName = displayName;
        }

        public object GetValue()
        {
            return this;
        }
    }
    public static class WindowCategoryExt
    {
        private static List<WindowCategoryItem> _itemList;
        public static List<WindowCategoryItem> ItemList
        {
            get
            {
                if (_itemList == null)
                {
                    _itemList = new List<WindowCategoryItem>();
                    var values = Enum.GetValues(typeof(WindowCategory));
                    for (var i = 0; i < values.Length; i++)
                    {
                        var value = (WindowCategory)values.GetValue(i);
                        _itemList.Add(new WindowCategoryItem(value, value.ToDisplay()));
                    }
                }
                return _itemList;
            }
        }

        public static string ToDisplay(this WindowCategory category)
        {
            switch (category)
            {
                case WindowCategory.NormalWindow: return "一般窗";
                default:
                    return string.Empty;

            }
        }
    }
}
