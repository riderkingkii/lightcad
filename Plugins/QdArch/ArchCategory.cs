﻿namespace QdArch
{
    public enum ArchCategory
    {
        Unknown,
        Wall,
        Door,
        Window,
        Stair
    }
    public interface IEnumItem
    {
         string Name { get; }
        object GetValue();
         string DisplayName { get; }
    }
    public class ArchCategoryItem: IEnumItem
    {
        public string Name => Value.ToString();
        public ArchCategory Value { get; set; }
        public string DisplayName { get; set; }
        public ArchCategoryItem(ArchCategory value, string displayName)
        {
            Value = value;
            DisplayName = displayName;
        }

        public object GetValue()
        {
            return this;
        }
    }
    public static class ArchCategoryExt
    {
        private static List<ArchCategoryItem> _itemList;
        public static List<ArchCategoryItem> ItemList
        {
            get
            {
                if (_itemList == null)
                {
                    _itemList = new List<ArchCategoryItem>();
                    var values=Enum.GetValues(typeof(ArchCategory));
                    for(var i=0; i< values.Length; i++)
                    {
                        var value = (ArchCategory)values.GetValue(i);
                        _itemList.Add(new ArchCategoryItem(value, value.ToDisplay()));
                    }
                }
                return _itemList;
            }
        }
        
        public static string ToDisplay(this ArchCategory category)
        {
            switch (category)
            {
                case ArchCategory.Wall: return "墙";
                case ArchCategory.Door: return "门";
                case ArchCategory.Window: return "窗";
                case ArchCategory.Stair: return "楼梯";
                case ArchCategory.Unknown: return "未知";
                default:
                    return string.Empty;
            }
        }
    }
}
