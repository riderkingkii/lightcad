﻿using LightCAD.Runtime;
using System;
using System.Windows.Forms;
//using LightCAD.UI;

namespace WinFormPlugin;
public class DemoPlugin : ILcPlugin
{
    public DemoPlugin()
    {

    }

    public void Loaded()
    {
        AppRuntime.UIInitilized += AppRuntime_UIInitilized;
        //Console.WriteLine("打印...");
        //var pwin = new PrintWindow();
        //pwin.Show();

    }

    private void AppRuntime_UIInitilized(object? sender, EventArgs e)
    {
        var form = new TestForm();
        form.Show();
    }

    public void ItemClick()
    {
    }

    public void InitUI()
    {
        //var sbar = UIManager.Instance.SideBar;
        //sbar.AddItem(new SideBarItem
        //{
        //    Title = "A",
        //    IsEnabled = true,
        //    Content = new SideBarContent()
        //});
        //sbar.AddItem(new SideBarItem
        //{
        //    Title = "B",
        //    IsEnabled = true,
        //    Click = ItemClick
        //});
        //sbar.AddItem(new SideBarItem
        //{
        //    Title = "C",
        //    IsEnabled = false,
        //});
    }
}

