﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    public interface ILightCADControls
    {
        public abstract void Enable(bool enabled);

        public abstract void EnableTranslate(bool enabled);

        public abstract void EnableRotate(bool enabled);

        public abstract void EnableScale(bool enabled);


        public abstract void AddEventListener(string type, Three.EventHandler listener);

        public void Dispose();

        public void Update();

        public void ClearState();

        public abstract void Focus(Vector3 center, Vector3 normal, Box3 boundingBox);

        public void Clone(ILightCADControls source);
    }
}
