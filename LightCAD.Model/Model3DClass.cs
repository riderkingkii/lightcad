﻿
using System.Collections;

namespace LightCAD.Model
{
    public class InstancedMeshManager
    {
        Dictionary<string, Dictionary<LcParameterSet, InstancedMesh>> InstancedMeshes;
        public InstancedMeshManager(Scene scene)
        {

        }
    }
    public class BufferMeshPoolManager
    {
        public const string DefaultGroupName = "BufferMeshGroup";
        private Scene scene;

        private Dictionary<Group,List<BufferMesh>> meshes = new Dictionary<Group, List<BufferMesh>> ();
        private Dictionary<Group, List<BufferMesh>> transMeshes = new Dictionary<Group, List<BufferMesh>>();
        public BufferMesh Get(int vertexCount,bool isTransparent)
        {
            var group = scene.getObjectByName(DefaultGroupName) as Group;
            if (group == null)
            {
                group = new Group();
                group.name = DefaultGroupName;
                scene.add(group);
            }
            return Get(group, vertexCount, isTransparent);
        }
        public BufferMesh Get(Group group, int vertexCount, bool isTransparent)
        {
            return null;
        }
        public BufferMeshPoolManager(Scene scene)
        {
            this.scene = scene;
        }
    }
    public class BufferMesh : Mesh
    {
        private List<BufferItem> BufferItems = new List<BufferItem>();

        public int BufferSize { get; set; }
        /// <summary>
        /// 已经使用的缓冲大小，与FreeBufferIndex 一起作为是否需要RebufferItems的依据
        /// </summary>
        public int UsedBufferSize { get; set; }
        /// <summary>
        /// 自由缓冲索引，随着Add增加，Remove最后一个会变动
        /// </summary>
        public int FreeBufferIndex { get;private  set; }
        public readonly bool HasUV;
        public readonly bool HasPackColor;
        public readonly bool IsPack;
        public BufferMesh()
        {
            this.Init();
        }
        private void Init() 
        {
            this.geometry.attributes.position = new BufferAttribute(new double[BufferSize * 3], 3);
            if(this.HasUV)
            this.geometry.attributes.uv = new BufferAttribute(new byte[BufferSize * 3], 3);
             if(this.HasPackColor)
            this.geometry.attributes.packColor = new BufferAttribute(new float[BufferSize], 1);
            this.geometry.attributes.normal = new BufferAttribute(new sbyte[BufferSize * 3], 3);
           
        }

        public void RebufferItems()
        {

        }
        public void AddBufferItem(BufferItem item) 
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void RemoveBufferItem(BufferItem item) 
        {

        }
        /// <summary>
        /// 如果Count不变，直接更新，如果变化，则先Remove，再添加
        /// </summary>
        /// <param name="item"></param>
        public void UpdateBufferItem(BufferItem item)
        {
        }

    }
    /// <summary>
    /// 统一管理
    /// </summary>
    public class BufferItem
    {
        public IElement3d Element { get; set; }
        public int Start { get; set; }
        public int Count { get; set; }
        public object Tag { get; set; }

    }

}
