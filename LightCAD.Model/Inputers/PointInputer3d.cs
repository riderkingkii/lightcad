﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    public class PointInputer3d : Inputer3d
    {
        private Vector3 inputp;
        private string option;
        private string prompt;
        public bool isOnlyPoint = true;
        public Snap3dRuntime SnapRuntime;
        public PointInputer3d(IDocumentEditor docEditor) : base(docEditor)
        {
            this.SnapRuntime = this.modelEditRt.Snap3DRuntime;
        }
        public async Task<InputResult3d> Execute(string prompt, SnapFilterType snapFilter = SnapFilterType.EdgeAndMesh)
        {
            inputp = null;
            option = null;
            this.prompt = prompt + (isOnlyPoint?"鼠标选取/输入坐标 格式 0,1,1":"");
            this.commandCtrl.SetAlternateCommandState(false);
            this.Reset();
            this.AttachEvents();
            Prompt(this.prompt);
            this.SnapRuntime.StartSnaping(snapFilter);
            await WaitFinish();
            this.SnapRuntime.EndSnaping();
            this.DetachEvents();
            this.commandCtrl.SetAlternateCommandState(true);
            //this.commandCtrl.WriteInfo(string.Empty);
            if (this.isCancelled)
                return new InputResult3d { IsCancelled = true };
            else
                return new InputResult3d(inputp, option) { };
        }
        protected override void OnRuntimeMouseDown(MouseEventRuntime e)
        {
            if (!this.isInputed && e.Button == LcMouseButtons.Left)
            {
                this.inputp = this.SnapRuntime.SnapPoint;
                this.commandCtrl.WriteInfo(this.commandCtrl.GetPrompt());
                if (this.inputp == null)
                    return;
                this.Finish();
            }
        }
        protected override void OnInputBoxKeyDown(KeyEventRuntime e)
        {
            base.OnInputBoxKeyDown(e);
            if (this.isInputed || e.KeyCode != 13)
                return;
            var text = this.commandCtrl.GetInputText();
            if (!string.IsNullOrEmpty(text))
            {
                text = text.Replace('，', ',');
                var strArr = text.Split(',');
                bool formatCorrect = true;
                if (strArr.Length == 3)
                {
                    double[] val = new double[3];
                    for (int i = 0; i < strArr.Length; i++)
                    {
                        if (double.TryParse(strArr[i], out double result))
                            val[i] = result;
                        else
                            formatCorrect = false;
                    }
                    this.inputp = new Vector3().FromArray(val);
                    this.commandCtrl.WriteInfo(this.commandCtrl.GetPrompt());
                    this.Finish();
                }else
                {
                    this.option = text;
                    this.commandCtrl.WriteInfo(this.commandCtrl.GetPrompt());
                    this.commandCtrl.SetInputText("");
                    this.Finish();
                }
                //else
                //    formatCorrect = false;
                //if (!formatCorrect)
                //{
                //    this.commandCtrl.WriteInfo("输入坐标格式错误!");
                //    return;
                //}
            }
        }

    }
}
