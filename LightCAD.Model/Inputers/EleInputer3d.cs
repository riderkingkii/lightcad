﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    public class EleInputer3d : Inputer3d
    {
        private ListEx<IElement3d> input = null;
        private string option = null;
        public EleInputer3d(IDocumentEditor docEditor) : base(docEditor)
        {

        }
        public async Task<InputResult3d> Execute(string prompt)
        {
            this.commandCtrl.SetAlternateCommandState(false);
            this.Reset();
            this.AttachEvents();
            Prompt(prompt);
            this.docRt.Action.SelectsChanged += Action_SelectsChanged;
            await WaitFinish();
            this.docRt.Action.SelectsChanged -= Action_SelectsChanged;
            this.DetachEvents();
            this.commandCtrl.SetAlternateCommandState(true);
            if (this.isCancelled)
                return null;
            else
                return new InputResult3d(input, "") { };
        }
        private void Action_SelectsChanged(IDocumentEditor sender, SelectedEventArgs args)
        {
            if (args.Selected)
            {
                input = args.Elements.Where(e => e is IElement3d).Cast<IElement3d>().ToListEx();
                this.Finish();
            }
        }
    }
}
