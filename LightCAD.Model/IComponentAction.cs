﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    public interface IComponentAction
    {
        List<Object3D> Render(IComponentInstance cptIns);
        void SetDocEditor(IDocumentEditor docEditor);
    }
}
