﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LightCAD.MathLib.MathUtil;
namespace LightCAD.Model
{
    public static class VectorUtil
    {
        public static readonly Vector3 XAxis = new Vector3(1, 0, 0);
        public static readonly Vector3 YAxis = new Vector3(0, 1, 0);
        public static readonly Vector3 ZAxis = new Vector3(0, 0, 1);
        public static bool Vec2EQ(Vector2 a, Vector2 b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a.X - b.X) < eps) && (Math.Abs(a.Y - b.Y) < eps);
        }
        public static bool Vec2NegateEQ(Vector2 a, Vector2 b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a.X + b.X) < eps) && (Math.Abs(a.Y + b.Y) < eps);
        }

        public static bool Vec3EQ(Vector3 a, Vector3 b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a.X - b.X) < eps) && (Math.Abs(a.Y - b.Y) < eps) && (Math.Abs(a.Z - b.Z) < eps);
        }
        public static bool Vec3NegateEQ(Vector3 a, Vector3 b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a.X + b.X) < eps) && (Math.Abs(a.Y + b.Y) < eps) && (Math.Abs(a.Z + b.Z) < eps);
        }

        public static bool IsParallel(Vector3 dir1, Vector3 dir2,double eps= EPS)
        {
            return Vec3EQ(dir1, dir2, eps) || Vec3EQ(dir1, dir2.Clone().Negate(), eps);
        }
        public static bool IsParallel(Vector2 dir1, Vector2 dir2, double eps = EPS)
        {
            return Vec2EQ(dir1, dir2, eps) || Vec2EQ(dir1, dir2.Clone().Negate(), eps);
        }
    }
}
