﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    public static  class LCThreeExt
    {
        //public static Vector2 ToThree(this Vector2 v2d, Vector2 target=null)
        //{
        //    return target?.Set(v2d.X, v2d.Y) ?? new Vector2(v2d.X, v2d.Y);
        //}
        public static Vector3 ToThree(this Vector2 v2d, double z, Vector3 target = null)
        {
            return target?.Set(v2d.X, v2d.Y, z) ?? new Vector3(v2d.X, v2d.Y, z);
        }
    }
}
