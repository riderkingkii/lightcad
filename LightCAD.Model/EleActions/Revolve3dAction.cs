﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    public class Revolve3dAction : Solid3dAction
    {
        public Revolve3dAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Revolve");
        }
        public async void ExecCreate(string[] args = null)
        {
            var mats = new LcMaterial[1]
            {
                new LcMaterial() { Color = new Color(0xff0000) },
                //new LcMaterial() { Color = new Color(0x00ff00) }
            };
            var outerLoop = new List<Curve3d>()
            {
                new Line3d(new Vector3(100, 0, 0), new Vector3(500, 0, 0)),
                new Line3d(new Vector3(500, 0, 0), new Vector3(500, 0, 700)),
                new Line3d(new Vector3(500, 0, 700), new Vector3(100, 0, 700)),
                new Line3d(new Vector3(100, 0, 700), new Vector3(100, 0, 0))
            };
            var profile = new PlanarSurface3d(new Plane().SetFromCoplanarPoints(
                                                    outerLoop[0].Start,
                                                    outerLoop[1].Start,
                                                    outerLoop[2].Start),
                                              outerLoop);

            var revolve = new LcRevolve(profile, new Vector3(), new Vector3(0, 0, 1), 0, Math.PI * 1.5, true, mats);
            revolve.Initilize(this.docRt.Document);
            revolve.Rt3DAction = this;
            this.docRt.Document.ModelSpace.InsertElement(revolve);
        }
    }
}
