﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    public class ComponentEditRuntime : Doc3dEditRuntime
    {
        private OrbitControls controls;

        public ComponentEditRuntime(DocumentRuntime docRt,IComponentEditorContorl control,ICommandControl commandControl):base(docRt,control,commandControl)
        {
            this.EditorType = EditorType.Component;
        }

        public override void EnableCameraControl(bool enable)
        {
            
        }
    }
}
