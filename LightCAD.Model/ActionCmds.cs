﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Model
{
    [CommandClass]
    public  class ActionCmds 
    {
        #region CreateElement3d
        [CommandMethod(Name = "Cone", ShortCuts = "CO", Category = "Model")]
        public CommandResult Cone(IDocumentEditor docEditor, string[] args)
        {
            var coneAction = new Cone3dAction(docEditor);
            coneAction.ExecCreate(args);
            return CommandResult.Succ();
        }
      
        [CommandMethod(Name = "Cylinder", ShortCuts = "CY", Category = "Model")]
        public CommandResult Cylinder(IDocumentEditor docEditor, string[] args)
        {
            var cylinderAction = new Cylinder3dAction(docEditor);
            cylinderAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "Prism", ShortCuts = "Pr", Category = "Model")]
        public CommandResult Prism(IDocumentEditor docEditor, string[] args)
        {
            var prismAction = new Prism3dAction(docEditor);
            prismAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "Sphere", ShortCuts = "Sp", Category = "Model")]
        public CommandResult Sphere(IDocumentEditor docEditor, string[] args)
        {
            var sphereAction = new Sphere3dAction(docEditor);
            sphereAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Ellipsoid", ShortCuts = "ELL", Category = "Model")]
        public CommandResult Ellipsoid(IDocumentEditor docEditor, string[] args)
        {
            var ellipsoidAction = new Ellipsoid3dAction(docEditor);
            ellipsoidAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Revolve", ShortCuts = "RL", Category = "Model")]
        public CommandResult Revolve(IDocumentEditor docEditor, string[] args)
        {
            //var revolveAction = new Revolve3dAction(docEditor);
            //revolveAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        #endregion

        #region Transform
        [CommandMethod(Name = "Move", ShortCuts = "MV", Category = "Model")]
        public CommandResult Move(IDocumentEditor docEditor, string[] args)
        {
            var transformAction = new TransformAction(docEditor);
            transformAction.MoveTransform(args);
            return CommandResult.Succ();
        }
        #endregion

        #region Editor
        [CommandMethod(Name = "PushPull", ShortCuts = "PP", Category = "Model")]
        public CommandResult PushPull(IDocumentEditor docEditor, string[] args)
        {
            var editorAction = new EditorAction(docEditor);
            editorAction.PushPull(args);
            return CommandResult.Succ();
        }
        #endregion

    }
}
