﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public static class ElementStatusExt
    {
        public static void ClearStatus(this LcElement element)
        {
            element.RtStatus = ElementStatus.Normal;
        }

        public static bool InStatus(this LcElement element, ElementStatus status)
        {
            return (element.RtStatus & status) == status;
        }

        public static bool IsSelected(this LcElement element)
        {
            return (element.RtStatus & ElementStatus.Selected) == ElementStatus.Selected;
        }
        public static void IsSelected(this LcElement element, bool selected)
        {
            if(selected)
                element.RtStatus |= ElementStatus.Selected;
            else
                element.RtStatus &= ~ElementStatus.Selected;
        }

        public static bool IsHovered(this LcElement element)
        {
            return (element.RtStatus & ElementStatus.Hovered) == ElementStatus.Hovered;
        }
        public static void IsHovered(this LcElement element, bool hovered)
        {
            if (hovered)
                element.RtStatus |= ElementStatus.Hovered;
            else
                element.RtStatus &= ~ElementStatus.Hovered;
        }

        public static bool IsDragging(this LcElement element)
        {
            return (element.RtStatus & ElementStatus.Dragging) == ElementStatus.Dragging;
        }
        public static void IsDragging(this LcElement element, bool dragging)
        {
            if (dragging)
                element.RtStatus |= ElementStatus.Dragging;
            else
                element.RtStatus &= ~ElementStatus.Dragging;
        }

        public static bool IsInputSelected(this LcElement element)
        {
            return (element.RtStatus & ElementStatus.InputSelected) == ElementStatus.InputSelected;
        }
        public static void IsInputSelected(this LcElement element, bool inputSelected)
        {
            if (inputSelected)
                element.RtStatus |= ElementStatus.InputSelected;
            else
                element.RtStatus &= ~ElementStatus.InputSelected;
        }


      

    }
}
