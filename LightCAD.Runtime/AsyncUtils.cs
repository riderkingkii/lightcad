﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    using System.Diagnostics;

    public static class AsyncUtils
    {
        /// <summary>
        /// 异步执行Action
        /// </summary>
        /// <param name="action">回调事件</param>
        /// <param name="delay">延迟时间，毫秒</param>
        public static Task Execute(Action action, int delay = 1)
        {
            return Task.Run(
                void () =>
                    {
                        Thread.Sleep(delay);
                        try
                        {
                            action();
                        }
                        catch (Exception ex)
                        {
                            Debug.Assert(false);
                            // TODO: Handle the System.Exception
                        }
                    });
        }
    }
}