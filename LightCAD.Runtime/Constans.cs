﻿using LightCAD.Core;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public static class Constants
    {
        public const string DefaultDrawingName = "图纸{0}.ldwg";

        public static SKColor Background = new SKColor(0xff212830);

        public static SKColor SelectRectFillColor_Included = new SKColor(0x80003B93);
        public static SKColor SelectRectFillColor_Crossed = new SKColor(0x80008217);
        public static float MouseMove_HoverTimeSpan = 0.5f;//悬停判别时间
        public static SKColor DrawEditor_Background = new SKColor(0xff212830);


        public static SKColor GridColor = new SKColor(0xff343A4B);
        public static SKColor SubGridColor = new SKColor(0xff282E3A);
        public static float GridSize = 5f;
        public static float GridRatio = 5;

        public static SKColor OriginAxisColor = new SKColor(0xffffffff);
        public static SKColor AxisYColor = new SKColor(0xff30ff30);
        public static SKColor AxisXColor = new SKColor(0xffff3030);


        public static double ZoomRatio = 0.8;

        public static float OriginLength = 100.0f;
        public static float OriginSize = 12.0f;


        public static int CursorAddonSize = 6;

        public static float CursorCrossSize = 100;

        public static float SelectBoxSize = 12;



        public static SKPaint SilverPen = new SKPaint { Color = SKColors.Gray, IsStroke = true };
        public static SKPathEffect SelectedEffect = SKPathEffect.CreateDash(new float[] { 3, 3 }, 10);
        public static SKPaint selectedPen = new SKPaint { Color = SKColors.White, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 3, 3 }, 10) };
        public static SKPaint defaultPen = new SKPaint { Color = new SKColor(0xFFE8E8E8), IsStroke = true };
        public static SKPaint draggingPen = new SKPaint { Color = SKColors.Gray, IsStroke = true };
        public static SKPaint disabledPen = new SKPaint { Color = SKColors.DarkGray, IsStroke = true };
        
        public static SKPaint auxOrangeDashPen = new SKPaint { Color = SKColors.Orange, IsStroke = true, PathEffect = Constants.SelectedEffect };
        public static SKPaint auxElementPen = new SKPaint { Color = SKColors.Silver, IsStroke = true };
        public static SKPaint auxCurvePen = new SKPaint { Color = SKColors.Silver, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 1, 1 }, 10) };

        public static SKPaint dottedlinePen = new SKPaint { Color = SKColors.Green, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 1, 1 }, 10) };
        public const float GripSize = 4;
        public static SKColor GripColor = SKColors.Blue;
        public static SKColor HoveredColor = SKColors.Red;
        public static SKColor SelectedColor = SKColors.Red;


        public static int HoverDelayTime = 500;//悬停延迟时间，毫秒

    }
}
