﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Runtime
{
    public enum PointerButtons
    {
        None = 0,
        LeftPressed = 1,
        RightPressed = 2,
        MiddlePressed = 4
    }




    public class PointerStatus
    {
        public Vector2 PressedPosition { get; set; }
        public Vector2 MovedPosition { get; set; }
        public Vector2 ReleasedPosition { get; set; }
        public Vector2 WheelPosition { get; set; }
        public Vector2 WheelDelta { get; set; }
        public PointerButtons Buttons { get; set; }

       
    }
}
