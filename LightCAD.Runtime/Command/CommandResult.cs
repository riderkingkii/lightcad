﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public class CommandResult
    {
        public static CommandResult Succ(object data = null)
        {
            return  new CommandResult {  Success = true, Data = data };
        }
        public static CommandResult GetError(string error, object data = null)
        {
            return new CommandResult { Success = false, Error = error, Data = data };
        }
        public static CommandResult GetException(Exception exception, object data = null)
        {
            return new CommandResult { Success = false, Execption = exception, Data = data };
        }

        public bool? Success { get; set; }
        public Exception Execption { get; set; }
        public string Error { get; set; }
        public object Data { get; set; }

        public bool IsExecuted => Success != null;
    }
}
