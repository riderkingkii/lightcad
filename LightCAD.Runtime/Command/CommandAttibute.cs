﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CommandClassAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class CommandMethodAttribute : Attribute
    {
        public CommandMethodAttribute(string name = null, string descr = null, string category = null, string hotKey = null, string shortCuts = null)
        {
            this.Name = name;
            this.Descr = descr;
            this.Category = category;
            this.HotKey = hotKey;
            this.ShortCuts = shortCuts;
        }

        public string Name { get; set; }
        public string Descr { get; set; }
        public string Category { get; set; }
        public string HotKey { get; set; }
        public string ShortCuts { get; set; }
    }
}