﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public class CommandExecuteEventArgs : EventArgs
    {
        public LcCommand Command { get; set; }  
        public CommandResult Result { get; set; }   
    }
    public delegate void CommandExecuteEventHandler(object sender, CommandExecuteEventArgs commandExecuteEventArgs);

}
