﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public class LcCommandEntry
    {
        public Assembly Assembly { get; set; }
        public Type Type { get; set; }
        public MethodInfo Method { get; set; }
        public Object Instance { get; set; }

        public LcCommandEntry(Assembly assembly, Type type, MethodInfo method)
        {
            Assembly = assembly;
            Type = type;
            Method = method;
        }   
    }

    /// <summary>
    /// 命令
    /// </summary>
    public class LcCommand
    {
        public string Name;
        public string Description;
        public string Category;
        public string HotKey; //热键
        public List<string> ShortCuts = new List<string>(); //TODO 改成单个ShortCut
        public LcCommandEntry Entry; //命令的触发入口
        public string[] Args;
    }
}
