﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public enum DocumentEditType
    {
        Drawing,
        Model,
        ModelView
    }
}
