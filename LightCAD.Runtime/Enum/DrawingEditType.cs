﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public enum DrawingEditorType
    {
        Drawing,
        Paper,
        Model3D,
        View3D
    }
}
