﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public enum LcCursorType
    {
        None,
        SelectElement,//十字加选择范围正方形
        InputPoint,//十字
        InputElement//选择范围正方形
    }
}
