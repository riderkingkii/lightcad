﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    [Flags]
    public enum EditorType
    {
        None,
        Drawing,
        Paper,
        Model3d,
        Component,
        View3d
    }
}
