﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public enum DisplayType
    {
        /// <summary>
        /// 内嵌显示
        /// </summary>
        Embed = 0,
        /// <summary>
        /// 浮动窗口显示
        /// </summary>
        Float = 1
    }
}
