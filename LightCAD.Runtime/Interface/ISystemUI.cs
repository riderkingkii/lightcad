﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IUISystem
    {
        string Name { get; }
        Task<string[]?> OpenFileDialog(bool allowMultiple = false);

        Task<string> SaveFileDialog(string initialFileName = null, string initialFilter = null, string initialDirectory = null);

        object NewClipboardDataObject();
        void SetClipboardData(object dataObject, string format, object value);
        void SetToClipboard(object dataObject);
        Task<object> GetClipboardData(string fromat);

        IWindow CreateWindow(string winName, params object[] args);
        void ShowWindow(IWindow  window, IWindow owner=null);
        LcDialogResult ShowDialog(IWindow window, IWindow owner=null);
        void ShowMessageBox(string message);
        void ShowException(Exception ex);
    }
}
