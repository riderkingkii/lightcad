﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface ICommandInfoControl
    {

    }

    public interface ICommandInfoObject
    {
        bool IsShow { get; set; }
        void Show(int scrLeft,int scrTop);
        void Hide();
        void SetPosition(int scrLeft, int scrTop);
        void SetValidCommonds(List<string> cmds);
        void Up();
        void Down();
        void AttachExecuteAction(Action<string> action);
        string GetCurrentCmd();

    }
}
