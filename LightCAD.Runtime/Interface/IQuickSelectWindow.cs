﻿namespace LightCAD.Runtime.Interface
{
    public interface IQuickSelectWindow : IWindow
    {
        string CurrentAction { get; set; }
    }
}
