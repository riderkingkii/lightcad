﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IHistoryCmdControl
    {

    }

    public interface IHistoryCmdObject
    {
        bool IsShow { get; set; }
        void Show(int scrLeft,int scrTop);
        void Hide();
        void SetPosition(int scrLeft, int scrTop);
        void SetValidCommonds(List<string> cmds);
        void Up();
        void Down();
        void AttachExecuteAction(Action<string> action);
        string GetCurrentCmd();

    }
}
