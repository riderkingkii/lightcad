﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface ICommandControl
    {
        void AttachInputEvent(Action<KeyEventRuntime> onInputKeyDown, Action<KeyEventRuntime> onInputKeyUp,Action<ICommandControl,EventArgs> onInputGotFocus, Action<ICommandControl, EventArgs> onInputLossFocus);
        void DetachInputEvent(Action<KeyEventRuntime> onInputKeyDown, Action<KeyEventRuntime> onInputKeyUp, Action<ICommandControl, EventArgs> onInputGotFocus, Action<ICommandControl, EventArgs> onInputLossFocus);
        /// <summary>
        /// 获取输入文本内容
        /// </summary>
        /// <returns></returns>
        string GetInputText();
        /// <summary>
        /// 设置输入文本内容
        /// </summary>
        /// <param name="str"></param>
        void SetInputText(string str);
        /// <summary>
        /// 命令结果
        /// </summary>
        /// <param name="info"></param>
        void WriteInfo(string info);
        string GetAllInfos();
        /// <summary>
        /// 设置命令提示
        /// </summary>
        /// <param name="prompt"></param>
        void Prompt(string prompt);
        /// <summary>
        /// 获取命令
        /// </summary>
        /// <param name="prompt"></param>
        string GetPrompt();

        /// <summary>
        /// 缓存所有的命令
        /// </summary>
        /// <param name="allCmds"></param>
        void SetAllCommonds(List<string> allCmds);

        /// <summary>
        /// 设置是否启用备选命令区
        /// </summary>
        /// <param name="enabled"></param>
        void SetAlternateCommandState(bool enabled);

        void AddHistoryCmd(string cmd);

        void SetCurMethod(LcCreateMethod method);
        void SetCurStep(LcCreateStep step);
    }
}
