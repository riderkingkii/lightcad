﻿namespace LightCAD.Runtime.Interface
{
    public interface ITableWindow : IWindow
    {
        string CurrentAction { get; set; }
    }
}
