﻿using LightCAD.Core;
using LightCAD.Three;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;
using System.Data;

namespace LightCAD.Runtime
{
    public interface IAxisLineManageFrom : IWindow
    {
        string CurrentAction { get; set; }
        double Radius { get; set; }
        string AxisNumType { get; set; }
        string FirstAxisNum { get; set; }
        string OrderingRule { get; set; }
        LcAxisGrid lcAxisGrid { get; set; }
    }
}
