﻿using LightCAD.Core;
using LightCAD.MathLib;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface ICreateDrawer
    {
        /// <summary>
        /// 创建实体时绘制辅助线,如绘制直线时，鼠标移动时的辅助线
        /// Aux(auxiliary)
        /// </summary>
        /// <param name="canvas"></param>
        void DrawAuxLines(SKCanvas canvas);

        /// <summary>
        /// 绘制创建时的临时元素
        /// </summary>
        /// <param name="canvas"></param>
        void DrawTemp(SKCanvas canvas);
    }
}
