﻿using LightCAD.Core;
using System;


namespace LightCAD.Runtime
{
    public interface IGLControl
    {
        Bounds EditorBounds { get; }
        void RefreshControl();

        //void ShowHoverTip(object content);
        void AttachEvents(Action onInit, Action onDestory, Action<string, MouseEventRuntime> mouseEvent,
            Action<KeyEventRuntime> keyDown,
             Action<KeyEventRuntime> keyUp,
            Action<Bounds> sizeChanged,
            Action onRender);
        void DetachEvents(Action onInit, Action onDestory, Action<string, MouseEventRuntime> mouseEvent,
            Action<KeyEventRuntime> keyDown,
            Action<KeyEventRuntime> keyUp,
            Action<Bounds> sizeChanged,
            Action onRender);
    }
    public interface IDocument3dEditorControl : IDocumentEidtorControl, IGLControl
    {
        void LoadDoc(LcDocument doc);
    }
}
