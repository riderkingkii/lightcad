﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IRefEditSelector : IWindow
    {
        public LcBlockRef Source { get; set; }
        public LcBlockRef[] RefLinks { get; set; } 
        public LcBlockRef Target { get; set; }
    }
}
