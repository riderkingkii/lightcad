﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface ICmdLogControl
    {

    }

    public interface ICmdLogObject
    {
        bool IsShow { get; set; }
        void Show(int scrLeft,int scrTop);
        void Hide();
        void SetPosition(int scrLeft, int scrTop);
        void SetLog(string log);
        string GetLog();
    }
}
