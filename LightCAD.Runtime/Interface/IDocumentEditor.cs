﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IDocumentEditor
    {
        string Name { get; }
        DocumentRuntime DocRt { get; }
        EditorType EditorType { get; }
        CommandCenter CommandCenter { get;}
        void CommandExecute(LcCommand cmd);
        void SetActive(bool active);
        void CancelAll();
        void SelectAll();
    }
}
