﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Runtime
{
    public interface ISectionWindow : IWindow
    {
        string CurrentAction { get; set; } 
        string SectionName { get; set; }
        string SectionId { get; set; } 

    }
}
