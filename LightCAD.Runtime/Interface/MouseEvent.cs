﻿using LightCAD.Core;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    [Flags]
    public enum LcMouseButtons
    {
        /// <summary>
        ///  No mouse button was pressed.
        /// </summary>
        None = 0,

        /// <summary>
        ///  The left mouse button was pressed.
        /// </summary>
        Left = 1,

 
        /// <summary>
        ///  The right mouse button was pressed.
        /// </summary>
        Right = 2,

        /// <summary>
        ///  The middle mouse button was pressed.
        /// </summary>
        Middle = 4,
    }
    public class MouseEventRuntime
    {
        public LcKeyModifiers KeyModifiers { get; set; }

        /// <summary>
        ///  Gets which mouse button was pressed.
        /// </summary>
        public LcMouseButtons Button { get; set; }

        /// <summary>
        ///  Gets the number of times the mouse button was pressed and released.
        /// </summary>
        public int Clicks { get; set; }

        /// <summary>
        ///  Gets the x-coordinate of a mouse click.
        /// </summary>
        public float X { get; set; }

        /// <summary>
        ///  Gets the y-coordinate of a mouse click.
        /// </summary>
        public float Y { get; set; }

        /// <summary>
        ///  Gets a signed count of the number of detents the mouse wheel has rotated.
        /// </summary>
        public float DeltaY { get; set; }

        public float DeltaX { get; set; }

        /// <summary>
        ///  Gets the location of the mouse during MouseEvent.
        /// </summary>
        public SKPoint Location => new SKPoint(X, Y);
    }
}
