﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Runtime
{
    public interface IBlockDefWindow:IWindow
    {
        string CurrentAction { get; set; }
        Vector2 BasePoint { get; set; }
        List<LcElement> Elements { get; set; }
        string BlockName { get; set; }
    }
}
