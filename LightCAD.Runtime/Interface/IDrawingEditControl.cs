﻿using LightCAD.Core;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IDrawingEditControl:IViewportControl,IDocumentEidtorControl
    {
        void RefreshViewport();
        void SetLocation(string point);
        LcViewport ViewPort { get;}

    }
}
