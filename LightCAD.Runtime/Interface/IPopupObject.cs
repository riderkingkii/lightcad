﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IPopupControl
    {
        void SetInfo(string info);
    }
    public interface IPopupObject
    {
        IPopupControl Control { get; }
        void Show(int scrLeft,int scrTop);
        void Hide();
        void SetPosition(int scrLeft, int scrTop);
    }
}
