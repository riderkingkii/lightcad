﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface ILoftDefWindow : IWindow
    {
        string CurrentAction { get; set; }
        string Name { get; set; }
        string Uuid { get; set; }
    }
}
