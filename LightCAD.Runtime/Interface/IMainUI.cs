﻿using LightCAD.Core;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IMainUI
    {
        bool IsActive { get; }
        bool IsMinimized { get;  }
        SKPoint Location { get; }
        IDocumentEidtorControl DocumentCtrl { get; }
        DocumentRuntime NewDocument();
        Task<DocumentRuntime> OpenDocument(string filePath=null);
        void CloseDocument();

        DocumentRuntime OpenDrawingTab(LcDocument document);

    }
}
