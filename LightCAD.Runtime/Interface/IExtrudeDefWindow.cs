﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Runtime
{
    public interface IExtrudeDefWindow:IWindow
    {
        string CurrentAction { get; set; } 
        string Name { get; set; }
        string Uuid{ get; set; }
        double TopScale { get; set; }

        double BottomScale { get; set; }

    }
}
