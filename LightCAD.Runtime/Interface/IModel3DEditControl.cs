﻿using LightCAD.Core;
using LightCAD.Runtime.Interface;
using LightCAD.Three;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IModel3DEditControl: IDocument3dEditorControl
    {
        event Action<List<LcLevel>> OnLevelFilterChanged;
    }
}
