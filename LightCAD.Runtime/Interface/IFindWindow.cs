﻿namespace LightCAD.Runtime.Interface
{
    public interface IFindWindow : IWindow
    {
        string CurrentAction { get; set; }
    }
}
