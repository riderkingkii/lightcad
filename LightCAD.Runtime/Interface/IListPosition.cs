﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IListPosition
    {
        /// <summary>
        /// Get the last object in the collection and changes the current index equal to the size-1 of the collection.
        /// </summary>
        /// <returns>The last object of the collection.</returns>
        object Last();
        /// <summary>
        /// Get the next object of the collection and also increases the current index of the collection.
        /// </summary>
        /// <returns>The next object of the collection.</returns>
        object Next();
        /// <summary>
        /// Get the previus object of the collection and also decreases the current index of the collection.
        /// </summary>
        /// <returns>The previus object of the collection.</returns>
        /// <remarks>If the collection reaches the end returns null.</remarks>
        object Previous();
        /// <summary>
        /// Set the current idex of the collection.
        /// </summary>
        /// <param name="index">An integer value representing the zero based index of the collection(less than the collection's size).</param>
        void SetCurrentIndex(int index);
        /// <summary>
        /// Change the current position of the collection to the index of a given object.
        /// </summary>
        /// <param name="entity">An existing object in the collection.</param>
        /// <returns>True if the object was found and the index was changed.</returns>
        /// <remarks>If the collection reaches the start returns null.</remarks>
        bool SetPosition(object entity);
        /// <summary>
        /// Get the first object in the collection and changes the current index to 0.
        /// </summary>
        /// <returns>The first object of the collection.</returns>
        object Start();

        /// <summary>
        /// Get the current object in the collection.
        /// </summary>
        object Current { get; }

        /// <summary>
        /// Get the current index of the collection.
        /// </summary>
        int CurrentIndex { get; }
    }
}
