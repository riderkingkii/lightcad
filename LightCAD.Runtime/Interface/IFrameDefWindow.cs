﻿using LightCAD.Core;
using LightCAD.Three;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Runtime
{
    public interface IFrameDefWindow : IWindow
    {
        string CurrentAction { get; set; }
        Box2 Box { get; set; }
        Vector2 BasePoint { get; set; }
        List<LcElement> Elements { get; set; }
        LcBuilding LcBuilding { get; set; }
        List<LcLevel> LcLevels { get; set; }
        string FrameName { get; set; }
        Dictionary<string, object> FrameProperties { get; set; }
    }
}
