﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IWindow
    {
        bool IsActive { get;  }
        Type WinType { get; }
        bool Visible { get; set; }

        /// <summary>
        /// 在UI线程上执行的方法
        /// </summary>
        /// <param name="method"></param>
        void Invoke(Action method);
    }
}
