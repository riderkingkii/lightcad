﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LightCAD.Core;

namespace LightCAD.Runtime
{
    /// <summary>
    /// 属性控件接口
    /// </summary>
    public interface IPropertyControl
    {
        /// <summary>
        ///  选择集变化时会触发此事件, 用于更新属性面板
        /// #ToDo 请补充完成此注释
        /// </summary>
        /// <param name="grpEles"> 字典, </param>
        /// <param name="eleObservers"> </param>
        /// <param name="eleTypeObservers"></param>
        /// <param name="mergeAll"></param>
        /// <param name="propertyChangedAction"></param>
        void SelectsChanged(Dictionary<ElementType, List<LcElement>> grpEles, List<PropertyObserver> eleObservers, Dictionary<ElementType, List<PropertyObserver>> eleTypeObservers, List<PropertyObserver> mergeAll, Action<List<LcElement>> propertyChangedAction);
        void PropertyChanged(Dictionary<ElementType, List<LcElement>> grpEles, List<PropertyObserver> eleObservers, Dictionary<ElementType, List<PropertyObserver>> eleTypeObservers, List<PropertyObserver> mergeAll, Action<List<LcElement>> propertyChangedAction);
    }
}