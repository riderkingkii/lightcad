﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime.Interface
{
    public interface IBuildingWindow : IWindow
    {
        string CurrentAction { get; set; }
        LcBuilding lcBuilding { get; set; }
    }
}
