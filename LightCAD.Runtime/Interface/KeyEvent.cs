﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{

    public class KeyEventRuntime
    {
         public int KeyCode { get; set; }
         public LcKeyModifiers KeyModifiers { get; set; }

        
    }
}
