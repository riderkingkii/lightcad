﻿using LightCAD.Core;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IViewportControl
    {
        string Guid { get;  }
        long ViewportId { get; }
        bool HoverEnabled { get; set; }
        bool IsActive { get; }
        bool ForceRefresh { get; set; }
        void Refresh();
        void ShowHoverTip(object content);
        void AttachEvents(Action<string, MouseEventRuntime> mouseEvent, 
            Action<KeyEventRuntime> keyDown,
             Action<KeyEventRuntime> keyUp,
            Action<Bounds> sizeChanged, 
            Action<SKSurface> skRender);
        void DetachEvents(Action<string, MouseEventRuntime> mouseEvent,
            Action<KeyEventRuntime> keyDown,
            Action<KeyEventRuntime> keyUp, 
            Action<Bounds> sizeChanged, 
            Action<SKSurface> skRender);
        void PopupContextMenu(Action closedAction);
        void ShowInfo(string type,object info);
        IPopupObject GetPopupObject(string type);
        void Invoke(Action method);
        SKPoint PointToScreen(SKPoint point);
        /// <summary>
        /// 获取输入文本内容
        /// </summary>
        /// <returns></returns>
        string GetInputText();
        /// <summary>
        /// 设置输入文本内容
        /// </summary>
        /// <param name="str"></param>
        void SetInputText(string str);
        void HideInputText();
 
        void ShowInputText();
    }
}
