﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{



    [Flags]
    public enum LcKeyStates
    {
        None = 0,
        Down = 1,
        Toggled = 2,
    }
}
