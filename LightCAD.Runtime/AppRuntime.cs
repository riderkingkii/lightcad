﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public class AppInitilizedEventArgs:EventArgs
    {
        public string[] Args;
    }
    public sealed class AppRuntime
    {
        public static bool IsClosing;

        public static event EventHandler UIInitilized;

        public static void InitUiSystem(IUISystem uiSystem)
        {
            UISystem = uiSystem;
        }
        public static void Initilize(IApp app,IMainUI mainUI, string[] args)
        {
            App = app;
            ActiveMainUI = mainUI;
            //判断命令行，然后打开图纸
            UIInitilized?.Invoke(app, new AppInitilizedEventArgs { Args = args });
        }

        public static IApp App { get; internal set; }
        public static IUISystem UISystem { get; internal set; }

        public static IMainUI ActiveMainUI {get; internal set; }
       

    }
}
