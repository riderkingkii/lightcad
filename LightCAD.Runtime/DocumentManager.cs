﻿using LightCAD.Core;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;
using System.Reflection.Metadata;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace LightCAD.Runtime
{
    public class DocumentManager
    {

        public static string DefaultDrawingName = "图纸";
        public static int DrawingIndex = 1;
        public static List<LcDocument> Documents = new List<LcDocument>();
        public static Dictionary<LcDocument, DocumentRecorder> Recorders = new Dictionary<LcDocument, DocumentRecorder>();
        public static LcDocument Current { get; private set; }
        public static DocumentRecorder CurrentRecorder { get; private set; }

        public static string GetAutoName()
        {
            return $"{DefaultDrawingName}{DrawingIndex}.ldwg";
        }

        /// <summary>
        /// 新建一个文档
        /// </summary>
        /// <returns></returns>
        public static DocumentRuntime New()
        {
            //由于新建文档过程涉及很多UI操作，因此把实现主体放在UI层
            var docRt = AppRuntime.ActiveMainUI.NewDocument();
            SetCurrent(docRt.Document);
            DocumentManager.CurrentRecorder.AttachChangeEvents();
            return docRt;
        }

        public async static Task<DocumentRuntime> Open(string filePath)
        {
            //由于打开文档过程涉及很多UI操作，因此把实现主体放在UI层
            var docRt = await AppRuntime.ActiveMainUI.OpenDocument(filePath);
            SetCurrent(docRt.Document);
            return docRt;
        }
        /// <summary>
        /// 保存文档
        /// </summary>
        /// <param name="docRt"></param>
        public static void Save(DocumentRuntime docRt)
        {
            //Save方法涉及UI较少
            DocumentUtils.Save(docRt.Document);
            docRt.ChangeNotSaved = false;//TODO:这里存在通知 
        }

        public static void SetCurrent(LcDocument document)
        {
            if (!Documents.Contains(document))
            {
                Documents.Add(document);
            }
            Current=document;
            //LcDocument.SetCurrent(document);

            if (Recorders.ContainsKey(document))
            {
                CurrentRecorder = Recorders[document];
            }
            else
            {
                CurrentRecorder = new DocumentRecorder(document);
                Recorders.Add(document, CurrentRecorder);
            }
        }


        public static List<DocumentRuntime> DocumentRts = new List<DocumentRuntime>();
        public static DocumentRuntime ActiveRt { get; internal set; }

        public static DocumentRuntime CreateRt(LcDocument doc, bool isActive = false)
        {
            var docRt = new DocumentRuntime(doc);
            DocumentRts.Add(docRt);
            if (isActive) SetActiveRt(docRt);
            return docRt;
        }
        public static void SetActiveRt(DocumentRuntime docRt)
        {
            ActiveRt = docRt;
        }
        public static bool RemoveRt(DocumentRuntime docRt)
        {
            return DocumentRts.Remove(docRt);
        }
    }
}
