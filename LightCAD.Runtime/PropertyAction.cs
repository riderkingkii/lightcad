﻿using LightCAD.Core;
using LightCAD.Three;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public interface IPropertyObserver 
    {
        public List<PropertyObserver> GetPropertyObservers();
       
    }
    public class PropertyAction
    {
        public static List<PropertyObserver> _sharedPropertyObservers;
        /// <summary>
        /// 返回共享的属性观察者(监视器)
        /// </summary>
        /// <returns></returns>
        public static List<PropertyObserver> GetSharedPropertyObservers(DocumentAction docAction)
        {
            //if (_sharedPropertyObservers != null)
            //{
            //    return _sharedPropertyObservers;
            //}
            var colors = new Dictionary<string, string>
            {
                { "ByLayer", "ByLayer" },
                { "ByBlock", "ByBlock" },
                { "红", "4294901760" },
                { "黄", "4294967040" },
                { "绿", "4278255360" },
                { "青", "4278255615" },
                { "蓝", "4278190335" },
                { "洋红", "4294902015" },
                { "白", "4294967295" },
            };
            var layers = docAction.docRt.Document.Layers.Select(l => l.Name).ToArray();
            var linetypes = docAction.docRt.Document.LineTypes.Select(lt => lt.LineTypeName).ToArray();

            _sharedPropertyObservers = new List<PropertyObserver>()
            {
                new PropertyObserver()
                {
                    Name = "Color",
                    DisplayName = "颜色",
                    CategoryName = "General",
                    CategoryDisplayName = "常规",
                    PropType = PropertyType.Array,
                    Source = (ele) => colors.Keys.ToArray(),
                    Getter = (ele) => colors.FirstOrDefault(c => c.Value == ele.Color).Key,
                    Setter = (ele, value) => { ele.Color = colors[(string)value]; }
                },
                new PropertyObserver()
                {
                    Name = "Layer",
                    DisplayName = "图层",
                    CategoryName = "General",
                    CategoryDisplayName = "常规",
                    PropType = PropertyType.Array,
                    Source = (ele) => layers,
                    Getter = (ele) => ele.Layer,
                    Setter = (ele, value) => { ele.Layer = (string)value; }
                },
                new PropertyObserver()
                {
                    Name = "LineType",
                    DisplayName = "线型",
                    CategoryName = "General",
                    CategoryDisplayName = "常规",
                    PropType = PropertyType.Array,
                    Source = (ele) => linetypes,
                    Getter = (ele) => ele.LineType,
                    Setter = (ele, value) => { ele.LineType = (string)value; }
                },
                new PropertyObserver()
                {
                    Name = "LineTypeScale",
                    DisplayName = "线型比例",
                    CategoryName = "General",
                    CategoryDisplayName = "常规",
                    Getter = (ele) => ele.LineTypeScale,
                    Setter = (ele, value) => { ele.LineTypeScale = (double)value; }
                },
                new PropertyObserver()
                {
                    Name = "LineWeight",
                    DisplayName = "线宽",
                    CategoryName = "General",
                    CategoryDisplayName = "常规",
                    Getter = (ele) => ele.LineWeight,
                    Setter = (ele, value) => { ele.LineWeight = (string)value; }
                },
                new PropertyObserver()
                {
                    Name = "PlotStyleName",
                    DisplayName = "打印样式",
                    CategoryName = "General",
                    CategoryDisplayName = "常规",
                    Getter = (ele) => ele.PlotStyleName,
                    Setter = (ele, value) => { ele.PlotStyleName = (string)value; }
                },
                new PropertyObserver()
                {
                    Name = "Transparency",
                    DisplayName = "透明度",
                    CategoryName = "General",
                    CategoryDisplayName = "常规",
                    Getter = (ele) => ele.Transparency,
                    Setter = (ele, value) => { ele.Transparency = (string)value; }
                }
            };
            return _sharedPropertyObservers;
        }
    }
}
