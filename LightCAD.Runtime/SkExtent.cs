﻿using LightCAD.Core;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Runtime
{
    public static class SkExtent
    {
        public static SKPoint ToSKPoint(this Vector2 point)
        {
            return new SKPoint((float)point.X, (float)point.Y);
        }
        public static Vector2 ToVector2d(this SKPoint point)
        {
            return new Vector2(point.X, point.Y);
        }
        public static SKRect ToSKRect(this Box2 box)
        {
            return new SKRect( (float)box.LeftTop.X, (float)box.LeftTop.Y, (float)box.RightBottom.X, (float)box.RightBottom.Y);
        }
        public static Box2 ToBox2d(this SKRect rect)
        {
            return new Box2().SetFromPoints(new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
        }

    }
}
