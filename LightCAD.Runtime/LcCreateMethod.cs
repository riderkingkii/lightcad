﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Runtime
{
    public class LcCreateStep
    {
        public string Name { get; set; }

        public string Options { get; set; }
    }

    public class LcCreateMethod
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public LcCreateStep[] Steps { get; set; }
    }
}