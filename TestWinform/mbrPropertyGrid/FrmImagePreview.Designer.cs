﻿namespace mbrPropertyGrid
{
    partial class FrmImagePreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImagePreview));
            this.btnViewPic = new System.Windows.Forms.Button();
            this.buttonsList = new System.Windows.Forms.ImageList(this.components);
            this.toolTipHelp = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // btnViewPic
            // 
            this.btnViewPic.ImageIndex = 0;
            this.btnViewPic.ImageList = this.buttonsList;
            this.btnViewPic.Location = new System.Drawing.Point(4, 4);
            this.btnViewPic.Name = "btnViewPic";
            this.btnViewPic.Size = new System.Drawing.Size(25, 21);
            this.btnViewPic.TabIndex = 1;
            this.toolTipHelp.SetToolTip(this.btnViewPic, "Change view mode (Zoom > Center > Stretch)");
            this.btnViewPic.UseVisualStyleBackColor = true;
            this.btnViewPic.Click += new System.EventHandler(this.btnViewPic_Click);
            // 
            // buttonsList
            // 
            this.buttonsList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("buttonsList.ImageStream")));
            this.buttonsList.TransparentColor = System.Drawing.Color.Red;
            this.buttonsList.Images.SetKeyName(0, "Button_ViewMode_16x15_RedMask.PNG");
            // 
            // FrmImagePreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(152, 94);
            this.Controls.Add(this.btnViewPic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(160, 120);
            this.Name = "FrmImagePreview";
            this.Opacity = 0.9;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Image preview";
            this.toolTipHelp.SetToolTip(this, "Double click to open image...");
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmImagePreview_Paint);
            this.DoubleClick += new System.EventHandler(this.FrmImagePreview_DoubleClick);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnViewPic;
        private System.Windows.Forms.ImageList buttonsList;
        private System.Windows.Forms.ToolTip toolTipHelp;
    }
}