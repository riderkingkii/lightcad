﻿
 10/08/2008
 ==========

 * Part of the open source project mbrPropertyGrid
 * Developer : mbr ® (Massimiliano Brugnerotto)
 
 Add here any new PropertyItem class you want to add to the
 PropertyGrid user control. The new class must inherit from
 PropertyItemGeneric or another class that inherit from it.
 Some addition code will be necessary in the PropertyGrid.cs module.
 
 