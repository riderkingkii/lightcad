﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
/*
 * 11/08/2008
 * 
 * Part of the open source project mbrPropertyGrid
 * Developer : mbr ® (Massimiliano Brugnerotto)
 *  
 */
namespace mbrPropertyGrid
{
    public partial class FrmImagePreview : Form
    {

        #region Private internal var./properties

        private enum ButtonStatus : int
        {
            Normal = 0,
            MouseOver = 1,
            Pressed = 2
        }

        private string mImageFileName = "";
        private int imgLayout = 0;

        #endregion

        /// <summary>
        /// Form constructor.
        /// </summary>
        public FrmImagePreview()
        {
            InitializeComponent();
        }


        #region Private internal function

        /// <summary>
        /// Execute an external application (.exe program).
        /// Return true if the application start successfully.
        /// </summary>
        private bool StartApp(Process app, string fname,
                                           string workDir,
                                           ProcessWindowStyle windowStyle,
                                           string processName,
                                           string arguments)
        {
            ProcessStartInfo startInfo;
            string msg = "";

            // Start a new task (.exe application)
            try
            {
                // Start the process
                startInfo = new ProcessStartInfo(fname);
                startInfo.WorkingDirectory = workDir;
                startInfo.WindowStyle = windowStyle;
                startInfo.Arguments = arguments;
                if (app == null) app = new Process();
                app.StartInfo = startInfo;
                app.Start();
                app.WaitForInputIdle(2000);
                return true;
            }
            catch (Exception err)
            {   
                // Error!
                msg = err.Message;
                return false;
            }
        }

        /// <summary>
        /// Draw a string with shadow (simple 3D)
        /// </summary>
        private void Draw3DString(Graphics g, string s, Font font, Brush foregroundBrush, Brush backgroundBrush, float x, float y)
        {
            // Shadow
            g.DrawString(s, font, backgroundBrush, x + 1, y);
            g.DrawString(s, font, backgroundBrush, x + 1, y + 1);
            g.DrawString(s, font, backgroundBrush, x, y + 1);
            // Text
            g.DrawString(s, font, foregroundBrush, x, y);
        }

        /// <summary>
        /// Print picture info like width and height size (pixel)
        /// </summary>
        private void DrawPictureInfo()
        {
            Graphics g;
            Brush brush;
            Brush backBrush;
            Font font;

            g = this.CreateGraphics();
            font = new Font("Arial", 8.0F, FontStyle.Regular);
            brush = new SolidBrush(Color.White);
            backBrush = new SolidBrush(Color.Black);
            if (this.BackgroundImage == null)
            {
                btnViewPic.Visible = false;
                Draw3DString(g, "(no image loaded)", font, brush, backBrush, 4, 8);
            }
            else
            {
                btnViewPic.Visible = true;
                Draw3DString(g, "[" + this.BackgroundImage.Width.ToString("0") + " , " +
                                       this.BackgroundImage.Height.ToString("0") + "] " + this.BackgroundImageLayout.ToString(),
                                       font, brush, backBrush, 32, 8);
            }
            g.Dispose();
        }

        /// <summary>
        /// Change view mode ( Zoom > Center > Stretch )
        /// </summary>
        private void ChangeViewMode()
        {
            if (this.BackgroundImage == null) return;
            imgLayout++;
            if (imgLayout == 3) imgLayout = 0;
            switch (imgLayout)
            {
                case 0:
                    this.BackgroundImageLayout = ImageLayout.Zoom;
                    break;
                case 1:
                    this.BackgroundImageLayout = ImageLayout.Center;
                    break;
                case 2:
                    this.BackgroundImageLayout = ImageLayout.Stretch;
                    break;
            }
            DrawPictureInfo();
        }

        /// <summary>
        /// Open image using external application (eg.MsPaint)
        /// </summary>
        private void OpenImageUsingExternalApplication()
        {
            Process app = new Process();
            string tempFile = "";
            string tempName = "";

            if (this.BackgroundImage == null) return;
            // If file exist open it.
            // If file do not exist create a temporary .png file and open it.
            try
            {
                if (System.IO.File.Exists(this.ImageFileName))
                    StartApp(app, "explorer.exe", "", ProcessWindowStyle.Maximized, "explorer.exe", this.ImageFileName);
                else
                {
                    tempFile = System.IO.Path.GetTempPath();
                    if (!tempFile.EndsWith(@"\"))
                        tempFile += @"\";
                    tempName = System.IO.Path.GetTempFileName();
                    tempName = System.IO.Path.GetFileNameWithoutExtension(tempName);
                    tempFile += tempName + ".png";
                    this.BackgroundImage.Save(tempFile, System.Drawing.Imaging.ImageFormat.Png);
                    StartApp(app, "explorer.exe", "", ProcessWindowStyle.Maximized, "explorer.exe", tempFile);
                }
            }
            catch
            {
                // Error!
            }
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Gets/sets the image file name. The image file name is used to edit the picture with an external application.
        /// To show in the preview form a picture use the .BackgroundImage property.
        /// </summary>
        public string ImageFileName
        {
            get
            {
                return mImageFileName;
            }
            set
            {
                mImageFileName = value;
            }
        }

        #endregion

        #region Events generated using form and controls

        private void btnViewPic_Click(object sender, EventArgs e)
        {
            ChangeViewMode();
        }

        private void FrmImagePreview_Paint(object sender, PaintEventArgs e)
        {
            DrawPictureInfo();
        }

        private void FrmImagePreview_DoubleClick(object sender, EventArgs e)
        {
            OpenImageUsingExternalApplication();
        }

        #endregion

    }
}
