﻿namespace TestWinform
{
    partial class SkiaSharpTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            splitContainer1 = new System.Windows.Forms.SplitContainer();
            skControl1 = new SkiaSharp.Views.Desktop.SKControl();
            skGL = new SkiaSharp.Views.Desktop.SKGLControl();
            panel1 = new System.Windows.Forms.Panel();
            button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            splitContainer1.Location = new System.Drawing.Point(0, 67);
            splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(skControl1);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(skGL);
            splitContainer1.Size = new System.Drawing.Size(1400, 603);
            splitContainer1.SplitterDistance = 25;
            splitContainer1.TabIndex = 0;
            // 
            // skControl1
            // 
            skControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            skControl1.Location = new System.Drawing.Point(0, 0);
            skControl1.Name = "skControl1";
            skControl1.Size = new System.Drawing.Size(25, 603);
            skControl1.TabIndex = 0;
            skControl1.Text = "skControl1";
            // 
            // skGL
            // 
            skGL.BackColor = System.Drawing.Color.Black;
            skGL.Dock = System.Windows.Forms.DockStyle.Fill;
            skGL.Location = new System.Drawing.Point(0, 0);
            skGL.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            skGL.Name = "skGL";
            skGL.Size = new System.Drawing.Size(1371, 603);
            skGL.TabIndex = 0;
            skGL.VSync = true;
            // 
            // panel1
            // 
            panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel1.Controls.Add(button1);
            panel1.Dock = System.Windows.Forms.DockStyle.Top;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(1400, 67);
            panel1.TabIndex = 0;
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(565, 31);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(94, 29);
            button1.TabIndex = 0;
            button1.Text = "更新四边形";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // SkiaSharpTest
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1400, 670);
            Controls.Add(splitContainer1);
            Controls.Add(panel1);
            Name = "SkiaSharpTest";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "SkiaSharpTest";
            WindowState = System.Windows.Forms.FormWindowState.Maximized;
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            panel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private SkiaSharp.Views.Desktop.SKControl skControl1;
        private SkiaSharp.Views.Desktop.SKGLControl skGL;
        private System.Windows.Forms.Button button1;
    }
}