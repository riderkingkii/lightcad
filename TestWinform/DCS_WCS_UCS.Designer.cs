﻿using System.Drawing;

namespace TestWinform
{
    partial class DCS_WCS_UCS
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            rdoScreen = new System.Windows.Forms.RadioButton();
            rdoWorld = new System.Windows.Forms.RadioButton();
            rdoUser = new System.Windows.Forms.RadioButton();
            panel1 = new System.Windows.Forms.Panel();
            btnUcsView = new System.Windows.Forms.Button();
            btnWcsView = new System.Windows.Forms.Button();
            btnDraw = new System.Windows.Forms.Button();
            txtCoord = new System.Windows.Forms.TextBox();
            lblScale = new System.Windows.Forms.Label();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // rdoScreen
            // 
            rdoScreen.AutoSize = true;
            rdoScreen.Checked = true;
            rdoScreen.Location = new Point(12, 3);
            rdoScreen.Name = "rdoScreen";
            rdoScreen.Size = new Size(90, 24);
            rdoScreen.TabIndex = 0;
            rdoScreen.TabStop = true;
            rdoScreen.Text = "屏幕坐标";
            rdoScreen.UseVisualStyleBackColor = true;
            // 
            // rdoWorld
            // 
            rdoWorld.AutoSize = true;
            rdoWorld.Location = new Point(108, 7);
            rdoWorld.Name = "rdoWorld";
            rdoWorld.Size = new Size(90, 24);
            rdoWorld.TabIndex = 2;
            rdoWorld.Text = "世界坐标";
            rdoWorld.UseVisualStyleBackColor = true;
            // 
            // rdoUser
            // 
            rdoUser.AutoSize = true;
            rdoUser.Location = new Point(221, 7);
            rdoUser.Name = "rdoUser";
            rdoUser.Size = new Size(90, 24);
            rdoUser.TabIndex = 3;
            rdoUser.Text = "用户坐标";
            rdoUser.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.Controls.Add(btnUcsView);
            panel1.Controls.Add(btnWcsView);
            panel1.Controls.Add(btnDraw);
            panel1.Controls.Add(txtCoord);
            panel1.Controls.Add(rdoScreen);
            panel1.Controls.Add(rdoUser);
            panel1.Controls.Add(rdoWorld);
            panel1.Location = new Point(159, 592);
            panel1.Name = "panel1";
            panel1.Size = new Size(965, 36);
            panel1.TabIndex = 4;
            // 
            // btnUcsView
            // 
            btnUcsView.Location = new Point(778, 7);
            btnUcsView.Name = "btnUcsView";
            btnUcsView.Size = new Size(143, 29);
            btnUcsView.TabIndex = 7;
            btnUcsView.Text = "按用户坐标系显示";
            btnUcsView.UseVisualStyleBackColor = true;
            btnUcsView.Click += btnUcsView_Click;
            // 
            // btnWcsView
            // 
            btnWcsView.Location = new Point(629, 7);
            btnWcsView.Name = "btnWcsView";
            btnWcsView.Size = new Size(143, 29);
            btnWcsView.TabIndex = 6;
            btnWcsView.Text = "按世界坐标系显示";
            btnWcsView.UseVisualStyleBackColor = true;
            btnWcsView.Click += btnWcsView_Click;
            // 
            // btnDraw
            // 
            btnDraw.Location = new Point(529, 7);
            btnDraw.Name = "btnDraw";
            btnDraw.Size = new Size(94, 29);
            btnDraw.TabIndex = 5;
            btnDraw.Text = "画线";
            btnDraw.UseVisualStyleBackColor = true;
            btnDraw.Click += btnDraw_Click;
            // 
            // txtCoord
            // 
            txtCoord.Location = new Point(365, 7);
            txtCoord.Name = "txtCoord";
            txtCoord.Size = new Size(158, 27);
            txtCoord.TabIndex = 4;
            txtCoord.Text = "100,100";
            // 
            // lblScale
            // 
            lblScale.AutoSize = true;
            lblScale.Location = new Point(12, 9);
            lblScale.Name = "lblScale";
            lblScale.Size = new Size(31, 20);
            lblScale.TabIndex = 5;
            lblScale.Text = "1.0";
            // 
            // DCS_WCS_UCS
            // 
            AutoScaleDimensions = new SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new Size(1310, 626);
            Controls.Add(lblScale);
            Controls.Add(panel1);
            DoubleBuffered = true;
            Name = "DCS_WCS_UCS";
            Text = "Form1";
            Paint += DCS_WCS_UCS_Paint;
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.RadioButton rdoScreen;
        private System.Windows.Forms.RadioButton rdoWorld;
        private System.Windows.Forms.RadioButton rdoUser;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.TextBox txtCoord;
        private System.Windows.Forms.Label lblScale;
        private System.Windows.Forms.Button btnWcsView;
        private System.Windows.Forms.Button btnUcsView;
    }
}