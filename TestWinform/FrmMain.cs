﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
/*
 * 20/08/2008
 * 
 * Part of the open source project mbrPropertyGrid
 * Developer : mbr ® (Massimiliano Brugnerotto)
 *  
 */
namespace TestWinform
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        #region Private internal functions

        /// <summary>
        /// Add items to the property grid at run-time
        /// </summary>
        private void PropGridAddItems()
        {
            mbrPropertyGrid.PropertyItemCategory catItem;
            mbrPropertyGrid.PropertyItemString strItem;
            mbrPropertyGrid.PropertyItemDropDownList cmbItem;
            mbrPropertyGrid.PropertyItemInt32 intItem;
            mbrPropertyGrid.PropertyItemBoolean boolItem;
            mbrPropertyGrid.PropertyItemColor colItem;
            mbrPropertyGrid.PropertyItemDouble dblItem;
            mbrPropertyGrid.PropertyItemDateTime dtTmItem;
            mbrPropertyGrid.PropertyItemFont fontItem;
            mbrPropertyGrid.PropertyItemImage imgItem;
            mbrPropertyGrid.PropertyItemFile fileItem;
            mbrPropertyGrid.PropertyItemDirectory dirItem;
            mbrPropertyGrid.PropertyItemProgressBar progBarItem;

            catItem = new mbrPropertyGrid.PropertyItemCategory("Main Category");
            PropertyGrid1.CategoryAdd("CatMain", catItem);

            strItem = new mbrPropertyGrid.PropertyItemString("Line01 - Name (String)", "Jak", "Jak Smith");
            strItem.HelpCaption = "Name";
            strItem.HelpText = "Tell me your name...";
            PropertyGrid1.ItemAdd("CatMain", "YourName", strItem);
           
            intItem = new mbrPropertyGrid.PropertyItemInt32("Line02 - Age (Int32)", 0);
            intItem.SetValidationRange(0, 120, 1);
            intItem.SetHelpCaptionText("Age", "Tell me your age (valid range : 0..120)");
            PropertyGrid1.ItemAdd("CatMain", "MyAge", intItem);
            
            // Set current selectd item (second row) and first column size (pixel)
            PropertyGrid1.SelectedItem = intItem;
            PropertyGrid1.ColumnWidth = 180;

            strItem = new mbrPropertyGrid.PropertyItemString("Line03 - Job (String)", "A software developer");
            strItem.HelpText = "Tell me about your job";
            strItem.Enabled = false;
            PropertyGrid1.ItemAdd("CatMain", "K03", strItem);

            cmbItem = new mbrPropertyGrid.PropertyItemDropDownList("Line04 - Best friend (Combo list)", "Bob", "Rosy", "Max|Bob|Carl|Antony|Rosy");
            cmbItem.HelpCaption = "Best friend";
            cmbItem.HelpText = "Tell me the name of your best friend!";
            PropertyGrid1.ItemAdd("CatMain", "K04", cmbItem);

            dblItem = new mbrPropertyGrid.PropertyItemDouble("Line05 - Distance (Double)", 0.0, -1.0);
            dblItem.DecimalPlaces = 5;
            dblItem.Format = "0.000";
            dblItem.EngineeringUnit = "[m]";
            dblItem.SetHelpCaptionText("Distance", "Set the distance offset (m) from the 0 line. Default value is -1.0 m.");
            PropertyGrid1.ItemAdd("CatMain", "K05", dblItem);

            boolItem = new mbrPropertyGrid.PropertyItemBoolean("Line06 - Sex (Bool)", true);
            boolItem.SetValidationRange("Male", "Female");
            boolItem.SetHelpCaptionText("Sex", "Male or Female?");
            PropertyGrid1.ItemAdd("CatMain", "K06", boolItem);

            catItem = new mbrPropertyGrid.PropertyItemCategory("Options");
            PropertyGrid1.CategoryAdd("C1", catItem);

            strItem = new mbrPropertyGrid.PropertyItemString("Line07 - ID Code (String)", "alpha");
            strItem.ShowExpandButton = true;
            strItem.MaxLength = 10;
            strItem.SetHelpCaptionText("Password", "Tell me your ID code (max.10 characters)");
            PropertyGrid1.ItemAdd("C1", "K07", strItem);

            boolItem = new mbrPropertyGrid.PropertyItemBoolean("Line08 - Drink wine (Bool)", true);
            boolItem.SetValidationRange("No", "Yes"); 
            boolItem.SetHelpCaptionText("Drink wine", "Say 'Yes' if you like to drink a glass of good wine");
            PropertyGrid1.ItemAdd("C1", "K08", boolItem);

            colItem = new mbrPropertyGrid.PropertyItemColor("Line09 - Back color (Color)", Color.Red, Color.Yellow);
            colItem.SetHelpCaptionText("Color", "You preferit background color");
            PropertyGrid1.ItemAdd("C1", "K09", colItem);

            dblItem = new mbrPropertyGrid.PropertyItemDouble("Line10 - Distance (Double)", 94.0);
            dblItem.SetHelpCaptionText("Distance", "The distance (Km) from your home to your office. Valid range : 1..150");
            dblItem.DecimalPlaces = 1;
            dblItem.Format = "0.000";
            dblItem.EngineeringUnit = "Km";
            dblItem.SetValidationRange(1.0, 150.0, 0.1, mbrPropertyGrid.ValidationRangeCheckType.Manual);
            PropertyGrid1.ItemAdd("C1", "K10", dblItem);

            dtTmItem = new mbrPropertyGrid.PropertyItemDateTime("Line11 - A Date", DateTime.Now, mbrPropertyGrid.DateTimeType.OnlyDate);
            dtTmItem.SetHelpCaptionText("Date", "Only date");
            PropertyGrid1.ItemAdd("C1", "K11", dtTmItem);

            dtTmItem = new mbrPropertyGrid.PropertyItemDateTime("Line12 - A Time", DateTime.Now, mbrPropertyGrid.DateTimeType.OnlyTime);
            dtTmItem.SetHelpCaptionText("Time", "Only time");
            PropertyGrid1.ItemAdd("C1", "K12", dtTmItem);

            dtTmItem = new mbrPropertyGrid.PropertyItemDateTime("Line13 - Date and Time", DateTime.Now, mbrPropertyGrid.DateTimeType.DateAndTime);
            dtTmItem.SetHelpCaptionText("Date and Time", "Date and time in the same property line");
            dtTmItem.Format = "HH:mm:ss - dd/MM/yyyy";
            PropertyGrid1.ItemAdd("C1", "K13", dtTmItem);

            fontItem = new mbrPropertyGrid.PropertyItemFont("Line14 - Font", new System.Drawing.Font("Arial", 10.0F));
            PropertyGrid1.ItemAdd("C1", "K14", fontItem);

            imgItem = new mbrPropertyGrid.PropertyItemImage("Line15 - Image", null, @"c:\windows\zapotec.bmp");
            PropertyGrid1.ItemAdd("C1", "K15", imgItem);

            fileItem = new mbrPropertyGrid.PropertyItemFile("Line16 - File", @"c:\windows\setupapi.log");
            PropertyGrid1.ItemAdd("C1", "K16", fileItem);

            dirItem = new mbrPropertyGrid.PropertyItemDirectory("Line17 - Directory", @"c:\windows\system32");
            dirItem.SetHelpCaptionText("Directory", "System directory path name");
            dirItem.Description = "Select the system directory path";
            PropertyGrid1.ItemAdd("C1", "K17", dirItem);

            progBarItem = new mbrPropertyGrid.PropertyItemProgressBar("Line18 - Progress bar", 90);
            PropertyGrid1.ItemAdd("C1", "K18", progBarItem);
            //progBarItem.Enabled = false;

             
            // Repaint control
            PropertyGrid1.RefreshControl(true);
        }

        /// <summary>
        /// Show property grid items in italian language
        /// </summary>
        private void SetLanguage_IT()
        {
            mbrPropertyGrid.PropertyItemCategory catItem;
            mbrPropertyGrid.PropertyItemString strItem;
            mbrPropertyGrid.PropertyItemDropDownList cmbItem;
            mbrPropertyGrid.PropertyItemInt32 intItem;
            mbrPropertyGrid.PropertyItemBoolean boolItem;
            mbrPropertyGrid.PropertyItemColor colItem;
            mbrPropertyGrid.PropertyItemDouble dblItem;
            mbrPropertyGrid.PropertyItemDateTime dtTmItem;
            mbrPropertyGrid.PropertyItemFont fontItem;
            mbrPropertyGrid.PropertyItemImage imgItem;
            mbrPropertyGrid.PropertyItemFile fileItem;
            mbrPropertyGrid.PropertyItemDirectory dirItem;
            mbrPropertyGrid.PropertyItemProgressBar progBarItem;

            catItem = PropertyGrid1.GetCategory("CatMain");
            catItem.Text = "Categoria principale";

            strItem = (mbrPropertyGrid.PropertyItemString)PropertyGrid1["YourName"];
            strItem.Text = "01 - Nome";
            strItem.SetHelpCaptionText("Nome", "Dimmi il tuo nome...");

            intItem = (mbrPropertyGrid.PropertyItemInt32)PropertyGrid1["MyAge"];
            intItem.Text = "02 - Età";
            intItem.SetHelpCaptionText("Età", "Dimmi quanti anni hai (minimo 0, massimo 120 anni)");

            strItem = (mbrPropertyGrid.PropertyItemString)PropertyGrid1["K03"];
            strItem.Text = "03 - Professione";
            strItem.SetHelpCaptionText("Professione", "Dimmi che lavoro fai");
            strItem.Value = "Programmatore";

            cmbItem = (mbrPropertyGrid.PropertyItemDropDownList)PropertyGrid1["K04"];
            cmbItem.Text = "04 - Migliore amico";
            cmbItem.HelpCaption = "Migliore amico";
            cmbItem.HelpText = "Dimmi il nome del tuo migliore amico!";

            dblItem = (mbrPropertyGrid.PropertyItemDouble)PropertyGrid1["K05"];
            dblItem.Text = "05 - Distanza";
            dblItem.SetHelpCaptionText("Distanza", "Dimmi la distanza (m) da una ipotetica linea 0. Il valore predefinito è -1.0 m.");

            boolItem = (mbrPropertyGrid.PropertyItemBoolean)PropertyGrid1.GetItem("K06");
            boolItem.Text = "06 - Sesso";
            boolItem.SetValidationRange("Maschio", "Femmina");
            boolItem.SetHelpCaptionText("Sesso", "Maschio o femmina?");

            catItem = PropertyGrid1.GetCategory("C1");
            catItem.Text = "Opzioni";
            
            strItem = (mbrPropertyGrid.PropertyItemString)PropertyGrid1["K07"];
            strItem.Text = "07 - Codice ID";
            strItem.SetHelpCaptionText("Password", "Dammi un codice ID (mssimo 10 caratteri)");

            boolItem = (mbrPropertyGrid.PropertyItemBoolean)PropertyGrid1["K08"];
            boolItem.Text = "08 - Bevi vino?";
            boolItem.SetValidationRange("No", "Si");
            boolItem.SetHelpCaptionText("Bevi vino", "Imposta 'Si' se ogni tanto ti piace bere un bicchiere di buon vino");
            
            colItem = (mbrPropertyGrid.PropertyItemColor)PropertyGrid1["K09"];
            colItem.Text = "09 - Colore sfondo";
            colItem.SetHelpCaptionText("Colore", "Colore dello sfondo preferito");
            
            dblItem = (mbrPropertyGrid.PropertyItemDouble)PropertyGrid1["K10"];
            dblItem.Text = "10 - Distanza dal lavoro";
            dblItem.SetHelpCaptionText("Distanza dal lavoro", "Distanza (Km) da casa tua fino al tuo ufficio. Intervallo valido : 1..150 Km");
            
            dtTmItem = (mbrPropertyGrid.PropertyItemDateTime)PropertyGrid1["K11"];
            dtTmItem.Text = "11 - Data";
            dtTmItem.SetHelpCaptionText("Data", "Una data");

            dtTmItem = (mbrPropertyGrid.PropertyItemDateTime)PropertyGrid1["K12"];
            dtTmItem.Text = "12 - Ora";
            dtTmItem.SetHelpCaptionText("Ora", "Ora");
 
            dtTmItem = (mbrPropertyGrid.PropertyItemDateTime)PropertyGrid1["K13"];
            dtTmItem.Text = "13 - Data e ora";
            dtTmItem.SetHelpCaptionText("Date e ora", "Data e ora sulla stessa riga");

            fontItem = (mbrPropertyGrid.PropertyItemFont)PropertyGrid1["K14"];
            fontItem.Text = "14 - Carattere";

            imgItem = (mbrPropertyGrid.PropertyItemImage)PropertyGrid1["K15"];
            imgItem.Text = "15 - Immagine";

            fileItem = (mbrPropertyGrid.PropertyItemFile)PropertyGrid1["K16"];
            fileItem.Text = "16 - File";

            dirItem = (mbrPropertyGrid.PropertyItemDirectory)PropertyGrid1["K17"];
            dirItem.Text = "17 - Directory";
            dirItem.SetHelpCaptionText("Directory", "Cartella di sistema");
            dirItem.Description = "Seleziona la cartella di sistema";

            progBarItem = (mbrPropertyGrid.PropertyItemProgressBar)PropertyGrid1["K18"];
            progBarItem.Text = "18 - Stato avanzamento";

            // Repaint control
            PropertyGrid1.RefreshControl(true);
        }

        /// <summary>
        /// Show property grid items in english language
        /// </summary>
        private void SetLanguage_EN()
        {
            mbrPropertyGrid.PropertyItemCategory catItem;
            mbrPropertyGrid.PropertyItemString strItem;
            mbrPropertyGrid.PropertyItemDropDownList cmbItem;
            mbrPropertyGrid.PropertyItemInt32 intItem;
            mbrPropertyGrid.PropertyItemBoolean boolItem;
            mbrPropertyGrid.PropertyItemColor colItem;
            mbrPropertyGrid.PropertyItemDouble dblItem;
            mbrPropertyGrid.PropertyItemDateTime dtTmItem;
            mbrPropertyGrid.PropertyItemFont fontItem;
            mbrPropertyGrid.PropertyItemImage imgItem;
            mbrPropertyGrid.PropertyItemFile fileItem;
            mbrPropertyGrid.PropertyItemDirectory dirItem;
            mbrPropertyGrid.PropertyItemProgressBar progBarItem;

            catItem = PropertyGrid1.GetCategory("CatMain");
            catItem.Text = "Main Category";

            strItem = (mbrPropertyGrid.PropertyItemString)PropertyGrid1["YourName"];
            strItem.Text = "Line01 - Name (String)";
            strItem.SetHelpCaptionText("Name", "Tell me your name...");
            
            intItem = (mbrPropertyGrid.PropertyItemInt32)PropertyGrid1["MyAge"];
            intItem.Text = "Line02 - Age (Int32)";
            intItem.SetHelpCaptionText("Age", "Tell me your age (valid range : 0..120)");
            
            strItem = (mbrPropertyGrid.PropertyItemString)PropertyGrid1["K03"];
            strItem.Text = "Line03 - Job (String)";
            strItem.SetHelpCaptionText("Job", "Tell me your job...");
            strItem.Value = "A software developer";

            cmbItem = (mbrPropertyGrid.PropertyItemDropDownList)PropertyGrid1["K04"];
            cmbItem.Text = "Line04 - Best friend (Combo list)";
            cmbItem.HelpCaption = "Best friend";
            cmbItem.HelpText = "Tell me the name of your best friend!";

            dblItem = (mbrPropertyGrid.PropertyItemDouble)PropertyGrid1["K05"];
            dblItem.Text = "Line05 - Distance (Double)";
            dblItem.SetHelpCaptionText("Distance", "Set the distance offset (m) from the 0 line. Default value is -1.0 m.");

            boolItem = (mbrPropertyGrid.PropertyItemBoolean)PropertyGrid1.GetItem("K06");
            boolItem.Text = "Line06 - Sex (Bool)";
            boolItem.SetValidationRange("Male", "Female");
            boolItem.SetHelpCaptionText("Sex", "Male or Female?");

            catItem = PropertyGrid1.GetCategory("C1");
            catItem.Text = "Options";

            strItem = (mbrPropertyGrid.PropertyItemString)PropertyGrid1["K07"];
            strItem.Text = "Line07 - ID Code (String)";
            strItem.SetHelpCaptionText("Password", "Tell me your ID code (max.10 characters)");

            boolItem = (mbrPropertyGrid.PropertyItemBoolean)PropertyGrid1["K08"];
            boolItem.Text = "Line08 - Drink wine (Bool)";
            boolItem.SetValidationRange("No", "Yes");
            boolItem.SetHelpCaptionText("Drink wine", "Say 'Yes' if you like to drink a glass of good wine");

            colItem = (mbrPropertyGrid.PropertyItemColor)PropertyGrid1["K09"];
            colItem.Text = "Line09 - Back color (Color)";
            colItem.SetHelpCaptionText("Color", "You preferit background color");

            dblItem = (mbrPropertyGrid.PropertyItemDouble)PropertyGrid1["K10"];
            dblItem.Text = "Line10 - Distance (Double)";
            dblItem.SetHelpCaptionText("Distance", "The distance (Km) from your home to your office. Valid range : 1..150");

            dtTmItem = (mbrPropertyGrid.PropertyItemDateTime)PropertyGrid1["K11"];
            dtTmItem.Text = "Line11 - A Date";
            dtTmItem.SetHelpCaptionText("Date", "Only date");

            dtTmItem = (mbrPropertyGrid.PropertyItemDateTime)PropertyGrid1["K12"];
            dtTmItem.Text = "Line12 - A Time";
            dtTmItem.SetHelpCaptionText("Time", "Only time");

            dtTmItem = (mbrPropertyGrid.PropertyItemDateTime)PropertyGrid1["K13"];
            dtTmItem.Text = "Line13 - Date and Time";
            dtTmItem.SetHelpCaptionText("Date and Time", "Date and time in the same property line");

            fontItem = (mbrPropertyGrid.PropertyItemFont)PropertyGrid1["K14"];
            fontItem.Text = "Line14 - Font";

            imgItem = (mbrPropertyGrid.PropertyItemImage)PropertyGrid1["K15"];
            imgItem.Text = "Line15 - Image";

            fileItem = (mbrPropertyGrid.PropertyItemFile)PropertyGrid1["K16"];
            fileItem.Text = "Line16 - File";

            dirItem = (mbrPropertyGrid.PropertyItemDirectory)PropertyGrid1["K17"];
            dirItem.Text = "Line17 - Directory";
            dirItem.SetHelpCaptionText("Directory", "System directory path name");
            dirItem.Description = "Select the system directory path";

            progBarItem = (mbrPropertyGrid.PropertyItemProgressBar)PropertyGrid1["K18"];
            progBarItem.Text = "Line18 - Progress bar";

            // Repaint control
            PropertyGrid1.RefreshControl(true);
        }

        

        /// <summary>
        /// Set the property grid items language (0=EN, 1=IT)
        /// </summary>
        private void SetLanguage(int index)
        {
            if (PropertyGrid1.ItemsCount == 0) return;
            switch (index)
            {
                case 0:
                    SetLanguage_EN();
                    break;
                case 1:
                    SetLanguage_IT();
                    break;
            }
        }

        #endregion

        #region Events generated by user interface

        private void FrmMain_Load(object sender, EventArgs e)
        {
            cmbBoxLanguage.SelectedIndex = 0;
            PropertyGrid1.Text = txtText.Text;
        }

        private void btnAddItems_Click(object sender, EventArgs e)
        {
            btnAddItems.Enabled = false;
            PropGridAddItems();
            SetLanguage(cmbBoxLanguage.SelectedIndex);
        }

        private void cmbBoxLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetLanguage(cmbBoxLanguage.SelectedIndex);
        }

        private void chkBoxTextVisible_CheckedChanged(object sender, EventArgs e)
        {
            PropertyGrid1.TextVisible = chkBoxTextVisible.Checked;
            PropertyGrid1.Text = txtText.Text;
        }

        private void chkBoxToolbarVisible_CheckedChanged(object sender, EventArgs e)
        {
            PropertyGrid1.ToolbarVisible = chkBoxToolbarVisible.Checked;
        }

        private void chkBoxHelpVisible_CheckedChanged(object sender, EventArgs e)
        {
            PropertyGrid1.HelpVisible = chkBoxHelpVisible.Checked;          
        }

        private void chkBoxButtonShowTextVisible_CheckedChanged(object sender, EventArgs e)
        {
            PropertyGrid1.ButtonShowTextVisible = chkBoxButtonShowTextVisible.Checked;
        }

        private void chkBoxButtonApplyVisible_CheckedChanged(object sender, EventArgs e)
        {
            PropertyGrid1.ButtonApplyVisible = chkBoxButtonApplyVisible.Checked;
        }

        private void txtText_TextChanged(object sender, EventArgs e)
        {
            PropertyGrid1.Text = txtText.Text;
        }

        private void btnGetValue_Click(object sender, EventArgs e)
        {
            if (PropertyGrid1.SelectedItem == null) return;
            MessageBox.Show("Selected item string value : " + PropertyGrid1.SelectedItem.ValueString, "Value", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void PropertyGrid1_ApplyButtonPressed(object sender, ref bool cancel)
        {
            int age = 0;

            // TODO : Apply settings (property grid values) to your 
            //        application here...
            if (PropertyGrid1.ItemsCount == 0) return;      // Check if items exits...
            age = ((mbrPropertyGrid.PropertyItemInt32)PropertyGrid1["MyAge"]).Value;

            // After settings is been applied changes are reset automatically
            // if cancel is false
            cancel = false;

            // To reset changes manually, in every moment you want, you must
            // call the method :
            //  PropertyGrid1.ResetChanges();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            PropertyGrid1.ShowFormAboutMe();
        }

        private void trkProgressBar_Scroll(object sender, EventArgs e)
        {
            if (PropertyGrid1["K18"] == null) return;
            ((mbrPropertyGrid.PropertyItemProgressBar)PropertyGrid1["K18"]).Value = trkProgressBar.Value;
            PropertyGrid1.RefreshControl(true);
        }

        private void btnSetDefaults_Click(object sender, EventArgs e)
        {
            PropertyGrid1.SetDefaultValues();
        }

        #endregion

    }
}
