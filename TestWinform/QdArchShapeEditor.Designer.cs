﻿namespace TestWinform
{
    partial class QdArchShapeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            splitContainer1 = new System.Windows.Forms.SplitContainer();
            panel4 = new System.Windows.Forms.Panel();
            txtBtmOffset = new System.Windows.Forms.NumericUpDown();
            txtHeight = new System.Windows.Forms.NumericUpDown();
            txtWidth = new System.Windows.Forms.NumericUpDown();
            txtWallThickness = new System.Windows.Forms.NumericUpDown();
            label3 = new System.Windows.Forms.Label();
            txtDebug = new System.Windows.Forms.RichTextBox();
            label5 = new System.Windows.Forms.Label();
            chkIsNear = new System.Windows.Forms.CheckBox();
            chkIsLeft = new System.Windows.Forms.CheckBox();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            panel5 = new System.Windows.Forms.Panel();
            lstShape = new System.Windows.Forms.ListBox();
            cboSubCategory = new System.Windows.Forms.ComboBox();
            panel2 = new System.Windows.Forms.Panel();
            skGL = new SkiaSharp.Views.Desktop.SKGLControl();
            panel1 = new System.Windows.Forms.Panel();
            cboCategory = new System.Windows.Forms.ComboBox();
            btnRunScript = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)txtBtmOffset).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtHeight).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtWidth).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtWallThickness).BeginInit();
            panel5.SuspendLayout();
            panel2.SuspendLayout();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            splitContainer1.Location = new System.Drawing.Point(0, 49);
            splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(panel4);
            splitContainer1.Panel1.Controls.Add(panel5);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(panel2);
            splitContainer1.Size = new System.Drawing.Size(1262, 604);
            splitContainer1.SplitterDistance = 479;
            splitContainer1.TabIndex = 0;
            // 
            // panel4
            // 
            panel4.Controls.Add(txtBtmOffset);
            panel4.Controls.Add(txtHeight);
            panel4.Controls.Add(txtWidth);
            panel4.Controls.Add(txtWallThickness);
            panel4.Controls.Add(label3);
            panel4.Controls.Add(txtDebug);
            panel4.Controls.Add(label5);
            panel4.Controls.Add(chkIsNear);
            panel4.Controls.Add(chkIsLeft);
            panel4.Controls.Add(label2);
            panel4.Controls.Add(label1);
            panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            panel4.Location = new System.Drawing.Point(255, 0);
            panel4.Name = "panel4";
            panel4.Size = new System.Drawing.Size(224, 604);
            panel4.TabIndex = 7;
            // 
            // txtBtmOffset
            // 
            txtBtmOffset.Increment = new decimal(new int[] { 50, 0, 0, 0 });
            txtBtmOffset.Location = new System.Drawing.Point(87, 162);
            txtBtmOffset.Maximum = new decimal(new int[] { 2000, 0, 0, 0 });
            txtBtmOffset.Name = "txtBtmOffset";
            txtBtmOffset.Size = new System.Drawing.Size(109, 27);
            txtBtmOffset.TabIndex = 16;
            // 
            // txtHeight
            // 
            txtHeight.Increment = new decimal(new int[] { 100, 0, 0, 0 });
            txtHeight.Location = new System.Drawing.Point(87, 115);
            txtHeight.Maximum = new decimal(new int[] { 5000, 0, 0, 0 });
            txtHeight.Name = "txtHeight";
            txtHeight.Size = new System.Drawing.Size(109, 27);
            txtHeight.TabIndex = 15;
            txtHeight.Value = new decimal(new int[] { 1800, 0, 0, 0 });
            // 
            // txtWidth
            // 
            txtWidth.Increment = new decimal(new int[] { 100, 0, 0, 0 });
            txtWidth.Location = new System.Drawing.Point(87, 69);
            txtWidth.Maximum = new decimal(new int[] { 5000, 0, 0, 0 });
            txtWidth.Name = "txtWidth";
            txtWidth.Size = new System.Drawing.Size(109, 27);
            txtWidth.TabIndex = 14;
            txtWidth.Value = new decimal(new int[] { 1000, 0, 0, 0 });
            // 
            // txtWallThickness
            // 
            txtWallThickness.Increment = new decimal(new int[] { 20, 0, 0, 0 });
            txtWallThickness.Location = new System.Drawing.Point(87, 23);
            txtWallThickness.Maximum = new decimal(new int[] { 500, 0, 0, 0 });
            txtWallThickness.Name = "txtWallThickness";
            txtWallThickness.Size = new System.Drawing.Size(109, 27);
            txtWallThickness.TabIndex = 13;
            txtWallThickness.Value = new decimal(new int[] { 300, 0, 0, 0 });
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(12, 23);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(39, 20);
            label3.TabIndex = 11;
            label3.Text = "墙厚";
            // 
            // txtDebug
            // 
            txtDebug.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            txtDebug.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtDebug.Location = new System.Drawing.Point(0, 251);
            txtDebug.Name = "txtDebug";
            txtDebug.Size = new System.Drawing.Size(222, 350);
            txtDebug.TabIndex = 10;
            txtDebug.Text = "";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(12, 164);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(69, 20);
            label5.TabIndex = 8;
            label5.Text = "底部偏移";
            // 
            // chkIsNear
            // 
            chkIsNear.AutoSize = true;
            chkIsNear.Checked = true;
            chkIsNear.CheckState = System.Windows.Forms.CheckState.Checked;
            chkIsNear.Location = new System.Drawing.Point(118, 204);
            chkIsNear.Name = "chkIsNear";
            chkIsNear.Size = new System.Drawing.Size(61, 24);
            chkIsNear.TabIndex = 7;
            chkIsNear.Text = "近端";
            chkIsNear.UseVisualStyleBackColor = true;
            // 
            // chkIsLeft
            // 
            chkIsLeft.AutoSize = true;
            chkIsLeft.Checked = true;
            chkIsLeft.CheckState = System.Windows.Forms.CheckState.Checked;
            chkIsLeft.Location = new System.Drawing.Point(27, 204);
            chkIsLeft.Name = "chkIsLeft";
            chkIsLeft.Size = new System.Drawing.Size(61, 24);
            chkIsLeft.TabIndex = 5;
            chkIsLeft.Text = "左边";
            chkIsLeft.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(12, 117);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(39, 20);
            label2.TabIndex = 2;
            label2.Text = "高度";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(12, 71);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(39, 20);
            label1.TabIndex = 0;
            label1.Text = "宽度";
            // 
            // panel5
            // 
            panel5.Controls.Add(lstShape);
            panel5.Controls.Add(cboSubCategory);
            panel5.Dock = System.Windows.Forms.DockStyle.Left;
            panel5.Location = new System.Drawing.Point(0, 0);
            panel5.Name = "panel5";
            panel5.Size = new System.Drawing.Size(255, 604);
            panel5.TabIndex = 7;
            // 
            // lstShape
            // 
            lstShape.Dock = System.Windows.Forms.DockStyle.Fill;
            lstShape.FormattingEnabled = true;
            lstShape.IntegralHeight = false;
            lstShape.ItemHeight = 20;
            lstShape.Location = new System.Drawing.Point(0, 28);
            lstShape.Name = "lstShape";
            lstShape.Size = new System.Drawing.Size(255, 576);
            lstShape.TabIndex = 0;
            lstShape.SelectedIndexChanged += lstShape_SelectedIndexChanged;
            // 
            // cboSubCategory
            // 
            cboSubCategory.Dock = System.Windows.Forms.DockStyle.Top;
            cboSubCategory.FormattingEnabled = true;
            cboSubCategory.Location = new System.Drawing.Point(0, 0);
            cboSubCategory.Name = "cboSubCategory";
            cboSubCategory.Size = new System.Drawing.Size(255, 28);
            cboSubCategory.TabIndex = 0;
            cboSubCategory.SelectedIndexChanged += cboSubCategory_SelectedIndexChanged;
            // 
            // panel2
            // 
            panel2.Controls.Add(skGL);
            panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Location = new System.Drawing.Point(0, 0);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(779, 604);
            panel2.TabIndex = 5;
            // 
            // skGL
            // 
            skGL.BackColor = System.Drawing.Color.Black;
            skGL.Dock = System.Windows.Forms.DockStyle.Fill;
            skGL.Location = new System.Drawing.Point(0, 0);
            skGL.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            skGL.Name = "skGL";
            skGL.Size = new System.Drawing.Size(779, 604);
            skGL.TabIndex = 0;
            skGL.VSync = true;
            // 
            // panel1
            // 
            panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel1.Controls.Add(cboCategory);
            panel1.Controls.Add(btnRunScript);
            panel1.Dock = System.Windows.Forms.DockStyle.Top;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(1262, 49);
            panel1.TabIndex = 0;
            // 
            // cboCategory
            // 
            cboCategory.FormattingEnabled = true;
            cboCategory.Location = new System.Drawing.Point(3, 9);
            cboCategory.Name = "cboCategory";
            cboCategory.Size = new System.Drawing.Size(252, 28);
            cboCategory.TabIndex = 6;
            cboCategory.SelectedIndexChanged += cboCategory_SelectedIndexChanged;
            // 
            // btnRunScript
            // 
            btnRunScript.Location = new System.Drawing.Point(393, 11);
            btnRunScript.Name = "btnRunScript";
            btnRunScript.Size = new System.Drawing.Size(112, 29);
            btnRunScript.TabIndex = 5;
            btnRunScript.Text = "运行脚本";
            btnRunScript.UseVisualStyleBackColor = true;
            // 
            // QdArchShapeEditor
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1262, 653);
            Controls.Add(splitContainer1);
            Controls.Add(panel1);
            Name = "QdArchShapeEditor";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "QdArch Shape编辑器";
            Load += QdArchShapeEditor_Load;
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            panel4.ResumeLayout(false);
            panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)txtBtmOffset).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtHeight).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtWidth).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtWallThickness).EndInit();
            panel5.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private SkiaSharp.Views.Desktop.SKGLControl skGL;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnRunScript;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListBox lstShape;
        private System.Windows.Forms.ComboBox cboSubCategory;
        private System.Windows.Forms.CheckBox chkIsNear;
        private System.Windows.Forms.CheckBox chkIsLeft;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox txtDebug;
        private System.Windows.Forms.NumericUpDown txtWallThickness;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown txtHeight;
        private System.Windows.Forms.NumericUpDown txtWidth;
        private System.Windows.Forms.NumericUpDown txtBtmOffset;
        private System.Windows.Forms.ComboBox cboCategory;
    }
}