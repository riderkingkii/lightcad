using LightCAD.Core;
using Svg;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace TestWinform
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // JsonTest.SerializeAndDeserialize();
            //JsonTest.WriterAndReader();
            //JsonTest.TestDoc(LcDocument.Create());
            //// To customize application configuration such as set high DPI settings or default font,
            //// see https://aka.ms/applicationconfiguration.
            //ApplicationConfiguration.Initialize();
            // Application.Run(new SkiaSharpTest());
            //Application.Run(new QdArchShapeEditor());
            //Application.Run(new ThreeTestForm());
            Application.Run(new FrmMain());
            //TestSvg();
            //TestSpeed();
        }
        static void TestSvg()
        {
           var files= Directory.GetFiles("svgicons");
            var icoXmlDocs = new List<XmlDocument>();
            foreach (var file in files)
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(File.ReadAllText(file));
                icoXmlDocs.Add(xmlDoc);
            }
            var start = DateTime.Now;
            for(var i = 0; i < 1; i++)
            {
                var icons = new List<Bitmap>();
                for (var j=0;j<icoXmlDocs.Count;j++)
                {
                    var xmlDoc = icoXmlDocs[j];
                    var svgDoc = Svg.SvgDocument.Open(xmlDoc);
                    icons.Add(svgDoc.Draw(32, 32));
                    icons[j].Save(files[j] + ".png");
                }
            }
            var delta = DateTime.Now - start;
            MessageBox.Show(delta.TotalSeconds.ToString());
        }

        

        #region TestSpeed
        static void TestSpeed()
        {
            DateTime start = DateTime.Now;
            double sum1 = 0;
            for (var i = 0; i < 1000000; i++)
            {
                sum1 += TestClass();
            }
            var delta1 = (DateTime.Now - start).TotalSeconds;

            start = DateTime.Now;
            double sum2 = 0;
            for (var i = 0; i < 1000000; i++)
            {
                sum2 += TestStruct();
            }
            var delta2 = (DateTime.Now - start).TotalSeconds;

            start = DateTime.Now;
            double sum3 = 0;
            for (var i = 0; i < 1000000; i++)
            {
                sum3 += TestStatic();
            }
            var detla3 = (DateTime.Now - start).TotalSeconds;

            MessageBox.Show(string.Format("Class:{0}  Struct:{1}  Static:{2}", delta1, delta2, detla3));

        }
        class CTest
        {
            public double X;
            public double Y;
            public double Z;
            public double X1;
            public double Y1;
            public double Z1;
            public double X2;
            public double Y2;
            public double Z2;
        }
        struct STest
        {
            public double X;
            public double Y;
            public double Z;
            public double X1;
            public double Y1;
            public double Z1;
            public double X2;
            public double Y2;
            public double Z2;

        }
        static double TestClass()
        {
            var c1 = new CTest { X = 1, Y = 2, Z = 3 };
            var c2 = new CTest { X = 3, Y = 4, Z = 5 };
            var c3 = new CTest { X = 4, Y = 5, Z = 6 };

            var c4 = new CTest { X = 1, Y = 2, Z = 3 };
            var c5 = new CTest { X = 3, Y = 4, Z = 5 };
            var c6 = new CTest { X = 4, Y = 5, Z = 6 };

            double sum = 0;
            for (var i = 0; i < 100; i++)
            {
                var x1 = c1.X + c2.X + c3.X;
                var y1 = c1.Y + c2.Y + c3.Y;
                var z1 = c1.Z + c2.Z + c3.Z;

                var x2 = c4.X + c5.X + c6.X;
                var y2 = c4.Y + c5.Y + c6.Y;
                var z2 = c6.Y + c5.Z + c6.Z;

                sum = x1 + y1 + z1 + x2 + y2 + z2;
            }
            return sum;
        }
        static double TestStruct()
        {
            var c1 = new STest { X = 1, Y = 2, Z = 3 };
            var c2 = new STest { X = 3, Y = 4, Z = 5 };
            var c3 = new STest { X = 4, Y = 5, Z = 6 };

            var c4 = new STest { X = 1, Y = 2, Z = 3 };
            var c5 = new STest { X = 3, Y = 4, Z = 5 };
            var c6 = new STest { X = 4, Y = 5, Z = 6 };

            double sum = 0;
            for (var i = 0; i < 100; i++)
            {
                var x1 = c1.X + c2.X + c3.X;
                var y1 = c1.Y + c2.Y + c3.Y;
                var z1 = c1.Z + c2.Z + c3.Z;

                var x2 = c4.X + c5.X + c6.X;
                var y2 = c4.Y + c5.Y + c6.Y;
                var z2 = c6.Y + c5.Z + c6.Z;

                sum = x1 + y1 + z1 + x2 + y2 + z2;
            }
            return sum;
        }


        static CTest cc1 = new CTest { X = 1, Y = 2, Z = 3 };
        static CTest cc2 = new CTest { X = 3, Y = 4, Z = 5 };
        static CTest cc3 = new CTest { X = 4, Y = 5, Z = 6 };

        static CTest cc4 = new CTest { X = 1, Y = 2, Z = 3 };
        static CTest cc5 = new CTest { X = 3, Y = 4, Z = 5 };
        static CTest cc6 = new CTest { X = 4, Y = 5, Z = 6 };

        static double TestStatic()
        {

            double sum = 0;
            for (var i = 0; i < 100; i++)
            {
                var x1 = cc1.X + cc2.X + cc3.X;
                var y1 = cc1.Y + cc2.Y + cc3.Y;
                var z1 = cc1.Z + cc2.Z + cc3.Z;

                var x2 = cc4.X + cc5.X + cc6.X;
                var y2 = cc4.Y + cc5.Y + cc6.Y;
                var z2 = cc4.Y + cc5.Z + cc6.Z;

                sum = x1 + y1 + z1 + x2 + y2 + z2;
            }
            return sum;
        }
        #endregion
    }
}