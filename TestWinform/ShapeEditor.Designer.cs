﻿namespace TestWinform
{
    partial class ShapeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShapeEditor));
            button1 = new System.Windows.Forms.Button();
            button2 = new System.Windows.Forms.Button();
            btnAddCmd = new System.Windows.Forms.Button();
            btnRemoveCmd = new System.Windows.Forms.Button();
            btnEditCmd = new System.Windows.Forms.Button();
            btnRunScript = new System.Windows.Forms.Button();
            btnRunScriptStep = new System.Windows.Forms.Button();
            btnAddShapeType = new System.Windows.Forms.Button();
            button5 = new System.Windows.Forms.Button();
            panel1 = new System.Windows.Forms.Panel();
            panel2 = new System.Windows.Forms.Panel();
            skGL = new SkiaSharp.Views.Desktop.SKGLControl();
            panel3 = new System.Windows.Forms.Panel();
            txtDebug = new System.Windows.Forms.RichTextBox();
            panel4 = new System.Windows.Forms.Panel();
            tabControl1 = new System.Windows.Forms.TabControl();
            tabParam = new System.Windows.Forms.TabPage();
            dataGridView2 = new System.Windows.Forms.DataGridView();
            colVarName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            colDataType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            colDefaultValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            colDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            colRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            tabScriptEdit = new System.Windows.Forms.TabPage();
            dgvCmds = new System.Windows.Forms.DataGridView();
            colEdit = new System.Windows.Forms.DataGridViewImageColumn();
            colRowNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            colCmd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            colContent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            tabScriptView = new System.Windows.Forms.TabPage();
            txtScript = new System.Windows.Forms.RichTextBox();
            panel5 = new System.Windows.Forms.Panel();
            lstShapeType = new System.Windows.Forms.ListBox();
            panel6 = new System.Windows.Forms.Panel();
            cboShapeCategory = new System.Windows.Forms.ComboBox();
            splitContainer1 = new System.Windows.Forms.SplitContainer();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            panel3.SuspendLayout();
            panel4.SuspendLayout();
            tabControl1.SuspendLayout();
            tabParam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView2).BeginInit();
            tabScriptEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvCmds).BeginInit();
            tabScriptView.SuspendLayout();
            panel5.SuspendLayout();
            panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(221, 11);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(94, 29);
            button1.TabIndex = 0;
            button1.Text = "添加参数";
            button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(321, 11);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(94, 29);
            button2.TabIndex = 1;
            button2.Text = "删除参数";
            button2.UseVisualStyleBackColor = true;
            // 
            // btnAddCmd
            // 
            btnAddCmd.Location = new System.Drawing.Point(497, 11);
            btnAddCmd.Name = "btnAddCmd";
            btnAddCmd.Size = new System.Drawing.Size(112, 29);
            btnAddCmd.TabIndex = 2;
            btnAddCmd.Text = "添加脚本命令";
            btnAddCmd.UseVisualStyleBackColor = true;
            btnAddCmd.Click += btnAddCmd_Click;
            // 
            // btnRemoveCmd
            // 
            btnRemoveCmd.Location = new System.Drawing.Point(615, 11);
            btnRemoveCmd.Name = "btnRemoveCmd";
            btnRemoveCmd.Size = new System.Drawing.Size(117, 29);
            btnRemoveCmd.TabIndex = 3;
            btnRemoveCmd.Text = "删除脚本命令";
            btnRemoveCmd.UseVisualStyleBackColor = true;
            btnRemoveCmd.Click += btnRemoveCmd_Click;
            // 
            // btnEditCmd
            // 
            btnEditCmd.Location = new System.Drawing.Point(738, 11);
            btnEditCmd.Name = "btnEditCmd";
            btnEditCmd.Size = new System.Drawing.Size(112, 29);
            btnEditCmd.TabIndex = 4;
            btnEditCmd.Text = "编辑脚本命令";
            btnEditCmd.UseVisualStyleBackColor = true;
            btnEditCmd.Click += btnEditCmd_Click;
            // 
            // btnRunScript
            // 
            btnRunScript.Location = new System.Drawing.Point(870, 11);
            btnRunScript.Name = "btnRunScript";
            btnRunScript.Size = new System.Drawing.Size(112, 29);
            btnRunScript.TabIndex = 5;
            btnRunScript.Text = "运行脚本";
            btnRunScript.UseVisualStyleBackColor = true;
            // 
            // btnRunScriptStep
            // 
            btnRunScriptStep.Location = new System.Drawing.Point(998, 11);
            btnRunScriptStep.Name = "btnRunScriptStep";
            btnRunScriptStep.Size = new System.Drawing.Size(112, 29);
            btnRunScriptStep.TabIndex = 6;
            btnRunScriptStep.Text = "单步运行脚本";
            btnRunScriptStep.UseVisualStyleBackColor = true;
            // 
            // btnAddShapeType
            // 
            btnAddShapeType.Location = new System.Drawing.Point(11, 11);
            btnAddShapeType.Name = "btnAddShapeType";
            btnAddShapeType.Size = new System.Drawing.Size(94, 29);
            btnAddShapeType.TabIndex = 7;
            btnAddShapeType.Text = "编辑类别类型";
            btnAddShapeType.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            button5.Location = new System.Drawing.Point(1133, 11);
            button5.Name = "button5";
            button5.Size = new System.Drawing.Size(147, 29);
            button5.TabIndex = 9;
            button5.Text = "编译类别为DLL";
            button5.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel1.Controls.Add(button5);
            panel1.Controls.Add(btnAddShapeType);
            panel1.Controls.Add(btnRunScriptStep);
            panel1.Controls.Add(btnRunScript);
            panel1.Controls.Add(btnEditCmd);
            panel1.Controls.Add(btnRemoveCmd);
            panel1.Controls.Add(btnAddCmd);
            panel1.Controls.Add(button2);
            panel1.Controls.Add(button1);
            panel1.Dock = System.Windows.Forms.DockStyle.Top;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(1510, 49);
            panel1.TabIndex = 0;
            // 
            // panel2
            // 
            panel2.Controls.Add(skGL);
            panel2.Controls.Add(panel3);
            panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Location = new System.Drawing.Point(0, 0);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(642, 621);
            panel2.TabIndex = 5;
            // 
            // skGL
            // 
            skGL.BackColor = System.Drawing.Color.Black;
            skGL.Dock = System.Windows.Forms.DockStyle.Fill;
            skGL.Location = new System.Drawing.Point(0, 157);
            skGL.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            skGL.Name = "skGL";
            skGL.Size = new System.Drawing.Size(642, 464);
            skGL.TabIndex = 0;
            skGL.VSync = true;
            // 
            // panel3
            // 
            panel3.Controls.Add(txtDebug);
            panel3.Dock = System.Windows.Forms.DockStyle.Top;
            panel3.Location = new System.Drawing.Point(0, 0);
            panel3.Name = "panel3";
            panel3.Size = new System.Drawing.Size(642, 157);
            panel3.TabIndex = 5;
            // 
            // txtDebug
            // 
            txtDebug.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            txtDebug.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDebug.Location = new System.Drawing.Point(0, 0);
            txtDebug.Name = "txtDebug";
            txtDebug.Size = new System.Drawing.Size(642, 157);
            txtDebug.TabIndex = 1;
            txtDebug.Text = "";
            // 
            // panel4
            // 
            panel4.Controls.Add(tabControl1);
            panel4.Controls.Add(panel5);
            panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            panel4.Location = new System.Drawing.Point(0, 0);
            panel4.Name = "panel4";
            panel4.Size = new System.Drawing.Size(864, 621);
            panel4.TabIndex = 7;
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(tabParam);
            tabControl1.Controls.Add(tabScriptEdit);
            tabControl1.Controls.Add(tabScriptView);
            tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            tabControl1.Location = new System.Drawing.Point(180, 0);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new System.Drawing.Size(684, 621);
            tabControl1.TabIndex = 0;
            // 
            // tabParam
            // 
            tabParam.Controls.Add(dataGridView2);
            tabParam.Location = new System.Drawing.Point(4, 29);
            tabParam.Name = "tabParam";
            tabParam.Padding = new System.Windows.Forms.Padding(3);
            tabParam.Size = new System.Drawing.Size(676, 588);
            tabParam.TabIndex = 0;
            tabParam.Text = "参数定义";
            tabParam.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            dataGridView2.AllowUserToDeleteRows = false;
            dataGridView2.AllowUserToResizeRows = false;
            dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { colVarName, colDataType, colDefaultValue, colDisplayName, colRemark });
            dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            dataGridView2.Location = new System.Drawing.Point(3, 3);
            dataGridView2.Name = "dataGridView2";
            dataGridView2.RowHeadersVisible = false;
            dataGridView2.RowHeadersWidth = 51;
            dataGridView2.RowTemplate.Height = 29;
            dataGridView2.Size = new System.Drawing.Size(670, 582);
            dataGridView2.TabIndex = 2;
            // 
            // colVarName
            // 
            colVarName.HeaderText = "变量名";
            colVarName.MinimumWidth = 6;
            colVarName.Name = "colVarName";
            colVarName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            colVarName.Width = 150;
            // 
            // colDataType
            // 
            colDataType.HeaderText = "数据类型";
            colDataType.MinimumWidth = 6;
            colDataType.Name = "colDataType";
            colDataType.Width = 125;
            // 
            // colDefaultValue
            // 
            colDefaultValue.HeaderText = "默认值";
            colDefaultValue.MinimumWidth = 6;
            colDefaultValue.Name = "colDefaultValue";
            colDefaultValue.Width = 150;
            // 
            // colDisplayName
            // 
            colDisplayName.HeaderText = "显示名";
            colDisplayName.MinimumWidth = 6;
            colDisplayName.Name = "colDisplayName";
            colDisplayName.Width = 150;
            // 
            // colRemark
            // 
            colRemark.HeaderText = "备注";
            colRemark.MinimumWidth = 6;
            colRemark.Name = "colRemark";
            colRemark.Width = 300;
            // 
            // tabScriptEdit
            // 
            tabScriptEdit.Controls.Add(dgvCmds);
            tabScriptEdit.Location = new System.Drawing.Point(4, 29);
            tabScriptEdit.Name = "tabScriptEdit";
            tabScriptEdit.Padding = new System.Windows.Forms.Padding(3);
            tabScriptEdit.Size = new System.Drawing.Size(676, 588);
            tabScriptEdit.TabIndex = 1;
            tabScriptEdit.Text = "编辑脚本";
            tabScriptEdit.UseVisualStyleBackColor = true;
            // 
            // dgvCmds
            // 
            dgvCmds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvCmds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { colEdit, colRowNo, colCmd, colContent });
            dgvCmds.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvCmds.Location = new System.Drawing.Point(3, 3);
            dgvCmds.MultiSelect = false;
            dgvCmds.Name = "dgvCmds";
            dgvCmds.RowHeadersVisible = false;
            dgvCmds.RowHeadersWidth = 51;
            dgvCmds.RowTemplate.Height = 29;
            dgvCmds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgvCmds.Size = new System.Drawing.Size(670, 582);
            dgvCmds.TabIndex = 1;
            // 
            // colEdit
            // 
            colEdit.HeaderText = "编辑";
            colEdit.MinimumWidth = 6;
            colEdit.Name = "colEdit";
            colEdit.ReadOnly = true;
            colEdit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            colEdit.Width = 60;
            // 
            // colRowNo
            // 
            colRowNo.HeaderText = "行号";
            colRowNo.MinimumWidth = 6;
            colRowNo.Name = "colRowNo";
            colRowNo.ReadOnly = true;
            colRowNo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            colRowNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            colRowNo.Width = 50;
            // 
            // colCmd
            // 
            colCmd.HeaderText = "命令";
            colCmd.MinimumWidth = 6;
            colCmd.Name = "colCmd";
            colCmd.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            colCmd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            colCmd.Width = 125;
            // 
            // colContent
            // 
            colContent.HeaderText = "内容";
            colContent.MinimumWidth = 6;
            colContent.Name = "colContent";
            colContent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            colContent.Width = 500;
            // 
            // tabScriptView
            // 
            tabScriptView.Controls.Add(txtScript);
            tabScriptView.Location = new System.Drawing.Point(4, 29);
            tabScriptView.Name = "tabScriptView";
            tabScriptView.Padding = new System.Windows.Forms.Padding(3);
            tabScriptView.Size = new System.Drawing.Size(676, 588);
            tabScriptView.TabIndex = 2;
            tabScriptView.Text = "查看脚本";
            tabScriptView.UseVisualStyleBackColor = true;
            // 
            // txtScript
            // 
            txtScript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            txtScript.Dock = System.Windows.Forms.DockStyle.Fill;
            txtScript.Location = new System.Drawing.Point(3, 3);
            txtScript.Name = "txtScript";
            txtScript.Size = new System.Drawing.Size(670, 582);
            txtScript.TabIndex = 0;
            txtScript.Text = resources.GetString("txtScript.Text");
            // 
            // panel5
            // 
            panel5.Controls.Add(lstShapeType);
            panel5.Controls.Add(panel6);
            panel5.Dock = System.Windows.Forms.DockStyle.Left;
            panel5.Location = new System.Drawing.Point(0, 0);
            panel5.Name = "panel5";
            panel5.Size = new System.Drawing.Size(180, 621);
            panel5.TabIndex = 7;
            // 
            // lstShapeType
            // 
            lstShapeType.Dock = System.Windows.Forms.DockStyle.Fill;
            lstShapeType.FormattingEnabled = true;
            lstShapeType.IntegralHeight = false;
            lstShapeType.ItemHeight = 20;
            lstShapeType.Location = new System.Drawing.Point(0, 29);
            lstShapeType.Name = "lstShapeType";
            lstShapeType.Size = new System.Drawing.Size(180, 592);
            lstShapeType.TabIndex = 0;
            // 
            // panel6
            // 
            panel6.Controls.Add(cboShapeCategory);
            panel6.Dock = System.Windows.Forms.DockStyle.Top;
            panel6.Location = new System.Drawing.Point(0, 0);
            panel6.Name = "panel6";
            panel6.Size = new System.Drawing.Size(180, 29);
            panel6.TabIndex = 9;
            // 
            // cboShapeCategory
            // 
            cboShapeCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            cboShapeCategory.FormattingEnabled = true;
            cboShapeCategory.Location = new System.Drawing.Point(0, 0);
            cboShapeCategory.Name = "cboShapeCategory";
            cboShapeCategory.Size = new System.Drawing.Size(180, 28);
            cboShapeCategory.TabIndex = 0;
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            splitContainer1.Location = new System.Drawing.Point(0, 49);
            splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(panel4);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(panel2);
            splitContainer1.Size = new System.Drawing.Size(1510, 621);
            splitContainer1.SplitterDistance = 864;
            splitContainer1.TabIndex = 0;
            // 
            // ShapeEditor
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1510, 670);
            Controls.Add(splitContainer1);
            Controls.Add(panel1);
            Name = "ShapeEditor";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "Shape2d 编辑器";
            WindowState = System.Windows.Forms.FormWindowState.Maximized;
            panel1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel3.ResumeLayout(false);
            panel4.ResumeLayout(false);
            tabControl1.ResumeLayout(false);
            tabParam.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView2).EndInit();
            tabScriptEdit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvCmds).EndInit();
            tabScriptView.ResumeLayout(false);
            panel5.ResumeLayout(false);
            panel6.ResumeLayout(false);
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnAddCmd;
        private System.Windows.Forms.Button btnRemoveCmd;
        private System.Windows.Forms.Button btnEditCmd;
        private System.Windows.Forms.Button btnRunScript;
        private System.Windows.Forms.Button btnRunScriptStep;
        private System.Windows.Forms.Button btnAddShapeType;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private SkiaSharp.Views.Desktop.SKGLControl skGL;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox txtDebug;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabParam;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVarName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colDataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDefaultValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRemark;
        private System.Windows.Forms.TabPage tabScriptEdit;
        private System.Windows.Forms.DataGridView dgvCmds;
        private System.Windows.Forms.DataGridViewImageColumn colEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRowNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCmd;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContent;
        private System.Windows.Forms.TabPage tabScriptView;
        private System.Windows.Forms.RichTextBox txtScript;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListBox lstShapeType;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox cboShapeCategory;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}