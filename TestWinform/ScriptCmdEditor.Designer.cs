﻿namespace TestWinform
{
    partial class ScriptCmdEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lstCmd = new System.Windows.Forms.ListBox();
            dgvArgs = new System.Windows.Forms.DataGridView();
            colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            colContent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            btnOK = new System.Windows.Forms.Button();
            btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)dgvArgs).BeginInit();
            SuspendLayout();
            // 
            // lstCmd
            // 
            lstCmd.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            lstCmd.FormattingEnabled = true;
            lstCmd.IntegralHeight = false;
            lstCmd.ItemHeight = 20;
            lstCmd.Location = new System.Drawing.Point(12, 10);
            lstCmd.Name = "lstCmd";
            lstCmd.Size = new System.Drawing.Size(214, 444);
            lstCmd.TabIndex = 0;
            // 
            // dgvArgs
            // 
            dgvArgs.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            dgvArgs.BackgroundColor = System.Drawing.SystemColors.Control;
            dgvArgs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvArgs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { colName, colContent });
            dgvArgs.Location = new System.Drawing.Point(232, 12);
            dgvArgs.Name = "dgvArgs";
            dgvArgs.RowHeadersVisible = false;
            dgvArgs.RowHeadersWidth = 51;
            dgvArgs.RowTemplate.Height = 29;
            dgvArgs.Size = new System.Drawing.Size(523, 442);
            dgvArgs.TabIndex = 1;
            // 
            // colName
            // 
            colName.Frozen = true;
            colName.HeaderText = "名称";
            colName.MinimumWidth = 6;
            colName.Name = "colName";
            colName.ReadOnly = true;
            colName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            colName.Width = 125;
            // 
            // colContent
            // 
            colContent.HeaderText = "内容";
            colContent.MinimumWidth = 6;
            colContent.Name = "colContent";
            colContent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            colContent.Width = 300;
            // 
            // btnOK
            // 
            btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnOK.Location = new System.Drawing.Point(519, 465);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(110, 29);
            btnOK.TabIndex = 2;
            btnOK.Text = "确定";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            btnCancel.Location = new System.Drawing.Point(645, 465);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(110, 29);
            btnCancel.TabIndex = 3;
            btnCancel.Text = "取消";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // ScriptCmdEditor
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(767, 506);
            Controls.Add(btnCancel);
            Controls.Add(btnOK);
            Controls.Add(dgvArgs);
            Controls.Add(lstCmd);
            Name = "ScriptCmdEditor";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "脚本命令编辑";
            Load += ScriptCmdEditor_Load;
            ((System.ComponentModel.ISupportInitialize)dgvArgs).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ListBox lstCmd;
        private System.Windows.Forms.DataGridView dgvArgs;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContent;
    }
}