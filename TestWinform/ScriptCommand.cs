﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWinform
{
    
    public class ScriptCommand
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Extent { get; set; }   
        public bool IsDebug { get; set; }
        public bool IsDrawing { get; set; }
        public List<KeyValuePair<string, string>> Parameters { get; set; }

        public ScriptCommand Clone()
        {
            var clone = new ScriptCommand();
            clone.Name = Name;
            clone.Description = Description;
            clone.Extent = Extent;
            clone.Parameters = new List<KeyValuePair<string, string>>();
            foreach(var kvp in this.Parameters)
            {
                clone.Parameters.Add(new KeyValuePair<string, string>(kvp.Key, kvp.Value));
            }
            clone.IsDebug = IsDebug;
            clone.IsDrawing = IsDrawing;
            return clone;
        }


        public static ScriptCommandCollection List = new ScriptCommandCollection
        {
             new ScriptCommand
            {
                Name="SetObject",
                Description="设置对象",
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("对象名","string"),
                }
            },
            new ScriptCommand
            {
                Name="CallFunc",
                Description="调用函数",
                Extent="x:bool",//返回值，赋值变量x，类型bool
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("函数名","string"),
                    new KeyValuePair<string, string>("参数1",""),
                    new KeyValuePair<string, string>("参数2",""),
                    new KeyValuePair<string, string>("参数3",""),
                    new KeyValuePair<string, string>("参数4",""),
                    new KeyValuePair<string, string>("参数5",""),
               }
            },
            new ScriptCommand
            {
                Name="SetValue",
                Description="设置变量",
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("变量名","int"),//float int bool string point 
                    new KeyValuePair<string, string>("表达式",""),
                }
            },
            new ScriptCommand
            {
                Name="DrawLine",
                Description="绘制直线",
                IsDrawing=true,
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("起点X","float"),
                    new KeyValuePair<string, string>("起点Y","float"),
                    new KeyValuePair<string, string>("终点X","float"),
                    new KeyValuePair<string, string>("终点Y","float"),
                }
            },
            new ScriptCommand
            {
                Name="IfStart",
                Description="条件开始",
                Extent="AND",
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("表达式1","bool"),
                    new KeyValuePair<string, string>("表达式2","bool"),
                    new KeyValuePair<string, string>("表达式3","bool"),
                    new KeyValuePair<string, string>("表达式4","bool"),
                }
            },
            new ScriptCommand
            {
                Name="IfEnd",
                Description="前续条件结束",
            },
            new ScriptCommand
            {
                Name="IfElse",
                Description="前续条件结束，否则条件开始",
                Extent="AND",
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("表达式1","bool"),
                    new KeyValuePair<string, string>("表达式2","bool"),
                    new KeyValuePair<string, string>("表达式3","bool"),
                    new KeyValuePair<string, string>("表达式4","bool"),
                }
            },
             new ScriptCommand
            {
                Name="ElseEnd",
                Description="否则条件结束",
            },

            new ScriptCommand
            {
                Name="ForStart",
                Description="For循环开始",
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("起始变量","int"),
                    new KeyValuePair<string, string>("步进表达式","string"),
                    new KeyValuePair<string, string>("结束条件","bool"),
                }
            }, 
            
            new ScriptCommand
            {
                Name="FoEnd",
                Description="For循环结束",
            },
        };
    }

    public class ScriptCommandCollection : KeyedCollection<string, ScriptCommand>
    {
        protected override string GetKeyForItem(ScriptCommand item)
        {
            return item.Name;
        }
    }

}
