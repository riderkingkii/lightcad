﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestWinform
{
    public partial class PropertyGridForm : Form
    {
        public static void ShowDialog(object propObj)
        {
            var form = new PropertyGridForm(propObj);
            form.ShowDialog();
        }
        public PropertyGridForm()
        {
            InitializeComponent();
        }
        public PropertyGridForm(object propObj)
        {
            InitializeComponent();
            this.propertyGrid1.SelectedObject = propObj;
        }
    }
}
