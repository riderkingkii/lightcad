﻿using SkiaSharp.Views.Desktop;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LightCAD.Runtime;
using LightCAD.Core;
using System.Text.Json.Serialization;
using System.Numerics;
using System.IO;
using System.Reflection;
using TestWinform.Properties;
using netDxf.Entities;

namespace TestWinform
{
    public partial class ShapeEditor : Form
    {
        SKSurface surface;
        public ShapeEditor()
        {
            InitializeComponent();
            this.skGL.PaintSurface += SkGL_PaintSurface;
            this.skGL.MouseClick += SkGL_MouseClick;
        }

        private void SkGL_MouseClick(object? sender, MouseEventArgs e)
        {

        }

        private DateTime lastTime;
        private int frameIndex;
        public double FPS;
        public string TitleExt;
        protected void CalFPS()
        {
            if (lastTime == DateTime.MinValue)
            {
                lastTime = DateTime.Now;
            }
            frameIndex++;
            var delta = (DateTime.Now - lastTime).TotalSeconds;
            if (delta > 1)
            {
                FPS = Math.Round(frameIndex / delta, 1);
                lastTime = DateTime.Now;
                frameIndex = 0;
            }
            this.Text = FPS.ToString("0.00");
        }

        //https://learn.microsoft.com/zh-cn/xamarin/xamarin-forms/user-interface/graphics/skiasharp/curves/effects
        private void SkGL_PaintSurface(object? sender, SKPaintGLSurfaceEventArgs e)
        {
            CalFPS();
            var backRender = e.BackendRenderTarget;
            var canvas = e.Surface.Canvas;

        }

        private void btnAddCmd_Click(object sender, EventArgs e)
        {
            var editor = new ScriptCmdEditor();
            if (editor.ShowDialog() == DialogResult.OK)
            {

            }

        }

        private void btnRemoveCmd_Click(object sender, EventArgs e)
        {

        }

        private void btnEditCmd_Click(object sender, EventArgs e)
        {
            if (dgvCmds.SelectedRows.Count == 0)
            {
                ShowDebug("请选中脚本命令！");
                return;
            }
            var row = dgvCmds.SelectedRows[0];
            var cmd = row.Tag as ScriptCommand;
            var editor = new ScriptCmdEditor(cmd);
            if (editor.ShowDialog() == DialogResult.OK)
            {

            }
        }

        public void ShowDebug(string info)
        {
            txtDebug.AppendText(info + "\n");
        }
    }
}






//struct point
//{
//    long long x, y;
//};

//long long cross_product(point a, point b){
//    return a.x * b.y - b.x * a.y;
//}

//long long sq_dist(point a, point b){
//    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
//}

//bool is_inside(long long x, long long y, vector<point>&points)
//{
//    point p1 = { points[points.size() - 1].x - points[0].x, points[points.size() - 1].y - points[0].y };
//    point p2 = { points[1].x - points[0].x, points[1].y - points[0].y };
//    point pq = { x - points[0].x, y - points[0].y };

//    if (!(cross_product(p1, pq) <= 0 && cross_product(p2, pq) >= 0)) return false;

//    int l = 0, r = points.size();
//    while (r - l > 1)
//    {
//        int mid = (l + r) / 2;
//        point cur = { points[mid].x - points[0].x, points[mid].y - points[0].y };
//        if (cross_product(cur, pq) < 0)
//        {
//            r = mid;
//        }
//        else
//        {
//            l = mid;
//        }
//    }

//    if (l == points.size() - 1)
//    {
//        return sq_dist(points[0], { x,y}) <= sq_dist(points[0], points[l]);
//    }else
//{
//    point l_l1 = { points[l + 1].x - points[l].x, points[l + 1].y - points[l].y };
//    point lq = { x - points[l].x, y - points[l].y };
//    return (cross_product(l_l1, lq) >= 0);
//}
//}

//void pick_p0(vector<point>&points)
//{
//    long long min_x = 1e9 + 5;
//    long long max_y = -1e9 - 5;
//    int min_i = 0;
//    for (int i = 0; i < points.size(); i++)
//    {
//        if (points[i].x < min_x || (points[i].x == min_x && points[i].y > max_y))
//        {
//            min_x = points[i].x;
//            max_y = points[i].y;
//            min_i = i;
//        }
//    }

//    rotate(points.begin(), points.begin() + min_i, points.end());
//}
