﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestWinform
{

    public partial class ScriptCmdEditor : Form
    {
        private ScriptCommand command;
        public ScriptCmdEditor()
        {
            InitializeComponent();
        }
        public ScriptCmdEditor(ScriptCommand command)
        {
            InitializeComponent();
            this.command = command;
        }

        private void ScriptCmdEditor_Load(object sender, EventArgs e)
        {
            for (var i = 0; i < 20; i++)
            {
                dgvArgs.Rows.Add();
            }
        }
    }
}
