﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestWinform
{
    public class UserHair
    {
        public int Length { get; set; }
        public int Color { get; set; }
    }

    public class Person
    {
        private int height;
        public bool isMarray;
        internal int size;
        public string Name { get; set; }
        public int Age { get; internal set; } = 39;
        public List<string> Books { get; set; } = new List<string>();

        [DefaultValue(20)]
        public int Weight { get; set; } = 40;

        public bool IsFat
        {
            get
            {
                return Weight > 100;
            }
        }
        public Dictionary<string, UserHair> Hairs { get; set; } = new Dictionary<string, UserHair>();
    }
    public class JDoc
    {
        public ElementCollection Elements { get; set; } = new ElementCollection();
    }
    public class JsonTest
    {
        public static void TestDoc(LcDocument doc)
        {
            doc.Info.Author = "Rider";
            doc.ModelSpace.AddLine(new Vector2(100, 100), new Vector2(300, 300));
            doc.ModelSpace.AddCircle(new Vector2(300, 300), 100);
            LcDocument.Save(doc, @"c:\temp\test.ldwg");
        }
        public static void Test()
        {
            var jdoc = new JDoc();
            jdoc.Elements.Add(new LcLine() { Start = new Vector2(100, 100), End = new Vector2(200, 200) });
            var options = new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                WriteIndented = true,
                IncludeFields = true,
            };
            var json = JsonSerializer.Serialize<JDoc>(jdoc, options);
            MessageBox.Show(json);
            jdoc = JsonSerializer.Deserialize<JDoc>(json);
            PropertyGridForm.ShowDialog(jdoc);

        }
        public static void SerializeAndDeserialize()
        {
            var person = new Person() { isMarray = true, size = 20 };
            person.Books.Add("HELLO");
            var options = new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                WriteIndented = true,
                IncludeFields = true,
            };
            var json = JsonSerializer.Serialize<Person>(person, options);
            MessageBox.Show(json);
            person = JsonSerializer.Deserialize<Person>(json);
            PropertyGridForm.ShowDialog(person);
        }
        public static void WriterAndReader()
        {
            var person = new Person() { isMarray = true, size = 20 };
            person.Books.Add("HELLO");
            person.Books.Add("WORLD");
            person.Hairs.Add("aa", new UserHair { Color = 4 });
            person.Hairs.Add("bb", new UserHair { Color = 5 });

            var options = new JsonWriterOptions
            {
                Indented = true
            };
            var jsonOptions = new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                WriteIndented = true,
                IncludeFields = true,
            };
            using var stream = new MemoryStream();
            using var writer = new Utf8JsonWriter(stream, options);
            JsonWriterObject(writer, person, jsonOptions);
            string json = Encoding.UTF8.GetString(stream.ToArray());
            MessageBox.Show(json);

            person = JsonSerializer.Deserialize<Person>(json);
            PropertyGridForm.ShowDialog(person);

        }

        public static void JsonWriterObject(Utf8JsonWriter writer, object obj, JsonSerializerOptions options)
        {
            var objType = obj.GetType();
            var objTypeName = objType.FullName;
            if (objType.IsValueType || objTypeName == "System.String" || objTypeName == "System.Nullable")
            {
                JsonSerializer.Serialize(writer, obj, objType, options);
                return;
            }

            var props = objType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            writer.WriteStartObject();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes();
                object defaultValue = null;
                bool hasJsonIgnore = false;
                bool hasJsonInclude = false;
                foreach (var attr in attrs)
                {
                    if (attr is DefaultValueAttribute)
                    {
                        defaultValue = (attr as DefaultValueAttribute).Value;
                    }
                    else if (attr is JsonIgnoreAttribute)
                    {
                        hasJsonIgnore = true;
                    }
                    if (attr is JsonIncludeAttribute)
                    {
                        hasJsonInclude = true;
                    }
                }
                var propTypeName = prop.PropertyType.FullName;
                var value = prop.GetValue(obj);
                if (prop.PropertyType.IsValueType || propTypeName == "System.String" || propTypeName == "System.Nullable")
                {
                    object typeDefaultValue = prop.PropertyType.IsValueType ? Activator.CreateInstance(prop.PropertyType) : null;

                    var getMethod = prop.GetGetMethod();
                    if ((getMethod.IsPublic && !hasJsonIgnore)
                        || (!getMethod.IsPublic && hasJsonInclude))
                    {
                        if (value != null && !value.Equals(typeDefaultValue))
                        {
                            if (defaultValue == null || !value.Equals(defaultValue))
                            {
                                writer.WritePropertyName(prop.Name);
                                JsonSerializer.Serialize(writer, value, prop.PropertyType, options);
                            }
                        }

                    }
                }
                else if (value != null)
                {
                    if (value is ICollection && (value as ICollection).Count > 0)
                    {
                        if (value is IDictionary)
                        {
                            var dict = value as IDictionary;
                            writer.WritePropertyName(prop.Name);
                            writer.WriteStartObject();
                            foreach (DictionaryEntry item in dict)
                            {
                                writer.WritePropertyName(item.Key.ToString());
                                if (item.Value == null)
                                    writer.WriteNullValue();
                                else
                                    JsonWriterObject(writer, item.Value, options);
                            }
                            writer.WriteEndObject();
                        }
                        else
                        {
                            var collection = value as ICollection;
                            writer.WritePropertyName(prop.Name);
                            writer.WriteStartArray();
                            foreach (var item in collection)
                            {
                                JsonWriterObject(writer, item, options);
                            }
                            writer.WriteEndArray();
                        }
                    }
                }
            }
            writer.WriteEndObject();
            writer.Flush();
            // var fields = objType.GetFields();

        }
    }
}
