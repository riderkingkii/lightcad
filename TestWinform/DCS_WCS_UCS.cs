using System.Drawing;
using System.Windows.Forms;
using System.Numerics;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TestWinform
{
    public partial class DCS_WCS_UCS : Form
    {
        public DCS_WCS_UCS()
        {
            InitializeComponent();
            this.MouseDown += DCS_WCS_UCS_MouseDown;
            this.MouseMove += DCS_WCS_UCS_MouseMove;
            this.MouseUp += DCS_WCS_UCS_MouseUp;
            this.MouseWheel += DCS_WCS_UCS_MouseWheel;
        }


        private void DCS_WCS_UCS_MouseDown(object? sender, MouseEventArgs e)
        {
            downPos = e.Location;

            if (e.Button == MouseButtons.Right)
            {
                this.isPan = true;
                downWcsOrg = wcsOrg;
            }
            else
            {

                if (rdoScreen.Checked)
                {
                    var vec = new Vector2(e.Location.X, e.Location.Y);
                    draws.Add(new KeyValuePair<string, Vector2>(rdoScreen.Name, vec));
                }
                if (rdoWorld.Checked)
                {
                    Matrix3x2.Invert(matWcs, out Matrix3x2 matWcsInvert);
                    var vec = Vector2.Transform(new Vector2(e.Location.X, e.Location.Y), matWcsInvert);
                    var vec1 = Vector2.Transform(vec, matWcs);
                    //Debug.Print(matWcs.ToString()+" ->DCS:"+e.Location.ToString()+" ->WCS:"+vec.ToString());
                    draws.Add(new KeyValuePair<string, Vector2>(rdoWorld.Name, vec));
                }
                if (rdoUser.Checked)
                {
                    Matrix3x2.Invert(matUcs, out Matrix3x2 matUcsInvert);
                    var vec = Vector2.Transform(new Vector2(e.Location.X, e.Location.Y), matUcsInvert);
                    draws.Add(new KeyValuePair<string, Vector2>(rdoUser.Name, vec));
                }
                this.Invalidate();

            }
        }
        private void DCS_WCS_UCS_MouseMove(object? sender, MouseEventArgs e)
        {
            movePos = e.Location;
            if (isPan)
            {
                PanWorld();
                this.Invalidate();
            }
        }

        private void DCS_WCS_UCS_MouseUp(object? sender, MouseEventArgs e)
        {
            this.isPan = false;
        }

        private void DCS_WCS_UCS_MouseWheel(object? sender, MouseEventArgs e)
        {
            //定点缩放是一个动态过程，不能放在渲染过程，需要即时处理（所有矩阵刷新都应该在渲染过程外处理）
            //Matrix3x2.Invert(matCwcs, out Matrix3x2 matInvert);
            //var point = Vector2.Transform(new Vector2(e.Location.X, e.Location.Y), matInvert);
            //var scaleOrg = new Vector2((point.X *scale ), (point.Y*scale));
            //var delta = scaleOrg - wcsOrg;
            ////Debug.Print("Scale:" + scale.ToString() + "CenterDCS:" + e.Location.ToString() + "CenterWCS:" + scaleCenter.ToString());
            //Vector2 scaleDetal;
            if (e.Delta > 0)
            {
                //scaleDetal = delta* 1.25f;
                scale *= 1.25f;//将世界坐标的内容放大
            }
            else
            {
                //scaleDetal = delta * 0.8f;
                scale *= 0.8f;//将世界坐标的内容缩小
            }
            //var delta1 = delta - scaleDetal;
            //wcsOrg += delta1;

            this.lblScale.Text = scale.ToString();
            this.ResetMatrix();
            this.Invalidate();
        }
        private Point downPos;
        private Point movePos;
        private Vector2 downWcsOrg;
        private Vector2 scaleCenter = new Vector2(-200, 200);
        private bool isPan = false;
        private float scale = 1.0f;
        private Matrix3x2 matCwcs;
        private Matrix3x2 matWcs;
        private Matrix3x2 matUcs;
        private Graphics g;
        private int w;
        private int h;

        private string viewType = "WCS";

        List<KeyValuePair<string, Vector2>> draws = new List<KeyValuePair<string, Vector2>>();
        Vector2 wcsOrg = new Vector2(-100, -100);
        Vector2 ucsOrg = new Vector2(100, 100);

        private void PanWorld()
        {
            var dx = movePos.X - downPos.X;
            var dy = movePos.Y - downPos.Y;
            var wx = dx / scale;
            var wy = -dy / scale;//由于屏幕坐标与世界坐标Y轴相反
            wcsOrg = Vector2.Add(downWcsOrg, new Vector2(wx, wy));
            this.ResetMatrix();
            this.Invalidate();
        }

        private void ResetMatrix()
        {
            //CenterWCS 中心世界坐标系，承接WCS与DCS的关联，在位置上固定，无法作为绘图空间
            this.matCwcs = Matrix3x2.Multiply(Matrix3x2.CreateTranslation(w / 2, -h / 2), Matrix3x2.CreateScale(1, -1));

            //WCS
            Matrix3x2 wmat = Matrix3x2.Multiply(Matrix3x2.CreateTranslation(wcsOrg.X, wcsOrg.Y), Matrix3x2.CreateScale(scale));
            this.matWcs = Matrix3x2.Multiply(wmat, this.matCwcs);

            //UCS
            Matrix3x2 umat = Matrix3x2.Multiply(Matrix3x2.CreateRotation((float)Math.PI / 4), Matrix3x2.CreateTranslation(ucsOrg.X, ucsOrg.Y));
            this.matUcs = Matrix3x2.Multiply(umat, this.matWcs);

            if (viewType == "UCS")
            {
                var rtmat = Matrix3x2.CreateRotation(-(float)Math.PI / 4);
                this.matWcs= Matrix3x2.Multiply(rtmat, this.matWcs);

                this.matUcs = Matrix3x2.Multiply(rtmat, this.matUcs);
            }
           

        }
        private bool inited;
        private void DCS_WCS_UCS_Paint(object sender, PaintEventArgs e)
        {
            this.g = e.Graphics;
            g.Clear(Color.White);
            w = e.ClipRectangle.Width;
            h = e.ClipRectangle.Height;
            if (!inited)
            {
                ResetMatrix();
                inited = true;
            }

            //DCS
            DrawLineDcs(new Vector2(0, 0), new Vector2(w, 0));
            DrawLineDcs(new Vector2(0, 0), new Vector2(0, h));

            DrawLineCwcs(new Vector2(-w / 2, 0), new Vector2(w / 2, 0));
            DrawLineCwcs(new Vector2(0, h / 2), new Vector2(0, -h / 2));
            //DrawCircleCwcs(new Vector2(100, 100), 30);

            DrawLineWcs(new Vector2(-w / 2, 0), new Vector2(w / 2, 0));
            DrawLineWcs(new Vector2(0, h / 2), new Vector2(0, -h / 2));
            //DrawCircleWcs(new Vector2(100, 100), 30);

            DrawLineUcs(new Vector2(-w / 2, 0), new Vector2(w / 2, 0));
            DrawLineUcs(new Vector2(0, h / 2), new Vector2(0, -h / 2));
            //DrawCircleUcs(new Vector2(100, 100), 30);


            if (draws.Count > 0)
            {
                foreach (var circle in draws)
                {
                    var type = circle.Key;
                    var c = circle.Value;
                    var r = 30;
                    switch (type)
                    {
                        case "rdoScreen": g.DrawLine(Pens.Red, 0,0,c.X, c.Y); break;
                        case "rdoCworld": DrawLineCwcs(new Vector2(), c); break;
                        case "rdoWorld": DrawLineWcs(new Vector2(), c); break;
                        case "rdoUser": DrawLineUcs(new Vector2(), c); break;
                    }
                }
            }
        }
        private void DrawLineDcs(Vector2 p0, Vector2 p1)
        {
            g.DrawLine(Pens.Red, p0.X, p0.Y, p1.X, p1.Y);
        }
        private void DrawCircleDcs(Vector2 c, float r)
        {
            var lt = new Vector2(c.X - r, c.Y + r);
            g.DrawEllipse(Pens.Red, new RectangleF(lt.X, lt.Y, r * 2, r * 2));
        }
        private void DrawLineCwcs(Vector2 p0, Vector2 p1)
        {
            var p0_m = Vector2.Transform(p0, matCwcs);
            var p1_m = Vector2.Transform(p1, matCwcs);
            g.DrawLine(Pens.Blue, p0_m.X, p0_m.Y, p1_m.X, p1_m.Y);
        }
        private void DrawCircleCwcs(Vector2 c, float r)
        {
            var sr = scale * r;
            var lt = new Vector2(c.X - sr, c.Y + sr);
            var lt_m = Vector2.Transform(lt, matCwcs);
            g.DrawEllipse(Pens.Blue, new RectangleF(lt_m.X, lt_m.Y, sr * 2, sr * 2));
        }

        private void DrawLineWcs(Vector2 p0, Vector2 p1)
        {
            var p0_m = Vector2.Transform(p0, matWcs);
            var p1_m = Vector2.Transform(p1, matWcs);
            g.DrawLine(Pens.Green, p0_m.X, p0_m.Y, p1_m.X, p1_m.Y);
        }
        private void DrawCircleWcs(Vector2 c, float r)
        {
            var sr = scale * r;
            //由于坐标系缩放，不能用矩形去变换
            var cm = Vector2.Transform(c, matWcs);
            g.DrawEllipse(Pens.Green, new RectangleF(cm.X - sr, cm.Y - sr, sr * 2, sr * 2));
        }


        private void DrawLineUcs(Vector2 p0, Vector2 p1)
        {
            var p0_m = Vector2.Transform(p0, matUcs);
            var p1_m = Vector2.Transform(p1, matUcs);
            g.DrawLine(Pens.Silver, p0_m.X, p0_m.Y, p1_m.X, p1_m.Y);
        }
        private void DrawCircleUcs(Vector2 c, float r)
        {
            var sr = scale * r;
            //由于坐标系旋转，矩形转换后不再是矩形
            var cm = Vector2.Transform(c, matUcs);
            g.DrawEllipse(Pens.Silver, new RectangleF(cm.X - sr, cm.Y - sr, sr * 2, sr * 2));
        }

        private void btnDrawCircle_Click(object sender, EventArgs e)
        {
            var parts = txtCoord.Text.Split(',');
            var x = float.Parse(parts[0]);
            var y = float.Parse(parts[1]);
            var vec = new Vector2(x, y);
            if (rdoScreen.Checked) draws.Add(new KeyValuePair<string, Vector2>(rdoScreen.Name, vec));
            if (rdoWorld.Checked) draws.Add(new KeyValuePair<string, Vector2>(rdoWorld.Name, vec));
            if (rdoUser.Checked) draws.Add(new KeyValuePair<string, Vector2>(rdoUser.Name, vec));
            this.Invalidate();
        }
        private void btnDraw_Click(object sender, EventArgs e)
        {
            var parts = txtCoord.Text.Split(',');
            var x = float.Parse(parts[0]);
            var y = float.Parse(parts[1]);
            var vec = new Vector2(x, y);
            if (rdoScreen.Checked) draws.Add(new KeyValuePair<string, Vector2>(rdoScreen.Name, vec));
            if (rdoWorld.Checked) draws.Add(new KeyValuePair<string, Vector2>(rdoWorld.Name, vec));
            if (rdoUser.Checked) draws.Add(new KeyValuePair<string, Vector2>(rdoUser.Name, vec));
            this.Invalidate();
        }
        private void btnWcsView_Click(object sender, EventArgs e)
        {
            this.viewType = "WCS";
            this.ResetMatrix();
            this.Invalidate();
        }

        private void btnUcsView_Click(object sender, EventArgs e)
        {
            this.viewType = "UCS";
            this.ResetMatrix();
            this.Invalidate();
        }


    }
}