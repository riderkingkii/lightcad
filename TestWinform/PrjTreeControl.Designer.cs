﻿using System.Drawing;
using System.Windows.Forms;

namespace TestWinform
{
    partial class PrjTreeControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            TreeNode treeNode1 = new TreeNode("图纸");
            TreeNode treeNode2 = new TreeNode("模型");
            TreeNode treeNode3 = new TreeNode("视图");
            TreeNode treeNode4 = new TreeNode("1号楼", new TreeNode[] { treeNode1, treeNode2, treeNode3 });
            TreeNode treeNode5 = new TreeNode("建筑", new TreeNode[] { treeNode4 });
            TreeNode treeNode6 = new TreeNode("结构");
            TreeNode treeNode7 = new TreeNode("xxxxx项目", new TreeNode[] { treeNode5, treeNode6 });
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrjTreeControl));
            panel1 = new Panel();
            treeView1 = new TreeView();
            panel2 = new Panel();
            comboBox1 = new ComboBox();
            button2 = new Button();
            textBox1 = new TextBox();
            toolStrip1 = new ToolStrip();
            toolStripButton1 = new ToolStripButton();
            toolStripButton2 = new ToolStripButton();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            toolStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(treeView1);
            panel1.Controls.Add(panel2);
            panel1.Controls.Add(toolStrip1);
            panel1.Dock = DockStyle.Fill;
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Padding = new Padding(10);
            panel1.Size = new Size(353, 586);
            panel1.TabIndex = 4;
            // 
            // treeView1
            // 
            treeView1.Dock = DockStyle.Fill;
            treeView1.Location = new Point(10, 70);
            treeView1.Name = "treeView1";
            treeNode1.Name = "节点1";
            treeNode1.Text = "图纸";
            treeNode2.Name = "节点3";
            treeNode2.Text = "模型";
            treeNode3.Name = "节点2";
            treeNode3.Text = "视图";
            treeNode4.Name = "节点6";
            treeNode4.Text = "1号楼";
            treeNode5.Name = "节点4";
            treeNode5.Text = "建筑";
            treeNode6.Name = "节点5";
            treeNode6.Text = "结构";
            treeNode7.Name = "节点0";
            treeNode7.Text = "xxxxx项目";
            treeView1.Nodes.AddRange(new TreeNode[] { treeNode7 });
            treeView1.Size = new Size(333, 506);
            treeView1.TabIndex = 2;
            // 
            // panel2
            // 
            panel2.Controls.Add(comboBox1);
            panel2.Controls.Add(button2);
            panel2.Controls.Add(textBox1);
            panel2.Dock = DockStyle.Top;
            panel2.Location = new Point(10, 37);
            panel2.Name = "panel2";
            panel2.Size = new Size(333, 33);
            panel2.TabIndex = 4;
            // 
            // comboBox1
            // 
            comboBox1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            comboBox1.FormattingEnabled = true;
            comboBox1.Location = new Point(239, 5);
            comboBox1.Name = "comboBox1";
            comboBox1.Size = new Size(91, 28);
            comboBox1.TabIndex = 3;
            comboBox1.Text = "所有专业";
            // 
            // button2
            // 
            button2.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button2.Location = new Point(141, 2);
            button2.Name = "button2";
            button2.Size = new Size(50, 29);
            button2.TabIndex = 2;
            button2.Text = "搜索";
            button2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            textBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBox1.Location = new Point(3, 3);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(134, 27);
            textBox1.TabIndex = 1;
            // 
            // toolStrip1
            // 
            toolStrip1.ImageScalingSize = new Size(20, 20);
            toolStrip1.Items.AddRange(new ToolStripItem[] { toolStripButton1, toolStripButton2 });
            toolStrip1.Location = new Point(10, 10);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.Size = new Size(333, 27);
            toolStrip1.TabIndex = 3;
            toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            toolStripButton1.Image = (Image)resources.GetObject("toolStripButton1.Image");
            toolStripButton1.ImageTransparentColor = Color.Magenta;
            toolStripButton1.Name = "toolStripButton1";
            toolStripButton1.Size = new Size(93, 24);
            toolStripButton1.Text = "打开项目";
            // 
            // toolStripButton2
            // 
            toolStripButton2.Alignment = ToolStripItemAlignment.Right;
            toolStripButton2.Image = (Image)resources.GetObject("toolStripButton2.Image");
            toolStripButton2.ImageTransparentColor = Color.Magenta;
            toolStripButton2.Name = "toolStripButton2";
            toolStripButton2.Size = new Size(63, 24);
            toolStripButton2.Text = "管理";
            // 
            // PrjTreeControl
            // 
            AutoScaleDimensions = new SizeF(9F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(panel1);
            Name = "PrjTreeControl";
            Size = new Size(353, 586);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            toolStrip1.ResumeLayout(false);
            toolStrip1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Panel panel1;
        private TreeView treeView1;
        private Panel panel2;
        private ComboBox comboBox1;
        private Button button2;
        private TextBox textBox1;
        private ToolStrip toolStrip1;
        private ToolStripButton toolStripButton1;
        private ToolStripButton toolStripButton2;
    }
}
