﻿using SkiaSharp.Views.Desktop;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LightCAD.Runtime;
using LightCAD.Core;
using System.Text.Json.Serialization;
using System.Numerics;
using System.IO;
using System.Reflection;
using TestWinform.Properties;
using netDxf.Entities;

namespace TestWinform
{
    public partial class SkiaSharpTest : Form
    {
        SKSurface surface;
        System.Timers.Timer timer;
        public SkiaSharpTest()
        {
            InitializeComponent();
            this.skGL.PaintSurface += SkGL_PaintSurface;
            this.Load += SkiaSharpTest_Load;
            this.timer = new System.Timers.Timer();
            this.timer.Interval = 20;
            this.timer.Elapsed += Timer_Elapsed;
            this.timer.Start();
            this.skGL.MouseClick += SkGL_MouseClick;
        }

        private void SkGL_MouseClick(object? sender, MouseEventArgs e)
        {
            var mp = new LightCAD.Core.Vector2(e.X, e.Y);
            var vps = new LightCAD.Core.Vector2[] {
                points[0].ToVector2d(),
                points[2].ToVector2d(),
                points[4].ToVector2d(),
                points[6].ToVector2d()
            };
            var sum = 0;
            var st = DateTime.Now;
            var count = 10000000;
            for (var i = 0; i < count; i++)
            {
                mp.X += 0.001;
                var inPoly = IsPointInPolygon(mp, vps);
                sum += inPoly ? 1 : 0;
            }
            var delta = (DateTime.Now - st).TotalSeconds;

            MessageBox.Show("IN " + sum + "   " + delta + "s");
        }

        private bool inTimer;
        private void Timer_Elapsed(object? sender, System.Timers.ElapsedEventArgs e)
        {
            if (inTimer) return;
            inTimer = true;
            this.Invoke(() =>
            {
                this.skGL.Invalidate();
                inTimer = false;
            });
        }

        private DateTime lastTime;
        private int frameIndex;
        public double FPS;
        public string TitleExt;
        protected void CalFPS()
        {
            if (lastTime == DateTime.MinValue)
            {
                lastTime = DateTime.Now;
            }
            frameIndex++;
            var delta = (DateTime.Now - lastTime).TotalSeconds;
            if (delta > 1)
            {
                FPS = Math.Round(frameIndex / delta, 1);
                lastTime = DateTime.Now;
                frameIndex = 0;
            }
            this.Text = FPS.ToString("0.00");
        }
        private void SkiaSharpTest_Load(object? sender, EventArgs e)
        {
            var hatchPattern = HatchPattern.Load(@"D:\Study\2d-study\LightCAD\ICONS-HATCH\Hatchs\2X2BRIKC.PAT", "2x2brikc");
        }
        SKImage skImg;
        static SKMatrix Multiply(SKMatrix first, SKMatrix second)
        {
            SKMatrix target = SKMatrix.MakeIdentity();
            SKMatrix.Concat(ref target, first, second);
            return target;
        }
        //https://learn.microsoft.com/zh-cn/xamarin/xamarin-forms/user-interface/graphics/skiasharp/curves/effects
        private void SkGL_PaintSurface(object? sender, SKPaintGLSurfaceEventArgs e)
        {
            this.InitSurface();
            CalFPS();
            var backRender = e.BackendRenderTarget;
            var canvas = e.Surface.Canvas;

            Render(canvas);
            //if(skImg==null) 
            //    skImg = RenderImage();
            //canvas.DrawImage(skImg, 0, 0);
            //skImg.Dispose();

            RenderClip(e);

            /** 路径上绘制Path
            const float linkRadius = 30;
            const float linkThickness = 5;

            SKPaint linksPaint = new SKPaint
            {
                Color = SKColors.Silver,
            };
            SKRect outer = new SKRect(-linkRadius, -linkRadius, linkRadius, linkRadius);
            SKRect inner = outer;
            inner.Inflate(-linkThickness, -linkThickness);
            using (SKPath linkPath = new SKPath())
            {
                linkPath.AddArc(outer, 55, 160);
                linkPath.ArcTo(inner, 215, -160, false);
                linkPath.Close();

                linkPath.AddArc(outer, 235, 160);
                linkPath.ArcTo(inner, 395, -160, false);
                linkPath.Close();

                // Set that path as the 1D path effect for linksPaint
                linksPaint.PathEffect =
                    SKPathEffect.Create1DPath(linkPath, 1.3f * linkRadius, 0,
                                              SKPath1DPathEffectStyle.Rotate);
            }
            canvas.DrawRect(0, 0, 400, 400, linksPaint);

            **/
            /**
            SKPaint fillPaint = new SKPaint();

            SKPathEffect horzLinesPath = SKPathEffect.Create2DLine(3, SKMatrix.MakeScale(6, 6));

            SKPathEffect vertLinesPath = SKPathEffect.Create2DLine(1,
                Multiply(SKMatrix.MakeRotationDegrees(90), SKMatrix.MakeScale(24, 24)));

            SKPathEffect diagLinesPath = SKPathEffect.Create2DLine(12,
                Multiply(SKMatrix.MakeScale(36, 36), SKMatrix.MakeRotationDegrees(45)));

            SKPaint strokePaint = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                StrokeWidth = 3,
                Color = SKColors.Black
            };
            using (SKPath roundRectPath = new SKPath())
            {
                var rect = e.BackendRenderTarget.Rect;
                // Create a path
                roundRectPath.AddRoundedRect(
                    new SKRect(50, 50, rect.Width - 50, rect.Height - 50), 100, 100);

                canvas.Save();
                canvas.ClipPath(roundRectPath);
                // Horizontal hatch marks
                //fillPaint.PathEffect = horzLinesPath;
                //fillPaint.Color = SKColors.Red;
                //canvas.DrawPath(roundRectPath, fillPaint);

                // Vertical hatch marks
                fillPaint.PathEffect = vertLinesPath;
                fillPaint.Color = SKColors.Blue;
                canvas.DrawPath(roundRectPath, fillPaint);

                // Diagonal hatch marks -- use clipping
                //fillPaint.PathEffect = diagLinesPath;
                //fillPaint.Color = SKColors.Green;

                canvas.DrawRect(new SKRect(0, 0, rect.Width, rect.Height), fillPaint);
                canvas.Restore();

                //Outline the path
                canvas.DrawPath(roundRectPath, strokePaint);
            }
            **/


            SKPath catPath = SKPath.ParseSvgPathData(
       "M 160 140 L 150 50 220 103" +              // Left ear
       "M 320 140 L 330 50 260 103" +              // Right ear
       "M 215 230 L 40 200" +                      // Left whiskers
       "M 215 240 L 40 240" +
       "M 215 250 L 40 280" +
       "M 265 230 L 440 200" +                     // Right whiskers
       "M 265 240 L 440 240" +
       "M 265 250 L 440 280" +
       "M 240 100" +                               // Head
       "A 100 100 0 0 1 240 300" +
       "A 100 100 0 0 1 240 100 Z" +
       "M 180 170" +                               // Left eye
       "A 40 40 0 0 1 220 170" +
       "A 40 40 0 0 1 180 170 Z" +
       "M 300 170" +                               // Right eye
       "A 40 40 0 0 1 260 170" +
       "A 40 40 0 0 1 300 170 Z");

            SKPaint catStroke = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                StrokeWidth = 5
            };

            SKPath scallopPath =
                SKPath.ParseSvgPathData("M 0 0 L 50 0 A 60 60 0 0 1 -50 0 Z");

            SKPaint framePaint = new SKPaint
            {
                Color = SKColors.Black
            };

            catPath.Transform(SKMatrix.MakeTranslation(-240, -175));

            // Now catPath is 400 by 250
            // Scale it down to 160 by 100
            catPath.Transform(SKMatrix.MakeScale(0.40f, 0.40f));

            // Get the outlines of the contours of the cat path
            SKPath outlinedCatPath = new SKPath();
            catStroke.GetFillPath(catPath, outlinedCatPath);

            // Create a 2D path effect from those outlines
            SKPathEffect fillEffect = SKPathEffect.Create2DPath(
                new SKMatrix
                {
                    ScaleX = 170,
                    ScaleY = 110,
                    TransX = 75,
                    TransY = 80,
                    Persp2 = 1
                },
                outlinedCatPath);

            // Create a 1D path effect from the scallop path
            SKPathEffect strokeEffect =
                SKPathEffect.Create1DPath(scallopPath, 75, 0, SKPath1DPathEffectStyle.Rotate);

            // Set the sum the effects to frame paint
            framePaint.PathEffect = SKPathEffect.CreateSum(fillEffect, strokeEffect);

            var drawPath = new SKPath();
            drawPath.AddCircle(400, 400, 400);
            canvas.ClipPath(drawPath);
            canvas.DrawPath(drawPath, framePaint);

        }
        bool inited;
        SKPaint rectangleFill;
        SKPaint rectangleStroke;
        SKBitmap bitmap = null;
        public void InitSurface()
        {
            if (inited) return;
            inited = true;
            string resourceID = "TestWinform.Resources.Test.png";
            Assembly assembly = GetType().GetTypeInfo().Assembly;
            using (Stream stream = assembly.GetManifestResourceStream(resourceID))
            {
                bitmap = SKBitmap.Decode(stream);
            }

            this.skGL.VSync = false;
            SKImageInfo info = new SKImageInfo
            {
                Width = this.skGL.Width,
                Height = this.skGL.Height,
                ColorType = SKColorType.Bgra8888,
                AlphaType = SKAlphaType.Opaque,
                ColorSpace = SKColorSpace.CreateSrgb()
            };
            this.surface = SKSurface.Create(this.skGL.GRContext, false, info);

            rectangleFill = new SKPaint
            {
                IsAntialias = true,
                Style = SKPaintStyle.Fill,
                Color = SKColors.Red,
                BlendMode = SKBlendMode.SrcOver
            };
            rectangleStroke = new SKPaint
            {
                IsAntialias = true,
                Style = SKPaintStyle.Stroke,
                Color = SKColors.Black,
                StrokeWidth = 1f,
                BlendMode = SKBlendMode.SrcOver
            };

        }
        public SKImage RenderImage()
        {
            SKCanvas canvas = this.surface.Canvas;
            Render(canvas);
            var image = surface.Snapshot();
            return image;
        }
        SKPath skPath = null;
        SKPoint[] points;
        SKPath keyholePath = SKPath.ParseSvgPathData(
       "M 300 130 L 250 350 L 450 350 L 400 130 A 70 70 0 1 0 300 130 Z");
        public void Render(SKCanvas canvas)
        {
            var rectangle = new SKRect
            {
                Left = 20,
                Right = 120,
                Top = 20,
                Bottom = 120
            };

            var random = new Random();
            canvas.Clear(SKColors.SeaGreen);
            canvas.DrawRect(rectangle, rectangleFill);
            canvas.DrawRect(rectangle, rectangleStroke);

            if (points == null)
            {
                // skPath = new SKPath();
                var pointList = new List<SKPoint>();
                for (var i = 0; i < 1; i++)
                {
                    var x0 = random.NextDouble() * this.Width;
                    var y0 = random.NextDouble() * this.Height;
                    var x1 = random.NextDouble() * this.Width;
                    var y1 = random.NextDouble() * this.Height;
                    var x2 = random.NextDouble() * this.Width;
                    var y2 = random.NextDouble() * this.Height;
                    var x3 = random.NextDouble() * this.Width;
                    var y3 = random.NextDouble() * this.Height;
                    pointList.Add(new SKPoint((float)x0, (float)y0));
                    pointList.Add(new SKPoint((float)x1, (float)y1));
                    pointList.Add(new SKPoint((float)x1, (float)y1));
                    pointList.Add(new SKPoint((float)x2, (float)y2));
                    pointList.Add(new SKPoint((float)x2, (float)y2));
                    pointList.Add(new SKPoint((float)x3, (float)y3));
                    pointList.Add(new SKPoint((float)x3, (float)y3));
                    pointList.Add(new SKPoint((float)x0, (float)y0));
                    //var r = random.NextDouble() * 200;
                    //canvas.DrawCircle((float)x0, (float)y0, 200, rectangleStroke);
                    //skPath.MoveTo((float)x0, (float)y0);
                    //skPath.LineTo((float)x1, (float)y1);
                    //canvas.DrawLine((float)x0, (float)y0, (float)x1, (float)y1, rectangleStroke);
                    // DrawArrowLine(canvas, SKColors.LightBlue, new Vector2d(x0, y0), new Vector2d(x1, y1),  ArrowStyle.Default, ArrowStyle.Default);
                }
                points = pointList.ToArray();
            }
            canvas.DrawPoints(SKPointMode.Lines, points, rectangleStroke);

            //canvas.DrawPath(skPath, rectangleStroke);
        }

        void RenderClip(SKPaintGLSurfaceEventArgs e)
        {
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Save();
            // Set the clip path
            canvas.ClipPath(keyholePath);
            var bounds = keyholePath.Bounds;
            // Display monkey to fill height of window but maintain aspect ratio
            using (var paint = new SKPaint { Color = SKColors.Red, IsStroke = true })
            {
                var h = 0;
                while (h < bounds.Height)
                {
                    canvas.DrawLine(bounds.Left, h, bounds.Right, h, paint);
                    h += 10;
                }
            }
            canvas.Restore();
            bounds.Offset(400, 0);
            canvas.DrawBitmap(bitmap, bounds);

        }
        private void button1_Click(object sender, EventArgs e)
        {
            points = null;

        }
        // The end point style.
        public class ArrowStyle
        {
            public static ArrowStyle Default { get; } = new ArrowStyle { Width = 8, Length = 8 * 6 };
            public bool NotFill { get; set; }
            public double Width { get; set; }
            public double Length { get; set; }
        }
        private void DrawArrowLine(SKCanvas gr, SKColor color, LightCAD.Core.Vector2 start, LightCAD.Core.Vector2 end, ArrowStyle startStyle, ArrowStyle endStyle)
        {

            // Draw the shaft.

            using (var pen = new SKPaint { Color = color, IsStroke = true })
            {
                gr.DrawLine(start.ToSKPoint(), end.ToSKPoint(), pen);
            }

            // Find the arrow shaft unit vector.
            var vec = (end - start).Normalized;

            // Draw the start.
            if (startStyle != null)
            {
                DrawArrowHead(gr, color, start, -vec, startStyle);
            }
            if (endStyle != null)
            {
                DrawArrowHead(gr, color, end, vec, endStyle);
            }
        }

        // Draw an arrowhead at the given point
        // in the normalized direction <nx, ny>.
        private void DrawArrowHead(SKCanvas gr, SKColor color, LightCAD.Core.Vector2 point, LightCAD.Core.Vector2 dir, ArrowStyle style)
        {
            var nep = point + (-dir * style.Length);//法量反方向延申
            var vdir = LightCAD.Core.Vector2.RotateInRadian(dir, Math.PI / 2);
            var pv0 = (nep + (vdir * style.Width / 2)).ToSKPoint();
            var pv1 = (nep + (-vdir * style.Width / 2)).ToSKPoint();
            var skp = point.ToSKPoint();

            if (style.NotFill)
            {
                using (var pen = new SKPaint { Color = color, IsStroke = true })
                {
                    SKPoint[] points = { skp, pv0, skp, pv1, pv0, pv1 };
                    gr.DrawPoints(SKPointMode.Lines, points, pen);
                }
            }
            else
            {
                using (var fill = new SKPaint { Color = color, Style = SKPaintStyle.Fill })
                {
                    var points = new SKPoint[3] { skp, pv0, pv1 };
                    var colors = new SKColor[3] { color, color, color };
                    gr.DrawVertices(SKVertexMode.Triangles, points, colors, fill);
                }
            }

        }


        public bool IsPointInPolygon(LightCAD.Core.Vector2 point, LightCAD.Core.Vector2[] polygon)
        {
            int polygonLength = polygon.Length, i = 0;
            bool inside = false;
            // x, y for tested point.
            double pointX = point.X, pointY = point.Y;
            // start / end point for the current polygon segment.
            double startX, startY, endX, endY;
            LightCAD.Core.Vector2 endPoint = polygon[polygonLength - 1];
            endX = endPoint.X;
            endY = endPoint.Y;
            while (i < polygonLength)
            {
                startX = endX; startY = endY;
                endPoint = polygon[i++];
                endX = endPoint.X; endY = endPoint.Y;
                //
                inside ^= (endY > pointY ^ startY > pointY) /* ? pointY inside [startY;endY] segment ? */
                          && /* if so, test if it is under the segment */
                          ((pointX - endX) < (pointY - endY) * (startX - endX) / (startY - endY));
            }
            return inside;
        }
    }
}






//struct point
//{
//    long long x, y;
//};

//long long cross_product(point a, point b){
//    return a.x * b.y - b.x * a.y;
//}

//long long sq_dist(point a, point b){
//    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
//}

//bool is_inside(long long x, long long y, vector<point>&points)
//{
//    point p1 = { points[points.size() - 1].x - points[0].x, points[points.size() - 1].y - points[0].y };
//    point p2 = { points[1].x - points[0].x, points[1].y - points[0].y };
//    point pq = { x - points[0].x, y - points[0].y };

//    if (!(cross_product(p1, pq) <= 0 && cross_product(p2, pq) >= 0)) return false;

//    int l = 0, r = points.size();
//    while (r - l > 1)
//    {
//        int mid = (l + r) / 2;
//        point cur = { points[mid].x - points[0].x, points[mid].y - points[0].y };
//        if (cross_product(cur, pq) < 0)
//        {
//            r = mid;
//        }
//        else
//        {
//            l = mid;
//        }
//    }

//    if (l == points.size() - 1)
//    {
//        return sq_dist(points[0], { x,y}) <= sq_dist(points[0], points[l]);
//    }else
//{
//    point l_l1 = { points[l + 1].x - points[l].x, points[l + 1].y - points[l].y };
//    point lq = { x - points[l].x, y - points[l].y };
//    return (cross_product(l_l1, lq) >= 0);
//}
//}

//void pick_p0(vector<point>&points)
//{
//    long long min_x = 1e9 + 5;
//    long long max_y = -1e9 - 5;
//    int min_i = 0;
//    for (int i = 0; i < points.size(); i++)
//    {
//        if (points[i].x < min_x || (points[i].x == min_x && points[i].y > max_y))
//        {
//            min_x = points[i].x;
//            max_y = points[i].y;
//            min_i = i;
//        }
//    }

//    rotate(points.begin(), points.begin() + min_i, points.end());
//}
