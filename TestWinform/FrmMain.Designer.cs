﻿namespace TestWinform
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnAddItems = new System.Windows.Forms.Button();
            this.cmbBoxLanguage = new System.Windows.Forms.ComboBox();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.grpBoxVisible = new System.Windows.Forms.GroupBox();
            this.chkBoxButtonApplyVisible = new System.Windows.Forms.CheckBox();
            this.chkBoxButtonShowTextVisible = new System.Windows.Forms.CheckBox();
            this.chkBoxHelpVisible = new System.Windows.Forms.CheckBox();
            this.chkBoxToolbarVisible = new System.Windows.Forms.CheckBox();
            this.chkBoxTextVisible = new System.Windows.Forms.CheckBox();
            this.lblText = new System.Windows.Forms.Label();
            this.txtText = new System.Windows.Forms.TextBox();
            this.btnGetValue = new System.Windows.Forms.Button();
            this.PropertyGrid1 = new mbrPropertyGrid.PropertyGrid();
            this.btnAbout = new System.Windows.Forms.Button();
            this.trkProgressBar = new System.Windows.Forms.TrackBar();
            this.lblProgressBar = new System.Windows.Forms.Label();
            this.btnSetDefaults = new System.Windows.Forms.Button();
            this.grpBoxVisible.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkProgressBar)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddItems
            // 
            this.btnAddItems.Location = new System.Drawing.Point(12, 4);
            this.btnAddItems.Name = "btnAddItems";
            this.btnAddItems.Size = new System.Drawing.Size(79, 27);
            this.btnAddItems.TabIndex = 1;
            this.btnAddItems.Text = "Add Items";
            this.btnAddItems.UseVisualStyleBackColor = true;
            this.btnAddItems.Click += new System.EventHandler(this.btnAddItems_Click);
            // 
            // cmbBoxLanguage
            // 
            this.cmbBoxLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxLanguage.FormattingEnabled = true;
            this.cmbBoxLanguage.Items.AddRange(new object[] {
            "EN - English",
            "IT - Italian"});
            this.cmbBoxLanguage.Location = new System.Drawing.Point(97, 22);
            this.cmbBoxLanguage.Name = "cmbBoxLanguage";
            this.cmbBoxLanguage.Size = new System.Drawing.Size(91, 21);
            this.cmbBoxLanguage.TabIndex = 3;
            this.cmbBoxLanguage.SelectedIndexChanged += new System.EventHandler(this.cmbBoxLanguage_SelectedIndexChanged);
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Location = new System.Drawing.Point(97, 4);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(55, 13);
            this.lblLanguage.TabIndex = 4;
            this.lblLanguage.Text = "Language";
            // 
            // grpBoxVisible
            // 
            this.grpBoxVisible.Controls.Add(this.chkBoxButtonApplyVisible);
            this.grpBoxVisible.Controls.Add(this.chkBoxButtonShowTextVisible);
            this.grpBoxVisible.Controls.Add(this.chkBoxHelpVisible);
            this.grpBoxVisible.Controls.Add(this.chkBoxToolbarVisible);
            this.grpBoxVisible.Controls.Add(this.chkBoxTextVisible);
            this.grpBoxVisible.Location = new System.Drawing.Point(281, 2);
            this.grpBoxVisible.Name = "grpBoxVisible";
            this.grpBoxVisible.Size = new System.Drawing.Size(198, 89);
            this.grpBoxVisible.TabIndex = 5;
            this.grpBoxVisible.TabStop = false;
            this.grpBoxVisible.Text = "Visible components";
            // 
            // chkBoxButtonApplyVisible
            // 
            this.chkBoxButtonApplyVisible.AutoSize = true;
            this.chkBoxButtonApplyVisible.Location = new System.Drawing.Point(79, 44);
            this.chkBoxButtonApplyVisible.Name = "chkBoxButtonApplyVisible";
            this.chkBoxButtonApplyVisible.Size = new System.Drawing.Size(83, 17);
            this.chkBoxButtonApplyVisible.TabIndex = 4;
            this.chkBoxButtonApplyVisible.Text = "ButtonApply";
            this.chkBoxButtonApplyVisible.UseVisualStyleBackColor = true;
            this.chkBoxButtonApplyVisible.CheckedChanged += new System.EventHandler(this.chkBoxButtonApplyVisible_CheckedChanged);
            // 
            // chkBoxButtonShowTextVisible
            // 
            this.chkBoxButtonShowTextVisible.AutoSize = true;
            this.chkBoxButtonShowTextVisible.Location = new System.Drawing.Point(79, 20);
            this.chkBoxButtonShowTextVisible.Name = "chkBoxButtonShowTextVisible";
            this.chkBoxButtonShowTextVisible.Size = new System.Drawing.Size(105, 17);
            this.chkBoxButtonShowTextVisible.TabIndex = 3;
            this.chkBoxButtonShowTextVisible.Text = "ButtonShowText";
            this.chkBoxButtonShowTextVisible.UseVisualStyleBackColor = true;
            this.chkBoxButtonShowTextVisible.CheckedChanged += new System.EventHandler(this.chkBoxButtonShowTextVisible_CheckedChanged);
            // 
            // chkBoxHelpVisible
            // 
            this.chkBoxHelpVisible.AutoSize = true;
            this.chkBoxHelpVisible.Checked = true;
            this.chkBoxHelpVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBoxHelpVisible.Location = new System.Drawing.Point(8, 66);
            this.chkBoxHelpVisible.Name = "chkBoxHelpVisible";
            this.chkBoxHelpVisible.Size = new System.Drawing.Size(48, 17);
            this.chkBoxHelpVisible.TabIndex = 2;
            this.chkBoxHelpVisible.Text = "Help";
            this.chkBoxHelpVisible.UseVisualStyleBackColor = true;
            this.chkBoxHelpVisible.CheckedChanged += new System.EventHandler(this.chkBoxHelpVisible_CheckedChanged);
            // 
            // chkBoxToolbarVisible
            // 
            this.chkBoxToolbarVisible.AutoSize = true;
            this.chkBoxToolbarVisible.Checked = true;
            this.chkBoxToolbarVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBoxToolbarVisible.Location = new System.Drawing.Point(8, 43);
            this.chkBoxToolbarVisible.Name = "chkBoxToolbarVisible";
            this.chkBoxToolbarVisible.Size = new System.Drawing.Size(62, 17);
            this.chkBoxToolbarVisible.TabIndex = 1;
            this.chkBoxToolbarVisible.Text = "Toolbar";
            this.chkBoxToolbarVisible.UseVisualStyleBackColor = true;
            this.chkBoxToolbarVisible.CheckedChanged += new System.EventHandler(this.chkBoxToolbarVisible_CheckedChanged);
            // 
            // chkBoxTextVisible
            // 
            this.chkBoxTextVisible.AutoSize = true;
            this.chkBoxTextVisible.Location = new System.Drawing.Point(8, 20);
            this.chkBoxTextVisible.Name = "chkBoxTextVisible";
            this.chkBoxTextVisible.Size = new System.Drawing.Size(47, 17);
            this.chkBoxTextVisible.TabIndex = 0;
            this.chkBoxTextVisible.Text = "Text";
            this.chkBoxTextVisible.UseVisualStyleBackColor = true;
            this.chkBoxTextVisible.CheckedChanged += new System.EventHandler(this.chkBoxTextVisible_CheckedChanged);
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Location = new System.Drawing.Point(97, 48);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(47, 13);
            this.lblText.TabIndex = 6;
            this.lblText.Text = "Text title";
            // 
            // txtText
            // 
            this.txtText.Location = new System.Drawing.Point(97, 64);
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(90, 20);
            this.txtText.TabIndex = 7;
            this.txtText.Text = "Title";
            this.txtText.TextChanged += new System.EventHandler(this.txtText_TextChanged);
            // 
            // btnGetValue
            // 
            this.btnGetValue.Location = new System.Drawing.Point(12, 34);
            this.btnGetValue.Name = "btnGetValue";
            this.btnGetValue.Size = new System.Drawing.Size(79, 27);
            this.btnGetValue.TabIndex = 8;
            this.btnGetValue.Text = "Get Value";
            this.btnGetValue.UseVisualStyleBackColor = true;
            this.btnGetValue.Click += new System.EventHandler(this.btnGetValue_Click);
            // 
            // PropertyGrid1
            // 
            this.PropertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PropertyGrid1.ButtonApplyVisible = false;
            this.PropertyGrid1.ButtonShowTextVisible = false;
            this.PropertyGrid1.ColumnWidth = 100;
            this.PropertyGrid1.HelpVisible = true;
            this.PropertyGrid1.Location = new System.Drawing.Point(12, 99);
            this.PropertyGrid1.Name = "PropertyGrid1";
            this.PropertyGrid1.RowsHeight = 19;
            this.PropertyGrid1.SelectedItem = null;
            this.PropertyGrid1.ShowItemsAsCategory = true;
            this.PropertyGrid1.Size = new System.Drawing.Size(467, 351);
            this.PropertyGrid1.TabIndex = 0;
            this.PropertyGrid1.Text = "PropertyGrid";
            this.PropertyGrid1.TextBackgroundColor = System.Drawing.Color.LightBlue;
            this.PropertyGrid1.TextBackgroundColorIsGradient = true;
            this.PropertyGrid1.TextBackgroundGradientColorFinish = System.Drawing.Color.Blue;
            this.PropertyGrid1.TextBackgroundGradientColorStart = System.Drawing.Color.LightBlue;
            this.PropertyGrid1.TextVisible = false;
            this.PropertyGrid1.ToolbarVisible = true;
            this.PropertyGrid1.TraceLogEnabled = false;
            this.PropertyGrid1.ApplyButtonPressed += new mbrPropertyGrid.PropertyGrid.ApplyButtonPressedHandle(this.PropertyGrid1_ApplyButtonPressed);
            // 
            // btnAbout
            // 
            this.btnAbout.Location = new System.Drawing.Point(12, 64);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(79, 27);
            this.btnAbout.TabIndex = 9;
            this.btnAbout.Text = "About...";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // trkProgressBar
            // 
            this.trkProgressBar.Location = new System.Drawing.Point(196, 24);
            this.trkProgressBar.Maximum = 100;
            this.trkProgressBar.Name = "trkProgressBar";
            this.trkProgressBar.Size = new System.Drawing.Size(79, 45);
            this.trkProgressBar.TabIndex = 10;
            this.trkProgressBar.TickFrequency = 20;
            this.trkProgressBar.Value = 90;
            this.trkProgressBar.Scroll += new System.EventHandler(this.trkProgressBar_Scroll);
            // 
            // lblProgressBar
            // 
            this.lblProgressBar.AutoSize = true;
            this.lblProgressBar.Location = new System.Drawing.Point(196, 4);
            this.lblProgressBar.Name = "lblProgressBar";
            this.lblProgressBar.Size = new System.Drawing.Size(66, 13);
            this.lblProgressBar.TabIndex = 11;
            this.lblProgressBar.Text = "Progress bar";
            // 
            // btnSetDefaults
            // 
            this.btnSetDefaults.Location = new System.Drawing.Point(196, 64);
            this.btnSetDefaults.Name = "btnSetDefaults";
            this.btnSetDefaults.Size = new System.Drawing.Size(79, 27);
            this.btnSetDefaults.TabIndex = 12;
            this.btnSetDefaults.Text = "Defaults";
            this.btnSetDefaults.UseVisualStyleBackColor = true;
            this.btnSetDefaults.Click += new System.EventHandler(this.btnSetDefaults_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 462);
            this.Controls.Add(this.btnSetDefaults);
            this.Controls.Add(this.lblProgressBar);
            this.Controls.Add(this.trkProgressBar);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.btnGetValue);
            this.Controls.Add(this.txtText);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.grpBoxVisible);
            this.Controls.Add(this.lblLanguage);
            this.Controls.Add(this.cmbBoxLanguage);
            this.Controls.Add(this.btnAddItems);
            this.Controls.Add(this.PropertyGrid1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "Test mbrPropertyGrid Control";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.grpBoxVisible.ResumeLayout(false);
            this.grpBoxVisible.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkProgressBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private mbrPropertyGrid.PropertyGrid PropertyGrid1;
        private System.Windows.Forms.Button btnAddItems;
        private System.Windows.Forms.ComboBox cmbBoxLanguage;
        private System.Windows.Forms.Label lblLanguage;
        private System.Windows.Forms.GroupBox grpBoxVisible;
        private System.Windows.Forms.CheckBox chkBoxToolbarVisible;
        private System.Windows.Forms.CheckBox chkBoxTextVisible;
        private System.Windows.Forms.CheckBox chkBoxHelpVisible;
        private System.Windows.Forms.CheckBox chkBoxButtonShowTextVisible;
        private System.Windows.Forms.CheckBox chkBoxButtonApplyVisible;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.Button btnGetValue;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.TrackBar trkProgressBar;
        private System.Windows.Forms.Label lblProgressBar;
        private System.Windows.Forms.Button btnSetDefaults;


    }
}

