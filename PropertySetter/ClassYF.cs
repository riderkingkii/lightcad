﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using TestWinform;

namespace PropertySetter
{
    public abstract class Element
    {
       public ElementAction Action;
    } 
    public interface ICurve2d 
    {
    }
    public interface ICurveElement 
    {

    }
    public class Style { }
    public interface ICurvesElement 
    {
        ICurve2d[] Curves { get; set; }
        Style[] Styles { get; set; }
    }

    public sealed class Line : CurveElement
    {

        private line2 line = new line2();
        public Line()
        {
            this.Curves = new ICurve2d[1] { line };
        }

    }

    public class CurveElement : Element, ICurvesElement
    {
        public ICurve2d[] Curves { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Style[] Styles { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }

    /// <summary>
    /// 封装元素关于二维表达的创建和交互
    /// </summary>
    public class ElementAction
    {
        public virtual Element Move(Vector2 v) => null;
        public virtual Element Rotation(Vector2 v, double angle) => null;
        public virtual Element Scale(Vector2 v) => null;
    }
    /// <summary>
    /// 封装元素关于三维表现的创建和交互
    /// </summary>
    public class Element3dAction
    {
        ISolid3d[] CreateSolid(ISolidElement solidEle) => null;
        ISolidElement Translate(ISolidElement solidEle) => null;
        ISolidElement Mirror(ISolidElement solidEle) => null;
        ISolidElement Rotate(ISolidElement solidEle) => null;
    }
   
    public interface ISolidElement 
    {
        Element3dAction Action3d { get; set; }
        ISolid3d[] Solids { get; set; }
        
    }
    public class line2:ICurve2d 
    { }


    public  class Text 
    {
    }
    public  class MText 
    {
    }
    public class Dim 
    {
    }

    public class DirectComponent : Element,ICurvesElement, ISolidElement
    {
        public ICurve2d[] Curves { get ; set ; }
        public ISolid3d[] Solids { get ; set ; }
        public Style[] Styles { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Element3dAction Action3d { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
    public class DirectComponentAction : ElementAction 
    {
    }
    public class DirectComponent3dAction : Element3dAction 
    {

    }

    public class ComponentRef : Element, ICurvesElement, ISolidElement
    {
        Matrix4x4 Transform;
        public ICurve2d[] Curves { get ; set ; }
        public ISolid3d[] Solids { get ; set ; }
        public Style[] Styles { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Element3dAction Action3d { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }

    public class Wall : DirectComponent
    {
        private ICurve2d baseCurve;
        public Wall()
        {
            this.Action = new WallAction();
            this.Action3d = new Wall3dAction();
            
        }
    }
    public class WallAction : DirectComponentAction 
    {
        
    }
    public class Wall3dAction : DirectComponent3dAction 
    {

    }
    public class Window : ComponentRef 
    {

    }
    public class Box2 
    { 
    }
    public static class CurveUtils 
    {
        public static ICurve2d Move(this ICurve2d curve, Vector2 v) => null;
        public static ICurve2d[] Move(this ICurve2d[] curves, Vector2 v) => null;
        public static Box2 GetBox(ICurve2d[] curve) => null;
        public static ICurve2d[] Translate(this ICurve2d[] curves, Vector2 v) => null;
        public static ICurve2d[] Rotation(this ICurve2d[] curves, Vector2 v, double angle) => null;
        public static ICurve2d[] Scale(this ICurve2d[] curves, Vector2 v) => null;
    }

}
