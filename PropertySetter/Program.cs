using System.Text.RegularExpressions;

namespace PropertySetter
{
    internal static class Program
    {
        public const string SetterFlagString = "//PropertySetter:";
        public const string Tab2 = "        ";
        public const string Tab3 = "            ";
        public const string Tab4 = "                ";
        public const string Tab5 = "                    ";

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //if (args.Length == 0) return;
            //var prjDir = args[0];
            //var prjDir = @"D:\Study\2d-study\LightCAD\Gitee\LightCAD.Core";
            //var files = ScanCsfileInFolder(prjDir);
            //foreach (var item in files)
            //{
            //    ScanPropertySetter(item.Key, item.Value);
            //}
         //   Application.EnableVisualStyles();
         //   Application.SetCompatibleTextRenderingDefault(false);
          //  Application.Run(new MainForm());
        }

        private static void ScanPropertySetter(FileInfo file, string sourceCode)
        {
            var srcChanged = false;
            var matches = Regex.Matches(sourceCode, SetterFlagString);
            for (var i = matches.Count - 1; i >= 0; i--)
            {
                var item = matches[i];
                var frontBracePos = sourceCode.LastIndexOf('{', item.Index);//�ҵ�ǰ�ô�����
                var setBody = Util.FetchBracket(sourceCode, frontBracePos, out string oldSetter);
                var changed = GenerateSetter(oldSetter, out string newSetter);
                if (changed)
                {
                    sourceCode = setBody.Replace("#SETTER_BODY#", newSetter);
                }
                srcChanged |= changed;
            }
            if (srcChanged)
            {
                File.WriteAllText(file.FullName, sourceCode);
            }
        }

        private static bool GenerateSetter(string setterSrc, out string newSetterSrc)
        {
            var reader = new StringReader(setterSrc);
            string line = null;
            string[] propNames = null;
            string setterLine = null;
            List<string> genLines = new List<string>();
            while ((line = reader.ReadLine()) != null)
            {
                if (line.Trim() == string.Empty) continue;

                if (line.IndexOf(SetterFlagString) >= 0)
                {
                    propNames = line.Replace(SetterFlagString, "").Trim().Split(',');
                    setterLine = line;
                    continue;
                }

                if (propNames != null)
                {
                    genLines.Add(line);
                }
            }
            genLines.RemoveAt(genLines.Count - 1);
            var newGenLines = GenSetterBody(propNames);
            var genCode = string.Join("\n", genLines);

            var newCode = string.Join("\n", newGenLines);
            if (genCode == newCode)
            {
                newSetterSrc = null;
                return false;
            }
            else
            {
                newSetterSrc = "{\n" + setterLine + "\n" + newCode + "\n" + Tab2 + "}\n";
                return true;
            }
        }

        public static List<string> GenSetterBody(string[]? propNames)
        {
            var lines = new List<string>();
            lines.Add(Tab3 + "if (!fireChangedEvent) {");
            for (var i = 0; i < propNames.Length; i++)
            {
                var propName = propNames[i];
                var lpropName = Util.LowerFirstChar(propName);
                lines.Add(Tab4 + $"if ({lpropName} != null) this.{propName} = {lpropName}.Value;");
            }
            lines.Add(Tab3 + "}");

            lines.Add(Tab3 + "else");
            lines.Add(Tab3 + "{");
            for (var i = 0; i < propNames.Length; i++)
            {
                var propName = propNames[i];
                var lpropName = Util.LowerFirstChar(propName);
                lines.Add(Tab4 + $"bool chg_{lpropName} = ({lpropName} != null && {lpropName} != this.{propName});");
                lines.Add(Tab4 + $"if (chg_{lpropName})");
                lines.Add(Tab4 + "{");
                lines.Add(Tab5 + $"PropertyChangedBefore(nameof({propName}), this.{propName}, {lpropName});");
                lines.Add(Tab5 + $"var oldValue = this.{propName};");
                lines.Add(Tab5 + $"this.{propName} = {lpropName}.Value;");
                lines.Add(Tab5 + $"PropertyChangedAfter(nameof({propName}), oldValue, this.{propName});");
                lines.Add(Tab4 + "}");
            }
            lines.Add(Tab3 + "}");
            return lines;
        }


        private static Dictionary<FileInfo, string> ScanCsfileInFolder(string folder)
        {
            var dict = new Dictionary<FileInfo, string>();
            var dirInfo = new DirectoryInfo(folder);
            var files = dirInfo.GetFiles("*.cs", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var src = File.ReadAllText(file.FullName);
                if (src.IndexOf("//PropertySetter:") > 0)
                {
                    dict.Add(file, src);
                }
            }
            return dict;
        }
    }
}