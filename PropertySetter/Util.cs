﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PropertySetter
{

    public class Util
    {
        public static string LowerFirstChar(string str)
        {
            return str.Substring(0,1).ToLower()+str.Substring(1);
        }
        public static string FetchBracket(string src,int startIndex,out string braceStr)
        {
            var cs = src;
            int startPos = -1, endPos = -1;
            braceStr = FindBracketsMatch(startIndex, cs, '{', '}', out startPos, out endPos);
            if (startPos >= 0 && endPos >= 0)
            {
                var jstmp = "";
                jstmp = cs.Substring(0, startIndex)+"#SETTER_BODY#";
                if (endPos < cs.Length - 1)
                {
                    jstmp += cs.Substring(endPos + 1);
                }
                return jstmp.Trim();
            }
            else
            {
                return null;
            }
        }

        private static string FindBracketsMatch(int begin, string str, char start, char end, out int startPos, out int endPos)
        {
            Stack<char> stack = new Stack<char>();
            startPos = -1;
            endPos = -1;
            for (int i = begin; i < str.Length; i++)
            {
                char current = str[i];

                if (current == start)
                {
                    if (startPos < 0)
                    {
                        startPos = i;
                    }
                    stack.Push(current);
                }
                if (current == end)
                {
                    if (stack.Count <= 0)
                    {
                        break;
                    }
                    else
                    {
                        char top = stack.Peek();
                        if (IsCouple(top, current))
                        {
                            stack.Pop();
                            if (stack.Count == 0)
                            {
                                endPos = i;
                                break;
                            }
                        }
                        else
                        {
                            stack.Push(current);
                        }
                    }
                }


            }
            if (startPos >= 0 && endPos >= 0)
                return str.Substring(startPos, endPos - startPos + 1);
            else
                return "";
        }

       
        static bool IsCouple(char left, char right)
        {
            if (left == '(' && right == ')')
            {
                return true;
            }
            if (left == '[' && right == ']')
            {
                return true;
            }
            if (left == '{' && right == '}')
            {
                return true;
            }
            return false;
        }
    }
}
