﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWinform;

namespace PropertySetter
{
    public class Application
    {
        public static string ExecutePath { get; set; }
        //各种应用级别的配置，配置文件等
        public static ApplicationRuntime Runtime { get; set; }
    }
    public class ApplicationRuntime
    {
        List<DocumentRuntime> DocumentRts { get; set; }
    }
    public class DocumentRuntime
    {
        /// <summary>
        /// 编辑器的运行时
        /// </summary>
        public List<IDocumentEditor> EditorRts;

        LcDocument Document { get; set; }

    }
    public interface IDocumentEditor
    {

    }
    public class DrawingEditorRuntime:IDocumentEditor
    {
        //绘图编辑器的各类设置
        
    }
    public class DrawingRuntime
    {

    }
    public class PaperRuntime: DrawingRuntime
    {

    }
    public class View3dRuntime : DrawingRuntime
    {

    }
    public class Model3dRuntime
    {

    }
    public class Model3dEditorRuntime: IDocumentEditor
    {

    }

    public class PaperEditorRuntime : IDocumentEditor
    {

    }
    public class View3dEditorRuntime : IDocumentEditor
    {

    }
    
}
