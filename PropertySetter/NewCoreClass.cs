﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TestWinform;

namespace PropertySetter
{

    public class xLcElement
    {
        public string Name;
        public bool IsNameAware;
        public virtual void HitTest() { }
        public virtual void GetBoundingBox() { }

          
    }

    public class xLcComposite : xLcElement
    {

    }

    public class xLcGroup : xLcComposite
    {

    }
    public class xLcDrawingFrame : xLcComposite
    {

    }


    public class xLcTable : xLcElement
    {

    }
    public interface ILcText
    {
    }
    public class xLcText : xLcElement, ILcText
    {
    }

    public class xBlock
    {

    }
    public class xBlockManager
    {

    }
    public class xBlockRef : xLcElement
    {

    }
    public class xLcMText : xLcElement, ILcText
    {

    }
    public class xLcXLine : xLcElement
    {

    }
    public class xLcRay : xLcElement
    {

    }
    public class xCurve2d
    {

    }
    public class xLine2d : xCurve2d
    {

    }
    

    public class xLcCurve2d : xLcElement
    {
        public xCurve2d Curve { get; }
    }
    public class xLcLine : xLcCurve2d
    {

    }
    public class xLcCircle : xLcCurve2d
    {

    }
    public class xLcArc : xLcCurve2d
    {

    }
    public class xLcEllapse : xLcCurve2d
    {

    }
    public class xLcPolyline : xLcCurve2d
    {

    }
    public class xLcPolygon : xLcCurve2d
    {

    }
    public class xLcSpline : xLcCurve2d
    {

    }

  
    public class xSolid3d
    {

    }
    public class xCubic3d : xSolid3d
    {

    }

    public class xSweep3d : xSolid3d
    {
    }


    public class xLcSolid3d : LcElement
    {
        public xSolid3d Solid { get; }
        public virtual void HitTest() { }
        public virtual void GetBoundingBox() { }
    }

    public class xLcCubic3d : xLcSolid3d
    {

    }
    public class xLcSweep3d : xLcSolid3d
    {

    }
    public interface ILcComponent
    {
        string Name { get; set; }
        string Namespace { get; set; }
        string Major { get; set; }
        string Category { get; set; }
        string SubCategory { get; set; }
        string UseType { get; set; }
        List<xSolid3d> Solids { get; }
        List<xCurve2d> Curves { get; }
    }
    public class xLcComponent
    {
        public object Curve2dProvider;
        public object Solid3dProvider;
    }
    public class ComponentManager
    {

    }
    public class ComponentLibraryManager
    {

    }
    public class xLcComponentInstance : LcElement, ILcComponent
    {
        public xLcComponent Component { get; set; }
        public List<xCurve2d> Curves => throw new NotImplementedException();

        public List<xSolid3d> Solids => throw new NotImplementedException();

        public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Namespace { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Major { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Category { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string SubCategory { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string UseType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        /// <summary>
        /// 获取其他用途的曲线，如立面图
        /// </summary>
        /// <param name="useType"></param>
        /// <returns></returns>
        public List<xCurve2d> GetCurves(string useType)
        {
            return null;
        }
    }
    public class xLcDirectComponent : LcElement, ILcComponent
    {
        public xCurve2d BaseCurve { get; set; }
        public List<xCurve2d> Curves => throw new NotImplementedException();

        public List<xSolid3d> Solids => throw new NotImplementedException();

        public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Namespace { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Major { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Category { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string SubCategory { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string UseType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }



}
