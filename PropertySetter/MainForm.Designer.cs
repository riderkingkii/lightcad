﻿namespace PropertySetter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtPropNames = new TextBox();
            btnGen = new Button();
            txtOutput = new RichTextBox();
            SuspendLayout();
            // 
            // txtPropNames
            // 
            txtPropNames.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtPropNames.Location = new Point(12, 20);
            txtPropNames.Name = "txtPropNames";
            txtPropNames.Size = new Size(933, 27);
            txtPropNames.TabIndex = 0;
            // 
            // btnGen
            // 
            btnGen.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnGen.Location = new Point(961, 18);
            btnGen.Name = "btnGen";
            btnGen.Size = new Size(132, 29);
            btnGen.TabIndex = 1;
            btnGen.Text = "生成";
            btnGen.UseVisualStyleBackColor = true;
            btnGen.Click += btnGen_Click;
            // 
            // txtOutput
            // 
            txtOutput.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            txtOutput.Location = new Point(12, 71);
            txtOutput.Name = "txtOutput";
            txtOutput.Size = new Size(1081, 570);
            txtOutput.TabIndex = 2;
            txtOutput.Text = "";
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(9F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1105, 653);
            Controls.Add(txtOutput);
            Controls.Add(btnGen);
            Controls.Add(txtPropNames);
            Name = "MainForm";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "MainForm";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox txtPropNames;
        private Button btnGen;
        private RichTextBox txtOutput;
    }
}