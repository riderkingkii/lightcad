﻿using LightCAD.Core;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LightCAD.Runtime;
using static LightCAD.Runtime.Constants;
using System.Xml.Linq;
using System.Runtime.CompilerServices;
using LightCAD.Core.Elements;
using LightCAD.Runtime.Interface;

namespace LightCAD.Drawing.Actions
{
    public class BuildingAction
    {
        protected DocumentRuntime docRt;
        public LcDocument lcDocument;
        protected ViewportRuntime vportRt;
        protected IDocumentEditor docEditor;
        public BuildingAction(IDocumentEditor docEditor)
        {
            this.docEditor = docEditor;
            this.docRt = docEditor.DocRt ;
            this.vportRt =(docEditor as DrawingEditRuntime).ActiveViewportRt;
            this.lcDocument = docRt.Document;
        }
        public LcBuilding lcBuilding;

        public async void ExecCreate(string[] args)
        {
            var pointInputer = new PointInputer(this.docEditor);
            var elementSetInputer = new ElementSetInputer(this.docEditor);
            if (this.lcDocument.Buildings.Count == 0)
            {
                //方便调试增加一个默认单体和楼层
                LcBuilding lcBuilding = lcDocument.CreateObject<LcBuilding>();
                lcBuilding.Name = "华润7#";
                //默认地上
                for (int i = 1; i <= 18; i++)
                {
                    LcLevel lcLevel = this.lcDocument.CreateObject<LcLevel>();
                    lcLevel.Index = i;
                    lcLevel.Name = i + "F";
                    lcLevel.Height = 2900;
                    lcLevel.Type = LcLevelType.Floor;
                    lcBuilding.AddGroups(lcLevel);
                }
                //屋顶层
                for (int i = 1; i <= 1; i++)
                {
                    LcLevel lcLevel = this.lcDocument.CreateObject<LcLevel>();
                    lcLevel.Index = i;
                    lcLevel.Name = "R"+i + "F";
                    lcLevel.Height = 4300;
                    lcLevel.Type = LcLevelType.RFloor;
                    lcBuilding.AddGroups(lcLevel);
                }
                this.lcDocument.Buildings.Add(lcBuilding);
            }
            var win = (IBuildingWindow)AppRuntime.UISystem.CreateWindow("BuildingSettingWindow", this);
            var result = AppRuntime.UISystem.ShowDialog(win);
            if (result == LcDialogResult.OK)
            {
                lcBuilding = win.lcBuilding;
            }
        }
    }
}
