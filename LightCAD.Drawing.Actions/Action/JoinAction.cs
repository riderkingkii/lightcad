﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Core.Filters;
using LightCAD.Runtime;
using netDxf.Entities;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.ConstrainedExecution;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using static LightCAD.Core.Elements.LcPolyLine;
using LightCAD.MathLib;
using System.Reflection.Metadata;
using Avalonia.Controls;
using static LightCAD.Three.WebGLMorphtargets;
using System.Diagnostics.Metrics;
using Avalonia.Controls.Shapes;
using Avalonia.Utilities;
using System.Net;
using SixLabors.ImageSharp.ColorSpaces;

namespace LightCAD.Drawing.Actions
{
    public class JoinAction : Curve2dAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;
        public LcElement SourceEle;
        public List<LcElement> MergeEles = new List<LcElement>();
        static JoinAction()
        {
            CommandName = "JOIN";
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "JOIN",
                Description = "JOIN",
                Steps = new LcCreateStep[]
                {

                    new LcCreateStep { Name=  "Step0", Options= "选择源对象或要一次合并的多个对象:" },
                    new LcCreateStep { Name=  "Step1", Options= "选择要合并的对象:" },
                }
            };
        }
        internal static void Initilize()
        {
            ElementActions.Join = new JoinAction();
            LcDocument.ElementActions.Add(BuiltinElementType.Join, ElementActions.Join);
        }
        private JoinAction() { }
        public JoinAction(IDocumentEditor docEditor) : base(docEditor)
        {
            
            this.commandCtrl.WriteInfo("命令：JOIN");
        }
        private LcCreateMethod GetMethod(string method)
        {
            if (method == null) return CreateMethods[0];
            var getted = CreateMethods.FirstOrDefault((m) => m.Name == method);
            if (getted == null)
                return CreateMethods[0];
            else
                return getted;
        }

        public async void ExecCreate(string[] args = null)
        {
            this.StartCreating();
            var curMethod = SetCurMethod(CreateMethods, 0);
            var ElementInputer = new ElementInputer(this.docEditor);
        Step0:
            var step0 = SetCurStep(curMethod, 0);
            var result0 = await ElementInputer.Execute(step0.Options);
            if (ElementInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result0.ValueX == null)
            {
                this.Cancel();
                goto End;
            }
            else
            {
                LcElement element = (LcElement)result0.ValueX;
                //可以合并的多个对象：选择直线、多段线、三维多段线、圆弧、椭圆弧、螺旋或样条曲线。
                if (element is LcLine || element is LcPolyLine || element is LcArc)
                {
                    SourceEle = element;
                    MergeEles.Clear();
                    MergeEles.Add(element);
                    goto Step1;
                }
                else {
                    goto Step0;
                }
            }
        Step1:
            var newElementInputer = new ElementInputer(this.docEditor);
            var step1 = SetCurStep(curMethod, 1);
            var result1 = await newElementInputer.Execute(step1.Options);
            if (newElementInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result1 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result1.ValueX == null)
            {
                goto End;
            }
            else
            {
                LcElement element = (LcElement)result1.ValueX;
                if (element is LcLine || element is LcPolyLine || element is LcArc)
                {
                    MergeEles.Add(element);
                }
                goto Step1;
            }
        //注释掉 by:Zcb 2023-07-21 21:42
        End:
            CreateElement();
            this.EndCreating();
        }

        public override void Cancel()
        {
            this.SourceEle = null;
            this.MergeEles = new List<LcElement>();
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
            //TODO:
            this.EndCreating();
        }

        private void CreateElement() {
            if (this.MergeEles.Count < 2) return;
            //var doc = this.docRt.Document;
            //DocumentManager.CurrentRecorder.BeginAction("Join");
            #region
            MergeNearestEle(this.MergeEles);
            #endregion
            //this.docRt.Action.ClearSelects();
            //DocumentManager.CurrentRecorder.EndAction();
        }

        public void MergeNearestEle(List<LcElement> lcelements) {
            List<LcLine> lines = new List<LcLine>();
            List<LcPolyLine> polylines = new List<LcPolyLine>();
            List<LcArc> arcs = new List<LcArc>();
            foreach (var item in lcelements)
            {
                if (item is LcLine)
                {
                    LcLine line = (LcLine)item;
                    lines.Add(line);
                }
                else if (item is LcPolyLine)
                {
                    LcPolyLine polyline = (LcPolyLine)item;
                    polylines.Add(polyline);
                }
                else if (item is LcArc)
                {
                    LcArc arc = (LcArc)item;
                    arcs.Add(arc);
                }
            }
            //最短距离
            double? mindis = null;
            //最短的2个元素进行合并
            List<LcElement> mindisEntitys = new List<LcElement>();
            foreach (var firstitem in lcelements)
            {
                foreach (var seconditem in lcelements)
                {
                    if (firstitem == seconditem) continue;
                    var firstpoints = GetPointByEle(firstitem);
                    var secondpoints = GetPointByEle(seconditem);
                    List<LcLine> alllines = new List<LcLine>() {
                        new LcLine(firstpoints.First(), secondpoints.First()),
                        new LcLine(firstpoints.First(), secondpoints.Last()),
                        new LcLine(firstpoints.Last(), secondpoints.First()),
                        new LcLine(firstpoints.Last(), secondpoints.Last()),
                    };
                    var linelengths = alllines.Select(x => x.Length).ToList();
                    linelengths.Sort();
                    double linemindis = linelengths.First();
                    if (mindis == null || (mindis != null && linemindis < mindis))
                    {
                        mindis = linemindis;
                        mindisEntitys.Clear();
                        mindisEntitys.Add(firstitem);
                        mindisEntitys.Add(seconditem);
                        //如果连接直接进行合并
                        if (mindis == 0) {
                            var cloneallele = lcelements.Clone();
                            cloneallele.Remove(firstitem);
                            cloneallele.Remove(seconditem);
                            //全是直线走直线方法
                            if (lines.Count > 0 && polylines.Count == 0 && arcs.Count == 0)
                            {
                                LcElement newele = GetMergeLineEntity(firstitem, seconditem);
                                if (newele != null)
                                {
                                    cloneallele.Insert(0, newele);
                                    MergeNearestEle(cloneallele);
                                }
                            }
                            else
                            {
                                if (firstitem is LcPolyLine && seconditem is LcPolyLine)
                                {
                                    LcElement newele = GetMergePolyLineEntity(firstitem, seconditem);
                                    if (newele != null)
                                    {
                                        cloneallele.Insert(0, newele);
                                        MergeNearestEle(cloneallele);
                                    }
                                }
                                else if ((firstitem is LcLine && seconditem is LcPolyLine) || (firstitem is LcPolyLine && seconditem is LcLine))
                                {
                                    LcElement newele = GetMergeLinePolyLineEntity(firstitem, seconditem);
                                    if (newele != null)
                                    {
                                        cloneallele.Insert(0, newele);
                                        MergeNearestEle(cloneallele);
                                    }
                                }
                            }
                            return;
                        }
                    }
                }
            }
            if (mindis != null) {
                var newcloneallele = lcelements.Clone();
                newcloneallele.Remove(mindisEntitys.First());
                newcloneallele.Remove(mindisEntitys.Last());
                //全是直线走直线方法
                if (lines.Count > 0 && polylines.Count == 0 && arcs.Count == 0)
                {
                    LcElement newele = GetMergeLineEntity(mindisEntitys.First(), mindisEntitys.Last());
                    if (newele != null)
                    {
                        newcloneallele.Insert(0, newele);
                        MergeNearestEle(newcloneallele);
                    }
                }
            }
        }
        /// <summary>
        /// 直线合并
        /// </summary>
        /// <param name="firstele"></param>
        /// <param name="secondele"></param>
        /// <returns></returns>
        public LcElement GetMergeLineEntity(LcElement firstele, LcElement secondele) {
            LcElement retele = null;
            LcLine firstline = (LcLine)firstele;
            LcLine secondline = (LcLine)secondele;
            //线段首尾相连
            bool endtoend = false;
            //线段共线
            bool collinear = false;
            collinear = JudgeCollinear(firstline, secondline);
            if (firstline.Start == secondline.Start || firstline.Start == secondline.End || firstline.End == secondline.Start || firstline.End == secondline.End)
            {
                endtoend = true;
            }
            var doc = this.docRt.Document;
            if (collinear)
            {
                List<LcLine> alllines = new List<LcLine>() {
                        new LcLine(firstline.Start, secondline.Start),
                        new LcLine(firstline.Start, secondline.End),
                        new LcLine(firstline.End, secondline.Start),
                        new LcLine(firstline.End, secondline.End),
                };
                //根据距离从近到远排序
                alllines.Sort((x, y) => x.Length.CompareTo(y.Length));
                retele = alllines.Last();
                doc.ModelSpace.RemoveElement(firstele);
                doc.ModelSpace.RemoveElement(secondele);
                doc.ModelSpace.InsertElement(retele);
            }
            else {
                if (endtoend) {
                    List<Curve2d> Segments = new List<Curve2d>();
                    //PlineSegment pl = new PlineSegment();
                    Line2d line2D1 = new Line2d();
                    line2D1.Start = firstline.Start;
                    line2D1.End = firstline.End;
                    Segments.Add(line2D1);
                    Line2d line2D2 = new Line2d();
                    line2D2.Start = secondline.Start;
                    line2D2.End = secondline.End;
                    Segments.Add(line2D2);
                    LcPolyLine polyline = new LcPolyLine();
                    polyline.IsClosed = false;
                    polyline.Curve2ds = Segments;
                    retele = polyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
            }
            return retele;
        }

        /// <summary>
        /// 直线多段线合并
        /// </summary>
        /// <param name="firstele"></param>
        /// <param name="secondele"></param>
        /// <returns></returns>
        public LcElement GetMergeLinePolyLineEntity(LcElement firstele, LcElement secondele) {
            LcElement retele = null;
            List<Curve2d> Segments = new List<Curve2d>();
            if ((firstele is LcLine && secondele is LcPolyLine) || (firstele is LcPolyLine && secondele is LcLine)) {
                LcLine line = new LcLine();
                LcPolyLine polyLine = new LcPolyLine();
                if (firstele is LcLine)
                {
                    line = (LcLine)firstele;
                    polyLine = (LcPolyLine)secondele;
                }
                else {
                    line = (LcLine)secondele;
                    polyLine = (LcPolyLine)firstele;
                }
                var doc = this.docRt.Document;
                LcPolyLine newpolyline = (LcPolyLine)polyLine.Clone();
                Segments = newpolyline.Curve2ds;
                if (line.Start == LcPolyLine.GetStart(newpolyline.Curve2ds[0]))
                {
                    //PlineSegment pl = new PlineSegment();
                    Line2d line2D = new Line2d();
                    line2D.Start = line.End;
                    line2D.End = line.Start;
                    //pl.Type = PlineSegmentType.Line;
                    Segments.Insert(0, line2D);
                    newpolyline.Curve2ds = Segments;
                    if (line.End == LcPolyLine.GetEnd(newpolyline.Curve2ds.Last()))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
                else if (line.End == LcPolyLine.GetStart(newpolyline.Curve2ds[0]))
                {
                    //PlineSegment pl = new PlineSegment();
                    Line2d line2D = new Line2d();
                    line2D.Start = line.Start;
                    line2D.End = line.End;
                    //p1.Type = PlineSegmentType.Line;
                    Segments.Insert(0, line2D);
                    newpolyline.Curve2ds = Segments;
                    if (line.Start == LcPolyLine.GetEnd(polyLine.Curve2ds.Last()))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
                else if (line.Start == LcPolyLine.GetEnd(newpolyline.Curve2ds.Last()))
                {
                    //PlineSegment pl = new PlineSegment();
                    Line2d line2D = new Line2d();
                    line2D.Start = line.Start;
                    line2D.End = line.End;
                    //pl.Type = PlineSegmentType.Line;
                    Segments.Add(line2D);
                    newpolyline.Curve2ds = Segments;
                    if (line.End == LcPolyLine.GetStart(newpolyline.Curve2ds[0]))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
                else if (line.End == LcPolyLine.GetEnd(newpolyline.Curve2ds.Last()))
                {
                    //PlineSegment pl = new PlineSegment();
                    Line2d line2D = new Line2d();
                    line2D.Start = line.Start;
                    line2D.End = line.End;
                    //pl.Start = line.Start;
                    //pl.End = line.End;
                    //pl.Type = PlineSegmentType.Line;
                    //Segments.Add(pl);
                    newpolyline.Curve2ds = Segments;
                    if (line.Start == LcPolyLine.GetStart(newpolyline.Curve2ds[0]))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
            }
            return retele;
        }

        /// <summary>
        /// 多段线合并
        /// </summary>
        /// <param name="firstele"></param>
        /// <param name="secondele"></param>
        /// <returns></returns>
        public LcElement GetMergePolyLineEntity(LcElement firstele, LcElement secondele)
        {
            LcElement retele = null;
            List<Curve2d> Segments = new List<Curve2d>();
            if (firstele is LcPolyLine && secondele is LcPolyLine)
            {
                LcPolyLine firstpolyLine = new LcPolyLine();
                LcPolyLine secondpolyLine = new LcPolyLine();
                var doc = this.docRt.Document;
                firstpolyLine = (LcPolyLine)firstele.Clone();
                secondpolyLine = (LcPolyLine)secondele.Clone();
                LcPolyLine newpolyline = (LcPolyLine)firstele.Clone();
                if (LcPolyLine.GetStart(firstpolyLine.Curve2ds[0]) == LcPolyLine.GetStart(secondpolyLine.Curve2ds[0]))
                {
                    firstpolyLine.Curve2ds.Reverse();
                    foreach (var item in firstpolyLine.Curve2ds)
                    {
                        var changestart = LcPolyLine.GetEnd(firstpolyLine.Curve2ds[0]).Clone();
                        var changeend = LcPolyLine.GetStart(firstpolyLine.Curve2ds[0]).Clone();

                        (item as Line2d).Start = changestart;
                        (item as Line2d).End = changeend;
                    }
                    Segments = firstpolyLine.Curve2ds;
                    Segments.AddRange(secondpolyLine.Curve2ds);
                    newpolyline.Curve2ds = Segments;
                    if (LcPolyLine.GetEnd(firstpolyLine.Curve2ds.Last()) == LcPolyLine.GetEnd(secondpolyLine.Curve2ds.Last()))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
                else if (LcPolyLine.GetEnd(firstpolyLine.Curve2ds.Last())== LcPolyLine.GetStart(secondpolyLine.Curve2ds[0]))
                {
                    Segments = firstpolyLine.Curve2ds;
                    Segments.AddRange(secondpolyLine.Curve2ds);
                    newpolyline.Curve2ds = Segments;
                    if (LcPolyLine.GetStart(secondpolyLine.Curve2ds[0]) == LcPolyLine.GetEnd(firstpolyLine.Curve2ds.Last()))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
                else if (LcPolyLine.GetStart(secondpolyLine.Curve2ds[0]) == LcPolyLine.GetEnd(firstpolyLine.Curve2ds.Last()))
                {
                    Segments = secondpolyLine.Curve2ds;
                    Segments.AddRange(firstpolyLine.Curve2ds);
                    newpolyline.Curve2ds = Segments;
                    if (LcPolyLine.GetEnd(firstpolyLine.Curve2ds.Last()) == LcPolyLine.GetStart(secondpolyLine.Curve2ds[0]))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
                else if (LcPolyLine.GetEnd(firstpolyLine.Curve2ds.Last()) == LcPolyLine.GetEnd(secondpolyLine.Curve2ds.Last()))
                {
                    firstpolyLine.Curve2ds.Reverse();
                    foreach (var item in firstpolyLine.Curve2ds)
                    {
                        var changestart = LcPolyLine.GetEnd(firstpolyLine.Curve2ds[0]).Clone();
                        var changeend = LcPolyLine.GetStart(firstpolyLine.Curve2ds[0]).Clone();
                        (item as Line2d).Start = changestart;
                        (item as Line2d).End = changeend;
                        //var changestart = item.End.Clone();
                        //var changeend = item.Start.Clone();
                        //item.Start = changestart; item.End = changeend;
                    }
                    Segments = secondpolyLine.Curve2ds;
                    Segments.AddRange(firstpolyLine.Curve2ds);
                    newpolyline.Curve2ds = Segments;
                    if (LcPolyLine.GetEnd(firstpolyLine.Curve2ds.Last()) == LcPolyLine.GetEnd(secondpolyLine.Curve2ds.Last()))
                    {
                        newpolyline.IsClosed = true;
                    }
                    retele = newpolyline;
                    doc.ModelSpace.RemoveElement(firstele);
                    doc.ModelSpace.RemoveElement(secondele);
                    doc.ModelSpace.InsertElement(retele);
                }
                
            }
            return retele;
        }

        public void ChangeObject(object firstval, object secondval) {
            object changefirst = secondval;
            object changesecond = firstval;
            firstval = changefirst; secondval = changesecond;
        }

        public bool JudgeCollinear(LcLine firstline, LcLine secondline)
        {
            bool retvalue = false;
            //double value = (double)Vector2.Dot(lhs.Normalize(), rhs.Normalize());
            //if (Math.Abs(value) == 1)
            //    retvalue = true;
            double angle = ThreePointGetAngle(firstline.Start, secondline.Start, secondline.End);
            if (angle % 180 != 0) return retvalue;
            angle = ThreePointGetAngle(firstline.End, secondline.Start, secondline.End);
            if (angle % 180 != 0) return retvalue;
            retvalue = true;
            return retvalue;
        }

        /// <summary>
        /// 三点确定角度
        /// </summary>
        /// <param name="startpoint"></param>
        /// <param name="centerpoint"></param>
        /// <param name="endpoint"></param>
        /// <param name="reversal">是否开启顺时针180为正，逆时针180为负,默认为逆时针负角度</param>
        /// <returns></returns>
        public double ThreePointGetAngle(Vector2 startpoint, Vector2 centerpoint, Vector2 endpoint, bool reversal = false)
        {
            double angle = 0;
            var so = new Vector2(startpoint.X - centerpoint.X, startpoint.Y - centerpoint.Y);
            var eo = new Vector2(endpoint.X - centerpoint.X, endpoint.Y - centerpoint.Y);
            var pddir = (so.X * eo.Y) - (so.Y * eo.X);
            angle = (so.Angle() - eo.Angle()) / Math.PI * 180;
            if (reversal)
            {
                angle = (so.Angle() + (pddir < 0 ? (2 * Math.PI - eo.Angle()) : -eo.Angle())) / Math.PI * 180;
            }

            return angle;
        }

        public List<Vector2> GetPointByEle(LcElement lcele) {
            List<Vector2> allpoint = new List<Vector2>();
            if (lcele is LcLine)
            {
                var line = (LcLine)lcele;
                allpoint.Add(line.Start);
                allpoint.Add(line.End);
            }
            else if (lcele is LcPolyLine) {
                var pline = (LcPolyLine)lcele;
                allpoint.Add(LcPolyLine.GetEnd(pline.Curve2ds.First()));
                allpoint.Add(LcPolyLine.GetEnd(pline.Curve2ds.Last()));
            }
            else if (lcele is LcArc)
            {
                var arc = (LcArc)lcele;
                allpoint.Add(arc.Startp);
                allpoint.Add(arc.Endp);
            }
            return allpoint;
        }

        public override void DrawAuxLines(SKCanvas canvas)
        {
           
        }



        private void DrawAuxLine(SKCanvas canvas, Vector2 p0, Vector2 p1)
        {
            var sk_pre = this.vportRt.ConvertWcsToScr(p0).ToSKPoint();
            var sk_p = this.vportRt.ConvertWcsToScr(p1).ToSKPoint();
            //辅助元素的颜色 
            canvas.DrawLine(sk_pre, sk_p, Constants.auxElementPen);
            //辅助曲线的颜色，包括辅助长度，辅助角度等
        }

        public override void CreateElement(LcElement element, Matrix3 matrix)
        {
            
        }
        public override void CreateElement(LcElement element, Vector2 basePoint, double scaleFactor)
        {
           
        }
        public override void CreateElement(LcElement element, Vector2 basePoint, Vector2 scaleVector)
        {
            
        }
       
        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {

        }
        /// <summary>
        /// 拖拽点的设置
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public override ControlGrip[] GetControlGrips(LcElement element)  
        {
            var dimarc = element as DimArc;
            var grips = new List<ControlGrip>();

            return grips.ToArray();
        }
        private string _gripName;
        private Vector2 _position;
        private LcArc _group;

        /// <summary>
        /// 拖拽完成后画弧
        /// </summary>
        /// <param name="element"></param>
        /// <param name="gripName"></param>
        /// <param name="position"></param>
        /// <param name="isEnd"></param>
        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)   
        {
        }
        /// <summary>
        /// 编辑过程中的渲染
        /// </summary>
        /// <param name="canvas"></param>
        public override void DrawDragGrip(SKCanvas canvas)  
        {
            if (_group == null) return;  //如果画的圆弧是空的就返回
            LcArc lcarc = new LcArc();
            if (_gripName == "Start")
            {
                lcarc.Startp = _position; //开始点
                lcarc.Endp = _group.Endp; //结束点
                lcarc.Midp = _group.Midp; //圆弧中间点
                lcarc.Center = GeoUtils.GetCenter(lcarc.Startp, lcarc.Midp, lcarc.Endp);
            }
            if (_gripName == "End")
            {
                lcarc.Startp = _group.Startp;
                lcarc.Endp = _position;
                lcarc.Midp = _group.Midp;
                lcarc.Center = GeoUtils.GetCenter(lcarc.Startp, lcarc.Midp, lcarc.Endp);
            }
            if (_gripName == "ArcMidp")
            {
                lcarc.Startp = _group.Startp;
                lcarc.Endp = _group.Endp;
                lcarc.Midp = _position;
                lcarc.Center = GeoUtils.GetCenter(lcarc.Startp, lcarc.Midp, lcarc.Endp);
            }
            if (_gripName == "Center")
            {
                lcarc.Startp = _group.Startp + (_position - _group.Center);
                lcarc.Endp = _group.Endp + (_position - _group.Center);
                lcarc.Midp = _group.Midp + (_position - _group.Center);
                lcarc.Center = _position;
            }
            double startAngle = (Math.Atan2(lcarc.Startp.Y - lcarc.Center.Y, lcarc.Startp.X - lcarc.Center.X) * 180 / Math.PI);
            double endAngle = (Math.Atan2(lcarc.Endp.Y - lcarc.Center.Y, lcarc.Endp.X - lcarc.Center.X) * 180 / Math.PI);
            endAngle = endAngle / 180 * Math.PI;  
            startAngle = startAngle / 180 * Math.PI;
            Matrix3 matrix = Matrix3.GetTranslate(new Vector2(0, 0));
            Arc2d arc2D = new Arc2d();  //逻辑模型
            arc2D.IsClockwise = false;
            arc2D.StartAngle = startAngle;
            arc2D.EndAngle = endAngle;
            arc2D.Center = lcarc.Center;
            arc2D.Radius = Vector2.Distance(arc2D.Center, lcarc.Startp); 
            this.vportRt.DrawArc(arc2D, matrix, canvas, Constants.auxElementPen);
            return;
        }

        public override List<PropertyObserver> GetPropertyObservers()
        {
            return new List<PropertyObserver>()
            {

            };
        }
    }
}
