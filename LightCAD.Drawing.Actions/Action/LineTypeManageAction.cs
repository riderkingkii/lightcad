﻿using LightCAD.Core.Elements;
using LightCAD.Core;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using LightCAD.Runtime.Interface;
using Avalonia.Controls;
using netDxf.Collections;
using System.IO;

namespace LightCAD.Drawing.Actions
{
    public class LineTypeManageAction
    {
        protected DocumentRuntime docRt;
        public LcDocument lcDocument;
        public ViewportRuntime vportRt;
        protected IDocumentEditor docEditor;
        public LineTypeManageAction(IDocumentEditor docEditor)
        {
            this.docEditor = docEditor;
            this.docRt = docEditor.DocRt;
            this.vportRt = (docEditor as DrawingEditRuntime).ActiveViewportRt;
            this.lcDocument = docRt.Document;
        }

        public async void ExecCreate(string[] args)
        {
            
            var pointInputer = new PointInputer(this.docEditor);
            var elementSetInputer = new ElementSetInputer(this.docEditor);

            byte[] buffer = Guid.NewGuid().ToByteArray();
            int lcid = (int)BitConverter.ToInt64(buffer, 0);
            //this.lcDocument.LineTypes.Clear();
            //LoadLinToPath("CENTER", "Center", new List<double> { 1.25, -0.25, 0.25, -0.25 });
            var win = (ILineTypeManageWindow)AppRuntime.UISystem.CreateWindow("LineTypeManageWindow", this);
            var result = AppRuntime.UISystem.ShowDialog(win);
            if (result == LcDialogResult.OK)
            {

            }
        }

        public SKPathEffect GetSKPathEffect(LcLineType lclinetype)
        {
            var typelist = lclinetype.LineTypeDefinition.TrimStart('A').TrimStart(',').split(",");
            float[] floatlist = new float[typelist.Count];
            for (int i = 0; i < typelist.Length; i++)
            {
                List<string> floatsp = typelist[i].split(".");
                if (floatsp.Count == 1)
                {
                    floatlist[i] = float.Parse("0." + floatsp[0]);
                }
                else {
                    floatlist[i] = float.Parse(floatsp[0] + "." + floatsp[1]);
                }
            }
            SKPathEffect skpath = SKPathEffect.CreateDash(floatlist, 0);
            return skpath;
        }
    }
}
