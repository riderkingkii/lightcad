﻿using HarfBuzzSharp;
using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using LightCAD.Three;
using netDxf.Entities;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using LightCAD.MathLib;

namespace LightCAD.Drawing.Actions
{
    public class DimJoggedActio : ElementAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;

        static DimJoggedActio()
        {
            CommandName = "DIJOGGED";
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "DJO",
                Description = "DIJOGGED",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep{ Name="Step0", Options="选择圆或圆弧:" },
                     new LcCreateStep{ Name="Step1", Options="指示图示中心位置:" },
                     new LcCreateStep{ Name="Step2", Options="指示尺寸线位置:" },
                     new LcCreateStep{ Name="Step3", Options="指示折弯位置:" },
                }
            };
            
        }

        internal static void Initilize()
        {
            ElementActions.DimJogged = new DimJoggedActio();
            LcDocument.ElementActions.Add(BuiltinElementType.DimJogged, ElementActions.DimJogged);
        }

        private PointInputer inputer { get; set; }

        private Vector2 point;
        private Vector2 endpoint;
        private Vector2 secondpoint;
        private Vector2 threepoint;
        private Vector2 copypoint;
        private Vector2 copythreepoint;
        private Vector2 copyendpoint;
        private Vector2 copythreepoint2;
        private Vector2 copyendpoint2;
        private Vector2 center;
        private double raduis;
    
        public DimJoggedActio() { }
        public DimJoggedActio(IDocumentEditor docEditor) : base(docEditor)
        {
            docEditor.CommandCenter.WriteInfo("命令 ：DJO");
        }
        private LcCreateMethod GetMethod(string method)
        {
            if (method == null) return CreateMethods[0];
            var getted= CreateMethods.FirstOrDefault((m) => m.Name == method);
            if (getted == null)
                return CreateMethods[0];
            else
                return getted;
        }
        public async void ExecCreate(string[] args = null)
        {
            this.StartCreating();
            //this.Segments = new List<RolineSegment>();
            var curMethod = CreateMethods[0];
            var ElementInputer = new ElementInputer(this.docEditor);
            this.inputer = new PointInputer(this.docEditor);
            Step0:
            var step0 = curMethod.Steps[0];
            var result0 = await ElementInputer.Execute(step0.Options);
            if (ElementInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result0.ValueX == null)
            {
                goto Step0;
            }
            else
            {
                LcElement element = (LcElement)result0.ValueX;
                this.point = (Vector2)result0.Extent;
                if (CreateSelects(element))
                {
                    if (element is LcArc) {
                        this.raduis = (element as LcArc).Radius;
                        this.center = (element as LcArc).Center;
                    }
                    if (element is LcCircle) {
                        this.raduis = (element as LcCircle).Radius;
                        this.center = (element as LcCircle).Center;
                    }
                    #region
                    LcLine lcline = new LcLine(this.center, this.point);
                    //需要移动的向量方向
                    Vector2 direction = new Vector2();
                    //需要移动的距离
                    double dis = Math.Abs(lcline.Length - this.raduis);
                    if (lcline.Length > this.raduis)
                    {
                        //向内延伸
                        direction = new Vector2(this.center.X - this.point.X, this.center.Y - this.point.Y);
                    }
                    else if (lcline.Length < this.raduis)
                    {
                        //向外部延伸
                        direction = new Vector2(this.point.X - this.center.X, this.point.Y - this.center.Y);
                    }
                    if (direction.Length() != 0)
                    {
                        //新的移动后的点位
                        Vector2 lidisptpos = this.point + (new Vector2(direction.X * dis, direction.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(direction.X, 2.0) + Math.Pow(direction.Y, 2.0))));
                        this.point = lidisptpos;
                    }
                    #endregion
                    goto Step1;
                }
                else
                {
                    goto Step0;
                }
            }
            Step1:
            var step1 = curMethod.Steps[1];
            var result1 = await inputer.Execute(step1.Options);
            if (inputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result1 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result1.ValueX == null)
            {
                if (result1.Option != null)
                {
                }
                else
                {
                    goto Step2;
                }
            }
            else
            {
                this.secondpoint = (Vector2)result1.ValueX;
                goto Step2;
            }
            Step2:
            var step2 = curMethod.Steps[2];
            var result2 = await inputer.Execute(step2.Options);
            if (inputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result2 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result2.ValueX == null)
            {
                if (result2.Option != null)
                {
                    
                }
                else
                {
                    goto Step3;
                }
            }
            else
            {
                this.threepoint = (Vector2)result2.ValueX;
                goto Step3;
            }
            Step3:
            var step3 = curMethod.Steps[3];
            var result3 = await inputer.Execute(step3.Options);
            if (inputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result3 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result3.ValueX == null)
            {
                if (result1.Option != null)
                {
                }
                else
                {
                    goto End;
                }
            }
            else
            {
                this.endpoint = (Vector2)result3.ValueX;
                CreateDimElement();
                goto End;
            }

            End:
            this.EndCreating();

        }
        private void CreateDimElement()
        {
            var doc = this.docRt.Document;
            DocumentManager.CurrentRecorder.BeginAction("DimJogged");
            var dimRadialLarge = doc.CreateObject<DimRadialLarge>();
            dimRadialLarge.Start = this.copypoint;
            dimRadialLarge.Second = this.secondpoint;
            dimRadialLarge.Three = this.copythreepoint2;
            dimRadialLarge.End = this.copyendpoint2;
            //折弯显示半径
            //dimRadialLarge.Dimtext = (this.raduis).ToString("0.00");

            LcText text = doc.CreateObject<LcText>();
            text.Start = this.copythreepoint2;
            text.TextStart = this.copythreepoint2;
            text.Heigh = 400;// this.raduis * 2 / 20 < 1 ? 1 : Math.Round(this.raduis * 2 / 20);
            text.Text = (this.raduis).ToString("0.00");
            text.Rotate = 180 / Math.PI * (this.copypoint - this.copythreepoint2).Angle();
            text.Alignment = "左对齐";
            text.Widthfactor = 1;
            text.Tilt = 0;

            dimRadialLarge.Dimtext = text;
            doc.ModelSpace.InsertElement(dimRadialLarge);
            this.docRt.Action.ClearSelects();
            DocumentManager.CurrentRecorder.EndAction();
        }


        /// <summary>
        /// 选中的对象
        /// </summary>
        /// <param name="elements"></param>
        /// <returns></returns>
        private bool CreateSelects(LcElement elements)
        {
            if (elements is LcCircle || elements is LcArc)
            {
                return true;
            }
            return false;

        }


        /// <summary>
        /// 对一个坐标点按照一个中心进行旋转
        /// </summary>
        /// <param name="center">中心点</param>
        /// <param name="p1">要旋转的点</param>
        /// <param name="angle">旋转角度，笛卡尔直角坐标</param>
        /// <returns></returns>
        private Vector2 PointRotate(Vector2 center, Vector2 p1, double angle)
        {
            Vector2 tmp = new Vector2();
            double angleHude = angle * Math.PI / 180;/*角度变成弧度*/
            double x1 = (p1.X - center.X) * Math.Cos(angleHude) + (p1.Y - center.Y) * Math.Sin(angleHude) + center.X;
            double y1 = -(p1.X - center.X) * Math.Sin(angleHude) + (p1.Y - center.Y) * Math.Cos(angleHude) + center.Y;
            tmp.X = (int)x1;
            tmp.Y = (int)y1;
            return tmp;
        }
        public override void Cancel()
        {
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
        }
       
        

        public override void CreateElement(LcElement element, Matrix3 matrix)
        {
            //DocumentManager.CurrentRecorder.BeginAction("Circle");
            //var dim = element as DimDiametric;
            //this.vportRt.ActiveElementSet.AddDimDiametric(matrix.MultiplyPoint(dim.Start), matrix.MultiplyPoint(dim.End),dim.Dimtext);
            //DocumentManager.CurrentRecorder.EndAction();
        }
        public override void CreateElement(LcElement element,Vector2 basePoint, double scaleFactor)
        {
            //var circle = element as LcCircle;
            //Matrix3d matrix3 = Matrix3d.Scale(scaleFactor, basePoint);
            //this.vportRt.ActiveElementSet.AddCircle(matrix3.MultiplyPoint(circle.Center), circle.Radius * scaleFactor);
        }
        public override void CreateElement(LcElement element, Vector2 basePoint, Vector2 scaleVector)
        {
            //var circle = element as LcCircle;
            //Matrix3d matrix3 = Matrix3d.Scale(scaleVector, basePoint);
            //this.vportRt.ActiveElementSet.AddCircle(matrix3.MultiplyPoint(circle.Center), circle.Radius * Vector2d.Distance(basePoint, scaleVector));
        }
        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var dim = element as DimRadialLarge;
          
          
            var mstart= matrix.MultiplyPoint(dim.Start);
            var msecond = matrix.MultiplyPoint(dim.Second);
            var mthree = matrix.MultiplyPoint(dim.Three);
            var mend = matrix.MultiplyPoint(dim.End);
            var mtext =  dim.Dimtext;
            var start = this.vportRt.ConvertWcsToScr(mstart).ToSKPoint();
            var secon = this.vportRt.ConvertWcsToScr(msecond).ToSKPoint();
            var three = this.vportRt.ConvertWcsToScr(mthree).ToSKPoint();
            var end= this.vportRt.ConvertWcsToScr(mend).ToSKPoint();
            //get Layer color 
            //bool isDragCopy = (matrix != Matrix3d.Zero);
            var pen = this.GetDrawPen(dim);

            Vector2 direction = new Vector2(dim.Start.X - dim.Three.X, dim.Start.Y - dim.Three.Y);
            double dis = Math.Abs(5000);
            Vector2 lidisptpos = dim.Start + (new Vector2(direction.X * dis, direction.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(direction.X, 2.0) + Math.Pow(direction.Y, 2.0))));
          

            var newend = this.vportRt.ConvertWcsToScr(lidisptpos).ToSKPoint();

           

            if (pen == Runtime.Constants.defaultPen)
            {
                //TODO:这里可以考虑将实线用颜色做KEY，对SKPaint进行缓存
                using (var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
                {

                    canvas.DrawLine(start,three, elePen);
                    canvas.DrawLine(secon, end, elePen);
                    canvas.DrawLine(three, end, elePen);

                    //using (var paint = new SKPaint())
                    //{

                    //    var fontManager = SKFontManager.Default;
                    //    var emojiTypeface = fontManager.MatchCharacter('时');
                    //    paint.TextSize = (float)(this.vportRt.Viewport.Scale * 200);
                    //    //paint.TextAlign = SKTextAlign.Right;
                    //    paint.IsAntialias = false;
                    //    paint.Color = new SKColor(0x42, 0x81, 0xA4);
                    //    paint.IsStroke = false;
                    //    paint.Typeface = emojiTypeface;

                    //    var skpath = new SKPath();
                    //    skpath.MoveTo(start);
                    //    skpath.LineTo((float)three.X, (float)three.Y);
                        
                        

                    //    canvas.DrawTextOnPath(mtext, skpath, 0, 0, paint);


                    //}
                    //elePen.Color = SKColors.Red;
                   
                    //using (var paint = new SKPaint())
                    //{

                    //    paint.TextSize = (float)this.vportRt.Viewport.Scale*200;
                    //    //paint.TextAlign = SKTextAlign.Right;
                    //    paint.IsAntialias = false;
                    //    paint.Color = new SKColor(0x42, 0x81, 0xA4);
                    //    paint.IsStroke = false;

                    //   //var skpath = new SKPath();
                    //   // skpath.MoveTo(start);
                    //   // skpath.LineTo((float)start.X, (float)start.Y);
                    //   // skpath.LineTo((float)end.X, (float)end.Y);

                    //   // canvas.DrawTextOnPath("aling a path", skpath, 20, 8, paint);

                    //    canvas.DrawText(mtext, start.X, start.Y, paint);
                    //    // canvas.RotateDegrees(50);
                    //}


                }
            }
            else
            {
                canvas.DrawLine(start, three, pen);
                canvas.DrawLine(secon, end, pen);
                canvas.DrawLine(three, end, pen);
                //using (var paint = new SKPaint())
                //{

                //    var fontManager = SKFontManager.Default;
                //    var emojiTypeface = fontManager.MatchCharacter('时');
                //    paint.TextSize = (float)(this.vportRt.Viewport.Scale * 200);
                //    //paint.TextAlign = SKTextAlign.Right;
                //    paint.IsAntialias = false;
                //    paint.Color = new SKColor(0x42, 0x81, 0xA4);
                //    paint.IsStroke = false;
                //    paint.Typeface = emojiTypeface;

                //    var skpath = new SKPath();
                //    skpath.MoveTo(start);
                //    skpath.LineTo((float)three.X, (float)three.Y);

                //    canvas.DrawTextOnPath(mtext, skpath, 0, 0, paint);


                //}
               
            }
            var eleAction = (dim.Dimtext.RtAction as ElementAction);

            eleAction.SetViewport(this.vportRt).Draw(canvas, dim.Dimtext, matrix);


        }
        //public override void Scale(Vector2d basePoint, double scaleFactor)
        //{
        //    Matrix3d matrix3 = Matrix3d.Scale(scaleFactor, basePoint);
        //    //this.Set(matrix3.MultiplyPoint(this.point), matrix3.MultiplyPoint(this.endpoint));
           
        //}
        private SKPath DrawArrow(Vector2 start, Vector2 end, double extraAngle = 180)
        {
            LcLine lcline = new LcLine();
            lcline.Start = start;
            lcline.End = end;
            var radian = lcline.Angle;

            var startve = new Vector2(lcline.Start.X, lcline.Start.Y);
            var arrowtwove = new Vector2(lcline.Start.X + 100, lcline.Start.Y + 30);
            var arrowendve = new Vector2(lcline.Start.X + 100, lcline.Start.Y - 30);
            var angle = Utils.RadianToDegree(radian) + extraAngle;
            arrowtwove = Vector2.Rotate(arrowtwove, startve, angle);
            arrowendve = Vector2.Rotate(arrowendve, startve, angle);

            var pointone = this.vportRt.ConvertWcsToScr(startve).ToSKPoint();
            var pointtwo = this.vportRt.ConvertWcsToScr(arrowtwove).ToSKPoint();
            var pointthree = this.vportRt.ConvertWcsToScr(arrowendve).ToSKPoint();

            var path = new SKPath();
            path.FillType = SKPathFillType.EvenOdd;
            //外圈 顺时针
            path.MoveTo((float)pointone.X, (float)pointone.Y);    //起点
            path.LineTo((float)pointtwo.X, (float)pointtwo.Y);
            path.LineTo((float)pointthree.X, (float)pointthree.Y);
            path.Close();
            return path;
        }


        public override void DrawAuxLines(SKCanvas canvas)
        {
            if (this.point != null && this.secondpoint != null) {
                var mp = this.vportRt.PointerMovedPosition.ToVector2d();
                var wcs_mp = this.vportRt.ConvertScrToWcs(mp);


                if (this.threepoint != null && this.endpoint == null)
                {
                    //var pt = Line2d.GetFootofperpendicular(wcs_mp, this.point, this.copythreepoint);
                    //var pt1 = Line2d.GetFootofperpendicular(wcs_mp, this.secondpoint, this.copyendpoint);
                    //向量
                    //var vector = new Vector2d(wcs_mp.X - pt.X, wcs_mp.Y - pt.Y);
                    //var vector1 = new Vector2d(wcs_mp.X - pt1.X, wcs_mp.Y - pt1.Y);
                    var vector1 = new Vector2(this.secondpoint.X - this.copyendpoint.X, this.secondpoint.Y - this.copyendpoint.Y);
                    var vector2 = new Vector2(this.copypoint.X - this.copythreepoint.X, this.copypoint.Y - this.copythreepoint.Y);

                    double jl = distance(wcs_mp, this.center)/2;
                    Vector2 newWCS_mp = new Vector2();
                    Vector2 newWCS_mp2 = new Vector2();
                    //if (distance(wcs_mp, this.center) > distance(this.copypoint, this.copythreepoint2) || distance(wcs_mp, this.center) > distance(this.secondpoint, this.copyendpoint2))
                    // {
                    
                    //}
                    //if (distance(this.point, this.copythreepoint2) <= 100 || distance(this.secondpoint, this.copyendpoint2) <= 100)
                    //{
                    //    newWCS_mp = this.copythreepoint - (new Vector2d(vector2.X * jl, vector2.Y * jl)) / Math.Sqrt(Math.Abs((Math.Pow(vector2.X, 2.0) + Math.Pow(vector2.Y, 2.0))));
                    //    newWCS_mp2 = this.copyendpoint + (new Vector2d(vector1.X * jl, vector1.Y * jl)) / Math.Sqrt(Math.Abs((Math.Pow(vector1.X, 2.0) + Math.Pow(vector1.Y, 2.0))));

                    //    //Vector2d newWCS_mp = new Vector2d(this.copythreepoint.X + vector.X, this.copythreepoint.Y + vector.Y);
                    //    //Vector2d newWCS_mp2 = new Vector2d(this.copyendpoint.X + vector1.X, this.copyendpoint.Y + vector1.Y);
                    //    //DrawAuxLine(canvas, firstPoint, newWCS_mp);
                    //    //DrawAuxLine(canvas, secondPoint, newWCS_mp2);
                    //    //DrawAuxLine(canvas, newWCS_mp, newWCS_mp2);

                    //    DrawAuxLine(canvas, this.point, newWCS_mp);
                    //    DrawAuxLine(canvas, this.secondpoint, newWCS_mp2);


                    //    DrawAuxLine(canvas, newWCS_mp, newWCS_mp2);

                    //}
                    //else {
                        newWCS_mp = this.copythreepoint + (new Vector2(vector2.X * jl, vector2.Y * jl)) / Math.Sqrt(Math.Abs((Math.Pow(vector2.X, 2.0) + Math.Pow(vector2.Y, 2.0))));
                        newWCS_mp2 = this.copyendpoint - (new Vector2(vector1.X * jl, vector1.Y * jl)) / Math.Sqrt(Math.Abs((Math.Pow(vector1.X, 2.0) + Math.Pow(vector1.Y, 2.0))));

                        //Vector2d newWCS_mp = new Vector2d(this.copythreepoint.X + vector.X, this.copythreepoint.Y + vector.Y);
                        //Vector2d newWCS_mp2 = new Vector2d(this.copyendpoint.X + vector1.X, this.copyendpoint.Y + vector1.Y);
                        //DrawAuxLine(canvas, firstPoint, newWCS_mp);
                        //DrawAuxLine(canvas, secondPoint, newWCS_mp2);
                        //DrawAuxLine(canvas, newWCS_mp, newWCS_mp2);

                        DrawAuxLine(canvas, this.copypoint, newWCS_mp);
                        DrawAuxLine(canvas, this.secondpoint, newWCS_mp2);


                        DrawAuxLine(canvas, newWCS_mp, newWCS_mp2);

                       

                    //}

                    this.copythreepoint2 = newWCS_mp;
                    this.copyendpoint2 = newWCS_mp2;





                }
                else { 

                Vector2 newstart = PointRotate(this.center, this.point, SEAngle(this.center, this.point, wcs_mp));
                //Vector2d newsecond = PointRotate(_dim.Center, newstart, 180);
                //计算两点距离
                double jl= distance(newstart, this.secondpoint)/2;

                //通过计算得出第三个点
                Vector2 direction = new Vector2(this.center.X - newstart.X, this.center.Y - newstart.Y);

                Vector2 newsecond = newstart + (new Vector2(direction.X * jl, direction.Y * jl)) / Math.Sqrt(Math.Abs((Math.Pow(direction.X, 2.0) + Math.Pow(direction.Y, 2.0))));

                //通过计算得出第四个点
                Vector2 direction2 = new Vector2(newstart.X - this.center.X, newstart.Y - this.center.Y);

                Vector2 newend = this.secondpoint + (new Vector2(direction2.X * jl, direction2.Y * jl)) / Math.Sqrt(Math.Abs((Math.Pow(direction2.X, 2.0) + Math.Pow(direction2.Y, 2.0))));


                //var mp = this.vportRt.PointerMovedPosition.ToVector2d();
                //var wcs_mp = this.vportRt.ConvertScrToWcs(mp);


                DrawAuxLine(canvas,newstart, newsecond);
                DrawAuxLine(canvas, this.secondpoint, newend);
                DrawAuxLine(canvas, newsecond, newend);
                //记录最新的起点以及第三个点
                this.copypoint = newstart;
                this.copythreepoint = newsecond;
                this.copyendpoint = newend;
                }

            }



        }

        private void DrawAuxLine(SKCanvas canvas, Vector2 p0, Vector2 p1)
        {
            var sk_pre = this.vportRt.ConvertWcsToScr(p0).ToSKPoint();
            var sk_p = this.vportRt.ConvertWcsToScr(p1).ToSKPoint();
            //辅助元素的颜色 
            canvas.DrawLine(sk_pre, sk_p, new SKPaint { Color = this.vportRt.GetAuxColorValue(), IsStroke = true });
            //辅助曲线的颜色，包括辅助长度，辅助角度等
        }
        #region Grip
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var dim = element as DimRadialLarge;

            var grips = new List<ControlGrip>();
            var gripStartr = new ControlGrip
            {
                Element = dim,
                Name = "Start",
                Position = dim.Start
            };
            grips.Add(gripStartr);

            var gripEnd = new ControlGrip
            {
                Element = dim,
                Name = "Second",
                Position = dim.Second
            };
            grips.Add(gripEnd);


            return grips.ToArray();
        }
        private string _gripName;
        private Vector2 _position;
        private DimRadialLarge _dim;


        /// <summary>  
        /// 根据余弦定理求两个线段夹角  
        /// </summary>  
        /// <param name="o">端点</param>  
        /// <param name="s">start点</param>  
        /// <param name="e">end点</param>  
        /// <returns></returns>  
        public double SEAngle(Vector2 o, Vector2 s, Vector2 e)
        {

            var so = new Vector2(s.X - o.X, s.Y - o.Y);
            var eo = new Vector2(e.X - o.X, e.Y - o.Y);
            return (so.Angle() - eo.Angle()) /Math.PI*180;

        }


        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            var dim= element as DimRadialLarge;

            _dim = dim;
            
        }


        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_dim == null) return;
            //直径
            var diameter = distance(_dim.Start, _dim.End);

            






        }

        #endregion
        public static double distance(Vector2 p1, Vector2 p2)
        {
            double result = 0;
            result = Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
            return result;

        }
        public override List<PropertyObserver> GetPropertyObservers()
        {
            return new List<PropertyObserver>();
            //{
            //    new PropertyObserver()
            //    {
            //        Name = "CenterX",
            //        DisplayName = "圆心 X 坐标",
            //        Getter = (ele) => Math.Round((ele as LcCircle).Center.X, 4),
            //        Setter = (ele, value) =>
            //        {
            //            var circle = (ele as LcCircle);
            //            var x = Convert.ToDouble(value);
            //            var cen = new Vector2d(x, circle.Center.Y);
            //            circle.Set(center : cen);
            //        }
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "CenterY",
            //        DisplayName = "圆心 Y 坐标",
            //        Getter = (ele) => Math.Round((ele as LcCircle).Center.Y, 4),
            //        Setter = (ele, value) =>
            //        {
            //            var circle = (ele as LcCircle);
            //            var y = Convert.ToDouble(value);
            //            var cen = new Vector2d(circle.Center.X, y);
            //            circle.Set(center : cen);
            //        }
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "Radius",
            //        DisplayName = "半径",
            //        Getter = (ele) => Math.Round((ele as LcCircle).Radius, 4),
            //        Setter = (ele, value) =>
            //        {
            //            var circle = (ele as LcCircle);
            //            var radius = Convert.ToDouble(value);
            //            circle.Set(radius : radius);
            //        }
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "Diameter",
            //        DisplayName = "直径",
            //        Getter = (ele) => Math.Round((ele as LcCircle).Radius * 2, 4),
            //        Setter = (ele, value) =>
            //        {
            //            var circle = (ele as LcCircle);
            //            var radius = Convert.ToDouble(value) / 2;
            //            circle.Set(radius : radius);
            //        }
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "Circumference",
            //        DisplayName = "周长",
            //        Getter = (ele) => Math.Round((ele as LcCircle).Radius * 2 * Math.PI, 4),
            //        Setter = (ele, value) =>
            //        {
            //            var circle = (ele as LcCircle);
            //            var radius = Convert.ToDouble(value) / 2 / Math.PI;
            //            circle.Set(radius : radius);
            //        }
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "Area",
            //        DisplayName = "面积",
            //        Getter = (ele) =>
            //        {
            //            var radius = (ele as LcCircle).Radius;
            //            return Math.Round(Math.Pow(radius, 2) * Math.PI, 4);
            //        },
            //        Setter = (ele, value) =>
            //        {
            //            var circle = (ele as LcCircle);
            //            var area = Convert.ToDouble(value);
            //            var radius = Math.Sqrt(area / Math.PI);
            //            circle.Set(radius : radius);
            //        }
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "NormalX",
            //        DisplayName = "法向 X 坐标",
            //        Getter = (ele) => 0
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "NormalY",
            //        DisplayName = "法向 Y 坐标",
            //        Getter = (ele) => 0
            //    },
            //    new PropertyObserver()
            //    {
            //        Name = "NormalZ",
            //        DisplayName = "法向 Z 坐标",
            //        Getter = (ele) => 1
            //    },
            //};
        }
    }
}
