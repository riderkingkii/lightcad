﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Drawing.Actions
{
    public class ComponentRefAction : ElementAction
    {
        public static LcCreateMethod[] CreateMethods;

        static ComponentRefAction()
        {
        }

        internal static void Initilize()
        {
            ElementActions.ComponentRef = new ComponentRefAction();
            LcDocument.ElementActions.Add(BuiltinElementType.ComponentRef, ElementActions.ComponentRef);
        }

        private List<Vector2> points;
        private Vector2 originPoint { get; set; }
        private Vector2 thruPoint { get; set; }
        private PointInputer inputer { get; set; }

        private ComponentRefAction() { }
        public ComponentRefAction(IDocumentEditor docEditor) : base(docEditor)
        {
        }
 
  
        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var comRef = element as LcComponentInstance;
           
        }
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var line = element as LcComponentInstance;
            var grips = new List<ControlGrip>();
            
            return grips.ToArray();
        }

        private string _gripName;
        private Vector2 _position;
        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            var line = element as LcComponentInstance;
            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {
                //if (gripName == "Start")
                //    line.Set(start: position);
                //else if (gripName == "End")
                //{
                //    _line.Set(end: position);
                //}
            }
        }

        public override void DrawDragGrip(SKCanvas canvas)
        {
         
        }
    }
}
