﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Drawing.Actions
{
    public class BlockRefAction : ElementAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;

        static BlockRefAction()
        {
            CreateMethods = new LcCreateMethod[3];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "Create",
                //Description = "创建组",
                Steps = new LcCreateStep[]
                {
                    //new LcCreateStep { Name=  "Step0", Options= "选择对象或[名称(N) 说明(D)]:" },
                    //new LcCreateStep { Name=  "Step1", Options= "输入编组名或[?]:" },
                    //new LcCreateStep { Name=  "Step2", Options= "输入组说明:" },
                }
            };

        }

        internal static void Initilize()
        {
            ElementActions.BlockRef = new BlockRefAction();
            LcDocument.ElementActions.Add(BuiltinElementType.BlockRef, ElementActions.BlockRef);
        }


        private BlockRefAction() { }
        public BlockRefAction(IDocumentEditor docRt) : base(docRt)
        {

        }

        public async void ExecInsert(string[] args = null)
        {

        }



        public override void Cancel()
        {
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
            //TODO:
            this.EndCreating();
        }

        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var blockRef = element as LcBlockRef;
            RefStack.Push(blockRef);
            try
            {
                var mat = matrix * blockRef.Matrix;
                foreach (var ele in blockRef.Block.Elements)
                {
                    var eleAction = (ele.RtAction as ElementAction);

                    eleAction.SetViewport(this.vportRt).Draw(canvas, ele, mat);

                }
            }catch(Exception ex)
            {
            }
            finally
            {
                RefStack.Pop();
            }
           
            if(matrix == Matrix3.Identity)
            {
                var leftTop = this.vportRt.ConvertWcsToScr(blockRef.BoundingBox.LeftTop);
                var rightBottom = this.vportRt.ConvertWcsToScr(blockRef.BoundingBox.RightBottom);
                canvas.DrawRect((float)leftTop.X, (float)leftTop.Y, (float)rightBottom.X - (float)leftTop.X, (float)rightBottom.Y - (float)leftTop.Y, Constants.SilverPen);
            }


            //var bbox = blockRef.BoundingBox;
            //var points = new List<Vector2d> { bbox.LeftBottom, bbox.RightBottom, bbox.RightTop, bbox.LeftTop, bbox.LeftBottom };
            //var convMat = matrix * vportRt.WcsToScr;
            //points = points.Select(p => convMat.MultiplyPoint(p)).ToList();

            //var skpoints = points.Select<Vector2d, SKPoint>(p => p.ToSKPoint()).ToArray();
            //canvas.DrawPoints(SKPointMode.Polygon, skpoints, Constants.SilverPen);
        }

        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var blkRef = element as LcBlockRef;
            var grips = new List<ControlGrip>();
            var insertPoint = new ControlGrip
            {
                Element = blkRef,
                Name = "InsertPoint",
                Position = blkRef.Matrix.GetTranslateVector()
            };
            grips.Add(insertPoint);
            return grips.ToArray();
        }
        private string _gripName;
        private Vector2 _position;
        private Vector2 _endDrag;
        private LcGroup _group;

        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            _group = element as LcGroup;

            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {
                if (gripName == "Center")
                {
                    var delta = position - _group.BoundingBox.Center;
                    _group.Translate(delta);
                }
            }
        }


        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_group == null) return;

            var offset = _position - _group.BoundingBox.Center;
            foreach (var ele in _group.Elements)
            {
                (ele.RtAction as ElementAction).Draw(canvas, ele, offset);
            }
        }

    }
}
