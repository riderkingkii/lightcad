﻿using Avalonia.Media.TextFormatting.Unicode;
using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.MathLib;
using netDxf.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using static LightCAD.Core.Elements.LcPolyLine;

namespace LightCAD.Drawing.Actions
{
    public class BreakAction : ElementAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;

        static BreakAction()
        {
            CommandName = "BREAK";
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "BR",
                Description = "BREAK",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep{ Name="Step0", Options="BREAK 选择对象:" },
                    new LcCreateStep { Name="Step1", Options= "BREK指定第二个打断点 或 [第一点(F)]:" },
                }
            };
        }

        internal static void Initilize()
        {
            ElementActions.Break = new BreakAction();
            //LcDocument.ElementActions.Add(BuiltinElementType.Break, ElementActions.Break);
        }

        private PointInputer inputer { get; set; }

        private Vector2 point;
        private Vector2 endpoint;
        private Vector2 center;
        private double raduis;
        private LcElement lcElement;

        public BreakAction() { }
        public BreakAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令 ：BR");
        }
        private LcCreateMethod GetMethod(string method)
        {
            if (method == null) return CreateMethods[0];
            var getted = CreateMethods.FirstOrDefault((m) => m.Name == method);
            if (getted == null)
                return CreateMethods[0];
            else
                return getted;
        }
        public async void ExecCreate(string[] args = null)
        {
            this.StartCreating();
            //this.Segments = new List<RolineSegment>();
            var curMethod = SetCurMethod(CreateMethods, 0);
            var ElementInputer = new ElementInputer(this.docEditor);
            this.inputer = new PointInputer(this.docEditor);
        Step0:
            var step0 = curMethod.Steps[0];


            var result0 = await ElementInputer.Execute(step0.Options);


            if (ElementInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result0.ValueX == null)
            {
                goto Step0;
            }
            else
            {
                this.lcElement = (LcElement)result0.ValueX;
                this.point = (Vector2)result0.Extent;
                if (CreateSelects(this.lcElement))
                {
                    //记住圆的圆心点 以及半径
                    if (this.lcElement is LcCircle || this.lcElement is LcArc)
                    {
                        if (this.lcElement is LcCircle)
                        {
                            var circle = this.lcElement as LcCircle;
                            this.center = circle.Center;
                            this.raduis = circle.Radius;
                            #region
                            Line2d line = new Line2d(this.center, this.point);
                            //需要移动的向量方向
                            Vector2 direction = new Vector2();
                            //需要移动的距离
                            double dis = Math.Abs(line.Length - this.raduis);
                            if (line.Length > this.raduis)
                            {
                                //向内延伸
                                direction = new Vector2(this.center.X - this.point.X, this.center.Y - this.point.Y);
                            }
                            else if (line.Length < this.raduis)
                            {
                                //向外部延伸
                                direction = new Vector2(this.point.X - this.center.X, this.point.Y - this.center.Y);
                            }
                            if (direction.Length() != 0)
                            {
                                //新的移动后的点位
                                Vector2 lidisptpos = this.point + (new Vector2(direction.X * dis, direction.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(direction.X, 2.0) + Math.Pow(direction.Y, 2.0))));
                                this.point = lidisptpos;
                            }
                            #endregion
                        }
                        else
                        {
                            var circle = this.lcElement as LcArc;
                            this.center = circle.Center;
                            this.raduis = circle.Radius;
                            #region
                            Line2d line = new Line2d(circle.Center, this.point);
                            //需要移动的向量方向
                            Vector2 direction = new Vector2();
                            //需要移动的距离
                            double dis = Math.Abs(line.Length - this.raduis);
                            if (line.Length > this.raduis)
                            {
                                //向内延伸
                                direction = new Vector2(this.center.X - this.point.X, this.center.Y - this.point.Y);
                            }
                            else if (line.Length < this.raduis)
                            {
                                //向外部延伸
                                direction = new Vector2(this.point.X - this.center.X, this.point.Y - this.center.Y);
                            }
                            if (direction.Length() != 0)
                            {
                                //新的移动后的点位
                                Vector2 lidisptpos = this.point + (new Vector2(direction.X * dis, direction.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(direction.X, 2.0) + Math.Pow(direction.Y, 2.0))));
                                this.point = lidisptpos;
                            }
                            #endregion
                        }
                    }
                    if (this.lcElement is LcLine || this.lcElement is LcRay || this.lcElement is LcXLine || this.lcElement is LcPolyLine)
                    {
                        //this.point = (Vector2)result0.Extent;
                    }
                    goto Step1;
                }
                goto Step0;

            }
        Step1:

            var step1 = curMethod.Steps[1];
            var result1 = await this.inputer.Execute(step1.Options);
            if (result1.Option == "F")
            {
                goto Step0;
            }
            else if (result1.Option == "@")
            {
                if (this.lcElement is LcCircle)
                {
                    this.commandCtrl.WriteInfo("圆弧不能是 360 度");
                    goto End;
                }
                this.endpoint = this.point;
            }
            else
            {
                //记住圆的圆心点 以及半径
                if (this.lcElement is LcCircle || this.lcElement is LcArc)
                {
                    //if (this.lcElement is LcCircle)
                    //{
                    this.endpoint = (Vector2)result1.ValueX;
                    #region
                    Line2d line = new Line2d(this.center, this.endpoint);
                    //需要移动的向量方向
                    Vector2 direction = new Vector2();
                    //需要移动的距离
                    double dis = Math.Abs(line.Length - this.raduis);
                    if (line.Length > this.raduis)
                    {
                        //向内延伸
                        direction = new Vector2(this.center.X - this.endpoint.X, this.center.Y - this.endpoint.Y);
                    }
                    else if (line.Length < this.raduis)
                    {
                        //向外部延伸
                        direction = new Vector2(this.endpoint.X - this.center.X, this.endpoint.Y - this.center.Y);
                    }
                    if (direction.Length() != 0)
                    {
                        //新的移动后的点位
                        Vector2 lidisptpos = this.endpoint + (new Vector2(direction.X * dis, direction.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(direction.X, 2.0) + Math.Pow(direction.Y, 2.0))));
                        this.endpoint = lidisptpos;
                    }
                    #endregion
                    //}
                    //else
                    //{
                    //    goto Step0;
                    //}


                }
                else if (this.lcElement is LcLine || this.lcElement is LcRay || this.lcElement is LcXLine || this.lcElement is LcPolyLine)
                {
                    this.endpoint = (Vector2)result1.ValueX;
                }
                else
                {
                    goto Step0;
                }
            }
            CreateElement(this.lcElement);
            goto End;
        End:
            this.EndCreating();

        }
        private void CreateElement(LcElement lcElement)
        {
            var doc = this.docRt.Document;
            DocumentManager.CurrentRecorder.BeginAction("Break");
            if (lcElement is LcLine)
            {
                LcLine lcLine = (LcLine)lcElement;
                //第一次选中直线线上的垂足点
                var pt1 = Line2d.GetFootofperpendicular(this.point, lcLine.Start, lcLine.End);
                //第二次点击屏幕与直线线上的垂足点
                var pt2 = Line2d.GetFootofperpendicular(this.endpoint, lcLine.Start, lcLine.End);
                var lcLineBreak = doc.CreateObject<LcLine>();
                lcLineBreak.Layer = lcLine.Layer;
                lcLineBreak.Color = lcLine.Color;
                lcLineBreak.LineType = lcLine.LineType;

                if (new Line2d(pt1, lcLine.Start).Length < new Line2d(pt2, lcLine.Start).Length)
                {
                    lcLineBreak.Start = pt2;
                    lcLineBreak.End = lcLine.End;
                    if (Line2d.PointInLine(lcLine.Start, lcLine.End, pt2))
                    {
                        doc.ModelSpace.InsertElement(lcLineBreak);
                    }
                    lcLine.End = pt1;
                }
                else if (new Line2d(pt1, lcLine.Start).Length > new Line2d(pt2, lcLine.Start).Length)
                {
                    lcLineBreak.Start = pt1;
                    lcLineBreak.End = lcLine.End;
                    if (Line2d.PointInLine(lcLine.Start, lcLine.End, pt2))
                    {
                        doc.ModelSpace.InsertElement(lcLineBreak);
                    }
                    lcLine.End = pt2;
                }
                else
                {
                    lcLineBreak.Start = pt2;
                    lcLineBreak.End = lcLine.End;
                    doc.ModelSpace.InsertElement(lcLineBreak);
                    lcLine.End = pt1;
                }

            }
            else if (lcElement is LcCircle)
            {
                LcCircle lcCircle = (LcCircle)lcElement;

                //var startAngle = (Math.Atan2(this.point.Y - center.Y, this.point.X - center.X) * 180 / Math.PI);
                //var endAngle = startAngle + angle;
                //if (endAngle > 360)
                //{
                //    endAngle -= 360;
                //}
                //startAngle = startAngle * Math.PI / 180;
                //endAngle = endAngle * Math.PI / 180;
                LcArc lcArc = doc.CreateObject<LcArc>();
                lcArc.Layer = lcCircle.Layer;
                lcArc.Color = lcCircle.Color;
                lcArc.LineType = lcCircle.LineType;
                lcArc.LineWeight = lcCircle.LineWeight;
                Arc2d arc2D = new Arc2d();
                arc2D = CreatePCE(this.endpoint, this.center, this.point);
                lcArc.Startp = this.endpoint;
                lcArc.Endp = this.point;
                lcArc.Center = arc2D.Center;
                lcArc.Radius = arc2D.Radius;
                lcArc.StartAngle = arc2D.StartAngle;
                lcArc.EndAngle = arc2D.EndAngle;
                lcArc.Midp = this.Getmidpoint(arc2D, this.endpoint);
                doc.ModelSpace.RemoveElement(lcElement);
                doc.ModelSpace.InsertElement(lcArc);
            }
            else if (lcElement is LcArc)
            {
                LcArc lcArc = (LcArc)lcElement;
                Arc2d arc2DByPoint = new Arc2d();
                //因为是逆时针画弧 所以StartAngle为结束点的角度 EndAngle为起点的角度
                arc2DByPoint = CreatePCE(lcArc.Startp, this.center, this.endpoint);
                Arc2d arc2DByEndPoint = new Arc2d();
                arc2DByEndPoint = CreatePCE(this.point, this.center, lcArc.Endp);
                //圆弧起点到Point的弧长
                double arcLengthByPoint = 0;
                //圆弧起点到EndPoint的弧长
                double arcLengthByEndPoint = 0;
                //圆弧起点到Endpoint的夹角
                double arcAndLineAngleByPoint = 0;
                //圆弧端点到Endpoint的夹角
                double arcAndLineAngleByEndPoint = 0;
                arcAndLineAngleByPoint = GetDegreesByTwoLine(lcArc.Startp, this.center, this.endpoint, this.center);
                arcAndLineAngleByEndPoint = GetDegreesByTwoLine(lcArc.Endp, this.center, this.endpoint, this.center);
                arcLengthByPoint = GetArcLength(arc2DByPoint.StartAngle, arc2DByPoint.EndAngle, arc2DByPoint.Radius);
                arcLengthByEndPoint = GetArcLength(arc2DByPoint.StartAngle, arc2DByEndPoint.StartAngle, arc2DByEndPoint.Radius);
                //判断打断第二次选中的点是否在圆弧上
                if (IsAngleBetween(lcArc.Startp, lcArc.Endp, lcArc.Midp))
                {
                    //判断Point和EndPoint哪个点离圆弧的起点更近
                    if (arcLengthByPoint < arcLengthByEndPoint)
                    {
                        LcArc newLcArc = doc.CreateObject<LcArc>();
                        newLcArc.Layer = lcArc.Layer;
                        newLcArc.Color = lcArc.Color;
                        newLcArc.LineType = lcArc.LineType;
                        newLcArc.LineWeight = lcArc.LineWeight;
                        ////StartAngle为结束点的角度 EndAngle为起点的角度
                        arc2DByPoint = CreatePCE(lcArc.Startp, this.center, this.endpoint);
                        newLcArc.Startp = lcArc.Startp;
                        newLcArc.Endp = this.endpoint;
                        newLcArc.Center = arc2DByPoint.Center;
                        newLcArc.Radius = arc2DByPoint.Radius;
                        newLcArc.StartAngle = arc2DByPoint.StartAngle;
                        newLcArc.EndAngle = arc2DByPoint.EndAngle;
                        newLcArc.Midp = this.Getmidpoint(arc2DByPoint, lcArc.Startp);

                        LcArc newLcArc2 = doc.CreateObject<LcArc>();
                        newLcArc2.Layer = lcArc.Layer;
                        newLcArc2.Color = lcArc.Color;
                        newLcArc2.LineType = lcArc.LineType;
                        newLcArc2.LineWeight = lcArc.LineWeight;
                        ////StartAngle为结束点的角度 EndAngle为起点的角度
                        arc2DByEndPoint = CreatePCE(this.point, this.center, lcArc.Endp);
                        newLcArc2.Startp = this.point;
                        newLcArc2.Endp = lcArc.Endp;
                        newLcArc2.Center = arc2DByEndPoint.Center;
                        newLcArc2.Radius = arc2DByEndPoint.Radius;
                        newLcArc2.StartAngle = arc2DByEndPoint.StartAngle;
                        newLcArc2.EndAngle = arc2DByEndPoint.EndAngle;
                        newLcArc2.Midp = this.Getmidpoint(arc2DByEndPoint, this.point);

                        doc.ModelSpace.InsertElement(newLcArc);
                        doc.ModelSpace.InsertElement(newLcArc2);
                    }
                    else
                    {
                        LcArc newLcArc = doc.CreateObject<LcArc>();
                        newLcArc.Layer = lcArc.Layer;
                        newLcArc.Color = lcArc.Color;
                        newLcArc.LineType = lcArc.LineType;
                        newLcArc.LineWeight = lcArc.LineWeight;
                        //StartAngle为结束点的角度 EndAngle为起点的角度
                        arc2DByPoint = CreatePCE(lcArc.Startp, this.center, this.point);
                        newLcArc.Startp = lcArc.Startp;
                        newLcArc.Endp = this.point;
                        newLcArc.Center = arc2DByPoint.Center;
                        newLcArc.Radius = arc2DByPoint.Radius;
                        newLcArc.StartAngle = arc2DByPoint.StartAngle;
                        newLcArc.EndAngle = arc2DByPoint.EndAngle;
                        newLcArc.Midp = this.Getmidpoint(arc2DByPoint, lcArc.Startp);

                        LcArc newLcArc2 = doc.CreateObject<LcArc>();
                        newLcArc2.Layer = lcArc.Layer;
                        newLcArc2.Color = lcArc.Color;
                        newLcArc2.LineType = lcArc.LineType;
                        newLcArc2.LineWeight = lcArc.LineWeight;
                        //StartAngle为结束点的角度 EndAngle为起点的角度
                        arc2DByEndPoint = CreatePCE(this.endpoint, this.center, lcArc.Endp);
                        newLcArc2.Startp = this.endpoint;
                        newLcArc2.Endp = lcArc.Endp;
                        newLcArc2.Center = arc2DByEndPoint.Center;
                        newLcArc2.Radius = arc2DByEndPoint.Radius;
                        newLcArc2.StartAngle = arc2DByEndPoint.StartAngle;
                        newLcArc2.EndAngle = arc2DByEndPoint.EndAngle;
                        newLcArc2.Midp = this.Getmidpoint(arc2DByEndPoint, this.endpoint);

                        doc.ModelSpace.InsertElement(newLcArc);
                        doc.ModelSpace.InsertElement(newLcArc2);
                    }

                }
                else
                {
                    //判断Point和EndPoint哪个点离圆弧的起点更近
                    if (arcAndLineAngleByPoint < arcAndLineAngleByEndPoint)
                    {
                        LcArc newLcArc = doc.CreateObject<LcArc>();
                        newLcArc.Layer = lcArc.Layer;
                        newLcArc.Color = lcArc.Color;
                        newLcArc.LineType = lcArc.LineType;
                        newLcArc.LineWeight = lcArc.LineWeight;
                        //StartAngle为结束点的角度 EndAngle为起点的角度
                        arc2DByPoint = CreatePCE(this.point, this.center, lcArc.Endp);
                        newLcArc.Startp = this.point;
                        newLcArc.Endp = lcArc.Endp;
                        newLcArc.Center = arc2DByPoint.Center;
                        newLcArc.Radius = arc2DByPoint.Radius;
                        newLcArc.StartAngle = arc2DByPoint.StartAngle;
                        newLcArc.EndAngle = arc2DByPoint.EndAngle;
                        newLcArc.Midp = this.Getmidpoint(arc2DByPoint, this.point);
                        doc.ModelSpace.InsertElement(newLcArc);
                    }
                    else
                    {
                        LcArc newLcArc = doc.CreateObject<LcArc>();
                        newLcArc.Layer = lcArc.Layer;
                        newLcArc.Color = lcArc.Color;
                        newLcArc.LineType = lcArc.LineType;
                        newLcArc.LineWeight = lcArc.LineWeight;
                        arc2DByPoint = CreatePCE(lcArc.Startp, this.center, this.point);
                        newLcArc.Startp = lcArc.Startp;
                        newLcArc.Endp = this.point;
                        newLcArc.Center = arc2DByPoint.Center;
                        newLcArc.Radius = arc2DByPoint.Radius;
                        newLcArc.StartAngle = arc2DByPoint.StartAngle;
                        newLcArc.EndAngle = arc2DByPoint.EndAngle;
                        newLcArc.Midp = this.Getmidpoint(arc2DByPoint, lcArc.Startp);
                        doc.ModelSpace.InsertElement(newLcArc);
                    }
                }
                doc.ModelSpace.RemoveElement(lcElement);


            }
            else if (lcElement is LcRay)
            {
                LcRay lcRay = (LcRay)lcElement;
                var box = this.vportRt.Renderer.GetWcsClipBox();
                var interPs = LcGeoUtils.IntersectRayBox(lcRay, box);
                var interP = interPs.OrderByDescending(P => Vector2.Distance(lcRay.StartPoint, P)).FirstOrDefault();

                //第一次选中直线线上的垂足点
                var pt1 = Line2d.GetFootofperpendicular(this.point, lcRay.StartPoint, interP);
                //第二次点击屏幕与直线线上的垂足点
                var pt2 = Line2d.GetFootofperpendicular(this.endpoint, lcRay.StartPoint, interP);
                var lcLineBreak = doc.CreateObject<LcLine>();
                lcLineBreak.Layer = lcRay.Layer;
                lcLineBreak.Color = lcRay.Color;
                lcLineBreak.LineType = lcRay.LineType;
                //如果第一个垂足点到射线起点的距离小于第二个垂足点到射线起点的距离 直线的开始点就是射线的开始点，直线的结束点是第一个垂足点
                if (new Line2d(pt1, lcRay.StartPoint).Length < new Line2d(pt2, lcRay.StartPoint).Length)
                {
                    lcLineBreak.Start = lcRay.StartPoint;
                    lcLineBreak.End = pt1;
                    if (Line2d.PointInLine(lcRay.StartPoint, interP, pt2))
                    {
                        doc.ModelSpace.InsertElement(lcLineBreak);
                    }
                    lcRay.StartPoint = pt2;
                }
                else if (new Line2d(pt1, lcRay.StartPoint).Length > new Line2d(pt2, lcRay.StartPoint).Length)
                {
                    lcLineBreak.End = lcRay.StartPoint;
                    lcLineBreak.Start = pt2;
                    if (Line2d.PointInLine(lcRay.StartPoint, interP, pt2))
                    {
                        doc.ModelSpace.InsertElement(lcLineBreak);
                    }
                    lcRay.StartPoint = pt1;
                }
                else
                {
                    lcLineBreak.Start = lcRay.StartPoint;
                    lcLineBreak.End = pt1;
                    doc.ModelSpace.InsertElement(lcLineBreak);
                    lcRay.StartPoint = pt2;
                }
            }
            else if (lcElement is LcXLine)
            {
                LcXLine lcXLine = (LcXLine)lcElement;
                var box = this.vportRt.Renderer.GetWcsClipBox();
                var interPs = LcGeoUtils.IntersectXLineBox(lcXLine, box);
                //var interP = interPs.OrderByDescending(P => Vector2.Distance(lcXLine.StartPoint, P)).FirstOrDefault();
                var mstart = interPs[0];
                var mend = interPs[1];
                //第一次选中直线线上的垂足点
                var pt1 = Line2d.GetFootofperpendicular(this.point, mstart, mend);
                //第二次点击屏幕与直线线上的垂足点
                var pt2 = Line2d.GetFootofperpendicular(this.endpoint, mstart, mend);
                //var lcRay1 = doc.CreateObject<LcRay>();
                //var lcRay2 = doc.CreateObject<LcRay>();
                //lcRay1.Layer = lcXLine.Layer;
                //lcRay1.Color = lcXLine.Color;
                //lcRay1.LineType = lcXLine.LineType;

                //lcRay2.Layer = lcXLine.Layer;
                //lcRay2.Color = lcXLine.Color;
                //lcRay2.LineType = lcXLine.LineType;
                if (new Line2d(pt1, mend).Length < new Line2d(pt2, mend).Length)
                {
                    doc.ModelSpace.AddRay(pt1, mend);
                    doc.ModelSpace.AddRay(pt2, mstart);
                    //lcRay1.StartPoint = pt1;
                    //lcRay1.Direction = mend;
                    //lcRay2.StartPoint = pt2;
                    //lcRay2.Direction = mstart;
                }
                else
                {
                    doc.ModelSpace.AddRay(pt1, mstart);
                    doc.ModelSpace.AddRay(pt2, mend);
                    //lcRay1.StartPoint = pt1;
                    //lcRay1.Direction = mstart;
                    //lcRay2.StartPoint = pt2;
                    //lcRay2.Direction = mend;
                }
                //doc.ModelSpace.AddRay(lcRay1.StartPoint,lcRay1.Direction);
                //doc.ModelSpace.AddRay(lcRay2.StartPoint,lcRay2.Direction);
                doc.ModelSpace.RemoveElement(lcElement);
                //如果第一个垂足点到射线起点的距离小于第二个垂足点到射线起点的距离 直线的开始点就是射线的开始点，直线的结束点是第一个垂足点
                //if (new Line2d(pt1, lcXLine.StartPoint).Length < new Line2d(pt2, lcXLine.StartPoint).Length)
                //{
                //    lcLineBreak.Start = lcXLine.StartPoint;
                //    lcLineBreak.End = pt1;
                //    if (Line2d.PointInLine(lcXLine.StartPoint, interP, pt2))
                //    {
                //        doc.ModelSpace.InsertElement(lcLineBreak);
                //    }
                //    lcXLine.StartPoint = pt2;
                //}
                //else if (new Line2d(pt1, lcXLine.StartPoint).Length > new Line2d(pt2, lcXLine.StartPoint).Length)
                //{
                //    lcLineBreak.End = lcXLine.StartPoint;
                //    lcLineBreak.Start = pt2;
                //    if (Line2d.PointInLine(lcXLine.StartPoint, interP, pt2))
                //    {
                //        doc.ModelSpace.InsertElement(lcLineBreak);
                //    }
                //    lcXLine.StartPoint = pt1;
                //}
                //else
                //{
                //    lcLineBreak.Start = lcXLine.StartPoint;
                //    lcLineBreak.End = pt1;
                //    doc.ModelSpace.InsertElement(lcLineBreak);
                //    lcXLine.StartPoint = pt2;
                //}
            }
            else if (lcElement is LcPolyLine)
            {
                LcPolyLine lcPolyLine = (LcPolyLine)lcElement;
                Dictionary<double, Curve2d> dicPlineSegment1 = new Dictionary<double, Curve2d>();
                Dictionary<double, Curve2d> dicPlineSegment2 = new Dictionary<double, Curve2d>();
                foreach (Curve2d polyLine in lcPolyLine.Curve2ds)
                {
                    if (polyLine.Type == Curve2dType.Line2d)
                    {
                        var lcpolyline1 = lcPolyLine.Curve2ds[0] as Line2d;
                        LcLine lcLine = new LcLine();
                        lcLine.Start = lcpolyline1.Start;
                        lcLine.End = lcpolyline1.End;
                        var pt1 = Line2d.GetFootofperpendicular(this.point, lcLine.Start, lcLine.End);
                        var pt2 = Line2d.GetFootofperpendicular(this.endpoint, lcLine.Start, lcLine.End);
                        if (Line2d.PointInLine(lcLine.Start, lcLine.End, pt1))
                        {
                            double lineLength = new Line2d(pt1, this.point).Length;
                            if (!dicPlineSegment1.ContainsKey(lineLength))
                                dicPlineSegment1.Add(lineLength, polyLine);
                            dicPlineSegment1[lineLength] = polyLine;
                        }
                        if (Line2d.PointInLine(lcLine.Start, lcLine.End, pt2))
                        {
                            double lineLength = new Line2d(pt2, this.endpoint).Length;
                            if (!dicPlineSegment2.ContainsKey(lineLength))
                                dicPlineSegment2.Add(lineLength, polyLine);
                            dicPlineSegment2[lineLength] = polyLine;
                        }
                    }
                    else
                    {

                    }
                }
                var PolyLine = doc.CreateObject<LcPolyLine>();
                PolyLine.Curve2ds = new List<Curve2d>();

                //var PolyLine2 = doc.CreateObject<LcPolyLine>();
                dicPlineSegment1 = dicPlineSegment1.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);

                dicPlineSegment2 = dicPlineSegment2.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
                Curve2d polyLine1 = dicPlineSegment1.First().Value;
                PolyLine.Curve2ds.Add(polyLine1);
                lcPolyLine.Curve2ds.Remove(polyLine1);
                var pt11 = Line2d.GetFootofperpendicular(this.point, (polyLine1 as Line2d).Start, (polyLine1 as Line2d).End);
                (polyLine1 as Line2d).End = pt11;
                Curve2d polyLine2 = dicPlineSegment2.First().Value;
                var pt22 = Line2d.GetFootofperpendicular(this.endpoint, (polyLine2 as Line2d).Start, (polyLine2 as Line2d).End);
                (polyLine2 as Line2d).Start = pt22;
                doc.ModelSpace.InsertElement(PolyLine);
                //foreach (PlineSegment polyLine in dicPlineSegment2.First().Value)
                //{

                //    polyLine.Start = pt2;
                //}
                //第一次选中直线线上的垂足点
                //var pt1 = Line2d.GetFootofperpendicular(this.point, lcPolyLine.Start, lcPolyLine.End);
                //foreach (var item in lcPolyLine)
                //{

                //}
                ////第二次点击屏幕与直线线上的垂足点
                //var pt2 = Line2d.GetFootofperpendicular(this.endpoint, lcLine.Start, lcLine.End);
                //var lcLineBreak = doc.CreateObject<LcLine>();
                //lcLineBreak.Layer = lcLine.Layer;
                //lcLineBreak.Color = lcLine.Color;
                //lcLineBreak.LineType = lcLine.LineType;
            }
            this.docRt.Action.ClearSelects();
            DocumentManager.CurrentRecorder.EndAction();
        }


        /// <summary>
        /// 选中的对象
        /// </summary>
        /// <param name="elements"></param>
        /// <returns></returns>
        private bool CreateSelects(LcElement elements)
        {


            if (elements is LcLine || elements is LcCircle || elements is LcArc || elements is LcPolyLine || elements is LcRay || elements is LcXLine)
            {
                return true;

            }

            return false;

        }

        public override void Cancel()
        {
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
        }



        //public override void CreateElement(LcElement element, Matrix3 matrix)
        //{

        //}
        //public override void CreateElement(LcElement element, Vector2 basePoint, double scaleFactor)
        //{

        //}
        //public override void CreateElement(LcElement element, Vector2 basePoint, Vector2 scaleVector)
        //{

        //}
        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {


        }



        public override void DrawAuxLines(SKCanvas canvas)
        {



        }

        private void DrawAuxLine(SKCanvas canvas, Vector2 p0, Vector2 p1)
        {
            var sk_pre = this.vportRt.ConvertWcsToScr(p0).ToSKPoint();
            var sk_p = this.vportRt.ConvertWcsToScr(p1).ToSKPoint();
            //辅助元素的颜色 
            canvas.DrawLine(sk_pre, sk_p, new SKPaint { Color = this.vportRt.GetAuxColorValue(), IsStroke = true });
            //辅助曲线的颜色，包括辅助长度，辅助角度等
        }
        #region Grip
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            return new List<ControlGrip>().ToArray();
        }


        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
        }


        public override void DrawDragGrip(SKCanvas canvas)
        {

        }

        #endregion
        public override List<PropertyObserver> GetPropertyObservers()
        {
            return new List<PropertyObserver>();

        }
        /// <summary>
        /// 计算圆弧中点
        /// </summary>
        /// <param name="arc2D"></param>arc对象
        /// <param name="matrixPoint"></param>起点
        /// <returns></returns>
        public Vector2 Getmidpoint(Arc2d arc2D, Vector2 matrixPoint)
        {

            var sage = arc2D.StartAngle * 180 / Math.PI;
            var eage = arc2D.EndAngle * 180 / Math.PI;
            double moveAngle = eage - sage;
            if (moveAngle < 0)
            {
                moveAngle += 360;
            }
            double midAngle = (moveAngle / 2);  //需要旋转的角度
            var matrix3 = Matrix3.Rotate(midAngle, center); //围绕圆心旋转
            var mp = matrix3.MultiplyPoint(matrixPoint); //旋转矩阵是逆时针旋转
            return new Vector2(mp.X, mp.Y);
        }

        /// <summary>
        /// 获取Arc2d的起点角度和端点角度
        /// </summary>
        /// <param name="sp"></param>起点
        /// <param name="cp"></param>圆心
        /// <param name="ep"></param>端点
        /// <returns></returns>
        public Arc2d CreatePCE(Vector2 sp, Vector2 cp, Vector2 ep)
        {
            Arc2d arc2D = new Arc2d();
            arc2D.Center = cp;
            arc2D.StartAngle = (Math.Atan2(sp.Y - cp.Y, sp.X - cp.X) * 180 / Math.PI);
            arc2D.EndAngle = (Math.Atan2(ep.Y - cp.Y, ep.X - cp.X) * 180 / Math.PI);
            arc2D.Radius = Vector2.Distance(center, sp);
            if (arc2D.StartAngle < 0)
            {
                arc2D.StartAngle += 360;
            }
            if (arc2D.EndAngle < 0)
            {
                arc2D.EndAngle += 360;
            }
            arc2D.StartAngle = arc2D.StartAngle / 180 * Math.PI;
            arc2D.EndAngle = arc2D.EndAngle / 180 * Math.PI;
            return arc2D;
        }
        /// <summary>
        /// 获取圆弧的弧长
        /// </summary>
        /// <param name="starAngle"></param>
        /// <param name="endAngle"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public double GetArcLength(double starAngle, double endAngle, double radius)
        {
            double sumAngle;
            double startangle = starAngle * 180 / Math.PI;
            double endangle = endAngle * 180 / Math.PI;
            sumAngle = endangle - startangle;
            if (sumAngle < 0)
            {
                sumAngle += 360;
            }
            sumAngle = sumAngle * (Math.PI / 180.0);
            return radius * sumAngle;
        }
        /// <summary>
        /// 是否在角度内
        /// </summary>
        /// <param name="arcStartPoint"></param>圆弧的起点
        /// <param name="arcEndPoint"></param>圆弧的端点
        /// <param name="arcMidPoint"></param>圆弧的中点
        /// <returns></returns>
        public bool IsAngleBetween(Vector2 arcStartPoint, Vector2 arcEndPoint, Vector2 arcMidPoint)
        {
            Line2d line1 = new Line2d(arcStartPoint, arcMidPoint);
            Line2d line2 = new Line2d(arcEndPoint, arcMidPoint);
            Line2d line3 = new Line2d(this.center, this.endpoint);
            return (Line2d.CrossLine(line1, line3) || Line2d.CrossLine(line2, line3));
        }
        /// <summary>
        /// 计算两条线的夹角
        /// </summary>
        /// <param name="line1start"></param>
        /// <param name="line1End"></param>
        /// <param name="line2start"></param>
        /// <param name="line2End"></param>
        /// <returns></returns>
        public double GetDegreesByTwoLine(Vector2 line1start, Vector2 line1End, Vector2 line2start, Vector2 line2End)
        {
            double x1 = line1start.X;
            double y1 = line1start.Y;
            double x2 = line1End.X;
            double y2 = line1End.Y;
            double x3 = line2start.X;
            double y3 = line2start.Y;
            double x4 = line2End.X;
            double y4 = line2End.Y;
            // 计算线段的向量表示
            double v1x = x2 - x1;
            double v1y = y2 - y1;
            double v2x = x4 - x3;
            double v2y = y4 - y3;
            // 计算向量的内积
            double dotProduct = v1x * v2x + v1y * v2y;
            // 计算向量的长度
            double magnitudeV1 = Math.Sqrt(v1x * v1x + v1y * v1y);
            double magnitudeV2 = Math.Sqrt(v2x * v2x + v2y * v2y);
            // 计算夹角余弦值
            double cosine = dotProduct / (magnitudeV1 * magnitudeV2);
            // 将夹角余弦值转换为角度
            double angleRadians = Math.Acos(cosine);
            double angleDegrees = angleRadians * 180 / Math.PI;
            return angleDegrees;
        }
    }
}
