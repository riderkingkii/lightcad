﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Core.Filters;
using LightCAD.Runtime;
using netDxf.Entities;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.ConstrainedExecution;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using static LightCAD.Core.Elements.LcPolyLine;
using LightCAD.MathLib;
using System.Reflection.Metadata;
using Avalonia.Controls;
using static LightCAD.Three.WebGLMorphtargets;
using System.Diagnostics.Metrics;
using Avalonia.Controls.Shapes;
using Avalonia.Utilities;
using System.Net;
using Avalonia;
using Avalonia.Styling;

namespace LightCAD.Drawing.Actions
{
    public class ArcAction : Curve2dAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;
        private Vector2 startPoint;
        private Vector2 endPoint;
        private Vector2 midPoint;
        private Vector2 center;
        private Vector2 radius;
        private Vector2 length;
        private string _methodName;

        private DrawArcType drawArcType;
        public enum DrawArcType
        {
            ArcCenter = 0,
            ArcThreePoint = 1,   //三个点
        }
        static ArcAction()
        {
            CreateMethods = new LcCreateMethod[10];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "CreateARC",
                Description = "三点创建弧线",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的起点或 [圆心(C)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的第二个点或 [圆心(C)/端点(E)]:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定圆弧的端点:" },
                }
            };
            CreateMethods[1] = new LcCreateMethod()
            {
                Name = "PCE",
                Description = "起点,圆心,端点",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的起点或 [圆心(C)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的圆心:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定圆弧的端点(按住 Ctrl 键以切换方向)或 [角度(A)/弦长(L)]:" },
                }
            };
            CreateMethods[2] = new LcCreateMethod()
            {
                Name = "PCA",
                Description = "起点,圆心,角度",
                Steps = new LcCreateStep[]
              {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的起点或 [圆心(C)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的圆心:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定夹角(按住 Ctrl 键以切换方向):" },
              }
            };
            CreateMethods[3] = new LcCreateMethod()
            {
                Name = "PCL",
                Description = "起点,圆心,长度",
                Steps = new LcCreateStep[]
           {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的起点或 [圆心(C)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的圆心:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定弦长(按住 Ctrl 键以切换方向):" },
           }
            };
            CreateMethods[4] = new LcCreateMethod()
            {
                Name = "PEA",
                Description = "起点,端点,角度",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的起点或 [圆心(C)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的端点:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定夹角(按住 Ctrl 键以切换方向):" },
                }
            };
            CreateMethods[5] = new LcCreateMethod()
            {
                Name = "PED",
                Description = "起点,端点,方向",
                Steps = new LcCreateStep[]
               {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的起点或 [圆心(C)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的端点:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定圆弧起点的相切方向(按住 Ctrl 键以切换方向):" },
               }
            };
            CreateMethods[6] = new LcCreateMethod()
            {
                Name = "PER",
                Description = "起点,端点,半径",
                Steps = new LcCreateStep[]
            {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的起点或 [圆心(C)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的端点:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定圆弧的半径(按住 Ctrl 键以切换方向):" },
            }
            };
            CreateMethods[7] = new LcCreateMethod()
            {
                Name = "CPE",
                Description = "圆心,起点,端点",
                Steps = new LcCreateStep[]
              {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的圆心:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的起点:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定圆弧的端点(按住 Ctrl 键以切换方向)或 [角度(A)/弦长(L)]:" },
              }
            };
            CreateMethods[8] = new LcCreateMethod()
            {
                Name = "CPA",
                Description = "圆心,起点,角度",
                Steps = new LcCreateStep[]
              {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的圆心:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的起点:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定夹角(按住 Ctrl 键以切换方向):" },
              }
            };
            CreateMethods[9] = new LcCreateMethod()
            {
                Name = "CPL",
                Description = "圆心,起点,长度",
                Steps = new LcCreateStep[]
              {
                    new LcCreateStep { Name=  "Step0", Options= "ARC指定圆弧的圆心:" },
                    new LcCreateStep { Name=  "Step1", Options= "ARC指定圆弧的起点:" },
                    new LcCreateStep { Name=  "Step2", Options= "ARC指定弦长(按住 Ctrl 键以切换方向):" },
              }
            };
        }
        internal static void Initilize()
        {
            ElementActions.Arc = new ArcAction();
            LcDocument.ElementActions.Add(BuiltinElementType.Arc, ElementActions.Arc);
        }
        private InElementsInputer InElementsInputer { get; set; }
        private ElementSetInputer ElementInputers { get; set; }
        private ElementInputer ElementInputer { get; set; }
        private CmdTextInputer CmdTextInputer { get; set; }
        private PointInputer PointInputer { get; set; }
        private ArcAction() { }
        public ArcAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：ARC");
        }
        private LcCreateMethod GetMethod(string method)
        {
            if (method == null) return CreateMethods[0];
            var getted = CreateMethods.FirstOrDefault((m) => m.Name == method);
            if (getted == null)
                return CreateMethods[0];
            else
                return getted;
        }

        public async void ExecCreate(string[] args = null)
        {
            var method = "CreateARC";
            if (args != null && args.Length > 0)
            {
                method = args[0];
            }
            var curMethod = this.GetMethod(method);
            _methodName = curMethod.Name;
            this.PointInputer = new PointInputer(this.docEditor);
            this.StartCreating();
            if (curMethod.Name == "PCE") goto Method_PCE_Step0;
            if (curMethod.Name == "PCA") goto Method_PCA_Step0;
            if (curMethod.Name == "PCL") goto Method_PCL_Step0;
            if (curMethod.Name == "PEA") goto Method_PEA_Step0;
            if (curMethod.Name == "PED") goto Method_PED_Step0;
            if (curMethod.Name == "PER") goto Method_PER_Step0;
            if (curMethod.Name == "CPE") goto Method_CPE_Step0;
            if (curMethod.Name == "CPA") goto Method_CPA_Step0;
            if (curMethod.Name == "CPL") goto Method_CPL_Step0;
            #region  CreateARC
            Method_CreateARC_Step0:

            curMethod = this.SetCurMethod(CreateMethods, 0);
            var step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("CreateARC");
            _methodName = curMethod.Name;

            var result0 = await PointInputer.Execute(step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result0.ValueX == null)
            {
                if (result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                    if (result0.Option.ToUpper() == "C")
                    {
                        goto Method_CPE_Step0;
                    }
                }
                else
                    goto Method_CreateARC_Step1;
            }
            else
            {
                this.startPoint = (Vector2)result0.ValueX;
                goto Method_CreateARC_Step1;
            }

        //ARC指定圆弧的第二个点或 [圆心(C)/端点(E)]: 
        Method_CreateARC_Step1:
            this.drawArcType = DrawArcType.ArcThreePoint;  //三点画弧
            var step1 = this.SetCurStep(curMethod, 1);
            var result1 = await PointInputer.Execute(step1.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                return;
            }
            if (result1.ValueX == null)
            {
                if (result1.Option != null)
                {
                    //goto Step1;
                    if (result1.Option.ToUpper() == "C")
                    {
                        goto Method_PCE_Step1;
                    }
                    if (result1.Option.ToUpper() == "E")
                    {
                    }
                }

                else
                    goto Method_CreateARC_Step2;
            }
            else
            {
                this.midPoint = (Vector2)result1.ValueX;
                goto Method_CreateARC_Step2;
            }
        //    ARC指定圆弧的端点:
        Method_CreateARC_Step2:
            var step2 = this.SetCurStep(curMethod, 2);
            var result2 = await PointInputer.Execute(step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (result2.ValueX == null)
            {
            }
            else
            {
                this.endPoint = (Vector2)result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion

        #region   PCE起点,圆心,端点
        Method_PCE_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 1);
            var pce_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("PCE");
            _methodName = curMethod.Name;
            var pce_result0 = await PointInputer.Execute(pce_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (pce_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (pce_result0.ValueX == null)
            {
                if (pce_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                    if (pce_result0.Option == "C")
                    {
                        goto Method_CPE_Step0;
                    }
                }
                else
                    goto Method_PCE_Step1;
            }
            else
            {
                this.startPoint = (Vector2)pce_result0.ValueX;
                goto Method_PCE_Step1;
            }
        Method_PCE_Step1:
            var pce_step1 = this.SetCurStep(curMethod, 1);
            var pce_result1 = await PointInputer.Execute(pce_step1.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pce_result1.ValueX == null) { }
            else
            {
                this.center = (Vector2)pce_result1.ValueX;
                goto Method_PCE_Step2;
            }
        Method_PCE_Step2:
            var pce_step2 = this.SetCurStep(curMethod, 2);
            var pce_result2 = await PointInputer.Execute(pce_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pce_result2.ValueX == null)
            {
            }
            else
            {
                this.endPoint = (Vector2)pce_result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion
        #region  PCA 起点，圆心，角度
        Method_PCA_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 2);
            var pca_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("PCA");
            _methodName = curMethod.Name;
            var pca_result0 = await PointInputer.Execute(pca_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (pca_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (pca_result0.ValueX == null)
            {
                if (pca_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                    if (pca_result0.Option == "C")
                    {
                        goto Method_CPE_Step0;
                    }
                }
                else
                    goto Method_PCA_Step1;
            }
            else
            {
                this.startPoint = (Vector2)pca_result0.ValueX;
                goto Method_PCA_Step1;
            }
        Method_PCA_Step1:
            var pca_step1 = this.SetCurStep(curMethod, 1);
            var pca_result1 = await PointInputer.Execute(pca_step1.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pca_result1.ValueX == null) { }
            else
            {
                this.center = (Vector2)pca_result1.ValueX;

                goto Method_PCA_Step2;
            }
        Method_PCA_Step2:
            var pca_step2 = this.SetCurStep(curMethod, 2);
            var pca_result2 = await PointInputer.Execute(pca_step2.Options);
            double startangle, endangle;
            double angle;
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pca_result2.ValueX == null)
            {
                //如果输入的是角度，应该如何画圆弧
            }
            else
            {
                this.endPoint = (Vector2)pca_result2.ValueX;
                CreateArc();
                goto End;

            }
        #endregion
        #region PCL起点，圆心，长度
        Method_PCL_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 3);
            var pcl_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("PCL");
            _methodName = curMethod.Name;
            var pcl_result0 = await PointInputer.Execute(pcl_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (pcl_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (pcl_result0.ValueX == null)
            {
                if (pcl_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                    if (pcl_result0.Option == "C")
                    {
                        goto Method_CPE_Step0;
                    }
                }
                else
                    goto Method_PCL_Step1;
            }
            else
            {
                this.startPoint = (Vector2)pcl_result0.ValueX;
                goto Method_PCL_Step1;
            }
        Method_PCL_Step1:
            var pcl_step1 = this.SetCurStep(curMethod, 1);
            var pcl_result1 = await PointInputer.Execute(pcl_step1.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pcl_result1.ValueX == null) { }
            else
            {
                this.center = (Vector2)pcl_result1.ValueX;
                goto Method_PCL_Step2;
            }
        Method_PCL_Step2:
            var pcl_step2 = this.SetCurStep(curMethod, 2);
            var pcl_result2 = await PointInputer.Execute(pcl_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pcl_result2.ValueX == null)
            {
                //如果输入的是角度，应该如何画圆弧
            }
            else
            {
                this.endPoint = (Vector2)pcl_result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion
        #region  PEA 起点，端点，角度
        Method_PEA_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 4);
            var pea_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("PEA");
            _methodName = curMethod.Name;
            var pea_result0 = await PointInputer.Execute(pea_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (pea_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (pea_result0.ValueX == null)
            {
                if (pea_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                    if (pea_result0.Option == "C")
                    {
                        goto Method_CPE_Step0;
                    }
                }
                else
                    goto Method_PEA_Step1;
            }
            else
            {
                this.startPoint = (Vector2)pea_result0.ValueX;
                goto Method_PEA_Step1;
            }
        Method_PEA_Step1:
            var pea_step1 = this.SetCurStep(curMethod, 1);
            var pea_result1 = await PointInputer.Execute(pea_step1.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pea_result1.ValueX == null)
            {

            }
            else
            {
                this.midPoint = (Vector2)pea_result1.ValueX;
                goto Method_PEA_Step2;
            }
        Method_PEA_Step2:
            var pea_step2 = this.SetCurStep(curMethod, 2);
            var pea_result2 = await PointInputer.Execute(pea_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (pea_result2.ValueX == null)
            {

            }
            else
            {
                this.endPoint = (Vector2)pea_result2.ValueX;

                CreateArc();
                goto End;
            }
        #endregion
        #region PED 起点，端点，方向
        Method_PED_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 5);
            var ped_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("PED");
            _methodName = curMethod.Name;
            var ped_result0 = await PointInputer.Execute(ped_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (ped_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (ped_result0.ValueX == null)
            {
                if (ped_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                    if (ped_result0.Option == "C")
                    {
                        goto Method_CPE_Step0;
                    }
                }
                else
                    goto Method_PED_Step1;
            }
            else
            {
                this.startPoint = (Vector2)ped_result0.ValueX;
                goto Method_PED_Step1;
            }
        Method_PED_Step1:
            var ped_step1 = this.SetCurStep(curMethod, 1);
            var ped_result1 = await PointInputer.Execute(ped_step1.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (ped_result1.ValueX == null)
            {

            }
            else
            {
                this.midPoint = (Vector2)ped_result1.ValueX;
                goto Method_PED_Step2;
            }
        Method_PED_Step2:
            var ped_step2 = this.SetCurStep(curMethod, 2);
            var ped_result2 = await PointInputer.Execute(ped_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (ped_result2.ValueX == null)
            {

            }
            else
            {
                this.endPoint = (Vector2)ped_result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion
        #region  PER 起点，端点，半径
        Method_PER_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 6);
            var per_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("PER");
            _methodName = curMethod.Name;
            var per_result0 = await PointInputer.Execute(per_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (per_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (per_result0.ValueX == null)
            {
                if (per_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                    if (per_result0.Option == "C")
                    {
                        goto Method_CPE_Step0;
                    }
                }
                else
                    goto Method_PED_Step1;
            }
            else
            {
                this.startPoint = (Vector2)per_result0.ValueX;
                goto Method_PER_Step1;
            }
        Method_PER_Step1:
            var per_step1 = this.SetCurStep(curMethod, 1);
            var per_result1 = await PointInputer.Execute(per_step1.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (per_result1.ValueX == null)
            {
            }
            else
            {
                this.midPoint = (Vector2)per_result1.ValueX;
                goto Method_PER_Step2;
            }
        Method_PER_Step2:
            var per_step2 = this.SetCurStep(curMethod, 2);
            var per_result2 = await PointInputer.Execute(per_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (per_result2.ValueX == null)
            {

            }
            else
            {
                this.endPoint = (Vector2)per_result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion
        #region   CPE 圆心，起点，端点
        Method_CPE_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 7);
            var cpe_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("CPE");
            _methodName = curMethod.Name;
            var cpe_result0 = await PointInputer.Execute(cpe_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (cpe_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (cpe_result0.ValueX == null)
            {
                if (cpe_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                    goto Method_CPE_Step1;
            }
            else
            {
                this.center = (Vector2)cpe_result0.ValueX;
                goto Method_CPE_Step1;
            }
        Method_CPE_Step1:
            var cpe_step1 = this.SetCurStep(curMethod, 1);
            curMethod = this.GetMethod("CPE");
            _methodName = curMethod.Name;
            var cpe_result1 = await PointInputer.Execute(cpe_step1.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (cpe_result1 == null)
            {
                this.Cancel();
                goto End;
            }
            if (cpe_result1.ValueX == null)
            {
                if (cpe_result1.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                    goto Method_CPE_Step2;
            }
            else
            {
                this.startPoint = (Vector2)cpe_result1.ValueX;
                goto Method_CPE_Step2;
            }
        Method_CPE_Step2:
            var cpe_step2 = this.SetCurStep(curMethod, 2);
            var cpe_result2 = await PointInputer.Execute(cpe_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (cpe_result2.ValueX == null)
            {
            }
            else
            {
                this.endPoint = (Vector2)cpe_result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion
        #region  CPA圆心，起点，角度
        Method_CPA_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 8);
            var cpa_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("CPA");
            _methodName = curMethod.Name;
            var cpa_result0 = await PointInputer.Execute(cpa_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (cpa_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (cpa_result0.ValueX == null)
            {
                if (cpa_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                    goto Method_CPA_Step1;
            }
            else
            {
                this.center = (Vector2)cpa_result0.ValueX;
                goto Method_CPA_Step1;
            }
        Method_CPA_Step1:
            var cpa_step1 = this.SetCurStep(curMethod, 1);
            curMethod = this.GetMethod("CPA");
            _methodName = curMethod.Name;
            var cpa_result1 = await PointInputer.Execute(cpa_step1.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (cpa_result1 == null)
            {
                this.Cancel();
                goto End;
            }
            if (cpa_result1.ValueX == null)
            {
                if (cpa_result1.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                    goto Method_CPA_Step2;
            }
            else
            {
                this.startPoint = (Vector2)cpa_result1.ValueX;
                goto Method_CPA_Step2;
            }
        Method_CPA_Step2:
            var cpa_step2 = this.SetCurStep(curMethod, 2);
            var cpa_result2 = await PointInputer.Execute(cpa_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (cpa_result2.ValueX == null)
            {
            }
            else
            {
                this.endPoint = (Vector2)cpa_result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion
        #region  CPL圆心，起点，长度
        Method_CPL_Step0:
            curMethod = this.SetCurMethod(CreateMethods, 9);
            var cpl_step0 = this.SetCurStep(curMethod, 0);
            curMethod = this.GetMethod("CPL");
            _methodName = curMethod.Name;
            var cpl_result0 = await PointInputer.Execute(cpl_step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (cpl_result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (cpl_result0.ValueX == null)
            {
                if (cpl_result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                    goto Method_CPL_Step1;
            }
            else
            {
                this.center = (Vector2)cpl_result0.ValueX;
                goto Method_CPL_Step1;
            }
        Method_CPL_Step1:
            var cpl_step1 = this.SetCurStep(curMethod, 1);
            var cpl_result1 = await PointInputer.Execute(cpl_step1.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (cpl_result1 == null)
            {
                this.Cancel();
                goto End;
            }
            if (cpl_result1.ValueX == null)
            {
                if (cpl_result1.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                    goto Method_CPL_Step2;
            }
            else
            {
                this.startPoint = (Vector2)cpl_result1.ValueX;
                goto Method_CPL_Step2;
            }
        Method_CPL_Step2:
            var cpl_step2 = this.SetCurStep(curMethod, 2);
            var cpl_result2 = await PointInputer.Execute(cpl_step2.Options);
            if (PointInputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (cpl_result2.ValueX == null)
            {
                //如果输入的是角度，应该如何画圆弧
            }
            else
            {
                this.endPoint = (Vector2)cpl_result2.ValueX;
                CreateArc();
                goto End;
            }
        #endregion
        //注释掉 by:Zcb 2023-07-21 21:42
        End:
            this.EndCreating();
        }
        /// <summary>
        /// GetARC被用于延伸里面的三点画弧
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="mp"></param>
        /// <param name="ep"></param>
        /// <param name="rf"></param>
        /// <returns></returns>
        public static Arc2d GetARC(Vector2 sp, Vector2 mp, Vector2 ep, out bool rf)
        {
            rf = false;
            Arc2d arc2D = new Arc2d();
            arc2D.Center = GeoUtils.GetCenter(sp, mp, ep);
            arc2D.Radius = Vector2.Distance(sp, arc2D.Center);
            double Degrees, smidAngle, emidAngle;
            arc2D.StartAngle = GeoUtils.GetDegreesByTwoLine(new(arc2D.Center.X, arc2D.Center.Y), new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y), new(arc2D.Center.X, arc2D.Center.Y), new(sp.X, sp.Y)); //计算圆弧的起始角度
            arc2D.EndAngle = GeoUtils.GetDegreesByTwoLine(new(arc2D.Center.X, arc2D.Center.Y), new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y), new(arc2D.Center.X, arc2D.Center.Y), new(ep.X, ep.Y));
            //  emidAngle = GetDegreesByTwoLine(new(arc2D.Center.X, arc2D.Center.Y), new(ep.X, ep.Y), new(arc2D.Center.X, arc2D.Center.Y), new(mp.X, mp.Y));
            if (GeoUtils.IsTopInLine(arc2D.Center, sp, new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y)) < 0)
            {
                arc2D.StartAngle = 360 - arc2D.StartAngle;
            }
            if (GeoUtils.IsTopInLine(arc2D.Center, ep, new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y)) < 0)
            {
                arc2D.EndAngle = 360 - arc2D.EndAngle;
            }
            var ms = GeoUtils.IsTopInLine(arc2D.Center, sp, mp);
            var me = GeoUtils.IsTopInLine(arc2D.Center, ep, mp);
            var sm = GeoUtils.IsTopInLine(arc2D.Center, ep, sp);
            bool zh = false;
            if ((ms == me && sm < 0) || (me > 0 && ms < 0))
            {
                zh = true;
            }

            if (zh)
            {
                arc2D.StartAngle = arc2D.StartAngle / 180 * Math.PI;
                arc2D.EndAngle = arc2D.EndAngle / 180 * Math.PI;
            }
            else
            {
                Degrees = arc2D.StartAngle;
                rf = true;
                arc2D.StartAngle = arc2D.EndAngle / 180 * Math.PI;
                arc2D.EndAngle = Degrees / 180 * Math.PI;
            }
            return arc2D;
        }
        private bool CreateArc()
        {
            Arc2d arc2D = new Arc2d();
            var doc = this.docRt.Document;
            DocumentManager.CurrentRecorder.BeginAction("arc");
            var lcArc = doc.CreateObject<LcArc>();
            if (_methodName == "CreateARC")
            {
                arc2D = Arc2d.CreateARC(this.startPoint, this.midPoint, this.endPoint);
            }
            if (_methodName == "PCE" || _methodName == "CPE")
            {
                arc2D = Arc2d.CreatePCE(this.startPoint, center, this.endPoint);
            }
            if (_methodName == "PCA" || _methodName == "CPA")
            {
                arc2D = Arc2d.CreatePCA(this.startPoint, center, this.endPoint);
            }
            if (_methodName == "PCL" || _methodName == "CPL")
            {
                arc2D = Arc2d.CreatePCL(this.startPoint, center, this.endPoint);
            }
            if (_methodName == "PEA")
            {
                arc2D = Arc2d.CreatePEA(this.startPoint, this.midPoint, this.endPoint);
            }
            if (_methodName == "PER")
            {
                arc2D = Arc2d.CreatePER(this.startPoint, this.midPoint, this.endPoint);
            }
            if (_methodName == "PED")
            {
                arc2D = Arc2d.CreatePED(this.startPoint, this.midPoint, this.endPoint);
            }
            lcArc.Curve = arc2D;
            doc.ModelSpace.InsertElement(lcArc);
            this.docRt.Action.ClearSelects();
            DocumentManager.CurrentRecorder.EndAction();
            return true;
        }
        public override void Cancel()
        {
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
            //TODO:
            this.EndCreating();
        }
        public override void DrawAuxLines(SKCanvas canvas)
        {
            var mp = this.vportRt.PointerMovedPosition.ToVector2d();
            var wcs_mp = this.vportRt.ConvertScrToWcs(mp);
            if (_methodName == "CreateARC")
            {
                if (this.startPoint != null && this.midPoint == null)
                {
                    DrawAuxLine(canvas, this.startPoint, wcs_mp);
                }
            }
            if (_methodName == "PCE" || _methodName == "PCA")
            {
                if (this.startPoint != null && this.center == null)
                {
                    DrawAuxLine(canvas, this.startPoint, wcs_mp);
                }
                if (this.startPoint != null && this.center != null)
                {
                    DrawAuxLine(canvas, this.center, wcs_mp);
                }
            }
            if (_methodName == "PCL")
            {
                if (this.startPoint != null && this.center == null)
                {
                    DrawAuxLine(canvas, this.startPoint, wcs_mp);
                }
                if (this.startPoint != null && this.center != null)
                {
                    DrawAuxLine(canvas, this.startPoint, wcs_mp);
                }
            }
            if (_methodName == "PEA")
            {
                if (this.startPoint != null && this.midPoint == null)
                {
                    DrawAuxLine(canvas, this.startPoint, wcs_mp);
                }
                if (this.startPoint != null && this.midPoint != null)
                {
                    DrawAuxLine(canvas, this.startPoint, wcs_mp);
                }

            }
            if (_methodName == "CPE" || _methodName == "CPA")
            {
                if (center != null && startPoint == null)
                {
                    DrawAuxLine(canvas, center, wcs_mp);
                }
                if (center != null && startPoint != null)
                {
                    DrawAuxLine(canvas, center, wcs_mp);
                }
            }
            if (_methodName == "CPL")
            {
                if (center != null && startPoint == null)
                {
                    DrawAuxLine(canvas, center, wcs_mp);
                }
                if (center != null && startPoint != null)
                {
                    DrawAuxLine(canvas, startPoint, wcs_mp);
                }
            }
            if (_methodName == "PER")
            {
                if (startPoint != null && midPoint == null)
                {
                    DrawAuxLine(canvas, startPoint, wcs_mp);
                }
                if (this.startPoint != null && this.midPoint != null)
                {
                    DrawAuxLine(canvas, this.midPoint, wcs_mp);
                }
            }
            if (_methodName == "PED")
            {
                if (startPoint != null && midPoint == null)
                {
                    DrawAuxLine(canvas, startPoint, wcs_mp);
                }
                if (this.startPoint != null && this.midPoint != null)
                {
                    DrawAuxLine(canvas, this.startPoint, wcs_mp);
                }
            }
        }
        public override void DrawTemp(SKCanvas canvas)
        {
            var mp = this.vportRt.PointerMovedPosition.ToVector2d();
            var wcs_mp = this.vportRt.ConvertScrToWcs(mp);
            if (_methodName == "CreateARC")
            {
                if (this.startPoint != null && this.midPoint != null)
                {
                    DrawAuxArc(canvas, wcs_mp);
                }
            }
            if (_methodName == "PCE" || _methodName == "PCA")
            {
                if (this.startPoint != null && this.center != null)
                {
                    DrawAuxArc(canvas, wcs_mp);
                }
            }
            if (_methodName == "PCL")
            {
                if (this.startPoint != null && this.center != null)
                {
                    DrawAuxArc(canvas, wcs_mp);
                }
            }
            if (_methodName == "PEA")
            {
                if (this.startPoint != null && this.midPoint != null)
                {
                    DrawAuxArc(canvas, wcs_mp);
                }
            }
            if (_methodName == "CPE" || _methodName == "CPA")
            {
                if (center != null && startPoint != null)
                {
                    DrawAuxArc(canvas, wcs_mp);
                }
            }
            if (_methodName == "CPL")
            {
                if (center != null && startPoint != null)
                {
                    DrawAuxArc(canvas, wcs_mp);
                }
            }
            if (_methodName == "PER" || _methodName == "PED")
            {
                if (midPoint != null && startPoint != null)
                {
                    DrawAuxArc(canvas, wcs_mp);
                }
            }
        }
        private void DrawAuxArc(SKCanvas canvas, Vector2 p0)
        {
            Matrix3 matrix = Matrix3.GetTranslate(new Vector2(0, 0));
            Arc2d arc2D = new Arc2d(); 
            if (_methodName == "CreateARC")
            {
                arc2D = Arc2d.CreateARC(this.startPoint, this.midPoint, p0);

            }
            if (_methodName == "PCE" || _methodName == "CPE")
            {
                arc2D = Arc2d.CreatePCE(this.startPoint, this.center, p0);
            }
            if (_methodName == "PCA" || _methodName == "CPA")
            {
                arc2D = Arc2d.CreatePCA(this.startPoint, this.center, p0);
            }
            if (_methodName == "PCL" || _methodName == "CPL")
            {
                arc2D = Arc2d.CreatePCL(this.startPoint, this.center, p0);
            }
            if (_methodName == "PEA")
            {
                arc2D = Arc2d.CreatePEA(this.startPoint, this.midPoint, p0);
            }
            if (_methodName == "PER")
            {
                arc2D = Arc2d.CreatePER(this.startPoint, this.midPoint, p0);
            }
            if (_methodName == "PED")
            {
                arc2D = Arc2d.CreatePED(this.startPoint, this.midPoint, p0);
            }
            this.vportRt.DrawArc(arc2D, matrix, canvas, new SKPaint { Color = this.vportRt.GetAuxColorValue(), IsStroke = true });
        }
        private void DrawAuxLine(SKCanvas canvas, Vector2 p0, Vector2 p1)
        {
            var sk_pre = this.vportRt.ConvertWcsToScr(p0).ToSKPoint();
            var sk_p = this.vportRt.ConvertWcsToScr(p1).ToSKPoint();
            //辅助元素的颜色 
            //canvas.DrawLine(sk_pre, sk_p, Constants.auxElementPen);
            canvas.DrawLine(sk_pre, sk_p, Constants.auxOrangeDashPen);
            //辅助曲线的颜色，包括辅助长度，辅助角度等
        }
        public override void CreateElement(LcElement element, Matrix3 matrix)
        {
            var grp = element as LcGroup;
            foreach (var ele in grp.Elements)
            {
                var eleAction = (ele.RtAction as ElementAction);
                eleAction.CreateElement(ele, matrix);
            }
        }
        public override void CreateElement(LcElement element, Vector2 basePoint, double scaleFactor)
        {
            var grp = element as LcGroup;
            foreach (var ele in grp.Elements)
            {
                var eleAction = (ele.RtAction as ElementAction);
                eleAction.CreateElement(ele, basePoint, scaleFactor);
            }
        }
        public override void CreateElement(LcElement element, Vector2 basePoint, Vector2 scaleVector)
        {
            var grp = element as LcGroup;
            foreach (var ele in grp.Elements)
            {
                var eleAction = (ele.RtAction as ElementAction);
                eleAction.CreateElement(ele, basePoint, scaleVector);
            }
        }
        public static Arc2d CreateArc3P(Vector2 sp, Vector2 mp, Vector2 ep, out bool reverse)
        {
            reverse = false;
            Arc2d arc2D = new Arc2d();
            arc2D.Center = GeoUtils.GetCenter(sp, mp, ep);
            arc2D.Radius = Vector2.Distance(sp, arc2D.Center);
            double Degrees, smidAngle, emidAngle;
            arc2D.StartAngle = GeoUtils.GetDegreesByTwoLine(new(arc2D.Center.X, arc2D.Center.Y), new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y), new(arc2D.Center.X, arc2D.Center.Y), new(sp.X, sp.Y)); //计算圆弧的起始角度
            arc2D.EndAngle = GeoUtils.GetDegreesByTwoLine(new(arc2D.Center.X, arc2D.Center.Y), new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y), new(arc2D.Center.X, arc2D.Center.Y), new(ep.X, ep.Y));  //计算圆弧的结束角度
            smidAngle = GeoUtils.GetDegreesByTwoLine(new(arc2D.Center.X, arc2D.Center.Y), new(sp.X, sp.Y), new(arc2D.Center.X, arc2D.Center.Y), new(mp.X, mp.Y));  //起始点到中点之间的角度
            emidAngle = GeoUtils.GetDegreesByTwoLine(new(arc2D.Center.X, arc2D.Center.Y), new(ep.X, ep.Y), new(arc2D.Center.X, arc2D.Center.Y), new(mp.X, mp.Y));
            if (GeoUtils.IsTopInLine(arc2D.Center, sp, new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y)) < 0)
            {
                arc2D.StartAngle = 360 - arc2D.StartAngle;
            }
            if (GeoUtils.IsTopInLine(arc2D.Center, ep, new(arc2D.Center.X + arc2D.Radius, arc2D.Center.Y)) < 0)
            {
                arc2D.EndAngle = 360 - arc2D.EndAngle;
            }
            double moveAngle = smidAngle + emidAngle;
            double xAngle = arc2D.EndAngle - arc2D.StartAngle;
            if (xAngle < 0)
            {
                xAngle += 360;
            }
            if (LcArc.IsAngleTrue(xAngle, moveAngle) == true)
            {
                arc2D.StartAngle = arc2D.StartAngle / 180 * Math.PI;
                arc2D.EndAngle = arc2D.EndAngle / 180 * Math.PI;
            }
            else
            {
                Degrees = arc2D.StartAngle;
                reverse = true;
                arc2D.StartAngle = arc2D.EndAngle / 180 * Math.PI;
                arc2D.EndAngle = Degrees / 180 * Math.PI;
            }
            return arc2D;
        }

        public static Vector2 Getmidpoint(double StartAngle, double EndAngle, Vector2 center, Vector2 startPoint)
        {
            var sage = StartAngle * 180 / Math.PI;
            var eage = EndAngle * 180 / Math.PI;
            double moveAngle = eage - sage;
            if (moveAngle < 0)
            {
                moveAngle += 360;
            }
            double midAngle = (moveAngle / 2);  //需要旋转的角度
            var matrix3 = Matrix3.Rotate(midAngle, center); //围绕圆心旋转
            var mp = matrix3.MultiplyPoint(startPoint); //旋转矩阵是逆时针旋转
            return new Vector2(mp.X, mp.Y);
        }
        public bool IsPlugAng(Vector2 line1start, Vector2 line1End, Vector2 line2start, Vector2 line2End)
        {
            double x1 = line1start.X;
            double y1 = line1start.Y;
            double x2 = line1End.X;
            double y2 = line1End.Y;
            double x3 = line2start.X;
            double y3 = line2start.Y;
            double x4 = line2End.X;
            double y4 = line2End.Y;
            if (x1 == x2)
            {
                if (x4 < x1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
                if (y1 == y2)
            {
                if (y4 > y1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                double xa = x2 - x1;
                double ya = y2 - y1;
                double xb = x4 - x3;
                double yb = y4 - y3;
                double angle = xa * yb - xb * ya;
                if (angle > 0)
                { return true; }
                else
                { return false; }
            }
        }
        public Vector2 GetVerticalpoint(Vector2 point, Vector2 center, Vector2 spoint, double ANG)
        {
            double x1 = point.X;
            double y1 = point.Y;
            double x2 = center.X;
            double y2 = center.Y;
            double x;
            double y;
            if (x1 == x2)
            {
                y = spoint.Y;
                x = x1;
            }
            else
            if (y1 == y2)
            {
                y = y1;
                x = spoint.X;
            }
            else
            {
                double k1 = (y2 - y1) / (x2 - x1);
                double b1 = y1 - k1 * x1;
                double k = -1 / k1;
                double b = spoint.Y - k * spoint.X;
                x = (b1 - b) / (k - k1);
                y = k * x + b;
            }

            if (ANG < 180)
            {
                Matrix3 matrix31 = Matrix3.GetMove(new Vector2(x, y), point);
                return matrix31.MultiplyPoint(spoint);
            }
            else
            {
                Matrix3 matrix31 = Matrix3.GetMove(spoint, point);
                return matrix31.MultiplyPoint(new Vector2(x, y));
            }

        }

        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
        }
        public override void Draw(SKCanvas canvas, LcElement element, Vector2 offset) //最终渲染的地方
        {
            Matrix3 matrix = Matrix3.GetTranslate(offset);
            var grp = element as LcArc;
            var pen = this.GetDrawPen(element);
            if(pen == Constants.defaultPen)
            {
                using(var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
                {
                    this.vportRt.DrawArc(grp.Arc, matrix, canvas, elePen);
                }
            }
            else
            {
                this.vportRt.DrawArc(grp.Arc, matrix, canvas, pen);
            }        

            //用于盒子判断，先不删除
            //this.vportRt.ActiveElementSet.AddLine(grp.GetBoundingBox().LeftBottom, grp.GetBoundingBox().LeftTop);
            //this.vportRt.ActiveElementSet.AddLine(grp.GetBoundingBox().LeftTop, grp.GetBoundingBox().RightTop);
            //this.vportRt.ActiveElementSet.AddLine(grp.GetBoundingBox().RightTop, grp.GetBoundingBox().RightBottom);
            //this.vportRt.ActiveElementSet.AddLine(grp.GetBoundingBox().RightBottom, grp.GetBoundingBox().LeftBottom);
        }
        /// <summary>
        /// 拖拽点的设置
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var group = element as LcArc;
            var grips = new List<ControlGrip>();

            var gripCenter = new ControlGrip
            {
                Element = group,
                Name = "Center",
                Position = group.Center
            };
            var gripStart = new ControlGrip
            {
                Element = group,
                Name = "Start",
                Position = group.Startp
            };
            var gripEnd = new ControlGrip
            {
                Element = group,
                Name = "End",
                Position = group.Endp
            };
            var gripArcMidp = new ControlGrip
            {
                Element = group,
                Name = "ArcMidp",
                Position = group.Midp
            };
            grips.Add(gripCenter);
            grips.Add(gripStart);
            grips.Add(gripEnd);
            grips.Add(gripArcMidp);
            return grips.ToArray();
        }
        private string _gripName;
        private Vector2 _position;
        private LcArc _group;

        /// <summary>
        /// 拖拽完成后画弧
        /// </summary>
        /// <param name="element"></param>
        /// <param name="gripName"></param>
        /// <param name="position"></param>
        /// <param name="isEnd"></param>
        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            //LcArc lcArc = element as LcArc;
            //Vector2 center = new Vector2();
            _group = element as LcArc;
            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {
                Vector2 dragposition = position;
                if (this.vportRt.SnapRt?.Current != null)
                {
                    dragposition = this.vportRt.SnapRt.Current.SnapPoint;
                }
                if (_gripName == "Start")
                {
                    _group.Set(start: dragposition);
                }
                if (_gripName == "End")
                {
                    
                    _group.Set(end: dragposition);
                    //center = LcArc.GetCenter(lcArc.Startp, lcArc.Midp, _position);
                }
                if (_gripName == "ArcMidp")
                {
                    _group.Set(mid: dragposition);
                }
                if (_gripName == "Center")
                {
                    _group.Set(center: dragposition);
                }
                return;

            }
        }
        /// <summary>
        /// 编辑过程中的渲染
        /// </summary>
        /// <param name="canvas"></param>
        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_group == null) return;  //如果画的圆弧是空的就返回
            LcArc lcarc = new LcArc();
            if (_gripName == "Start")
            {
                lcarc.Startp = _position; //开始点
                lcarc.Endp = _group.Endp; //结束点
                lcarc.Midp = _group.Midp; //圆弧中间点
                lcarc.Center = GeoUtils.GetCenter(lcarc.Startp, lcarc.Midp, lcarc.Endp);
            }
            if (_gripName == "End")
            {
                lcarc.Startp = _group.Startp;
                lcarc.Endp = _position;
                lcarc.Midp = _group.Midp;
                lcarc.Center = GeoUtils.GetCenter(lcarc.Startp, lcarc.Midp, lcarc.Endp);
            }
            if (_gripName == "ArcMidp")
            {
                lcarc.Startp = _group.Startp;
                lcarc.Endp = _group.Endp;
                lcarc.Midp = _position;
                lcarc.Center = GeoUtils.GetCenter(lcarc.Startp, lcarc.Midp, lcarc.Endp);
            }
            if (_gripName == "Center")
            {
                lcarc.Startp = _group.Startp + (_position - _group.Center);
                lcarc.Endp = _group.Endp + (_position - _group.Center);
                lcarc.Midp = _group.Midp + (_position - _group.Center);
                lcarc.Center = _position;
            }
            bool df = false;
            Arc2d arc2D = LcArc.CreateARC(lcarc.Startp, lcarc.Midp, lcarc.Endp, out df);
            Matrix3 matrix = Matrix3.GetTranslate(new Vector2(0, 0));
            arc2D.Center = lcarc.Center;
            arc2D.Radius = Vector2.Distance(arc2D.Center, lcarc.Startp);
            this.vportRt.DrawArc(arc2D, matrix, canvas, Constants.auxElementPen);
            return;
        }

   
        public override SnapPointResult SnapPoint(SnapRuntime snapRt, LcElement element, Vector2 point, bool forRef, Vector2 PrePoint = null)
        {
            var maxDistance = vportRt.GetSnapMaxDistance();
            LcArc arc = element as LcArc;
            var sscur = SnapSettings.Current;
            var result = new SnapPointResult { Element = element };
            Arc2d arc2D = new Arc2d();
            arc2D.Center = arc.Center;
            arc2D.Radius = arc.Radius;
            arc2D.StartAngle = arc.StartAngle;
            arc2D.EndAngle = arc.EndAngle;
            var icir = arc2D;
            if (SnapSettings.Current.ObjectOn)
            {
                if (sscur.PointType.Has(SnapPointType.Center))
                {
                    if ((point - arc.Center).Length() <= maxDistance)
                    {
                        result.Point = arc.Center;
                        result.Name = "Center";
                        result.Curves.Add(new SnapRefCurve(SnapPointType.Center, icir));
                    }
                }
                if (sscur.PointType.Has(SnapPointType.Quadrant))
                {
                    foreach (var item in GetControlGrips(element))
                    {
                        if (item.Name != "Center")
                        {
                            if ((point - item.Position).Length() <= maxDistance)
                            {
                                result.Point = item.Position;
                                result.Name = item.Name;
                                result.Curves.Add(new SnapRefCurve(SnapPointType.Quadrant, icir));
                            }
                        }
                    }
                }
                if (!forRef && result.Point == null)
                {
                    var distance = Vector2.Distance(point, arc.Center);
                    if (sscur.PointType.Has(SnapPointType.Nearest))
                    {
                        if (Math.Abs(arc.Radius - distance) < maxDistance)
                        {
                            double[] x = new double[2];
                            double[] y = new double[2];
                            var distancex = Vector2.Distance(point, new Vector2(x[0], x[1]));
                            var distancey = Vector2.Distance(point, new Vector2(y[0], y[1]));
                            var dir = (point - arc.Center).Normalize();
                            var nearest = arc.Center + dir * arc.Radius;
                            result.Point = nearest;
                            result.Name = "Nearest";
                            result.Curves.Add(new SnapRefCurve(SnapPointType.Nearest, icir));
                        }
                    }
                }
            }

            if (result.Point != null)
                return result;
            else
                return null;
        }
        /// <summary>
        /// 属性栏
        /// </summary>
        /// <returns></returns>
        public override List<PropertyObserver> GetPropertyObservers()
        {
            return new List<PropertyObserver>()
            {
                new PropertyObserver()
                {
                    Name = "StartX",
                    DisplayName = "起点 X 坐标",
                    Getter = (ele) => Math.Round((ele as LcArc).Startp.X, 4),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var x = Convert.ToDouble(value);
                        var start = new Vector2(x, lcArc.Startp.Y);
                        lcArc.Set(start : start);
                    }
                },
                 new PropertyObserver()
                {
                    Name = "StartY",
                    DisplayName = "起点 Y 坐标",
                    Getter = (ele) => Math.Round((ele as LcArc).Startp.Y, 4),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var y = Convert.ToDouble(value);
                        var start = new Vector2( lcArc.Center.X, y);
                        lcArc.Set(start : start);
                    }
                },
                new PropertyObserver()
                {
                     Name = "CenterX",
                    DisplayName = "圆心 X 坐标",
                    Getter = (ele) => Math.Round((ele as LcArc).Center.X, 4),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var x = Convert.ToDouble(value);
                        var cen = new Vector2(x, lcArc.Center.Y);
                        lcArc.Set(center : cen);
                    }
                },
                new PropertyObserver()
                {
                    Name = "CenterY",
                    DisplayName = "圆心 Y 坐标",
                    Getter = (ele) => Math.Round((ele as LcArc).Center.Y, 4),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var y = Convert.ToDouble(value);
                        var cen = new Vector2(lcArc.Center.X, y);
                        lcArc.Set(center : cen);
                    }
                },
                new PropertyObserver()
                {
                     Name = "EndX",
                    DisplayName = "端点 X 坐标",
                    Getter = (ele) => Math.Round((ele as LcArc).Endp.X, 4),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var x = Convert.ToDouble(value);
                        var end = new Vector2(x, lcArc.Endp.Y);
                        lcArc.Set(end : end);
                    }
                },
                new PropertyObserver()
                {
                     Name = "EndY",
                    DisplayName = "端点 Y 坐标",
                    Getter = (ele) => Math.Round((ele as LcArc).Endp.Y, 4),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var y = Convert.ToDouble(value);
                        var end = new Vector2(lcArc.Endp.X, y);
                        lcArc.Set(start : end);
                    }
                },
                new PropertyObserver()
                {
                    Name = "Radius",
                    DisplayName = "半径",
                    Getter = (ele) => Math.Round((ele as LcArc).Radius, 4),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var radius = Convert.ToDouble(value);
                        lcArc.Set(radius : radius);
                    }
                },
                new PropertyObserver()
                {
                    Name = "Startangle",
                    DisplayName = "起点角度",
                    Getter = (ele) => Math.Round((ele as LcArc).StartAngle*180/Math.PI ),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var startAngle = Convert.ToDouble(value);
                        lcArc.Set(startAngle : startAngle);
                    }
                },
                 new PropertyObserver()
                {
                    Name = "Endangle",
                    DisplayName = "端点角度",
                    Getter = (ele) => Math.Round((ele as LcArc).EndAngle*180/Math.PI ),
                    Setter = (ele, value) =>
                    {
                        var lcArc = (ele as LcArc);
                        var endAngle = Convert.ToDouble(value);
                        lcArc.Set(endAngle : endAngle);
                    }
                },
                  new PropertyObserver()
                {
                    Name = "Totalangle",
                    DisplayName = "总角度",
                    Getter = (ele) =>
                    {
                        double sumAngle;
                        double startangle =(ele as LcArc).StartAngle*180/Math.PI;
                        double endangle=(ele as LcArc).EndAngle * 180 / Math.PI;
                        sumAngle=endangle-startangle;
                          if (sumAngle < 0)
                        {
                            sumAngle+=360;
                        }
                          return sumAngle;
                    }
                },
                  new PropertyObserver()
                {
                    Name = "Arclength",
                    DisplayName = "弧长",
                    Getter = (ele) =>
                    {
                        double sumAngle;
                        double startangle =(ele as LcArc).StartAngle*180/Math.PI;
                        double endangle=(ele as LcArc).EndAngle * 180 / Math.PI;
                        sumAngle=endangle-startangle;
                          if (sumAngle < 0)
                        {
                            sumAngle+=360;
                        }
                        sumAngle = sumAngle * (Math.PI / 180.0);
                        double arcLength = (ele as LcArc).Radius * sumAngle;
                        return arcLength;
                    }
                },
                   new PropertyObserver()
                {
                    Name = "Area",
                    DisplayName = "面积",
                    Getter = (ele) =>
                    {
                        double sumAngle;
                        double startangle =(ele as LcArc).StartAngle*180/Math.PI;
                        double endangle=(ele as LcArc).EndAngle * 180 / Math.PI;
                        sumAngle=endangle-startangle;
                          if (sumAngle < 0)
                        {
                            sumAngle+=360;
                        }
                        sumAngle = sumAngle * (Math.PI / 180.0);
                        double arcArea = (Math.Pow((ele as LcArc).Radius, 2) / 2) * ((ele as LcArc).Radius - Math.Sin((ele as LcArc).Radius));
                        return arcArea;
                    }
                },
                  new PropertyObserver()
                {
                    Name = "NormalX",
                    DisplayName = "法向 X 坐标",
                    Getter = (ele) => 0
                },
                new PropertyObserver()
                {
                    Name = "NormalY",
                    DisplayName = "法向 Y 坐标",
                    Getter = (ele) => 0
                },
                new PropertyObserver()
                {
                    Name = "NormalZ",
                    DisplayName = "法向 Z 坐标",
                    Getter = (ele) => 1
                },

            };
        }
    }
}
