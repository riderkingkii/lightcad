﻿using LightCAD.Core.Elements.Basic;
using LightCAD.Runtime;
using SkiaSharp;

namespace LightCAD.Drawing.Actions.Action
{
    public class TableEditAction : ElementAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;

        public TableEditAction() { }
        public TableEditAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令 ：TableEdit");
        }

        static TableEditAction()
        {
            CommandName = "TableEdit";
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "Input",
                Description = "输入文字",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep{ Name="Step0", Options="编辑单元格文字：" },
                }
            };
        }

        internal static void Initilize()
        {
            ElementActions.TableEdit = new TableEditAction();
            LcDocument.ElementActions.Add(BuiltinElementType.TableCell, ElementActions.TableEdit);
        }

        private PointInputer inputer;
        public string text = ""; 

        public async Task<string> ExecCreate(string[] args = null)
        {
            this.StartCreating();
            var curMethod = CreateMethods[0];
            this.inputer = new PointInputer(this.docEditor);

            Step0:
                var step0 = curMethod.Steps[0];
                var result0 = await inputer.Execute(step0.Options);
                if (inputer.isCancelled || result0 == null) { this.Cancel(); goto End; }

                if (result0.ValueX == null)
                {
                    if (!string.IsNullOrEmpty(result0.Option))
                    {
                        this.text = result0.Option;
                        goto End;
                    }
                }
                goto Step0;
            End:
                this.EndCreating();
            return this.text;
        }

        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var tableCell = element as LcTableCell;

            var start = this.vportRt.ConvertWcsToScr(matrix.MultiplyPoint(tableCell.FirstPoint)).ToSKPoint();
            var pen = this.GetDrawPen(tableCell);
            if (pen == Constants.defaultPen)
            {
                pen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true };
            }

            DrawCore(canvas, (float)tableCell.ColumnWidth, (float)tableCell.RowHeight, start, tableCell.Text ?? "", pen);
        }

        private void DrawCore(SKCanvas canvas, float width, float height, SKPoint start, string text, SKPaint pen)
        {
            width *= (float)this.vportRt.Viewport.Scale;
            height *= (float)this.vportRt.Viewport.Scale;
            SKRect cellRect = new SKRect();
            cellRect.Left = start.X;
            cellRect.Top = start.Y;
            cellRect.Right = cellRect.Left + width;
            cellRect.Bottom = cellRect.Top + height;
            canvas.DrawRect(cellRect, pen);

            // 设置画笔和文本样式
            var index = SKFontManager.Default.FontFamilies.ToList().IndexOf("宋体");
            // 创建宋体字形
            var songtiTypeface = SKFontManager.Default.GetFontStyles(index).CreateTypeface(0);
            SKPaint paint = new SKPaint
            {
                Color = new SKColor(0x42, 0x81, 0xA4),
                TextSize = 100.0f * (float)this.vportRt.Viewport.Scale,
                TextAlign = SKTextAlign.Center,
                Typeface = songtiTypeface,
            };

            // 计算文本位置使其在矩形中居中
            float textWidth = paint.MeasureText(text);
            SKRect bounds = new SKRect();
            paint.MeasureText(text, ref bounds);
            // 绘制文本
            canvas.DrawText(text, start.X + (float)width / 2, cellRect.MidY - (bounds.MidY), paint);
        }
    }
}
