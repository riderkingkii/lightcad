﻿using LightCAD.Three;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing.Actions
{
    public class HatchAction : ElementAction
    {
        public static LcCreateMethod[] CreateMethods;

        public HatchAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Hatch");
        }
        public HatchAction()
        {
            CreateMethods = new LcCreateMethod[2];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "CreateHatch",
                Description = "创建填充",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep { Name=  "Step0", Options= "拾取内部点或 [选择对象(S)/放弃(U)/设置(T)]:" },
                    new LcCreateStep { Name=  "Step1", Options= "选择对象或 [拾取内部点(K)/放弃(U)/设置(T)]:" },
                }
            };
        }
        internal static void Initilize()
        {
            ElementActions.Hatch = new HatchAction();
            LcDocument.ElementActions.Add(BuiltinElementType.Hatch, ElementActions.Hatch);
        }
        private ElementSetInputer ElementInputers { get; set; }
        private PointInputer PointInputer { get; set; }
        private List<LcElement> lcElements { get; set; }
        private HatchPattern HatchPattern { get; set; }

        public async void ExecCreate(string[] args = null)
        {
            this.StartCreating();
            var curMethod = CreateMethods[2];

        Step0:
            var step0 = curMethod.Steps[0];
            this.PointInputer = new PointInputer(this.docEditor);
            var result0 = await PointInputer.Execute(step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            if (result0.Option == null)
            {
                goto Step0;
            }
        Step1:
            var step1 = curMethod.Steps[1];
            this.ElementInputers = new ElementSetInputer(this.docEditor);
            var result1 = await ElementInputers.Execute(step1.Options);
            if (ElementInputers.isCancelled)
            {
                this.Cancel();
                goto End;
            }
            if (result1.Option == null)
            {
                goto Step1;
            }
            else if (result1.Option.ToUpper() == "K")
            {
                goto Step0;
            }
            else
            {

                List<LcElement> elements = (List<LcElement>)result1.ValueX;
                if (elements.Count == 0)
                {
                    goto Step1;
                }
                else
                {
                    lcElements.AddRange(elements);
                    goto Step3;
                }


            }
        Step3:
            //获取填充图案
            HatchPattern = HatchPattern.Load(@"C:\Users\ASUS\Desktop\图案填充\QD_ANSI32.pat", "QD_ANSI32");


        End:
            this.EndCreating();
            this.docRt.Action.ClearSelects();
        }

        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var lcHatch = element as LcHatch;
            HatchPattern hatchPattern = lcHatch.pattern;
            SKPath path = new SKPath();

            path.FillType = SKPathFillType.EvenOdd;
            SKPaint paint = new SKPaint(new SKFont(SKTypeface.Default))
            {
                IsAntialias = true,
                Style = SKPaintStyle.Fill,
                Color = SKColors.Blue.WithAlpha((byte)(0xFF * (1 - 0.5))),
                StrokeWidth = 1
            };
            canvas.DrawPath(path, paint);
            var hatch = new SKPath();
            hatch.AddCircle(0, 0, 1);
            var hatchPaint = new SKPaint
            {
                PathEffect = SKPathEffect.Create2DPath(SKMatrix.MakeScale(7, 7), hatch),
                Color = SKColors.RosyBrown,
                Style = SKPaintStyle.Stroke,
                StrokeWidth = 3
            };
            canvas.DrawPath(path, hatchPaint);
        }
    }
}
