﻿using LightCAD.Three.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing.Actions
{
    public class Curve2dAction: ElementAction
    {
        protected Curve2dAction() { }
        public Curve2dAction(IDocumentEditor docEditor)
         : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Curve2d");
        }
        public override void Cancel()
        {
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
            //TODO:
            this.EndCreating();
        }
        public override void CreateElement(LcElement element, Matrix3 matrix)
        {
            var curveEle = (element as LcCurve2d).Clone();
            curveEle.ApplyMatrix(matrix);
            this.vportRt.ActiveElementSet.InsertElement(curveEle);
        }

        public override void CreateElement(LcElement element, Vector2 basePoint, double scaleFactor)
        {
            var curveEle = (element as LcCurve2d).Clone()as LcCurve2d;
            Matrix3 matrix3 = Matrix3.GetScale(scaleFactor, basePoint);
            curveEle.ApplyMatrix(matrix3);
            this.vportRt.ActiveElementSet.InsertElement(curveEle);
        }

        public override void CreateElement(LcElement element, Vector2 basePoint, Vector2 scaleVector)
        {
            var curveEle = (element as LcCurve2d).Clone() as LcCurve2d;
            Matrix3 matrix3 = Matrix3.GetScale(scaleVector, basePoint);
            curveEle.ApplyMatrix(matrix3);
            this.vportRt.ActiveElementSet.InsertElement(curveEle);
        }
        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var points = (element as LcCurve2d).Curve.GetPoints(-1,this.vportRt.Viewport.Scale);
            for (int i = 0; i < points.Length; i++)
            {
                var point = points[i];
                point.ApplyMatrix3(matrix);
            }
            var pen = this.GetDrawPen(element);
            if (pen == Constants.defaultPen)
            {
                //TODO:这里可以考虑将实线用颜色做KEY，对SKPaint进行缓存
                pen.Color = new SKColor(element.GetColorValue());
                pen.IsStroke = true;
            }
            canvas.DrawPoints(SKPointMode.Polygon, points.Select(p => this.vportRt.ConvertWcsToScr(p).ToSKPoint()).ToArray(), pen);
        }
        public override void Draw(SKCanvas canvas, LcElement element, Vector2 offset)
        {
            if (this.vportRt==null)
            {
                return;
            }
            if (!(element is LcCurve2d))
            {

            }
            var points = (element as LcCurve2d).Curve.GetPoints(-1, this.vportRt.Viewport.Scale);
            for (int i = 0; i < points.Length; i++)
            {
                var point = points[i];
                point.Add(offset);
            }
            var pen =  this.GetDrawPen(element);
            if (pen == Constants.defaultPen)
            {
                //TODO:这里可以考虑将实线用颜色做KEY，对SKPaint进行缓存
                pen.Color = new SKColor(element.GetColorValue());
                pen.IsStroke = true;
            }
            canvas.DrawPoints(SKPointMode.Polygon, points.Select(p => this.vportRt.ConvertWcsToScr(p).ToSKPoint()).ToArray(), pen);
        }
       
    }
}
