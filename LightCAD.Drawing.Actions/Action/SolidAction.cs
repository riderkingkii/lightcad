﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing.Actions
{
    public class SolidAction:ElementAction
    {
        public SolidAction() : base() { }
        public SolidAction(IDocumentEditor documentEditor) : base(documentEditor) 
        { }
        internal static void Initialize() 
        {
            ElementActions.Solid = new SolidAction();
            LcDocument.ElementActions.Add(BuiltinElementType.Solid3d, ElementActions.Solid);
        }
    }
}
