﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;


namespace LightCAD.Drawing.Actions
{
    public class DirectComponentAction : ComponentInstance2dAction
    {
        internal static void Initilize()
        {
            ElementActions.DirectComponent = new DirectComponentAction();
            LcDocument.ElementActions.Add(BuiltinElementType.DirectComponent, ElementActions.DirectComponent);
        }

        private List<Vector2> points;
        private Vector2 originPoint { get; set; }
        private Vector2 thruPoint { get; set; }
        private PointInputer inputer { get; set; }

        protected DirectComponentAction():base() { }
        public DirectComponentAction(IDocumentEditor docEditor) : base(docEditor)
        {
        }

        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var dirCom = element as DirectComponent;
            var mstart = matrix.MultiplyPoint((dirCom.BaseCurve as Line2d).Start);
            var mend = matrix.MultiplyPoint((dirCom.BaseCurve as Line2d).End);
            var start = this.vportRt.ConvertWcsToScr(mstart).ToSKPoint();
            var end = this.vportRt.ConvertWcsToScr(mend).ToSKPoint();

            using (var elePen = new SKPaint { Color = SKColors.Gray, IsStroke = true, PathEffect = SKPathEffect.CreateDash(new float[] { 10, 10, 10, 10 }, 0) })
            {
                canvas.DrawLine(start, end, elePen);
            }
            foreach (var itemGrp in dirCom.Curves)
            {
                foreach (var item in itemGrp.Curve2ds)
                {
                    Line2d line2D = item as Line2d;
                    mstart = matrix.MultiplyPoint(line2D.Start);
                    mend = matrix.MultiplyPoint(line2D.End);
                    start = this.vportRt.ConvertWcsToScr(mstart).ToSKPoint();
                    end = this.vportRt.ConvertWcsToScr(mend).ToSKPoint();
                    var pen = this.GetDrawPen(element);
                    if (pen == Constants.defaultPen)
                    {
                        using (var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
                        {
                            canvas.DrawLine(start, end, elePen);
                        }
                    }
                    else
                    {
                        canvas.DrawLine(start, end, pen);
                    }
                }
            }
        }
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var dirCom = element as DirectComponent;
            var grips = new List<ControlGrip>();
            var center = dirCom.BoundingBox.Center;
            grips.Add(new ControlGrip
            {
                Element = element,
                Position = center,
                Name = "Center",
                Shape = GripShape.Rect
            });
            return grips.ToArray();
        }

        private string _gripName;
        private Vector2 _position;
        private DirectComponent _dirCom;
        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            var dirCom = element as DirectComponent;
            _dirCom = dirCom;
            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {
                if (gripName == "Center")
                    dirCom.Translate(position-dirCom.BoundingBox.Center);
                _dirCom = null;
             }
        }

        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_dirCom == null) return;

            var offset = _position - _dirCom.BoundingBox.Center;
            this.Draw(canvas, _dirCom, offset);
        }
    }
}
