﻿using HarfBuzzSharp;
using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using LightCAD.Three;
using netDxf.Entities;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using LightCAD.MathLib;
using Avalonia.Controls;
using Avalonia.Media;

namespace LightCAD.Drawing.Actions
{
    public class TextAction : ElementAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;

        static TextAction()
        {
            CommandName = "TEXT";
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "TEXT",
                Description = "TEXT",
                Steps = new LcCreateStep[]
                {
                    new LcCreateStep{ Name="Step0", Options="指定文字起点:" },
                    new LcCreateStep{ Name="Step1", Options="指定文字高度:" },
                    new LcCreateStep{ Name="Step2", Options="指定文字角度:" },
                    new LcCreateStep{ Name="Step3", Options="输入文字:" },
                    //new LcCreateStep{ Name="Step4", Options="倾斜:" },
                    //new LcCreateStep{ Name="Step5", Options="宽度因子:" },
                    //new LcCreateStep{ Name="Step6", Options="旋转:" },
                }
            };
            
        }

        internal static void Initilize()
        {
            ElementActions.LcText = new TextAction();
            LcDocument.ElementActions.Add(BuiltinElementType.LcText, ElementActions.LcText);
        }

        private PointInputer inputer { get; set; }

        private Vector2 startpoint;
        private Vector2 endpoint;
        private Vector2 heightpoint;
        private string Text;

        //倾斜
        private double Tilt;
        //宽度因子
        private double Widthfactor;
        //旋转
        private double Rotate;

        private List<Line2d> line2D;



        public TextAction() { }
        public TextAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令 TEXT");
        }
        private LcCreateMethod GetMethod(string method)
        {
            if (method == null) return CreateMethods[0];
            var getted= CreateMethods.FirstOrDefault((m) => m.Name == method);
            if (getted == null)
                return CreateMethods[0];
            else
                return getted;
        }
        public async void ExecCreate(string[] args = null)
        {
            this.StartCreating();
            //this.Segments = new List<RolineSegment>();
            var curMethod = CreateMethods[0];
            var ElementInputer = new ElementInputer(this.docEditor);
            this.inputer = new PointInputer(this.docEditor);
            Step0:
            var step0 = curMethod.Steps[0];
            var result0 = await inputer.Execute(step0.Options);
            if (inputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result0.ValueX == null)
            {
                if (result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step1;
                }
            }
            else
            {
               
                this.startpoint = (Vector2)result0.ValueX;

                goto Step1;
            }
            Step1:
            var step1 = curMethod.Steps[1];
            var result1 = await inputer.Execute(step1.Options);
            if (inputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result1 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result1.ValueX == null)
            {
                if (result1.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step2;
                }
            }
            else
            {

                this.heightpoint = (Vector2)result1.ValueX;

                goto Step2;
            }
            Step2:
            var step2 = curMethod.Steps[2];
            var result2 = await inputer.Execute(step2.Options);
            if (inputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result2 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result2.ValueX == null)
            {
                if (result2.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step3;
                }
            }
            else
            {

                this.endpoint = (Vector2)result2.ValueX;

                goto Step3;
            }
            Step3:
            var step3 = curMethod.Steps[3];
            var result3 = await inputer.Execute(step3.Options);
            if (inputer.isCancelled) { this.Cancel(); return; }
            // zcb: 增加Res0为空的判定
            if (result3 == null)
            {
                this.Cancel();
                goto End;
            }
            if (result3.ValueX == null)
            {
                if (result3.Option != null)
                {
                    if (result3.Option!= "")
                    {
                        this.Text = result3.Option;
                        CreateTextElement();

                    }
                    goto End;
                }
                else
                {
                    goto Step3;
                }
            }
            else
            {
              goto  End;
            }
            #region
            //Step4:
            //var step4 = curMethod.Steps[4];
            //var result4 = await inputer.Execute(step4.Options);
            //if (inputer.isCancelled) { this.Cancel(); return; }
            //// zcb: 增加Res0为空的判定
            //if (result4 == null)
            //{
            //    this.Cancel();
            //    goto End;
            //}
            //if (result4.ValueX == null)
            //{
            //    if (result4.Option != null)
            //    {
            //        if (result4.Option != "")
            //        {
            //            this.Tilt =Convert.ToDouble(result4.Option);


            //        }
            //        goto Step5;
            //    }
            //    else
            //    {
            //        goto Step5;
            //    }
            //}
            //else
            //{
            //    goto Step5;
            //}
            //Step5:
            //var step5 = curMethod.Steps[5];
            //var result5 = await inputer.Execute(step5.Options);
            //if (inputer.isCancelled) { this.Cancel(); return; }
            //// zcb: 增加Res0为空的判定
            //if (result5 == null)
            //{
            //    this.Cancel();
            //    goto End;
            //}
            //if (result5.ValueX == null)
            //{
            //    if (result5.Option != null)
            //    {
            //        if (result5.Option != "")
            //        {
            //            this.Widthfactor = Convert.ToDouble(result5.Option);


            //        }
            //        goto Step6;
            //    }
            //    else
            //    {
            //        goto Step6;
            //    }
            //}
            //else
            //{
            //    goto Step6;
            //}
            //Step6:
            //var step6 = curMethod.Steps[6];
            //var result6 = await inputer.Execute(step6.Options);
            //if (inputer.isCancelled) { this.Cancel(); return; }
            //// zcb: 增加Res0为空的判定
            //if (result6 == null)
            //{
            //    this.Cancel();
            //    goto End;
            //}
            //if (result6.ValueX == null)
            //{
            //    if (result6.Option != null)
            //    {
            //        if (result6.Option != "")
            //        {
            //            this.Rotate = Convert.ToDouble(result6.Option);
            //            CreateTextElement();

            //        }
            //        goto End;
            //    }
            //    else
            //    {
            //        goto Step6;
            //    }
            //}
            //else
            //{
            //    goto Step6;
            //}
            #endregion
            goto End;
            End:
            this.EndCreating();

        }
        private void CreateTextElement()
        {
            var doc = this.docRt.Document;
            DocumentManager.CurrentRecorder.BeginAction("TEXT");
            var Text = doc.CreateObject<LcText>();
            Text.Start = this.startpoint;
            Text.TextStart= this.startpoint;
            Text.Boxstart= this.startpoint; 
            //Text.End = this.endpoint.Value;
            Text.Text = this.Text;
            if (this.heightpoint.Y > this.startpoint.Y)
            {
                Text.Heigh = this.heightpoint.Y - this.startpoint.Y;
            }
            else
            {
                Text.Heigh = this.startpoint.Y - this.heightpoint.Y;
            }
            //字体
            Text.Typeface = "";

            #region 如果输入的角度
            //if (this.Rotate != 0) 
            //{
            //    this.endpoint = Vector2.Rotate(new Vector2(this.startpoint.X+100, this.startpoint.Y), this.startpoint, this.Rotate);

            //}
            #endregion

            //向量
            //Text.XLpoint=new Vector2(this.endpoint.X - this.startpoint.X, this.endpoint.Y - this.startpoint.Y);

            //默认值
            Text.Tilt = 0;
            Text.Widthfactor = 1;

            //计算文字角度
            Text.Rotate = 180 / Math.PI * (this.endpoint - this.startpoint).Angle();

            Text.Alignment = "左对齐";

            //
            #region 计算文字宽度
            //var painttext = new SKPaint();
            //painttext.TextSize = (float)(this.vportRt.Viewport.Scale * Text.Heigh);

            //var tRect = new SKRect();
            ////文字长度
            //double diss = painttext.MeasureText(this.Text, ref tRect);

            //double dis = this.vportRt.ConvertScrToWcs(diss);

            ////通过textsize 计算出文字大小


            ////double dis = pen.MeasureText(mtext);
            //Vector2 lidisptpos = Text.Start + (new Vector2(Text.XLpoint.X * dis, Text.XLpoint.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(Text.XLpoint.X, 2.0) + Math.Pow(Text.XLpoint.Y, 2.0))));
            //Text.End = lidisptpos;

            // 文字宽度公式=文字高度* 宽度因子*字符串长度 * 0.9
            //double dist = Text.Heigh * Text.Widthfactor * Text.Text.Length * 1;
            //Vector2 lidisptpost = Text.Start + (new Vector2(Text.XLpoint.X * dist, Text.XLpoint.Y * dist)) / Math.Sqrt(Math.Abs((Math.Pow(Text.XLpoint.X, 2.0) + Math.Pow(Text.XLpoint.Y, 2.0))));
            //Text.End = lidisptpost;


             
            
            #endregion


            doc.ModelSpace.InsertElement(Text);
            this.docRt.Action.ClearSelects();
            DocumentManager.CurrentRecorder.EndAction();
        }
       

     


      
        public override void Cancel()
        {
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
        }

      



        public override void CreateElement(LcElement element, Matrix3 matrix)
        {
            //DocumentManager.CurrentRecorder.BeginAction("Circle");
            //var dim = element as DimDiametric;
            //this.vportRt.ActiveElementSet.AddDimDiametric(matrix.MultiplyPoint(dim.Start), matrix.MultiplyPoint(dim.End),dim.Dimtext);
            //DocumentManager.CurrentRecorder.EndAction();
        }
        public override void CreateElement(LcElement element,Vector2 basePoint, double scaleFactor)
        {
            //var circle = element as LcCircle;
            //Matrix3d matrix3 = Matrix3d.Scale(scaleFactor, basePoint);
            //this.vportRt.ActiveElementSet.AddCircle(matrix3.MultiplyPoint(circle.Center), circle.Radius * scaleFactor);
        }
        public override void CreateElement(LcElement element, Vector2 basePoint, Vector2 scaleVector)
        {
            //var circle = element as LcCircle;
            //Matrix3d matrix3 = Matrix3d.Scale(scaleVector, basePoint);
            //this.vportRt.ActiveElementSet.AddCircle(matrix3.MultiplyPoint(circle.Center), circle.Radius * Vector2.Distance(basePoint, scaleVector));
        }
        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var dim = element as LcText;
          

            var mtext = dim.Text;
            var pathpoint = new Vector2(0, 0).ToSKPoint();
         

            //判断字体
            if (dim.Typeface == "")
            {
                //默认宋体
                dim.Typeface = "宋体";

            }
            else
            {
                //表示输入的字体无效或者不存在
                if (SKFontManager.Default.FontFamilies.ToList().IndexOf(dim.Typeface) == -1)
                {
                    dim.Typeface = "宋体";
                }
                else
                {
                }

            }

            var pen = this.GetDrawPen(dim);
            //SKTypeface sKTypeface = new SKTypeface();


            var painttext = new SKPaint();
            var fontManager1 = SKFontManager.Default;
            var emojiTypeface1 = fontManager1.MatchCharacter('时');
            var index = SKFontManager.Default.FontFamilies.ToList().IndexOf(dim.Typeface);
            // 创建宋体字形
            var songtiTypeface = SKFontManager.Default.GetFontStyles(index).CreateTypeface(0);
            painttext.TextSize = (float)(this.vportRt.Viewport.Scale * dim.Heigh);
            //painttext.TextAlign =SKTextAlign.Center;
            painttext.IsAntialias = false;
            painttext.Color = new SKColor(0x42, 0x81, 0xA4);
            painttext.IsStroke = false;
            painttext.Typeface = songtiTypeface;
            //倾斜度
            painttext.TextSkewX = (float)dim.Tilt;

            //宽度因子
            painttext.TextScaleX = (float)dim.Widthfactor;

            var tRect = new SKRect();
            //文字长度
            double diss = painttext.MeasureText(mtext, ref tRect);
            double dis = this.vportRt.ConvertScrToWcs(diss);

            double rectheight= this.vportRt.ConvertScrToWcs(tRect.Height);

           // dim.Boxstart =    this.vportRt.ConvertScrToWcs( new Vector2(tRect.Left,tRect.Top));

            Vector2 lidisptpos = new Vector2();
            SKPoint newend;
            //判断是哪种对齐方式
            //if (dim.Alignment == "右对齐")
            //{

            //    //右对齐就是  起点变为结束点  计算新的起点
            //    lidisptpos = dim.Start - (new Vector2(dim.XLpoint.X * dim.Width, dim.XLpoint.Y * dim.Width)) / Math.Sqrt(Math.Abs((Math.Pow(dim.XLpoint.X, 2.0) + Math.Pow(dim.XLpoint.Y, 2.0))));

            //    var mnewstart = matrix.MultiplyPoint(lidisptpos.Clone());
            //    var newstart = this.vportRt.ConvertWcsToScr(mnewstart).ToSKPoint();




            //    //新的文字起点
            //    dim.TextStart = lidisptpos;



            //    ////新的end点
            //    //Vector2 lidisptposs = dim.TextStart + (new Vector2(dim.XLpoint.X * dis, dim.XLpoint.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(dim.XLpoint.X, 2.0) + Math.Pow(dim.XLpoint.Y, 2.0))));
            //    //var mnewend = matrix.MultiplyPoint(lidisptposs);
            //    //newend = this.vportRt.ConvertWcsToScr(mnewend).ToSKPoint();
            //    ////dim.End = lidisptpos;




            //}
            //else if (dim.Alignment == "左对齐")
            //{

            //    ////double dis = pen.MeasureText(mtext);
            //    // lidisptpos = dim.Start + (new Vector2(dim.XLpoint.X * dis, dim.XLpoint.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(dim.XLpoint.X, 2.0) + Math.Pow(dim.XLpoint.Y, 2.0))));
            //    //var mnewend = matrix.MultiplyPoint(lidisptpos);
            //    // newend = this.vportRt.ConvertWcsToScr(mnewend).ToSKPoint();
            //    ////dim.End = lidisptpos;
            //    dim.TextStart = dim.Start;
            //}
            //else if (dim.Alignment == "居中") 
            //{
            //    //居中需要计算文字宽度的一半 计算新的起点
            //    lidisptpos = dim.Start - (new Vector2(dim.XLpoint.X * (dim.Width/2), dim.XLpoint.Y * (dim.Width/2))) / Math.Sqrt(Math.Abs((Math.Pow(dim.XLpoint.X, 2.0) + Math.Pow(dim.XLpoint.Y, 2.0))));

            //    var mnewstart = matrix.MultiplyPoint(lidisptpos.Clone());
            //    var newstart = this.vportRt.ConvertWcsToScr(mnewstart).ToSKPoint();




            //    //新的文字起点
            //    dim.TextStart = lidisptpos;

            //}

            var mstart = matrix.MultiplyPoint(dim.TextStart);
            var start = this.vportRt.ConvertWcsToScr(mstart).ToSKPoint();
            var mend = matrix.MultiplyPoint(dim.End);
            var end = this.vportRt.ConvertWcsToScr(mend).ToSKPoint();



            //通过获得的盒子高度  获取盒子的开始点
            //dim.Boxstart = new Vector2(dim.TextStart.X, dim.TextStart.Y + rectheight);

            if (pen == Runtime.Constants.defaultPen)
            {
                using (var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
                {

                        var skpath = new SKPath();
                        skpath.MoveTo(start);
                        //skpath.LineTo((float)start.X, (float)start.Y);
                        skpath.LineTo((float)end.X, (float)end.Y);


                        SKFont sKFont = new SKFont();
                        //// 获取宋体在字体集合中的下标
                        //var index = SKFontManager.Default.FontFamilies.ToList().IndexOf(typeface);
                        //// 创建宋体字形
                        //var songtiTypeface = SKFontManager.Default.GetFontStyles(index).CreateTypeface(0);
                        sKFont.Typeface = songtiTypeface;
                        sKFont.Size = (float)(this.vportRt.Viewport.Scale * dim.Heigh);

                        //倾斜度
                        sKFont.SkewX = (float)dim.Tilt;

                        //宽度因子
                        sKFont.ScaleX = (float)dim.Widthfactor;

                        canvas.DrawTextOnPath(mtext, skpath, pathpoint, false, sKFont, painttext);



                }
            }
            else
            {

                    var font = new SKFont(SKTypeface.Default);
                    
                    var skpath = new SKPath();
                    skpath.MoveTo(start);
                    //skpath.LineTo((float)start.X, (float)start.Y);
                    skpath.LineTo((float)end.X, (float)end.Y);
                    SKFont sKFont = new SKFont();
                    //// 获取宋体在字体集合中的下标
                    //var index = SKFontManager.Default.FontFamilies.ToList().IndexOf(typeface);
                    //// 创建宋体字形
                    //var songtiTypeface = SKFontManager.Default.GetFontStyles(index).CreateTypeface(0);
                    sKFont.Typeface = songtiTypeface;
                    sKFont.Size = (float)(this.vportRt.Viewport.Scale * dim.Heigh);

                    //倾斜度
                    sKFont.SkewX = (float)dim.Tilt;

                    //宽度因子
                    sKFont.ScaleX = (float)dim.Widthfactor;
                //canvas.DrawTextOnPath(mtext, skpath, 0, 0, paint); 
                canvas.DrawTextOnPath(mtext, skpath, pathpoint, false, sKFont, painttext);
            }
            if (matrix != Matrix3.Identity)
            {
                var leftTop = this.vportRt.ConvertWcsToScr(matrix.MultiplyPoint(dim.BoundingBox.LeftTop));
                var rightBottom = this.vportRt.ConvertWcsToScr(matrix.MultiplyPoint(dim.BoundingBox.RightBottom));
                canvas.DrawRect((float)leftTop.X, (float)leftTop.Y, (float)rightBottom.X - (float)leftTop.X, (float)rightBottom.Y - (float)leftTop.Y, Constants.SilverPen);
            }


        }
       
    


        public override void DrawAuxLines(SKCanvas canvas)
        {
            if (this.startpoint != null&&this.endpoint==null) { 
                var mp = this.vportRt.PointerMovedPosition.ToVector2d();
                var wcs_mp = this.vportRt.ConvertScrToWcs(mp);
                DrawAuxLine(canvas, startpoint, wcs_mp);
            }



        }

        private void DrawAuxLine(SKCanvas canvas, Vector2 p0, Vector2 p1)
        {
            var sk_pre = this.vportRt.ConvertWcsToScr(p0).ToSKPoint();
            var sk_p = this.vportRt.ConvertWcsToScr(p1).ToSKPoint();
            //辅助元素的颜色 
            canvas.DrawLine(sk_pre, sk_p, new SKPaint { Color = this.vportRt.GetAuxColorValue(), IsStroke = true });
            //辅助曲线的颜色，包括辅助长度，辅助角度等
        }
        #region Grip
        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var dim = element as LcText;

            var grips = new List<ControlGrip>();
            var gripTextsatrt = new ControlGrip
            {
                Element = dim,
                Name = "Textsatrt",
                Position = dim.TextStart
            };
            grips.Add(gripTextsatrt);

            

            if (dim.Alignment == "右对齐"|| dim.Alignment == "居中") 
            {
                var gripStartr = new ControlGrip
                {
                    Element = dim,
                    Name = "Start",
                    Position = dim.Start
                };
                grips.Add(gripStartr);

            }



            //var gripStarts = new ControlGrip
            //{
            //    Element = dim,
            //    Name = "box",
            //    Position = dim.Boxstart
            //};
            //grips.Add(gripStarts);




            return grips.ToArray();
        }
        private string _gripName;
        private Vector2 _position;
        private LcText _dim;


        /// <summary>  
        /// 根据余弦定理求两个线段夹角  
        /// </summary>  
        /// <param name="o">端点</param>  
        /// <param name="s">start点</param>  
        /// <param name="e">end点</param>  
        /// <returns></returns>  
        public double SEAngle(Vector2 o, Vector2 s, Vector2 e)
        {

            var so = new Vector2(s.X - o.X, s.Y - o.Y);
            var eo = new Vector2(e.X - o.X, e.Y - o.Y);
            return (so.Angle() - eo.Angle()) /Math.PI*180;

        }


        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            var dim= element as LcText;

            _dim = dim;
            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {
                if (gripName == "Textsatrt")
                {
                    //计算停止时 计算start点
                    //计算点位移动向量
                    Vector2 xl =  new Vector2(position.X - _dim.TextStart.X, position.Y - _dim.TextStart.Y);
                   double movelenght= (position - _dim.TextStart).Length();

                    //start点向通方向移动等量距离
                    var newstart = _dim.Start + (new Vector2(xl.X * movelenght, xl.Y * movelenght)) / Math.Sqrt(Math.Abs((Math.Pow(xl.X, 2.0) + Math.Pow(xl.Y, 2.0))));



                    dim.Set(start: newstart,textstart: position);
                }
                if (gripName == "Start")
                {
                    //计算停止时 计算textstart点
                    //计算点位移动向量
                    Vector2 xl = new Vector2(position.X - _dim.Start.X, position.Y - _dim.Start.Y);
                    double movelenght = (position - _dim.Start).Length();

                    //start点向通方向移动等量距离
                    var newtextstart = _dim.TextStart + (new Vector2(xl.X * movelenght, xl.Y * movelenght)) / Math.Sqrt(Math.Abs((Math.Pow(xl.X, 2.0) + Math.Pow(xl.Y, 2.0))));

                    dim.Set(start: position,textstart: newtextstart);
                }

                else
                {
                 
                   
                    //dim.Set(end: position);
                }
            }
        }


        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_dim == null) return;


            #region  
            var start = this.vportRt.ConvertWcsToScr(_dim.Start);
            var textstart = this.vportRt.ConvertWcsToScr(_dim.TextStart);
            var end = this.vportRt.ConvertWcsToScr(_position);


            //辅助线
           

            var newstart = this.vportRt.ConvertWcsToScr(_position);

            var painttext = new SKPaint();
            //painttext.TextSize = (float)(this.vportRt.Viewport.Scale * dim.Heigh);
            var fontManager1 = SKFontManager.Default;
            var emojiTypeface1 = fontManager1.MatchCharacter('时');
            painttext.TextSize = (float)(this.vportRt.Viewport.Scale * _dim.Heigh);
            //paint.TextAlign = SKTextAlign.Right;
            painttext.IsAntialias = false;
            painttext.Color = new SKColor(0x42, 0x81, 0xA4);
            painttext.IsStroke = false;
            painttext.Typeface = emojiTypeface1;
            //倾斜度
            painttext.TextSkewX = (float)_dim.Tilt;

            //宽度因子
            painttext.TextScaleX = (float)_dim.Widthfactor; 

            var tRect = new SKRect();
            //文字长度
            double diss = painttext.MeasureText(_dim.Text, ref tRect);
            double dis = this.vportRt.ConvertScrToWcs(diss);


            Vector2 lidisptpos = _position + (new Vector2(_dim.XLpoint.X * _dim.Width, _dim.XLpoint.Y * _dim.Width)) / Math.Sqrt(Math.Abs((Math.Pow(_dim.XLpoint.X, 2.0) + Math.Pow(_dim.XLpoint.Y, 2.0))));

            var newend = this.vportRt.ConvertWcsToScr(lidisptpos).ToSKPoint();

            string typeface = "";

            if (_gripName == "Textsatrt") {
                canvas.DrawLine(textstart.ToSKPoint(), end.ToSKPoint(), Runtime.Constants.draggingPen);
                using (var paint = new SKPaint())
                {

                    //判断字体
                    if (_dim.Typeface == "")
                    {
                        //默认宋体
                        typeface = "宋体";

                    }
                    else
                    {
                        //表示输入的字体无效或者不存在
                        if (SKFontManager.Default.FontFamilies.ToList().IndexOf(_dim.Typeface) == -1)
                        {
                            typeface = "宋体";
                        }
                        else
                        {
                            typeface = _dim.Typeface;
                        }

                    }
                    var pathpoint = new Vector2(0, 0).ToSKPoint();

                    var fontManager = SKFontManager.Default;
                    var emojiTypeface = fontManager.MatchCharacter('时');
                    paint.Typeface = emojiTypeface;
                    paint.TextSize = (float)(this.vportRt.Viewport.Scale * _dim.Heigh);
                    //paint.TextAlign = SKTextAlign.Right;
                    paint.IsAntialias = false;
                    paint.Color = new SKColor(0x42, 0x81, 0xA4);
                    paint.IsStroke = false;

                    var font = new SKFont(SKTypeface.Default);

                    var skpath = new SKPath();
                    skpath.MoveTo(newstart.ToSKPoint());
                    //skpath.LineTo((float)newstart.X, (float)newstart.Y);
                    skpath.LineTo((float)newend.X, (float)newend.Y);

                    SKFont sKFont = new SKFont();
                    // 获取宋体在字体集合中的下标
                    var index = SKFontManager.Default.FontFamilies.ToList().IndexOf(typeface);
                    // 创建宋体字形
                    var songtiTypeface = SKFontManager.Default.GetFontStyles(index).CreateTypeface(0);
                    sKFont.Typeface = songtiTypeface;
                    sKFont.Size = (float)(this.vportRt.Viewport.Scale * _dim.Heigh);
                    //倾斜度
                    sKFont.SkewX = (float)_dim.Tilt;

                    //宽度因子
                    sKFont.ScaleX = (float)_dim.Widthfactor;
                    //canvas.DrawTextOnPath(mtext, skpath, 0, 0, paint);
                    canvas.DrawTextOnPath(_dim.Text, skpath, pathpoint, false, sKFont, paint);


                    //canvas.DrawTextOnPath(_dim.text, skpath, 0, 0, paint);
                }




            }
            #endregion






        }

        #endregion
        public static double distance(Vector2 p1, Vector2 p2)
        {
            double result = 0;
            result = Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
            return result;

        }

        public override List<PropertyObserver> GetPropertyObservers()
        {

            var alignment = new string[]
            {
                "左对齐",
                "右对齐",
                "居中",
            };

            return new List<PropertyObserver>()
                       {
                           new PropertyObserver()
                               {
                                    Name = "Text",
                                    DisplayName = "内容",
                                    CategoryName = "Geometry",
                                    CategoryDisplayName = "文字",
                                    Getter = (ele) => (ele as LcText).Text,
                                    Setter = (ele, value) =>
                                       {
                                           var lctext = (ele as LcText);
                                           string textvalue = value.ToString();
                                           if(textvalue!=""){
                                           lctext.Set(textvalue: textvalue);
                                           }
                                           
                                       }
                               },
                           new PropertyObserver()
                               {
                                    Name = "Alignment",
                                    DisplayName = "对正",
                                    CategoryName = "Geometry",
                                    CategoryDisplayName = "文字",
                                    PropType = PropertyType.Array,
                                    Source=(ele) => alignment,
                                    Getter = (ele) => (ele as LcText).Alignment,
                                    Setter = (ele, value) =>
                                       {
                                           var lctext = (ele as LcText);
                                           var textAlignment = value.ToString();
                                           lctext.Set(alignment: textAlignment);
                                       }
                               },
                           new PropertyObserver()
                               {
                                    Name = "Height",
                                    DisplayName = "高度",
                                    CategoryName = "Geometry",
                                    CategoryDisplayName = "文字",
                                    Getter = (ele) =>(ele as LcText).Heigh,
                                    Setter = (ele, value) =>
                                       {
                                           var lctext = (ele as LcText);
                                           var textheight = Convert.ToDouble(value);
                                           lctext.Set(height: textheight);
                                       }
                               },
                            new PropertyObserver()
                               {
                                    Name = "Rotate",
                                    DisplayName = "旋转",
                                    CategoryName = "Geometry",
                                    CategoryDisplayName = "文字",
                                    Getter = (ele) => Math.Round((ele as LcText).Rotate),
                                    Setter = (ele, value) =>
                                       {
                                          
                                           var lctext = (ele as LcText);
                                           var textRotate = Convert.ToDouble(value);

                                           if(textRotate>0){ 
                                               //如果大于360
                                               while (textRotate>360)
                                               {
                                                   textRotate=textRotate-360;

                                               }
                                           }
                                           if(textRotate<0){
                                               while(textRotate<0){
                                                textRotate=textRotate+360;
                                               }
                                           
                                           }
                                           if(textRotate==360){
                                            textRotate=0;
                                           }

                                           lctext.Set(rotate: textRotate);
                                       }
                               },
                              new PropertyObserver()
                               {
                                    Name = "Widthfactor",
                                    DisplayName = "宽度因子",
                                    CategoryName = "Geometry",
                                    CategoryDisplayName = "文字",
                                    Getter = (ele) => (ele as LcText).Widthfactor,
                                    Setter = (ele, value) =>
                                       {
                                           var lctext = (ele as LcText);
                                           var textWidthfactor = Convert.ToDouble(value);
                                           lctext.Set(widthfactor: textWidthfactor);
                                       }
                               },
                                new PropertyObserver()
                               {
                                    Name = "Tilt",
                                    DisplayName = "倾斜",
                                    CategoryName = "Geometry",
                                    CategoryDisplayName = "文字",
                                    Getter = (ele) => (ele as LcText).Tilt,
                                    Setter = (ele, value) =>
                                       {
                                           var lctext = (ele as LcText);
                                           var textTilt = Convert.ToDouble(value);
                                           lctext.Set(tilt: textTilt);
                                       }
                               },



                       };
        }
    }
}
