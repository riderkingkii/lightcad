﻿using LightCAD.Core.Elements;
using LightCAD.Core;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using LightCAD.MathLib;

namespace LightCAD.Drawing.Actions
{
    public class DimAlignedAction : ElementAction
    {
        private DimAlignedAction() { }

        public DimAlignedAction(IDocumentEditor docEditor)
            : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Dimaligned");
        }


        private static readonly LcCreateMethod[] CreateMethods;

        static DimAlignedAction()
        {
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "创建对齐标注",
                Description = "创建对齐标注",
                Steps = new LcCreateStep[]
                                                   {
                                                       new LcCreateStep { Name = "Step0", Options = "指定第一个尺寸界线原点或 <选择对象>:" },
                                                       new LcCreateStep { Name = "Step1", Options = "指定第二条尺寸界线原点:" },
                                                       new LcCreateStep { Name = "Step2", Options = "指定尺寸线位置或\r\n[多行文字(M)/文字(T)/角度(A)]:" },
                                                   }
            };
        }

        internal static void Initilize()
        {
            ElementActions.DimAligned = new DimAlignedAction();
            LcDocument.ElementActions.Add(BuiltinElementType.DimAligned, ElementActions.DimAligned);
        }

        private List<Vector2> points;

        private Vector2 firstPoint { get; set; }

        private Vector2 secondPoint { get; set; }
        private Vector2 thirdPoint { get; set; }

        private PointInputer inputer { get; set; }


        public async void ExecCreate(string[] args = null)
        {
            DocumentManager.CurrentRecorder.BeginAction("DimAligned");

            this.points = new List<Vector2>();
            this.StartCreating();
            this.inputer = new PointInputer(this.docEditor);
            var ElementInputer = new ElementInputer(this.docEditor);
            var curMethod = SetCurMethod(CreateMethods, 0);

        Step0:
            var step0 = SetCurStep(curMethod, 0);
            var result0 = await inputer.Execute(step0.Options);
            //var result0 = await inputer.Execute(step0.Options);

            if (inputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }

            // zcb: 增加Res0为空的判定
            if (result0 == null)
            {
                this.Cancel();
                goto End;
            }

            if (result0.ValueX == null)
            {
                if (result0.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step1;
                }
            }

            this.firstPoint = (Vector2)result0.ValueX;

        Step1:
            var step1 = SetCurStep(curMethod, 1);
            if (this.points.Count >= 2)
            {
                step1 = curMethod.Steps[2];
            }

            var result1 = await inputer.Execute(step1.Options);
            if (inputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }

            if (result1.ValueX == null)
            {
                if (result1.Option == " ")
                {
                    goto End;
                }
                else if (result1.Option != null)
                {
                    this.commandCtrl.WriteInfo(SR.PointError);
                    goto Step2;
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step2;
                }
            }
            this.secondPoint = (Vector2)result1.ValueX;
        Step2:
            var step2 = SetCurStep(curMethod, 2);
            var result2 = await inputer.Execute(step2.Options);
            //var result0 = await inputer.Execute(step0.Options);

            if (inputer.isCancelled)
            {
                this.Cancel();
                goto End;
            }

            // zcb: 增加Res0为空的判定
            if (result2 == null)
            {
                this.Cancel();
                goto End;
            }

            if (result2.ValueX == null)
            {
                if (result2.Option != null)
                {
                    //TODO:AutoCAD画线输入一个数字，是怎么确定点的？
                }
                else
                {
                    goto Step2;
                }
            }

            this.thirdPoint = (Vector2)result2.ValueX;

            var dimAligned = docRt.Document.CreateObject<DimAligned>(firstPoint, secondPoint, thirdPoint);
            points.Add(firstPoint);
            points.Add(secondPoint);
            points.Add(thirdPoint);
            firstPoint = secondPoint;

            CreateDimElements(dimAligned);
        End:
            this.inputer = null;
            this.EndCreating();

            if (this.points.Count > 0)
            {
                DocumentManager.CurrentRecorder.EndAction();
            }
            else
            {
                DocumentManager.CurrentRecorder.CancleAction();
            }
        }

        public override void Cancel()
        {
            base.Cancel();
            this.vportRt.SetCreateDrawer(null);
        }
        public void CreateDimElements(LcElement lcElement)
        {
            DimAligned dimAligned = lcElement as DimAligned;
            var doc = this.docRt.Document;

            LcText text = doc.CreateObject<LcText>();
            text.Start = dimAligned.Fifth;
            text.TextStart = dimAligned.Fifth;
            text.Heigh = 500;// this.raduis * 2 / 20 < 1 ? 1 : Math.Round(this.raduis * 2 / 20);
            text.Text = dimAligned.Length.ToString("0.00");
            text.Rotate = 180 / Math.PI * (new Vector2(dimAligned.Third.X, dimAligned.Third.Y) - dimAligned.Fifth).Angle();

            text.Alignment = "左对齐";
            text.Widthfactor = 1;
            text.Tilt = 0;
            dimAligned.Text = text;
            this.docRt.Document.ModelSpace.InsertElement(lcElement);
        }
        bool isDraw = true;
        public override void DrawAuxLines(SKCanvas canvas)
        {
            if (firstPoint != null && isDraw)
            {
                isDraw = true;
                var mp = this.vportRt.PointerMovedPosition.ToVector2d();
                var wcs_mp = this.vportRt.ConvertScrToWcs(mp);
                DrawAuxLine(canvas, firstPoint, wcs_mp);
            }
            if (secondPoint != null)
            {

                isDraw = false;
                var mp = this.vportRt.PointerMovedPosition.ToVector2d();
                var wcs_mp = this.vportRt.ConvertScrToWcs(mp);
                //if (firstPoint.X == secondPoint.X)
                //{
                //    vertical = false;
                //}
                //else if (firstPoint.Y == secondPoint.Y)
                //{
                //    vertical = true;
                //}
                //else if ((wcs_mp.X > firstPoint.X && wcs_mp.X < secondPoint.X) || (wcs_mp.X < firstPoint.X && wcs_mp.X > secondPoint.X))
                //{
                //    vertical = true;
                //}
                //else if ((wcs_mp.Y > firstPoint.Y && wcs_mp.Y < secondPoint.Y) || (wcs_mp.Y < firstPoint.Y && wcs_mp.Y > secondPoint.Y))
                //{
                //    vertical = false;
                //}
                //if (vertical)
                //{
                //    Vector2d newWCS_mp = new Vector2d(firstPoint.X, wcs_mp.Y);
                //    Vector2d newWCS_mp2 = new Vector2d(secondPoint.X, wcs_mp.Y);
                //    DrawAuxLine(canvas, firstPoint, newWCS_mp);
                //    DrawAuxLine(canvas, secondPoint, newWCS_mp2);
                //    DrawAuxLine(canvas, newWCS_mp, newWCS_mp2);
                //}
                //else
                //{
                var pt = Line2d.GetFootofperpendicular(wcs_mp, firstPoint, secondPoint);
                //向量
                var vector = new Vector2(wcs_mp.X - pt.X, wcs_mp.Y - pt.Y);

                Vector2 newWCS_mp = new Vector2(firstPoint.X + vector.X, firstPoint.Y + vector.Y);
                Vector2 newWCS_mp2 = new Vector2(secondPoint.X + vector.X, secondPoint.Y + vector.Y);
                DrawAuxLine(canvas, firstPoint, newWCS_mp);
                DrawAuxLine(canvas, secondPoint, newWCS_mp2);
                DrawAuxLine(canvas, newWCS_mp, newWCS_mp2);
                //}

            }
        }

        //public override void DrawTemp(SKCanvas canvas)
        //{
        //    for (var i = 1; i < this.points.Count; i++)
        //    {
        //        var pre = this.points[i - 1];
        //        var p = this.points[i];
        //        DrawLine(canvas, pre, p);
        //    }
        //}
        private void DrawAuxLine(SKCanvas canvas, Vector2 p0, Vector2 p1)
        {
            var sk_pre = this.vportRt.ConvertWcsToScr(p0).ToSKPoint();
            var sk_p = this.vportRt.ConvertWcsToScr(p1).ToSKPoint();
            //辅助元素的颜色 
            canvas.DrawLine(sk_pre, sk_p, new SKPaint { Color = this.vportRt.GetAuxColorValue(), IsStroke = true });
            //辅助曲线的颜色，包括辅助长度，辅助角度等
        }

        public override void Draw(SKCanvas canvas, LcElement element, Matrix3 matrix)
        {
            var dimAligned = element as DimAligned;
            var first = matrix.MultiplyPoint(dimAligned.First);
            var second = matrix.MultiplyPoint(dimAligned.Second);
            Vector2 third = new Vector2();
            third = matrix.MultiplyPoint(dimAligned.Third);
            var firstStart = this.vportRt.ConvertWcsToScr(first).ToSKPoint();
            var secondStart = this.vportRt.ConvertWcsToScr(second).ToSKPoint();
            var thirdStart = this.vportRt.ConvertWcsToScr(third).ToSKPoint();
            Vector2 firstE = new Vector2();
            Vector2 secondE = new Vector2();
            Vector2 thirdE = new Vector2();

            firstE = matrix.MultiplyPoint(dimAligned.GetVector2D(dimAligned.First));
            secondE = matrix.MultiplyPoint(dimAligned.GetVector2D(dimAligned.Second));

            //向量方向
            Vector2 directionF = new Vector2();
            directionF = new Vector2(dimAligned.First.X - dimAligned.GetVector2D(dimAligned.First).X, dimAligned.First.Y - dimAligned.GetVector2D(dimAligned.First).Y);
            double dis = 250;
            //按向量延长250距离
            Vector2 lidisptposF = firstE - (new Vector2(directionF.X * dis, directionF.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(directionF.X, 2.0) + Math.Pow(directionF.Y, 2.0))));

            Vector2 directionS = new Vector2();
            directionS = new Vector2(dimAligned.Second.X - dimAligned.GetVector2D(dimAligned.Second).X, dimAligned.Second.Y - dimAligned.GetVector2D(dimAligned.Second).Y);
            Vector2 lidisptposS = secondE - (new Vector2(directionS.X * dis, directionS.Y * dis)) / Math.Sqrt(Math.Abs((Math.Pow(directionS.X, 2.0) + Math.Pow(directionS.Y, 2.0))));

            //var pt = Line2d.GetFootofperpendicular(third, first, second);
            ////向量
            //var vector = new Vector2d(third.X - pt.X, third.Y - pt.Y);



            //thirdE = matrix.MultiplyPoint(new Vector2d(dimAligned.Second.X, dimAligned.Third.Y));
            var ArrowFirst = matrix.MultiplyPoint(dimAligned.GetVector2D(dimAligned.First));
            var ArrowSecond = matrix.MultiplyPoint(dimAligned.GetVector2D(dimAligned.Second));

            var firstEnd = this.vportRt.ConvertWcsToScr(lidisptposF).ToSKPoint();
            var secondEnd = this.vportRt.ConvertWcsToScr(lidisptposS).ToSKPoint();
            var AfterFirstEnd = this.vportRt.ConvertWcsToScr(ArrowFirst).ToSKPoint();
            var AfterSecondEnd = this.vportRt.ConvertWcsToScr(ArrowSecond).ToSKPoint();
            //var thirdEnd = this.vportRt.ConvertWcsToScr(thirdE).ToSKPoint();
            var path = DrawArrow(ArrowFirst, ArrowSecond);
            var path2 = DrawArrow(ArrowSecond, ArrowFirst);
            var pen = this.GetDrawPen(dimAligned);
            var eleAction = (dimAligned.Text.RtAction as ElementAction);
            dimAligned.Text.Start = dimAligned.Fifth;
            dimAligned.Text.Text = dimAligned.Length.ToString("0.00");
            eleAction.SetViewport(this.vportRt).Draw(canvas, dimAligned.Text, matrix);
            //文字宽度
            float textWidth = 50000;
            //if (pen.MeasureText(dimAligned.Length.ToString("0.00")) < 50)
                //textWidth = 50000;
            //else
            //    textWidth = pen.MeasureText(dimAligned.Length.ToString("0.00"));
            Vector2 textVector = new Vector2();
            textVector = new Vector2(ArrowFirst.X - ArrowSecond.X, ArrowFirst.Y - ArrowSecond.Y);
            Vector2 textEndPoint = ArrowSecond - (new Vector2(textVector.X * textWidth, textVector.Y * textWidth)) / Math.Sqrt(Math.Abs((Math.Pow(textVector.X, 2.0) + Math.Pow(textVector.Y, 2.0))));

            var textEnd = matrix.MultiplyPoint(textEndPoint);
            var textEndSKPoint = this.vportRt.ConvertWcsToScr(textEnd).ToSKPoint();

            if (pen == Constants.defaultPen)
            {
                //TODO:这里可以考虑将实线用颜色做KEY，对SKPaint进行缓存
                using (var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
                {
                    canvas.DrawLine(firstStart, firstEnd, elePen);
                    canvas.DrawLine(secondStart, secondEnd, elePen);
                    canvas.DrawLine(AfterFirstEnd, AfterSecondEnd, elePen);
                    #region 文字
                    //var fontManager = SKFontManager.Default;
                    //var emojiTypeface = fontManager.MatchCharacter('时');
                    //elePen.TextSize = (float)(this.vportRt.Viewport.Scale * 200);
                    ////paint.TextAlign = SKTextAlign.Right;
                    //elePen.IsAntialias = false;
                    ////elePen.Color = new SKColor(0x42, 0x81, 0xA4);
                    //elePen.IsStroke = false;
                    //elePen.Typeface = emojiTypeface;
                    //elePen.Style = SKPaintStyle.Fill;


                    //var skpath = new SKPath();
                    //skpath.MoveTo((AfterFirstEnd.X + AfterSecondEnd.X) / 2, (AfterFirstEnd.Y + AfterSecondEnd.Y) / 2);
                    //skpath.LineTo((float)textEndSKPoint.X, (float)textEndSKPoint.Y);

                    //canvas.DrawTextOnPath(dimAligned.Length.ToString("0.00"), skpath, 0, 0, elePen);
                    //canvas.DrawPath(path, elePen);
                    //canvas.DrawPath(path2, elePen);
                    #endregion
                    //canvas.DrawText(dimAligned.Length.ToString(), (firstEnd.X + secondEnd.X) / 2, (firstEnd.Y + secondEnd.Y) / 2, elePen);
                }
            }
            else
            {
                canvas.DrawLine(firstStart, firstEnd, pen);
                canvas.DrawLine(secondStart, secondEnd, pen);
                canvas.DrawLine(AfterFirstEnd, AfterSecondEnd, pen);
                #region 文字
                //var fontManager = SKFontManager.Default;
                //var emojiTypeface = fontManager.MatchCharacter('时');
                //pen.TextSize = (float)(this.vportRt.Viewport.Scale * 200);
                ////paint.TextAlign = SKTextAlign.Right;
                ////pen.IsAntialias = false;
                ////pen.Color = new SKColor(0x42, 0x81, 0xA4);
                ////pen.IsStroke = false;
                //pen.Typeface = emojiTypeface;
                ////pen.Style = SKPaintStyle.Fill;
                ////pen.IsAntialias = true;
                ////pen.StrokeWidth = 2;

                //var skpath = new SKPath();
                //skpath.MoveTo((AfterFirstEnd.X + AfterSecondEnd.X) / 2, (AfterFirstEnd.Y + AfterSecondEnd.Y) / 2);
                //skpath.LineTo((float)textEndSKPoint.X, (float)textEndSKPoint.Y);

                //canvas.DrawTextOnPath(dimAligned.Length.ToString("0.00"), skpath, 0, 0, pen);
                //canvas.DrawPath(path, pen);
                //canvas.DrawPath(path2, pen);
                #endregion
                //canvas.DrawText(dimAligned.Length.ToString(), (firstEnd.X + secondEnd.X) / 2, (firstEnd.Y + secondEnd.Y) / 2, pen);
            }

            #region

            //using (var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
            //{
            //    //elePen.Color = SKColors.Red;
            //    elePen.Style = SKPaintStyle.Fill;
            //    elePen.IsAntialias = true;
            //    elePen.StrokeWidth = 2;
            //    //绘制路径
            //    canvas.DrawPath(path, elePen);
            //    canvas.DrawPath(path2, elePen);
            //}

            //using (var elePen = new SKPaint { Color = new SKColor(element.GetColorValue()), IsStroke = true })
            //{
            //    //elePen.Color = SKColors.Red;
            //    elePen.Style = SKPaintStyle.Fill;
            //    elePen.IsAntialias = true;
            //    elePen.StrokeWidth = 2;
            //    //绘制路径x
            //    canvas.DrawPath(path2, elePen);
            //}
            #endregion
        }

        public override ControlGrip[] GetControlGrips(LcElement element)
        {
            var dimAligned = element as DimAligned; //getHel
            var grips = new List<ControlGrip>();
            var gripFirst = new ControlGrip
            {
                Element = dimAligned,
                Name = "First",
                Position = dimAligned.First
            };
            grips.Add(gripFirst);
            var gripSecond = new ControlGrip
            {
                Element = dimAligned,
                Name = "Second",
                Position = dimAligned.Second
            };
            grips.Add(gripSecond);
            ControlGrip gripThird = new ControlGrip();
            gripThird = new ControlGrip
            {
                Element = dimAligned,
                Name = "Third",
                Position = dimAligned.GetVector2D(dimAligned.First)
            };
            grips.Add(gripThird);
            ControlGrip gripFourth = new ControlGrip();
            gripFourth = new ControlGrip
            {
                Element = dimAligned,
                Name = "Fourth",
                Position = dimAligned.GetVector2D(dimAligned.Second)
            };
            grips.Add(gripFourth);
            ControlGrip gripFifth = new ControlGrip();
            gripFifth = new ControlGrip
            {
                Element = dimAligned,
                Name = "Fifth",
                Position = new Vector2((dimAligned.GetVector2D(dimAligned.First).X + dimAligned.GetVector2D(dimAligned.Second).X) / 2, (dimAligned.GetVector2D(dimAligned.First).Y + dimAligned.GetVector2D(dimAligned.Second).Y) / 2)
            };
            grips.Add(gripFifth);
            return grips.ToArray();
        }

        //public override void CreateElement(LcElement element, Matrix3d matrix)
        //{
        //    var line = element as LcLine;
        //    this.vportRt.ActiveElementSet.AddLine(matrix.MultiplyPoint(line.Start), matrix.MultiplyPoint(line.End));
        //}

        //public override void CreateElement(LcElement element, Vector2d basePoint, double scaleFactor)
        //{
        //    var line = element as LcLine;
        //    Matrix3d matrix3 = Matrix3d.Scale(scaleFactor, basePoint);
        //    this.vportRt.ActiveElementSet.AddLine(matrix3.MultiplyPoint(line.Start), matrix3.MultiplyPoint(line.End));
        //}

        //public override void CreateElement(LcElement element, Vector2d basePoint, Vector2d scaleVector)
        //{
        //    var line = element as LcLine;
        //    Matrix3d matrix3 = Matrix3d.Scale(scaleVector, basePoint);
        //    this.vportRt.ActiveElementSet.AddLine(matrix3.MultiplyPoint(line.Start), matrix3.MultiplyPoint(line.End));
        //}

        private string _gripName;

        private Vector2 _position;

        private DimAligned _dimAligned;

        public override void SetDragGrip(LcElement element, string gripName, Vector2 position, bool isEnd)
        {
            var dimAligned = element as DimAligned;
            _dimAligned = dimAligned;
            if (!isEnd)
            {
                _gripName = gripName;
                _position = position;
            }
            else
            {
                if (gripName == "First")
                    dimAligned.Set(first: position);
                else if (gripName == "Second")
                {
                    dimAligned.Set(second: position);
                }
                else if (gripName == "Third" || gripName == "Fourth" || gripName == "Fifth")
                {
                    dimAligned.Set(third: position);
                }
            }
            //  DocumentManager.CurrentRecorder.EndAction();
        }

        public override void DrawDragGrip(SKCanvas canvas)
        {
            if (_dimAligned == null) return;




            var first = this.vportRt.ConvertWcsToScr(_dimAligned.First);
            if (_gripName == "First")
                first = this.vportRt.ConvertWcsToScr(_position);

            var second = this.vportRt.ConvertWcsToScr(_dimAligned.Second);
            if (_gripName == "Second")
                second = this.vportRt.ConvertWcsToScr(_position);

            var third = this.vportRt.ConvertWcsToScr(_dimAligned.Third);

            var pt = Line2d.GetFootofperpendicular(third, first, second);
            //向量
            var vector = new Vector2(third.X - pt.X, third.Y - pt.Y);

            var fifth = this.vportRt.ConvertWcsToScr(new Vector2((first.X + vector.X + second.X + vector.X) / 2, (first.Y + vector.Y + second.Y + vector.Y) / 2));
            if (_gripName == "Third" || _gripName == "Fourth" || _gripName == "Fifth")
            {
                fifth = this.vportRt.ConvertWcsToScr(_position);
                if (_gripName == "Third")
                    canvas.DrawLine(fifth.ToSKPoint(), new Vector2(first.X + vector.X, first.Y + vector.Y).ToSKPoint(), Constants.draggingPen);
                if (_gripName == "Fourth")
                    canvas.DrawLine(fifth.ToSKPoint(), new Vector2(second.X + vector.X, second.Y + vector.Y).ToSKPoint(), Constants.draggingPen);
                if (_gripName == "Fifth")
                    canvas.DrawLine(fifth.ToSKPoint(), new Vector2((first.X + vector.X + second.X + vector.X) / 2, (first.Y + vector.Y + second.Y + vector.Y) / 2).ToSKPoint(), Constants.draggingPen);
                //移动后需要求新的向量
                var newPt = Line2d.GetFootofperpendicular(fifth, first, second);
                var newVector = new Vector2(fifth.X - newPt.X, fifth.Y - newPt.Y);

                canvas.DrawLine(first.ToSKPoint(), new Vector2(first.X + newVector.X, first.Y + newVector.Y).ToSKPoint(), Constants.draggingPen);
                canvas.DrawLine(second.ToSKPoint(), new Vector2(second.X + newVector.X, second.Y + newVector.Y).ToSKPoint(), Constants.draggingPen);
                canvas.DrawLine(new Vector2(first.X + newVector.X, first.Y + newVector.Y).ToSKPoint(), new Vector2(second.X + newVector.X, second.Y + newVector.Y).ToSKPoint(), Constants.draggingPen);
            }
            if (_gripName == "Second" || _gripName == "First")
            {
                if (_gripName == "First")
                    canvas.DrawLine(first.ToSKPoint(), this.vportRt.ConvertWcsToScr(_dimAligned.First).ToSKPoint(), Constants.draggingPen);
                else
                    canvas.DrawLine(second.ToSKPoint(), this.vportRt.ConvertWcsToScr(_dimAligned.Second).ToSKPoint(), Constants.draggingPen);
                canvas.DrawLine(first.ToSKPoint(), new Vector2(first.X + vector.X, first.Y + vector.Y).ToSKPoint(), Constants.draggingPen);
                canvas.DrawLine(second.ToSKPoint(), new Vector2(second.X + vector.X, second.Y + vector.Y).ToSKPoint(), Constants.draggingPen);
                canvas.DrawLine(new Vector2(first.X + vector.X, first.Y + vector.Y).ToSKPoint(), new Vector2(second.X + vector.X, second.Y + vector.Y).ToSKPoint(), Constants.draggingPen);
            }
        }
        private SKPath DrawArrow(Vector2 start, Vector2 end, double extraAngle = 0)
        {
            LcLine lcline = new LcLine();
            lcline.Start = start;
            lcline.End = end;
            var radian = lcline.Angle;

            var startve = new Vector2(lcline.Start.X, lcline.Start.Y);
            var arrowtwove = new Vector2(lcline.Start.X + 100, lcline.Start.Y + 30);
            var arrowendve = new Vector2(lcline.Start.X + 100, lcline.Start.Y - 30);
            var angle = Utils.RadianToDegree(radian) + extraAngle;
            arrowtwove = Vector2.Rotate(arrowtwove, startve, angle);
            arrowendve = Vector2.Rotate(arrowendve, startve, angle);

            var pointone = this.vportRt.ConvertWcsToScr(startve).ToSKPoint();
            var pointtwo = this.vportRt.ConvertWcsToScr(arrowtwove).ToSKPoint();
            var pointthree = this.vportRt.ConvertWcsToScr(arrowendve).ToSKPoint();

            var path = new SKPath();
            path.FillType = SKPathFillType.EvenOdd;
            //外圈 顺时针
            path.MoveTo((float)pointone.X, (float)pointone.Y);    //起点
            path.LineTo((float)pointtwo.X, (float)pointtwo.Y);
            path.LineTo((float)pointthree.X, (float)pointthree.Y);
            path.Close();
            return path;
        }
        //public override List<PropertyObserver> GetPropertyObservers()
        //{
        //    return new List<PropertyObserver>()
        //               {
        //                   new PropertyObserver()
        //                       {
        //                           Name = "StartX",
        //                           DisplayName = "起点 X 坐标",
        //                           Getter = (ele) => (ele as DimRotated).Start.X,
        //                           Setter = (ele, value) =>
        //                               {
        //                                   var line = (ele as DimRotated);
        //                                   var x = Convert.ToDouble(value);
        //                                   var sp = new Vector2d(x, line.Start.Y);
        //                                   line.Set(start: sp);
        //                               }
        //                       },
        //                   new PropertyObserver()
        //                       {
        //                           Name = "StartY",
        //                           DisplayName = "起点 Y 坐标",
        //                           Getter = (ele) => (ele as DimRotated).Start.Y,
        //                           Setter = (ele, value) =>
        //                               {
        //                                   var line = (ele as DimRotated);
        //                                   var y = Convert.ToDouble(value);
        //                                   var sp = new Vector2d(line.Start.X, y);
        //                                   line.Set(start: sp);
        //                               }
        //                       },
        //                   new PropertyObserver()
        //                       {
        //                           Name = "EndX",
        //                           DisplayName = "端点 X 坐标",
        //                           Getter = (ele) => (ele as LcLine).End.X,
        //                           Setter = (ele, value) =>
        //                               {
        //                                   var line = (ele as LcLine);
        //                                   var x = Convert.ToDouble(value);
        //                                   var ep = new Vector2d(x, line.End.Y);
        //                                   line.Set(end: ep);
        //                               }
        //                       },
        //                   new PropertyObserver()
        //                       {
        //                           Name = "EndY",
        //                           DisplayName = "端点 Y 坐标",
        //                           Getter = (ele) => (ele as LcLine).End.Y,
        //                           Setter = (ele, value) =>
        //                               {
        //                                   var line = (ele as LcLine);
        //                                   var y = Convert.ToDouble(value);
        //                                   var ep = new Vector2d(line.End.X, y);
        //                                   line.Set(end: ep);
        //                               }
        //                       },
        //                   new PropertyObserver()
        //                       {
        //                           Name = "DeltaX",
        //                           DisplayName = "增量X",
        //                           Getter = (ele) => (ele as LcLine).DeltaX
        //                       },
        //                   new PropertyObserver()
        //                       {
        //                           Name = "DeltaY",
        //                           DisplayName = "增量Y",
        //                           Getter = (ele) => (ele as LcLine).DeltaY
        //                       },
        //                   new PropertyObserver()
        //                       {
        //                           Name = "Length",
        //                           DisplayName = "长度",
        //                           Getter = (ele) => (ele as LcLine).Length
        //                       },
        //                   new PropertyObserver()
        //                       {
        //                           Name = "Angle",
        //                           DisplayName = "角度",
        //                           Getter = (ele) => (ele as LcLine).Angle * 180 / Math.PI
        //                       }
        //               };
        //}
    }
}
