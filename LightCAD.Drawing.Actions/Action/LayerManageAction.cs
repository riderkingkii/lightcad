﻿using LightCAD.Core.Elements;
using LightCAD.Core;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using LightCAD.Runtime.Interface;

namespace LightCAD.Drawing.Actions
{
    public class LayerManageAction
    {
        protected DocumentRuntime docRt;
        public LcDocument lcDocument;
        public ViewportRuntime vportRt;
        protected IDocumentEditor docEditor;
        public LayerManageAction(IDocumentEditor docEditor)
        {
            this.docEditor= docEditor;
            this.docRt = docEditor.DocRt;
            this.vportRt = (docEditor as DrawingEditRuntime).ActiveViewportRt;
            this.lcDocument = docRt.Document;
        }

        public async void ExecCreate(string[] args)
        {
            var pointInputer = new PointInputer(this.docEditor);
            var elementSetInputer = new ElementSetInputer(this.docEditor);
            if (this.lcDocument.Layers.Count < 2)
            {
                int index = this.vportRt.ActiveElementSet.Elements.Count;
                //方便调试增加一个默认单体和楼层
                //LcLayer lcLayer = lcDocument.CreateObject<LcLayer>();
                //lcLayer.Name = "默认";
                for (int i = 0; i < index; i++)
                {
                    if (this.lcDocument.Layers.Where(m => m.Name == this.vportRt.ActiveElementSet.Elements[i].Layer).Count() > 0)
                        continue;
                    LcLayer LcLayer = this.lcDocument.CreateObject<LcLayer>();
                    //LcLayer.Description = true;
                    LcLayer.Name = this.vportRt.ActiveElementSet.Elements[i].Layer ;
                    LcLayer.IsOff = false;
                    LcLayer.IsFrozen = false;
                    LcLayer.IsLocked = false;
                    LcLayer.Color = 0xFF00FF00;
                    LcLayer.LineTypeId = 1;
                    LcLayer.LineWeight = LineWeight.ByLayer;
                    LcLayer.Transparency = 0;
                    LcLayer.LineTypeName = "BYLAYER";
                    //LcLayer.PrintStyle = "Color" + i;
                    //LcLayer.IsPrint = true;
                    LcLayer.ViewportVisibilityDefault = 0;
                    LcLayer.Description = "";
                    this.lcDocument.Layers.Add(LcLayer);
                }

                //this.lcDocument.LineTypes.Clear();
                ////添加默认的线型
                //byte[] buffer = Guid.NewGuid().ToByteArray();
                //int lcid = (int)BitConverter.ToInt64(buffer, 0);
                //var linetypelist = new List<LcLineType>() {
                //    new LcLineType() { Id=lcid,LineTypeName="CENTER",LineTypeExplain="Center",LineTypeDefinition="A,1.25,-.25,.25,-.25" },
                //};
                //foreach (var item in linetypelist)
                //{
                //    this.lcDocument.LineTypes.Add(item);
                //}
            }
            var win = (ILayerManageWindow)AppRuntime.UISystem.CreateWindow("LayerManageWindow", this);
            var result = AppRuntime.UISystem.ShowDialog(win);
            if (result == LcDialogResult.OK)
            {
                //lcBuilding = win.lcBuilding;
            }
        }

    }
}
