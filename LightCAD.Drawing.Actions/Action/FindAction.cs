﻿using LightCAD.Core;
using LightCAD.Runtime.Interface;

namespace LightCAD.Drawing.Actions.Action
{
    public class FindAction
    {
        protected DocumentRuntime docRt;
        public LcDocument lcDocument;
        public ViewportRuntime vportRt;
        protected IDocumentEditor docEditor;

        public FindAction(IDocumentEditor docEditor)
        {
            this.docEditor = docEditor;
            this.docRt = docEditor.DocRt;
            this.vportRt = (docEditor as DrawingEditRuntime).ActiveViewportRt;
            this.lcDocument = docRt.Document;
        }

        public async void ExecCreate(string[] args)
        {
            var win = (IFindWindow)AppRuntime.UISystem.CreateWindow("FindWindow", this);
            var result = AppRuntime.UISystem.ShowDialog(win);
            if (result == LcDialogResult.OK)
            {
            }
        }
    }
}
