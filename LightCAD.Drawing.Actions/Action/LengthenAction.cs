﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using static LightCAD.Core.Elements.LcPolyLine;

namespace LightCAD.Drawing.Actions.Action
{
    public class LengthenAction : ITransformDrawer
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;
        private string CurrentTransform;

        //public LengthenAction(IDocumentEditor docEditor) : base(docEditor)
        //{
        //    docEditor.CommandCenter.WriteInfo("命令 ：LEN");
        //}

        public virtual void DrawLengthen(SKCanvas canvas)
        {
            switch (this.CurrentTransform)
            {
                case " LENGTHEN": DrawLengthen(canvas); break;

            }
        }





        protected DocumentRuntime docRt;
        protected ViewportRuntime vportRt;
        protected IDocumentEditor docEditor;
        protected ICommandControl commandCtrl;



        private ElementSetInputer ElementInputers { get; set; }
        private ElementInputer ElementInputer { get; set; }
        private CmdTextInputer CmdTextInputer { get; set; }
        private PointInputer PointInputer { get; set; }
        public LengthenAction() { }
        private PointInputer Inputer { get; set; }

        private string _methodName;
        private string _methodArg;



        public LengthenAction(IDocumentEditor docEditor)
        {
            this.docEditor = docEditor;
            this.docRt = docEditor.DocRt;
            this.vportRt = (docEditor as DrawingEditRuntime).ActiveViewportRt;
            this.commandCtrl = this.vportRt.CommandCtrl;
            this.commandCtrl.WriteInfo("命令 ：LEN");
        }


        private LcCreateMethod GetMethod(string method)
        {
            if (method == null) return CreateMethods[0];
            var getted = CreateMethods.FirstOrDefault((m) => m.Name == method);
            if (getted == null)
                return CreateMethods[0];
            else
                return getted;
        }




        private double Add { get; set; }
        private double AddLength { get; set; }
        private double AddAngle { get; set; }
        private double Length { get; set; }
        private double Angle { get; set; }
        private double Lpercentage { get; set; }
        private Vector2? NewPoint { get; set; }
        private Vector2? Newlineendpoint { get; set; }




        private List<Curve2d> SelectedElements;

        private List<LcElement> selectedElements;
        private List<LcElement> selectedEXElements;


        private Line2d BeginLine;
        private Line2d ResultLine;
        private Arc2d BeginArc;
        private Arc2d ResultArc;
        private Curve2d BeginCurve;
        private Curve2d ResultCurve;



        public void StartAction()
        {
            if (vportRt.CheckStatus(ViewportStatus.InAction))
            {
                vportRt.CancelCurrentAction();
            }
            vportRt.SetStatus(ViewportStatus.InAction);
            this.vportRt.CommandCenter.InAction = true;
        }
        public void EndAction()
        {
            this.vportRt.SetTransformDrawer(null);
            vportRt.ClearStatus();
            this.commandCtrl.Prompt(string.Empty);

            this.vportRt.CurrentAction = null;
            this.vportRt.CursorType = LcCursorType.SelectElement;
            this.vportRt.CursorAddon = CursorAddonType.None;
            this.vportRt.CommandCenter.InAction = false;
            this.vportRt.CancelCurrentAction();
            if (this.docRt.Action.SelectedElements.Count > 0)
            {
                this.docRt.Action.SetSelectsToDragging(false);
                this.vportRt.ClearElementGrips();
                this.docRt.Action.ClearSelects();
            }
        }

        public void Cancel()
        {

            this.EndAction();
        }






      
        //代码修改演示
        //代码修改演示

        static LengthenAction()
        {
            CommandName = "LENGTHEN";
            CreateMethods = new LcCreateMethod[1];
            CreateMethods[0] = new LcCreateMethod()
            {
                Name = "LEN",
                Description = "拉长",
                Steps = new LcCreateStep[]
                {

                    new LcCreateStep{ Name="Step0", Options="LENGTHEN 选择要测量的对象或[增量（DE）百分比（P）总计（T）动态（DY）]:" },
                    new LcCreateStep { Name="Step1", Options= "LENGTHEN 选择要修改的对象或[放弃（U）]:" },

                    new LcCreateStep { Name="DE-Step0", Options= "LENGTHEN 输入长度增量或[角度（A）]:" },
                    new LcCreateStep { Name="DE-Step1", Options= "LENGTHEN 输入角度增量:" },

                    new LcCreateStep { Name="P-Step0", Options= "LENGTHEN 输入长度百分数:" },

                    new LcCreateStep { Name="T-Step0", Options= "LENGTHEN 指定总长度或[角度（A）]:" },
                    new LcCreateStep { Name="T-Step1", Options= "LENGTHEN 指定总角度:" },

                    new LcCreateStep { Name="DY-Step0", Options= "LENGTHEN 指定新端点:" },

                }
            };
        }




        public async void LengthenTransform(string[] args = null)
        {

            this.StartAction();

            var curMethod = CreateMethods[0];
            this.ElementInputers = new ElementSetInputer(this.docEditor);
            this.CmdTextInputer = new CmdTextInputer(this.docEditor);
            this.PointInputer = new PointInputer(this.docEditor);


        #region  Step0:分支开头
        Step0:
            var step0 = curMethod.Steps[0];
            var result0 = await PointInputer.Execute(step0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            if (result0.ValueX != null) { return; }
            if (result0.ValueX == null)
            {
                if (result0.Option.ToUpper() == "DE")
                {
                    goto Step1;

                }

                if (result0.Option.ToUpper() == "P")
                {
                    goto Step2;

                }


                if (result0.Option.ToUpper() == "T")
                {
                    goto Step3;

                }


                if (result0.Option.ToUpper() == "DY")
                {
                    goto Step4;

                }


            }
        #endregion


        #region  Step1:选择DE
        Step1:
            var DEStep0 = curMethod.Steps[2];
            var DEresult0 = await PointInputer.Execute(DEStep0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            if (DEresult0.ValueX != null) { return; }
            if (DEresult0.ValueX == null)
            {
                if (DEresult0.Option != null)
                {
                    if (double.TryParse(DEresult0.Option, out double optionValue))
                    {
                        AddLength = optionValue;

                        //鼠标悬停，并判断悬停对象 1是否有效对象 2什么对象
                        //记录悬停对象


                        //临时渲染


                        //执行拉长

                        this.vportRt.SetTransformDrawer(this);     //开启渲染
                        var stepDE1 = curMethod.Steps[1];
                        var resultDE1 = await ElementInputers.Execute(stepDE1.Options);
                        if (resultDE1 == null || resultDE1.IsCancelled) { this.Cancel(); goto End; }

                        //if (!this.vportRt.CheckStatus(ViewportStatus.RectSelect))//单个元素的点击状态(如果是单个元素被点击）
                        //{
                        //    var wcsp = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());//指针移动到的坐标转换为点之后转换为世界坐标
                        //                                                                                            ////this.inputp = wcsp;
                        //    var wsize = this.vportRt.ConvertScrToWcs(Constants.SelectBoxSize);   //盒子的宽度
                        //    var hitResult = this.vportRt.HitSelect(this.docRt.Document.ModelSpace, wcsp, wsize);


                        //}



                        this.selectedElements = new List<LcElement>();
                        selectedElements.AddRange((List<LcElement>)resultDE1.ValueX);


                        DELengthen(AddLength, selectedElements);

                        goto End;

                        #region 学习代码（废）
                        //var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());  //记录鼠标的悬停对象
                        //this.selectedElements = new List<LcElement>();

                        //foreach (LcElement item in this.vportRt.DocEditor.DocRt.Document.ModelSpace.Elements)
                        //{
                        //    if (item.PointInElement(wcsp1))
                        //    {
                        //        selectedElements.Add(item);
                        //    }
                        //}

                        //SelectedElements = selectedElements.OfType<Curve2d>().ToList();


                        //ResultCurve=DELengthen(AddLength, SelectedElements);   //传入的都是鼠标悬停对象

                        ////实时渲染

                        //goto End;
                        #endregion
                    }


                    if (DEresult0.Option.ToUpper() == "A")
                    {
                        var DEStep1 = curMethod.Steps[3];
                        var DEresult1 = await PointInputer.Execute(DEStep1.Options);
                        if (PointInputer.isCancelled) { this.Cancel(); return; }
                        if (double.TryParse(DEresult1.Option, out double Value))
                        {
                            AddAngle = Value;

                            this.vportRt.SetTransformDrawer(this);     //开启渲染
                            var stepDE1 = curMethod.Steps[1];
                            var resultDE1 = await ElementInputers.Execute(stepDE1.Options);
                            if (resultDE1 == null || resultDE1.IsCancelled) { this.Cancel(); goto End; }


                            List<LcElement> result = new List<LcElement>();
                            result.AddRange((List<LcElement>)resultDE1.ValueX);


                            DEALengthen(AddAngle, result);

                            goto End;








                        }
                        else { return; }


                    }
                    if (DEresult0.Option.ToUpper() != "A" && !double.TryParse(DEresult0.Option, out double OValue))
                    { this.Cancel(); goto End; }

                }
            }
        #endregion


        #region  Step2:选择P
        Step2:
            var PStep0 = curMethod.Steps[4];
            var Presult0 = await PointInputer.Execute(PStep0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            if (Presult0.ValueX != null) { return; }
            if (Presult0.ValueX == null)
            {
                if (double.TryParse(Presult0.Option, out double optionValue))
                {
                    Lpercentage = optionValue / 100;


                    this.vportRt.SetTransformDrawer(this);     //开启渲染
                    var stepDE1 = curMethod.Steps[1];
                    var resultDE1 = await ElementInputers.Execute(stepDE1.Options);
                    if (resultDE1 == null || resultDE1.IsCancelled) { this.Cancel(); goto End; }


                    List<LcElement> result = new List<LcElement>();
                    result.AddRange((List<LcElement>)resultDE1.ValueX);


                    PLengthen(Lpercentage, result);

                    goto End;


                }
                if (!double.TryParse(Presult0.Option, out double Value))
                {
                    this.Cancel(); goto End;
                }
            }
        #endregion


        #region  Step3:选择T
        Step3:
            var TStep0 = curMethod.Steps[5];
            var Tresult0 = await PointInputer.Execute(TStep0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            if (Tresult0.ValueX != null) { return; }
            if (Tresult0.ValueX == null)
            {
                if (Tresult0.Option != null)
                {
                    if (double.TryParse(Tresult0.Option, out double optionValue))
                    {
                        Length = optionValue;

                        this.vportRt.SetTransformDrawer(this);     //开启渲染
                        var stepDE1 = curMethod.Steps[1];
                        var resultDE1 = await ElementInputers.Execute(stepDE1.Options);
                        if (resultDE1 == null || resultDE1.IsCancelled) { this.Cancel(); goto End; }


                        List<LcElement> result = new List<LcElement>();
                        result.AddRange((List<LcElement>)resultDE1.ValueX);


                        TLengthen(Length, result);

                        goto End;


                    }
                    if (Tresult0.Option.ToUpper() == "A")
                    {
                        var TStep1 = curMethod.Steps[6];
                        var Tresult1 = await PointInputer.Execute(TStep1.Options);
                        if (PointInputer.isCancelled) { this.Cancel(); return; }
                        if (double.TryParse(Tresult1.Option, out double Value))
                        {
                            Angle = Value;

                            //鼠标悬停，并判断悬停对象 1是否有效对象 2什么对象
                            //记录悬停对象结果

                            this.vportRt.SetTransformDrawer(this);     //开启渲染
                            var stepDE1 = curMethod.Steps[1];
                            var resultDE1 = await ElementInputers.Execute(stepDE1.Options);
                            if (resultDE1 == null || resultDE1.IsCancelled) { this.Cancel(); goto End; }


                            List<LcElement> result = new List<LcElement>();
                            result.AddRange((List<LcElement>)resultDE1.ValueX);


                            TALengthen(Angle, result);

                            goto End;

                        }
                        else { return; }


                    }
                    if (Tresult0.Option.ToUpper() != "A" && !double.TryParse(Tresult0.Option, out double OValue))
                    { this.Cancel(); goto End; }

                }
            }

        #endregion




        #region  Step4:选择Dy
        Step4:
            var DYStep0 = curMethod.Steps[7];
            var DYresult0 = await PointInputer.Execute(DYStep0.Options);
            if (PointInputer.isCancelled) { this.Cancel(); return; }
            if (DYresult0.ValueX != null)
            {
                NewPoint = (Vector2)DYresult0.ValueX;

                //要加个判断，判断移动点是否在元素的延长线上等



                //鼠标悬停，并判断悬停对象 1是否有效对象 2什么对象
                //记录悬停对象结果

                var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
                this.selectedElements = new List<LcElement>();

                foreach (LcElement item in this.vportRt.DocEditor.DocRt.Document.ModelSpace.Elements)
                {
                    if (item.PointInElement(wcsp1))
                    {
                        selectedElements.Add(item);
                    }
                }

                DYLengthen(NewPoint, SelectedElements);
                goto End;
            }

        #endregion

        #region  End
        End:
            this.EndAction();



        }
        #endregion






        //static LcElement


        public void DELengthen(double length, List<LcElement> element1)        //长度增量拉长 (对象包括直线，多段线，圆弧）（判断哪个端点，哪边拉长）
        {

            if (element1 != null)
            {
                foreach (var element11 in element1)
                {
                    if (element11 is LcLine)
                    {
                        LcLine line = (LcLine)element11;



                        var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
                        double diatance3 = Vector2.Distance(wcsp1, line.Start);
                        double diatance4 = Vector2.Distance(wcsp1, line.End);
                        if (diatance3 < diatance4)
                        {
                            Vector2 firstPoint = line.Start;
                            Vector2 secondPoint = line.End;

                            line.Set(start: secondPoint, end: firstPoint);

                        }




                        // 处理线段

                        Vector2 startPoint = line.Start;
                        Vector2 endPoint = line.End;

                        // 计算线段的当前长度
                        double currentLength = line.Length;

                        // 计算新的长度
                        double newLength = currentLength + length;

                        double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                        // 计算直线方程中的截距 b
                        double b = startPoint.Y - k * startPoint.X;



                        if (endPoint.X > startPoint.X)
                        // 根据新的长度计算新的终点 x 坐标
                        {
                            double newX = startPoint.X + newLength / Math.Sqrt(1 + k * k);


                            // 根据 x 坐标计算新的终点 y 坐标
                            double newY = k * newX + b;

                            // 创建新的终点坐标
                            Vector2 newEndPoint = new Vector2((float)newX, (float)newY);




                            line.Set(end: newEndPoint);
                            //return line;
                        }

                        else if (endPoint.X < startPoint.X)
                        {
                            double newX = startPoint.X - newLength / Math.Sqrt(1 + k * k);


                            // 根据 x 坐标计算新的终点 y 坐标
                            double newY = k * newX + b;

                            // 创建新的终点坐标
                            Vector2 newEndPoint = new Vector2((float)newX, (float)newY);




                            line.Set(end: newEndPoint);
                        }

                        else if (endPoint.X == startPoint.X)
                        {

                            if (wcsp1.Y > endPoint.Y)
                            {
                                double newY = endPoint.Y - length;
                                Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                line.Set(end: newEndPoint);


                            }
                            if (wcsp1.Y < endPoint.Y)
                            {
                                double newY = endPoint.Y + length;
                                Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                line.Set(end: newEndPoint);

                            }
                        }

                        else if (endPoint.Y == startPoint.Y)
                        {
                            if (wcsp1.X > endPoint.X)
                            {
                                double newX = endPoint.X - length;
                                Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                line.Set(end: newEndPoint);


                            }
                            if (wcsp1.X < endPoint.X)
                            {
                                double newX = endPoint.X + length;
                                Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                line.Set(end: newEndPoint);


                            }


                        }


                        }


                    else if (element11 is LcPolyLine)
                    {
                        LcPolyLine polyline = (LcPolyLine)element11;

                        // 处理多段线
                        // 计算多段线的新顶点
                        if (polyline.Curve2ds.Count > 1)
                        {
                            // 如果多段线有多于一段的线段

                            var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
                            double diatance3 = Vector2.Distance(wcsp1, LcPolyLine.GetStart(polyline.Curve2ds[0]));
                            double diatance4 = Vector2.Distance(wcsp1, LcPolyLine.GetEnd(polyline.Curve2ds[polyline.Curve2ds.Count - 1]));
                            if (diatance3 < diatance4)//点击点离开始点更近
                            {
                                for (int i = 0; i < polyline.Curve2ds.Count; i++)//多段线每段线起点终点互换，不只换第一段，是怕多段线发生其它错误
                                {
                                    Vector2 firstPoint = LcPolyLine.GetStart(polyline.Curve2ds[i]);
                                    Vector2 secondPoint = LcPolyLine.GetEnd(polyline.Curve2ds[i]);

                                    (polyline.Curve2ds[i] as Line2d).Start = secondPoint;
                                    (polyline.Curve2ds[i] as Line2d).End = firstPoint;
                                    
                                }
                                Vector2 startPoint = LcPolyLine.GetStart(polyline.Curve2ds[0]);
                                Vector2 endPoint = LcPolyLine.GetEnd(polyline.Curve2ds[0]);

                                // 计算线段的当前长度
                                double currentLength = Vector2.Distance(startPoint, endPoint);

                                // 计算新的长度
                                double newLength = currentLength + length;

                                double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                                // 计算直线方程中的截距 b
                                double b = startPoint.Y - k * startPoint.X;


                                if (endPoint.X > startPoint.X)//最后一段线的终点在右边

                                // 根据新的长度计算新的终点 x 坐标
                                {
                                    double newX = startPoint.X + newLength / Math.Sqrt(1 + k * k);

                                    // 根据 x 坐标计算新的终点 y 坐标
                                    double newY = k * newX + b;

                                    // 创建新的终点坐标
                                    Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                    // 更新多段线的最后一段线段
                                    (polyline.Curve2ds[0] as Line2d).End = newEndPoint;

                                }


                                else if (endPoint.X < startPoint.X) //最后一段线的终点在左边

                                // 根据新的长度计算新的终点 x 坐标
                                {
                                    double newX = startPoint.X - newLength / Math.Sqrt(1 + k * k);

                                    // 根据 x 坐标计算新的终点 y 坐标
                                    double newY = k * newX + b;

                                    // 创建新的终点坐标
                                    Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                    // 更新多段线的最后一段线段
                                    (polyline.Curve2ds[0] as Line2d).End = newEndPoint;

                                }

                                else if (endPoint.X == startPoint.X)
                                {

                                    if (wcsp1.Y > endPoint.Y)
                                    {
                                        double newY = endPoint.Y - length;
                                        Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                        (polyline.Curve2ds[0] as Line2d).End = newEndPoint;


                                    }
                                    if (wcsp1.Y < endPoint.Y)
                                    {
                                        double newY = endPoint.Y + length;
                                        Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                        (polyline.Curve2ds[0] as Line2d).End = newEndPoint;

                                    }
                                }

                                else if (endPoint.Y == startPoint.Y)
                                {
                                    if (wcsp1.X > endPoint.X)
                                    {
                                        double newX = endPoint.X - length;
                                        Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                        (polyline.Curve2ds[0] as Line2d).End = newEndPoint;


                                    }
                                    if (wcsp1.X < endPoint.X)
                                    {
                                        double newX = endPoint.X + length;
                                        Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                        (polyline.Curve2ds[0] as Line2d).End = newEndPoint;


                                    }


                                }



                            }
                                
                          

                            else if (diatance3 > diatance4)


                            {
                                Vector2 startPoint = LcPolyLine.GetStart(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);
                                Vector2 endPoint = LcPolyLine.GetEnd(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);

                                // 计算线段的当前长度
                                double currentLength = Vector2.Distance(startPoint, endPoint);

                                // 计算新的长度
                                double newLength = currentLength + length;

                                double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                                // 计算直线方程中的截距 b
                                double b = startPoint.Y - k * startPoint.X;


                                if (endPoint.X > startPoint.X)

                                // 根据新的长度计算新的终点 x 坐标
                                {
                                    double newX = startPoint.X + newLength / Math.Sqrt(1 + k * k);

                                    // 根据 x 坐标计算新的终点 y 坐标
                                    double newY = k * newX + b;

                                    // 创建新的终点坐标
                                    Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                    // 更新多段线的最后一段线段
                                    (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;

                                }

                                else if (endPoint.X < startPoint.X)

                                // 根据新的长度计算新的终点 x 坐标
                                {
                                    double newX = startPoint.X - newLength / Math.Sqrt(1 + k * k);

                                    // 根据 x 坐标计算新的终点 y 坐标
                                    double newY = k * newX + b;

                                    // 创建新的终点坐标
                                    Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                    // 更新多段线的最后一段线段
                                    (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;

                                }


                                else if (endPoint.X == startPoint.X)
                                {

                                    if (wcsp1.Y > endPoint.Y)
                                    {
                                        double newY = endPoint.Y - length;
                                        Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                        (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;


                                    }
                                    if (wcsp1.Y < endPoint.Y)
                                    {
                                        double newY = endPoint.Y + length;
                                        Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                        (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;

                                    }
                                }

                                else if (endPoint.Y == startPoint.Y)
                                {
                                    if (wcsp1.X > endPoint.X)
                                    {
                                        double newX = endPoint.X - length;
                                        Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                        (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;


                                    }
                                    if (wcsp1.X < endPoint.X)
                                    {
                                        double newX = endPoint.X + length;
                                        Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                        (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;


                                    }


                                }




                            }
                            //return polyline;
                        }

                    }



              
                   else if (element11 is LcArc)
                    {
                        LcArc arc = (LcArc)element11;
                        var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
                        // 获取圆弧的属性
                        Vector2 center = arc.Center;
                        Vector2 start = arc.Startp;
                        Vector2 end = arc.Endp;
                        double radius = arc.Radius;

                        // 弧长的增量
                        double lengthIncrement = length;

                        // 计算点击点到圆弧起点和终点的距离
                        double distanceToStart = Vector2.Distance(wcsp1, start);
                        double distanceToEnd = Vector2.Distance(wcsp1, end);

                        // 初始化新的终点为原终点
                        Vector2 newEnd = end;

                        // 计算原始弧长
                        double originalArcLength = Math.Sqrt(Math.Pow(end.X - start.X, 2) + Math.Pow(end.Y - start.Y, 2));
                        // 计算新的弧长
                        double newArcLength = originalArcLength + lengthIncrement;

                        if (distanceToStart < distanceToEnd)// 如果点击点更接近起点，计算新的终点角度
                        {
                            

                            if (wcsp1.X < end.X)
                            {
                                double newEndAngle = Math.Atan2(end.Y - center.Y, end.X - center.X) + (newArcLength / radius);

                                // 使用新的终点角度计算新的终点坐标
                                newEnd = new Vector2(center.X + radius * Math.Cos(newEndAngle), center.Y + radius * Math.Sin(newEndAngle));
                            }

                           else if (wcsp1.X > end.X)
                            {
                                double newEndAngle = Math.Atan2(start.Y - center.Y, start.X - center.X) + (newArcLength / radius);

                                // 使用新的终点角度计算新的终点坐标
                                newEnd = new Vector2(center.X + radius * Math.Cos(newEndAngle), center.Y + radius * Math.Sin(newEndAngle));
                            }



                        }
                        else if (distanceToStart > distanceToEnd) // 如果点击点更接近终点，计算新的终点角度
                        {
                            if (end.X > start.X)
                            {
                                double newEndAngle = Math.Atan2(start.Y - center.Y, start.X - center.X) + (newArcLength / radius);

                                // 使用新的终点角度计算新的终点坐标
                                newEnd = new Vector2(center.X + radius * Math.Cos(newEndAngle), center.Y + radius * Math.Sin(newEndAngle));
                            }

                        }

                        // 更新圆弧的终点
                        arc.Set(end: newEnd);
                    }

                }
            }
            //return null;


        }


        public static LcArc DEALengthen(double angle, List<LcElement> Scurve)        //角度增量拉长 （对象只有圆弧，不包括多段线画出的弧线）
        {

            if (Scurve != null)
            {
                foreach (var element in Scurve)
                {
                    if (element is LcLine) { return null; }
                    else if (element is LcPolyLine) { return null; }
                    else if (element is LcArc)
                    {
                        LcArc arc = (LcArc)element;
                        // 处理圆弧

                        // 获取圆弧的属性


                        double endAngle = arc.EndAngle;
                        double newEndAngle = (endAngle + angle) % 360;
                        if (newEndAngle < 0)
                        {
                            return null;
                        }
                        // 创建新的圆弧


                        Vector2 Endp = new Vector2(arc.Center.X + arc.Radius * Math.Cos(newEndAngle),
                                               arc.Center.Y + arc.Radius * Math.Sin(newEndAngle));

                        arc.Set(end: Endp);

                        return arc;
                    }
                    else { return null; }
                }
            }
            return null;
        }


        public void PLengthen(double lpercentage, List<LcElement> Selements)        //长度百分数
        {
            if (Selements != null)
            {
                foreach (var element in Selements)
                {
                    if (element is LcLine)
                    {
                        LcLine line = (LcLine)element;
                        

                        var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
                        double diatance3 = Vector2.Distance(wcsp1, line.Start);
                        double diatance4 = Vector2.Distance(wcsp1, line.End);
                        if (diatance3 < diatance4)
                        {
                            Vector2 firstPoint = line.Start;
                            Vector2 secondPoint = line.End;

                            line.Set(start: secondPoint, end: firstPoint);

                        }


                        // 处理线段
                        Vector2 startPoint = line.Start;
                        Vector2 endPoint = line.End;

                        // 计算线段的当前长度
                        double currentLength = line.Length;

                        // 计算新的长度
                        double newLength = currentLength * lpercentage;

                        double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                        // 计算直线方程中的截距 b
                        double b = startPoint.Y - k * startPoint.X;

                        // 计算新增长度
                        double length = newLength - currentLength;

                        if (endPoint.X > startPoint.X)
                        // 根据新的长度计算新的终点 x 坐标
                        {
                            double newX = startPoint.X + newLength / Math.Sqrt(1 + k * k);


                            // 根据 x 坐标计算新的终点 y 坐标
                            double newY = k * newX + b;

                            // 创建新的终点坐标
                            Vector2 newEndPoint = new Vector2((float)newX, (float)newY);




                            line.Set(end: newEndPoint);
                            //return line;
                        }

                        else if (endPoint.X < startPoint.X)
                        {
                            double newX = startPoint.X - newLength / Math.Sqrt(1 + k * k);


                            // 根据 x 坐标计算新的终点 y 坐标
                            double newY = k * newX + b;

                            // 创建新的终点坐标
                            Vector2 newEndPoint = new Vector2((float)newX, (float)newY);




                            line.Set(end: newEndPoint);
                        }

                        else if (endPoint.X == startPoint.X)
                        {

                            if (wcsp1.Y > endPoint.Y)
                            {
                                double newY = endPoint.Y - length;
                                Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                line.Set(end: newEndPoint);


                            }
                            if (wcsp1.Y < endPoint.Y)
                            {
                                double newY = endPoint.Y + length;
                                Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                line.Set(end: newEndPoint);

                            }
                        }

                        else if (endPoint.Y == startPoint.Y)
                        {
                            if (wcsp1.X > endPoint.X)
                            {
                                double newX = endPoint.X - length;
                                Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                line.Set(end: newEndPoint);


                            }
                            if (wcsp1.X < endPoint.X)
                            {
                                double newX = endPoint.X + length;
                                Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                line.Set(end: newEndPoint);


                            }


                        }



                    }


                    else if (element is LcPolyLine)
                    {
                        LcPolyLine polyline = (LcPolyLine)element;
                        // 处理多段线
                        // 计算多段线的新顶点
                      



                            // 处理多段线
                            // 计算多段线的新顶点
                            if (polyline.Curve2ds.Count > 1)
                            {
                                // 如果多段线有多于一段的线段




                                var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
                                double diatance3 = Vector2.Distance(wcsp1, LcPolyLine.GetStart(polyline.Curve2ds[0]));
                                double diatance4 = Vector2.Distance(wcsp1, LcPolyLine.GetEnd(polyline.Curve2ds[polyline.Curve2ds.Count - 1]));

                            if (lpercentage > 100)
                            {

                                if (diatance3 < diatance4)//点击点离开始点更近
                                {
                                    for (int i = 0; i < polyline.Curve2ds.Count; i++)//多段线每段线起点终点互换，不只换第一段，是怕多段线发生其它错误
                                    {
                                        Vector2 firstPoint = LcPolyLine.GetStart(polyline.Curve2ds[i]);
                                        Vector2 secondPoint = LcPolyLine.GetEnd(polyline.Curve2ds[i]);

                                        (polyline.Curve2ds[i] as Line2d).Start = secondPoint;
                                        (polyline.Curve2ds[i] as Line2d).End = firstPoint;

                                    }
                                    Vector2 startPoint = LcPolyLine.GetStart(polyline.Curve2ds[0]);
                                    Vector2 endPoint = LcPolyLine.GetEnd(polyline.Curve2ds[0]);


                                    //计算线段的当前长度
                                    double currentLength = Vector2.Distance(startPoint, endPoint);

                                    //计算原多段线的长度
                                    double PLcurrentLength = polyline.LineTypeScale;

                                    //计算新多段线的长度
                                    double newPLcurrentLength = PLcurrentLength * lpercentage;

                                 
                                    // 计算新增长度
                                    double length = newPLcurrentLength - PLcurrentLength;

                                    double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                                    // 计算直线方程中的截距 b
                                    double b = startPoint.Y - k * startPoint.X;


                                    if (endPoint.X > startPoint.X)//最后一段线的终点在右边

                                    // 根据新的长度计算新的终点 x 坐标
                                    {

                                        double newX = startPoint.X + length / Math.Sqrt(1 + k * k);

                                        // 根据 x 坐标计算新的终点 y 坐标
                                        double newY = k * newX + b;

                                        // 创建新的终点坐标
                                        Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                        // 更新多段线的最后一段线段
                                        (polyline.Curve2ds[0] as Line2d).End = newEndPoint;

                                    }


                                    else if (endPoint.X < startPoint.X) //最后一段线的终点在左边

                                    // 根据新的长度计算新的终点 x 坐标
                                    {
                                        double newX = startPoint.X - length / Math.Sqrt(1 + k * k);

                                        // 根据 x 坐标计算新的终点 y 坐标
                                        double newY = k * newX + b;

                                        // 创建新的终点坐标
                                        Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                        // 更新多段线的最后一段线段
                                        (polyline.Curve2ds[0] as Line2d).End = newEndPoint;

                                    }

                                    else if (endPoint.X == startPoint.X)
                                    {

                                        if (wcsp1.Y > endPoint.Y)
                                        {
                                            double newY = endPoint.Y - length;
                                            Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                            (polyline.Curve2ds[0] as Line2d).End = newEndPoint;


                                        }
                                        if (wcsp1.Y < endPoint.Y)
                                        {
                                            double newY = endPoint.Y + length;
                                            Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                            (polyline.Curve2ds[0] as Line2d).End = newEndPoint;

                                        }
                                    }

                                    else if (endPoint.Y == startPoint.Y)
                                    {
                                        if (wcsp1.X > endPoint.X)
                                        {
                                            double newX = endPoint.X - length;
                                            Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                            (polyline.Curve2ds[0] as Line2d).End = newEndPoint;


                                        }
                                        if (wcsp1.X < endPoint.X)
                                        {
                                            double newX = endPoint.X + length;
                                            Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                            (polyline.Curve2ds[0] as Line2d).End = newEndPoint;


                                        }


                                    }



                                }



                                else if (diatance3 > diatance4)


                                {
                                    Vector2 startPoint = LcPolyLine.GetStart(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);
                                    Vector2 endPoint = LcPolyLine.GetEnd(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);

                                    //计算线段的当前长度
                                    double currentLength = Vector2.Distance(startPoint, endPoint);

                                    //计算原多段线的长度
                                    double PLcurrentLength = polyline.LineTypeScale;

                                    //计算新多段线的长度
                                    double newPLcurrentLength = PLcurrentLength * lpercentage;


                                    // 计算新增长度
                                    double length = newPLcurrentLength - PLcurrentLength;

                                    double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                                    // 计算直线方程中的截距 b
                                    double b = startPoint.Y - k * startPoint.X;


                                    if (endPoint.X > startPoint.X)

                                    // 根据新的长度计算新的终点 x 坐标
                                    {
                                        double newX = startPoint.X + Length / Math.Sqrt(1 + k * k);

                                        // 根据 x 坐标计算新的终点 y 坐标
                                        double newY = k * newX + b;

                                        // 创建新的终点坐标
                                        Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                        // 更新多段线的最后一段线段
                                        (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;

                                    }

                                    else if (endPoint.X < startPoint.X)

                                    // 根据新的长度计算新的终点 x 坐标
                                    {
                                        double newX = startPoint.X - Length / Math.Sqrt(1 + k * k);

                                        // 根据 x 坐标计算新的终点 y 坐标
                                        double newY = k * newX + b;

                                        // 创建新的终点坐标
                                        Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                                        // 更新多段线的最后一段线段
                                        (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;

                                    }


                                    else if (endPoint.X == startPoint.X)
                                    {

                                        if (wcsp1.Y > endPoint.Y)
                                        {
                                            double newY = endPoint.Y - length;
                                            Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                            (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;


                                        }
                                        if (wcsp1.Y < endPoint.Y)
                                        {
                                            double newY = endPoint.Y + length;
                                            Vector2 newEndPoint = new Vector2(endPoint.X, (float)newY);
                                            (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;

                                        }
                                    }

                                    else if (endPoint.Y == startPoint.Y)
                                    {
                                        if (wcsp1.X > endPoint.X)
                                        {
                                            double newX = endPoint.X - length;
                                            Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                            (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;


                                        }
                                        if (wcsp1.X < endPoint.X)
                                        {
                                            double newX = endPoint.X + length;
                                            Vector2 newEndPoint = new Vector2((float)newX, endPoint.Y);
                                            (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;


                                        }


                                    }




                                }

                            }

                            }

                    }



                    else if (element is LcArc)
                    {
                        LcArc arc = (LcArc)element;
                        // 处理圆弧

                        // 获取圆弧的属性
                        Vector2 center = arc.Center;
                        Vector2 sp = arc.Startp;
                        Vector2 end = arc.Endp;
                        double radius = arc.Radius;



                        double startAngle = Math.Atan2(sp.Y - center.Y, sp.X - center.X);
                        double endAngle = Math.Atan2(end.Y - center.Y, end.X - center.X);

                        // 计算原始弧的弧长
                        double originalArcLength = Math.Abs(endAngle - startAngle) * radius;

                        // 计算新的弧长
                        double newArcLength = originalArcLength * lpercentage;

                        // 计算新的终点角度
                        double newEndAngle = startAngle + (newArcLength / radius);

                        // 使用新的终点角度计算新的终点坐标
                        Vector2 newEnd = new Vector2(center.X + radius * Math.Cos(newEndAngle), center.Y + radius * Math.Sin(newEndAngle));

                        //更新弧的终点
                        arc.Set(end: newEnd);
                      
                    }
                }
            }
           

        }


        public static LcElement TLengthen(double length, List<LcElement> Selements)        //指定总长度
        {
            if (Selements != null)
            {
                foreach (var element in Selements)
                {
                    if (element is LcLine)
                    {
                        LcLine line = (LcLine)element;
                        // 处理线段

                        Vector2 startPoint = line.Start;
                        Vector2 endPoint = line.End;

                        // 计算线段的当前长度
                        double currentLength = line.Length;

                        // 计算新的长度
                        double newLength = length;

                        double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                        // 计算直线方程中的截距 b
                        double b = startPoint.Y - k * startPoint.X;

                        // 根据新的长度计算新的终点 x 坐标
                        double newX = startPoint.X + newLength / Math.Sqrt(1 + k * k);

                        // 根据 x 坐标计算新的终点 y 坐标
                        double newY = k * newX + b;

                        // 创建新的终点坐标
                        Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                        line.Set(end: newEndPoint);
                        return line;


                    }


                    else if (element is LcPolyLine)
                    {
                        LcPolyLine polyline = (LcPolyLine)element;
                        // 处理多段线
                        // 计算多段线的新顶点
                        if (polyline.Curve2ds.Count > 1)
                        {
                            // 如果多段线有多于一段的线段

                            //Vector2 startPoint = (polyline.Curve2ds[polyline.Curve2ds.Count] as Line2d).Start;（另一种写法）
                            //Vector2 endPoint = (polyline.Curve2ds[polyline.Curve2ds.Count-1] as Line2d).End

                            Vector2 startPoint = LcPolyLine.GetStart(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);
                            Vector2 endPoint = LcPolyLine.GetEnd(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);


                            // 计算线段的当前长度
                            double currentLength = Vector2.Distance(startPoint, endPoint);

                            // 计算新的长度
                            double newLength = length;

                            double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                            // 计算直线方程中的截距 b
                            double b = startPoint.Y - k * startPoint.X;

                            // 根据新的长度计算新的终点 x 坐标
                            double newX = startPoint.X + newLength / Math.Sqrt(1 + k * k);

                            // 根据 x 坐标计算新的终点 y 坐标
                            double newY = k * newX + b;

                            // 创建新的终点坐标
                            Vector2 newEndPoint = new Vector2((float)newX, (float)newY);

                            // 更新多段线的最后一段线段
                            (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;


                            return polyline;
                        }

                    }



                    else if (element is LcArc)
                    {
                        LcArc arc = (LcArc)element;
                        // 处理圆弧

                        // 获取圆弧的属性
                        Vector2 center = arc.Center;
                        Vector2 sp = arc.Startp;
                        Vector2 end = arc.Endp;
                        double radius = arc.Radius;



                        double startAngle = Math.Atan2(sp.Y - center.Y, sp.X - center.X);
                        double endAngle = Math.Atan2(end.Y - center.Y, end.X - center.X);

                        // 计算原始弧的弧长
                        double originalArcLength = Math.Abs(endAngle - startAngle) * radius;

                        // 计算新的弧长
                        double newArcLength = length;

                        // 计算新的终点角度
                        double newEndAngle = startAngle + (newArcLength / radius);

                        // 使用新的终点角度计算新的终点坐标
                        Vector2 newEnd = new Vector2(center.X + radius * Math.Cos(newEndAngle), center.Y + radius * Math.Sin(newEndAngle));

                        //更新弧的终点
                        arc.Set(end: newEnd);
                        return arc;
                    }

                }
            }

            return null;






        }


        public static LcArc TALengthen(double angle, List<LcElement> Tcurve)        //指定总角度
        {
            if (Tcurve != null)
            {
                foreach (var element in Tcurve)
                {
                    if (element is LcLine) { return null; }
                    else if (element is LcPolyLine) { return null; }
                    else if (element is LcArc)
                    {
                        LcArc arc = (LcArc)element;
                        // 处理圆弧

                        // 获取圆弧的属性


                        double endAngle = arc.EndAngle;
                        double newEndAngle = angle % 360;
                        if (newEndAngle < 0)
                        {
                            return null;
                        }
                        // 创建新的圆弧


                        Vector2 Endp = new Vector2(arc.Center.X + arc.Radius * Math.Cos(newEndAngle),
                                               arc.Center.Y + arc.Radius * Math.Sin(newEndAngle));

                        arc.Set(end: Endp);

                        return arc;

                    }
                    else { return null; }
                }
            }
            return null;
        }


        public static Curve2d DYLengthen(Vector2 newendpoint, List<Curve2d> Selements)        //指定新端点
        {
            Curve2d DYResult = null;
            //foreach (Curve2d element in Selements)
            //{
            //    if (element is Line2d)
            //    {
            //        Line2d line = (Line2d)element;
            //        // 处理线段

            //        Vector2 startPoint = line.Start;
            //        Vector2 endPoint = line.End;

            //        // 计算原线段的方向向量
            //        Vector2 direction = endPoint - startPoint;

            //        // 计算新的线段长度，假设为原线段长度
            //        double newLength = direction.Length();

            //        // 计算新的终点坐标，使其在原线段所在的直线上
            //        Vector2 newEndPoint = startPoint + direction;

            //        // 创建新的 Line2d 对象
            //        Line2d newLine = new Line2d(startPoint, newEndPoint);



            //        DYResult = newLine;


            //    }


            //    else if (element is Polyline2d)
            //    {
            //        Polyline2d polyline = (Polyline2d)element;
            //        // 处理多段线
            //        // 计算多段线的新顶点
            //        if (polyline.Curve2ds.Count > 1)
            //        {
            //            // 如果多段线有多于一段的线段

            //            //Vector2 startPoint = (polyline.Curve2ds[polyline.Curve2ds.Count] as Line2d).Start;（另一种写法）
            //            //Vector2 endPoint = (polyline.Curve2ds[polyline.Curve2ds.Count-1] as Line2d).End

            //            Vector2 startPoint = LcPolyLine.GetStart(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);
            //            Vector2 endPoint = LcPolyLine.GetEnd(polyline.Curve2ds[polyline.Curve2ds.Count - 1]);


            //            // 计算线段的当前长度
            //            double currentLength = Vector2.Distance(startPoint, endPoint);

            //            // 计算新的长度
            //            double newLength = currentLength + length;

            //            // 计算新的终点坐标，保持线段方向不变
            //            Vector2 newEndPoint = startPoint + (endPoint - startPoint) * newLength;

            //            // 更新多段线的最后一段线段
            //            (polyline.Curve2ds[polyline.Curve2ds.Count - 1] as Line2d).End = newEndPoint;

            //            DYResult = polyline;
            //        }

            //    }



            //    else if (element is Arc2d)
            //    {
            //        Arc2d arc = (Arc2d)element;
            //        // 处理圆弧

            //        // 获取圆弧的属性
            //        Vector2 center = arc.Center;
            //        Vector2 sp = arc.Startp;
            //        Vector2 end = arc.Endp;
            //        double radius = arc.Radius;

            //        // 弧长的增量
            //        double lengthIncrement = length;

            //        double startAngle = Math.Atan2(sp.Y - center.Y, sp.X - center.X);
            //        double endAngle = Math.Atan2(end.Y - center.Y, end.X - center.X);

            //        // 计算原始弧的弧长
            //        double originalArcLength = Math.Abs(endAngle - startAngle) * radius;

            //        // 计算新的弧长
            //        double newArcLength = originalArcLength + lengthIncrement;

            //        // 计算新的终点角度
            //        double newEndAngle = startAngle + (newArcLength / radius);

            //        // 使用新的终点角度计算新的终点坐标
            //        Vector2 newEnd = new Vector2(center.X + radius * Math.Cos(newEndAngle), center.Y + radius * Math.Sin(newEndAngle));

            //        //更新弧的终点
            //        arc.Endp = newEnd;

            //        DYResult = arc;
            //    }

            //}


            return DYResult;


        }





        private void DrawAuxLine(SKCanvas canvas, Vector2 p0, Vector2 p1, SKPaint sKPaint)   //实时渲染
        {
            var sk_pre = this.vportRt.ConvertWcsToScr(p0).ToSKPoint();
            var sk_p = this.vportRt.ConvertWcsToScr(p1).ToSKPoint();
            //辅助元素的颜色 
            canvas.DrawLine(sk_pre, sk_p, sKPaint);
            //辅助曲线的颜色，包括辅助长度，辅助角度等

        }







        public void CreateLengthen()
        {
            //foreach (var element in selectedEXElements)
            //{
            //    if (element != null)
            //    {
            //        List<Vector2> points = new List<Vector2>();
            //        if (element is LcPolyLine) //判断第元素是不是多段线
            //        {
            //            ResultCurve = DELengthen(AddLength, element);
            //        }
            //    }
            //}




        }


        public virtual void DrawTrans(SKCanvas canvas)    //渲染
        {

            //Vector2 wp =  this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
            //  //= this.vportRt.HitSelect(this.docRt.Document.ModelSpace, wcsp, wsize);
            //  var hitResult = this.vportRt.HitSelect(this.docRt.Document.ModelSpace, wp, 2);
            if (!this.vportRt.CheckStatus(ViewportStatus.RectSelect))//单个元素的点击状态(如果是单个元素被点击）
            {
                var wcsp = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());//指针移动到的坐标转换为点之后转换为世界坐标
                ////this.inputp = wcsp;
                var wsize = this.vportRt.ConvertScrToWcs(Constants.SelectBoxSize);   //盒子的宽度
                var hitResult = this.vportRt.HitSelect(this.docRt.Document.ModelSpace, wcsp, wsize);



                if (hitResult != null)
                {


                    if (hitResult.Item1 is LcLine)
                    {
                        List<Vector2> points = new List<Vector2>();
                        DrawAuxLine(canvas, AddLength, points, selectedElements);
                    }
                    //if (hitResult.Item1 is LcArc)
                    //{
                    //    DrawAuArc(canvas, points, hitResult.Item1);
                    //}


                }
            }
        }






        public void DrawAuxLine(SKCanvas canvas, double length, List<Vector2> points, List<LcElement> element1)
        {

            Vector2 point = new Vector2();
            SKPaint AuxPen = new SKPaint { Color = SKColors.Gray, IsStroke = true, PathEffect = Constants.SelectedEffect };
          


            if (element1 != null)
            {
                foreach (var element11 in element1)
                {
                    if (element11 is LcLine)
                    {
                        LcLine line = (LcLine)element11;



                        var wcsp1 = this.vportRt.ConvertScrToWcs(this.vportRt.PointerMovedPosition.ToVector2d());
                        double diatance3 = Vector2.Distance(wcsp1, line.Start);
                        double diatance4 = Vector2.Distance(wcsp1, line.End);
                        if (diatance3 < diatance4)
                        {
                            Vector2 firstPoint = line.Start;
                            Vector2 secondPoint = line.End;

                            line.Set(start: secondPoint, end: firstPoint);

                        }




                        // 处理线段

                        Vector2 startPoint = line.Start;
                        Vector2 endPoint = line.End;

                        // 计算线段的当前长度
                        double currentLength = line.Length;

                        // 计算新的长度
                        double newLength = currentLength + length;

                        double k = (endPoint.Y - startPoint.Y) / (endPoint.X - startPoint.X);

                        // 计算直线方程中的截距 b
                        double b = startPoint.Y - k * startPoint.X;



                        if (endPoint.X > startPoint.X)
                        // 根据新的长度计算新的终点 x 坐标
                        {
                            double newX = startPoint.X + newLength / Math.Sqrt(1 + k * k);


                            // 根据 x 坐标计算新的终点 y 坐标
                            double newY = k * newX + b;

                            // 创建新的终点坐标
                            Vector2 newEndPoint = new Vector2((float)newX, (float)newY);




                            line.Set(end: newEndPoint);
                            //return line;
                        }

                        else if (endPoint.X < startPoint.X)
                        {
                            double newX = startPoint.X - newLength / Math.Sqrt(1 + k * k);


                            // 根据 x 坐标计算新的终点 y 坐标
                            double newY = k * newX + b;

                            // 创建新的终点坐标
                            Vector2 newEndPoint = new Vector2((float)newX, (float)newY);




                            line.Set(end: newEndPoint);
                        }


                        canvas.DrawLine(vportRt.ConvertWcsToScr(line.Start).ToSKPoint(), vportRt.ConvertWcsToScr(point).ToSKPoint(), AuxPen);
                    }
                }
            }
















        }
    }
}
































