﻿
namespace LightCAD.Drawing.Actions
{
    /// <summary>
    /// 元素活动的静态对象，用于渲染，避免新创建对象
    /// </summary>
    public static class ElementActions
    {
        public static ElementAction Point{ get; set; }
        public static ElementAction Line { get; set; }
        public static ElementAction Ellipse { get; set; }
        public static ElementAction Ray { get; set; }
        public static ElementAction XLine { get; set; }
        public static ElementAction Circle { get; set; }
        public static ElementAction Group { get; set; }
        public static ElementAction BlockRef { get; set; }
        public static ElementAction PolyLine { get; set; }
        public static ElementAction Rectang { get; set; }

        public static ElementAction Polygon { get; set; }

        public static ElementAction Arc { get; set; }

        public static ElementAction RoLine { get; set; }
        public static ElementAction DimDiametric { get; set; }

        public static ElementAction DimArcLenght { get; set; }

        public static ElementAction DimOrdinate { get; set; }

        public static ElementAction DimJogged { get; set; }

        public static ElementAction LcText { get; set; }

        public static ElementAction MText { get; set; }
        public static ElementAction ComponentRef { get; set; }
        public static ElementAction DirectComponent { get; set; }
        public static ElementAction DimRotated { get; set; }
        public static ElementAction DimAligned { get; set; }
        public static ElementAction QLeader { get; set; }

        public static ElementAction DimRadial { get; set; }
        public static ElementAction DimArc { get; set; }
        public static ElementAction DimAngularArc { get; set; }
        public static ElementAction Section { get; set; }
        public static ElementAction Solid { get; set; }

        public static ElementAction Break { get; set; }
        public static ElementAction Join { get; set; }

        public static ElementAction AxisGrid { get; set; }
        public static ElementAction AxisLine { get; set; }
        public static ElementAction Stretch { get; set; }
        public static ElementAction RasterImage { get; set; }
        public static ElementAction Table { get; set; }
        public static ElementAction TableEdit { get; set; }
        public static ElementAction Hatch { get; set; }





    }
}
