﻿


using LightCAD.Core.Elements.Basic;
using LightCAD.Drawing.Actions.Action;

namespace LightCAD.Drawing.Actions
{
    [CommandClass]
    public class ActionCmds
    {
        #region Basic Element

        [CommandMethod(Name = "LINE", ShortCuts = "L")]
        public CommandResult DrawLine(IDocumentEditor docEditor, string[] args)
        {
            var lineAction = new LineAction(docEditor);
            lineAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "CIRCLE", ShortCuts = "C")]
        public CommandResult DrawCircle(IDocumentEditor docEditor, string[] args)
        {
            var circleAction = new CircleAction(docEditor);
            circleAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "ELLIPSE", ShortCuts = "EL")]
        public CommandResult DrawEllipse(IDocumentEditor docEditor, string[] args)
        {
            docEditor.CommandCenter.WriteInfo("命令：" + "Ellipse");
            var ellipseAction = new EllipseAction(docEditor);
            ellipseAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "RAY", ShortCuts = "R")]
        public CommandResult DrawRay(IDocumentEditor docEditor, string[] args)
        {
            var rayAction = new RayAction(docEditor);
            rayAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "XLINE", ShortCuts = "XL")]
        public CommandResult DrawXLine(IDocumentEditor docEditor, string[] args)
        {
            var xlineAction = new XLineAction(docEditor);
            xlineAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "PLINE", ShortCuts = "PL")]
        public CommandResult DrawPolyLine(IDocumentEditor docEditor, string[] args)
        {
            var plineAction = new PolyLineAction(docEditor);
            plineAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "RECTANG", ShortCuts = "REC")]
        public CommandResult DrawRectangPolyLine(IDocumentEditor docEditor, string[] args)
        {
            var plineAction = new RectangAction(docEditor);
            plineAction.ExecCreate(args);
            return CommandResult.Succ();

        }
        [CommandMethod(Name = "SECTION", ShortCuts = "SEC")]
        public CommandResult DrawSectionPolyLine(IDocumentEditor docEditor, string[] args)
        {
            var sectionAction = new SectionAction(docEditor, "SECTION");
            sectionAction.ExecCreate(args);
            return CommandResult.Succ();
        }


        [CommandMethod(Name = "SCEDIT", ShortCuts = "SCE")]
        public CommandResult EditSection(IDocumentEditor docEditor, string[] args)
        {
            var sectionAction = new SectionAction(docEditor, "SCEDIT");
            sectionAction.ExecEdit(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "SCEDITEND", ShortCuts = "SCEND")]
        public CommandResult EditSectionEnd(IDocumentEditor docEditor, string[] args)
        {
            var sectionAction = new SectionAction(docEditor, "SCEDITEND");
            sectionAction.ExecEndEdit(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "POINT", ShortCuts = "P")]
        public CommandResult DrawPoint(IDocumentEditor docEditor, string[] args)
        {
            var plineAction = new PointAction(docEditor);
            plineAction.ExecCreate(args);
            return CommandResult.Succ();

        }
        [CommandMethod(Name = "POLYGON", ShortCuts = "POL")]
        public CommandResult DrawPolygon(IDocumentEditor docEditor, string[] args)
        {
            var plineAction = new PolygonAction(docEditor);
            plineAction.ExecCreate(args);
            return CommandResult.Succ();

        }


        [CommandMethod(Name = "ARC", ShortCuts = "AR")]
        public CommandResult DrawArc(IDocumentEditor docEditor, string[] args)
        {
            var ArcAction = new ArcAction(docEditor);
            ArcAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "QLEADER", ShortCuts = "QL")]
        public CommandResult DrawQleader(IDocumentEditor docEditor, string[] args)
        {

            var QleaderAction = new QleaderAction(docEditor);
            QleaderAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "DIMRADIAL", ShortCuts = "DIMRAD")]
        public CommandResult DrawDimRadial(IDocumentEditor docEditor, string[] args)
        {

            var DimRadialAction = new DimRadialAction(docEditor);
            DimRadialAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "ROL", ShortCuts = "RL")]
        public CommandResult DrawRol(IDocumentEditor docEditor, string[] args)
        {
            var RolLineAction = new RolLineAction(docEditor);
            RolLineAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "EXTEND", ShortCuts = "EX")]
        public CommandResult DrawExtend(IDocumentEditor docEditor, string[] args)
        {
            var RolLineAction = new ExtendAction(docEditor);
            RolLineAction.ExecCreate(args);
            return CommandResult.Succ();
        }


        /// <summary>
        /// 直径标注
        /// </summary>
        /// <param name="docRt"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [CommandMethod(Name = "DIMDIAMETER", ShortCuts = "DDI")]
        public CommandResult DrawDDI(IDocumentEditor docEditor, string[] args)
        {
            var DimDiameterAction = new DimDiameterAction(docEditor);
            DimDiameterAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        /// <summary>
        ///弧长标注
        /// </summary>
        /// <param name="docRt"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [CommandMethod(Name = "DIMARC", ShortCuts = "DAR")]
        public CommandResult DrawDimArc(IDocumentEditor docEditor, string[] args)
        {
            var DimAngularAction = new DimArcAction(docEditor);
            DimAngularAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        /// <summary>
        ///坐标标注
        /// </summary>
        /// <param name="docRt"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [CommandMethod(Name = "DIMORDINATE", ShortCuts = "DOR")]
        public CommandResult DrawDimDor(IDocumentEditor docEditor, string[] args)
        {
            var DimOrdinateAction = new DimOrdinateAction(docEditor);
            DimOrdinateAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        /// <summary>
        ///折弯标注
        /// </summary>
        /// <param name="docRt"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [CommandMethod(Name = "DIMJOGGED", ShortCuts = "DJO")]
        public CommandResult DrawDimDjo(IDocumentEditor docEditor, string[] args)
        {
            var DimJoggedActio = new DimJoggedActio(docEditor);
            DimJoggedActio.ExecCreate(args);
            return CommandResult.Succ();
        }

        /// <summary>
        ///文字
        /// </summary>
        /// <param name="docRt"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [CommandMethod(Name = "TEXT", ShortCuts = "TEXT")]
        public CommandResult DrawText(IDocumentEditor docEditor, string[] args)
        {
            var TextAction = new TextAction(docEditor);
            TextAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        /// <summary>
        ///多行文字
        /// </summary>
        /// <param name="docRt"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        [CommandMethod(Name = "MTEXT", ShortCuts = "MTEXT")]
        public CommandResult DrawMText(IDocumentEditor docEditor, string[] args)
        {
            var MTextAction = new MTextAction(docEditor);
            MTextAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "MEASURE", ShortCuts = "ID")]
        public CommandResult DrawMesure(IDocumentEditor docEditor, string[] args)
        {
            var MTextAction = new MeasureAction(docEditor);
            MTextAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "CLIP", ShortCuts = "CP")]
        public CommandResult DrawClip(IDocumentEditor docEditor, string[] args)
        {
            var MTextAction = new ClipAction(docEditor);
            MTextAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        #endregion

        #region Group
        [CommandMethod(Name = "GROUP", ShortCuts = "G")]
        public CommandResult CreateGroup(IDocumentEditor docEditor, string[] args)
        {
            var groupAction = new GroupAction(docEditor);
            groupAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "GROUPEDIT", ShortCuts = "")]
        public CommandResult EditGroup(IDocumentEditor docEditor, string[] args)
        {
            var groupAction = new GroupAction(docEditor);
            groupAction.ExecEdit(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "PLCKGROUP", ShortCuts = "")]
        public CommandResult PlckGroup(IDocumentEditor docEditor, string[] args)
        {
            var groupAction = new GroupAction(docEditor);
            groupAction.ExecPlckEdit(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "UNGROUP", ShortCuts = "")]
        public CommandResult Ungroup(IDocumentEditor docEditor, string[] args)
        {
            var groupAction = new GroupAction(docEditor);
            groupAction.ExecUngroup(args);
            return CommandResult.Succ();
        }
        #endregion

        #region Block
        [CommandMethod(Name = "BLOCK", ShortCuts = "B")]
        public CommandResult CreateBlock(IDocumentEditor docEditor, string[] args)
        {
            var blockAction = new BlockAction(docEditor);
            blockAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "BEDIT", ShortCuts = "")]
        public CommandResult EditBlock(IDocumentEditor docEditor, string[] args)
        {
            var blockAction = new BlockAction(docEditor);
            blockAction.ExecEdit(args);
            return CommandResult.Succ();
        }


        [CommandMethod(Name = "BLOCKREPLACE", ShortCuts = "")]
        public CommandResult ReplaceBlock(IDocumentEditor docEditor, string[] args)
        {
            var blockAction = new BlockAction(docEditor);
            blockAction.Replace(args);
            return CommandResult.Succ();
        }



        [CommandMethod(Name = "INSERT", ShortCuts = "")]
        public CommandResult InsertBlock(IDocumentEditor docEditor, string[] args)
        {
            var blockRefAction = new BlockRefAction(docEditor);
            blockRefAction.ExecInsert(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "REFEDIT", ShortCuts = "")]
        public CommandResult RefEdit(IDocumentEditor docEditor, string[] args)
        {
            //if(Xref)new XrefAction();
            var blockAction = new BlockAction(docEditor);
            blockAction.ExecRefEdit(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "REFCLOSE", ShortCuts = "")]
        public CommandResult RefClose(IDocumentEditor docEditor, string[] args)
        {
            var blockAction = new BlockAction(docEditor);
            blockAction.ExecRefClose(args);
            return CommandResult.Succ();
        }
        #endregion

        #region Trans
        [CommandMethod(Name = "SCALE", ShortCuts = "")]
        public CommandResult TransformScale(IDocumentEditor docEditor, string[] args)
        {
            TransformAction TransformAction = new TransformAction(docEditor);
            TransformAction.ScaleTransform(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "ROTATE", ShortCuts = "")]
        public CommandResult TransformRotate(IDocumentEditor docEditor, string[] args)
        {
            TransformAction TransformAction = new TransformAction(docEditor);
            TransformAction.RotateTransform(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "MIRROR", ShortCuts = "")]
        public CommandResult TransformMirror(IDocumentEditor docEditor, string[] args)
        {
            TransformAction TransformAction = new TransformAction(docEditor);
            TransformAction.MirrorTransform(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "MOVE", ShortCuts = "")]
        public CommandResult TransformMove(IDocumentEditor docEditor, string[] args)
        {
            TransformAction TransformAction = new TransformAction(docEditor);
            TransformAction.MoveTransform(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "UCS", ShortCuts = "")]
        public CommandResult OpenUCS(IDocumentEditor docEditor, string[] args)
        {
            UCSAction UCSAction = new UCSAction(docEditor);
            UCSAction.OPENUCS(args);
            return CommandResult.Succ();
        }


        [CommandMethod(Name = "LENGTHEN", ShortCuts = "LEN")]
        public CommandResult TransformLengthen(IDocumentEditor docEditor, string[] args)
        {
            LengthenAction LengthenAction = new LengthenAction(docEditor);
            LengthenAction.LengthenTransform(args);
            return CommandResult.Succ();
        }



        [CommandMethod(Name = "TRIM", ShortCuts = "TR")]
        public CommandResult Trim(IDocumentEditor docEditor, string[] args)
        {
            TrimAction TrimAction = new TrimAction(docEditor);
            TrimAction.ExecCreate(args);
            return CommandResult.Succ();
        }


        #endregion
        #region Building
        [CommandMethod(Name = "Building", ShortCuts = "BD")]
        public CommandResult RefBuilding(IDocumentEditor docEditor, string[] args)
        {
            var buildingAction = new BuildingAction(docEditor);
            buildingAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "BasePoint", ShortCuts = "BP")]
        public CommandResult RefBasePoint(IDocumentEditor docEditor, string[] args)
        {
            var basePointAction = new BasePointAction(docEditor);
            basePointAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Drawing", ShortCuts = "DF")]
        public CommandResult RefDrawing(IDocumentEditor docEditor, string[] args)
        {
            var drawingFrameAction = new DrawingFrameAction(docEditor);
            drawingFrameAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        #endregion
        [CommandMethod(Name = "DIMLINEAR", ShortCuts = "DLI")]
        public CommandResult DrawDimLinear(IDocumentEditor docEditor, string[] args)
        {
            var dimRotatedAction = new DimRotatedAction(docEditor);
            dimRotatedAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "DIMALIGNED", ShortCuts = "DAL")]
        public CommandResult DrawDimaLigned(IDocumentEditor docEditor, string[] args)
        {
            var dimAlignedAction = new DimAlignedAction(docEditor);
            dimAlignedAction.ExecCreate(args);
            return CommandResult.Succ();
        }


        [CommandMethod(Name = "STRETCH", ShortCuts = "STR")]
        public CommandResult StretchEle(IDocumentEditor docEditor, string[] args)
        {
            var stretchAction = new StretchAction(docEditor);
            stretchAction.ExecCreate(args);
            return CommandResult.Succ();
        }






        [CommandMethod(Name = "DIMANGULARARC", ShortCuts = "DAN")]
        public CommandResult DrawDimangulararc(IDocumentEditor docEditor, string[] args)
        {
            var dimangulararcAction = new DimAngularArcAction(docEditor);
            dimangulararcAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "LAYER", ShortCuts = "LA")]
        public CommandResult LayerManage(IDocumentEditor docEditor, string[] args)
        {
            var layerManageAction = new LayerManageAction(docEditor);
            layerManageAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "DRAWINGORDER", ShortCuts = "DWO")]
        public CommandResult PreposeManage(IDocumentEditor docEditor, string[] args)
        {
            var drawingOrderAction = new DrawingOrderAction(docEditor);
            drawingOrderAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        //[CommandMethod(Name = "DRAWINGORDER_POST", ShortCuts = "POST")]
        //public CommandResult PostposeManage(IDocumentEditor docEditor, string[] args)
        //{
        //    var changeSeatAction = new DrawingOrderAction(docEditor);
        //    changeSeatAction.Postpose(args);
        //    return CommandResult.Succ();
        //}

        //[CommandMethod(Name = "DRAWINGORDER_UP", ShortCuts = "UP")]
        //public CommandResult ChangeSeatUP(IDocumentEditor docEditor, string[] args)
        //{
        //    var changeSeatAction = new DrawingOrderAction(docEditor);
        //    changeSeatAction.ChangeSeatUP(args);
        //    return CommandResult.Succ();
        //}

        //[CommandMethod(Name = "DRAWINGORDER_DOWN", ShortCuts = "DOWN")]
        //public CommandResult ChangeSeatDOWN(IDocumentEditor docEditor, string[] args)
        //{
        //    var changeSeatAction = new DrawingOrderAction(docEditor);
        //    changeSeatAction.ChangeSeatDOWN(args);
        //    return CommandResult.Succ();
        //}



        [CommandMethod(Name = "TRANSPARENCY", ShortCuts = "")]
        public CommandResult TransparencyManage(IDocumentEditor docEditor, string[] args)
        {

            return CommandResult.Succ();
        }
        [CommandMethod(Name = "LINETYPE", ShortCuts = "LT")]

        public CommandResult LineTypeManage(IDocumentEditor docEditor, string[] args)
        {
            var linetypeManage = new LineTypeManageAction(docEditor);
            linetypeManage.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "ExportToPdf", ShortCuts = "PDF")]

        public CommandResult ExportToPdf(IDocumentEditor docEditor, string[] args)
        {
            SkiaPdfMaker.ExportToPdf(docEditor);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "AxisGrid", ShortCuts = "AG")]

        public CommandResult AxisGrid(IDocumentEditor docEditor, string[] args)
        {
            AxisGridAction axisLineAction = new AxisGridAction(docEditor);
            axisLineAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "AxisNum", ShortCuts = "AN")]
        public CommandResult AxisNum(IDocumentEditor docEditor, string[] args)
        {
            AxisGridAction axisLineAction = new AxisGridAction(docEditor);
            axisLineAction.ExecCreate(new string[] { "AxisNum" });
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "BREAK", ShortCuts = "BR")]
        public CommandResult Break(IDocumentEditor docEditor, string[] args)
        {
            var breakAction = new BreakAction(docEditor);
            breakAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "JOIN", ShortCuts = "J")]
        public CommandResult Join(IDocumentEditor docEditor, string[] args)
        {
            var joinAction = new JoinAction(docEditor);
            joinAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "QuickSelect", ShortCuts = "QS")]
        public CommandResult QuickSelect(IDocumentEditor docEditor, string[] args)
        {
            var qsAction = new QuickSelectAction(docEditor);
            qsAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "SelAll", ShortCuts = "")]
        public CommandResult SelAll(IDocumentEditor docEditor, string[] args)
        {
            docEditor.SelectAll();
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "Find", ShortCuts = "")]
        public CommandResult Find(IDocumentEditor docEditor, string[] args)
        {
            var findAction = new FindAction(docEditor);
            findAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "RasterImage", ShortCuts = "RImage")]
        public CommandResult RasterImage(IDocumentEditor docEditor, string[] args)
        {
            var imageAction = new RasterImageAction(docEditor);
            imageAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "Table", ShortCuts = "")]
        public CommandResult Table(IDocumentEditor docEditor, string[] args)
        {
            var tableAction = new TableAction(docEditor);
            tableAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "TableEdit", ShortCuts = "")]
        public CommandResult TableEdit(IDocumentEditor docEditor, string[] args)
        {
            var tableEditAction = new TableEditAction(docEditor);
            tableEditAction.ExecCreate(args);
            return CommandResult.Succ();
        }

        [CommandMethod(Name = "LoadPat", ShortCuts = "lp")]
        public CommandResult LoadPat(IDocumentEditor docEditor, string[] args)
        {
            var pattern = HatchPattern.Load(@"C:\Users\ASUS\Desktop\图案填充\QD_ANSI32.pat", "QD_ANSI32");
            Box2 BoundingBox = new Box2(new Vector2(), new Vector2(1000, 1000));
            var max = BoundingBox.Max;
            var min = BoundingBox.Min;
            List<LcLine> lines = new List<LcLine>();
            var doc = docEditor.DocRt.Document;

            foreach (HatchPatternLineDefinition hatchPatternLine in pattern.LineDefinitions)
            {
                var angle = hatchPatternLine.Angle;
                var origin = hatchPatternLine.Origin;
                double radians = (Math.PI / 180) * angle;
                Vector2 comVec = new Vector2(1, 0);
                Vector2 dirVec = comVec.RotateAround(origin, radians);
                LcRay ray = new LcRay(origin, dirVec);
                var interPs = LcGeoUtils.IntersectRayBox(ray, BoundingBox);
                var interP = interPs.OrderByDescending(p => Vector2.Distance(ray.StartPoint, p)).FirstOrDefault();
                var delta = hatchPatternLine.Delta;
                double f = min.X / delta.X;
                var px1 = min.X == 0 || delta.X == 0 ? 0 : (int)Math.Floor(f);
                f = min.Y / delta.Y;
                var py1 = min.Y == 0 || delta.Y == 0 ? 0 : (int)Math.Floor(f);
                f = max.X / delta.X;
                var px2 = max.X == 0 || delta.X == 0 ? 1 : (int)Math.Floor(f);
                f = max.Y / delta.Y;
                var py2 = max.Y == 0 || delta.Y == 0 ? 1 : (int)Math.Floor(f);
                Vector2 dvx = new Vector2(radians) * delta.X;
                Vector2 dvy = new Vector2(radians + Math.PI * 0.5) * delta.Y;
                for (int px = px1; px < px2; px++)
                {
                    for (int py = py1; py < py2; py++)
                    {
                        var moveValue = new Vector2((delta.X * px) , (delta.Y * py));
                        LcLine line =doc.CreateObject<LcLine>( );
                        line.Start = origin.Clone();
                        line.End = interP.Clone();
                        line.Translate(moveValue);

                        lines.Add(line);
                    }
                }

            }
            foreach (var item in lines)
            {
                doc.ModelSpace.InsertElement(item);

            }
            return CommandResult.Succ();
        }
    }
}
