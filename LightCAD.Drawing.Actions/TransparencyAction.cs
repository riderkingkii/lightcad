﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing.Actions
{
    public class TransparencyAction
    {
        public static string CommandName;
        public static LcCreateMethod[] CreateMethods;
        protected CancellationTokenSource cts;
        protected bool isFinish;
        protected DocumentRuntime docRt;
        protected ViewportRuntime vportRt;
        protected IDocumentEditor docEditor;
        protected ICommandControl commandCtrl;

        TransparencyAction()
        {
            CreateMethods = new LcCreateMethod[0];
            CreateMethods[0] = new LcCreateMethod() 
            {
                Name = "TRANSPARENCY",
                Description = "透明度",
                Steps = new LcCreateStep[] 
                {
                    new LcCreateStep { Name = "Step0", Options = "输入 TRANSPARENCY 的新值 <" + this.vportRt.Transparency + ">:" },
                }
            };
        }

        public TransparencyAction(IDocumentEditor docEditor)
        {
            this.docEditor = docEditor;
            this.docRt = docEditor.DocRt;
            this.vportRt = (docEditor as DrawingEditRuntime).ActiveViewportRt;
            this.commandCtrl = this.vportRt.CommandCtrl;
        }

        public void TransaprencyTransform(string[] args)
        {
            var curMethod = CreateMethods[0];
        }
    }
}
