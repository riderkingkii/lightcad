﻿


namespace LightCAD.Drawing.Actions
{
    public class CmdTextInputer : Inputer
    {
        public class InputText
        {
            public string ValueX { get; set; }

            public InputText()
            {
            }
            public InputText(string value)
            {
                ValueX = value;

            }

        }
        private string option;
        public CmdTextInputer(IDocumentEditor docEditor) : base(docEditor)
        {
        }
        public async Task<InputText> Execute(string prompt)
        {
            option = null;
            var vportRt = this.vportRt;
            vportRt.CursorType = LcCursorType.InputPoint;
            vportRt.SetStatus(ViewportStatus.CmdTextInInput, false);
            this.commandCtrl.SetAlternateCommandState(false);
            this.Reset();
            this.AttachEvents();
            Prompt(prompt);
            await WaitFinish();
            this.DetachEvents();
            this.commandCtrl.SetAlternateCommandState(true);
            vportRt.RemoveStatus(ViewportStatus.CmdTextInInput);
            vportRt.CursorType = LcCursorType.SelectElement;
            if (this.isCancelled)
                return null;
            else
            {
                return new InputText(option);
            }
        }


        protected override void OnInputBoxKeyDown(KeyEventRuntime e)
        {
            base.OnInputBoxKeyDown(e);
            if (e.KeyCode == 13 || e.KeyCode == 32)//Keys.Enter
            {
                var text = this.commandCtrl.GetInputText().Trim();
                this.commandCtrl.SetInputText(string.Empty);
                this.option = text;
                if (this.option == string.Empty)
                {
                    this.option = "";
                }
                this.Finish();
            }

        }
    }
}
