﻿
namespace LightCAD.Drawing.Actions
{
    public class ElementSetInputer : Inputer
    {
        private List<LcElement> inputElements; 
        private string option;
        public ViewportStatus Status { get; set; }
        public ElementSetInputer(IDocumentEditor docEditor) : base(docEditor)
        {
        }
        public async Task<InputResult> Execute(string prompt)
        {
            inputElements = new List<LcElement>();
            option = null;
            this.commandCtrl.SetAlternateCommandState(true);
            this.Reset();
            this.AttachEvents();
            Prompt(prompt);
            await WaitFinish();
            this.DetachEvents();
            this.commandCtrl.SetAlternateCommandState(false);
            return new InputResult(inputElements, option);
        }

        protected override void OnViewportMouseDown(MouseEventRuntime e)
        {
            var wcsp = this.vportRt.ConvertScrToWcs(e.Location.ToVector2d());
            var wsize = this.vportRt.ConvertScrToWcs(Constants.SelectBoxSize);
            var hitResult = this.vportRt.HitSelect(this.docRt.Document.ModelSpace, wcsp, wsize);
            // this.inputp = e.Location.ToVector2d();
            if (hitResult != null && hitResult.Item1 != null)
            {
                if (!inputElements.Contains(hitResult.Item1))
                {
                    inputElements.Add(hitResult.Item1);
                }
            }

            if (hitResult == null)
            {
                this.vportRt.SetStatus(ViewportStatus.RectSelect, false);
                this.vportRt.PointerPressedPosition = e.Location;
            }
        }
        protected override void OnViewportMouseMove(MouseEventRuntime e)
        {
            base.OnViewportMouseMove(e);
            if (this.vportRt.CheckStatus(ViewportStatus.RectSelect))
            {
                this.vportRt.PointerMovedPosition = e.Location;
            }
        }
        protected override void OnViewportMouseUp(MouseEventRuntime e)
        {
            base.OnViewportMouseUp(e);
            ////this.vportRt.PointerReleasedPosition = e.Location;
            //////var fpw = this.vportRt.ConvertScrToWcs(this.vportRt.PointerPressedPosition.ToVector2d());
            //////var spw = this.vportRt.ConvertScrToWcs(e.Location.ToVector2d());
            //////var selectBox = new Box2d(fpw, spw);
            //////var elements = this.docRt.Action.RectSelects(this.docRt.Document.ModelSpace, selectBox, spw.X > fpw.X);
            ////////this.docRt.Action.ResetElementGrips(elements); this.vportRt.RectSelects()
            ////inputElements.AddRange(elements);
            this.vportRt.RemoveStatus(ViewportStatus.RectSelect);
            inputElements = this.docRt.Action.SelectedElements;
            this.commandCtrl.WriteInfo("鼠标框选找到"+ inputElements.Count().ToString()+ "个");
        }
        protected override void OnInputBoxKeyDown(KeyEventRuntime e)
        {
            base.OnInputBoxKeyDown(e);
            if (e.KeyCode == 13 || e.KeyCode == 32)//Keys.Enter
            {
                var text = this.commandCtrl.GetInputText().Trim();
                this.commandCtrl.SetInputText(string.Empty);
                this.option = text;
                if (this.option == string.Empty)
                {
                    this.option = "";
                }
                this.Finish();
            }

        }
    }
}
