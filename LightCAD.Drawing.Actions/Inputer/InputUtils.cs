﻿
namespace LightCAD.Drawing.Actions
{
    public class InputUtils
    {
        public static Vector2 ParsePoint(string input)
        {
            try
            {
                if (input.Contains(','))
                {
                    var parts = input.Split(',');
                    if (parts.Length <= 1) return null;
                    return new Vector2(double.Parse(parts[0]), double.Parse(parts[1]));
                }
                else if (input.Contains('，'))
                {
                    var parts = input.Split('，');
                    if (parts.Length <= 1) return null;
                    return new Vector2(double.Parse(parts[0]), double.Parse(parts[1]));
                }
                else
                { return null; }

            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="elements"></param>
        /// <param name="hitPoint"></param>
        /// <param name="selectSize"></param>
        /// <returns></returns>
        public static Tuple<LcElement, bool> ElementsSelect(ElementCollection elements, Vector2 hitPoint, double selectSize)
        {
            for (var i = elements.Count - 1; i >= 0; i--)
            {
                var ele = elements[i];
                var testBox = new Box2(new Vector2(hitPoint.X - selectSize, hitPoint.Y - selectSize), selectSize * 2, selectSize * 2);
                var intersected = ele.IntersectWithBox(testBox.ToPolygon());
                if (intersected)
                {
                    if (ele.IsSelected)
                    {
                        return new Tuple<LcElement, bool>(ele, false);
                    }
                    else
                    {
                        return new Tuple<LcElement, bool>(ele, true);
                    }
                }
            }
            return null;
        }
    }
}
