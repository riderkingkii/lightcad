﻿
namespace LightCAD.Drawing.Actions
{
    public class ElementInputer : Inputer
    {
        private LcElement inputElement;
        private Vector2 inputp;
        private string option;
        public ElementInputer(IDocumentEditor docEditor) : base(docEditor)
        {
        }
        public async Task<InputResult> Execute(string prompt)
        {
            inputp = null;
            option = null;
            this.commandCtrl.SetAlternateCommandState(false);
            this.Reset();
            this.AttachEvents();
            Prompt(prompt);
            await WaitFinish();
            this.DetachEvents();
            this.commandCtrl.SetAlternateCommandState(true);
            if (this.isCancelled)
                return null;
            else
            {
                
                return new InputResult(inputElement, option, this.inputp);
            }
            
        }

        protected override void OnViewportMouseDown(MouseEventRuntime e)
        {
            var wcsp = this.vportRt.ConvertScrToWcs(e.Location.ToVector2d());
            this.inputp = wcsp;
            var wsize = this.vportRt.ConvertScrToWcs(Constants.SelectBoxSize);
            var hitResult = this.vportRt.HitSelect(this.docRt.Document.ModelSpace, wcsp, wsize);
            if (hitResult != null && hitResult.Item1 != null)
            {
                inputElement = hitResult.Item1;
                this.commandCtrl.WriteInfo(this.commandCtrl.GetPrompt());
                this.Finish();
            }
        }

        protected override void OnInputBoxKeyDown(KeyEventRuntime e)
        {
            base.OnInputBoxKeyDown(e);
            if (e.KeyCode == 13 || e.KeyCode == 32)//Keys.Enter
            {
                var text = this.commandCtrl.GetInputText().Trim();
                this.commandCtrl.SetInputText(string.Empty);
                //TODO:检查可以输入的字符
                this.inputp = InputUtils.ParsePoint(text);
                if (this.inputp == null)
                {
                    this.option = text;
                    if (this.option == string.Empty)
                    {
                        this.option = "";
                    }
                }
                this.Finish();
            }
        }
    }
}
