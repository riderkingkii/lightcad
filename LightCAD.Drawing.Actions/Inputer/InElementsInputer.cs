﻿
namespace LightCAD.Drawing.Actions
{
    public class InElementsInputer : Inputer
    {
        private LcElement inputElement;
        private Vector2 inputp;
        private string option;
        private ElementCollection Elements;
        private Matrix3 Matrix;
        protected ViewportRuntime vportRt;
        public InElementsInputer(IDocumentEditor docEditor, ElementCollection elements, Matrix3 matrix =null) : base(docEditor)
        {
            if (matrix == null)
                matrix = new Matrix3();

            Elements = elements;
            Matrix= matrix;
        }
        public async Task<InputResult> Execute(string prompt)
        {
            inputp = null;
            option = null;
            this.Reset();
            this.AttachEvents();
            Prompt(prompt);
            await WaitFinish();
            this.DetachEvents();
            if (this.isCancelled)
                return null;
            else
            {
                return new InputResult(inputElement, option);
            }
        }

        protected override void OnViewportMouseDown(MouseEventRuntime e)
        {
            this.inputp = e.Location.ToVector2d();
            var wcsp = this.vportRt.ConvertScrToWcs(e.Location.ToVector2d());
            var wsize = this.vportRt.ConvertScrToWcs(Constants.SelectBoxSize);

            var hitResult= InputUtils.ElementsSelect(Elements, wcsp, wsize);
            //var hitResult = docRt.Action.HitSelect(this.docRt.Document.ModelSpace, wcsp, wsize);
            if (hitResult != null && hitResult.Item1 != null)
            {
                inputElement = hitResult.Item1;
                this.commandCtrl.WriteInfo(this.commandCtrl.GetPrompt());
                this.Finish();
            }
        }

        protected override void OnInputBoxKeyDown(KeyEventRuntime e)
        {
            base.OnInputBoxKeyDown(e);
            if (e.KeyCode == 13 || e.KeyCode == 32)//Keys.Enter
            {
                var text = this.commandCtrl.GetInputText().Trim();
                this.commandCtrl.SetInputText(string.Empty);
                //TODO:检查可以输入的字符
                this.inputp = InputUtils.ParsePoint(text);
                if (this.inputp == null)
                {
                    this.option = text;
                    if (this.option == string.Empty)
                    {
                        this.option = "";
                    }
                }
                this.Finish();
            }


        }
    }
}
