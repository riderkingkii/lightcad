﻿
using LightCAD.Drawing.Actions.Action;

namespace LightCAD.Drawing.Actions
{
    public class Loader
    {
        /// <summary>
        /// 调用此方法，确保程序集加载到内存，并初始化相关类
        /// #ToDo 做一种自动注册的机制
        /// </summary>
        public static void Initilize()
        {
            LineAction.Initilize();
            CircleAction.Initilize();
            EllipseAction.Initilize();
            RayAction.Initilize();
            XLineAction.Initilize();
            GroupAction.Initilize();
            BlockRefAction.Initilize();
            PolyLineAction.Initilize();
            RectangAction.Initilize();
            ArcAction.Initilize();
            BasePointAction.Initilize();
            DrawingFrameAction.Initilize();
            DimRadialAction.Initilize();
            //直径
            DimDiameterAction.Initilize();
            //弧长
            DimArcAction.Initilize();
            //坐标
            DimOrdinateAction.Initilize();
            //文字
            TextAction.Initilize();
            //点
            PointAction.Initilize();
            //折弯
            DimJoggedActio.Initilize();        
            ComponentRefAction.Initilize();
            DirectComponentAction.Initilize();
            DimRotatedAction.Initilize();
            DimAlignedAction.Initilize();
			//角度
            DimAngularArcAction.Initilize();           
            MTextAction.Initilize();
            StretchAction.Initilize();
            //轴网
            AxisGridAction.Initilize();
            AxisLineAction.Initilize();
            BreakAction.Initilize();
            JoinAction.Initilize();
            // 图像
            RasterImageAction.Initilize();
            // 表格
            TableAction.Initilize();
            TableEditAction.Initilize();

            SolidAction.Initialize();
            SectionAction.Initilize();
        }
    }
}
