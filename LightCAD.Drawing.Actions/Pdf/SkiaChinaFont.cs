﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing.Actions
{
    /// <summary>
    /// Skia中文字体
    /// </summary>
    public static class SkiaChinaFont
    {
        public static SKTypeface ChinaFont { get; private set; }

        static SkiaChinaFont()
        {
            //加载字体资源文件方案，需要把字体文件复制运行目录下，设置文件属性为如果较新则复制
            string fontPath = Path.Combine(AppContext.BaseDirectory, "DroidSansFallback.ttf");
            ChinaFont = SKTypeface.FromFile(fontPath);
        }
    }
}
