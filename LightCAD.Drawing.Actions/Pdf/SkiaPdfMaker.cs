﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using LightCAD.Runtime;
using SkiaSharp;
using System;
using System.IO;
using LightCAD.MathLib;

namespace LightCAD.Drawing.Actions
{
    /// <summary>
    /// 创建PDF绘图文件
    /// </summary>
    public class SkiaPdfMaker
    {
        /// <summary>
        /// 每毫米对应多少打印点数 = 每英寸打印多少点数 / 每英寸多少毫米 = 2.83464567
        /// Dpi打印分辨率，Dot Per Inch，每英寸打印点数，默认72
        /// 1英寸=25.4毫米
        /// </summary>
        public const float DotPermm = SKDocument.DefaultRasterDpi / 25.4f;

        public async static void ExportToPdf(IDocumentEditor docEditor)
        {
            var filePath = await AppRuntime.UISystem.SaveFileDialog();
            if (filePath == null) return;
            SkiaPdfMaker itextPdfMaker = new SkiaPdfMaker();
            DocEditor = docEditor;
            System.IO.File.WriteAllBytes(filePath, itextPdfMaker.Export());
        }
        private static IDocumentEditor DocEditor;
        /// <summary>
        /// 导出PDF文件
        /// </summary>
        /// <returns></returns>
        public byte[] Export()
        {
            var lcDocument = DocEditor.DocRt.Document;
            using MemoryStream stream = new MemoryStream();
            var pdfMetadata = new SKDocumentPdfMetadata
            {
                Author = "SkiaPdfMaker",
            };
            //初始化一个PdfDocument类实例
            using SKDocument pdfDocument = SKDocument.CreatePdf(stream, pdfMetadata);

            //新添加一页纸张，创建纸张大小的画布，A4尺寸 = 210 X 297 mm
            float paperWidth = DotPermm * 210;
            float paperHeight = DotPermm * 297;
            var canvas = pdfDocument.BeginPage(paperWidth, paperHeight);
            //var DcsToScr = Matrix3d.Translate(paperWidth / 2, paperHeight / 2) * Matrix3d.Scale(1,-1);
            //var WcsToDcs = Matrix3d.Translate(paperWidth/2, paperHeight/2) * Matrix3d.Scale(0.05);
            //var WcsToScr = DcsToScr * WcsToDcs;
            using var paint = new SKPaint
            {
                Color = SKColors.Black,
                IsAntialias = true,
                Typeface = SkiaChinaFont.ChinaFont,
                TextSize = 24
            };
            var viewportRuntime = (DocEditor as DrawingEditRuntime).ActiveViewportRt;
            var viewport = viewportRuntime.Viewport;
            viewport.Width = paperWidth;
            viewport.Height = paperHeight;
            viewport.Center = new Vector2();
            foreach (var element in lcDocument.ModelSpace.Elements)
            {
                if (element is LcDrawing)
                {

                    LcDrawing lcDrawing = (LcDrawing)element;
                    viewport.Scale = paperWidth/lcDrawing.BoundingBox.Width;
                    viewportRuntime.ResetCoordMatrix();
                    var drawingAction = (ElementAction)lcDrawing.RtAction;
                    Vector2 offset = -lcDrawing.BoundingBox.Center;
                    drawingAction.SetViewport(viewportRuntime);
                    drawingAction.Draw(canvas, lcDrawing, offset);
                    foreach (var lcElement in lcDrawing.Elements.Values)
                    {
                        var eleAction = (lcElement.RtAction as ElementAction);
                        eleAction.SetViewport(viewportRuntime);
                        eleAction.Draw(canvas, lcElement, offset);
                    }
                }
            }

            //结束一页内容
            pdfDocument.EndPage();

            //结束文档内容
            pdfDocument.Close();

            byte[] ary = stream.ToArray();

            return ary;
        }
    }
}
