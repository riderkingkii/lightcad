﻿using System.ComponentModel;
using System.Reflection;

namespace LightCAD.Drawing.Actions
{
    public static class EnumExtensions
    {
        //public static CategoryAttribute GetCategoryAttribute(this Enum value)
        //{
        //    var fieldInfo = value.GetType().GetField(value.ToString());
        //    var attributes = fieldInfo.GetCustomAttributes(typeof(CategoryAttribute), false) as CategoryAttribute[];
        //    return attributes?.Length > 0 ? attributes[0] : null;
        //}

        public static string GetDescription(this Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());

            if (fieldInfo == null)
            {
                return null; // 枚举成员不存在
            }

            DescriptionAttribute[] attributes = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            return attributes?.FirstOrDefault()?.Description;
        }

        public static Dictionary<TEnum, string> GetAllDescriptions<TEnum>(string categoryName = "")
            where TEnum : Enum
        {
            Dictionary<TEnum, string> descriptions = new Dictionary<TEnum, string>();

            foreach (TEnum enumValue in Enum.GetValues(typeof(TEnum)))
            {
                var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
                var attributes = fieldInfo.GetCustomAttributes(typeof(CategoryAttribute), false) as CategoryAttribute[];
                var category = attributes?.Length > 0 ? attributes[0].Category : null;
                if (category == null || category.Contains(categoryName))
                {
                    string description = enumValue.GetDescription();
                    if (!string.IsNullOrEmpty(description))
                    {
                        descriptions.Add(enumValue, description);
                    }
                }
            }

            return descriptions;
        }

        // 根据 Description 获取枚举值的通用方法
        public static TEnum GetEnumValueFromDescription<TEnum>(string description) where TEnum : Enum
        {
            foreach (TEnum value in Enum.GetValues(typeof(TEnum)))
            {
                FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
                DescriptionAttribute[] attributes = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

                if (attributes != null && attributes.Length > 0 && attributes[0].Description.Contains(description))
                {
                    return value;
                }
            }

            // 如果未找到匹配的枚举值，你可以选择返回默认值或抛出异常，这里返回最后一个枚举值作为示例
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>().LastOrDefault();
        }
    }

    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class CategoryAttribute : Attribute
    {
        public string Category { get; }

        public CategoryAttribute(string category)
        {
            Category = category;
        }
    }

}
