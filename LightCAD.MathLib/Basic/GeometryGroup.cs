﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public class GeometryGroup
    {
        /// <summary>
        /// 例如一个圆柱体，由Top，Bottom及Around三个面组成
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 组的数据是否在光滑曲面上，Around圆柱面就是光滑曲面
        /// 光滑曲面上的三角面之间的线是可以隐藏的，形成投影图时页不需要
        /// </summary>
        public bool IsSmooth { get; set; }
        public int Start;
        public int Count;
        public int MaterialIndex;
        public GeometryGroup Clone()
        {
            return new GeometryGroup
            {
                Name = this.Name,
                IsSmooth = this.IsSmooth,
                Start = this.Start,
                Count = this.Count,
                MaterialIndex = MaterialIndex
            };
        }
    }
}
