using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using LightCAD.MathLib;

namespace LightCAD.MathLib
{
    public sealed class Box2
    {
        #region scope properties or methods
        //private static Vector2 _vector = new Vector2();
        private Box2Context GetContext()
        {
            return ThreadContext.GetCurrThreadContext().Box2Ctx;
        }
        #endregion

        /// <summary>
        /// 长宽为0的矩形
        /// </summary>
        public static readonly Box2 Zero = new Box2
        {
            Min = default,
            Max = default
        };

        /// <summary>
        /// 空白(无效的)矩形
        /// </summary>
        public static Box2 Empty { get { return new Box2().MakeEmpty(); } }


        public Vector2 LeftBottom
        {
            [MethodImpl((MethodImplOptions)768)]
            get
            {
                return Min.Clone();
            }
        }

        public Vector2 LeftTop
        {
            [MethodImpl((MethodImplOptions)768)]
            get
            {
                return new Vector2(this.Min.X, this.Max.Y);
            }
        }

        public Vector2 RightTop
        {
            [MethodImpl((MethodImplOptions)768)]
            get
            {
                return this.Max.Clone();
            }
        }

        public Vector2 RightBottom
        {
            [MethodImpl((MethodImplOptions)768)]
            get
            {
                return new Vector2(this.Max.X, this.Min.Y);
            }
        }
        public Vector2 Center
        {
            [MethodImpl((MethodImplOptions)768)]
            get
            {
                return this.GetCenter();
            }
        }

        public double Width
        {
            [MethodImpl((MethodImplOptions)768)]
            get => this.Max.X - this.Min.X;
        }
        public double Height
        {
            [MethodImpl((MethodImplOptions)768)]
            get => this.Max.Y - this.Min.Y;
        }

        #region Properties

        public Vector2 Min;
        public Vector2 Max;

        #endregion

        #region constructor
        public Box2(Vector2 min = null, Vector2 max = null)
        {
            if (min == null) min = new Vector2(double.PositiveInfinity, double.PositiveInfinity);
            if (max == null) max = new Vector2(double.NegativeInfinity, double.NegativeInfinity);
            this.Min = min;
            this.Max = max;
        }
        public Box2(double minx, double miny, double maxx, double maxy)
        {
            this.Min = new Vector2(minx, miny);
            this.Max = new Vector2(maxx, maxy);
        }
        public Box2(Vector2 location, double width, double height)
        {
            this.Min = location;
            this.Max = new Vector2(location.X + width, location.Y + height);
        }
        #endregion

        #region methods
        [MethodImpl((MethodImplOptions)768)]
        public Box2 Set(Vector2 min, Vector2 max)
        {
            this.Min.Copy(min);
            this.Max.Copy(max);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 SetFromPoints(params Vector2[] points)
        {
            this.MakeEmpty();
            foreach (var p in points)
            {
                this.ExpandByPoint(p);
            }
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 SetFromPoints(IList<Vector2> points)
        {
            this.MakeEmpty();
            foreach (var p in points)
            {
                this.ExpandByPoint(p);
            }
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 SetFromCenterAndSize(Vector2 center, Vector2 size)
        {
            Vector2 halfSize = GetContext()._vector.Copy(size).MultiplyScalar(0.5);
            this.Min.Copy(center).Sub(halfSize);
            this.Max.Copy(center).Add(halfSize);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 Clone()
        {
            return new Box2().Copy(this);
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 Copy(Box2 box)
        {
            this.Min.Copy(box.Min);
            this.Max.Copy(box.Max);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 MakeEmpty()
        {
            this.Min.X = this.Min.Y = double.PositiveInfinity;
            this.Max.X = this.Max.Y = double.NegativeInfinity;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public bool IsEmpty()
        {
            // this is a more robust check for empty than ( volume <= 0 ) because volume can get positive with two negative axes
            return (this.Max.X < this.Min.X) || (this.Max.Y < this.Min.Y);
        }
        [MethodImpl((MethodImplOptions)768)]
        public Vector2 GetCenter(Vector2 target = null)
        {
            target = target ?? new Vector2();
            return this.IsEmpty() ? target.Set(0, 0) : target.AddVectors(this.Min, this.Max).MultiplyScalar(0.5);
        }
        [MethodImpl((MethodImplOptions)768)]
        public Vector2 GetSize(Vector2 target = null)
        {
            target = target ?? new Vector2();
            return this.IsEmpty() ? target.Set(0, 0) : target.SubVectors(this.Max, this.Min);
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 ExpandByPoints(params Vector2[] points)
        {
            Vector2 min = new Vector2(double.MaxValue, double.MaxValue);
            Vector2 max = new Vector2(double.MinValue, double.MinValue);
            foreach (Vector2 p in points)
            {
                min.X = Math.Min(min.X, p.X);
                min.Y = Math.Min(min.Y, p.Y);
                max.X = Math.Max(max.X, p.X);
                max.Y = Math.Max(max.Y, p.Y);
            }
            this.Union(new Box2(min, max));
            return this;
        }

        [MethodImpl((MethodImplOptions)768)]
        public Box2 ExpandByPoint(Vector2 point)
        {
            this.Min.Min(point);
            this.Max.Max(point);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 ExpandByVector(Vector2 vector)
        {
            this.Min.Sub(vector);
            this.Max.Add(vector);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 ExpandByScalar(double scalar)
        {
            this.Min.AddScalar(-scalar);
            this.Max.AddScalar(scalar);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public bool ContainsPoint(Vector2 point)
        {
            return point.X < this.Min.X || point.X > this.Max.X ||
                point.Y < this.Min.Y || point.Y > this.Max.Y ? false : true;
        }
        [MethodImpl((MethodImplOptions)768)]
        public bool ContainsBox(Box2 box)
        {
            return this.Min.X <= box.Min.X && box.Max.X <= this.Max.X &&
                this.Min.Y <= box.Min.Y && box.Max.Y <= this.Max.Y;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Vector2 GetParameter(Vector2 point, Vector2 target)
        {
            // This can potentially have a divide by zero if the box
            // has a size dimension of 0.
            return target.Set(
                (point.X - this.Min.X) / (this.Max.X - this.Min.X),
                (point.Y - this.Min.Y) / (this.Max.Y - this.Min.Y)
            );
        }
        [MethodImpl((MethodImplOptions)768)]
        public bool IntersectsBox(Box2 box)
        {
            // using 4 splitting planes to rule out intersections
            return box.Max.X < this.Min.X || box.Min.X > this.Max.X ||
              box.Max.Y < this.Min.Y || box.Min.Y > this.Max.Y ? false : true;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Vector2 ClampPoint(Vector2 point, Vector2 target)
        {
            return target.Copy(point).Clamp(this.Min, this.Max);
        }
        [MethodImpl((MethodImplOptions)768)]
        public double DistanceToPoint(Vector2 point)
        {
            return this.ClampPoint(point, GetContext()._vector).DistanceTo(point);
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 Intersect(Box2 box)
        {
            this.Min.Max(box.Min);
            this.Max.Min(box.Max);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 Union(Box2 box)
        {
            this.Min.Min(box.Min);
            this.Max.Max(box.Max);

            if (this.IsEmpty()) this.MakeEmpty();

            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 Translate(Vector2 offset)
        {
            this.Min.Add(offset);
            this.Max.Add(offset);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Box2 Translate(double dx,double dy)
        {
            this.Min.X += dx;
            this.Max.X += dx;
            this.Min.Y += dy;
            this.Max.Y += dy;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public bool Equals(Box2 box)
        {
            return box.Min.Equals(this.Min) && box.Max.Equals(this.Max);
        }
        [MethodImpl((MethodImplOptions)768)]
        public Rect2d ToRect()
        {
            return new Rect2d { Min = LeftBottom, Max = RightTop };
        }
        [MethodImpl((MethodImplOptions)768)]
        public Polygon2d ToPolygon()
        {
            //逆时针方向
            return new Polygon2d()
            {
                Points = new Vector2[] {
                    LeftBottom,RightBottom,RightTop,LeftTop
                }
            };
        }
        [MethodImpl((MethodImplOptions)768)]
        public Polygon2d ToPolygon(Matrix3 matrix)
        {
            //逆时针方向
            return new Polygon2d()
            {
                Points = new Vector2[] {
                    matrix.MultiplyPoint(LeftBottom),
                    matrix.MultiplyPoint(RightBottom),
                    matrix.MultiplyPoint(RightTop),
                    matrix.MultiplyPoint(LeftTop)
                }
            };
        }
        #endregion

    }
    public sealed class Box2Context
    {
        public readonly Vector2 _vector = new Vector2();
    }
}
