using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

using static LightCAD.MathLib.MathEx;

namespace LightCAD.MathLib
{
    public enum ColorSpace
    {
        None,
        SRGB,
        LinearSRGB
    }
    public class HSL
    {
        public double h;
        public double s;
        public double l;
    }
    public class Color
    {
        #region scope properties or methods
        private static JsObj<int> _colorKeywords = new JsObj<int>{ {"aliceblue", 0xF0F8FF},{ "antiquewhite", 0xFAEBD7},{ "aqua", 0x00FFFF},{ "aquamarine", 0x7FFFD4},{ "azure", 0xF0FFFF},{
    "beige", 0xF5F5DC},{ "bisque", 0xFFE4C4},{ "black", 0x000000},{ "blanchedalmond", 0xFFEBCD},{ "blue", 0x0000FF},{ "blueviolet", 0x8A2BE2},{
    "brown", 0xA52A2A},{ "burlywood", 0xDEB887},{ "cadetblue", 0x5F9EA0},{ "chartreuse", 0x7FFF00},{ "chocolate", 0xD2691E},{ "coral", 0xFF7F50},{
    "cornflowerblue", 0x6495ED},{ "cornsilk", 0xFFF8DC},{ "crimson", 0xDC143C},{ "cyan", 0x00FFFF},{ "darkblue", 0x00008B},{ "darkcyan", 0x008B8B},{
    "darkgoldenrod", 0xB8860B},{ "darkgray", 0xA9A9A9},{ "darkgreen", 0x006400},{ "darkgrey", 0xA9A9A9},{ "darkkhaki", 0xBDB76B},{ "darkmagenta", 0x8B008B},{
    "darkolivegreen", 0x556B2F},{ "darkorange", 0xFF8C00},{ "darkorchid", 0x9932CC},{ "darkred", 0x8B0000},{ "darksalmon", 0xE9967A},{ "darkseagreen", 0x8FBC8F},{
    "darkslateblue", 0x483D8B},{ "darkslategray", 0x2F4F4F},{ "darkslategrey", 0x2F4F4F},{ "darkturquoise", 0x00CED1},{ "darkviolet", 0x9400D3},{
    "deeppink", 0xFF1493},{ "deepskyblue", 0x00BFFF},{ "dimgray", 0x696969},{ "dimgrey", 0x696969},{ "dodgerblue", 0x1E90FF},{ "firebrick", 0xB22222},{
    "floralwhite", 0xFFFAF0},{ "forestgreen", 0x228B22},{ "fuchsia", 0xFF00FF},{ "gainsboro", 0xDCDCDC},{ "ghostwhite", 0xF8F8FF},{ "gold", 0xFFD700},{
    "goldenrod", 0xDAA520},{ "gray", 0x808080},{ "green", 0x008000},{ "greenyellow", 0xADFF2F},{ "grey", 0x808080},{ "honeydew", 0xF0FFF0},{ "hotpink", 0xFF69B4},{
    "indianred", 0xCD5C5C},{ "indigo", 0x4B0082},{ "ivory", 0xFFFFF0},{ "khaki", 0xF0E68C},{ "lavender", 0xE6E6FA},{ "lavenderblush", 0xFFF0F5},{ "lawngreen", 0x7CFC00},{
    "lemonchiffon", 0xFFFACD},{ "lightblue", 0xADD8E6},{ "lightcoral", 0xF08080},{ "lightcyan", 0xE0FFFF},{ "lightgoldenrodyellow", 0xFAFAD2},{ "lightgray", 0xD3D3D3},{
    "lightgreen", 0x90EE90},{ "lightgrey", 0xD3D3D3},{ "lightpink", 0xFFB6C1},{ "lightsalmon", 0xFFA07A},{ "lightseagreen", 0x20B2AA},{ "lightskyblue", 0x87CEFA},{
    "lightslategray", 0x778899},{ "lightslategrey", 0x778899},{ "lightsteelblue", 0xB0C4DE},{ "lightyellow", 0xFFFFE0},{ "lime", 0x00FF00},{ "limegreen", 0x32CD32},{
    "linen", 0xFAF0E6},{ "magenta", 0xFF00FF},{ "maroon", 0x800000},{ "mediumaquamarine", 0x66CDAA},{ "mediumblue", 0x0000CD},{ "mediumorchid", 0xBA55D3},{
    "mediumpurple", 0x9370DB},{ "mediumseagreen", 0x3CB371},{ "mediumslateblue", 0x7B68EE},{ "mediumspringgreen", 0x00FA9A},{ "mediumturquoise", 0x48D1CC},{
    "mediumvioletred", 0xC71585},{ "midnightblue", 0x191970},{ "mintcream", 0xF5FFFA},{ "mistyrose", 0xFFE4E1},{ "moccasin", 0xFFE4B5},{ "navajowhite", 0xFFDEAD},{
    "navy", 0x000080},{ "oldlace", 0xFDF5E6},{ "olive", 0x808000},{ "olivedrab", 0x6B8E23},{ "orange", 0xFFA500},{ "orangered", 0xFF4500},{ "orchid", 0xDA70D6},{
    "palegoldenrod", 0xEEE8AA},{ "palegreen", 0x98FB98},{ "paleturquoise", 0xAFEEEE},{ "palevioletred", 0xDB7093},{ "papayawhip", 0xFFEFD5},{ "peachpuff", 0xFFDAB9},{
    "peru", 0xCD853F},{ "pink", 0xFFC0CB},{ "plum", 0xDDA0DD},{ "powderblue", 0xB0E0E6},{ "purple", 0x800080},{ "rebeccapurple", 0x663399},{ "red", 0xFF0000},{ "rosybrown", 0xBC8F8F},{
    "royalblue", 0x4169E1},{ "saddlebrown", 0x8B4513},{ "salmon", 0xFA8072},{ "sandybrown", 0xF4A460},{ "seagreen", 0x2E8B57},{ "seashell", 0xFFF5EE},{
    "sienna", 0xA0522D},{ "silver", 0xC0C0C0},{ "skyblue", 0x87CEEB},{ "slateblue", 0x6A5ACD},{ "slategray", 0x708090},{ "slategrey", 0x708090},{ "snow", 0xFFFAFA},{
    "springgreen", 0x00FF7F},{ "steelblue", 0x4682B4},{ "tan", 0xD2B48C},{ "teal", 0x008080},{ "thistle", 0xD8BFD8},{ "tomato", 0xFF6347},{ "turquoise", 0x40E0D0},{
    "violet", 0xEE82EE},{ "wheat", 0xF5DEB3},{ "white", 0xFFFFFF},{ "whitesmoke", 0xF5F5F5},{ "yellow", 0xFFFF00},{ "yellowgreen", 0x9ACD32 } };
        private static Color _color = new Color() { R = 0, G = 0, B = 0 };
        private static HSL _hslA = new HSL() { h = 0, s = 0, l = 0 };
        private static HSL _hslB = new HSL() { h = 0, s = 0, l = 0 };
        private static double hue2rgb(double p, double q, double t)
        {

            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6.0) return p + (q - p) * 6 * t;
            if (t < 1 / 2.0) return q;
            if (t < 2 / 3.0) return p + (q - p) * 6 * (2 / 3.0 - t);
            return p;

        }
        //private static Color toComponents(Color source, Color target)
        //{

        //    target.r = source.r;
        //    target.g = source.g;
        //    target.b = source.b;
        //    target.a = source.a;

        //    return target;

        //}
        public static bool legacyMode = true;
        public static ColorSpace workingColorSpace
        {
            get { return ColorSpace.LinearSRGB; }
        }

        public static Color random()
        {
            return new Color(MathEx.Random(), MathEx.Random(), MathEx.Random());
        }

        /*图像文件(png,jpg等)中的数据：sRGB
↓ Shader中读取颜色值：进行Gamma展开 （变得更暗）
Shader运算中的数据：Linear
↓ 把颜色值写到FrameBuffer：进行Gamma压缩 （变得更亮）
最终渲染结果的数据：sRGB
↓ 显示到显示器上 进入人眼的数据：Linear
记住：同一个认知意义上的颜色值，当用sRGB空间表示时，要比Linear空间表示时更亮。
比如，亮度为0.5的灰色，我们预期应该是白色亮度的一半。在sRGB空间表示时，却是0.73。*/

        //伽马压缩 Linear to sRGB
        //伽马展开 sRGB to Linear
        public static double SRGBToLinear(double c)
        {
            return (c < 0.04045) ? c * 0.0773993808 : Math.Pow(c * 0.9478672986 + 0.0521327014, 2.4);
        }

        public static double LinearToSRGB(double c)
        {
            return (c < 0.0031308) ? c * 12.92 : 1.055 * (Math.Pow(c, 0.41666)) - 0.055;
        }
        public static Color convert(Color color, ColorSpace source, ColorSpace target)
        {
            if (legacyMode || source == target || source == ColorSpace.None || target == ColorSpace.None)
            {
                return color;
            }
            if (source == ColorSpace.LinearSRGB && target == ColorSpace.SRGB)
            {
                color.R = LinearToSRGB(color.R);
                color.G = LinearToSRGB(color.G);
                color.B = LinearToSRGB(color.B);
            }
            if (source == ColorSpace.SRGB && target == ColorSpace.LinearSRGB)
            {
                color.R = SRGBToLinear(color.R);
                color.G = SRGBToLinear(color.G);
                color.B = SRGBToLinear(color.B);
            }
            return color;
        }
        #endregion

        #region Properties

        public static JsObj<int> NAMES = _colorKeywords;


        public double R;
        public double G;
        public double B;
        public double A = 1;

        #endregion

        #region constructor
        public Color(object r = null, double g = -1, double b = -1, double a = 1)
        {
            this.R = 1;
            this.G = 1;
            this.B = 1;
            if (r == null)
                return;
            if (g == -1 && b == -1)
            {
                // r is THREE.Color, hex or string
                this.Set(r);
                return;
            }
            this.SetRGB(Convert.ToDouble(r), g, b);
            this.A = a;
        }
        #endregion

        #region properties
       
        #endregion

        #region methods
        public Color Set(object value)
        {
            if (value != null && value is Color)
            {
                this.Copy((Color)value);
            }
            else if (value is Int32 || value is UInt32 || value is double)
            {
                this.SetHex(Convert.ToInt32(value));
            }
            else if (value is string)
            {
                this.SetStyle((string)value);
            }
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color SetScalar(double scalar)
        {
            this.R = scalar;
            this.G = scalar;
            this.B = scalar;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color SetHex(int hex, ColorSpace colorSpace = ColorSpace.SRGB)
        {
            this.R = (double)(hex >> 16 & 255) / 255;
            this.G = (double)(hex >> 8 & 255) / 255;
            this.B = (double)(hex & 255) / 255;
            convert(this, colorSpace, workingColorSpace);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color SetRGB(double r, double g, double b, ColorSpace colorSpace = ColorSpace.LinearSRGB)
        {
            this.R = r;
            this.G = g;
            this.B = b;
            convert(this, colorSpace, workingColorSpace);
            return this;
        }
        public Color SetHSL(double h, double s, double l, ColorSpace colorSpace = ColorSpace.LinearSRGB)
        {
            // h,s,l ranges are in 0.0 - 1.0
            h = EuclideanModulo(h, 1);
            s = Clamp(s, 0, 1);
            l = Clamp(l, 0, 1);
            if (s == 0)
            {
                this.R = this.G = this.B = l;
            }
            else
            {
                double p = l <= 0.5 ? l * (1 + s) : l + s - (l * s);
                double q = (2 * l) - p;
                this.R = hue2rgb(q, p, h + 1 / 3.0);
                this.G = hue2rgb(q, p, h);
                this.B = hue2rgb(q, p, h - 1 / 3.0);
            }
            convert(this, colorSpace, workingColorSpace);
            return this;
        }

        public Color SetStyle(string style, ColorSpace colorSpace = ColorSpace.SRGB)
        {
            var lstyle = style.ToLower().Trim();
            if (lstyle.StartsWith("#"))
            {
                var hexStyle = lstyle.Substring(1);
                //#ff0
                if (hexStyle.Length == 3)
                {
                    this.R = int.Parse(string.Empty + hexStyle[0] + hexStyle[0], NumberStyles.HexNumber) / 255.0;
                    this.G = int.Parse(string.Empty + hexStyle[1] + hexStyle[1], NumberStyles.HexNumber) / 255.0;
                    this.B = int.Parse(string.Empty + hexStyle[2] + hexStyle[2], NumberStyles.HexNumber) / 255.0;
                }
                //#ffff00
                else if (hexStyle.Length == 6)
                {
                    this.R = int.Parse(string.Empty + hexStyle[0] + hexStyle[1], NumberStyles.HexNumber) / 255.0;
                    this.G = int.Parse(string.Empty + hexStyle[2] + hexStyle[3], NumberStyles.HexNumber) / 255.0;
                    this.B = int.Parse(string.Empty + hexStyle[4] + hexStyle[5], NumberStyles.HexNumber) / 255.0;
                }
                else if (hexStyle.Length == 8)
                {
                    this.A = int.Parse(string.Empty + hexStyle[0] + hexStyle[1], NumberStyles.HexNumber) / 255.0;
                    this.R = int.Parse(string.Empty + hexStyle[2] + hexStyle[3], NumberStyles.HexNumber) / 255.0;
                    this.G = int.Parse(string.Empty + hexStyle[4] + hexStyle[5], NumberStyles.HexNumber) / 255.0;
                    this.B = int.Parse(string.Empty + hexStyle[6] + hexStyle[7], NumberStyles.HexNumber) / 255.0;
                }
                else
                {
                    console.warn("THREE.Color: Unknown color " + style);
                }
            }
            else if (lstyle.StartsWith("rgba") || lstyle.StartsWith("hsla") || lstyle.StartsWith("rgb") || lstyle.StartsWith("hsl"))
            {
                // rgb(255,0,0) rgba(255,0,0,0.5)
                // hsl(120,50%,50%) hsla(120,50%,50%,0.5)
                var m = Regex.Match(lstyle, @"(rgba|hsla|rgb|hsl)\(([^)]*)\)");
                if (m.Success)
                {
                    if (m.Groups[1].Value == "rgba" || m.Groups[1].Value == "rgb")
                    {
                        var color = m.Groups[2].Value.Split(',');
                        this.R = Math.Min(255, int.Parse(color[0])) / 255.0;
                        this.G = Math.Min(255, int.Parse(color[1])) / 255.0;
                        this.B = Math.Min(255, int.Parse(color[2])) / 255.0;
                        if (color.Length == 4)
                        {
                            this.A = double.Parse(color[3]);
                        }
                    }
                    else if (m.Groups[1].Value == "hsla" || m.Groups[1].Value == "hsl")
                    {
                        var color = m.Groups[2].Value.Split(',');
                        double h = double.Parse(color[0]) / 360;
                        double s = double.Parse(color[1]) / 100;
                        double l = double.Parse(color[2]) / 100;
                        if (color.Length == 4)
                        {
                            this.A = double.Parse(color[3]);
                        }
                    }
                }
                else
                {
                    console.warn("THREE.Color: Unknown color " + style);
                }
            }
            else
            {
                return this.SetColorName(style, colorSpace);
            }
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color SetColorName(string style, ColorSpace colorSpace = ColorSpace.SRGB)
        {
            // color keywords
            int hex = _colorKeywords[style.ToLower()];
            if (hex != 0)
            {
                // red
                this.SetHex(hex, colorSpace);
            }
            else
            {
                // unknown color
                console.warn("THREE.Color: Unknown color " + style);
            }
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public virtual Color Clone()
        {
            return new Color(this.R, this.G, this.B) { A = this.A };
        }
        [MethodImpl((MethodImplOptions)768)]
        public virtual Color Copy(Color color)
        {
            this.R = color.R;
            this.G = color.G;
            this.B = color.B;
            this.A = color.A;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color CopySRGBToLinear(Color color)
        {
            this.R = SRGBToLinear(color.R);
            this.G = SRGBToLinear(color.G);
            this.B = SRGBToLinear(color.B);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color CopyLinearToSRGB(Color color)
        {
            this.R = LinearToSRGB(color.R);
            this.G = LinearToSRGB(color.G);
            this.B = LinearToSRGB(color.B);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color ConvertSRGBToLinear()
        {
            this.CopySRGBToLinear(this);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color ConvertLinearToSRGB()
        {
            this.CopyLinearToSRGB(this);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public int GetHex(ColorSpace colorSpace = ColorSpace.SRGB)
        {
            convert(_color.Copy(this), workingColorSpace, colorSpace);
            return (int)Clamp(_color.R * 255, 0, 255) << 16 ^ (int)Clamp(_color.G * 255, 0, 255) << 8 ^ (int)Clamp(_color.B * 255, 0, 255) << 0;
        }
        [MethodImpl((MethodImplOptions)768)]
        public string GetHexString(ColorSpace colorSpace = ColorSpace.SRGB)
        {
            return ("000000" + this.GetHex(colorSpace).ToString("x")).slice(-6);
        }
        public HSL GetHSL(HSL target, ColorSpace colorSpace = ColorSpace.LinearSRGB)
        {
            // h,s,l ranges are in 0.0 - 1.0
            convert(_color.Copy(this), workingColorSpace, colorSpace);
            double r = _color.R, g = _color.G, b = _color.B;
            double max = MathEx.Max(r, g, b);
            double min = MathEx.Min(r, g, b);
            double hue = 0, saturation;
            double lightness = (min + max) / 2.0;
            if (min == max)
            {
                hue = 0;
                saturation = 0;
            }
            else
            {
                double delta = max - min;
                saturation = lightness <= 0.5 ? delta / (max + min) : delta / (2 - max - min);

                if (max == r) { hue = (g - b) / delta + (g < b ? 6 : 0); }
                else if (max == g) { hue = (b - r) / delta + 2; }
                else if (max == b) { hue = (r - g) / delta + 4; }
                hue /= 6;
            }
            target.h = hue;
            target.s = saturation;
            target.l = lightness;
            return target;
        }
        public Color GetRGB(Color target, ColorSpace colorSpace = ColorSpace.LinearSRGB)
        {
            convert(_color.Copy(this), workingColorSpace, colorSpace);
            target.R = _color.R;
            target.G = _color.G;
            target.B = _color.B;
            return target;
        }
        public string GetStyle(ColorSpace colorSpace = ColorSpace.SRGB)
        {
            convert(_color.Copy(this), workingColorSpace, colorSpace);
            if (colorSpace != ColorSpace.SRGB)
            {
                // Requires CSS Color Module Level 4 (https://www.w3.org/TR/css-color-4/).
                return $"color({colorSpace} ${_color.R} ${_color.G} ${_color.B})";
            }
            return $"rgb({(int)(_color.R * 255)},{(int)(_color.G * 255)},{(int)(_color.B * 255)})";
        }
        public Color OffsetHSL(double h, double s, double l)
        {
            this.GetHSL(_hslA);
            _hslA.h += h; _hslA.s += s; _hslA.l += l;
            this.SetHSL(_hslA.h, _hslA.s, _hslA.l);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color Add(Color color)
        {
            this.R += color.R;
            this.G += color.G;
            this.B += color.B;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color AddColors(Color color1, Color color2)
        {
            this.R = color1.R + color2.R;
            this.G = color1.G + color2.G;
            this.B = color1.B + color2.B;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color AddScalar(double s)
        {
            this.R += s;
            this.G += s;
            this.B += s;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color Sub(Color color)
        {
            this.R = Math.Max(0, this.R - color.R);
            this.G = Math.Max(0, this.G - color.G);
            this.B = Math.Max(0, this.B - color.B);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color Multiply(Color color)
        {
            this.R *= color.R;
            this.G *= color.G;
            this.B *= color.B;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color MultiplyScalar(double s)
        {
            this.R *= s;
            this.G *= s;
            this.B *= s;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color Lerp(Color color, double alpha)
        {
            this.R += (color.R - this.R) * alpha;
            this.G += (color.G - this.G) * alpha;
            this.B += (color.B - this.B) * alpha;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color LerpColors(Color color1, Color color2, double alpha)
        {
            this.R = color1.R + (color2.R - color1.R) * alpha;
            this.G = color1.G + (color2.G - color1.G) * alpha;
            this.B = color1.B + (color2.B - color1.B) * alpha;
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color LerpHSL(Color color, double alpha)
        {
            this.GetHSL(_hslA);
            color.GetHSL(_hslB);
            double h = MathEx.Lerp(_hslA.h, _hslB.h, alpha);
            double s = MathEx.Lerp(_hslA.s, _hslB.s, alpha);
            double l = MathEx.Lerp(_hslA.l, _hslB.l, alpha);
            this.SetHSL(h, s, l);
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public virtual bool Equals(Color c)
        {
            return (c.R == this.R) && (c.G == this.G) && (c.B == this.B);
        }
        [MethodImpl((MethodImplOptions)768)]
        public Color SetFromVector3(Vector3 v)
        {
            this.R = v.X;
            this.G = v.Y;
            this.B = v.Z;
            return this;
        }

        public Color ApplyMatrix3(Matrix3 m)
        {
            double r = this.R, g = this.G, b = this.B;
            var e = m.Elements;

            this.R = e[0] * r + e[3] * g + e[6] * b;
            this.G = e[1] * r + e[4] * g + e[7] * b;
            this.B = e[2] * r + e[5] * g + e[8] * b;

            return this;
        }

        [MethodImpl((MethodImplOptions)768)]
        public Color FromArray(double[] array, int offset = 0)
        {
            this.R = array[offset];
            this.G = array[offset + 1];
            this.B = array[offset + 2];
            return this;
        }
        [MethodImpl((MethodImplOptions)768)]
        public double[] ToArray(double[] array = null, int offset = 0)
        {
            if (array == null) array = new double[3];
            array[offset] = this.R;
            array[offset + 1] = this.G;
            array[offset + 2] = this.B;
            return array;
        }
        [MethodImpl((MethodImplOptions)768)]
        public ListEx<double> ToJsArray(ListEx<double> array = null)
        {
            if (array == null) array = new ListEx<double>();
            array.Push(this.R, this.G, this.B);
            return array;
        }

        [MethodImpl((MethodImplOptions)768)]
        public override string ToString()
        {
            return this.GetHexString();
        }
        public IEnumerator GetEnumerator()
        {
            yield return this.R;
            yield return this.G;
            yield return this.B;
        }
        #endregion

    }

    public class ClearCoatColor : Color
    {
        private double _clearcoat;
        public int version;

        public double Clearcoat
        {
            get
            {
                return this._clearcoat;
            }
            set
            {
                if (this._clearcoat > 0 != value > 0)
                {
                    this.version++;
                }
                this._clearcoat = value;
            }
        }
    }
}
