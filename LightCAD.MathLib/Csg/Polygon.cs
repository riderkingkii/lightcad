﻿using System.Collections.Generic;

namespace LightCAD.MathLib.Csg
{
	/// <summary>
	/// Convex polygons comprised of vertices lying on a plane.
	/// Each polygon also has "Shared" data which is any
	/// metadata (usually a material reference) that you need to
	/// share between sets of polygons.
	/// </summary>
	public class CsgPolygon
	{
		public readonly List<CsgVertex> Vertices;
		public readonly CsgPlane Plane;
		public readonly PolygonShared Shared;

		readonly bool debug = false;

		static readonly PolygonShared defaultShared = new PolygonShared(null);

		BoundingSphere? cachedBoundingSphere;
		BoundingBox? cachedBoundingBox;

		public CsgPolygon(List<CsgVertex> vertices, PolygonShared? shared = null, CsgPlane? plane = null)
		{
			Vertices = vertices;
			Shared = shared ?? defaultShared;
			Plane = plane ?? CsgPlane.FromVector3Ds(vertices[0].Pos, vertices[1].Pos, vertices[2].Pos);
			if (debug)
			{
				//CheckIfConvex();
			}
		}

		public CsgPolygon(params CsgVertex[] vertices)
			: this(new List<CsgVertex>(vertices))
		{
		}

		public BoundingSphere BoundingSphere
		{
			get
			{
				if (cachedBoundingSphere == null)
				{
					var box = BoundingBox;
					var middle = (box.Min + box.Max) * 0.5;
					var radius3 = box.Max - middle;
					var radius = radius3.Length;
					cachedBoundingSphere = new BoundingSphere { Center = middle, Radius = radius };
				}
				return cachedBoundingSphere;
			}
		}

		public BoundingBox BoundingBox
		{
			get
			{
				if (cachedBoundingBox == null)
				{
					CsgVec3 minpoint, maxpoint;
					var vertices = this.Vertices;
					var numvertices = vertices.Count;
					if (numvertices == 0)
					{
						minpoint = new CsgVec3(0, 0, 0);
					}
					else {
						minpoint = vertices[0].Pos;
					}
					maxpoint = minpoint;
					for (var i = 1; i < numvertices; i++)
					{
						var point = vertices[i].Pos;
						minpoint = minpoint.Min(point);
						maxpoint = maxpoint.Max(point);
					}
					cachedBoundingBox = new BoundingBox(minpoint, maxpoint);
				}
				return cachedBoundingBox;
			}
		}

		public CsgPolygon Flipped()
		{
			var newvertices = new List<CsgVertex>(Vertices.Count);
			for (int i = 0; i < Vertices.Count; i++)
			{
				newvertices.Add(Vertices[i].Flipped());
			}
			newvertices.Reverse();
			var newplane = Plane.Flipped();
			return new CsgPolygon(newvertices, Shared, newplane);
		}

		
	}

	public class PolygonShared
	{
		int tag = 0;
		public int Tag {
			get {
				if (tag == 0) {
					tag = CsgSolid.GetTag ();
				}
				return tag;
			}
		}
		public PolygonShared(object color)
		{			
		}
		public string Hash
		{
			get
			{
				return "null";
			}
		}
	}

	public class Properties
	{
		public readonly Dictionary<string, object> All = new Dictionary<string, object>();
		public Properties Merge(Properties otherproperties)
		{
			var result = new Properties();
			foreach (var x in All)
			{
				result.All.Add(x.Key, x.Value);
			}
			foreach (var x in otherproperties.All)
			{
				result.All[x.Key] = x.Value;
			}
			return result;
		}
		public Properties Transform(Matrix4x4 matrix4x4)
		{
			var result = new Properties();
			foreach (var x in All)
			{
				result.All.Add(x.Key, x.Value);
			}
			return result;
		}
	}
}

