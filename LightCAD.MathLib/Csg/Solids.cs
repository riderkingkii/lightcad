﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LightCAD.MathLib.Csg
{
	public static class CsgSolids
	{
		public static CsgSolid Cube(CubeOptions options)
		{
			var c = options.Center;
			var r = options.Radius.Abs; // negative radii make no sense
			if (r.X == 0.0 || r.Y == 0.0 || r.Z == 0.0)
				return new CsgSolid();
			var result = CsgSolid.FromPolygons(cubeData.Select(info =>
			{
				//var normal = new Vector3D(info[1]);
				//var plane = new Plane(normal, 1);
				var vertices = info[0].Select(i =>
				{
					var pos = new CsgVec3(
						c.X + r.X * (2 * ((i & 1) != 0 ? 1 : 0) - 1),
							c.Y + r.Y * (2 * ((i & 2) != 0 ? 1 : 0) - 1),
							c.Z + r.Z * (2 * ((i & 4) != 0 ? 1 : 0) - 1));
					return NoTexVertex(pos);
				});
				return new CsgPolygon(vertices.ToList());
			}).ToList());
			return result;
		}

		public static CsgSolid Cube(double size = 1, bool center = false)
		{
			var r = new CsgVec3(size / 2, size / 2, size / 2);
			var c = center ? new CsgVec3(0, 0, 0) : r;
			return CsgSolids.Cube(new CubeOptions { Radius = r, Center = c });
		}

		public static CsgSolid Cube(double size, CsgVec3 center)
		{
			var r = new CsgVec3(size / 2, size / 2, size / 2);
			var c = center;
			return CsgSolids.Cube(new CubeOptions { Radius = r, Center = c });
		}

		public static CsgSolid Cube(CsgVec3 size, bool center = false)
		{
			var r = size / 2;
			var c = center ? new CsgVec3(0, 0, 0) : r;
			return CsgSolids.Cube(new CubeOptions { Radius = r, Center = c });
		}

		public static CsgSolid Cube(CsgVec3 size, CsgVec3 center)
		{
			var r = size / 2;
			var c = center;
			return CsgSolids.Cube(new CubeOptions { Radius = r, Center = c });
		}

		public static CsgSolid Cube(double width, double height, double depth, bool center = false)
		{
			var r = new CsgVec3(width/2, height/2, depth/2);
			var c = center ? new CsgVec3(0, 0, 0) : r;
			return CsgSolids.Cube(new CubeOptions { Radius = r, Center = c });
		}

		public static CsgSolid Sphere(SphereOptions options)
		{
			var center = options.Center;
			var radius = Math.Abs(options.Radius);
			if (radius == 0.0)
				return new CsgSolid();
			var resolution = options.Resolution;
			var xvector = options.XAxis * radius;
			var yvector = options.YAxis * radius;
			var zvector = options.ZAxis * radius;
			if (resolution < 4) resolution = 4;
			var qresolution = resolution / 4;
			var prevcylinderpoint = new CsgVec3(0,0,0);
			var polygons = new List<CsgPolygon>();
			for (var slice1 = 0; slice1 <= resolution; slice1++)
			{
				var angle = Math.PI * 2.0 * slice1 / resolution;
				var cylinderpoint = xvector * (Math.Cos(angle)) + (yvector * (Math.Sin(angle)));
				if (slice1 > 0)
				{
					double prevcospitch = 0, prevsinpitch = 0;
					for (var slice2 = 0; slice2 <= qresolution; slice2++)
					{
						var pitch = 0.5 * Math.PI * (double)slice2 / qresolution;
						var cospitch = Math.Cos(pitch);
						var sinpitch = Math.Sin(pitch);
						if (slice2 > 0)
						{
							var vertices = new List<CsgVertex>();
							vertices.Add(NoTexVertex(center + (prevcylinderpoint * (prevcospitch) - (zvector * (prevsinpitch)))));
							vertices.Add(NoTexVertex(center + (cylinderpoint * (prevcospitch) - (zvector * (prevsinpitch)))));
							if (slice2 < qresolution)
							{
								vertices.Add(NoTexVertex(center + (cylinderpoint * (cospitch) - (zvector * (sinpitch)))));
							}
							vertices.Add(NoTexVertex(center + (prevcylinderpoint * (cospitch) - (zvector * (sinpitch)))));
							polygons.Add(new CsgPolygon(vertices));
							vertices = new List<CsgVertex>();
							vertices.Add(NoTexVertex(center + (prevcylinderpoint * (prevcospitch) + (zvector * (prevsinpitch)))));
							vertices.Add(NoTexVertex(center + (cylinderpoint * (prevcospitch) + (zvector * (prevsinpitch)))));
							if (slice2 < qresolution)
							{
								vertices.Add(NoTexVertex(center + (cylinderpoint * (cospitch) + (zvector * (sinpitch)))));
							}
							vertices.Add(NoTexVertex(center + (prevcylinderpoint * (cospitch) + (zvector * (sinpitch)))));
							vertices.Reverse();
							polygons.Add(new CsgPolygon(vertices));
						}
						prevcospitch = cospitch;
						prevsinpitch = sinpitch;
					}
				}
				prevcylinderpoint = cylinderpoint;
			}
			var result = CsgSolid.FromPolygons(polygons);
			return result;
		}

		public static CsgSolid Sphere(double r = 1, bool center = true)
		{
			var c = center ? new CsgVec3(0, 0, 0) : new CsgVec3(r, r, r);
			return CsgSolids.Sphere(new SphereOptions { Radius = r, Center = c });
		}

		public static CsgSolid Sphere(double r, CsgVec3 center)
		{
			return CsgSolids.Sphere(new SphereOptions { Radius = r, Center = center });
		}

		public static CsgSolid Cylinder(CylinderOptions options)
		{
			var s = options.Start;
			var e = options.End;
			var r = Math.Abs(options.RadiusStart);
			var rEnd = Math.Abs(options.RadiusEnd);
			var rStart = r;
			var alpha = options.SectorAngle;
			alpha = alpha > 360 ? alpha % 360 : alpha;

			if ((rEnd == 0) && (rStart == 0))
			{
				return new CsgSolid();
			}
			if (s.Equals(e))
			{
				return new CsgSolid();
			}

			var slices = options.Resolution;
			var ray = e - (s);
			var axisZ = ray.Unit;
			var axisX = axisZ.RandomNonParallelVector().Unit;

			var axisY = axisX.Cross(axisZ).Unit;
			axisX = axisZ.Cross(axisY).Unit;
			var start = NoTexVertex(s);
			var end = NoTexVertex(e);
			var polygons = new List<CsgPolygon>();

			Func<double, double, double, CsgVertex> point = (stack, slice, radius) =>
			{
				var angle = slice * Math.PI * alpha / 180;
				var outp = axisX * (Math.Cos(angle)) + (axisY * (Math.Sin(angle)));
				var pos = s + (ray * (stack)) + (outp * (radius));
				return NoTexVertex(pos);
			};

			if (alpha > 0)
			{
				for (var i = 0; i < slices; i++)
				{
					double t0 = (double)i / slices;
					double t1 = (double)(i + 1) / slices;
					if (rEnd == rStart)
					{
						polygons.Add(new CsgPolygon(start, point(0, t0, rEnd), point(0, t1, rEnd)));
						polygons.Add(new CsgPolygon(point(0, t1, rEnd), point(0, t0, rEnd), point(1, t0, rEnd), point(1, t1, rEnd)));
						polygons.Add(new CsgPolygon(end, point(1, t1, rEnd), point(1, t0, rEnd)));
					}
					else {
						if (rStart > 0)
						{
							polygons.Add(new CsgPolygon(start, point(0, t0, rStart), point(0, t1, rStart)));
							polygons.Add(new CsgPolygon(point(0, t0, rStart), point(1, t0, rEnd), point(0, t1, rStart)));
						}
						if (rEnd > 0)
						{
							polygons.Add(new CsgPolygon(end, point(1, t1, rEnd), point(1, t0, rEnd)));
							polygons.Add(new CsgPolygon(point(1, t0, rEnd), point(1, t1, rEnd), point(0, t1, rStart)));
						}
					}
				}
	            if (alpha < 360) {
	                polygons.Add(new CsgPolygon(start, end, point(0, 0, rStart)));
	                polygons.Add(new CsgPolygon(point(0, 0, rStart), end, point(1, 0, rEnd)));
	                polygons.Add(new CsgPolygon(start, point(0, 1, rStart), end));
	                polygons.Add(new CsgPolygon(point(0, 1, rStart), point(1, 1, rEnd), end));
	            }
			}
			var result = CsgSolid.FromPolygons(polygons);
	        return result;
	    }

		public static CsgSolid Cylinder(double r, double h, bool center = false)
		{
			var start = center ? new CsgVec3(0, -h / 2, 0) : new CsgVec3(0, 0, 0);
			var end = center ? new CsgVec3(0, h / 2, 0) : new CsgVec3(0, h, 0);
			return Cylinder(new CylinderOptions { Start = start, End = end, RadiusStart = r, RadiusEnd = r, });
		}

		public static CsgSolid Union(params CsgSolid[] csgs)
		{
			if (csgs.Length == 0)
			{
				return new CsgSolid();
			}
			else if (csgs.Length == 1)
			{
				return csgs[0];
			}
			else
			{
				var head = csgs[0];
				var rest = csgs.Skip(1).ToArray();
				return head.Union(rest);
			}
		}

		public static CsgSolid Difference(params CsgSolid[] csgs)
		{
			if (csgs.Length == 0)
			{
				return new CsgSolid();
			}
			else if (csgs.Length == 1)
			{
				return csgs[0];
			}
			else
			{
				var head = csgs[0];
				var rest = csgs.Skip(1).ToArray();
				return head.Substract(rest);
			}
		}

		public static CsgSolid Intersection(params CsgSolid[] csgs)
		{
			if (csgs.Length == 0 || csgs.Length == 1)
			{
				return new CsgSolid();
			}
			else
			{
				var head = csgs[0];
				var rest = csgs.Skip(1).ToArray();
				return head.Intersect(rest);
			}
		}

		static CsgVertex NoTexVertex (CsgVec3 pos) => new CsgVertex (pos, new Vector2D (0, 0));

		static readonly int[][][] cubeData =
			{
				new[] {
					new[] { 0, 4, 6, 2 },
					new[] { -1, 0, 0 }
				},
				new[] {
					new[] {1, 3, 7, 5},
					new[] {+1, 0, 0}
				},
				new[] {
					new[] {0, 1, 5, 4},
					new[] {0, -1, 0},
				},
				new[] {
					new[] {2, 6, 7, 3},
					new[] { 0, +1, 0}
				},
				new[] {
					new[] {0, 2, 3, 1},
					new[] { 0, 0, -1}
				},
				new[] {
					new[] {4, 5, 7, 6},
					new[] { 0, 0, +1}
				}
			};
	}

	public class CubeOptions
	{
		public CsgVec3 Center;
		public CsgVec3 Radius = new CsgVec3(1, 1, 1);
	}

	public class SphereOptions
	{
		public CsgVec3 XAxis = new CsgVec3(1, 0, 0);
		public CsgVec3 YAxis = new CsgVec3(0, -1, 0);
		public CsgVec3 ZAxis = new CsgVec3(0, 0, 1);
		public CsgVec3 Center;
		public double Radius = 1;
		public int Resolution = CsgSolid.DefaultResolution3D;
	}

	public class CylinderOptions
	{
		public CsgVec3 Start;
		public CsgVec3 End;
		public double RadiusStart = 1;
		public double RadiusEnd = 1;
		public double SectorAngle = 360;
		public int Resolution = CsgSolid.DefaultResolution3D;
	}
}

