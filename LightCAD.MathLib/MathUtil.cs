﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public static class MathUtil
    {
        public const double DoublePi = Math.PI * 2;
        public const double HalfPi = Math.PI / 2;
        public const double Pi = Math.PI;
        public const double EPS = 0.01;
        public static int FloatSortEQ(double a, double b)
        {
            if (a > b)
                return 1;
            else if (a < b)
                return -1;
            else
                return 0;
        }
        /// <summary>
        /// 大于
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static bool FloatGT(double a, double b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a - b) >= eps) && a >= b;
        }
        /// <summary>
        /// 小于
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static bool FloatLT(double a, double b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a - b) > eps) && a < b;
        }
        /// <summary>
        /// 大于等于
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static bool FloatGE(double a, double b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return a >= b || (Math.Abs(a - b) < eps);
        }


        /// <summary>
        /// 小于等于
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static bool FloatLE(double a, double b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return a <= b || (Math.Abs(a - b) < eps);
        }
        /// <summary>
        /// 等于
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static bool FloatEQ(double a, double b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a - b) < eps);
        }


        public static bool NumEQ(double a, double b, double? eps = null)
        {
            if (eps == null)
                eps = EPS;
            return (Math.Abs(a - b) < eps);
        }
        public static bool NumEQ(double a, double b, double eps)
        {
            return (Math.Abs(a - b) < eps);
        }
    }
}
