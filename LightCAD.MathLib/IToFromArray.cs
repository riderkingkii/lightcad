﻿using System;
using System.Collections;
using System.Linq;

namespace LightCAD.MathLib
{
    public interface IIOArray
    {
        IIOArray FromArray(double[] array, int offset);
        double[] ToArray(double[] array = null, int offset = 0);
        bool Equals(IIOArray obj);
    }


}
