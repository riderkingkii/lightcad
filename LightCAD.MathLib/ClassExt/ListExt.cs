﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{

    public static class ListExt
    {
        /// <summary>
        /// List元素复制，内部对象如果实现ICloneable，则内部元素Clone，否则复制引用
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<T> Clone<T>(this List<T> list)
        {
            var newList = new List<T>();
            foreach (var item in list)
            {
                if (item is ICloneable)
                {
                    newList.Add((T)(item as ICloneable).Clone());
                }
                else
                {
                    newList.Add(item);
                }
            }
            return newList;
        }
        /// <summary>
        /// Array元素复制，内部对象如果实现ICloneable，则内部元素Clone，否则复制引用
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <returns></returns>
        public static T[] Clone<T>(this T[] array)
        {
            var newArr = new T[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                var item = array[i];
                if (item is ICloneable)
                {
                    newArr[i] = ((T)(item as ICloneable).Clone());
                }
                else
                {
                    newArr[i] = item;
                }
            }
            return newArr;
        }
    }
}
