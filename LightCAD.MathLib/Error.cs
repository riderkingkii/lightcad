﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public class Error : Exception
    {
        public Error(string message) : base(message)
        {
        }
        public string name
        {
            get { return "Error"; }
        }
        public string message
        {
            get { return base.Message; }
        }
        public string stack
        {
            get { return base.StackTrace; }
        }
    }
}