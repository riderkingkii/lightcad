﻿using System;
using LightCAD.MathLib;

namespace LightCAD.MathLib
{
    public struct CoordSystem2d
    {
        public Vector2 Origin;
        public Vector2 Xaxis;
        public Vector2 Yaxis;
    }
}
