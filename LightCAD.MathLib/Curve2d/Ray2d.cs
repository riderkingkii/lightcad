﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace LightCAD.MathLib
{
    public class Ray2d : Curve2d
    {
        [JsonInclude]
        public Vector2 StartPoint;
        public Vector2 Direction;
        public override bool IsClosed { get => base.IsClosed; }
        public Ray2d()
        {
            this.isClosed = false;
            this.Type = Curve2dType.Ray2d;
        }

        public override Curve2d Clone()
        {
            var newObj = new Ray2d();
            newObj.Copy(this);
            return newObj;
        }
        public override void Copy(Curve2d src)
        {
            var ray2D = src as Ray2d;
            this.StartPoint = ray2D.StartPoint;
            this.Direction = ray2D.Direction;
            this.Name = ray2D.Name;
        }
        public override Vector2[] GetPoints(int div = 5, double scaleInFuture = 1)
        {
          return new Vector2[div];
        }
        public override Curve2d Mirror(Vector2 axisStart, Vector2 axisDir)
        {
            
            return null;
        }
        public override Curve2d RotateAround(Vector2 basePoint, double angle)
        {
       
            return null;
        }
        public override Curve2d Translate(Vector2 offset)
        {
            return null;
        }








    }
}
