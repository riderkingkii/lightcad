﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public enum Curve2dType
    {
        None = 0,
        Point2d = 1,
        Line2d = 2,
        Rect2d = 3,
        Arc2d = 4,
        Circle2d = 5,
        Spline2d = 6,
        Polyline2d = 7,
        Polygon2d = 8,
        Path2d = 9,
        Ellipse2d = 10,
        Ray2d = 11
    }

    public abstract class Curve2d : ICloneable
    {
        public string Name { get; set; } = null;
        public Curve2dType Type { get; protected set; }
        protected bool isClosed = false;
        public virtual bool IsClosed { get => isClosed; set => this.isClosed = value; }
        [JsonIgnore]
        public object Source { get; set; } = null;
        public abstract void Copy(Curve2d src);
        public abstract Curve2d Clone();

        public Curve2d Translate(double dx, double dy)
        {
            var v2 = ThreadContext.GetCurrThreadContext().Vector2.Set(dx, dy);
            return this.Translate(v2);
        }
        public abstract Curve2d Translate(Vector2 offset);
        public abstract Curve2d RotateAround(Vector2 basePoint, double angle);
        public abstract Curve2d Mirror(Vector2 axisStart, Vector2 axisDir);
        object ICloneable.Clone() => this.Clone();
        public abstract Vector2[] GetPoints(int div = 5, double scaleInFuture = 1);
        public virtual Vector2[] GetPoints() => GetPoints(5);
        public virtual Curve2d Reverse() => this;

        public virtual Curve3d ToCurve3d() => null;
    }


}
