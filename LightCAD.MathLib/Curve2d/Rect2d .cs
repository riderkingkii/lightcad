﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.MathLib
{
    public class Rect2d : Curve2d
    {
        /// <summary>
        /// 左下角
        /// </summary>
        [JsonInclude]
        public Vector2 Min;
        /// <summary>
        /// 右上角
        /// </summary>
        [JsonInclude]
        public Vector2 Max;
        /// <summary>
        /// 绕MIn旋转的角度
        /// </summary>
        [JsonInclude]
        public double Angle;

        [JsonIgnore]
        public override bool IsClosed => true;
        public Rect2d() 
        {
            this.Type = Curve2dType.Rect2d;
        }
        public override void Copy(Curve2d src)
        {
            var rect = src as Rect2d;
            this.Min = rect.Min;
            this.Max = rect.Max;
            this.Name = rect.Name;
            this.Angle = rect.Angle;
        }
        public override Curve2d Clone()
        {
            var newObj = new Rect2d();
            newObj.Copy(this);
            return newObj;
        }

        public override Curve2d Translate(Vector2 offset)
        {
            this.Min.Add(offset);
            this.Max.Add(offset);
            return this;
        }

        public override Curve2d RotateAround(Vector2 basePoint, double angle)
        {
            this.Min.RotateAround(basePoint,angle);
            this.Max.RotateAround(basePoint,angle);
            this.Angle+= angle; 
            return this;
        }

        public override Curve2d Mirror(Vector2 axisStart, Vector2 axisDir)
        {
            this.Min.Mirror(axisStart, axisDir);
            this.Max.Mirror(axisStart, axisDir);
            var divAngle = axisDir.Angle();
            if (divAngle > MathUtil.Pi)
                divAngle -= MathUtil.Pi;
            this.Angle -= 2 * (this.Angle - divAngle);
            return this;
        }
        public override Vector2[] GetPoints(int div = 5, double scaleInFuture = 1)
        {
            //var box = new Box2(this.Min, this.Max);
            //var lb = box.LeftBottom;
            //var rb = box.RightBottom.RotateAround(this.Min, this.Angle);
            //var rt = box.RightTop.RotateAround(this.Min, this.Angle);
            //var lt = box.LeftTop.RotateAround(this.Min, this.Angle);
            var newMax = this.Max.Clone().RotateAround(this.Min, - this.Angle);
            var box = new Box2(this.Min, newMax);
            var lt = box.LeftTop.RotateAround(this.Min, this.Angle);
            var rb = box.RightBottom.RotateAround(this.Min, this.Angle);
            return new Vector2[] { this.Min.Clone(),lt,this.Max.Clone(),rb };
        }
    }
}
