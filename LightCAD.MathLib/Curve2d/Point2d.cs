﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using LightCAD.MathLib;
namespace LightCAD.MathLib
{
    public class Point2d : Curve2d
    {

        [JsonInclude]
        public Vector2 XY;
        public override bool IsClosed => false;
        public Point2d() 
        {
            this.Type = Curve2dType.Point2d;
        }
        public override void Copy(Curve2d src)
        {
            var point = src as Point2d;
            this.XY = point.XY;
            this.Name = point.Name;
        }
        public override Curve2d Clone()
        {
            var newObj = new Point2d();
            newObj.Copy(this);
            return newObj;
        }

        public override Curve2d Translate(Vector2 offset)
        {
            this.XY.Add(offset);
            return this;
        }

        public override Curve2d RotateAround(Vector2 basePoint, double angle)
        {
            this.XY.RotateAround(basePoint,angle);
            return this;
        }

        public override Curve2d Mirror(Vector2 axisStart, Vector2 axisDir)
        {
            this.XY.Mirror(axisStart, axisDir);
            return this;
        }
        public override Vector2[] GetPoints(int div = 5, double scaleInFuture = 1)
        {
            return new Vector2[] { this.XY.Clone() };
        }
    }
}
