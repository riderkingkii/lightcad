﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public class Path2d : Curve2d
    {
        public Curve2d[] Curves { get; set; }
        public Path2d() 
        {
            this.Type= Curve2dType.Path2d;
        }

        public override void Copy(Curve2d src)
        {
            var path = src as Path2d;
            this.Curves = path.Curves.Clone<Curve2d>();
            this.Name = path.Name;
        }
        public override Curve2d Clone()
        {
            var newObj = new Path2d();
            newObj.Copy(this);
            return newObj;
        }
        public override Curve2d Translate(Vector2 offset)
        {
            for (int i = 0; i < this.Curves.Length; i++)
            {
                var curve = this.Curves[i]; 
                curve.Translate(offset);    
            }
            return this;
        }

        public override Curve2d RotateAround(Vector2 basePoint, double angle)
        {
            for (int i = 0; i < this.Curves.Length; i++)
            {
                var curve = this.Curves[i];
                curve.RotateAround(basePoint,angle);
            }
            return this;
        }

        public override Curve2d Mirror(Vector2 axisStart, Vector2 axisDir)
        {
            for (int i = 0; i < this.Curves.Length; i++)
            {
                var curve = this.Curves[i];
                curve.Mirror(axisStart, axisDir);
            }
            return this;
        }
        public override Vector2[] GetPoints(int div = 5, double scaleInFuture = 1)
        {
            return this.Curves.SelectMany(c => c.GetPoints(div,scaleInFuture)).ToArray();
        }
    }
}
