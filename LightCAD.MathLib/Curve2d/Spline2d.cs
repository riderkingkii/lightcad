﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public class Spline2d : Curve2d
    {
        [JsonIgnore]
        public override bool IsClosed => false;
        public Spline2d() 
        {
            this.Type = Curve2dType.Spline2d;
        }

        public override void Copy(Curve2d src)
        {
            var spline = src as Spline2d;
             this.Name = spline.Name;
        }
        public override Curve2d Clone()
        {
            var newObj = new Spline2d();
            newObj.Copy(this);
            return newObj;
        }

        public override Curve2d Translate(Vector2 offset)
        {
            throw new NotImplementedException();
        }

        public override Curve2d RotateAround(Vector2 basePoint, double angle)
        {
            throw new NotImplementedException();
        }

        public override Curve2d Mirror(Vector2 axisStart, Vector2 axisDir)
        {
            throw new NotImplementedException();
        }
        public override Vector2[] GetPoints(int div = 5, double scaleInFuture = 1)
        {
            throw new NotImplementedException();
        }
    }
}
