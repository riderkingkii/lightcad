﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{


    public struct LineSegment
    {

    }
    /// <summary>
    /// 三维线段组
    /// </summary>
    public class LineSegments3d : Geometry3d
    {
        public override bool IsLine { get; } = true;
        public List<LineSegment> Lines { get; set; }
    }
}
