﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public class Solid3d
    {
        public string Name { get; set; }
        public Transform3d Transform3d { get; set; }=new Transform3d();
        public GeometryData Geometry { get; set; }
        public GeometryData Edge { get; set; }
        public GeometryData Curve { get; set; }
        public Solid3d()
        {

        }
        public virtual Solid3d CreateMesh()
        {
            var topoModel = this.topoFaceModel??this.CreateTopoModel();
            if (topoModel != null)
            {
                this.Geometry = new GeometryData();
                var posArr = new List<double>();
                var idxArr = new List<int>();
                var groups = new List<GeometryGroup>();
                int idxOffset = 0;
                for (int i = 0; i < topoModel.Surfaces.Length; i++)
                {
                    var face = topoModel.Surfaces[i];
                    var tuple = face.Trianglate();
                    posArr.AddRange(tuple.Item1);
                    groups.Add(new GeometryGroup() {
                        Name = i+"_",
                        Start = idxArr.Count,
                        Count = tuple.Item2.Count(),
                        MaterialIndex = 0
                    });
                    idxArr.AddRange(tuple.Item2.Select(idx => idx + idxOffset));
                    idxOffset += tuple.Item1.Length / 3;
                }
                this.Geometry.Groups = groups.ToArray();
                this.Geometry.Verteics = posArr.ToArray();
                this.Geometry.Indics = idxArr.ToArray();
            }
            else
            { }
            return this;
        }
        public bool Topoable = false;
        private TopoFaceModel topoFaceModel;
        public TopoFaceModel TopoFaceModel
        {
            get
            {
                if (Topoable)
                {
                    if (topoFaceModel == null)
                        topoFaceModel = CreateTopoModel();
                    return topoFaceModel;
                }
                else
                    return null;
            }
        }
        public void ResetTopoModel ()
        {
            this.topoFaceModel = null;
        }
        public virtual TopoFaceModel CreateTopoModel()
        {
            return null;
        }
      

        public Solid3d MeshOneMaterial(Surface3d[] surfaces)
        {
            this.Geometry.Groups = new GeometryGroup[1]
            {
                new GeometryGroup{ Name = "Geometry", Start = 0, Count = this.Geometry.Indics.Length, MaterialIndex = 0 },
            };

            this.Edge = new GeometryData();

            var allPoints = new ListEx<Vector3>();
            var indices = new ListEx<int>();
            foreach (var face in surfaces)
            {
                foreach (var loop in face.OuterLoop)
                {
                    var points = loop.GetPoints();
                    for (int i = 0; i < points.Count - 1; i++)
                    {
                        int nxi = i + 1;

                        var count = allPoints.Count;
                        indices.Push(count, count + 1);
                        allPoints.Push(points[i], points[nxi]);
                    }
                }
            }

            this.Edge.Verteics = Utils.GenerateVertexArr(allPoints);
            this.Edge.Indics = indices.ToArray();
            return this;
        }
    }
 
    public class GeometryData
    {
        public double[] Verteics;
        public int[] Indics;
        public GeometryGroup[] Groups;
    }
  

    //public struct TriIndex
    //{
    //    //索引号，用于面索引
    //    public int i;
    //    public int a;
    //    public int b;
    //    public int c;
    //}
    //public class FaceIndex
    //{
    //    public int Index;
    //    /// <summary>
    //    /// 轮廓的顶点索引
    //    /// </summary>
    //    public List<int> Contour { get; set; }
    //    public List<List<int>> Holes { get; set; }
    //    /// <summary>
    //    /// 三角形索引
    //    /// </summary>
    //    public List<int> Triangles { get; set; }
    //    /// <summary>
    //    /// 面的材质
    //    /// </summary>
    //    public string Material { get; set; }
    //    /// <summary>
    //    /// 材质设置
    //    /// </summary>
    //    public string MaterialSettings { get; set; }
    //}
    ///// <summary>
    ///// 原生
    ///// </summary>
    //public class Solid3d : Geometry3d
    //{
    //    public override bool IsSolid { get; } = true;
    //    public List<Vector3> Vertices { get; set; }
    //    public List<TriIndex> Indics { get; set; }

    //    /// <summary>
    //    /// 逻辑面的索引
    //    /// </summary>
    //    public List<FaceIndex> Faces { get; set; }

    //}
}
