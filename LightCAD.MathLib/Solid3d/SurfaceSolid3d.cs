﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    /// <summary>
    /// 曲面体，可以包含曲线
    /// </summary>
    public class SurfaceSolid3d : Geometry3d
    {
        public List<Curve3d> Curves { get; set; }
        public List<Surface3d> Surfaces { get; set; }
    }
}
