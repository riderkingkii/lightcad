﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{


    /// <summary>
    /// 三维多段线
    /// </summary>
    public class Polyline3d : Geometry3d
    {
        public override bool IsLine { get; } = true;
        public List<Vector3> Points { get; set; }
        public Surface3d Source { get; set; }
        public bool IsClosed { get; set; }
    }
}
