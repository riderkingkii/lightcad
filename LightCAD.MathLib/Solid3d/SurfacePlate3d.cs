﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    /// <summary>
    /// 曲面板
    /// </summary>
    public class SurfacePlate3d : Geometry3d
    {
        public Surface3d Surface { get; set; }
        public double Thickness { get; set; }
    }
}
