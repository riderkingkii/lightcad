﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{

    /// <summary>
    /// 棱台体
    /// </summary>
    public class Frustum3d : Geometry3d
    {
        public bool Rounded { get; set; }

    }
}
