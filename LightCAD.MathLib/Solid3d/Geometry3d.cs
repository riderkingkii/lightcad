﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public abstract class Geometry3d
    {
        public virtual bool IsLine { get; }
        public virtual bool IsSolid { get; }
        public virtual string Material { get; set; }
        public virtual string MaterialSettings { get; set; }

        public virtual Geometry3d CreateSolid()
        {
            return null;
        }
    }
}
