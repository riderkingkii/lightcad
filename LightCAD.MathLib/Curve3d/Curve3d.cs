﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public enum Curve3dType
    {
        None = 0,
        Line3d = 1,
        Arc3d = 2,
        Circle3d = 3,
        CylindricalHelix3d = 4,
        Ellipse3d = 5,
        EllipseArc3d = 6,
        HermiteSpline3d =7,
        NurbSpline3d = 8
    }
    public abstract class Curve3d: ICloneable
    {
        public Vector3 Start;
        public Vector3 End;
        public virtual Curve3dType Type { get;protected set; }= Curve3dType.None;

        public virtual List<Vector3> GetPoints(int div) => null;
        public virtual List<Vector3> GetPoints() => GetPoints(32);
        public virtual Curve3d Reverse() => this;
        public abstract void Copy(Curve3d src);

        public abstract Curve3d Clone();

        object ICloneable.Clone() => this.Clone();
        public Curve3d Translate(double dx, double dy, double dz)
        {
            var v3 = ThreadContext.GetCurrThreadContext().Vector3.Set(dx, dy,dz);
            return this.Translate(v3);
        }
        public abstract Curve3d Translate(Vector3 offset);


        public virtual Curve3d RotateRoundAxis(Vector3 origin, Vector3 axis, double angle) => null;
    }
}
