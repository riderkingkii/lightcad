﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{

    public class NurbSpline3d : Curve3d
    {
        public override void Copy(Curve3d src)
        {
            var spline = src as NurbSpline3d;
        }
        public override Curve3d Clone()
        {
            var newObj = new NurbSpline3d();
            newObj.Copy(this);
            return newObj;
        }
        public override Curve3d Translate(Vector3 offset)
        {
            return this;
        }
        public override Curve3dType Type => Curve3dType.NurbSpline3d;
    }
}
