﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public class Line3d : Curve3d
    {
        public override Curve3dType Type => Curve3dType.Line3d;
        public Vector3 Dir { get => this.End.Clone().Sub(this.Start).Normalize(); }
        public Line3d()
        {
 
        }
        public Line3d(Vector3 start,Vector3 end) 
        {
            this.Start = start;
            this.End = end;
        }
        public override List<Vector3> GetPoints(int div)
        {
            if (div <= 2)
            {
                return new List<Vector3> { Start.Clone(), End.Clone() };
            }
            double len = this.End.DistanceTo(this.Start);
            double divLen = len / div;
            var dir = this.Dir;
            var points = new List<Vector3>()
            {
                this.Start.Clone()
            };
            for (int i = 1; i <= div; i++)
            {
                points.Add(this.Start.Clone().Add(dir.Clone().MulScalar(i * divLen)));
            }
            return points;
        }

        public override List<Vector3> GetPoints()
        {
            return this.GetPoints(2);
        }
        public override void Copy(Curve3d src)
        {
            var line = src as Line3d;
            this.Start = line.Start.Clone();
            this.End = line.End.Clone();
        }
        public override Curve3d Clone()
        {
            var newObj = new Line3d();
            newObj.Copy(this);
            return newObj;
        }
        public override Curve3d Translate(Vector3 offset)
        {
            this.Start.Add(offset);
            this.End.Add(offset);
            return this;
        }

        public override Curve3d Reverse()
        {
            var temp = this.Start;
            this.Start = this.End;
            this.End = temp;
            return this;
        }

        public override Curve3d RotateRoundAxis(Vector3 origin, Vector3 axis, double angle)
        {
            this.Start.RotateRoundAxis(origin, axis, angle);
            this.End.RotateRoundAxis(origin, axis, angle);
            return this;
        }
    }
}
