﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{

    public class Circle3d : Curve3d
    {
        public override Curve3dType Type => Curve3dType.Circle3d;
        public double Radius;
        private int fixediv = -1;

        public Circle3d(double radius)
        {
            this.Radius = radius;
        }
        public Circle3d()
        {
        }
        public void UseFixedDiv(int div)
        {
            fixediv = div;
        }

        public void UnuseFixedDiv()
        {
            fixediv = -1;
        }
        public override void Copy(Curve3d src)
        {
            var arc = src as Circle3d;
            this.Radius = arc.Radius;
        }
        public override Curve3d Clone()
        {
            var newObj = new Circle3d();
            newObj.Copy(this);
            return newObj;
        }
        public override Curve3d Translate(Vector3 offset)
        {
            return this;
        }
        public override List<Vector3> GetPoints(int div = 5)
        {
            if (fixediv != -1)
            {
                div = fixediv;
            }
            var points = new List<Vector3>();
            for (var i = 0; i < div; i++)
            {
                points.Add(new Vector3(
                                    this.Radius * Math.Cos(Math.PI * 2 * i / div),
                                    this.Radius * Math.Sin(Math.PI * 2 * i / div),
                                    0
                                ));
            }
            return points;
        }
    }
}
