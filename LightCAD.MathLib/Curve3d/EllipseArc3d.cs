﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{

    public class EllipseArc3d : Curve3d
    {
        public override void Copy(Curve3d src)
        {
            var arc = src as EllipseArc3d;
        }
        public override Curve3d Clone()
        {
            var newObj = new EllipseArc3d();
            newObj.Copy(this);
            return newObj;
        }
        public override Curve3d Translate(Vector3 offset)
        {
            return this;
        }
        public override Curve3dType Type => Curve3dType.EllipseArc3d;
    }
}
