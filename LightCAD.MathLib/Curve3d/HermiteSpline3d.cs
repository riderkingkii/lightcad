﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{

    public class HermiteSpline3d : Curve3d
    {
        public override void Copy(Curve3d src)
        {
            var arc = src as HermiteSpline3d;
        }
        public override Curve3d Clone()
        {
            var newObj = new HermiteSpline3d();
            newObj.Copy(this);
            return newObj;
        }
        public override Curve3d Translate(Vector3 offset)
        {
            return this;
        }
        public override Curve3dType Type => Curve3dType.HermiteSpline3d;
    }
}
