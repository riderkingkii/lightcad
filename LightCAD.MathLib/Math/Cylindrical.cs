using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.MathLib
{
    public class Cylindrical
    {
        #region Properties

        public double Radius;
        public double Theta;
        public double Y;

        #endregion

        #region constructor
        public Cylindrical(double radius = 1, double theta = 0, double y = 0)
        {
            this.Radius = radius; // distance from the origin to a point in the x-z plane
            this.Theta = theta; // counterclockwise angle in the x-z plane measured in radians from the positive z-axis
            this.Y = y; // height above the x-z plane
        }
        #endregion

        #region methods
        public Cylindrical Set(double radius, double theta, double y)
        {
            this.Radius = radius;
            this.Theta = theta;
            this.Y = y;
            return this;
        }
        public virtual Cylindrical Copy(Cylindrical other)
        {
            this.Radius = other.Radius;
            this.Theta = other.Theta;
            this.Y = other.Y;
            return this;
        }
        public Cylindrical SetFromVector3(Vector3 v)
        {
            return this.SetFromCartesianCoords(v.X, v.Y, v.Z);
        }
        public Cylindrical SetFromCartesianCoords(double x, double y, double z)
        {
            this.Radius = Math.Sqrt(x * x + z * z);
            this.Theta = Math.Atan2(x, z);
            this.Y = y;
            return this;
        }
        public virtual Cylindrical Clone()
        {
            return new Cylindrical().Copy(this);
        }
        #endregion

    }
}
