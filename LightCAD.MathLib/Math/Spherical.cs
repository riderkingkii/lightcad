using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.MathLib
{
    public class Spherical
    {
        #region Properties

        public double Radius;
        public double Phi;
        public double Theta;

        #endregion

        #region constructor
        public Spherical(double radius = 1, double phi = 0, double theta = 0)
        {
            this.Radius = radius;
            this.Phi = phi; // polar angle
            this.Theta = theta; // azimuthal angle
        }
        #endregion

        #region methods
        public Spherical Set(double radius, double phi, double theta)
        {
            this.Radius = radius;
            this.Phi = phi;
            this.Theta = theta;
            return this;
        }
        public Spherical Copy(Spherical other)
        {
            this.Radius = other.Radius;
            this.Phi = other.Phi;
            this.Theta = other.Theta;
            return this;
        }
        public Spherical MakeSafe()
        {
            double EPS = 0.000001;
            this.Phi = Math.Max(EPS, Math.Min(MathEx.PI - EPS, this.Phi));
            return this;
        }
        public Spherical SetFromVector3(Vector3 v)
        {
            return this.SetFromCartesianCoords(v.X, v.Y, v.Z);
        }
        public Spherical SetFromCartesianCoords(double x, double y, double z)
        {
            this.Radius = Math.Sqrt(x * x + y * y + z * z);
            if (this.Radius == 0)
            {
                this.Theta = 0;
                this.Phi = 0;
            }
            else
            {
                this.Theta = Math.Atan2(x, z);
                this.Phi = Math.Acos(MathEx.Clamp(y / this.Radius, -1, 1));
            }
            return this;
        }
        public virtual bool Equals(Spherical other)
        {
            return this.Radius == other.Radius && this.Phi == other.Phi &&  this.Theta == other.Theta;
        }
        public virtual Spherical Clone()
        {
            return new Spherical().Copy(this);
        }
        #endregion

    }
}
