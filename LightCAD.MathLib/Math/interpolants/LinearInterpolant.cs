using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.MathLib
{
    public class LinearInterpolant : Interpolant
    {
        #region constructor
        public LinearInterpolant(double[] parameterPositions, double[] sampleValues, int sampleSize, double[] resultBuffer = null)
        : base(parameterPositions, sampleValues, sampleSize, resultBuffer)
        {

        }
        #endregion

        #region methods
        public override double[] Interpolate_(int i1, double t0, double t, double t1)
        {
            double[] result = this.ResultBuffer,
                values = this.SampleValues;
            int stride = this.ValueSize,
               offset1 = i1 * stride,
               offset0 = offset1 - stride;
            double weight1 = (t - t0) / (t1 - t0),
                weight0 = 1 - weight1;
            for (int i = 0; i != stride; ++i)
            {
                result[i] = values[offset0 + i] * weight0 + values[offset1 + i] * weight1;
            }
            return result;
        }
        #endregion

    }
}
