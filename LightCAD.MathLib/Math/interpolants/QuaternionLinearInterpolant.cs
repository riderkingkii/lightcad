using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.MathLib
{
    public class QuaternionLinearInterpolant : Interpolant
    {
        #region constructor
        public QuaternionLinearInterpolant(double[] parameterPositions, double[] sampleValues, int sampleSize, double[] resultBuffer = null)
        : base(parameterPositions, sampleValues, sampleSize, resultBuffer)
        {
        }
        #endregion

        #region methods
        public override double[] Interpolate_(int i1, double t0, double t, double t1)
        {
            double[] result = this.ResultBuffer,
                     values = this.SampleValues;
            int stride = this.ValueSize;
            double alpha = (t - t0) / (t1 - t0);
            var offset = i1 * stride;
            for (int end = offset + stride; offset != end; offset += 4)
            {
                Quaternion.SlerpFlat(result, 0, values, offset - stride, values, offset, alpha);
            }
            return result;
        }
        #endregion

    }
}
