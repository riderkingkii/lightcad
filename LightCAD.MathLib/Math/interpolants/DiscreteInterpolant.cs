using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.MathLib
{
    public class DiscreteInterpolant : Interpolant
    {
        #region constructor
        public DiscreteInterpolant(double[] parameterPositions, double[] sampleValues, int sampleSize, double[] resultBuffer = null)
        : base(parameterPositions, sampleValues, sampleSize, resultBuffer)
        {

        }
        #endregion

        #region methods
        public override double[] Interpolate_(int i1, double t0, double t, double t1)
        {
            //return null;
            return this.CopySampleValue_(i1 - 1);
        }
        #endregion

    }
}
