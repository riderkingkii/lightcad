using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.MathLib
{
    public class InterpolantSettings
    {
        public int EndingStart;
        public int EndingEnd;

    }
    public class Interpolant
    {
        public double[] ParameterPositions;

        /// <summary>
        /// </summary>
        public double[] ResultBuffer;

        /// <summary>
        /// </summary>
        public double[] SampleValues;

        internal int _cachedIndex;
        /// <summary>
        /// Optional, subclass-specific settings structure.
        /// </summary>
        public InterpolantSettings Settings;
        public InterpolantSettings DefaultSettings_;
        // optional, subclass-specific settings structure
        // Note: The indirection allows central control of many interpolants.

        /// <summary>
        /// </summary>
        public int ValueSize;

        public int __cacheIndex;//for animation
        public Interpolant(double[] parameterPositions, double[] sampleValues, int sampleSize, double[] resultBuffer = null)
        {

            this.ParameterPositions = parameterPositions;
            this._cachedIndex = 0;

            this.ResultBuffer = resultBuffer ?? new double[sampleSize];
            this.SampleValues = sampleValues;
            this.ValueSize = sampleSize;

            this.Settings = null;
            this.DefaultSettings_ = new InterpolantSettings();

        }
        protected double Get(IList<double> pp, int idx)
        {
            if (idx >= pp.Count || idx < 0)
                return double.NaN;
            else
                return pp[idx];
        }
        public double[] Evaluate(double t)
        {
            ListEx<double> pp = this.ParameterPositions.ToListEx();
            int i1 = this._cachedIndex;
            double t1 = Get(pp, i1);
            double t0 = Get(pp, i1 - 1);

        validate_interval:
            {

            seek:
                {

                    int right;

                linear_scan:
                    {

                    //- See http://jsperf.com/comparison-to-undefined/3
                    //- slower code:
                    //-
                    //- 				if ( t >= t1 || t1 === undefined ) {
                    forward_scan: if (!(t < t1))
                        {

                            for (int giveUpAt = i1 + 2; ;)
                            {

                                if (double.IsNaN(t1))
                                {

                                    if (t < t0) goto break_forward_scan;

                                    // after end

                                    i1 = pp.Length;
                                    this._cachedIndex = i1;
                                    return this.CopySampleValue_(i1 - 1);

                                }

                                if (i1 == giveUpAt) break; // this loop

                                t0 = t1;
                                t1 = Get(pp, ++i1);

                                if (t < t1)
                                {

                                    // we have arrived at the sought interval
                                    goto break_seek;

                                }

                            }

                            // prepare binary search on the right side of the index
                            right = pp.Length;
                            goto break_linear_scan;

                        }
                    break_forward_scan:

                        //- slower code:
                        //-					if ( t < t0 || t0 === undefined ) {
                        if (!(t >= t0))
                        {

                            // looping?

                            double t1global = Get(pp, 1);

                            if (t < t1global)
                            {

                                i1 = 2; // + 1, using the scan for the details
                                t0 = t1global;

                            }

                            // linear reverse scan

                            for (int giveUpAt = i1 - 2; ;)
                            {

                                if (double.IsNaN(t0))
                                {

                                    // before start

                                    this._cachedIndex = 0;
                                    return this.CopySampleValue_(0);

                                }

                                if (i1 == giveUpAt) break; // this loop

                                t1 = t0;
                                t0 = Get(pp, --i1 - 1);

                                if (t >= t0)
                                {

                                    // we have arrived at the sought interval
                                    goto break_seek;

                                }

                            }

                            // prepare binary search on the left side of the index
                            right = i1;
                            i1 = 0;
                            goto break_linear_scan;

                        }

                        // the interval is valid

                        goto break_validate_interval;

                    } // linear scan
                break_linear_scan:
                    // binary search

                    while (i1 < right)
                    {

                        int mid = (i1 + right) >> 1;//�޷�������

                        if (t < Get(pp, mid))
                        {

                            right = mid;

                        }
                        else
                        {

                            i1 = mid + 1;

                        }

                    }

                    t1 = Get(pp, i1);
                    t0 = Get(pp, i1 - 1);

                    // check boundary cases, again

                    if (double.IsNaN(t0))
                    {

                        this._cachedIndex = 0;
                        return this.CopySampleValue_(0);

                    }

                    if (double.IsNaN(t1))
                    {

                        i1 = pp.Length;
                        this._cachedIndex = i1;
                        return this.CopySampleValue_(i1 - 1);

                    }

                } // seek
            break_seek:
                this._cachedIndex = i1;
                this.IntervalChanged_(i1, t0, t1);

            } // validate_interval
        break_validate_interval:
            return this.Interpolate_(i1, t0, t, t1);

        }

        public InterpolantSettings GetSettings_()
        {

            return this.Settings ?? this.DefaultSettings_;

        }

        public virtual double[] CopySampleValue_(int index)
        {

            // copies a sample value to the result buffer

            var result = this.ResultBuffer;
            var values = this.SampleValues;
            int stride = this.ValueSize;
            int offset = index * stride;

            for (int i = 0; i != stride; ++i)
            {

                result[i] = values[offset + i];

            }

            return result;

        }

        // Template methods for derived classes:
        public virtual double[] Interpolate_(int i1, double t0, double t, double t1)
        {
            return null;
        }

        public virtual void IntervalChanged_(int i1, double t0, double t1)
        {
            // empty
        }

    }
}
