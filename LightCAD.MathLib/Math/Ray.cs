using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.MathEx;

namespace LightCAD.MathLib
{
    public class Ray
    {
        #region scope properties or methods
        //private static Vector3 _vector = new Vector3();
        //private static Vector3 _segCenter = new Vector3();
        //private static Vector3 _segDir = new Vector3();
        //private static Vector3 _diff = new Vector3();
        //private static Vector3 _edge1 = new Vector3();
        //private static Vector3 _edge2 = new Vector3();
        //private static Vector3 _normal = new Vector3();
        private RayContext GetContext()
        {
            return ThreadContext.GetCurrThreadContext().RayCtx;
        }
        #endregion

        #region Properties

        public Vector3 Origin;
        public Vector3 Direction;

        #endregion

        #region constructor
        public Ray(Vector3 origin = null, Vector3 direction = null)
        {
            if (origin == null) origin = new Vector3();
            if (direction == null) direction = new Vector3(0, 0, -1);
            this.Origin = origin;
            this.Direction = direction;
        }
        #endregion

        #region methods
        public Ray Set(Vector3 origin, Vector3 direction)
        {
            this.Origin.Copy(origin);
            this.Direction.Copy(direction);
            return this;
        }
        public Ray Copy(Ray ray)
        {
            this.Origin.Copy(ray.Origin);
            this.Direction.Copy(ray.Direction);
            return this;
        }
        public Vector3 At(double t, Vector3 target = null)
        {
            target = target ?? new Vector3();
            return target.Copy(this.Origin).AddScaledVector(this.Direction, t);
            //return target.copy(this.direction).multiplyScalar(t).add(this.origin);
        }
        public Ray LookAt(Vector3 v)
        {
            this.Direction.Copy(v).Sub(this.Origin).Normalize();
            return this;
        }
        public Ray Recast(double t)
        {
            this.Origin.Copy(this.At(t, GetContext()._vector));
            return this;
        }
        public Vector3 ClosestPointToPoint(Vector3 point, Vector3 target)
        {
            target.SubVectors(point, this.Origin);
            var directionDistance = target.Dot(this.Direction);
            if (directionDistance < 0)
            {
                return target.Copy(this.Origin);
            }
            return target.Copy(this.Origin).AddScaledVector(this.Direction, directionDistance);
            //return target.copy(this.direction).multiplyScalar(directionDistance).add(this.origin);
        }
        public double DistanceToPoint(Vector3 point)
        {
            return Math.Sqrt(this.DistanceSqToPoint(point));
        }
        public double DistanceSqToPoint(Vector3 point)
        {
            var _vector = GetContext()._vector;
            var directionDistance = _vector.SubVectors(point, this.Origin).Dot(this.Direction);
            // point behind the ray
            if (directionDistance < 0)
            {
                return this.Origin.DistanceToSquared(point);
            }
            _vector.Copy(this.Origin).AddScaledVector(this.Direction, directionDistance);
            // _vector.copy(this.direction).multiplyScalar(directionDistance).add(this.origin);
            return _vector.DistanceToSquared(point);
        }
        public double DistanceSqToSegment(Vector3 v0, Vector3 v1, Vector3 optionalPointOnRay, Vector3 optionalPointOnSegment)
        {
            // from https://github.com/pmjoniak/GeometricTools/blob/master/GTEngine/Include/Mathematics/GteDistRaySegment.h
            // It returns the min distance between the ray and the segment
            // defined by v0 and v1
            // It can also set two optional targets :
            // - The closest point on the ray
            // - The closest point on the segment
            var ctx = GetContext();
            var _segCenter = ctx._segCenter;
            var _segDir = ctx._segDir;
            var _diff = ctx._diff;

            _segCenter.Copy(v0).Add(v1).MulScalar(0.5);
            _segDir.Copy(v1).Sub(v0).Normalize();
            _diff.Copy(this.Origin).Sub(_segCenter);
            var segExtent = v0.DistanceTo(v1) * 0.5;
            var a01 = -this.Direction.Dot(_segDir);
            var b0 = _diff.Dot(this.Direction);
            var b1 = -_diff.Dot(_segDir);
            var c = _diff.LengthSq();
            var det = Math.Abs(1 - a01 * a01);
            double s0, s1, sqrDist, extDet;
            if (det > 0)
            {
                // The ray and segment are not parallel.
                s0 = a01 * b1 - b0;
                s1 = a01 * b0 - b1;
                extDet = segExtent * det;
                if (s0 >= 0)
                {
                    if (s1 >= -extDet)
                    {
                        if (s1 <= extDet)
                        {
                            // region 0
                            // Minimum at interior points of ray and segment.
                            var invDet = 1 / det;
                            s0 *= invDet;
                            s1 *= invDet;
                            sqrDist = s0 * (s0 + a01 * s1 + 2 * b0) + s1 * (a01 * s0 + s1 + 2 * b1) + c;
                        }
                        else
                        {
                            // region 1
                            s1 = segExtent;
                            s0 = Math.Max(0, -(a01 * s1 + b0));
                            sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;
                        }
                    }
                    else
                    {
                        // region 5
                        s1 = -segExtent;
                        s0 = Math.Max(0, -(a01 * s1 + b0));
                        sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;
                    }
                }
                else
                {
                    if (s1 <= -extDet)
                    {
                        // region 4
                        s0 = Math.Max(0, -(-a01 * segExtent + b0));
                        s1 = (s0 > 0) ? -segExtent : Math.Min(Math.Max(-segExtent, -b1), segExtent);
                        sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;
                    }
                    else if (s1 <= extDet)
                    {
                        // region 3
                        s0 = 0;
                        s1 = Math.Min(Math.Max(-segExtent, -b1), segExtent);
                        sqrDist = s1 * (s1 + 2 * b1) + c;
                    }
                    else
                    {
                        // region 2
                        s0 = Math.Max(0, -(a01 * segExtent + b0));
                        s1 = (s0 > 0) ? segExtent : Math.Min(Math.Max(-segExtent, -b1), segExtent);
                        sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;
                    }
                }
            }
            else
            {
                // Ray and segment are parallel.
                s1 = (a01 > 0) ? -segExtent : segExtent;
                s0 = Math.Max(0, -(a01 * s1 + b0));
                sqrDist = -s0 * s0 + s1 * (s1 + 2 * b1) + c;
            }
            if (optionalPointOnRay != null)
            {
                optionalPointOnRay.Copy(this.Origin).AddScaledVector(this.Direction, s0);
                // optionalPointOnRay.copy(this.direction).multiplyScalar(s0).add(this.origin);
            }
            if (optionalPointOnSegment != null)
            {
                optionalPointOnSegment.Copy(_segCenter).AddScaledVector(_segDir, s1);
                //optionalPointOnSegment.copy(_segDir).multiplyScalar(s1).add(_segCenter);
            }
            return sqrDist;
        }
        public Vector3 IntersectSphere(Sphere sphere, Vector3 target)
        {
            var ctx = GetContext();
            var _vector = ctx._vector;
            _vector.SubVectors(sphere.Center, this.Origin);
            var tca = _vector.Dot(this.Direction);
            var d2 = _vector.Dot(_vector) - tca * tca;
            var radius2 = sphere.Radius * sphere.Radius;
            if (d2 > radius2) return null;
            var thc = Math.Sqrt(radius2 - d2);
            // t0 = first intersect point - entrance on front of sphere
            var t0 = tca - thc;
            // t1 = second intersect point - exit point on back of sphere
            var t1 = tca + thc;

            // test to see if t1 is behind the ray - if so, return null
            if (t1 < 0) return null;
            // test to see if t0 is behind the ray:
            // if it is, the ray is inside the sphere, so return the second exit point scaled by t1,
            // in order to always return an intersect point that is in front of the ray.
            if (t0 < 0) return this.At(t1, target);
            // else t0 is in front of the ray, so return the first collision point scaled by t0
            return this.At(t0, target);
        }
        public bool IntersectsSphere(Sphere sphere)
        {
            return this.DistanceSqToPoint(sphere.Center) <= (sphere.Radius * sphere.Radius);
        }
        public double DistanceToPlane(Plane plane)
        {
            var denominator = plane.Normal.Dot(this.Direction);
            if (denominator == 0)
            {
                // line is coplanar, return origin
                if (plane.DistanceToPoint(this.Origin) == 0)
                {
                    return 0;
                }
                // Null is preferable to undefined since undefined means.... it is undefined
                return double.NaN;
            }
            var t = -(this.Origin.Dot(plane.Normal) + plane.Constant) / denominator;
            // Return if the ray never intersects the plane
            return t >= 0 ? t : double.NaN;
        }
        public Vector3 IntersectPlane(Plane plane, Vector3 target = null)
        {
            var t = this.DistanceToPlane(plane);
            if (double.IsNaN(t))
            {
                return null;
            }
            return this.At(t, target);
        }
        public bool IntersectsPlane(Plane plane)
        {
            // check if the ray lies on the plane first
            var distToPoint = plane.DistanceToPoint(this.Origin);
            if (distToPoint == 0)
            {
                return true;
            }
            var denominator = plane.Normal.Dot(this.Direction);
            if (denominator * distToPoint < 0)
            {
                return true;
            }
            // ray origin is behind the plane (and is pointing behind it)
            return false;
        }
        public Vector3 IntersectBox(Box3 box, Vector3 target = null)
        {
            target = target ?? new Vector3();
            double tmin, tmax, tymin, tymax, tzmin, tzmax;
            double invdirx = 1 / this.Direction.X,
                invdiry = 1 / this.Direction.Y,
                invdirz = 1 / this.Direction.Z;
            var origin = this.Origin;
            if (invdirx >= 0)
            {
                tmin = (box.Min.X - origin.X) * invdirx;
                tmax = (box.Max.X - origin.X) * invdirx;
            }
            else
            {
                tmin = (box.Max.X - origin.X) * invdirx;
                tmax = (box.Min.X - origin.X) * invdirx;
            }
            if (invdiry >= 0)
            {
                tymin = (box.Min.Y - origin.Y) * invdiry;
                tymax = (box.Max.Y - origin.Y) * invdiry;
            }
            else
            {
                tymin = (box.Max.Y - origin.Y) * invdiry;
                tymax = (box.Min.Y - origin.Y) * invdiry;
            }
            if ((tmin > tymax) || (tymin > tmax)) return null;
            if (tymin > tmin || IsNaN(tmin)) tmin = tymin;
            if (tymax < tmax || IsNaN(tmax)) tmax = tymax;
            if (invdirz >= 0)
            {
                tzmin = (box.Min.Z - origin.Z) * invdirz;
                tzmax = (box.Max.Z - origin.Z) * invdirz;
            }
            else
            {
                tzmin = (box.Max.Z - origin.Z) * invdirz;
                tzmax = (box.Min.Z - origin.Z) * invdirz;
            }
            if ((tmin > tzmax) || (tzmin > tmax)) return null;
            if (tzmin > tmin || IsNaN(tmin)) tmin = tzmin;
            if (tzmax < tmax || IsNaN(tmax)) tmax = tzmax;
            //return point closest to the ray (positive side)
            if (tmax < 0) return null;
            return this.At(tmin >= 0 ? tmin : tmax, target);
        }
        public bool IntersectsBox(Box3 box)
        {
            return this.IntersectBox(box, GetContext()._vector) != null;
        }
        public Vector3 IntersectTriangle(Vector3 a, Vector3 b, Vector3 c, bool backfaceCulling, Vector3 target = null)
        {
            // Compute the offset origin, edges, and normal.
            // from https://github.com/pmjoniak/GeometricTools/blob/master/GTEngine/Include/Mathematics/GteIntrRay3Triangle3.h
            var ctx = GetContext();
            var _edge1 = ctx._edge1;
            var _edge2 = ctx._edge2;
            var _normal = ctx._normal;
            var _diff = ctx._diff;

            _edge1.SubVectors(b, a);
            _edge2.SubVectors(c, a);
            _normal.CrossVectors(_edge1, _edge2);
            // Solve Q + t*D = b1*E1 + b2*E2 (Q = kDiff, D = ray direction,
            // E1 = kEdge1, E2 = kEdge2, N = Cross(E1,E2)) by
            //   |Dot(D,N)|*b1 = sign(Dot(D,N))*Dot(D,Cross(Q,E2))
            //   |Dot(D,N)|*b2 = sign(Dot(D,N))*Dot(D,Cross(E1,Q))
            //   |Dot(D,N)|*t = -sign(Dot(D,N))*Dot(Q,N)
            var DdN = this.Direction.Dot(_normal);
            int sign;
            if (DdN > 0)
            {
                if (backfaceCulling) return null;
                sign = 1;
            }
            else if (DdN < 0)
            {
                sign = -1;
                DdN = -DdN;
            }
            else
            {
                return null;
            }
            _diff.SubVectors(this.Origin, a);
            var DdQxE2 = sign * this.Direction.Dot(_edge2.CrossVectors(_diff, _edge2));
            // b1 < 0, no intersection
            if (DdQxE2 < 0)
            {
                return null;
            }
            var DdE1xQ = sign * this.Direction.Dot(_edge1.Cross(_diff));
            // b2 < 0, no intersection
            if (DdE1xQ < 0)
            {
                return null;
            }
            // b1+b2 > 1, no intersection
            if (DdQxE2 + DdE1xQ > DdN)
            {
                return null;
            }
            // Line intersects triangle, check if ray does.
            var QdN = -sign * _diff.Dot(_normal);
            // t < 0, no intersection
            if (QdN < 0)
            {
                return null;
            }
            // Ray intersects triangle.
            return this.At(QdN / DdN, target);
        }
        public Ray ApplyMatrix4(Matrix4 matrix4)
        {
            this.Origin.ApplyMatrix4(matrix4);
            this.Direction.TransformDirection(matrix4);
            return this;
        }
        public virtual bool Equals(Ray ray)
        {
            return ray.Origin.Equals(this.Origin) && ray.Direction.Equals(this.Direction);
        }
        public virtual Ray Clone()
        {
            return new Ray().Copy(this);
        }
        #endregion

    }
    public sealed class RayContext
    {
        public readonly Vector3 _vector = new Vector3();
        public readonly Vector3 _segCenter = new Vector3();
        public readonly Vector3 _segDir = new Vector3();
        public readonly Vector3 _diff = new Vector3();
        public readonly Vector3 _edge1 = new Vector3();
        public readonly Vector3 _edge2 = new Vector3();
        public readonly Vector3 _normal = new Vector3();
    }
}
