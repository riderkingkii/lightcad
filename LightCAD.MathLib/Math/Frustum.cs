using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace LightCAD.MathLib
{
    public class Frustum
    {
        #region scope properties or methods
        //private static Sphere _sphere = new Sphere();
        //private static Vector3 _vector = new Vector3();
        public static FrustumContext GetContext()
        {
            var tctx = ThreadContext.GetCurrThreadContext();
            FrustumContext _context = tctx.FrustumCtx;
            return _context;
        }

        #endregion

        #region Properties

         

        public Plane[] Planes;

        #endregion

        #region constructor
        public Frustum(Plane p0 = null, Plane p1 = null, Plane p2 = null, Plane p3 = null, Plane p4 = null, Plane p5 = null)
        {
            if (p0 == null) p0 = new Plane();
            if (p1 == null) p1 = new Plane();
            if (p2 == null) p2 = new Plane();
            if (p3 == null) p3 = new Plane();
            if (p4 == null) p4 = new Plane();
            if (p5 == null) p5 = new Plane();
            this.Planes = new Plane[] { p0, p1, p2, p3, p4, p5 };
        }
        #endregion

        #region methods
        public Frustum Set(Plane p0, Plane p1, Plane p2, Plane p3, Plane p4, Plane p5)
        {
            Plane[] planes = this.Planes;
            planes[0].Copy(p0);
            planes[1].Copy(p1);
            planes[2].Copy(p2);
            planes[3].Copy(p3);
            planes[4].Copy(p4);
            planes[5].Copy(p5);
            return this;
        }
        public virtual Frustum Copy(Frustum frustum)
        {
            Plane[] planes = this.Planes;
            for (int i = 0; i < 6; i++)
            {
                planes[i].Copy(frustum.Planes[i]);
            }
            return this;
        }
        public Frustum SetFromProjectionMatrix(Matrix4 m)
        {
            Plane[] planes = this.Planes;
            double[] me = m.Elements;
            double me0 = me[0], me1 = me[1], me2 = me[2], me3 = me[3];
            double me4 = me[4], me5 = me[5], me6 = me[6], me7 = me[7];
            double me8 = me[8], me9 = me[9], me10 = me[10], me11 = me[11];
            double me12 = me[12], me13 = me[13], me14 = me[14], me15 = me[15];
            planes[0].SetComponents(me3 - me0, me7 - me4, me11 - me8, me15 - me12).Normalize();
            planes[1].SetComponents(me3 + me0, me7 + me4, me11 + me8, me15 + me12).Normalize();
            planes[2].SetComponents(me3 + me1, me7 + me5, me11 + me9, me15 + me13).Normalize();
            planes[3].SetComponents(me3 - me1, me7 - me5, me11 - me9, me15 - me13).Normalize();
            planes[4].SetComponents(me3 - me2, me7 - me6, me11 - me10, me15 - me14).Normalize();
            planes[5].SetComponents(me3 + me2, me7 + me6, me11 + me10, me15 + me14).Normalize();
            return this;
        }

        public bool IntersectsSphere(Sphere sphere)
        {
            Plane[] planes = this.Planes;
            Vector3 center = sphere.Center;
            double negRadius = -sphere.Radius;
            for (int i = 0; i < 6; i++)
            {
                double distance = planes[i].DistanceToPoint(center);
                if (distance < negRadius)
                {
                    return false;
                }
            }
            return true;
        }
        public bool IntersectsBox(Box3 box)
        {
            var _vector = GetContext()._vector;
            Plane[] planes = this.Planes;
            for (int i = 0; i < 6; i++)
            {
                Plane plane = planes[i];
                // corner at max distance
                _vector.X = plane.Normal.X > 0 ? box.Max.X : box.Min.X;
                _vector.Y = plane.Normal.Y > 0 ? box.Max.Y : box.Min.Y;
                _vector.Z = plane.Normal.Z > 0 ? box.Max.Z : box.Min.Z;
                if (plane.DistanceToPoint(_vector) < 0)
                {
                    return false;
                }
            }
            return true;
        }
        public bool ContainsPoint(Vector3 point)
        {
            Plane[] planes = this.Planes;
            for (int i = 0; i < 6; i++)
            {
                if (planes[i].DistanceToPoint(point) < 0)
                {
                    return false;
                }
            }
            return true;
        }
        public virtual Frustum Clone()
        {
            return new Frustum().Copy(this);
        }
        #endregion

    }
    public sealed class FrustumContext
    {
        public readonly Sphere _sphere = new Sphere();
        public readonly Vector3 _vector = new Vector3();
    }

}
