namespace LightCAD.MathLib
{
    public class TriangulateOutput
    {
        public ListEx<double> vertices;
        public ListEx<int> holeIndices;
    }
    public class ShapeUtils
    {
        #region scope properties or methods

        private static void removeDupEndPts(ListEx<Vector2> points)
        {

            int l = points.Length;

            if (l > 2 && points[l - 1].Equals(points[0]))
            {
                points.Pop();
            }
        }

        private static void addContour(ListEx<double> vertices, ListEx<Vector2> contour)
        {
            for (var i = 0; i < contour.Length; i++)
            {
                vertices.Push(contour[i].X);
                vertices.Push(contour[i].Y);
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// 用Green公式积分法求二维闭合曲线面积
        /// </summary>
        /// <param name="contour"></param>
        /// <returns></returns>
        public static double area(ListEx<Vector2> contour)
        {
            var n = contour.Length;
            var a = 0.0;
            for (int p = n - 1, q = 0; q < n; p = q++)
            {
                a += contour[p].X * contour[q].Y - contour[q].X * contour[p].Y;
            }
            return a * 0.5;
        }
        public static bool isClockWise(ListEx<Vector2> pts)
        {
            return ShapeUtils.area(pts) < 0;
        }
        public static ListEx<ListEx<int>> triangulateShape(ListEx<Vector2> contour, ListEx<ListEx<Vector2>> holes)
        {
            return triangulateShape(contour, holes, null);
        }
        public static ListEx<ListEx<int>> triangulateShape(ListEx<Vector2> contour, ListEx<ListEx<Vector2>> holes, TriangulateOutput output)
        {
            var vertices = new ListEx<double>(); // flat array of vertices like [ x0,y0, x1,y1, x2,y2, ... ]
            var holeIndices = new ListEx<int>(); // array of hole indices
            var faces = new ListEx<ListEx<int>>(); // final array of vertex indices like [ [ a,b,d ], [ b,c,d ] ]
            removeDupEndPts(contour);
            addContour(vertices, contour);
            //
            var holeIndex = contour.Length;
            holes.ForEach(removeDupEndPts);
            for (int i = 0; i < holes.Length; i++)
            {
                holeIndices.Push(holeIndex);
                holeIndex += holes[i].Length;
                addContour(vertices, holes[i]);
            }
            //
            var triangles = Earcut.triangulate(vertices, holeIndices);
            //
            for (int i = 0; i < triangles.Length; i += 3)
            {
                faces.Push(triangles.Slice(i, i + 3));
            }

            if (output != null)
            {
                output.vertices = vertices;
                output.holeIndices = holeIndices;
            }
            return faces;
        }
        #endregion

    }
}
