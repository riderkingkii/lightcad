﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public abstract class Surface3d
    {
        public List<Curve3d> OuterLoop=new List<Curve3d>();
        public List<List<Curve3d>> InnerLoops=new List<List<Curve3d>>();
        public virtual Tuple<double[], int[]> Trianglate() 
        {
            return null;
        }

        public virtual void Translate(Vector3 offset)
        {

        }

        public virtual void RotateRoundAxis(Vector3 origin, Vector3 axis, double angle)
        {

        }
    }
}
