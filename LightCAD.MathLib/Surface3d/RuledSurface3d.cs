﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.MathLib
{
    public class RuledSurface3d : Surface3d
    {
        public Curve3d Curve1 { get; private set; }

        public Curve3d Curve2 { get; private set; }

        public RuledSurface3d(Curve3d curve1, Curve3d curve2)
        {
            this.Curve1 = curve1;
            this.Curve2 = curve2;
            //this.OuterLoop = new List<Curve3d>()
            //{
            //    this.Curve1,
            //    new Line3d(this.Curve1.End, this.Curve2.End),
            //    this.Curve2.Reverse(),
            //    new Line3d(this.Curve2.Start, this.Curve2.Start),
            //};
        }

        public override Tuple<double[], int[]> Trianglate()
        {
            int div = 10;
            var points1 = this.Curve1.GetPoints(div);
            var points2 = this.Curve2.GetPoints(div);
            var allPoints = new ListEx<Vector3>();
            allPoints.AddRange(points1);
            allPoints.AddRange(points2);

            var indices = new ListEx<int>();
            for (int i = 0; i < div - 1; i++)
            {
                int nxi = i + 1;

                indices.Push(i, nxi, div + nxi);
                indices.Push(i, div + nxi, div + i);
            }

            var posArr = Utils.GenerateVertexArr(allPoints);

            return new Tuple<double[], int[]>(posArr, indices.ToArray());
        }
    }
}
