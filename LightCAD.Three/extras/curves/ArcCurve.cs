using System;
using System.Collections;
using System.Collections.Generic;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class ArcCurve : EllipseCurve
    {
        #region Properties

        #endregion

        #region constructor
        private ArcCurve() { }
        public ArcCurve(double aX, double aY, double aRadius, double aStartAngle, double aEndAngle, bool aClockwise)
              : base(aX, aY, aRadius, aRadius, aStartAngle, aEndAngle, aClockwise)
        {
            this.type = "ArcCurve";
        }
        #endregion
        public override Curve<Vector2> clone()
        {
            return new ArcCurve().copy(this);
        }
    }
}
