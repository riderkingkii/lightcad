using System;
using System.Collections;
using System.Collections.Generic;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class QuadraticBezierCurve : Curve<Vector2>
    {
        #region Properties

        public Vector2 v0;
        public Vector2 v1;
        public Vector2 v2;

        #endregion

        #region constructor
        public QuadraticBezierCurve(Vector2 v0 = null, Vector2 v1 = null, Vector2 v2 = null)
        {
            if (v0 == null) v0 = new Vector2();
            if (v1 == null) v1 = new Vector2();
            if (v2 == null) v2 = new Vector2();
            this.type = "QuadraticBezierCurve";
            this.v0 = v0;
            this.v1 = v1;
            this.v2 = v2;
        }
        #endregion

        #region methods
        public override Vector2 getPoint(double t, Vector2 optionalTarget = null)
        {
            if (optionalTarget == null) optionalTarget = new Vector2();
            var point = optionalTarget;
            Vector2 v0 = this.v0, v1 = this.v1, v2 = this.v2;
            point.Set(
                Interpolations.QuadraticBezier(t, v0.X, v1.X, v2.X),
                Interpolations.QuadraticBezier(t, v0.Y, v1.Y, v2.Y)
            );
            return point;
        }
        public override Curve<Vector2> copy(Curve<Vector2> source)
        {
            return copy(source as QuadraticBezierCurve);
        }
        public QuadraticBezierCurve copy(QuadraticBezierCurve source)
        {
            base.copy(source);
            this.v0.Copy(source.v0);
            this.v1.Copy(source.v1);
            this.v2.Copy(source.v2);
            return this;
        }

        public override Curve<Vector2> clone()
        {
            return new QuadraticBezierCurve().copy(this);
        }

        #endregion

    }
}
