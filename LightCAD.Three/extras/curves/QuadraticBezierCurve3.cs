using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class QuadraticBezierCurve3 : Curve<Vector3>
    {
        #region Properties

        public Vector3 v0;
        public Vector3 v1;
        public Vector3 v2;

        #endregion

        #region constructor
        public QuadraticBezierCurve3(Vector3 v0 = null, Vector3 v1 = null, Vector3 v2 = null)
        {
            if (v0 == null) v0 = new Vector3();
            if (v1 == null) v1 = new Vector3();
            if (v2 == null) v2 = new Vector3();
            this.type = "QuadraticBezierCurve3";
            this.v0 = v0;
            this.v1 = v1;
            this.v2 = v2;
        }
        #endregion

        #region methods
        public override Vector3 getPoint(double t, Vector3 optionalTarget = null)
        {
            if (optionalTarget == null) optionalTarget = new Vector3();
            var point = optionalTarget;
            Vector3 v0 = this.v0, v1 = this.v1, v2 = this.v2;
            point.Set(
                Interpolations.QuadraticBezier(t, v0.X, v1.X, v2.X),
                Interpolations.QuadraticBezier(t, v0.Y, v1.Y, v2.Y),
               Interpolations.QuadraticBezier(t, v0.Z, v1.Z, v2.Z)
            );
            return point;
        }
        public override Curve<Vector3> copy(Curve<Vector3> source)
        {
            return copy(source as QuadraticBezierCurve3);
        }
        public QuadraticBezierCurve3 copy(QuadraticBezierCurve3 source)
        {
            base.copy(source);
            this.v0.Copy(source.v0);
            this.v1.Copy(source.v1);
            this.v2.Copy(source.v2);
            return this;
        }

        public override Curve<Vector3> clone()
        {
            return new QuadraticBezierCurve3().copy(this);
        }

        #endregion

    }
}
