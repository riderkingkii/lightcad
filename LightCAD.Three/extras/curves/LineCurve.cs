using System;
using System.Collections;
using System.Collections.Generic;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class LineCurve : Curve<Vector2>
    {
        #region Properties
        public Vector2 v1;
        public Vector2 v2;

        #endregion

        #region constructor
        public LineCurve(Vector2 v1 = null, Vector2 v2 = null)
        {
            if (v1 == null) v1 = new Vector2();
            if (v2 == null) v2 = new Vector2();
            this.type = "LineCurve";
            this.v1 = v1;
            this.v2 = v2;
        }
        #endregion

        #region methods
        public override Vector2 getPoint(double t, Vector2 optionalTarget = null)
        {
            if (optionalTarget == null) optionalTarget = new Vector2();
            var point = optionalTarget;
            if (t == 1)
            {
                point.Copy(this.v2);
            }
            else
            {
                point.Copy(this.v2).Sub(this.v1);
                point.MultiplyScalar(t).Add(this.v1);
            }
            return point;
        }
        public override Vector2 getPointAt(double u, Vector2 optionalTarget = null)
        {
            return this.getPoint(u, optionalTarget);
        }
        public override Vector2 getTangent(double t = 0, Vector2 optionalTarget = null)
        {
            var tangent = optionalTarget ?? new Vector2();
            tangent.Copy(this.v2).Sub(this.v1).Normalize();
            return tangent;
        }
        public override Vector2 getTangentAt(double u, Vector2 optionalTarget)
        {

            return this.getTangent(u, optionalTarget);

        }
        public override Curve<Vector2> copy(Curve<Vector2> source)
        {
            return copy(source as LineCurve);
        }
        public LineCurve copy(LineCurve source)
        {
            base.copy(source);
            this.v1.Copy(source.v1);
            this.v2.Copy(source.v2);
            return this;
        }
        public double cacheLength;
        public Vector2 direction;
        public override double getLength()
        {
            var dir = v2.Clone().Sub(v1);
            this.cacheLength = dir.Length();
            this.direction = dir.Normalize();
            return this.cacheLength;    
        }
        public override Curve<Vector2> clone()
        {
            return new LineCurve().copy(this);
        }

        #endregion

    }
}
