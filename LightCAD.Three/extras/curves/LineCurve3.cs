using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class LineCurve3 : Curve<Vector3>
    {
        #region Properties
        public Vector3 v1;
        public Vector3 v2;

        #endregion

        #region constructor
        public LineCurve3(Vector3 v1 = null, Vector3 v2 = null)
        {
            if (v1 == null) v1 = new Vector3();
            if (v2 == null) v2 = new Vector3();
            this.type = "LineCurve3";
            this.v1 = v1;
            this.v2 = v2;
        }
        #endregion

        #region methods
        public override Vector3 getPoint(double t, Vector3 optionalTarget = null)
        {
            if (optionalTarget == null) optionalTarget = new Vector3();
            var point = optionalTarget;
            if (t == 1)
            {
                point.Copy(this.v2);
            }
            else
            {
                point.Copy(this.v2).Sub(this.v1);
                point.MulScalar(t).Add(this.v1);
            }
            return point;
        }
        public override Vector3 getPointAt(double u, Vector3 optionalTarget = null)
        {
            return this.getPoint(u, optionalTarget);
        }
        public override Vector3 getTangent(double t, Vector3 optionalTarget = null)
        {
            optionalTarget = optionalTarget ?? new Vector3();
            return optionalTarget.SubVectors(this.v2, this.v1).Normalize();
        }
        public override Vector3 getTangentAt(double u, Vector3 optionalTarget = null)
        {
            optionalTarget = optionalTarget ?? new Vector3();
            return this.getTangent(u, optionalTarget);
        }
        public override Curve<Vector3> copy(Curve<Vector3> source)
        {
            return copy(source as LineCurve3);
        }
        public LineCurve3 copy(LineCurve3 source)
        {
            base.copy(source);
            this.v1.Copy(source.v1);
            this.v2.Copy(source.v2);
            return this;
        }

        public override Curve<Vector3> clone()
        {
            return new LineCurve3().copy(this);
        }

        #endregion

    }
}
