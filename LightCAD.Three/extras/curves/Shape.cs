using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{

    public class Shape : Path
    {
        public class ShapeHoles
        {
            public ListEx<Vector2> shape;
            public ListEx<ListEx<Vector2>> holes;
        }
        #region Properties

        public string uuid;
        public JsObj<object> ext;
        //public string type;
        public ListEx<Path> holes;

        #endregion

        #region constructor
        public Shape(ListEx<Vector2> points = null) : base(points)
        {
            this.uuid = MathEx.GenerateUUID();
            this.type = "Shape";
            this.holes = new ListEx<Path>();
            this.ext = new JsObj<object>();
        }
        #endregion

        #region methods
        public ListEx<ListEx<Vector2>> getPointsHoles(int divisions)
        {
            var holesPts = new ListEx<ListEx<Vector2>>();
            for (int i = 0, l = this.holes.Count; i < l; i++)
            {
                holesPts[i] = this.holes[i].getPoints(divisions);
            }
            return holesPts;
        }
        public ShapeHoles extractPoints(int divisions)
        {
            return new ShapeHoles
            {
                shape = this.getPoints(divisions),
                holes = this.getPointsHoles(divisions)
            };
        }
        public Shape copy(Shape source)
        {
            base.copy(source);
            this.holes = new ListEx<Path>();
            for (int i = 0, l = source.holes.Count; i < l; i++)
            {
                var hole = source.holes[i];
                this.holes.Push((Path)hole.clone());
            }
            return this;
        }
        public override Curve<Vector2> copy(Curve<Vector2> source)
        {
            return copy(source as Shape);
        }
        public override Curve<Vector2> clone()
        {
            return new Shape().copy(this);
        }
        //public override Curve<Vector2> fromJSON(JObject json)
        //{
        //    base.fromJSON(json);

        //    this.uuid = json.GetString(uuid);
        //    this.holes = new JsArr<Path>();
        //    var holeArr = json.GetArray("holes");
        //    for (int i = 0, l = holeArr.Count; i < l; i++)
        //    {
        //        var hole = holeArr[i] as JObject;
        //        this.holes.push(new Path().fromJSON(hole) as Path);
        //    }

        //    return this;
        //}
        #endregion
    }
}
