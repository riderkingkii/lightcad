using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class PMREMGenerator
    {
        #region scope properties or methods
        private const int LOD_MIN = 4;
        private static readonly ListEx<double> EXTRA_LOD_SIGMA = new ListEx<double> { 0.125, 0.215, 0.35, 0.446, 0.526, 0.582 };
        private const int MAX_SAMPLES = 20;
        private static readonly OrthographicCamera _flatCamera = new OrthographicCamera();
        private static readonly Color _clearColor = new Color();
        private static readonly double PHI = (1 + Math.Sqrt(5)) / 2;
        private static readonly double INV_PHI = 1 / PHI;

        private static readonly ListEx<Vector3> _axisDirections = new ListEx<Vector3>()
            {
                /*@__PURE__*/ new Vector3( 1, 1, 1 ),
	            /*@__PURE__*/ new Vector3( - 1, 1, 1 ),
	            /*@__PURE__*/ new Vector3( 1, 1, - 1 ),
	            /*@__PURE__*/ new Vector3( - 1, 1, - 1 ),
	            /*@__PURE__*/ new Vector3( 0, PHI, INV_PHI ),
	            /*@__PURE__*/ new Vector3( 0, PHI, - INV_PHI ),
	            /*@__PURE__*/ new Vector3( INV_PHI, 0, PHI ),
	            /*@__PURE__*/ new Vector3( - INV_PHI, 0, PHI ),
	            /*@__PURE__*/ new Vector3( PHI, INV_PHI, 0 ),
	            /*@__PURE__*/ new Vector3( - PHI, INV_PHI, 0 )
            };

        #endregion
        private WebGLRenderTarget _oldTarget = null;
        private WebGLRenderer _renderer;
        private WebGLRenderTarget _pingPongRenderTarget;

        private int _lodMax;
        private int _cubeSize;
        private ListEx<BufferGeometry> _lodPlanes;
        private ListEx<int> _sizeLods;
        private ListEx<double> _sigmas;

        private ShaderMaterial _blurMaterial;
        private ShaderMaterial _cubemapMaterial;
        private ShaderMaterial _equirectMaterial;
        public PMREMGenerator(WebGLRenderer renderer)
        {
            this._renderer = renderer;
            this._pingPongRenderTarget = null;

            this._lodMax = 0;
            this._cubeSize = 0;
            this._lodPlanes = new ListEx<BufferGeometry>();
            this._sizeLods = new ListEx<int>();
            this._sigmas = new ListEx<double>();

            this._blurMaterial = null;
            this._cubemapMaterial = null;
            this._equirectMaterial = null;

            this._compileMaterial(this._blurMaterial);

        }
        /**
 * Generates a PMREM from a supplied Scene, which can be faster than using an
 * image if networking bandwidth is low. Optional sigma specifies a blur radius
 * in radians to be applied to the scene before PMREM generation. Optional near
 * and far planes ensure the scene is rendered in its entirety (the cubeCamera
 * is placed at the origin).
 */
        public WebGLRenderTarget fromScene(Scene scene, double sigma = 0, double near = 0.1, double far = 100)
        {

            _oldTarget = this._renderer.getRenderTarget();

            this._setSize(256);

            var cubeUVRenderTarget = this._allocateTargets();
            cubeUVRenderTarget.depthBuffer = true;

            this._sceneToCubeUV(scene, near, far, cubeUVRenderTarget);

            if (sigma > 0)
            {

                this._blur(cubeUVRenderTarget, 0, 0, sigma);

            }

            this._applyPMREM(cubeUVRenderTarget);
            this._cleanup(cubeUVRenderTarget);

            return cubeUVRenderTarget;

        }

        /**
         * Generates a PMREM from an equirectangular texture, which can be either LDR
         * or HDR. The ideal input image size is 1k (1024 x 512),
         * as this matches best with the 256 x 256 cubemap output.
         */
        public WebGLRenderTarget fromEquirectangular(Texture equirectangular, WebGLRenderTarget renderTarget = null)
        {

            return this._fromTexture(equirectangular, renderTarget);

        }

        /**
         * Generates a PMREM from an cubemap texture, which can be either LDR
         * or HDR. The ideal input cube size is 256 x 256,
         * as this matches best with the 256 x 256 cubemap output.
         */
        public WebGLRenderTarget fromCubemap(CubeTexture cubemap, WebGLRenderTarget renderTarget = null)
        {
            return this._fromTexture(cubemap, renderTarget);
        }
        /**
         * Pre-compiles the cubemap shader. You can get faster start-up by invoking this method during
         * your texture"s network fetch for increased concurrency.
         */
        public void compileCubemapShader()
        {

            if (this._cubemapMaterial == null)
            {

                this._cubemapMaterial = _getCubemapMaterial();
                this._compileMaterial(this._cubemapMaterial);

            }

        }

        /**
         * Pre-compiles the equirectangular shader. You can get faster start-up by invoking this method during
         * your texture"s network fetch for increased concurrency.
         */
        public void compileEquirectangularShader()
        {

            if (this._equirectMaterial == null)
            {

                this._equirectMaterial = _getEquirectMaterial();
                this._compileMaterial(this._equirectMaterial);

            }

        }

        /**
         * Disposes of the PMREMGenerator"s internal memory. Note that PMREMGenerator is a static class,
         * so you should not need more than one PMREMGenerator object. If you do, calling dispose() on
         * one of them will cause any others to also become unusable.
         */
        public void dispose()
        {

            this._dispose();

            if (this._cubemapMaterial != null) this._cubemapMaterial.dispose();
            if (this._equirectMaterial != null) this._equirectMaterial.dispose();

        }
        private void _setSize(int cubeSize)
        {

            this._lodMax = (int)Math.Floor(Math.Log2(cubeSize));
            this._cubeSize = (int)Math.Pow(2, this._lodMax);

        }

        private void _dispose()
        {

            if (this._blurMaterial != null) this._blurMaterial.dispose();

            if (this._pingPongRenderTarget != null) this._pingPongRenderTarget.dispose();

            for (int i = 0; i < this._lodPlanes.Length; i++)
            {

                this._lodPlanes[i].dispose();

            }

        }
        private void _cleanup(WebGLRenderTarget outputTarget)
        {

            this._renderer.setRenderTarget(_oldTarget);
            outputTarget.scissorTest = false;
            _setViewport(outputTarget, 0, 0, outputTarget.width, outputTarget.height);

        }
        private WebGLRenderTarget _fromTexture(Texture texture, WebGLRenderTarget renderTarget)
        {

            if (texture.mapping == CubeReflectionMapping || texture.mapping == CubeRefractionMapping)
            {

                this._setSize((texture as CubeTexture).images.Length == 0 ? 16 : (texture.images[0].width > 0 ? texture.images[0].width : (texture.images[0] as Texture).image.width));

            }
            else
            { // Equirectangular

                this._setSize(texture.image.width / 4);

            }

            _oldTarget = this._renderer.getRenderTarget();

            var cubeUVRenderTarget = renderTarget ?? this._allocateTargets();
            this._textureToCubeUV(texture, cubeUVRenderTarget);
            this._applyPMREM(cubeUVRenderTarget);
            this._cleanup(cubeUVRenderTarget);

            return cubeUVRenderTarget;

        }

        public WebGLRenderTarget _allocateTargets()
        {

            var width = 3 * (int)Math.Max(this._cubeSize, 16 * 7);
            var height = 4 * (int)this._cubeSize;

            var _params = new RenderTargetOptions
            {
                magFilter = LinearFilter,
                minFilter = LinearFilter,
                generateMipmaps = false,
                type = HalfFloatType,
                format = RGBAFormat,
                encoding = LinearEncoding,
                depthBuffer = false

            };

            var cubeUVRenderTarget = _createRenderTarget(width, height, _params);

            if (this._pingPongRenderTarget == null || this._pingPongRenderTarget.width != width || this._pingPongRenderTarget.height != height)
            {

                if (this._pingPongRenderTarget != null)
                {

                    this._dispose();

                }

                this._pingPongRenderTarget = _createRenderTarget(width, height, _params);

                var _lodMax = this._lodMax;
                //( { sizeLods: this._sizeLods, lodPlanes: this._lodPlanes, sigmas: this._sigmas } = _createPlanes(_lodMax) );
                var planes = _createPlanes(_lodMax);
                this._sizeLods = planes.sizeLods;
                this._lodPlanes = planes.lodPlanes;
                this._sigmas = planes.sigmas;
                this._blurMaterial = _getBlurShader(_lodMax, width, height);

            }

            return cubeUVRenderTarget;

        }

        private void _compileMaterial(Material material)
        {

            var tmpMesh = new Mesh(this._lodPlanes[0], material);
            this._renderer.compile(tmpMesh, _flatCamera);

        }

        private void _sceneToCubeUV(Scene scene, double near, double far, WebGLRenderTarget cubeUVRenderTarget)
        {
            var fov = 90;
            var aspect = 1;
            var cubeCamera = new PerspectiveCamera(fov, aspect, near, far);
            var upSign = new ListEx<int> { 1, -1, 1, 1, 1, 1 };
            var forwardSign = new ListEx<int> { 1, 1, 1, -1, -1, -1 };
            var renderer = this._renderer;

            var originalAutoClear = renderer.autoClear;
            var toneMapping = renderer.toneMapping;
            renderer.getClearColor(_clearColor);

            renderer.toneMapping = NoToneMapping;
            renderer.autoClear = false;

            var backgroundMaterial = new MeshBasicMaterial
            {
                name = "PMREM.Background",
                side = BackSide,
                depthWrite = false,
                depthTest = false
            };

            var backgroundBox = new Mesh(new BoxGeometry(), backgroundMaterial);

            var useSolidColor = false;
            var background = scene.background;

            if (background != null)
            {

                if (background is Color)
                {

                    backgroundMaterial.color.Copy(background as Color);
                    scene.background = null;
                    useSolidColor = true;

                }

            }
            else
            {

                backgroundMaterial.color.Copy(_clearColor);
                useSolidColor = true;

            }

            for (var i = 0; i < 6; i++)
            {

                var col = i % 3;

                if (col == 0)
                {

                    cubeCamera.up.Set(0, upSign[i], 0);
                    cubeCamera.lookAt(forwardSign[i], 0, 0);

                }
                else if (col == 1)
                {

                    cubeCamera.up.Set(0, 0, upSign[i]);
                    cubeCamera.lookAt(0, forwardSign[i], 0);

                }
                else
                {

                    cubeCamera.up.Set(0, upSign[i], 0);
                    cubeCamera.lookAt(0, 0, forwardSign[i]);
                }
                var size = this._cubeSize;
                _setViewport(cubeUVRenderTarget, col * size, i > 2 ? size : 0, size, size);
                renderer.setRenderTarget(cubeUVRenderTarget);
                //by yf 目前发现opengl需要在Scissor范围内先clear一次缓冲区才能正常离屏渲染
                //      opengles貌似不需要，只要setScissorTest就会clear指定范围的缓冲区
                renderer.clear();
                if (useSolidColor)
                {
                    renderer.render(backgroundBox, cubeCamera);

                }

                renderer.render(scene, cubeCamera);

            }

            backgroundBox.geometry.dispose();
            backgroundBox.material.dispose();

            renderer.toneMapping = toneMapping;
            renderer.autoClear = originalAutoClear;
            scene.background = background;

        }

        private void _textureToCubeUV(Texture texture, WebGLRenderTarget cubeUVRenderTarget)
        {

            var renderer = this._renderer;

            var isCubeTexture = (texture.mapping == CubeReflectionMapping || texture.mapping == CubeRefractionMapping);

            if (isCubeTexture)
            {

                if (this._cubemapMaterial == null)
                {

                    this._cubemapMaterial = _getCubemapMaterial();

                }

                (this._cubemapMaterial as ShaderMaterial).uniforms["flipEnvMap"].value = (texture.isRenderTargetTexture == false) ? -1 : 1;

            }
            else
            {

                if (this._equirectMaterial == null)
                {

                    this._equirectMaterial = _getEquirectMaterial();

                }

            }

            var material = isCubeTexture ? this._cubemapMaterial : this._equirectMaterial;
            var mesh = new Mesh(this._lodPlanes[0], material);

            var uniforms = (material as ShaderMaterial).uniforms;
            uniforms["envMap"].value = texture;
            var size = this._cubeSize;

            _setViewport(cubeUVRenderTarget, 0, 0, 3 * size, 2 * size);

            renderer.setRenderTarget(cubeUVRenderTarget);
            renderer.render(mesh, _flatCamera);

        }

        private void _applyPMREM(WebGLRenderTarget cubeUVRenderTarget)
        {

            var renderer = this._renderer;
            var autoClear = renderer.autoClear;
            renderer.autoClear = false;

            for (var i = 1; i < this._lodPlanes.Length; i++)
            {

                var sigma = Math.Sqrt(this._sigmas[i] * this._sigmas[i] - this._sigmas[i - 1] * this._sigmas[i - 1]);

                var poleAxis = _axisDirections[(i - 1) % _axisDirections.Length];

                this._blur(cubeUVRenderTarget, i - 1, i, sigma, poleAxis);

            }

            renderer.autoClear = autoClear;

        }
        private void _blur(WebGLRenderTarget cubeUVRenderTarget, int lodIn, int lodOut, double sigma, object poleAxis = null)
        {

            var pingPongRenderTarget = this._pingPongRenderTarget;

            this._halfBlur(
                cubeUVRenderTarget,
                pingPongRenderTarget,
                lodIn,
                lodOut,
                sigma,
                "latitudinal",
                poleAxis);

            this._halfBlur(
                pingPongRenderTarget,
                cubeUVRenderTarget,
                lodOut,
                lodOut,
                sigma,
                "longitudinal",
                poleAxis);

        }

        private void _halfBlur(WebGLRenderTarget targetIn, WebGLRenderTarget targetOut, int lodIn, int lodOut, double sigmaRadians, string direction, object poleAxis)
        {

            var renderer = this._renderer;
            var blurMaterial = this._blurMaterial;

            if (direction != "latitudinal" && direction != "longitudinal")
            {

                console.error(
                    "blur direction must be either latitudinal or longitudinal!");

            }

            // Number of standard deviations at which to cut off the discrete approximation.
            var STANDARD_DEVIATIONS = 3;

            var blurMesh = new Mesh(this._lodPlanes[lodOut], blurMaterial);
            var blurUniforms = blurMaterial.uniforms;

            var pixels = this._sizeLods[lodIn] - 1;
            var radiansPerPixel = MathEx.IsFinite(sigmaRadians) ? Math.PI / (2 * pixels) : 2 * Math.PI / (2 * MAX_SAMPLES - 1);
            var sigmaPixels = sigmaRadians / radiansPerPixel;
            var samples = MathEx.IsFinite(sigmaRadians) ? 1 + Math.Floor(STANDARD_DEVIATIONS * sigmaPixels) : MAX_SAMPLES;

            if (samples > MAX_SAMPLES)
            {

                console.warn($"sigmaRadians, ${sigmaRadians}" +
                    $", is too large and will clip, as it requested {samples}" +
                $"samples when the maximum is set to {MAX_SAMPLES}");

            }

            var weights = new ListEx<double>();
            double sum = 0;

            for (var i = 0; i < MAX_SAMPLES; ++i)
            {

                var _x = i / sigmaPixels;
                var weight = Math.Exp(-_x * _x / 2);
                weights.Push(weight);

                if (i == 0)
                {

                    sum += weight;

                }
                else if (i < samples)
                {

                    sum += 2 * weight;

                }

            }

            for (var i = 0; i < weights.Length; i++)
            {

                weights[i] = weights[i] / sum;

            }

            blurUniforms["envMap"].value = targetIn.texture;
            blurUniforms["samples"].value = samples;
            blurUniforms["weights"].value = weights;
            blurUniforms["latitudinal"].value = direction == "latitudinal";

            if (poleAxis != null)
            {

                blurUniforms["poleAxis"].value = poleAxis;

            }

            var _lodMax = this._lodMax;
            blurUniforms["dTheta"].value = radiansPerPixel;
            blurUniforms["mipInt"].value = _lodMax - lodIn;

            var outputSize = this._sizeLods[lodOut];
            var x = 3 * outputSize * (lodOut > _lodMax - LOD_MIN ? lodOut - _lodMax + LOD_MIN : 0);
            var y = 4 * (this._cubeSize - outputSize);

            _setViewport(targetOut, x, y, 3 * outputSize, 2 * outputSize);
            renderer.setRenderTarget(targetOut);
            renderer.render(blurMesh, _flatCamera);

        }
        public sealed class Planes
        {
            public ListEx<BufferGeometry> lodPlanes;
            public ListEx<int> sizeLods;
            public ListEx<double> sigmas;
        }
        private Planes _createPlanes(int lodMax)
        {

            var lodPlanes = new ListEx<BufferGeometry>();
            var sizeLods = new ListEx<int>();
            var sigmas = new ListEx<double>();

            var lod = lodMax;

            var totalLods = lodMax - LOD_MIN + 1 + EXTRA_LOD_SIGMA.Length;

            for (var i = 0; i < totalLods; i++)
            {

                var sizeLod = (int)Math.Pow(2, lod);
                sizeLods.Push(sizeLod);
                var sigma = 1.0 / sizeLod;

                if (i > lodMax - LOD_MIN)
                {

                    sigma = EXTRA_LOD_SIGMA[i - lodMax + LOD_MIN - 1];

                }
                else if (i == 0)
                {

                    sigma = 0;

                }

                sigmas.Push(sigma);

                var texelSize = 1.0 / (sizeLod - 2);
                var min = -texelSize;
                var max = 1 + texelSize;
                var uv1 = new double[] { min, min, max, min, max, max, min, min, max, max, min, max };

                var cubeFaces = 6;
                var vertices = 6;
                var positionSize = 3;
                var uvSize = 2;
                var faceIndexSize = 1;

                var position = new double[positionSize * vertices * cubeFaces];
                var uv = new double[uvSize * vertices * cubeFaces];
                var faceIndex = new double[faceIndexSize * vertices * cubeFaces];

                for (var face = 0; face < cubeFaces; face++)
                {

                    var x = (double)(face % 3) * 2.0 / 3.0 - 1.0;
                    var y = face > 2 ? 0.0 : -1.0;
                    var coordinates = new double[] {
                            x, y, 0,
                            x + 2 / 3.0, y, 0,
                            x + 2 / 3.0, y + 1, 0,
                            x, y, 0,
                            x + 2 / 3.0, y + 1, 0,
                            x, y + 1, 0
                        };
                    position.set(coordinates, positionSize * vertices * face);
                    uv.set(uv1, uvSize * vertices * face);
                    var fill = new int[] { face, face, face, face, face, face };
                    faceIndex.set(fill, faceIndexSize * vertices * face);

                }

                var planes = new BufferGeometry();
                planes.setAttribute("position", new BufferAttribute(position, positionSize));
                planes.setAttribute("uv", new BufferAttribute(uv, uvSize));
                planes.setAttribute("faceIndex", new BufferAttribute(faceIndex, faceIndexSize));
                lodPlanes.Push(planes);

                if (lod > LOD_MIN)
                {

                    lod--;

                }

            }

            return new Planes { lodPlanes = lodPlanes, sizeLods = sizeLods, sigmas = sigmas };

        }

        private static WebGLRenderTarget _createRenderTarget(int width, int height, RenderTargetOptions options)
        {

            var cubeUVRenderTarget = new WebGLRenderTarget(width, height, options);
            cubeUVRenderTarget.texture.mapping = CubeUVReflectionMapping;
            cubeUVRenderTarget.texture.name = "PMREM.cubeUv";
            cubeUVRenderTarget.scissorTest = true;
            return cubeUVRenderTarget;

        }

        private void _setViewport(WebGLRenderTarget target, double x, double y, int width, int height)
        {

            target.viewport.Set(x, y, width, height);
            target.scissor.Set(x, y, width, height);

        }

        private static ShaderMaterial _getBlurShader(int lodMax, int width, int height)
        {

            var weights = new ListEx<double>(MAX_SAMPLES);
            var poleAxis = new Vector3(0, 1, 0);
            var shaderMaterial = new ShaderMaterial
            {
                name = "SphericalGaussianBlur",
                defines = new JsObj<object> {
                        { "n", MAX_SAMPLES },
                        {"CUBEUV_TEXEL_WIDTH", 1.0 / width },
                        { "CUBEUV_TEXEL_HEIGHT", 1.0 / height},
                        {"CUBEUV_MAX_MIP", $"{lodMax}.0"},
                    },

                uniforms = new Uniforms{
                        { "envMap",new Uniform { value= null } },
                        { "samples",new Uniform { value= 1 } },
                        {"weights",new Uniform { value= weights}},
                        {"latitudinal",new Uniform { value= false }},
                        {"dTheta",new Uniform{ value= 0.0 }},
                        {"mipInt",new Uniform { value= 0 }},
                        {"poleAxis",new Uniform { value= poleAxis } }
                    },

                vertexShader = _getCommonVertexShader(),

                fragmentShader =/* glsl */ @"
			            precision mediump float;
			            precision mediump int;

			            varying vec3 vOutputDirection;

			            uniform sampler2D envMap;
			            uniform int samples;
			            uniform float weights[ n ];
			            uniform bool latitudinal;
			            uniform float dTheta;
			            uniform float mipInt;
			            uniform vec3 poleAxis;

			            #define ENVMAP_TYPE_CUBE_UV
			            #include <cube_uv_reflection_fragment>

			            vec3 getSample( float theta, vec3 axis ) {

			            	float cosTheta = cos( theta );
			            	// Rodrigues' axis-angle rotation
			            	vec3 sampleDirection = vOutputDirection * cosTheta
			            		+ cross( axis, vOutputDirection ) * sin( theta )
			            		+ axis * dot( axis, vOutputDirection ) * ( 1.0 - cosTheta );

			            	return bilinearCubeUV( envMap, sampleDirection, mipInt );

			            }

			            void main() {

			            	vec3 axis = latitudinal ? poleAxis : cross( poleAxis, vOutputDirection );

			            	if ( all( equal( axis, vec3( 0.0 ) ) ) ) {

			            		axis = vec3( vOutputDirection.z, 0.0, - vOutputDirection.x );

			            	}

			            	axis = Normalize( axis );

			            	gl_FragColor = vec4( 0.0, 0.0, 0.0, 1.0 );
			            	gl_FragColor.rgb += weights[ 0 ] * getSample( 0.0, axis );

			            	for ( int i = 1; i < n; i++ ) {

			            		if ( i >= samples ) {

			            			break;

			            		}

			            		float theta = dTheta * float( i );
			            		gl_FragColor.rgb += weights[ i ] * getSample( -1.0 * theta, axis );
			            		gl_FragColor.rgb += weights[ i ] * getSample( theta, axis );

			            	}

			            }",
                blending = NoBlending,
                depthTest = false,
                depthWrite = false

            };

            return shaderMaterial;
        }

        private static ShaderMaterial _getEquirectMaterial()
        {

            return new ShaderMaterial
            {

                name = "EquirectangularToCubeUV",

                uniforms = new Uniforms
{
            { "envMap",new Uniform { value= null } }
             },

                vertexShader = _getCommonVertexShader(),

                fragmentShader = /* glsl */@"

			precision mediump float;
			precision mediump int;

			varying vec3 vOutputDirection;

			uniform sampler2D envMap;

			#include <common>

			void main() {

				vec3 outputDirection = Normalize( vOutputDirection );
				vec2 uv = equirectUv( outputDirection );

				gl_FragColor = vec4( texture2D ( envMap, uv ).rgb, 1.0 );

			}
		",

                blending = NoBlending,
                depthTest = false,
                depthWrite = false

            };

        }

        private static ShaderMaterial _getCubemapMaterial()
        {

            return new ShaderMaterial
            {

                name = "CubemapToCubeUV",

                uniforms = new Uniforms
                    {
                        { "envMap", new Uniform{ value=null } },
                        { "flipEnvMap",new Uniform { value= -1 } }
                    },

                vertexShader = _getCommonVertexShader(),

                fragmentShader = /* glsl */@"

			            precision mediump float;
			            precision mediump int;

			            uniform float flipEnvMap;

			            varying vec3 vOutputDirection;

			            uniform samplerCube envMap;

			            void main() {

			            	gl_FragColor = textureCube( envMap, vec3( flipEnvMap * vOutputDirection.x, vOutputDirection.yz ) );

			            }
		",

                blending = NoBlending,
                depthTest = false,
                depthWrite = false



            };

        }

        private static string _getCommonVertexShader()
        {

            return /* glsl */@"

		        precision mediump float;
		        precision mediump int;

		        attribute float faceIndex;

		        varying vec3 vOutputDirection;

		        // RH coordinate system; PMREM face-indexing convention
		        vec3 getDirection( vec2 uv, float face ) {

		        	uv = 2.0 * uv - 1.0;

		        	vec3 direction = vec3( uv, 1.0 );

		        	if ( face == 0.0 ) {

		        		direction = direction.zyx; // ( 1, v, u ) pos x

		        	} else if ( face == 1.0 ) {

		        		direction = direction.xzy;
		        		direction.xz *= -1.0; // ( -u, 1, -v ) pos y

		        	} else if ( face == 2.0 ) {

		        		direction.x *= -1.0; // ( -u, v, 1 ) pos z

		        	} else if ( face == 3.0 ) {

		        		direction = direction.zyx;
		        		direction.xz *= -1.0; // ( -1, v, -u ) neg x

		        	} else if ( face == 4.0 ) {

		        		direction = direction.xzy;
		        		direction.xy *= -1.0; // ( -u, -1, v ) neg y

		        	} else if ( face == 5.0 ) {

		        		direction.z *= -1.0; // ( u, v, -1 ) neg z

		        	}

		        	return direction;

		        }

		        void main() {

		        	vOutputDirection = getDirection( uv, faceIndex );
		        	gl_Position = vec4( position, 1.0 );

		        }
	            ";
        }

    }
}
