﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public static class TriangleExt
    {
        public static Triangle SetFromAttributeAndIndices(this Triangle tri, BufferAttribute attribute, int i0, int i1, int i2)
        {
            tri.A.FromBufferAttribute(attribute, i0);
            tri.B.FromBufferAttribute(attribute, i1);
            tri.C.FromBufferAttribute(attribute, i2);
            return tri;
        }
    }
}
