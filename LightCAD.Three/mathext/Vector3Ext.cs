﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public static class Vector3Ext
    {
        public static Vector3 FromBufferAttribute(this Vector3 vec, BufferAttribute attribute, int index)
        {
            vec.X = attribute.getX(index);
            vec.Y = attribute.getY(index);
            vec.Z = attribute.getZ(index);
            return vec;
        }
        public static Vector3 Project(this Vector3 vec, Camera camera)
        {
            return vec.ApplyMatrix4(camera.matrixWorldInverse).ApplyMatrix4(camera.projectionMatrix);
        }
        public static Vector3 Unproject(this Vector3 vec, Camera camera)
        {
            return vec.ApplyMatrix4(camera.projectionMatrixInverse).ApplyMatrix4(camera.matrixWorld);
        }
    }
}
