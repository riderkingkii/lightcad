﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public static class FrustumExt
    {
        public static bool IntersectsObject(this Frustum frustum, Object3D _object)
        {
            if (_object == null)
                return false;

            var ctx = Frustum.GetContext();

            if (_object is IGeometry)
            {
                var geometry = (_object as IGeometry).getGeometry();
                if (geometry.boundingSphere == null) geometry.computeBoundingSphere();
                ctx._sphere.Copy(geometry.boundingSphere).ApplyMatrix4(_object.matrixWorld);
            }
            else if (_object is IBounding)
            {
                var sphere = (Sphere)_object.GetField("boundingSphere");
                if (sphere == null)
                {
                    (_object as IBounding).computeBoundingSphere();
                    sphere = (Sphere)_object.GetField("boundingSphere");
                }
                ctx._sphere.Copy(sphere).ApplyMatrix4(_object.matrixWorld);
            }
            else
            {
                return false;
            }


            return frustum.IntersectsSphere(ctx._sphere);
        }
        public static bool IntersectsSprite(this Frustum frustum, Sprite sprite)
        {
            var _sphere = Frustum.GetContext()._sphere;
            _sphere.Center.Set(0, 0, 0);
            _sphere.Radius = 0.7071067811865476;
            _sphere.ApplyMatrix4(sprite.matrixWorld);
            return frustum.IntersectsSphere(_sphere);
        }
    }
}
