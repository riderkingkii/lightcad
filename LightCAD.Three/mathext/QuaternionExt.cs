﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public static class QuaternionExt
    {
        public static Quaternion FromBufferAttribute(this Quaternion quat, BufferAttribute attribute, int index)
        {
            quat.Set(
                attribute.getX(index),
                attribute.getY(index),
                attribute.getZ(index),
                attribute.getW(index)
            );
            return quat;
        }
    }
}
