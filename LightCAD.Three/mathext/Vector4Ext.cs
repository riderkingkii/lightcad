﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public static class Vector4Ext
    {
        public static Vector4 FromBufferAttribute(this Vector4 vec, BufferAttribute attribute, int index)
        {
            if (attribute.arrObj.GetValue(0) is double)
            {
                vec.X = attribute.getX(index);
                vec.Y = attribute.getY(index);
                vec.Z = attribute.getZ(index);
                vec.W = attribute.getW(index);
            }
            else if (attribute.arrObj.GetValue(0) is int)
            {
                vec.X = attribute.getIntX(index);
                vec.Y = attribute.getIntY(index);
                vec.Z = attribute.getIntZ(index);
                vec.W = attribute.getIntW(index);
            }
            else
                console.log("warning unknown data!!!!");

            return vec;
        }

    }
}
