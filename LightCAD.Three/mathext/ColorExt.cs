﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public static class ColorExt
    {
        public static  Color FromBufferAttribute(this Color color, BufferAttribute attribute, int index)
        {
            color.R = attribute.getX(index);
            color.G = attribute.getY(index);
            color.B = attribute.getZ(index);
            return color;
        }
    }
}
