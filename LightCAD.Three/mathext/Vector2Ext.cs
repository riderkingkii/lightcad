﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public static class Vector2Ext
    {
        public static Vector2 FromBufferAttribute(this Vector2 vec, BufferAttribute attribute, int index)
        {
            vec.X = attribute.getX(index);
            vec.Y = attribute.getY(index);
            return vec;
        }
    }
}
