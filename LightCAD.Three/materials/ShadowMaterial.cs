using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class ShadowMaterial : Material
    {
        #region Properties
        //public Color color ;


        #endregion

        #region constructor
        public ShadowMaterial()
        {
            this.type = "ShadowMaterial";
            this.color = new Color(0x000000);
            this.transparent = true;
            this.fog = true;
        }
        #endregion

        #region methods
        public ShadowMaterial copy(ShadowMaterial source)
        {
            base.copy(source);
            this.color.Copy(source.color);
            this.fog = source.fog;
            return this;
        }
        public override Material clone()
        {
            return new ShadowMaterial().copy(this);
        }
        #endregion

    }
}
