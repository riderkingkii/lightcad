using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{

    public class ShaderMaterial : Material
    {
        public const string default_vertex = "void main() {\r\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\r\n}";
        public const string default_fragment = "void main() {\r\n\tgl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );\r\n}";
        public class Extensions
        {
            public bool derivatives = false; // set to use derivatives

            public bool fragDepth = false; // set to use fragment depth values

            public bool drawBuffers = false; // set to use draw buffers

            public bool shaderTextureLOD = false;// set to use shader texture LOD
            public Extensions clone()
            {
                return new Extensions
                {
                    derivatives = this.derivatives,
                    fragDepth = this.fragDepth,
                    drawBuffers = this.drawBuffers,
                    shaderTextureLOD = this.shaderTextureLOD
                };
            }
        }

        #region Properties

        //public JsObj<object> defines;
        public Uniforms uniforms;
        //public JsArr<UniformsGroup> uniformsGroups;
        public string vertexShader;
        public string fragmentShader;
        public bool lights;
        public Extensions extensions;
        public JsObj<ListEx<double>> defaultAttributeValues;
        public string index0AttributeName;
        public bool uniformsNeedUpdate;
        public string glslVersion;
        //public virtual double linewidth { get; set; }
        #endregion
        //envMap和map放在基类，不支持这种属性访问
        //public Func<Texture> envMapFunc;
        // public Func<Texture> mapFunc;
        //public  new Texture envMap
        // {
        //     get
        //     {
        //         if (envMapFunc == null)
        //             return null;
        //         else
        //             return envMapFunc();

        //     }
        // }
        //public new Texture map
        //{
        //    get
        //    {
        //        if (mapFunc == null)
        //            return null;
        //        else
        //            return mapFunc();

        //    }
        //}

        #region constructor
        public ShaderMaterial()
        {
            this.type = "ShaderMaterial";
            this.defines = new JsObj<object> { };
            this.uniforms = new Uniforms();
            this.uniformsGroups = new ListEx<UniformsGroup>();
            this.vertexShader = default_vertex;
            this.fragmentShader = default_fragment;
            this.linewidth = 1;
            this.wireframe = false;
            this.wireframeLinewidth = 1;
            this.fog = false; // set to use scene fog
            this.lights = false; // set to use scene lights
            this.clipping = false; // set to use user-defined clipping planes
            this.forceSinglePass = true;
            this.extensions = new Extensions
            {
                derivatives = false, // set to use derivatives
                fragDepth = false, // set to use fragment depth values
                drawBuffers = false, // set to use draw buffers
                shaderTextureLOD = false // set to use shader texture LOD
            };
            // When rendered geometry doesn"t include these attributes but the material does,
            // use these default values in WebGL. This avoids errors when buffer data is missing.
            this.defaultAttributeValues = new JsObj<ListEx<double>> {
                    { "color",new ListEx<double> { 1, 1, 1 } },
                    { "uv", new ListEx<double>{ 0, 0 } },
                    { "uv2",new ListEx<double>{ 0, 0 }}
                };
            this.index0AttributeName = null;
            this.uniformsNeedUpdate = false;
            this.glslVersion = null;
            
        }
        #endregion

        #region methods
        public ShaderMaterial copy(ShaderMaterial source)
        {
            base.copy(source);
            this.fragmentShader = source.fragmentShader;
            this.vertexShader = source.vertexShader;
            this.uniforms = UniformsUtils.cloneUniforms(source.uniforms);
            this.uniformsGroups = UniformsUtils.cloneUniformsGroups(source.uniformsGroups);
            this.defines = source.defines.clone();
            this.wireframe = source.wireframe;
            this.wireframeLinewidth = source.wireframeLinewidth;
            this.fog = source.fog;
            this.lights = source.lights;
            this.clipping = source.clipping;
            this.extensions = source.extensions.clone();
            this.glslVersion = source.glslVersion;
            return this;
        }


        public override Material clone()
        {
            return new ShaderMaterial().copy(this);
        }
        #endregion
    }
}
