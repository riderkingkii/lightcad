using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class MaterialAccessor
    {
        public Material ctx;
        //public MeshBasicMaterial basicCtx;
        //public MeshPhongMaterial phongCtx;
        //public MeshLambertMaterial lambertCtx;
        //public MeshStandardMaterial standardCtx;
        //public MeshToonMaterial toonCtx;
        //public ShaderMaterial shaderCtx;
        //public MeshMatcapMaterial matcapCtx;
        //public MeshDepthMaterial depthCtx;
        //public MeshDistanceMaterial distanceCtx;
        //public PointsMaterial pointCtx;
        //public SpriteMaterial spriteCtx;
        //public bool isbasic;
        //public bool isphong;
        //public bool islambert;
        //public bool isstandard;
        //public bool istoon;
        //public bool isshader;
        //public bool ismatcap;
        //public bool isdepth;
        //public bool isdistance;
        //public bool ispoint;
        //public bool issprite;
        public MaterialAccessor(Material ctx)
        {
            this.ctx = ctx;
            //if (ctx is MeshBasicMaterial) isbasic=true;
            //if (ctx is MeshPhongMaterial) isphong=true;
            //if (ctx is MeshLambertMaterial) islambert = true;
            //if (ctx is MeshStandardMaterial) isstandard = true;
            //if (ctx is MeshToonMaterial)istoon=true;
            //if (ctx is ShaderMaterial) isshader=true;
            //if (ctx is MeshMatcapMaterial) ismatcap = true;
            //if (ctx is MeshDepthMaterial) isdepth=true;
            //if (ctx is MeshDistanceMaterial)isdistance=true;
            //if (ctx is PointsMaterial)ispoint=true;
            //if (ctx is SpriteMaterial) issprite=true;

            //if (ctx is MeshBasicMaterial) basicCtx = ctx as MeshBasicMaterial;
            //if (ctx is MeshPhongMaterial) phongCtx = ctx as MeshPhongMaterial;
            //if (ctx is MeshLambertMaterial) lambertCtx = ctx as MeshLambertMaterial;
            //if (ctx is MeshStandardMaterial) standardCtx = ctx as MeshStandardMaterial;
            //if (ctx is MeshToonMaterial) toonCtx = ctx as MeshToonMaterial;
            //if (ctx is ShaderMaterial) shaderCtx = ctx as ShaderMaterial;
            //if (ctx is MeshMatcapMaterial) matcapCtx = ctx as MeshMatcapMaterial;
            //if (ctx is MeshDepthMaterial) depthCtx = ctx as MeshDepthMaterial;
            //if (ctx is MeshDistanceMaterial) distanceCtx = ctx as MeshDistanceMaterial;
            //if (ctx is PointsMaterial) pointCtx = ctx as PointsMaterial;
            //if (ctx is SpriteMaterial) spriteCtx = ctx as SpriteMaterial;
        }
        public string index0AttributeName { get => (string)ctx["index0AttributeName"]; set { ctx["index0AttributeName"] = value; } }
        public double linewidth { get => ctx.hasValue("linewidth") ? (double)ctx["linewidth"] : 0; set { ctx["linewidth"] = value; } }
        public Vector2 resolution { get => ctx.hasValue("resolution") ? ctx["resolution"] as Vector2 : null; set { ctx["resolution"] = value; } }
        public Uniforms uniforms { get => (Uniforms)ctx["uniforms"]; set { ctx["uniforms"] = value; } }
        public ShaderMaterial.Extensions extensions { get => (ShaderMaterial.Extensions)ctx["extensions"]; set { ctx["extensions"] = value; } }
        public string glslVersion { get => (string)ctx["glslVersion"]; set { ctx["glslVersion"] = value; } }

        //#region Material通用属性
        //public string uuid { get => ctx.uuid; set { ctx.uuid = value; } }
        //public string name { get => ctx.name; set { ctx.name = value; } }
        //public string type { get => ctx.type; set { ctx.type = value; } }
        //public int blending { get => ctx.blending; set { ctx.blending = value; } }
        //public int side { get => ctx.side; set { ctx.side = value; } }
        //public bool vertexColors { get => ctx.vertexColors; set { ctx.vertexColors = value; } }
        //public double opacity { get => ctx.opacity; set { ctx.opacity = value; } }
        //public bool transparent { get => ctx.transparent; set { ctx.transparent = value; } }

        //public int blendSrc { get => ctx.blendSrc; set { ctx.blendSrc = value; } }
        //public int blendDst { get => ctx.blendDst; set { ctx.blendDst = value; } }
        //public int blendEquation { get => ctx.blendEquation; set { ctx.blendEquation = value; } }
        //public int blendSrcAlpha { get => ctx.blendSrcAlpha; set { ctx.blendSrcAlpha = value; } }
        //public int blendDstAlpha { get => ctx.blendDstAlpha; set { ctx.blendDstAlpha = value; } }
        //public int blendEquationAlpha { get => ctx.blendEquationAlpha; set { ctx.blendEquationAlpha = value; } }

        //public int depthFunc { get => ctx.blendSrc; set { ctx.blendSrc = value; } }
        //public bool depthTest { get => ctx.depthTest; set { ctx.depthTest = value; } }
        //public bool depthWrite { get => ctx.depthWrite; set { ctx.depthWrite = value; } }
        //public bool fog { get => ctx.fog; set { ctx.fog = value; } }
        //public bool wireframe { get => ctx.wireframe; set { ctx.wireframe = value; } }
        //public uint stencilWriteMask { get => ctx.stencilWriteMask; set { ctx.stencilWriteMask = value; } }
        //public int stencilFunc { get => ctx.stencilFunc; set { ctx.stencilFunc = value; } }
        //public int stencilRef { get => ctx.stencilRef; set { ctx.stencilRef = value; } }
        //public uint stencilFuncMask { get => ctx.stencilFuncMask; set { ctx.stencilFuncMask = value; } }
        //public int stencilFail { get => ctx.stencilFail; set { ctx.stencilFail = value; } }
        //public int stencilZFail { get => ctx.stencilZFail; set { ctx.stencilZFail = value; } }
        //public int stencilZPass { get => ctx.stencilZPass; set { ctx.stencilZPass = value; } }
        //public bool stencilWrite { get => ctx.stencilWrite; set { ctx.stencilWrite = value; } }

        //public bool clipping { get => ctx.clipping; set { ctx.clipping = value; } }
        //public JsArr<Plane> clippingPlanes { get => ctx.clippingPlanes; set { ctx.clippingPlanes = value; } }
        //public bool clipIntersection { get => ctx.clipIntersection; set { ctx.clipIntersection = value; } }
        //public bool clipShadows { get => ctx.clipShadows; set { ctx.clipShadows = value; } }
        //public int shadowSide { get => ctx.shadowSide; set { ctx.shadowSide = value; } }
        //public bool colorWrite { get => ctx.colorWrite; set { ctx.colorWrite = value; } }
        //public string precision { get => ctx.precision; set { ctx.precision = value; } }
        //public bool polygonOffset { get => ctx.polygonOffset; set { ctx.polygonOffset = value; } }
        //public double polygonOffsetFactor { get => ctx.polygonOffsetFactor; set { ctx.polygonOffsetFactor = (float)value; } }
        //public double polygonOffsetUnits { get => ctx.polygonOffsetUnits; set { ctx.polygonOffsetUnits = (float)value; } }
        //public bool dithering { get => ctx.dithering; set { ctx.dithering = value; } }
        //public bool alphaToCoverage { get => ctx.alphaToCoverage; set { ctx.alphaToCoverage = value; } }
        //public bool premultipliedAlpha { get => ctx.premultipliedAlpha; set { ctx.premultipliedAlpha = value; } }
        //public bool visible { get => ctx.visible; set { ctx.visible = value; } }
        //public bool toneMapped { get => ctx.toneMapped; set { ctx.toneMapped = value; } }
        //public JsObj<object> userData { get => ctx.userData; set { ctx.userData = value; } }
        //public int version { get => ctx.version; set { ctx.version = value; } }
        //public int id { get => ctx.id; }

        //public double alphaTest { get => ctx.alphaTest; set { ctx.alphaTest = value; } }
        //public bool needsUpdate { set { ctx.needsUpdate = value; } }

        //#endregion

        //public bool flatShading { get => ctx.hasValue("flatShading") ? (bool)ctx["flatShading"] : false; set { ctx["flatShading"] = value; } }


        ////public double[] resolution { get => (double[])ctx["resolution"]; set { ctx["resolution"] = value; } }
        //public double[] clippingState { get => (double[])ctx["clippingState"]; set { ctx["clippingState"] = value; } }
        //public bool needsLights { get => ctx.hasValue("needsLights") ? (bool)ctx["needsLights"] : false; set { ctx["needsLights"] = value; } }
        //public bool vertexTangents { get => ctx.hasValue("vertexTangents") ? (bool)ctx["vertexTangents"] : false; set { ctx["vertexTangents"] = value; } }
        //public bool morphTargets { get => ctx.hasValue("morphTargets") ? (bool)ctx["morphTargets"] : false; set { ctx["morphTargets"] = value; } }
        //public bool morphNormals { get => ctx.hasValue("morphNormals") ? (bool)ctx["morphNormals"] : false; set { ctx["morphNormals"] = value; } }
        //public bool morphColors { get => ctx.hasValue("morphColors") ? (bool)ctx["morphColors"] : false; set { ctx["morphColors"] = value; } }
        //public int toneMapping { get => ctx.hasValue("toneMapping") ? (int)ctx["toneMapping"] : 0; set { ctx["toneMapping"] = value; } }
        //public int morphTargetsCount { get => ctx.hasValue("morphTargetsCount") ? (int)ctx["morphTargetsCount"] : 0; set { ctx["morphTargetsCount"] = value; } }

        //public bool vertexAlphas { get => ctx.hasValue("vertexAlphas") ? (bool)ctx["vertexAlphas"] : false; set { ctx["vertexAlphas"] = value; } }
        //public int __version { get => ctx.hasValue("__version") ? (int)ctx["__version"] : 0; set { ctx["__version"] = value; } }
        //public JsArr<IUniform> uniformsList { get => ctx.hasValue("uniformsList") ? ctx["uniformsList"] as JsArr<IUniform> : null; set { ctx["uniformsList"] = value; } }


        //public Color color { get => (Color)ctx["color"]; set { ctx["color"] = value; } }
        //public int depthPacking { get => ctx.hasValue("depthPacking") ? (int)ctx["depthPacking"]:0; set { ctx["depthPacking"] = value; } }

        //public JsObj<object> defines { get => (JsObj<object>)ctx["defines"]; set { ctx["defines"] = value; } }
        //public string vertexShader { get => (string)ctx["vertexShader"]; set { ctx["vertexShader"] = value; } }
        //public string fragmentShader { get => (string)ctx["fragmentShader"]; set { ctx["fragmentShader"] = value; } }
        ////public Texture map { get => phongCtx.map; set { phongCtx.map = value; } }
        ///// <summary>
        ///// 渲染循环需要使用
        ///// </summary>
        //public Texture map
        //{
        //    get
        //    {
        //        if (ctx is MeshBasicMaterial) return (ctx as MeshBasicMaterial).map;
        //        if (ctx is MeshPhongMaterial) return (ctx as MeshPhongMaterial).map;
        //        if (ctx is MeshLambertMaterial) return (ctx as MeshLambertMaterial).map;
        //        if (ctx is MeshStandardMaterial) return (ctx as MeshStandardMaterial).map;
        //        if (ctx is MeshToonMaterial) return (ctx as MeshToonMaterial).map;
        //        if (ctx is ShaderMaterial) return (ctx as ShaderMaterial).map;
        //        if (ctx is MeshMatcapMaterial) return (ctx as MeshMatcapMaterial).map;
        //        if (ctx is MeshDepthMaterial) return (ctx as MeshDepthMaterial).map;
        //        if (ctx is MeshDistanceMaterial) return (ctx as MeshDistanceMaterial).map;
        //        if (ctx is PointsMaterial) return (ctx as PointsMaterial).map;
        //        if (ctx is SpriteMaterial) return (ctx as SpriteMaterial).map;
        //        return null;
        //    }
        //    set
        //    {
        //        if (ctx is MeshBasicMaterial)  (ctx as MeshBasicMaterial).map=value;
        //        if (ctx is MeshPhongMaterial)  (ctx as MeshPhongMaterial).map = value;
        //        if (ctx is MeshToonMaterial)  (ctx as MeshToonMaterial).map = value;
        //        if (ctx is MeshLambertMaterial)  (ctx as MeshLambertMaterial).map = value;
        //        if (ctx is MeshStandardMaterial)  (ctx as MeshStandardMaterial).map = value;
        //        if (ctx is MeshMatcapMaterial)  (ctx as MeshMatcapMaterial).map = value;
        //        if (ctx is MeshDepthMaterial)  (ctx as MeshDepthMaterial).map = value;
        //        if (ctx is MeshDistanceMaterial)  (ctx as MeshDistanceMaterial).map = value;
        //        if (ctx is PointsMaterial)  (ctx as PointsMaterial).map = value;
        //        if (ctx is SpriteMaterial)  (ctx as SpriteMaterial).map = value;
        //        console.log("Material setValue not exist . (map)");
        //    }
        //}

        //public Color emissive { get => (Color)ctx["emissive"]; set { ctx["emissive"] = value; } }

        //public double? emissiveIntensity { get => ctx.hasValue("emissiveIntensity") ? (double?)ctx ["emissiveIntensity"]:null; set { ctx["emissiveIntensity"] = value; } }

        //public Texture emissiveMap { get => (Texture)ctx["emissiveMap"]; set { ctx["emissiveMap"] = value; } }

        //public Texture alphaMap { get => (Texture)ctx["alphaMap"]; set { ctx["alphaMap"] = value; } }
        //public Texture bumpMap { get => (Texture)ctx["bumpMap"]; set { ctx["bumpMap"] = value; } }
        //public double bumpScale { get => (double)ctx["bumpScale"]; set { ctx["bumpScale"] = value; } }

        //public Texture displacementMap { get => (Texture)ctx["displacementMap"]; set { ctx["displacementMap"] = value; } }
        //public double displacementScale { get => ctx.hasValue("displacementScale") ? (double)ctx["displacementScale"]:0; set { ctx["displacementScale"] = value; } }
        //public double displacementBias { get => ctx.hasValue("displacementBias") ? (double)ctx["displacementBias"]:0; set { ctx["displacementBias"] = value; } }

        ///// <summary>
        ///// 渲染循环需要使用
        ///// </summary>
        //public Texture normalMap {
        //    get
        //    {
        //        if (ctx is MeshPhongMaterial) return (ctx as MeshPhongMaterial).normalMap;
        //        if (ctx is MeshLambertMaterial) return (ctx as MeshLambertMaterial).normalMap;
        //        if (ctx is MeshStandardMaterial) return (ctx as MeshStandardMaterial).normalMap;
        //        if (ctx is MeshToonMaterial) return (ctx as MeshToonMaterial).normalMap;
        //        if (ctx is MeshMatcapMaterial) return (ctx as MeshMatcapMaterial).normalMap;

        //        return null;
        //    }
        //    set
        //    {
        //        if (ctx is MeshPhongMaterial) (ctx as MeshPhongMaterial).normalMap = value;
        //        if (ctx is MeshToonMaterial) (ctx as MeshToonMaterial).normalMap = value;
        //        if (ctx is MeshLambertMaterial) (ctx as MeshLambertMaterial).normalMap = value;
        //        if (ctx is MeshStandardMaterial) (ctx as MeshStandardMaterial).normalMap = value;
        //        if (ctx is MeshMatcapMaterial) (ctx as MeshMatcapMaterial).normalMap = value;

        //        console.log("Material setValue not exist . (normalMap)");
        //    }
        //}
        //public Vector2 normalScale { get => (Vector2)ctx["normalScale"]; set { ctx["normalScale"] = value; } }


        //public Texture specularMap { get => (Texture)ctx["specularMap"]; set { ctx["specularMap"] = value; } }

        //public Texture metalnessMap { get => (Texture)ctx["metalnessMap"]; set { ctx["metalnessMap"] = value; } }
        //public Texture roughnessMap { get => (Texture)ctx["roughnessMap"]; set { ctx["roughnessMap"] = value; } }

        //public Texture clearcoatMap { get => (Texture)ctx["clearcoatMap"]; set { ctx["clearcoatMap"] = value; } }
        //public Texture clearcoatNormalMap { get => (Texture)ctx["clearcoatNormalMap"]; set { ctx["clearcoatNormalMap"] = value; } }
        //public Texture clearcoatRoughnessMap { get => (Texture)ctx["clearcoatRoughnessMap"]; set { ctx["clearcoatRoughnessMap"] = value; } }

        //public Texture iridescenceMap { get => (Texture)ctx["iridescenceMap"]; set { ctx["iridescenceMap"] = value; } }
        //public Texture iridescenceThicknessMap { get => (Texture)ctx["iridescenceThicknessMap"]; set { ctx["iridescenceThicknessMap"] = value; } }

        //public Texture specularIntensityMap { get => (Texture)ctx["specularIntensityMap"]; set { ctx["specularIntensityMap"] = value; } }
        //public Texture specularColorMap { get => (Texture)ctx["specularColorMap"]; set { ctx["specularColorMap"] = value; } }
        //public Texture transmissionMap { get => (Texture)ctx["transmissionMap"]; set { ctx["transmissionMap"] = value; } }
        //public Texture thicknessMap { get => (Texture)ctx["thicknessMap"]; set { ctx["thicknessMap"] = value; } }

        //public Texture sheenColorMap { get => (Texture)ctx["sheenColorMap"]; set { ctx["sheenColorMap"] = value; } }
        //public Texture sheenRoughnessMap { get => (Texture)ctx["sheenRoughnessMap"]; set { ctx["sheenRoughnessMap"] = value; } }
        //public Texture aoMap { get => (Texture)ctx["aoMap"]; set { ctx["aoMap"] = value; } }
        //public double? aoMapIntensity { get => ctx.hasValue("aoMapIntensity") ?(double?) ctx ["aoMapIntensity"]:null; set { ctx["aoMapIntensity"] = value; } }

        //public Texture lightMap { get => (Texture)ctx["lightMap"]; set { ctx["lightMap"] = value; } }
        ///// <summary>
        ///// 渲染循环需要使用
        ///// </summary>
        //public Texture envMap {
        //    get
        //    {
        //        if (ctx is MeshBasicMaterial) return (ctx as MeshBasicMaterial).envMap;
        //        if (ctx is MeshPhongMaterial) return (ctx as MeshPhongMaterial).envMap;
        //        if (ctx is MeshLambertMaterial) return (ctx as MeshLambertMaterial).envMap;
        //        if (ctx is MeshStandardMaterial) return (ctx as MeshStandardMaterial).envMap;
        //        if (ctx is ShaderMaterial) return (ctx as ShaderMaterial).envMap;
        //        return null;
        //    }
        //    set
        //    {
        //        if (ctx is MeshBasicMaterial) (ctx as MeshBasicMaterial).envMap = value;
        //        if (ctx is MeshPhongMaterial) (ctx as MeshPhongMaterial).envMap = value;
        //        if (ctx is MeshLambertMaterial) (ctx as MeshLambertMaterial).envMap = value;
        //        if (ctx is MeshStandardMaterial) (ctx as MeshStandardMaterial).envMap = value;
        //        console.log("Material setValue not exist . (envMap)");
        //    }
        //}

        //public double clearcoat { get => ctx.hasValue("clearcoat") ? (double)ctx["clearcoat"]:0; set { ctx["clearcoat"] = value; } }
        //public double iridescence { get => ctx.hasValue("iridescence") ? (double)ctx["iridescence"]:0; set { ctx["iridescence"] = value; } }
        //public Texture matcap { get => (Texture)ctx["matcap"]; set { ctx["matcap"] = value; } }
        //public int normalMapType { get => ctx.hasValue("normalMapType") ? (int)ctx["normalMapType"]:0; set { ctx["normalMapType"] = value; } }
        //public Texture gradientMap { get => (Texture)ctx["gradientMap"]; set { ctx["gradientMap"] = value; } }
        //public double sheen { get => ctx.hasValue("sheen") ?(double)ctx["sheen"]:0; set { ctx["sheen"] = value; } }
        //public double transmission { get => ctx.hasValue("transmission") ? (double)ctx["transmission"]:0; set { ctx["transmission"] = value; } }
        //public int combine { get => ctx.hasValue("combine") ? (int)ctx["combine"]:0; set { ctx["combine"] = value; } }
        //public bool sizeAttenuation { get => ctx.hasValue("sizeAttenuation") ? (bool)ctx["sizeAttenuation"]:false; set { ctx["sizeAttenuation"] = value; } }
        //public double? reflectivity { get => ctx.hasValue("reflectivity") ? (double?)ctx["reflectivity"] :null; set { ctx["reflectivity"] = value; } }
        //public double? ior { get => ctx.hasValue("ior") ? (double?)ctx["ior"] : null; set { ctx["ior"] = value; } }
        //public double? refractionRatio { get => ctx.hasValue("refractionRatio") ? (double?)ctx["refractionRatio"] : null; set { ctx["refractionRatio"] = value; } }
        //public double? lightMapIntensity { get => ctx.hasValue("lightMapIntensity") ? (double?)ctx["lightMapIntensity"] : null; set { ctx["lightMapIntensity"] = value; } }

    }
}
