using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{

    public class RawShaderMaterial : ShaderMaterial
    {
        #region Properties
        #endregion

        #region constructor
        public RawShaderMaterial() 
        {
            this.type = "RawShaderMaterial";
        }
        public override Material clone()
        {
            return new RawShaderMaterial().copy(this);
        }
        #endregion
    }
}
