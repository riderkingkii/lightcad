using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;
using LightCAD.MathLib;
namespace LightCAD.Three
{
    public class MeshToonMaterial : Material
    {
        #region Properties

        //public Color color ;
        //public Texture map ;
        //public JsArr<Texture> maps ;
        //public JsObj<string> defines;
        //public Texture gradientMap;
        //public Texture lightMap ;
        //public double lightMapIntensity;
        //public Texture aoMap;
        //public double aoMapIntensity;
        //public Texture emissiveMap ;
        //public double emissiveIntensity ;
        //public Color emissive ;
        //public int normalMapType;
        //public Texture normalMap ;
        //public Vector2 normalScale;
        //public string wireframeLinecap;
        //public string wireframeLinejoin;

        //public Texture displacementMap ;
        //public double displacementScale ;
        //public double displacementBias ;
        //public Texture bumpMap ;
        //public double bumpScale ;
        //public Texture alphaMap ;
        #endregion

        #region constructor
        public MeshToonMaterial() 
        {
            this.defines = new JsObj<object> { { "TOON", "" } };
            this.type = "MeshToonMaterial";
            this.color = new Color(0xffffff);
            this.map = null;
            this.gradientMap = null;
            this.lightMap = null;
            this.lightMapIntensity = 1.0;
            this.aoMap = null;
            this.aoMapIntensity = 1.0;
            this.emissive = new Color(0x000000);
            this.emissiveIntensity = 1.0;
            this.emissiveMap = null;
            this.bumpMap = null;
            this.bumpScale = 1;
            this.normalMap = null;
            this.normalMapType = TangentSpaceNormalMap;
            this.normalScale = new Vector2(1, 1);
            this.displacementMap = null;
            this.displacementScale = 1;
            this.displacementBias = 0;
            this.alphaMap = null;
            this.wireframe = false;
            this.wireframeLinewidth = 1;
            this.wireframeLinecap = "round";
            this.wireframeLinejoin = "round";
            this.fog = true;
        }
        #endregion

        #region methods
        public MeshToonMaterial copy(MeshToonMaterial source)
        {
            base.copy(source);
            this.color.Copy(source.color);
            this.map = source.map;
            this.gradientMap = source.gradientMap;
            this.lightMap = source.lightMap;
            this.lightMapIntensity = source.lightMapIntensity;
            this.aoMap = source.aoMap;
            this.aoMapIntensity = source.aoMapIntensity;
            this.emissive.Copy(source.emissive);
            this.emissiveMap = source.emissiveMap;
            this.emissiveIntensity = source.emissiveIntensity;
            this.bumpMap = source.bumpMap;
            this.bumpScale = source.bumpScale;
            this.normalMap = source.normalMap;
            this.normalMapType = source.normalMapType;
            this.normalScale.Copy(source.normalScale);
            this.displacementMap = source.displacementMap;
            this.displacementScale = source.displacementScale;
            this.displacementBias = source.displacementBias;
            this.alphaMap = source.alphaMap;
            this.wireframe = source.wireframe;
            this.wireframeLinewidth = source.wireframeLinewidth;
            this.wireframeLinecap = source.wireframeLinecap;
            this.wireframeLinejoin = source.wireframeLinejoin;
            this.fog = source.fog;
            return this;
        }
        public override Material clone()
        {
            return new MeshToonMaterial().copy(this);
        }
        #endregion

    }
}
