using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class LineBasicMaterial : Material
    {
        #region Properties
        //public Color color ;
        //public string linecap ;
        //public string linejoin ;
        //public double linewidth ;

        #endregion

        #region constructor
        public LineBasicMaterial()
        {
            this.defines = new JsObj<object>{
                    {"LineBasic", "" }
                };
            this.type = "LineBasicMaterial";
            this.color = new Color(0xffffff);
            this.map = null;
            this.linewidth = 1;
            this.linecap = "round";
            this.linejoin = "round";
            this.fog = true;
        }
        #endregion

        #region methods
        public LineBasicMaterial copy(LineBasicMaterial source)
        {
            base.copy(source);
            this.color.Copy(source.color);
            this.map = source.map;
            this.linewidth = source.linewidth;
            this.linecap = source.linecap;
            this.linejoin = source.linejoin;
            this.fog = source.fog;
            return this;
        }

        public double getLineWidth()
        {
            return this.linewidth;
        }

        public void setLineWidth(double lineWidth)
        {
            this.linewidth = lineWidth;
        }
        public override Material clone()
        {
            return new LineBasicMaterial().copy(this);
        }
        #endregion
    }
}
