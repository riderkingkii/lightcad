using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class MeshBasicMaterial : Material
    {
        //#region Properties

        //public Color color ;
        //public Texture map ;
        //public JsArr<Texture> maps ;
        //public Texture alphaMap ;
        //public Texture aoMap ;
        //public Texture envMap ;
        //public Texture lightMap ;
        //public int combine ;
        //public Texture specularMap ;



        //public double lightMapIntensity ;
        //public double aoMapIntensity ;
        //public double reflectivity ;
        //public double refractionRatio ;
        //public string wireframeLinecap ;
        //public string wireframeLinejoin ;

        //#endregion

        #region constructor
        public MeshBasicMaterial() : base()
        {
            this.defines = new JsObj<object>{
                    {"MeshBasic", "" }
                };
            this.type = "MeshBasicMaterial";
            this.color = new Color(0xffffff); // emissive
            this.map = null;
            this.lightMap = null;
            this.lightMapIntensity = 1.0;
            this.aoMap = null;
            this.aoMapIntensity = 1.0;
            this.specularMap = null;
            this.alphaMap = null;
            this.envMap = null;
            this.combine = MultiplyOperation;
            this.reflectivity = 1;
            this.refractionRatio = 0.98;
            this.wireframe = false;
            this.wireframeLinewidth = 1;
            this.wireframeLinecap = "round";
            this.wireframeLinejoin = "round";
            this.fog = true;
        }
        #endregion

        #region methods
        public MeshBasicMaterial copy(MeshBasicMaterial source)
        {
            base.copy(source);
            this.color.Copy(source.color);
            this.map = source.map;
            this.lightMap = source.lightMap;
            this.lightMapIntensity = source.lightMapIntensity;
            this.aoMap = source.aoMap;
            this.aoMapIntensity = source.aoMapIntensity;
            this.specularMap = source.specularMap;
            this.alphaMap = source.alphaMap;
            this.envMap = source.envMap;
            this.combine = source.combine;
            this.reflectivity = source.reflectivity;
            this.refractionRatio = source.refractionRatio;
            this.wireframe = source.wireframe;
            this.wireframeLinewidth = source.wireframeLinewidth;
            this.wireframeLinecap = source.wireframeLinecap;
            this.wireframeLinejoin = source.wireframeLinejoin;
            this.fog = source.fog;
            return this;
        }
        public override Material clone()
        {
            return new MeshBasicMaterial().copy(this);
        }
        #endregion
    }
}
