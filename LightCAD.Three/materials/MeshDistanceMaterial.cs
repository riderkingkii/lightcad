using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class MeshDistanceMaterial : Material
    {
        #region Properties
        //public Texture map ;
        //public Texture alphaMap ;
        //public Texture displacementMap ;
        //public double? displacementScale ;
        //public double? displacementBias ;

        //public Vector3 referencePosition ;
        //public double nearDistance ;
        //public double farDistance ;

        #endregion

        #region constructor

        public MeshDistanceMaterial() 
        {
            this.type = "MeshDistanceMaterial";
            this.map = null;
            this.alphaMap = null;
            this.displacementMap = null;
            this.displacementScale = 1;
            this.displacementBias = 0;
        }

        #endregion

        #region methods
        public MeshDistanceMaterial copy(MeshDistanceMaterial source)
        {
            base.copy(source);
            this.map = source.map;
            this.alphaMap = source.alphaMap;
            this.displacementMap = source.displacementMap;
            this.displacementScale = source.displacementScale;
            this.displacementBias = source.displacementBias;
            return this;
        }
        public override Material clone()
        {
            return new MeshDistanceMaterial().copy(this);
        }
        #endregion

    }
}
