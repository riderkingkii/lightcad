using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{

    public abstract class Material : EventDispatcher, IDispose
    {

        #region scope properties or methods
        //private static int materialId = 0;
        #endregion

        #region Properties
        public bool clipping = false;
        public bool fog = false;
        public string uuid;
        public string name;
        public string type;
        public int blending;
        public int side;
        public bool vertexColors;

        public bool transparent;

        public int blendSrc;
        public int blendDst;
        public int blendEquation;
        public int blendSrcAlpha;
        public int blendDstAlpha;
        public int blendEquationAlpha;

        public int depthFunc;
        public bool depthTest;
        public bool depthWrite;

        public uint stencilWriteMask;
        public int stencilFunc;
        public int stencilRef;
        public uint stencilFuncMask;
        public int stencilFail;
        public int stencilZFail;
        public int stencilZPass;
        public bool stencilWrite;

        public ListEx<Plane> clippingPlanes;
        public bool clipIntersection;
        public bool clipShadows;
        public int shadowSide;
        public bool colorWrite;
        public string precision;
        public bool polygonOffset;
        public float polygonOffsetFactor;
        public float polygonOffsetUnits;
        public bool dithering;

        public bool premultipliedAlpha;
        public bool forceSinglePass;

        public bool visible;
        public bool toneMapped;
        public JsObj<object> userData;
        public int version;

        private double _alphaTest;
        public bool wireframe;
        public double wireframeLinewidth;
        public readonly int id;

        public readonly MaterialAccessor args;
        #endregion

        #region Basic

        public Color color;
        public Texture map;
        public ListEx<Texture> maps;
        public Texture alphaMap;
        public Texture aoMap;
        public Texture envMap;
        public Texture lightMap;
        public int? combine = -1;
        public Texture specularMap;


        public double lightMapIntensity;
        public double aoMapIntensity;
        public double reflectivity;
        public double refractionRatio;
        public string wireframeLinecap;
        public string wireframeLinejoin;

        #endregion

        #region lambert

        //public Texture lightMap;
        //public double lightMapIntensity;
        //public Texture alphaMap;

        //public Texture aoMap;
        //public double aoMapIntensity;

        public Texture emissiveMap;
        public double emissiveIntensity;
        public Color emissive;

        public Texture normalMap;
        public int normalMapType;
        public Vector2 normalScale;

        //public double reflectivity;
        //public double refractionRatio;
        //public string wireframeLinecap;
        //public string wireframeLinejoin;
        public bool flatShading;

        #endregion

        #region Phong

        //public Color color;
        //public Texture map;
        //public JsArr<Texture> maps;
        public Color specular;
        public double shininess;
        //public Texture lightMap;
        //public double lightMapIntensity;
        //public Texture aoMap;
        //public double aoMapIntensity;
        //public Texture emissiveMap;
        //public double emissiveIntensity;
        //public Color emissive;
        //public int normalMapType;
        //public Texture normalMap;
        //public Vector2 normalScale;
        //public double reflectivity;
        //public double refractionRatio;
        //public string wireframeLinecap;
        //public string wireframeLinejoin;
        //public bool flatShading;
        public Texture bumpMap;
        public double bumpScale;
        public Texture displacementMap;
        public double displacementScale;
        public double displacementBias;

        //public Texture specularMap;
        //public Texture alphaMap;
        //public Texture envMap;
        //public int combine;

        #endregion

        #region Standard

        public JsObj<object> defines;
        public double roughness;
        public double metalness;
        //public Texture lightMap;
        //public double lightMapIntensity;
        //public Texture aoMap;
        //public double aoMapIntensity;
        //public Texture emissiveMap;
        //public double emissiveIntensity;
        //public Color emissive;
        //public Texture normalMap;
        //public int normalMapType;
        //public Vector2 normalScale;
        public double envMapIntensity;
        //public string wireframeLinecap;
        //public string wireframeLinejoin;
        //public bool flatShading;

        //public Color color;
        //public Texture map;
        //public JsArr<Texture> maps;
        //public Texture bumpMap;
        //public double bumpScale;
        //public Texture displacementMap;
        //public double displacementScale;
        //public double displacementBias;
        public Texture roughnessMap;
        public Texture metalnessMap;
        //public Texture alphaMap;
        //public Texture envMap;
        #endregion

        #region Physic

        public Texture clearcoatMap;
        public Texture clearcoatNormalMap;
        public Texture clearcoatRoughnessMap;
        public double clearcoatRoughness;
        public Vector2 clearcoatNormalScale;
        public ListEx<double> iridescenceThicknessRange;
        public Color sheenColor;
        public Texture sheenColorMap;
        public Texture sheenRoughnessMap;
        public double sheenRoughness;
        public double thickness;
        public double attenuationDistance;
        public Color attenuationColor;
        public double specularIntensity;
        public Color specularColor;

        public double sheen;
        public double clearcoat;
        public double iridescence;
        public double transmission;

        public double ior;
        public double iridescenceIOR;
        public Texture iridescenceMap;
        public Texture iridescenceThicknessMap;

        public Texture transmissionMap;
        public Texture thicknessMap;
        public Texture specularIntensityMap;
        public Texture specularColorMap;
        #endregion

        #region Toon

        //public Color color;
        //public Texture map;
        //public JsArr<Texture> maps;
        //public JsObj<string> defines;
        public Texture gradientMap;
        //public Texture lightMap;
        //public double lightMapIntensity;
        //public Texture aoMap;
        //public double aoMapIntensity;
        //public Texture emissiveMap;
        //public double emissiveIntensity;
        //public Color emissive;
        //public int normalMapType;
        //public Texture normalMap;
        //public Vector2 normalScale;
        //public string wireframeLinecap;
        //public string wireframeLinejoin;

        //public Texture displacementMap;
        //public double displacementScale;
        //public double displacementBias;
        //public Texture bumpMap;
        //public double bumpScale;
        //public Texture alphaMap;
        #endregion

        #region Points

        //public Color color;
        //public Texture map;
        //public JsArr<Texture> maps;
        //public Texture alphaMap;
        public double size;
        public bool sizeAttenuation;

        #endregion

        #region Sprite

        //public Color color ;
        //public Texture map ;
        //public JsArr<Texture> maps ;
        //public Texture alphaMap ;
        public double rotation;
        //public bool sizeAttenuation;

        #endregion

        #region Depth
        //public Texture map;
        //public Texture alphaMap;
        //public Texture displacementMap;
        //public double? displacementScale;
        //public double? displacementBias;

        public int depthPacking;
        #endregion

        #region Distance
        //public Texture map;
        //public Texture alphaMap;
        //public Texture displacementMap;
        //public double? displacementScale;
        //public double? displacementBias;
        //r151删除了这三个字段
        //public Vector3 referencePosition;
        //public double nearDistance;
        //public double farDistance;

        #endregion

        #region Normal

        //public int normalMapType;
        //public Vector2 normalScale;
        //public Texture normalMap;

        //public bool flatShading;
        //public Texture bumpMap;
        //public double bumpScale;
        //public Texture displacementMap;
        //public double displacementScale;
        //public double displacementBias;
        public Color clearColor;
        public float? clearAlpha;
        #endregion

        #region Matcap

        //public JsObj<object> defines;
        public string matcap;
        //public Texture normalMap;
        //public int normalMapType;
        //public Vector2 normalScale;
        //public bool flatShading;

        //public Color color;
        //public Texture map;
        //public Texture bumpMap;
        //public double bumpScale;
        //public Texture alphaMap;
        //public Texture displacementMap;
        //public double displacementScale;
        //public double displacementBias;

        #endregion

        #region Line
        //public Color color;
        public string linecap;
        public string linejoin;

        public double linewidth;

        public double scale;
        public double dashSize;
        public double gapSize;
        #endregion

        #region UBOs
        public ListEx<UniformsGroup> uniformsGroups;
        #endregion

        #region constructor
        private static ConcurrentDictionary<int, int> objIndex = new ConcurrentDictionary<int, int>();
        public Material() : base()
        {
            this.id = ThreeThreadContext.NewId(objIndex);
            this.uuid = MathEx.GenerateUUID();
            this.name = "";
            this.type = "Material";
            this.blending = NormalBlending;
            this.side = FrontSide;
            this.vertexColors = false;
            this.opacity = 1;
            this.transparent = false;
            this.blendSrc = SrcAlphaFactor;
            this.blendDst = OneMinusSrcAlphaFactor;
            this.blendEquation = AddEquation;
            this.blendSrcAlpha = 0;
            this.blendDstAlpha = 0;
            this.blendEquationAlpha = 0;
            this.depthFunc = LessEqualDepth;
            this.depthTest = true;
            this.depthWrite = true;
            this.stencilWriteMask = 0xff;
            this.stencilFunc = AlwaysStencilFunc;
            this.stencilRef = 0;
            this.stencilFuncMask = 0xff;
            this.stencilFail = KeepStencilOp;
            this.stencilZFail = KeepStencilOp;
            this.stencilZPass = KeepStencilOp;
            this.stencilWrite = false;
            this.clippingPlanes = null;
            this.clipIntersection = false;
            this.clipShadows = false;
            this.shadowSide = 0;
            this.colorWrite = true;
            this.precision = null; // override the renderer"s default precision for this material
            this.polygonOffset = false;
            this.polygonOffsetFactor = 0;
            this.polygonOffsetUnits = 0;
            this.dithering = false;
            this.alphaToCoverage = false;
            this.premultipliedAlpha = false;
            this.forceSinglePass = false;
            this.visible = true;
            this.toneMapped = true;
            this.userData = new JsObj<object>();
            this.version = 0;
            this._alphaTest = 0;
            this.args = new MaterialAccessor(this);
        }
        #endregion

        #region properties
        public virtual bool alphaToCoverage { get; set; }
        public virtual double opacity { get; set; }
        public double alphaTest
        {
            get
            {
                return this._alphaTest;
            }
            set
            {
                if (this._alphaTest > 0 != value > 0)
                {
                    this.version++;
                }
                this._alphaTest = value;
            }
        }
        public bool needsUpdate
        {
            set
            {
                if (value) this.version++;
            }
        }
        #endregion

        #region methods
        internal object this[string name]
        {
            get { return this.getValue(name); }
            set { this.setValue(name, value); }
        }
        public bool hasValue(string name)
        {
            var hasField = this.HasField(name);
            var hasProp = this.HasProperty(name);
            if (!hasField && !hasProp) return false;
            if (hasField)
                return this.GetField(name) != null;
            else
                return this.GetProperty(name) != null;
        }
        public object getValue(string name)
        {
            if (this.HasField(name))
                return this.GetField(name);
            else
                return this.GetProperty(name);

        }
        public void setValue(string name, object value)
        {
            if (this.HasField(name))
                this.SetField(name, value);
            else if (this.HasProperty(name))
                this.SetProperty(name, value);
            else
                console.log($"Material.setValue not exist {name}.");
        }
        public Action<Object3D, object, object> onBuildAction;
        public Action<WebGLRenderer, Object3D, Camera, BufferGeometry, Object3D, GeometryGroup> onBeforeRenderAction;
        public Action<object, object> onBeforeCompileAction;
        public void onBuild(Object3D _object, object parameters, object renderer)
        {
            onBuildAction?.Invoke(_object, parameters, renderer);
        }
        public void onBeforeRender(WebGLRenderer render, Object3D scene, Camera camera, BufferGeometry geometry, Object3D _object, GeometryGroup group)
        {
            onBeforeRenderAction?.Invoke(render, scene, camera, geometry, _object, group);
        }
        public void onBeforeCompile(object parameters, object renderer)
        {
            onBeforeCompileAction?.Invoke(parameters, renderer);
        }
        public string customProgramCacheKey()
        {
            if (onBeforeCompileAction == null)
                return String.Empty;
            else
                return onBeforeCompileAction.GetHashCode().ToString();
        }
        //private static Dictionary<string, FieldInfo> MatValFields = typeof(MaterialValues).GetFields(BindingFlags.Public | BindingFlags.Instance).ToDictionary(f => f.Name, f => f);
        //public virtual void setValues(MaterialValues values = null)
        //{
        //    if (values == null) return;
        //    foreach (var item in MatValFields)
        //    {
        //        var key = item.Key;
        //        var newValue = item.Value.GetValue(values);
        //        if (newValue == null)
        //        {
        //            //console.warn("THREE.Material: \"" + key + "\" parameter is undefined.");
        //            continue;
        //        }
        //        //bool isProperty = key == "needsUpdate" || key == "alphaTest";
        //        bool isProperty = false;
        //        bool isValidKey = false;
        //        object currentValue = null;
        //        if (this.HasField(key))
        //        {
        //            currentValue = this.GetField(key);
        //            isValidKey = true;
        //        }
        //        else if (this.HasProperty(key))
        //        {
        //            currentValue = this.GetProperty(key);
        //            isProperty = true;
        //            isValidKey = true;
        //        }
        //        if (!isValidKey)
        //        {
        //            console.warn($"THREE.Material: '{key}' is not a property of THREE.{this.type}.");
        //            continue;
        //        }
        //        if (currentValue != null && currentValue is Color)
        //        {
        //            (currentValue as Color).set(newValue);
        //        }
        //        else if ((currentValue != null && currentValue is Vector3) && (newValue != null && newValue is Vector3))
        //        {
        //            (currentValue as Vector3).copy(newValue as Vector3);
        //        }
        //        else
        //        {
        //            if (isProperty)
        //                this.SetProperty(key, newValue);
        //            else
        //                this.SetField(key, newValue);
        //        }
        //    }
        //}

        public abstract Material clone();
        public virtual Material copy(Material source)
        {
            this.name = source.name;
            this.blending = source.blending;
            this.side = source.side;
            this.vertexColors = source.vertexColors;
            this.opacity = source.opacity;
            this.transparent = source.transparent;
            this.blendSrc = source.blendSrc;
            this.blendDst = source.blendDst;
            this.blendEquation = source.blendEquation;
            this.blendSrcAlpha = source.blendSrcAlpha;
            this.blendDstAlpha = source.blendDstAlpha;
            this.blendEquationAlpha = source.blendEquationAlpha;
            this.depthFunc = source.depthFunc;
            this.depthTest = source.depthTest;
            this.depthWrite = source.depthWrite;
            this.stencilWriteMask = source.stencilWriteMask;
            this.stencilFunc = source.stencilFunc;
            this.stencilRef = source.stencilRef;
            this.stencilFuncMask = source.stencilFuncMask;
            this.stencilFail = source.stencilFail;
            this.stencilZFail = source.stencilZFail;
            this.stencilZPass = source.stencilZPass;
            this.stencilWrite = source.stencilWrite;
            var srcPlanes = source.clippingPlanes;
            ListEx<Plane> dstPlanes = null;
            if (srcPlanes != null)
            {
                var n = srcPlanes.Length;
                dstPlanes = new ListEx<Plane>(n);
                for (int i = 0; i != n; ++i)
                {
                    dstPlanes[i] = srcPlanes[i].Clone();
                }
            }
            this.clippingPlanes = dstPlanes;
            this.clipIntersection = source.clipIntersection;
            this.clipShadows = source.clipShadows;
            this.shadowSide = source.shadowSide;
            this.colorWrite = source.colorWrite;
            this.precision = source.precision;
            this.polygonOffset = source.polygonOffset;
            this.polygonOffsetFactor = source.polygonOffsetFactor;
            this.polygonOffsetUnits = source.polygonOffsetUnits;
            this.dithering = source.dithering;
            this.alphaTest = source.alphaTest;
            this.alphaToCoverage = source.alphaToCoverage;
            this.premultipliedAlpha = source.premultipliedAlpha;
            this.forceSinglePass = source.forceSinglePass;
            this.visible = source.visible;
            this.toneMapped = source.toneMapped;
            this.userData = source.userData.clone();//JSON.parse(JSON.stringify(source.userData));
            this.uniformsGroups = UniformsUtils.cloneUniformsGroups(source.uniformsGroups);//by yf albubo
            return this;
        }
        public void dispose()
        {
            //this.args.ctx = null;//解除互相引用
            this.dispatchEvent(new EventArgs { type = "dispose" });
        }
        #endregion

    }
}
