using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class MeshNormalMaterial : Material
    {
        #region Properties

        //public int normalMapType;
        //public Vector2 normalScale;
        //public Texture normalMap ;

        //public bool flatShading;
        //public Texture bumpMap ;
        //public double bumpScale ;
        //public Texture displacementMap ;
        //public double displacementScale ;
        //public double displacementBias ;

        #endregion

        #region constructor
        public MeshNormalMaterial() : base()
        {
            this.type = "MeshNormalMaterial";
            this.bumpMap = null;
            this.bumpScale = 1;
            this.normalMap = null;
            this.normalMapType = TangentSpaceNormalMap;
            this.normalScale = new Vector2(1, 1);
            this.displacementMap = null;
            this.displacementScale = 1;
            this.displacementBias = 0;
            this.wireframe = false;
            this.wireframeLinewidth = 1;
            this.flatShading = false;
        }
        #endregion

        #region methods
        public MeshNormalMaterial copy(MeshNormalMaterial source)
        {
            base.copy(source);
            this.bumpMap = source.bumpMap;
            this.bumpScale = source.bumpScale;
            this.normalMap = source.normalMap;
            this.normalMapType = source.normalMapType;
            this.normalScale.Copy(source.normalScale);
            this.displacementMap = source.displacementMap;
            this.displacementScale = source.displacementScale;
            this.displacementBias = source.displacementBias;
            this.wireframe = source.wireframe;
            this.wireframeLinewidth = source.wireframeLinewidth;
            this.flatShading = source.flatShading;
            return this;
        }
        public override Material clone()
        {
            return new MeshNormalMaterial().copy(this);
        }
        #endregion

    }
}
