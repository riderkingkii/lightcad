using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class SpriteMaterial : Material
    {
        #region Properties

        //public Color color ;
        //public Texture map ;
        //public JsArr<Texture> maps ;
        //public Texture alphaMap ;
        //public double rotation;
        //public bool sizeAttenuation;

        #endregion

        #region constructor
        public SpriteMaterial() : base()
        {
            this.type = "SpriteMaterial";
            this.color = new Color(0xffffff);
            this.map = null;
            this.alphaMap = null;
            this.rotation = 0;
            this.sizeAttenuation = true;
            this.transparent = true;
            this.fog = true;
        }
        #endregion

        #region methods
        public SpriteMaterial copy(SpriteMaterial source)
        {
            base.copy(source);
            this.color.Copy(source.color);
            this.map = source.map;
            this.alphaMap = source.alphaMap;
            this.rotation = source.rotation;
            this.sizeAttenuation = source.sizeAttenuation;
            this.fog = source.fog;
            return this;
        }
        public override Material clone()
        {
            return new SpriteMaterial().copy(this);
        }
        #endregion

    }
}
