using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class PointsMaterial : Material
    {
        #region Properties

        //public Color color ;
        //public Texture map ;
        //public JsArr<Texture> maps ;
        //public Texture alphaMap ;
        //public double size;
        //public bool sizeAttenuation;

        #endregion

        #region constructor
        public PointsMaterial() : base()
        {
            this.type = "PointsMaterial";
            this.color = new Color(0xffffff);
            this.map = null;
            this.alphaMap = null;
            this.size = 1;
            this.sizeAttenuation = true;
            this.fog = true;
        }
        #endregion

        #region methods
        public PointsMaterial copy(PointsMaterial source)
        {
            base.copy(source);
            this.color.Copy(source.color);
            this.map = source.map;
            this.alphaMap = source.alphaMap;
            this.size = source.size;
            this.sizeAttenuation = source.sizeAttenuation;
            this.fog = source.fog;
            return this;
        }
        public override Material clone()
        {
            return new PointsMaterial().copy(this);
        }
        #endregion

    }
}
