using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class MeshMatcapMaterial : Material
    {
        #region Properties

        //public JsObj<object> defines;
        //public string matcap;
        //public Texture normalMap ;
        //public int normalMapType;
        //public Vector2 normalScale;
        //public bool flatShading;

        //public Color color ;
        //public  Texture map ;
        //public Texture bumpMap ;
        //public double bumpScale ;
        //public Texture alphaMap ;
        //public Texture displacementMap ;
        //public double displacementScale ;
        //public double displacementBias ;

        #endregion

        #region constructor
        public MeshMatcapMaterial() : base()
        {
            this.defines = new JsObj<object> { { "MATCAP", "" } };
            this.type = "MeshMatcapMaterial";
            this.color = new Color(0xffffff); // diffuse
            this.matcap = null;
            this.map = null;
            this.bumpMap = null;
            this.bumpScale = 1;
            this.normalMap = null;
            this.normalMapType = TangentSpaceNormalMap;
            this.normalScale = new Vector2(1, 1);
            this.displacementMap = null;
            this.displacementScale = 1;
            this.displacementBias = 0;
            this.alphaMap = null;
            this.flatShading = false;
            this.fog = true;
        }
        #endregion

        #region methods
        public MeshMatcapMaterial copy(MeshMatcapMaterial source)
        {
            base.copy(source);
            this.defines = new JsObj<object> { { "MATCAP", "" } };
            this.color.Copy(source.color);
            this.matcap = source.matcap;
            this.map = source.map;
            this.bumpMap = source.bumpMap;
            this.bumpScale = source.bumpScale;
            this.normalMap = source.normalMap;
            this.normalMapType = source.normalMapType;
            this.normalScale.Copy(source.normalScale);
            this.displacementMap = source.displacementMap;
            this.displacementScale = source.displacementScale;
            this.displacementBias = source.displacementBias;
            this.alphaMap = source.alphaMap;
            this.flatShading = source.flatShading;
            this.fog = source.fog;
            return this;
        }


        public override Material clone()
        {
            return new MeshMatcapMaterial().copy(this);
        }
        #endregion

    }
}
