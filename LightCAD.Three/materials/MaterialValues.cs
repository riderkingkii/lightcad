using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;


namespace LightCAD.Three
{
    //public class MaterialValues
    //{
    //    public Texture map = null;
    //    public Texture envMap = null;
    //    public double? scale = null;
    //    public double? dashSize = null;
    //    public double? gapSize = null;
    //    public bool? dashed = null;
    //    public Color specular = null;
    //    public double? shininess = null;
    //    public double? linewidth = null;
    //    public bool? clipping = null;
    //    public string name = null;
    //    public string type = null;
    //    public int? blending = null;
    //    public int? side = null;
    //    public int? depthPacking = null;
    //    public bool? vertexColors = null;
    //    public bool? wireFrame = null;
    //    public Color color = null;
    //    public Color emissive = null;
    //    public double? size = null;

    //    public double? envMapIntensity;

    //    public int? normalMapType;

    //    /// <summary>
    //    /// 定义材质是否使用平面着色进行渲染。默认值为false
    //    /// </summary>
    //    public bool? flatShading = null;

    //    public double? opacity = null;
    //    public bool? transparent = null;

    //    public int? blendSrc = null;
    //    public int? blendDst = null;
    //    public int? blendEquation = null;
    //    public int? blendSrcAlpha = null;
    //    public int? blendDstAlpha = null;
    //    public int? blendEquationAlpha = null;

    //    public int? depthFunc = null;
    //    public bool? depthTest = null;
    //    public bool? depthWrite = null;
    //    public bool? fog = null;
    //    public bool? wireframe = null;
    //    public uint? stencilWriteMask = null;
    //    public int? stencilFunc = null;
    //    public int? stencilRef = null;
    //    public uint? stencilFuncMask = null;
    //    public int? stencilFail = null;
    //    public int? stencilZFail = null;
    //    public int? stencilZPass = null;
    //    public bool? stencilWrite = null;

    //    public JsArr<Plane> clippingPlanes = null;
    //    public bool? clipIntersection = null;
    //    public bool? clipShadows = null;
    //    public int? shadowSide = null;
    //    public bool? colorWrite = null;
    //    public string precision = null;
    //    public bool? polygonOffset = null;
    //    public double? polygonOffsetFactor = null;
    //    public double? polygonOffsetUnits = null;
    //    public bool? dithering = null;
    //    public bool? alphaToCoverage = null;
    //    public bool? premultipliedAlpha = null;
    //    public bool? visible = null;
    //    public bool? toneMapped = null;
    //    public JsObj<object> userData = null;
    //    public int? version = null;
    //    public double? alphaTest = null;
    //    public bool? needsUpdate = null;
    //    public int? id = null;
    //    public JsObj<object> defines = null;
    //    public Uniforms uniforms = null;
    //    public string vertexShader = null;
    //    public string fragmentShader = null;
    //    public string glslVersion = null;

    //    /// <summary>
    //    /// 金属度
    //    /// </summary>
    //    public double? metalness = null;
    //    /// <summary>
    //    /// 该纹理的蓝色通道用于改变材质的金属度。
    //    /// </summary>
    //    public Texture metalnessMap = null;

    //    /// <summary>
    //    /// 
    //    /// 粗糙度
    //    /// </summary>
    //    public double? roughness = null;
    //    /// <summary>
    //    /// 该纹理的绿色通道用于改变材质的粗糙度。
    //    /// </summary>
    //    public Texture roughnessMap = null;

    //    public bool sizeAttenuation;

    //    public double? emissiveIntensity;
    //    public Vector2 normalScale;
    //    public Texture normalMap;
    //    public double? aoMapIntensity;
    //    public Texture aoMap;
    //    public Texture emissiveMap;

    //    public double? clearcoat;
    //    public Texture clearcoatMap;
    //    public double? clearcoatRoughness;
    //    public Texture clearcoatRoughnessMap;
    //    public Vector2 clearcoatNormalScale;
    //    public Texture clearcoatNormalMap;

    //    public double iridescence;
    //    public double iridescenceIOR;
    //    public double[] iridescenceThicknessRange;
    //    public Texture iridescenceThicknessMap;
    //    public Texture iridescenceMap;

    //    public Color sheenColor;
    //    public double sheen;
    //    public double sheenRoughness;
    //    public Texture sheenColorMap;
    //    public Texture sheenRoughnessMap;

    //    public double transmission;
    //    public Texture transmissionMap;

    //    public double thickness;
    //    public Texture thicknessMap;
    //    public double attenuationDistance;
    //    public Color attenuationColor;
    //    public double ior;

    //    public double specularIntensity;
    //    public Texture specularIntensityMap;
    //    public Color specularColor;
    //    public Texture specularColorMap;

    //    /// <summary>
    //    /// 凹凸比例
    //    /// </summary>
    //    public double? bumpScale;

    //    /// <summary>
    //    /// 凹凸贴图
    //    /// </summary>
    //    public Texture bumpMap;


    //    public void setFromJsObj(JsObj<object> jsObj)
    //    {
    //        foreach (var item in jsObj)
    //        {
    //            var name = item.Key;
    //            var value = item.Value;
    //            if (this.HasField(name))
    //            {
    //                this.SetField(name, value);
    //            }

    //        }
    //    }
    //}

}
