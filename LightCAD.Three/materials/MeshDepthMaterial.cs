using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class MeshDepthMaterial : Material
    {
        #region Properties
        //public Texture map ;
        //public Texture alphaMap ;
        //public Texture displacementMap ;
        //public double? displacementScale ;
        //public double? displacementBias ;

        //public int depthPacking ;
        #endregion

        #region constructor
        public MeshDepthMaterial() : base()
        {
            this.type = "MeshDepthMaterial";
            this.depthPacking = BasicDepthPacking;
            this.map = null;
            this.alphaMap = null;
            this.displacementMap = null;
            this.displacementScale = 1;
            this.displacementBias = 0;
            this.wireframe = false;
            this.wireframeLinewidth = 1;
        }
        #endregion

        #region methods
        public MeshDepthMaterial copy(MeshDepthMaterial source)
        {
            base.copy(source);
            this.depthPacking = source.depthPacking;
            this.map = source.map;
            this.alphaMap = source.alphaMap;
            this.displacementMap = source.displacementMap;
            this.displacementScale = source.displacementScale;
            this.displacementBias = source.displacementBias;
            this.wireframe = source.wireframe;
            this.wireframeLinewidth = source.wireframeLinewidth;
            return this;
        }

        public override Material clone()
        {
            return new MeshDepthMaterial().copy(this);
        }
        #endregion

    }
}
