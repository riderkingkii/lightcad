using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;
using LightCAD.MathLib;
namespace LightCAD.Three
{
    public class MeshPhongMaterial : Material
    {
        //#region Properties

        //public Color color ;
        //public Texture map ;
        //public JsArr<Texture> maps ;
        //public Color specular;
        //public double shininess;
        //public Texture lightMap ;
        //public double lightMapIntensity;
        //public Texture aoMap;
        //public double aoMapIntensity;
        //public Texture emissiveMap ;
        //public double emissiveIntensity ;
        //public Color emissive ;
        //public int normalMapType;
        //public Texture normalMap ;
        //public Vector2 normalScale;
        //public double reflectivity;
        //public double refractionRatio;
        //public string wireframeLinecap;
        //public string wireframeLinejoin;
        //public bool flatShading;
        //public Texture bumpMap ;
        //public double bumpScale ;
        //public Texture displacementMap ;
        //public double displacementScale ;
        //public double displacementBias ;

        //public Texture specularMap ;
        //public Texture alphaMap ;
        //public  Texture envMap ;
        //public int combine;

        //#endregion

        #region constructor
        public MeshPhongMaterial() : base()
        {
            this.defines = new JsObj<object>{
                    {"Phong", "" }
                };
            this.type = "MeshPhongMaterial";
            this.color = new Color(0xffffff); // diffuse
            this.specular = new Color(0x111111);
            this.shininess = 30;
            this.map = null;
            this.lightMap = null;
            this.lightMapIntensity = 1.0;
            this.aoMap = null;
            this.aoMapIntensity = 1.0;
            this.emissive = new Color(0x000000);
            this.emissiveIntensity = 1.0;
            this.emissiveMap = null;
            this.bumpMap = null;
            this.bumpScale = 1;
            this.normalMap = null;
            this.normalMapType = TangentSpaceNormalMap;
            this.normalScale = new Vector2(1, 1);
            this.displacementMap = null;
            this.displacementScale = 1;
            this.displacementBias = 0;
            this.specularMap = null;
            this.alphaMap = null;
            this.envMap = null;
            this.combine = MultiplyOperation;
            this.reflectivity = 1;
            this.refractionRatio = 0.98;
            this.wireframe = false;
            this.wireframeLinewidth = 1;
            this.wireframeLinecap = "round";
            this.wireframeLinejoin = "round";
            this.flatShading = false;
            this.fog = true;
        }
        #endregion

        #region methods
        public MeshPhongMaterial copy(MeshPhongMaterial source)
        {
            base.copy(source);
            this.color.Copy(source.color);
            this.specular.Copy(source.specular);
            this.shininess = source.shininess;
            this.map = source.map;
            this.lightMap = source.lightMap;
            this.lightMapIntensity = source.lightMapIntensity;
            this.aoMap = source.aoMap;
            this.aoMapIntensity = source.aoMapIntensity;
            this.emissive.Copy(source.emissive);
            this.emissiveMap = source.emissiveMap;
            this.emissiveIntensity = source.emissiveIntensity;
            this.bumpMap = source.bumpMap;
            this.bumpScale = source.bumpScale;
            this.normalMap = source.normalMap;
            this.normalMapType = source.normalMapType;
            this.normalScale.Copy(source.normalScale);
            this.displacementMap = source.displacementMap;
            this.displacementScale = source.displacementScale;
            this.displacementBias = source.displacementBias;
            this.specularMap = source.specularMap;
            this.alphaMap = source.alphaMap;
            this.envMap = source.envMap;
            this.combine = source.combine;
            this.reflectivity = source.reflectivity;
            this.refractionRatio = source.refractionRatio;
            this.wireframe = source.wireframe;
            this.wireframeLinewidth = source.wireframeLinewidth;
            this.wireframeLinecap = source.wireframeLinecap;
            this.wireframeLinejoin = source.wireframeLinejoin;
            this.flatShading = source.flatShading;
            this.fog = source.fog;
            return this;
        }
        public override Material clone()
        {
            return new MeshPhongMaterial().copy(this);
        }
        #endregion

    }
}
