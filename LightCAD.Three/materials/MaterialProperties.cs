using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;


namespace LightCAD.Three
{
    public class MaterialProperties
    {
        internal double[] clippingState { get; set; }
        internal Uniforms uniforms { get; set; }

        internal int lightsStateVersion { get; set; }
        internal int outputEncoding { get; set; }
        internal bool instancing { get; set; }
        internal bool skinning { get; set; }
        internal int numClippingPlanes { get; set; } = -1; //0也是有效值
        internal int numIntersection { get; set; }
        internal bool receiveShadow { get; set; }

        internal Texture environment { get; set; }
        internal Texture envMap { get; set; }
        internal WebGLProgram currentProgram { get; set; }

        public JsObj<WebGLProgram> programs { get; set; }

        public bool needsLights { get; set; }
        public bool vertexTangents { get; set; }
        public bool morphTargets { get; set; }
        public bool morphNormals { get; set; }
        public bool morphColors { get; set; }
        public int toneMapping { get; set; }
        public int morphTargetsCount { get; set; }
        public bool vertexAlphas { get; set; }
        public int? __version { get; set; }
        public ListEx<IUniform> uniformsList { get; set; }
        public IFog fog { get; set; }

        public Light light;
    }

}
