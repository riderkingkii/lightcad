using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;
using LightCAD.MathLib;
namespace LightCAD.Three
{

    public class MeshStandardMaterial : Material
    {
        #region Properties

        //public JsObj<string> defines;
        //public double roughness;
        //public double metalness;
        //public Texture lightMap ;
        //public double lightMapIntensity;
        //public Texture aoMap;
        //public double aoMapIntensity;
        //public Texture emissiveMap ;
        //public double emissiveIntensity ;
        //public Color emissive ;
        //public Texture normalMap ;
        //public int normalMapType;
        //public Vector2 normalScale;
        //public double envMapIntensity;
        //public string wireframeLinecap;
        //public string wireframeLinejoin;
        //public bool flatShading;

        //public Color color ;
        //public Texture map ;
        //public JsArr<Texture> maps ;
        //public Texture bumpMap ;
        //public double bumpScale ;
        //public Texture displacementMap ;
        //public double displacementScale ;
        //public double displacementBias ;
        //public Texture roughnessMap;
        //public Texture metalnessMap;
        //public Texture alphaMap ;
        //public Texture envMap ;
        #endregion

        #region constructor
        public MeshStandardMaterial() 
        {
            this.defines = new JsObj<object> { { "STANDARD", "" } };
            this.type = "MeshStandardMaterial";
            this.color = new Color(0xffffff); // diffuse
            this.roughness = 1.0;
            this.metalness = 0.0;
            this.map = null;
            this.lightMap = null;
            this.lightMapIntensity = 1.0;
            this.aoMap = null;
            this.aoMapIntensity = 1.0;
            this.emissive = new Color(0x000000);
            this.emissiveIntensity = 1.0;
            this.emissiveMap = null;
            this.bumpMap = null;
            this.bumpScale = 1;
            this.normalMap = null;
            this.normalMapType = TangentSpaceNormalMap;
            this.normalScale = new Vector2(1, 1);
            this.displacementMap = null;
            this.displacementScale = 1;
            this.displacementBias = 0;
            this.roughnessMap = null;
            this.metalnessMap = null;
            this.alphaMap = null;
            this.envMap = null;
            this.envMapIntensity = 1.0;
            this.wireframe = false;
            this.wireframeLinewidth = 1;
            this.wireframeLinecap = "round";
            this.wireframeLinejoin = "round";
            this.flatShading = false;
            this.fog = true;
        }
        #endregion

        #region methods
        public MeshStandardMaterial copy(MeshStandardMaterial source)
        {
            base.copy(source);
            this.defines = new JsObj<object> { { "STANDARD", "" } };
            this.color.Copy(source.color);
            this.roughness = source.roughness;
            this.metalness = source.metalness;
            this.map = source.map;
            this.lightMap = source.lightMap;
            this.lightMapIntensity = source.lightMapIntensity;
            this.aoMap = source.aoMap;
            this.aoMapIntensity = source.aoMapIntensity;
            this.emissive.Copy(source.emissive);
            this.emissiveMap = source.emissiveMap;
            this.emissiveIntensity = source.emissiveIntensity;
            this.bumpMap = source.bumpMap;
            this.bumpScale = source.bumpScale;
            this.normalMap = source.normalMap;
            this.normalMapType = source.normalMapType;
            this.normalScale.Copy(source.normalScale);
            this.displacementMap = source.displacementMap;
            this.displacementScale = source.displacementScale;
            this.displacementBias = source.displacementBias;
            this.roughnessMap = source.roughnessMap;
            this.metalnessMap = source.metalnessMap;
            this.alphaMap = source.alphaMap;
            this.envMap = source.envMap;
            this.envMapIntensity = source.envMapIntensity;
            this.wireframe = source.wireframe;
            this.wireframeLinewidth = source.wireframeLinewidth;
            this.wireframeLinecap = source.wireframeLinecap;
            this.wireframeLinejoin = source.wireframeLinejoin;
            this.flatShading = source.flatShading;
            this.fog = source.fog;
            return this;
        }
        public override Material clone()
        {
            return new MeshStandardMaterial().copy(this);
        }
        #endregion

    }
}
