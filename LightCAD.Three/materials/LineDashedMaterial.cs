using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class LineDashedMaterial : LineBasicMaterial
    {
        #region Properties

        //public double scale ;
        //public double dashSize ;
        //public double gapSize ;

        #endregion

        #region constructor

        public LineDashedMaterial() 
        {
            this.type = "LineDashedMaterial";
            this.scale = 1;
            this.dashSize = 3;
            this.gapSize = 1;
        }
        #endregion

        #region methods

        public LineDashedMaterial copy(LineDashedMaterial source)
        {
            base.copy(source);
            this.scale = source.scale;
            this.dashSize = source.dashSize;
            this.gapSize = source.gapSize;
            return this;
        }
        public override Material clone()
        {
            return new LineDashedMaterial().copy(this);
        }
        #endregion

    }
}
