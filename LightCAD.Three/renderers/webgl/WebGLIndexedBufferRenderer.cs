﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.Three.OpenGL;

namespace LightCAD.Three
{

    public class WebGLIndexedBufferRenderer : WebGLBufferRenderer
    {
        protected int type;
        protected int bytesPerElement;

        public WebGLIndexedBufferRenderer(WebGLExtensions extensions, WebGLInfo info, WebGLCapabilities capabilities)
        : base(extensions, info, capabilities)
        {
            this.extensions = extensions;
            this.info = info;
            this.capabilities = capabilities;

        }
        public override void setMode(int value)
        {
            mode = value;
        }
        public void setIndex(BufferObject value)
        {

            this.type = value.type;
            this.bytesPerElement = value.bytesPerElement;

        }
        public override void render(int start, int count)
        {
            gl.drawElements(mode, count, type, start * bytesPerElement);

            info.update(count, mode, 1);
        }

        public override void renderInstances(int start, int count, int primcount)
        {
            if (primcount == 0) return;

            var extension = extensions.get("ANGLE_instanced_arrays");

            if (extension == 0)
            {
                console.error("THREE.WebGLIndexedBufferRenderer: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.");
                return;
            }

            gl.drawElementsInstanced(mode, count, type, start * bytesPerElement, primcount);

            info.update(count, mode, primcount);

        }
    }
    public class WebGLIndexedBufferIndirectRenderer : WebGLIndexedBufferRenderer
    {
        public WebGLIndexedBufferIndirectRenderer(WebGLExtensions extensions, WebGLInfo info, WebGLCapabilities capabilities) : base(extensions, info, capabilities)
        {

        }
        private gl.DrawElementsIndirectCommand[] indexCommands;
        //public void setCmds(IMultiIndirectObject multiObj, GeoGroup group)
        //{
        //    indexCommands = multiObj.getIndexCommands(group as MergeGroup.MGeoGroup);
        //}
        /// <summary>
        /// Indirect 对象由cmd来决定drawRange start,和count无效
        /// </summary>
        /// <param name="start"></param>
        /// <param name="count"></param>
        public override void render(int start, int count)
        {
            gl.MultiDrawElementsIndirect(mode, type, indexCommands, 0);
            info.update(count, mode, 1);
        }
    }
}
