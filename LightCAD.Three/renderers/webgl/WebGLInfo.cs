﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;
using LightCAD.Three.OpenGL;

namespace LightCAD.Three
{
    public class WebGLInfo
    {
        public class WebGLMemoryInfo
        {
            public int geometries;
            public int textures;
        }
        public class WebGLRenderInfo
        {
            public int frame;
            public int calls;
            public int triangles;
            public int points;
            public int lines;
        }
        public readonly WebGLMemoryInfo memory;
        public readonly WebGLRenderInfo render;
        public bool autoReset = true;
        public ListEx<WebGLProgram> programs = null;
        public WebGLInfo()
        {
            this.memory = new WebGLMemoryInfo
            {
                geometries = 0,
                textures = 0

            };

            this.render = new WebGLRenderInfo
            {
                frame = 0,
                calls = 0,
                triangles = 0,
                points = 0,
                lines = 0
            };
        }
        public void update(int count, int mode, int instanceCount)
        {

            render.calls++;

            switch (mode)
            {

                case gl.TRIANGLES:
                    render.triangles += instanceCount * (count / 3);
                    break;

                case gl.LINES:
                    render.lines += instanceCount * (count / 2);
                    break;

                case gl.LINE_STRIP:
                    render.lines += instanceCount * (count - 1);
                    break;

                case gl.LINE_LOOP:
                    render.lines += instanceCount * count;
                    break;

                case gl.POINTS:
                    render.points += instanceCount * count;
                    break;

                default:
                    console.error("THREE.WebGLInfo: Unknown draw mode:", mode);
                    break;

            }

        }

        public void reset()
        {

            render.frame++;
            render.calls = 0;
            render.triangles = 0;
            render.points = 0;
            render.lines = 0;

        }

    }
}
