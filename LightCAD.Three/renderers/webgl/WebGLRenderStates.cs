﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LightCAD.Three
{
    public class WebGLRenderState
    {
        public class State
        {
            public ListEx<Light> lightsArray;
            public ListEx<Light> shadowsArray;
            public WebGLLights lights;
        }
        private readonly WebGLLights lights;
        public readonly State state;
        private readonly ListEx<Light> lightsArray;
        private readonly ListEx<Light> shadowsArray;
        public WebGLRenderState(WebGLExtensions extensions, WebGLCapabilities capabilities)
        {
            this.lights = new WebGLLights(extensions, capabilities);
            this.lightsArray = new ListEx<Light>();
            this.shadowsArray = new ListEx<Light>();
            this.state = new State
            {
                lightsArray = lightsArray,
                shadowsArray = shadowsArray,
                lights = lights
            };
        }
        public void init()
        {

            lightsArray.Length = 0;
            shadowsArray.Length = 0;
        }

        public void pushLight(Light light)
        {

            lightsArray.Push(light);

        }

        public void pushShadow(Light shadowLight)
        {

            shadowsArray.Push(shadowLight);

        }

        public void setupLights(bool useLegacyLights)
        {
            lights.setup(lightsArray, useLegacyLights);
        }

        public void setupLightsView(Camera camera)
        {
            lights.setupView(lightsArray, camera);
        }
    }

    public class WebGLRenderStates : IDispose
    {
        private JsObj<Object3D, ListEx<WebGLRenderState>> renderStates;
        private WebGLExtensions extensions;
        private WebGLCapabilities capabilities;
        public WebGLRenderStates(WebGLExtensions extensions, WebGLCapabilities capabilities)
        {
            this.renderStates = new JsObj<Object3D, ListEx<WebGLRenderState>>();
            this.extensions = extensions;
            this.capabilities = capabilities;
        }
        public WebGLRenderState get(Object3D scene, int renderCallDepth = 0)
        {

            var renderStateArray = renderStates.get(scene);
            WebGLRenderState renderState;

            if (renderStateArray == null)
            {

                renderState = new WebGLRenderState(extensions, capabilities);
                renderStates.set(scene, new ListEx<WebGLRenderState> { renderState });

            }
            else
            {

                if (renderCallDepth >= renderStateArray.Length)
                {

                    renderState = new WebGLRenderState(extensions, capabilities);
                    renderStateArray.Push(renderState);

                }
                else
                {

                    renderState = renderStateArray[renderCallDepth];

                }

            }

            return renderState;

        }

        public void dispose()
        {

            renderStates = new JsObj<Object3D, ListEx<WebGLRenderState>>();
        }
    }
}
