﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{

    public class WebGLProperties : IDispose
    {
        private JsObj<object, object> properties;
        public WebGLProperties()
        {
            this.properties = new JsObj<object, object>();
        }
        public JsObj<object> get(object _object)
        {
            var map = properties.get(_object);
            if (map == null)
            {
                map = new JsObj<object> { };
                properties.set(_object, map);
            }
            return (JsObj<object>)map;
        }
        public MaterialProperties get(Material _object)
        {
            var map = properties.get(_object);
            if (map == null)
            {
                map = new MaterialProperties();
                properties.set(_object, map);
            }
            return (MaterialProperties)map;
        }
        public void remove(object _object)
        {
            properties.delete(_object);
        }

        public void update(object _object, string key, object value)
        {
            if (_object is Material)
                (properties.get(_object) as MaterialProperties).SetField(key, value);
            else
                (properties.get(_object) as JsObj<object>)[key] = value;
        }

        public void dispose()
        {
            properties = new JsObj<object, object>();
        }
    }
}
