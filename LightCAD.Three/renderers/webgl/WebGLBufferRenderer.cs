﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.Three.OpenGL;

namespace LightCAD.Three
{
    public class WebGLBufferRenderer
    {
        protected int mode;
        protected WebGLExtensions extensions;
        protected WebGLInfo info;
        protected WebGLCapabilities capabilities;
        public WebGLBufferRenderer(WebGLExtensions extensions, WebGLInfo info, WebGLCapabilities capabilities)
        {
            this.extensions = extensions;
            this.info = info;
            this.capabilities = capabilities;
        }


        public virtual void setMode(int value)
        {
            mode = value;
        }

        public virtual void render(int start, int count)
        {
            gl.drawArrays(mode, start, count);

            info.update(count, mode, 1);

        }

        public virtual void renderInstances(int start, int count, int primcount)
        {

            if (primcount == 0) return;

            bool isWebGL2 = capabilities.isWebGL2;
            if (isWebGL2)
            {
                //找不到接口
                gl.drawArraysInstanced(mode, start, count, primcount);
            }
            else
            {
                var extension = extensions.get("ANGLE_instanced_arrays");

                if (extension == 0) return;

                if (gl.getSupport("GL_EXT_draw_instanced"))//"glDrawArraysInstanced"
                {
                    gl.drawArraysInstanced(mode, start, count, primcount);
                }
                else if (gl.getSupport("GL_ARB_draw_instanced"))//"glDrawArraysInstancedARB"
                    gl.drawArraysInstancedARB(mode, start, count, primcount);
            }

            info.update(count, mode, primcount);
        }
    }
}
