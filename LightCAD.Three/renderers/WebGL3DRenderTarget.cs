﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public class WebGL3DRenderTarget : WebGLRenderTarget
    {
        public WebGL3DRenderTarget(int width = 1, int height = 1, int depth = 1) :
                base(width, height)
        {
            this.depth = depth;
            this.texture = new Data3DTexture(null, width, height, depth);
            this.texture.isRenderTargetTexture = true;

        }
    }
}
