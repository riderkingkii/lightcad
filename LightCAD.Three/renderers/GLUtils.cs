﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using LightCAD.Three.OpenGL;

namespace LightCAD.Three
{
    public static class GLUtils
    {
        public static readonly RenderCanvas Canvas = new RenderCanvas();
        
       
        
    }
    public interface IGraphicsContext
    {
        public event EventHandler<FrameEventArgs> RenderFrame;
    }
    public sealed class RenderCanvas
    {

        public int Width;
        public int Height;
        public IGraphicsContext Context { get; set; }
       
        public RenderCanvas()
        {
        }
        public RenderCanvas(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }
        public RenderCanvas Set(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            return this;
        }
        public void MakeCurrent()
        {
        }
    }


    public class FrameEventArgs : EventArgs
    {
        private double elapsed;

        /// <summary>
        /// Constructs a new FrameEventArgs instance.
        /// </summary>
        public FrameEventArgs()
        { }

        /// <summary>
        /// Constructs a new FrameEventArgs instance.
        /// </summary>
        /// <param name="elapsed">The amount of time that has elapsed since the previous event, in seconds.</param>
        public FrameEventArgs(double elapsed)
        {
            Time = elapsed;
        }

        /// <summary>
        /// Gets a <see cref="System.Double"/> that indicates how many seconds of time elapsed since the previous event.
        /// </summary>
        public double Time
        {
            get { return elapsed; }
            internal set
            {

                elapsed = value;
            }
        }
    }
}
