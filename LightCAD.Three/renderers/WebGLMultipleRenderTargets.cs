﻿
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class WebGLMultipleRenderTargets : WebGLRenderTarget
    {
        public WebGLMultipleRenderTargets(int width = 1, int height = 1, int count = 1, RenderTargetOptions options = null) : base(width, height, options)
        {
            var texture = this.texture;

            this.textures = new ListEx<Texture>();

            for (int i = 0; i < count; i++)
            {

                this.textures[i] = texture.Clone();
                this.textures[i].isRenderTargetTexture = true;

            }

        }
        public override WebGLRenderTarget setSize(int width, int height, int depth = 1)
        {

            if (this.width != width || this.height != height || this.depth != depth)
            {

                this.width = width;
                this.height = height;
                this.depth = depth;
                for (int i = 0, il = this.textures.Length; i < il; i++)
                {
                    this.textures[i].image.width = width;
                    this.textures[i].image.height = height;
                    this.textures[i].image.depth = depth;
                }

                this.dispose();

            }

            this.viewport.Set(0, 0, width, height);
            this.scissor.Set(0, 0, width, height);
            return this;

        }
        public override WebGLRenderTarget clone()
        {
            return new WebGLMultipleRenderTargets().copy(this);
        }
        public override WebGLRenderTarget copy(WebGLRenderTarget source)
        {
            return copy(source as WebGLMultipleRenderTargets);
        }
        public WebGLMultipleRenderTargets copy(WebGLMultipleRenderTargets source)
        {
            this.dispose();

            this.width = source.width;
            this.height = source.height;
            this.depth = source.depth;

            this.viewport.Set(0, 0, this.width, this.height);
            this.scissor.Set(0, 0, this.width, this.height);

            this.depthBuffer = source.depthBuffer;
            this.stencilBuffer = source.stencilBuffer;
            if (source.depthTexture != null) this.depthTexture = source.depthTexture.Clone() as DepthTexture;
            this.textures.Length = 0;
            this.texture = source.texture.Clone();
            this.texture.isRenderTargetTexture = true;
            for (int i = 0, il = source.textures.Length; i < il; i++)
            {

                this.textures[i] = source.textures[i].Clone();
                this.textures[i].isRenderTargetTexture = true;

            }

            return this;

        }
    }
}