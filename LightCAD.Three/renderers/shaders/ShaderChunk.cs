using System.Reflection;
using System.Linq;
using System;
using System.IO;

namespace LightCAD.Three
{

    public class ShaderChunk
    {

        public static string get(string resName)
        {
            var asm = Assembly.GetExecutingAssembly();
            var names = asm.GetManifestResourceNames();
            var key = "LightCAD.Three.renderers.shaders.ShaderChunk." + resName;
            var stream = asm.GetManifestResourceStream(key);
            var sr = new StreamReader(stream);
            return sr.ReadToEnd();
        }
        //by yf albubo
        public static string albnormaltarget_fragment = get("albnormaltarget_fragment.glsl.js");
        public static string albnormaltarget_pars_fragment = get("albnormaltarget_pars_fragment.glsl.js");
        public static string albubo_fragment = get("albubo_fragment.glsl.js");
        public static string albubo_pars_fragment = get("albubo_pars_fragment.glsl.js");
        public static string albubo_pars_vertex = get("albubo_pars_vertex.glsl.js");
        public static string albubo_vertex = get("albubo_vertex.glsl.js");

        public static string alphamap_fragment = get("alphamap_fragment.glsl.js");
        public static string alphamap_pars_fragment = get("alphamap_pars_fragment.glsl.js");
        public static string alphatest_fragment = get("alphatest_fragment.glsl.js");

        public static string alphatest_pars_fragment = get("alphatest_pars_fragment.glsl.js");
        public static string aomap_fragment = get("aomap_fragment.glsl.js");
        public static string aomap_pars_fragment = get("aomap_pars_fragment.glsl.js");
        public static string begin_vertex = get("begin_vertex.glsl.js");
        public static string beginnormal_vertex = get("beginnormal_vertex.glsl.js");
        public static string bsdfs = get("bsdfs.glsl.js");
        public static string iridescence_fragment = get("iridescence_fragment.glsl.js");
        public static string bumpmap_pars_fragment = get("bumpmap_pars_fragment.glsl.js");
        public static string clipping_planes_fragment = get("clipping_planes_fragment.glsl.js");
        public static string clipping_planes_pars_fragment = get("clipping_planes_pars_fragment.glsl.js");
        public static string clipping_planes_pars_vertex = get("clipping_planes_pars_vertex.glsl.js");
        public static string clipping_planes_vertex = get("clipping_planes_vertex.glsl.js");
        public static string color_fragment = get("color_fragment.glsl.js");
        public static string color_pars_fragment = get("color_pars_fragment.glsl.js");
        public static string color_pars_vertex = get("color_pars_vertex.glsl.js");
        public static string color_vertex = get("color_vertex.glsl.js");
        public static string common = get("common.glsl.js");
        public static string cube_uv_reflection_fragment = get("cube_uv_reflection_fragment.glsl.js");
        public static string defaultnormal_vertex = get("defaultnormal_vertex.glsl.js");
        public static string displacementmap_pars_vertex = get("displacementmap_pars_vertex.glsl.js");
        public static string displacementmap_vertex = get("displacementmap_vertex.glsl.js");
        public static string emissivemap_fragment = get("emissivemap_fragment.glsl.js");
        public static string emissivemap_pars_fragment = get("emissivemap_pars_fragment.glsl.js");
        public static string encodings_fragment = get("encodings_fragment.glsl.js");
        public static string encodings_pars_fragment = get("encodings_pars_fragment.glsl.js");
        public static string envmap_fragment = get("envmap_fragment.glsl.js");
        public static string envmap_common_pars_fragment = get("envmap_common_pars_fragment.glsl.js");
        public static string envmap_pars_fragment = get("envmap_pars_fragment.glsl.js");
        public static string envmap_pars_vertex = get("envmap_pars_vertex.glsl.js");
        public static string envmap_vertex = get("envmap_vertex.glsl.js");
        public static string fog_vertex = get("fog_vertex.glsl.js");
        public static string fog_pars_vertex = get("fog_pars_vertex.glsl.js");
        public static string fog_fragment = get("fog_fragment.glsl.js");
        public static string fog_pars_fragment = get("fog_pars_fragment.glsl.js");
        public static string gradientmap_pars_fragment = get("gradientmap_pars_fragment.glsl.js");
        public static string lightmap_fragment = get("lightmap_fragment.glsl.js");
        public static string lightmap_pars_fragment = get("lightmap_pars_fragment.glsl.js");
        public static string lights_lambert_fragment = get("lights_lambert_fragment.glsl.js");
        public static string lights_lambert_pars_fragment = get("lights_lambert_pars_fragment.glsl.js");
        public static string lights_pars_begin = get("lights_pars_begin.glsl.js");
        public static string envmap_physical_pars_fragment = get("envmap_physical_pars_fragment.glsl.js");
        public static string lights_toon_fragment = get("lights_toon_fragment.glsl.js");
        public static string lights_toon_pars_fragment = get("lights_toon_pars_fragment.glsl.js");
        public static string lights_phong_fragment = get("lights_phong_fragment.glsl.js");
        public static string lights_phong_pars_fragment = get("lights_phong_pars_fragment.glsl.js");
        public static string lights_physical_fragment = get("lights_physical_fragment.glsl.js");
        public static string lights_physical_pars_fragment = get("lights_physical_pars_fragment.glsl.js");
        public static string lights_fragment_begin = get("lights_fragment_begin.glsl.js");
        public static string lights_fragment_maps = get("lights_fragment_maps.glsl.js");
        public static string lights_fragment_end = get("lights_fragment_end.glsl.js");
        public static string logdepthbuf_fragment = get("logdepthbuf_fragment.glsl.js");
        public static string logdepthbuf_pars_fragment = get("logdepthbuf_pars_fragment.glsl.js");
        public static string logdepthbuf_pars_vertex = get("logdepthbuf_pars_vertex.glsl.js");
        public static string logdepthbuf_vertex = get("logdepthbuf_vertex.glsl.js");
        public static string map_fragment = get("map_fragment.glsl.js");
        public static string map_pars_fragment = get("map_pars_fragment.glsl.js");
        public static string map_particle_fragment = get("map_particle_fragment.glsl.js");
        public static string map_particle_pars_fragment = get("map_particle_pars_fragment.glsl.js");
        public static string metalnessmap_fragment = get("metalnessmap_fragment.glsl.js");
        public static string metalnessmap_pars_fragment = get("metalnessmap_pars_fragment.glsl.js");
        public static string morphcolor_vertex = get("morphcolor_vertex.glsl.js");
        public static string morphnormal_vertex = get("morphnormal_vertex.glsl.js");
        public static string morphtarget_pars_vertex = get("morphtarget_pars_vertex.glsl.js");
        public static string morphtarget_vertex = get("morphtarget_vertex.glsl.js");
        public static string normal_fragment_begin = get("normal_fragment_begin.glsl.js");
        public static string normal_fragment_maps = get("normal_fragment_maps.glsl.js");
        public static string normal_pars_fragment = get("normal_pars_fragment.glsl.js");
        public static string normal_pars_vertex = get("normal_pars_vertex.glsl.js");
        public static string normal_vertex = get("normal_vertex.glsl.js");
        public static string normalmap_pars_fragment = get("normalmap_pars_fragment.glsl.js");
        public static string clearcoat_normal_fragment_begin = get("clearcoat_normal_fragment_begin.glsl.js");
        public static string clearcoat_normal_fragment_maps = get("clearcoat_normal_fragment_maps.glsl.js");
        public static string clearcoat_pars_fragment = get("clearcoat_pars_fragment.glsl.js");
        public static string iridescence_pars_fragment = get("iridescence_pars_fragment.glsl.js");
        public static string output_fragment = get("output_fragment.glsl.js");
        public static string packing = get("packing.glsl.js");
        public static string premultiplied_alpha_fragment = get("premultiplied_alpha_fragment.glsl.js");
        public static string project_vertex = get("project_vertex.glsl.js");
        public static string dithering_fragment = get("dithering_fragment.glsl.js");
        public static string dithering_pars_fragment = get("dithering_pars_fragment.glsl.js");
        public static string roughnessmap_fragment = get("roughnessmap_fragment.glsl.js");
        public static string roughnessmap_pars_fragment = get("roughnessmap_pars_fragment.glsl.js");
        public static string shadowmap_pars_fragment = get("shadowmap_pars_fragment.glsl.js");
        public static string shadowmap_pars_vertex = get("shadowmap_pars_vertex.glsl.js");
        public static string shadowmap_vertex = get("shadowmap_vertex.glsl.js");
        public static string shadowmask_pars_fragment = get("shadowmask_pars_fragment.glsl.js");
        public static string skinbase_vertex = get("skinbase_vertex.glsl.js");
        public static string skinning_pars_vertex = get("skinning_pars_vertex.glsl.js");
        public static string skinning_vertex = get("skinning_vertex.glsl.js");
        public static string skinnormal_vertex = get("skinnormal_vertex.glsl.js");
        public static string specularmap_fragment = get("specularmap_fragment.glsl.js");
        public static string specularmap_pars_fragment = get("specularmap_pars_fragment.glsl.js");
        public static string tonemapping_fragment = get("tonemapping_fragment.glsl.js");
        public static string tonemapping_pars_fragment = get("tonemapping_pars_fragment.glsl.js");
        public static string transmission_fragment = get("transmission_fragment.glsl.js");
        public static string transmission_pars_fragment = get("transmission_pars_fragment.glsl.js");
        public static string uv_pars_fragment = get("uv_pars_fragment.glsl.js");
        public static string uv_pars_vertex = get("uv_pars_vertex.glsl.js");
        public static string uv_vertex = get("uv_vertex.glsl.js");
        //public static string uv2_pars_fragment = get("uv2_pars_fragment.glsl.js");
        //public static string uv2_pars_vertex = get("uv2_pars_vertex.glsl.js");
        //public static string uv2_vertex = get("uv2_vertex.glsl.js");
        public static string worldpos_vertex = get("worldpos_vertex.glsl.js");
        //by rider
        //public static string lbim_vertex = get("lbim_vertex.glsl.js");
        //public static string lbim_fragment = get("lbim_fragment.glsl.js");

        public static string background_vert = glsl_background.vertex;
        public static string background_frag = glsl_background.fragment;
        public static string backgroundCube_vert = glsl_backgroundCube.vertex;
        public static string backgroundCube_frag = glsl_backgroundCube.fragment;
        public static string cube_vert = glsl_cube.vertex;
        public static string cube_frag = glsl_cube.fragment;
        public static string depth_vert = glsl_depth.vertex;
        public static string depth_frag = glsl_depth.fragment;
        public static string distanceRGBA_vert = glsl_distanceRGBA.vertex;
        public static string distanceRGBA_frag = glsl_distanceRGBA.fragment;
        public static string equirect_vert = glsl_equirect.vertex;
        public static string equirect_frag = glsl_equirect.fragment;
        public static string linedashed_vert = glsl_linedashed.vertex;
        public static string linedashed_frag = glsl_linedashed.fragment;
        public static string meshbasic_vert = glsl_meshbasic.vertex;
        public static string meshbasic_frag = glsl_meshbasic.fragment;
        public static string meshlambert_vert = glsl_meshlambert.vertex;
        public static string meshlambert_frag = glsl_meshlambert.fragment;
        public static string meshmatcap_vert = glsl_meshmatcap.vertex;
        public static string meshmatcap_frag = glsl_meshmatcap.fragment;
        public static string meshnormal_vert = glsl_meshnormal.vertex;
        public static string meshnormal_frag = glsl_meshnormal.fragment;
        public static string meshphong_vert = glsl_meshphong.vertex;
        public static string meshphong_frag = glsl_meshphong.fragment;
        public static string meshphysical_vert = glsl_meshphysical.vertex;
        public static string meshphysical_frag = glsl_meshphysical.fragment;
        public static string meshtoon_vert = glsl_meshtoon.vertex;
        public static string meshtoon_frag = glsl_meshtoon.fragment;
        public static string points_vert = glsl_points.vertex;
        public static string points_frag = glsl_points.fragment;
        public static string shadow_vert = glsl_shadow.vertex;
        public static string shadow_frag = glsl_shadow.fragment;
        public static string sprite_vert = glsl_sprite.vertex;
        public static string sprite_frag = glsl_sprite.fragment;
    }

}