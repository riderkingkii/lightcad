﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace LightCAD.Three
{
    public class UniformCommonValues : Uniforms
    {
        public UniformCommonValues()
        {
            this.diffuse = new Uniform() { value = new Color(0xffffff) };
            this.opacity = new Uniform() { value = 1.0 };
            this.map = new Uniform() { value = null };
            this.mapTransform = new Uniform() { value = new Matrix3() };
            //this.uvTransform = new Uniform() { value = new Matrix3() };
            //this.uv2Transform = new Uniform() { value = new Matrix3() };
            this.alphaMap = new Uniform() { value = null };
            this.alphaMapTransform = new Uniform() { value = new Matrix3() };
            this.alphaTest = new Uniform() { value = 0.0 };
        }
    }
    public class UniformSpecularMapValues : Uniforms
    {
        public UniformSpecularMapValues()
        {
            this.specularMap = new Uniform() { value = null };
            this.specularMapTransform = new Uniform() { value = new Matrix3() };
        }
    }
    public class UniformEnvMapValues : Uniforms
    {
        public UniformEnvMapValues()
        {
            this.envMap = new Uniform() { value = null };
            this.flipEnvMap = new Uniform() { value = -1 };
            this.reflectivity = new Uniform() { value = 1.0 };// basic, lambert, phong
            this.ior = new Uniform() { value = 1.5 };// physical
            this.refractionRatio = new Uniform() { value = 0.98 };// basic, lambert, phong
        }
    }
    public class UniformAoMapValues : Uniforms
    {
        public UniformAoMapValues()
        {
            this.aoMap = new Uniform() { value = null };
            this.aoMapIntensity = new Uniform() { value = 1 };
            this.aoMapTransform = new Uniform { value = new Matrix3() };
        }
    }
    public class UniformLightMapValues : Uniforms
    {

        public UniformLightMapValues()
        {
            this.lightMap = new Uniform() { value = null };
            this.lightMapIntensity = new Uniform() { value = 1 };
            this.lightMapTransform = new Uniform { value = new Matrix3() };
        }
    }

    public class UniformBumpMapValues : Uniforms
    {
        public UniformBumpMapValues()
        {
            this.bumpMap = new Uniform() { value = null };
            this.bumpMapTransform = new Uniform { value = new Matrix3() };
            this.bumpScale = new Uniform() { value = 1 };
        }
    }
    public class UniformNormalmapValues : Uniforms
    {

        public UniformNormalmapValues()
        {
            this.normalMap = new Uniform() { value = null };
            this.normalMapTransform = new Uniform { value = new Matrix3() };
            this.normalScale = new Uniform { value = new Vector2(1, 1) };
        }
    }
    public class UniformDisplacementmapValues : Uniforms
    {

        public UniformDisplacementmapValues()
        {
            this.displacementMap = new Uniform() { value = null };
            this.displacementMapTransform = new Uniform { value = new Matrix3() };
            this.displacementScale = new Uniform() { value = 1.0 };
            this.displacementBias = new Uniform() { value = 0.0 };
        }
    }

    public class UniformEmissiveMapValues : Uniforms
    {
        public UniformEmissiveMapValues()
        {
            this.emissiveMap = new Uniform() { value = null };
            this.emissiveMapTransform = new Uniform { value = new Matrix3() };
        }
    }

    public class UniformMetalnessmapValues : Uniforms
    {
        public UniformMetalnessmapValues()
        {
            this.metalnessMap = new Uniform() { value = null };
            this.metalnessMapTransform = new Uniform { value = new Matrix3() };
        }
    }
    public class UniformRoughnessMapValues : Uniforms
    {
        public UniformRoughnessMapValues()
        {
            this.roughnessMap = new Uniform() { value = null };
            this.roughnessMapTransform = new Uniform { value = new Matrix3() };
        }
    }

    public class UniformGradientmapValues : Uniforms
    {
        public UniformGradientmapValues()
        {
            this.gradientMap = new Uniform() { value = null };
        }
    }
    public class UniformFogValues : Uniforms
    {
        public UniformFogValues()
        {
            this.fogDensity = new Uniform() { value = 0.00025 };
            this.fogNear = new Uniform() { value = 1.0 };
            this.fogFar = new Uniform() { value = 2000.0 };
            this.fogColor = new Uniform() { value = new Color(0xffffff) };
        }
    }
    public class UniformLightsValues : Uniforms
    {
        public UniformLightsValues()
        {
            this.ambientLightColor = new Uniform() { value = new ListEx<Color>() };
            this.lightProbe = new Uniform() { value = new ListEx<object>() };

            this.directionalLights = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                        { "direction",new JsObj<object>()},
                        { "color",new JsObj<object>()}
                    }
            };
            this.directionalLightShadows = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                        { "shadowBias",new JsObj<object>()},
                        { "shadowNormalBias",new JsObj<object>()},
                        { "shadowRadius",new JsObj<object>()},
                        { "shadowMapSize",new JsObj<object>()}
                    }
            };
            this.directionalShadowMap = new Uniform() { value = new ListEx<object>() };
            this.directionalShadowMatrix = new Uniform() { value = new ListEx<object>() };

            this.spotLights = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                        { "color",new JsObj<object>()},
                        { "position",new JsObj<object>()},
                        { "direction",new JsObj<object>()},
                        { "distance",new JsObj<object>()},
                        { "coneCos",new JsObj<object>()},
                        { "penumbraCos",new JsObj<object>()},
                        { "decay",new JsObj<object>()}
                    }
            };
            this.spotLightShadows = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                        { "shadowBias",new JsObj<object>()},
                        { "shadowNormalBias",new JsObj<object>()},
                        { "shadowRadius",new JsObj<object>()},
                        { "shadowMapSize",new JsObj<object>()}
                    }
            };
            this.spotLightMap = new Uniform() { value = new ListEx<object>() };
            this.spotShadowMap = new Uniform() { value = new ListEx<object>() };
            this.spotLightMatrix = new Uniform() { value = new ListEx<object>() };
            this.pointLights = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                        { "color",new JsObj<object>()},
                        { "position",new JsObj<object>()},
                        { "decay",new JsObj<object>()},
                        { "distance",new JsObj<object>()}
                    }
            };
            this.pointLightShadows = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                       { "shadowBias",new JsObj<object>()},
                        { "shadowNormalBias",new JsObj<object>()},
                        { "shadowRadius",new JsObj<object>()},
                        { "shadowMapSize",new JsObj<object>()},
                        { "shadowCameraNear",new JsObj<object>()},
                        { "shadowCameraFar",new JsObj<object>()}
                    }
            };
            this.pointShadowMap = new Uniform() { value = new ListEx<object>() };
            this.pointShadowMatrix = new Uniform() { value = new ListEx<object>() };
            this.hemisphereLights = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                       { "direction",new JsObj<object>()},
                        { "skyColor",new JsObj<object>()},
                        { "groundColor",new JsObj<object>()}

                    }
            };
            this.rectAreaLights = new Uniform()
            {
                value = new ListEx<object>(),
                properties = new JsObj<object>
                    {
                       { "color",new JsObj<object>()},
                        { "position",new JsObj<object>()},
                        { "width",new JsObj<object>()},
                        { "height",new JsObj<object>()}
                    }
            };
            this.ltc_1 = new Uniform() { value = null };
            this.ltc_2 = new Uniform() { value = null };

        }
    }
    public class UniformPointsValues : Uniforms
    {
        public UniformPointsValues()
        {
            this.diffuse = new Uniform() { value = new Color(0xffffff) };
            this.opacity = new Uniform() { value = 1.0 };
            this.size = new Uniform() { value = 1.0 };
            this.scale = new Uniform() { value = 1.0 };
            this.map = new Uniform() { value = null };
            this.alphaMap = new Uniform() { value = null };
            this.alphaTest = new Uniform() { value = 0.0 };
            this.uvTransform = new Uniform() { value = new Matrix3() }; //151
        }
    }
    public class UniformSpriteValues : Uniforms
    {
        public UniformSpriteValues()
        {
            this.diffuse = new Uniform { value = /*@__PURE__*/ new Color(0xffffff) };
            this.opacity = new Uniform { value = 1.0 };
            this.center = new Uniform { value = /*@__PURE__*/ new Vector2(0.5, 0.5) };
            this.rotation = new Uniform { value = 0.0 };
            this.map = new Uniform { value = null };
            this.mapTransform = new Uniform { value = /*@__PURE__*/ new Matrix3() };
            this.alphaMap = new Uniform { value = null };
            this.alphaTest = new Uniform { value = 0 };
        }
    }

    public static partial class UniformsLib
    {
        public static UniformCommonValues common = new UniformCommonValues();
        public static UniformSpecularMapValues specularmap = new UniformSpecularMapValues();
        public static UniformEnvMapValues envmap = new UniformEnvMapValues();
        public static UniformAoMapValues aomap = new UniformAoMapValues();
        public static UniformLightMapValues lightmap = new UniformLightMapValues();
        public static UniformEmissiveMapValues emissivemap = new UniformEmissiveMapValues();
        public static UniformBumpMapValues bumpmap = new UniformBumpMapValues();
        public static UniformNormalmapValues normalmap = new UniformNormalmapValues();
        public static UniformDisplacementmapValues displacementmap = new UniformDisplacementmapValues();
        public static UniformRoughnessMapValues roughnessmap = new UniformRoughnessMapValues();
        public static UniformMetalnessmapValues metalnessmap = new UniformMetalnessmapValues();
        public static UniformGradientmapValues gradientmap = new UniformGradientmapValues();
        public static UniformFogValues fog = new UniformFogValues();
        public static UniformLightsValues lights = new UniformLightsValues();
        public static UniformPointsValues points = new UniformPointsValues();
        public static UniformSpriteValues sprite = new UniformSpriteValues();
        public static DataTexture LTC_FLOAT_1;
        public static DataTexture LTC_FLOAT_2;
    }
}
