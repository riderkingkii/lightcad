﻿//by yf albubo
#if defined(USE_ALBUBO) && defined(Merge_Max_Size)
    vec3 m_position = MergeItemMaterix.position[objIndex];
    vec3 m_scale = MergeItemMaterix.scale[objIndex];
    vec4 m_quater = MergeItemMaterix.quaternion[objIndex];
    mat4 m_trans = compose(m_position, m_quater, m_scale);
    transformed.xyz = (m_trans * vec4(transformed, 1)).xyz;
    vColor.xyz = getColorFromPack(ColorState.packColor[objIndex]).xyz;
#endif
#if defined(USE_PACKCOLOR)
    vColor.xyz = getColorFromPack(packColor).xyz;
#endif