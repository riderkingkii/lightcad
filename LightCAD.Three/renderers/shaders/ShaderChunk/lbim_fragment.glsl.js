
#if defined(_physical) || defined(_phong)

	#ifdef NORMALMAP

normalColor = vec4(packNormalToRGB(normal), 1.0);

	#endif

	#ifdef DEPTHMAP

float invClipZ = 0.5 * vHighPrecisionZW[0] / vHighPrecisionZW[1] + 0.5;

depthlColor = vec4(packDepthToRGB(invClipZ), 1.0);

	#endif

	#ifdef IDMAP

idColor = vec4(uniqueId / 255.0, 1.0);

	#endif

#elif defined(_basic)

	#ifdef NORMALMAP

vec3 fdx = vec3(dFdx(vViewPosition.x), dFdx(vViewPosition.y), dFdx(vViewPosition.z));
vec3 fdy = vec3(dFdy(vViewPosition.x), dFdy(vViewPosition.y), dFdy(vViewPosition.z));
vec3 normal = normalize(cross(fdx, fdy));
normal *= ((step(0.0, dot(normal, vViewPosition)) - 0.5) * 2.0);

normalColor = vec4(normal, opacity);
	#endif

	#ifdef DEPTHMAP

float invClipZ = 0.5 * vHighPrecisionZW[0] / vHighPrecisionZW[1] + 0.5;
depthlColor = vec4(packDepthToRGB(invClipZ), 1.0);	
	#endif

	#ifdef IDMAP

idColor = vec4(uniqueId / 255.0, 1.0);

	#endif

#else

    #ifdef NORMALMAP

normalColor = vec4(0.0, 0.0, 0.0, 0.0);

    #endif

    #ifdef DEPTHMAP

depthlColor = vec4(1.0, 1.0, 1.0, 1.0);

    #endif

	#ifdef IDMAP

idColor = vec4(0.0, 0.0, 0.0, 0.0);

	#endif

#endif
