﻿//by yf albubo 结构
#if defined(USE_ALBUBO) && defined(Merge_Max_Size)

	in int objIndex;
	//因为可能有对4字节传输的对齐要求   实际size为16*3=48byte  使用标准std140布局屏蔽差异
	layout(std140) uniform MergeItemMatrixData {
		vec3[Merge_Max_Size] position;
		vec3[Merge_Max_Size] scale;
		vec4[Merge_Max_Size] quaternion;
	} MergeItemMaterix;

	layout(std140)	uniform ColorStateData {
		float[Merge_Max_Size] packColor;
		uint [Merge_Max_Size] state;
	} ColorState;

	mat4 compose(in vec3 position,in vec4 quaternion,in vec3 scale) {
	mat4 te;

	float x = quaternion.x, y = quaternion.y, z = quaternion.z, w = quaternion.w;
	float x2 = x + x, y2 = y + y, z2 = z + z;
	float xx = x * x2, xy = x * y2, xz = x * z2;
	float yy = y * y2, yz = y * z2, zz = z * z2;
	float wx = w * x2, wy = w * y2, wz = w * z2;

	float sx = scale.x, sy = scale.y, sz = scale.z;

	te[0][0] = (1.0 - (yy + zz)) * sx;
	te[0][1] = (xy + wz) * sx;
	te[0][2] = (xz - wy) * sx;
	te[0][3] = 0.0;

	te[1][0] = (xy - wz) * sy;
	te[1][1] = (1.0 - (xx + zz)) * sy;
	te[1][2] = (yz + wx) * sy;
	te[1][3] = 0.0;

	te[2][0] = (xz + wy) * sz;
	te[2][1] = (yz - wx) * sz;
	te[2][2] = (1.0 - (xx + yy)) * sz;
	te[2][3] = 0.0;

	te[3][0] = position.x;
	te[3][1] = position.y;
	te[3][2] = position.z;
	te[3][3] = 1.0;

	return te;

}
#endif
#if((defined(USE_ALBUBO) && defined(Merge_Max_Size)) || defined(USE_PACKCOLOR))
	vec4 getColorFromPack(float packColor){
		int icolor = int(packColor);
	    float r = float((icolor & 63 * 262144) >> 18) / 63.0;
	    float g = float((icolor & 63 * 4096) >> 12) / 63.0;
	    float b = float((icolor & 63 * 64) >> 6) / 63.0;
	    float a = float(icolor & 63) / 63.0;
		return vec4(r, g, b, a);
}
	in float packColor;
	#if(defined(USE_COLOR_ALPHA) || defined(USE_COLOR))
	#else
		varying vec3 vColor;
	#endif
#endif

