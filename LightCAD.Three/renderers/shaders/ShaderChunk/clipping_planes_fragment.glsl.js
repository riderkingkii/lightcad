
#if NUM_CLIPPING_PLANES > 0

	vec4 plane;
	#if 6 >= NUM_CLIPPING_PLANES
	#pragma unroll_loop_start
	for ( int i = 0; i < UNION_CLIPPING_PLANES; i ++ ) {

		plane = clippingPlanes[ i ];
		if ( dot( vClipPosition, plane.xyz ) > plane.w ) discard;

	}
	#pragma unroll_loop_end
	#endif

#if UNION_CLIPPING_PLANES < NUM_CLIPPING_PLANES && 6 >= NUM_CLIPPING_PLANES

		bool clipped = true;

		#pragma unroll_loop_start
		for ( int i = UNION_CLIPPING_PLANES; i < NUM_CLIPPING_PLANES; i ++ ) {

			plane = clippingPlanes[ i ];
			clipped = ( dot( vClipPosition, plane.xyz ) > plane.w ) && clipped;

		}
		#pragma unroll_loop_end

		if ( clipped ) discard;

	#endif
#if 6 < NUM_CLIPPING_PLANES

#pragma multi_clipbox

#endif
#endif

