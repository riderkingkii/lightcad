using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class DirectionalLightHelper : Object3D
    {
        #region scope properties or methods
        //private static Vector3 _v1 = new Vector3();
        //private static Vector3 _v2 = new Vector3();
        //private static Vector3 _v3 = new Vector3();
        private static DirectionalLightHelperContext getContext()
        {
            return ThreeThreadContext.GetCurrThreadContext().DirectionalLightHelperCtx;
        }
        #endregion

        #region Properties

        public DirectionalLight light;
        public Color color;
        public Line lightPlane;
        public Line targetLine;

        #endregion

        #region constructor
        public DirectionalLightHelper(DirectionalLight light, double size = 1, Color color = null)
        {
            this.light = light;
            this.matrix = light.matrixWorld;
            this.matrixAutoUpdate = false;
            this.color = color;
            this.type = "DirectionalLightHelper";
            var geometry = new BufferGeometry();
            geometry.setAttribute("position", new Float32BufferAttribute(new double[] {
                    -size, size, 0,
                    size, size, 0,
                    size, -size, 0,
                    -size, -size, 0,
                    -size, size, 0
                }, 3));
            var material = new LineBasicMaterial{ fog = false, toneMapped = false };
            this.lightPlane = new Line(geometry, material);
            this.add(this.lightPlane);
            geometry = new BufferGeometry();
            geometry.setAttribute("position", new Float32BufferAttribute(new double[] { 0, 0, 0, 0, 0, 1 }, 3));
            this.targetLine = new Line(geometry, material);
            this.add(this.targetLine);
            this.update();
        }
        #endregion

        #region methods
        public void dispose()
        {
            this.lightPlane.geometry.dispose();
            this.lightPlane.material.dispose();
            this.targetLine.geometry.dispose();
            this.targetLine.material.dispose();
        }
        public void update()
        {
            var ctx = getContext();
            var _v1 = ctx._v1;
            var _v2 = ctx._v2;
            var _v3 = ctx._v3;
            this.light.updateWorldMatrix(true, false);
            this.light.target.updateWorldMatrix(true, false);
            _v1.SetFromMatrixPosition(this.light.matrixWorld);
            _v2.SetFromMatrixPosition(this.light.target.matrixWorld);
            _v3.SubVectors(_v2, _v1);
            this.lightPlane.lookAt(_v2);
            if (this.color != null)
            {
                (this.lightPlane.material as LineBasicMaterial).color.Set(this.color);
                (this.targetLine.material as LineBasicMaterial).color.Set(this.color);
            }
            else
            {
                (this.lightPlane.material as LineBasicMaterial).color.Copy(this.light.color);
                (this.targetLine.material as LineBasicMaterial).color.Copy(this.light.color);
            }
            this.targetLine.lookAt(_v2);
            this.targetLine.scale.Z = _v3.Length();
        }
        #endregion

    }
}
