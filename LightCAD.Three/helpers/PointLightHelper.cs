using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class PointLightHelper : Mesh
    {
        #region Properties

        public PointLight light;
        public Color color;

        #endregion

        #region constructor
        public PointLightHelper(PointLight light, double sphereSize, Color color)
            : base(new SphereGeometry(sphereSize, 4, 2), new MeshBasicMaterial{ wireframe = true, fog = false, toneMapped = false })
        {
            this.light = light;
            this.color = color;
            this.type = "PointLightHelper";
            this.matrix = this.light.matrixWorld;
            this.matrixAutoUpdate = false;
            this.update();
            /*
            // TODO: delete this comment?
            var distanceGeometry = new THREE.IcosahedronGeometry( 1, 2 );
            var distanceMaterial = new THREE.MeshBasicMaterial( { color: hexColor, fog: false, wireframe: true, opacity: 0.1, transparent: true } );
            this.lightSphere = new THREE.Mesh( bulbGeometry, bulbMaterial );
            this.lightDistance = new THREE.Mesh( distanceGeometry, distanceMaterial );
            var d = light.distance;
            if ( d == 0.0 ) {
                this.lightDistance.visible = false;
            } else {
                this.lightDistance.scale.set( d, d, d );
            }
            this.add( this.lightDistance );
            */

        }
        #endregion

        #region methods
        public void dispose()
        {
            this.geometry.dispose();
            this.material.dispose();
        }
        public void update()
        {
            this.light.updateWorldMatrix(true, false);
            if (this.color != null)
            {
                (this.material as MeshBasicMaterial).color.Set(this.color);
            }
            else
            {
                (this.material as MeshBasicMaterial).color.Copy(this.light.color);
            }
            /*
            var d = this.light.distance;
            if ( d == 0.0 ) {
                this.lightDistance.visible = false;
            } else {
                this.lightDistance.visible = true;
                this.lightDistance.scale.set( d, d, d );
            }
            */
        }
        #endregion

    }
}
