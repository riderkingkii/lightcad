﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class RectAreaLightHelper : Line
    {
        public RectAreaLight light;
        public Color color;

        public RectAreaLightHelper(RectAreaLight light, Color color = null) :
            base(makeGeometry(), new LineBasicMaterial{ fog = false })
        {

            this.light = light;
            this.color = color; // optional hardwired color for the helper
            this.type = "RectAreaLightHelper";

            //
            var positions2 = new double[] { 1, 1, 0, -1, 1, 0, -1, -1, 0, 1, 1, 0, -1, -1, 0, 1, -1, 0 };

            var geometry2 = new BufferGeometry();
            geometry2.setAttribute("position", new Float32BufferAttribute(positions2, 3));
            geometry2.computeBoundingSphere();

            this.add(new Mesh(geometry2, new MeshBasicMaterial{ side = BackSide, fog = false }));

        }
        private static BufferGeometry makeGeometry()
        {
            var positions = new double[] { 1, 1, 0, -1, 1, 0, -1, -1, 0, 1, -1, 0, 1, 1, 0 };

            var geometry = new BufferGeometry();
            geometry.setAttribute("position", new Float32BufferAttribute(positions, 3));
            geometry.computeBoundingSphere();
            return geometry;
        }
        public override void updateMatrixWorld(bool force = false)
        {
            this.scale.Set(0.5 * this.light.width, 0.5 * this.light.height, 1);
            var lineMat = this.material as LineBasicMaterial;
            var mesh = this.children[0] as Mesh;
            if (this.color != null)
            {
                lineMat.color.Set(this.color);
                mesh.material.color.Set(this.color);
            }
            else
            {

                lineMat.color.Copy(this.light.color).MultiplyScalar(this.light.intensity);

                // prevent hue shift
                var c = lineMat.color;
                var max = MathEx.Max(c.R, c.G, c.B);
                if (max > 1) c.MultiplyScalar(1 / max);

                mesh.material.color.Copy(lineMat.color);

            }

            // ignore world scale on light
            this.matrixWorld.ExtractRotation(this.light.matrixWorld).Scale(this.scale).CopyPosition(this.light.matrixWorld);

            this.children[0].matrixWorld.Copy(this.matrixWorld);

        }

        public void dispose()
        {
            this.geometry.dispose();
            this.material.dispose();
            (this.children[0] as Mesh).geometry.dispose();
            (this.children[0] as Mesh).material.dispose();
        }
    }
}
