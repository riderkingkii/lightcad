using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class GridHelper : LineSegments
    {
        #region constructor
        public GridHelper(int size = 10, int divisions = 10, int color1 = 0x444444, int color2 = 0x888888)
            : base(makeBuffergeometry(size, divisions, new Color(color1), new Color(color2)), new LineBasicMaterial{ vertexColors = true, toneMapped = false })
        {
            this.type = "GridHelper";
        }

        private static BufferGeometry makeBuffergeometry(int size, int divisions, Color color1, Color color2)
        {
            var center = divisions / 2;
            var step = size / divisions;
            var halfSize = size / 2;
            var vertices = new ListEx<double>();
            var colors = new ListEx<double>();
            for (int i = 0, k = -halfSize; i <= divisions; i++, k += step)
            {
                vertices.Push(-halfSize, 0, k, halfSize, 0, k);
                vertices.Push(k, 0, -halfSize, k, 0, halfSize);
                var color = i == center ? color1 : color2;
                color.ToJsArray(colors);
                color.ToJsArray(colors);
                color.ToJsArray(colors);
                color.ToJsArray(colors);
            }
            var geometry = new BufferGeometry();
            geometry.setAttribute("position", new Float32BufferAttribute(vertices.ToArray(), 3));
            geometry.setAttribute("color", new Float32BufferAttribute(colors.ToArray(), 3));
            return geometry;
        }
        #endregion

        #region methods
        public void dispose()
        {
            this.geometry.dispose();
            this.material.dispose();
        }
        #endregion

    }
}
