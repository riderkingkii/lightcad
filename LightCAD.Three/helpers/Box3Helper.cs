using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class Box3Helper : LineSegments
    {
        #region Properties

        public Box3 box;

        #endregion

        #region constructor
        public Box3Helper(Box3 box, int color = 0xffff00)
            : base(getDefaultGeometry(), new LineBasicMaterial{ color = new Color(color), toneMapped = false })
        {
            this.box = box;
            this.type = "Box3Helper";
            this.geometry.computeBoundingSphere();
        }
        private static BufferGeometry getDefaultGeometry()
        {
            var indices = new int[] { 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7 };
            var positions = new double[] { 1, 1, 1, -1, 1, 1, -1, -1, 1, 1, -1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1 };
            var geometry = new BufferGeometry();
            geometry.setIndex(new BufferAttribute(indices, 1));
            geometry.setAttribute("position", new Float32BufferAttribute(positions, 3));
            return geometry;
        }
        #endregion

        #region methods
        public override void updateMatrixWorld(bool force)
        {
            var box = this.box;
            if (box.IsEmpty()) return;
            box.GetCenter(this.position);
            box.GetSize(this.scale);
            this.scale.MulScalar(0.5);
            base.updateMatrixWorld(force);
        }
        public void dispose()
        {
            this.geometry.dispose();
            this.material.dispose();
        }
        #endregion

    }
}
