using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class PlaneHelper : Line
    {
        #region Properties

        public Plane plane;
        public double size;

        #endregion

        #region constructor
        public PlaneHelper(Plane plane, double size = 1, int color = 0xffff00)
            : base(makeGeometry(), new LineBasicMaterial{ color = new Color(color), toneMapped = false })
        {
            this.type = "PlaneHelper";
            this.plane = plane;
            this.size = size;
            var positions2 = new double[] { 1, 1, 0, -1, 1, 0, -1, -1, 0, 1, 1, 0, -1, -1, 0, 1, -1, 0 };
            var geometry2 = new BufferGeometry();
            geometry2.setAttribute("position", new Float32BufferAttribute(positions2, 3));
            geometry2.computeBoundingSphere();
            this.add(new Mesh(geometry2, new MeshBasicMaterial{ color = new Color(color), opacity = 0.2, transparent = true, depthWrite = false, toneMapped = false }));
        }
        private static BufferGeometry makeGeometry()
        {
            var positions = new double[] { 1, -1, 0, -1, 1, 0, -1, -1, 0, 1, 1, 0, -1, 1, 0, -1, -1, 0, 1, -1, 0, 1, 1, 0 };
            var geometry = new BufferGeometry();
            geometry.setAttribute("position", new Float32BufferAttribute(positions, 3));
            geometry.computeBoundingSphere();
            return geometry;
        }
        #endregion

        #region methods
        public override void updateMatrixWorld(bool force)
        {
            this.position.Set(0, 0, 0);
            this.scale.Set(0.5 * this.size, 0.5 * this.size, 1);
            this.lookAt(this.plane.Normal);
            this.translateZ(-this.plane.Constant);
            base.updateMatrixWorld(force);
        }
        public void dispose()
        {
            this.geometry.dispose();
            this.material.dispose();
            (this.children[0] as Mesh).geometry.dispose();
            (this.children[0] as Mesh).material.dispose();
        }
        #endregion

    }
}
