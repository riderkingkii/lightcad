using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;


namespace LightCAD.Three
{
    public class SkeletonHelper : LineSegments
    {
        #region scope properties or methods
        //private static Vector3 _vector = new Vector3();
        //private static Matrix4 _boneMatrix = new Matrix4();
        //private static Matrix4 _matrixWorldInv = new Matrix4();
        public SkeletonHelperContext getContext()
        {
            return ThreeThreadContext.GetCurrThreadContext().SkeletonHelperCtx;
        }
        private static ListEx<Bone> getBoneList(Object3D _object, ListEx<Bone> boneList = null)
        {
            if (boneList == null)
            {
                boneList = new ListEx<Bone>();
            }

            if (_object is Bone)
            {
                boneList.Push(_object as Bone);
            }

            for (int i = 0; i < _object.children.Length; i++)
            {
                getBoneList(_object.children[i], boneList);
            }
            return boneList;
        }
        #endregion

        #region Properties

        public Object3D root;
        public ListEx<Bone> bones;

        #endregion

        #region constructor
        public SkeletonHelper(Object3D _object)
            : this(makeGeometry(_object))
        {
            this.type = "SkeletonHelper";
            this.root = _object;
            this.matrix = _object.matrixWorld;
            this.matrixAutoUpdate = false;
        }

        public SkeletonHelper(Tuple<BufferGeometry, ListEx<Bone>> result)
            : base(result.Item1, new LineBasicMaterial{ vertexColors = true, depthTest = false, depthWrite = false, toneMapped = false, transparent = true })
        {
            this.bones = result.Item2;
        }

        private static Tuple<BufferGeometry, ListEx<Bone>> makeGeometry(Object3D _object)
        {
            var bones = getBoneList(_object);
            var geometry = new BufferGeometry();
            var vertices = new ListEx<double>();
            var colors = new ListEx<double>();
            var color1 = new Color(0, 0, 1);
            var color2 = new Color(0, 1, 0);
            for (int i = 0; i < bones.Length; i++)
            {
                var bone = bones[i];
                if (bone.parent != null && bone.parent is Bone)
                {
                    vertices.Push(0, 0, 0);
                    vertices.Push(0, 0, 0);
                    colors.Push(color1.R, color1.G, color1.B);
                    colors.Push(color2.R, color2.G, color2.B);
                }
            }
            geometry.setAttribute("position", new Float32BufferAttribute(vertices.ToArray(), 3));
            geometry.setAttribute("color", new Float32BufferAttribute(colors.ToArray(), 3));
            return Tuple.Create(geometry, bones);
        }
        #endregion

        #region methods
        public override void updateMatrixWorld(bool force)
        {
            var ctx = getContext();
            var _matrixWorldInv = ctx._matrixWorldInv;
            var _boneMatrix = ctx._boneMatrix;
            var _vector = ctx._vector;
            var bones = this.bones;
            var geometry = this.geometry;
            var position = geometry.getAttribute("position");
            _matrixWorldInv.Copy(this.root.matrixWorld).Invert();
            for (int i = 0, j = 0; i < bones.Length; i++)
            {
                var bone = bones[i];
                if (bone.parent != null && bone.parent is Bone)
                {
                    _boneMatrix.MultiplyMatrices(_matrixWorldInv, bone.matrixWorld);
                    _vector.SetFromMatrixPosition(_boneMatrix);
                    position.setXYZ(j, _vector.X, _vector.Y, _vector.Z);
                    _boneMatrix.MultiplyMatrices(_matrixWorldInv, bone.parent.matrixWorld);
                    _vector.SetFromMatrixPosition(_boneMatrix);
                    position.setXYZ(j + 1, _vector.X, _vector.Y, _vector.Z);
                    j += 2;
                }
            }
            geometry.getAttribute("position").needsUpdate = true;
            base.updateMatrixWorld(force);
        }
        public void dispose()
        {
            this.geometry.dispose();
            this.material.dispose();
        }
        #endregion

    }
}
