namespace LightCAD.Three
{
    public class AxesHelper : LineSegments
    {
        #region Properties
        #endregion

        #region constructor
        public AxesHelper(double size = 1) : base(getGeometry(size), getMaterial())
        {

            this.type = "AxesHelper";
        }
        private static BufferGeometry getGeometry(double size)
        {
            var vertices = new double[] {
            0, 0, 0, size, 0, 0,
                    0, 0, 0, 0, size, 0,
                    0, 0, 0, 0, 0, size
                };
            var colors = new double[] {
                    1, 0, 0, 1, 0.6, 0,
                    0, 1, 0, 0.6, 1, 0,
                    0, 0, 1, 0, 0.6, 1
                };
            var geometry = new BufferGeometry();
            geometry.setAttribute("position", new Float32BufferAttribute(vertices, 3));
            geometry.setAttribute("color", new Float32BufferAttribute(colors, 3));
            return geometry;
        }
        private static Material getMaterial()
        {
            var material = new LineBasicMaterial{ vertexColors = true, toneMapped = false };
            return material;
        }
        #endregion

        #region methods
        public AxesHelper setColors(object xAxisColor, object yAxisColor, object zAxisColor)
        {
            var color = new Color();
            var array = this.geometry.attributes["color"].array;
            color.Set(xAxisColor);
            color.ToArray(array, 0);
            color.ToArray(array, 3);
            color.Set(yAxisColor);
            color.ToArray(array, 6);
            color.ToArray(array, 9);
            color.Set(zAxisColor);
            color.ToArray(array, 12);
            color.ToArray(array, 15);
            this.geometry.attributes["color"].needsUpdate = true;
            return this;
        }
        public void dispose()
        {
            this.geometry.dispose();
            this.material.dispose();
        }
        #endregion

    }
}
