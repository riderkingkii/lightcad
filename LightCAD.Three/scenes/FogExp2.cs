using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class FogExp2 : IFog
    {
        #region Properties

        public double density;

        #endregion
        public string name { get; set; }
        public Color color { get; set; }
        #region constructor
        public FogExp2(object color, double density = 0.00025)
        {
            this.name = "";
            this.color = new Color(color);
            this.density = density;
        }
        #endregion

        #region methods
        public IFog clone()
        {
            return new FogExp2(this.color, this.density);
        }
        #endregion

    }
}
