using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public interface IFog
    {
        string name { get; set; }
        Color color { get; set; }
        IFog clone();
    }
    public class Fog : IFog
    {
        #region Properties


        public double near;
        public double far;

        #endregion
        public string name { get; set; }

        public Color color { get; set; }
        #region constructor
        public Fog(object color, double near = 1, double far = 1000)
        {
            this.name = "";
            this.color = new Color(color);
            this.near = near;
            this.far = far;
        }
        #endregion

        #region methods
        public IFog clone()
        {
            return new Fog(this.color, this.near, this.far);
        }
        #endregion
    }
}
