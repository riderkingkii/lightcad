using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class Scene : Object3D
    {
        #region Properties
        public ListEx<string> multiRenderTargets = null;
        public object background; //Color | Texture | WebGLCubeRenderTarget;
        public Texture environment;
        public IFog fog;
        public double backgroundBlurriness;
        public double backgroundIntensity;
        public Material overrideMaterial;

        #endregion

        #region constructor
        public Scene()
        {
            this.type = "Scene";
            this.background = null;
            this.environment = null;
            this.fog = null;
            this.backgroundBlurriness = 0;
            this.backgroundIntensity = 1;
            this.overrideMaterial = null;
        }
        #endregion

        #region properties
        public bool autoUpdate
        {
            get
            {
                console.warn("THREE.Scene: autoUpdate was renamed to matrixWorldAutoUpdate in r144.");
                return this.matrixWorldAutoUpdate;
            }
            set
            {
                console.warn("THREE.Scene: autoUpdate was renamed to matrixWorldAutoUpdate in r144.");
                this.matrixWorldAutoUpdate = value;
            }
        }
        #endregion

        #region methods
        public Scene copy(Scene source, bool recursive)
        {
            base.copy(source, recursive);
            if (source.background != null) this.background = source.background.InvokeMethod("clone");
            if (source.environment != null) this.environment = source.environment.Clone();
            if (source.fog != null) this.fog = source.fog.clone();
            this.backgroundBlurriness = source.backgroundBlurriness;
            this.backgroundIntensity = source.backgroundIntensity;
            if (source.overrideMaterial != null) this.overrideMaterial = source.overrideMaterial.clone();
            this.matrixAutoUpdate = source.matrixAutoUpdate;
            return this;
        }
        #endregion
    }
}
