using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class CircleGeometry : BufferGeometry
    {
        public class Parameters
        {
            public double radius;
            public int segments;
            public double thetaStart;
            public double thetaLength;
        }
        #region Properties

        public Parameters parameters;

        #endregion

        #region constructor
        public CircleGeometry(double radius = 1, int segments = 32, double thetaStart = 0, double thetaLength = Math.PI * 2)
        {
            this.type = "CircleGeometry";
            this.parameters = new Parameters()
            {
                radius = radius,
                segments = segments,
                thetaStart = thetaStart,
                thetaLength = thetaLength
            };
            segments = Math.Max(3, segments);
            // buffers
            var indices = new ListEx<int>();
            var vertices = new ListEx<double>();
            var normals = new ListEx<double>();
            var uvs = new ListEx<double>();
            // helper variables
            var vertex = new Vector3();
            var uv = new Vector2();
            // center point
            vertices.Push(0, 0, 0);
            normals.Push(0, 0, 1);
            uvs.Push(0.5, 0.5);
            for (int s = 0, i = 3; s <= segments; s++, i += 3)
            {
                var segment = thetaStart + s / (double)segments * thetaLength;
                // vertex
                vertex.X = radius * Math.Cos(segment);
                vertex.Y = radius * Math.Sin(segment);
                vertices.Push(vertex.X, vertex.Y, vertex.Z);
                // normal
                normals.Push(0, 0, 1);
                // uvs
                uv.X = (vertices[i] / radius + 1) / 2;
                uv.Y = (vertices[i + 1] / radius + 1) / 2;
                uvs.Push(uv.X, uv.Y);
            }
            // indices
            for (int i = 1; i <= segments; i++)
            {
                indices.Push(i, i + 1, 0);
            }
            // build geometry
            this.setIndex(indices.ToArray());
            this.setAttribute("position", new Float32BufferAttribute(vertices.ToArray(), 3));
            this.setAttribute("normal", new Float32BufferAttribute(normals.ToArray(), 3));
            this.setAttribute("uv", new Float32BufferAttribute(uvs.ToArray(), 2));
        }
        #endregion


        #region methods
        public CircleGeometry copy(CircleGeometry source)
        {
            base.copy(source);
            this.parameters = new Parameters()
            {
                radius = source.parameters.radius,
                segments = source.parameters.segments,
                thetaStart = source.parameters.thetaStart,
                thetaLength = source.parameters.thetaLength,
            };
            return this;
        }

        #endregion
    }
}
