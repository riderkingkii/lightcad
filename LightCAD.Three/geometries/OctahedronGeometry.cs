using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class OctahedronGeometry : PolyhedronGeometry
    {
        #region constructor
        public OctahedronGeometry(double radius = 1, int detail = 0)
            : base(makeVertices(), makeIndices(), radius, detail)
        {
            this.type = "OctahedronGeometry";
            this.parameters = new PolyhedronGeometry.Parameters
            {
                radius = radius,
                detail = detail

            };
        }
        private static ListEx<double> makeVertices()
        {
            var t = (1 + Math.Sqrt(5)) / 2;
            var vertices = new ListEx<double>() {
                    1, 0, 0, -1, 0, 0, 0, 1, 0,
                    0, -1, 0, 0, 0, 1, 0, 0, -1
                };
            return vertices;
        }

        private static ListEx<int> makeIndices()
        {
            var indices = new ListEx<int>() {
                    0, 2, 4, 0, 4, 3, 0, 3, 5,
                    0, 5, 2, 1, 2, 5, 1, 5, 3,
                    1, 3, 4, 1, 4, 2
                };
            return indices;
        }
        #endregion
    }
}
