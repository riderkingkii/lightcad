using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class CapsuleGeometry : LatheGeometry
    {
        public class Parameters
        {
            public double radius;
            public double height;
            public int capSegments;
            public int radialSegments;
        }
        #region Properties

        public Parameters parameters;

        #endregion

        #region constructor
        public CapsuleGeometry(double radius = 1, double length = 1, int capSegments = 4, int radialSegments = 8)
            : base(makePath(radius, length).getPoints(capSegments), radialSegments)
        {
            this.type = "CapsuleGeometry";
            this.parameters = new Parameters()
            {
                radius = radius,
                height = length,
                capSegments = capSegments,
                radialSegments = radialSegments,
            };
        }
        private static Path makePath(double radius, double length)
        {
            var path = new Path();
            path.absarc(0, -length / 2, radius, MathEx.PI * 1.5, 0);
            path.absarc(0, length / 2, radius, 0, MathEx.PI * 0.5);
            return path;
        }
        #endregion
    }
}
