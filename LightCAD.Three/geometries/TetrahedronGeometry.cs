using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class TetrahedronGeometry : PolyhedronGeometry
    {
        #region constructor
        public TetrahedronGeometry(double radius = 1, int detail = 0)
            : base(makeVertices(), makeIndices(), radius, detail)
        {
            this.type = "TetrahedronGeometry";
            this.parameters = new PolyhedronGeometry.Parameters()
            {
                radius = radius,
                detail = detail

            };
        }
        private static ListEx<double> makeVertices()
        {
            var t = (1 + Math.Sqrt(5)) / 2;
            var vertices = new ListEx<double>() {
                     1, 1, 1, -1, -1, 1, -1, 1, -1, 1, -1, -1
                };
            return vertices;
        }

        private static ListEx<int> makeIndices()
        {
            var indices = new ListEx<int>() {
                    2, 1, 0, 0, 3, 2, 1, 3, 0, 2, 3, 1
                };
            return indices;
        }
        #endregion

    }
}
