using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class ConeGeometry : CylinderGeometry
    {
        public class Parameters
        {
            public double radius;
            public double height;
            public int radialSegments;
            public int heightSegments;
            public bool openEnded;
            public double thetaStart;
            public double thetaLength;
        }
        #region Properties

        public Parameters parameters;

        #endregion

        #region constructor
        public ConeGeometry(double radius = 1, double height = 1, int radialSegments = 32, int heightSegments = 1, bool openEnded = false, double thetaStart = 0, double thetaLength = Math.PI * 2)
            : base(0, radius, height, radialSegments, heightSegments, openEnded, thetaStart, thetaLength)
        {
            this.type = "ConeGeometry";
            this.parameters = new Parameters()
            {
                radius = radius,
                height = height,
                radialSegments = radialSegments,
                heightSegments = heightSegments,
                openEnded = openEnded,
                thetaStart = thetaStart,
                thetaLength = thetaLength

            };
        }
        #endregion

    }
}
