using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class PlaneGeometry : BufferGeometry
    {
        public class Parameters
        {
            public double width;
            public double height;
            public int widthSegments;
            public int heightSegments;
        }
        #region Properties

        public Parameters parameters;

        #endregion

        #region constructor
        public PlaneGeometry(double width = 1, double height = 1, int widthSegments = 1, int heightSegments = 1) : base()
        {

            this.type = "PlaneGeometry";
            this.parameters = new Parameters
            {
                width = width,
                height = height,
                widthSegments = widthSegments,
                heightSegments = heightSegments

            };
            var width_half = width / 2;
            var height_half = height / 2;
            var gridX = widthSegments;
            var gridY = heightSegments;
            var gridX1 = gridX + 1;
            var gridY1 = gridY + 1;
            var segment_width = width / gridX;
            var segment_height = height / gridY;
            //
            var indices = new ListEx<int>();
            var vertices = new ListEx<double>();
            var normals = new ListEx<double>();
            var uvs = new ListEx<double>();
            for (int iy = 0; iy < gridY1; iy++)
            {
                var y = iy * segment_height - height_half;
                for (int ix = 0; ix < gridX1; ix++)
                {
                    var x = ix * segment_width - width_half;
                    vertices.Push(x, -y, 0);
                    normals.Push(0, 0, 1);
                    uvs.Push(ix / (double)gridX);
                    uvs.Push(1 - (iy / (double)gridY));
                }
            }
            for (int iy = 0; iy < gridY; iy++)
            {
                for (int ix = 0; ix < gridX; ix++)
                {
                    var a = ix + gridX1 * iy;
                    var b = ix + gridX1 * (iy + 1);
                    var c = (ix + 1) + gridX1 * (iy + 1);
                    var d = (ix + 1) + gridX1 * iy;
                    indices.Push(a, b, d);
                    indices.Push(b, c, d);
                }
            }
            this.setIndex(indices.ToArray());
            this.setAttribute("position", new Float32BufferAttribute(vertices.ToArray(), 3));
            this.setAttribute("normal", new Float32BufferAttribute(normals.ToArray(), 3));
            this.setAttribute("uv", new Float32BufferAttribute(uvs.ToArray(), 2));
        }
        #endregion

        #region methods
        public PlaneGeometry copy(PlaneGeometry source)
        {
            base.copy(source);
            this.parameters = new Parameters()
            {
                width = source.parameters.width,
                height = source.parameters.height,
                widthSegments = source.parameters.widthSegments,
                heightSegments = source.parameters.heightSegments,
            };
            return this;
        }
        #endregion

    }
}
