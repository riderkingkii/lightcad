﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public class TextGeometry : ExtrudeGeometry
    {
        public class TextExtrudeOption : ExtrudeOption
        {
            public Font font;
            public double height = 50;
            public int size;
        }
        public TextGeometry(string text, TextExtrudeOption parameters)
        {
            init(text, parameters);
            this.type = "TextGeometry";
        }
        private void init(string text, TextExtrudeOption parameters)
        {
            var font = parameters.font;

            if (font == null)
            {
                base.init(null, null); // generate default extrude geometry
            }
            else
            {
                var shapes = font.generateShapes(text, parameters.size);

                // translate parameters to ExtrudeGeometry API

                parameters.depth = parameters.height;

                // defaults

                if (parameters.bevelThickness == null) parameters.bevelThickness = 10;
                if (parameters.bevelSize == null) parameters.bevelSize = 8;
                if (parameters.bevelEnabled == null) parameters.bevelEnabled = false;
                base.init(shapes, parameters);
            }
        }
    }
}
