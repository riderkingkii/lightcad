using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class CubeTexture : Texture
    {
        #region constructor
        public CubeTexture(ListEx<Image> images = null,
                            int mapping = CubeReflectionMapping,
                            int wrapS = ClampToEdgeWrapping,
                            int wrapT = ClampToEdgeWrapping,
                            int magFilter = LinearFilter,
                            int minFilter = LinearMipmapLinearFilter,
                            int format = RGBAFormat,
                            int type = UnsignedByteType,
                            double anisotropy = Texture.DEFAULT_ANISOTROPY,
                            int encoding = LinearEncoding)
            : base(images, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy, encoding)
        {
            if (images == null || this.images.Count <= 1)
            {
                this.images = new ListEx<IImage>(6);
            }
            this.flipY = false;
        }
        #endregion
    }
}
