using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class DataArrayTexture : Texture
    {
        #region Properties

        public int wrapR;

        #endregion

        #region constructor
        public DataArrayTexture(Array data = null, int width = 1, int height = 1, int depth = 1)
            : base(null)
        {
            this.image = new Image { data = data, width = width, height = height, depth = depth };
            this.magFilter = NearestFilter;
            this.minFilter = NearestFilter;
            this.wrapR = ClampToEdgeWrapping;
            this.generateMipmaps = false;
            this.flipY = false;
            this.unpackAlignment = 1;
        }
        #endregion

    }
}
