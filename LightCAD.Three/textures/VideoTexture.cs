using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class VideoTexture : Texture
    {
        #region constructor
        public VideoTexture(Image video = Texture.DEFAULT_IMAGE,
                        int mapping = Texture.DEFAULT_MAPPING,
                        int wrapS = ClampToEdgeWrapping,
                        int wrapT = ClampToEdgeWrapping,
                        int magFilter = LinearFilter,
                        int minFilter = LinearFilter,
                        int format = RGBAFormat,
                        int type = UnsignedByteType,
                        double anisotropy = Texture.DEFAULT_ANISOTROPY,
                        int encoding = LinearEncoding)
            : base(video, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy)
        {
            this.generateMipmaps = false;
        }
        #endregion

        #region methods
        public void update()
        {
            var video = this.image;
            //if (hasVideoFrameCallback == false && video.readyState >= video.HAVE_CURRENT_DATA)
            {
                this.needsUpdate = true;
            }
        }
        #endregion
    }
}
