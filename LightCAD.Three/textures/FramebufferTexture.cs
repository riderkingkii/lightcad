using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class FramebufferTexture : Texture
    {

        #region constructor
        public FramebufferTexture(int width, int height, int format)
            : base(FramebufferTexture.CreateImage(width, height))
        {
            this.format = format;
            this.magFilter = NearestFilter;
            this.minFilter = NearestFilter;
            this.generateMipmaps = false;
            this.needsUpdate = true;
        }
        #endregion

        public static Image CreateImage(int width, int height)
        {
            return new Image()
            {
                width = width,
                height = height,
                complete = true
            };
        }

    }
}
