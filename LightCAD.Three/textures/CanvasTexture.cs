using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class CanvasTexture : Texture
    {
        #region constructor
        public CanvasTexture(object canvas,
                           int mapping = Texture.DEFAULT_MAPPING,
                            int wrapS = ClampToEdgeWrapping,
                            int wrapT = ClampToEdgeWrapping,
                            int magFilter = LinearFilter,
                            int minFilter = LinearMipmapLinearFilter,
                            int format = RGBAFormat,
                            int type = UnsignedByteType,
                            double anisotropy = Texture.DEFAULT_ANISOTROPY)
            : base(canvas, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy)
        {
            this.needsUpdate = true;
        }
        #endregion
    }
}
