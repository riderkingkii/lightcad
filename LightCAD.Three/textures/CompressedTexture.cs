using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class CompressedTexture : Texture
    {
        #region constructor
        public CompressedTexture(ListEx<Image> mipmaps, int width, int height,
                                int format = RGBAFormat,
                                int type = UnsignedByteType,
                                int mapping = Texture.DEFAULT_MAPPING,
                                int wrapS = ClampToEdgeWrapping,
                                int wrapT = ClampToEdgeWrapping,
                                int magFilter = LinearFilter,
                                int minFilter = LinearMipmapLinearFilter,
                                double anisotropy = Texture.DEFAULT_ANISOTROPY,
                                int encoding = LinearEncoding)
            : base(null, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy, encoding)
        {
            this.image = new Image { width = width, height = height, complete = true };
            this.mipmaps = mipmaps;
            // no flipping for cube textures
            // (also flipping doesn"t work for compressed textures )
            this.flipY = false;
            // can"t generate mipmaps for compressed textures
            // mips must be embedded in DDS files
            this.generateMipmaps = false;
        }
        #endregion

        public override Texture Clone()
        {
            var texture = new CompressedTexture(this.mipmaps, this.image.width, this.image.height).copy(this);
            texture.flipY = this.flipY;
            texture.generateMipmaps = this.generateMipmaps;
            return texture;
        }
    }
}
