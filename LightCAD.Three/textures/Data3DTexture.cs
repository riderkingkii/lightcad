using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class Data3DTexture : Texture
    {
        #region Properties

        public int wrapR;

        #endregion

        #region constructor
        public Data3DTexture(byte[] data = null, int width = 1, int height = 1, int depth = 1)
            : base(null)
        {
            // We"re going to add .setXXX() methods for setting properties later.
            // Users can still set in DataTexture3D directly.
            //
            //	var texture = new THREE.DataTexture3D( data, width, height, depth );
            // 	texture.anisotropy = 16;
            //
            // See #14839

            this.image = new Image { data = data, width = width, height = height, depth = depth, complete = true };
            this.magFilter = NearestFilter;
            this.minFilter = NearestFilter;
            this.wrapR = ClampToEdgeWrapping;
            this.generateMipmaps = false;
            this.flipY = false;
            this.unpackAlignment = 1;
        }
        #endregion

    }
}
