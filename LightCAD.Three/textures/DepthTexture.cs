using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class DepthTexture : Texture
    {

        #region constructor
        public DepthTexture(int width = 0, int height = 0,
                            int type = UnsignedByteType,
                            int mapping = Texture.DEFAULT_MAPPING,
                            int wrapS = ClampToEdgeWrapping,
                            int wrapT = ClampToEdgeWrapping,
                            int magFilter = NearestFilter,
                            int minFilter = NearestFilter,
                            double anisotropy = Texture.DEFAULT_ANISOTROPY,
                            int format = DepthFormat)
            : base(null, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy)
        {
            this.image = new Image { width = width, height = height, complete = true };
            this.flipY = false;
            this.generateMipmaps = false;
        }
        #endregion

        public override Texture Clone()
        {
            return new DepthTexture().copy(this);
        }
    }
}
