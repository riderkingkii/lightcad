using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class DataTexture : Texture
    {
        #region constructor
        public DataTexture(Array data = null,
                            int width = 1,
                            int height = 1,
                            int format = RGBAFormat,
                            int type = UnsignedByteType,
                            int mapping = Texture.DEFAULT_MAPPING,
                            int wrapS = ClampToEdgeWrapping,
                            int wrapT = ClampToEdgeWrapping,
                            int magFilter = NearestFilter,
                            int minFilter = NearestFilter,
                            double anisotropy = Texture.DEFAULT_ANISOTROPY,
                            int encoding = LinearEncoding)
            : base(null, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy, encoding)
        {
            this.image = new Image { data = data, width = width, height = height, complete = true };
            this.generateMipmaps = false;
            this.flipY = false;
            this.unpackAlignment = 1;
        }
        #endregion

    }
}
