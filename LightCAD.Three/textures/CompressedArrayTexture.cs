using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class CompressedArrayTexture : CompressedTexture
    {
        #region Properties

        public object wrapR;

        #endregion

        #region constructor
        public CompressedArrayTexture(ListEx<Image> mipmaps,
                                        int width,
                                        int height,
                                        int depth,
                                        int format = RGBAFormat,
                                        int type = UnsignedByteType)
            : base(mipmaps, width, height, format, type)
        {
            this.image.depth = depth;
            this.wrapR = ClampToEdgeWrapping;
        }
        #endregion

    }
}
