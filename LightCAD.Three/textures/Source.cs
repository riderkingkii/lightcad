using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class Source
    {
        #region Properties

        public string uuid;
        public ListEx<IImage> data;
        public int version;

        #endregion

        #region constructor
        public Source(params IImage[] data)
        {
            this.uuid = MathEx.GenerateUUID();
            this.data = data.ToListEx();
            this.version = 0;
        }
        #endregion

        #region properties
        public bool needsUpdate
        {
            set
            {
                if (value) this.version++;
            }
        }
        #endregion
    }
}
