using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.MathEx;

namespace LightCAD.Three
{
    public class InstancedBufferGeometry : BufferGeometry
    {
        #region Properties

        public int instanceCount;
        public int _maxInstanceCount = 0;

        #endregion

        #region constructor
        public InstancedBufferGeometry() : base()
        {
            this.type = "InstancedBufferGeometry";
            this.instanceCount = Infinity;
        }
        #endregion

        #region methods
        public InstancedBufferGeometry copy(InstancedBufferGeometry source)
        {
            base.copy(source);
            this.instanceCount = source.instanceCount;
            return this;
        }
        #endregion

    }
}
