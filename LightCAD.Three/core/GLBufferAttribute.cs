using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class GLBufferAttribute : BufferAttribute
    {
        #region Properties

        public int buffer;
        public int type;
        public int elementSize;

        #endregion

        #region constructor
        public GLBufferAttribute(int buffer, int type, int itemSize, int elementSize, int count)
        {
            this.name = "";
            this.buffer = buffer;
            this.type = type;
            this.itemSize = itemSize;
            this.elementSize = elementSize;
            this.count = count;
            this.version = 0;
        }
        #endregion

        #region properties

        #endregion

        #region methods
        public GLBufferAttribute setBuffer(int buffer)
        {
            this.buffer = buffer;
            return this;
        }
        public GLBufferAttribute setType(int type, int elementSize)
        {
            this.type = type;
            this.elementSize = elementSize;
            return this;
        }
        public GLBufferAttribute setItemSize(int itemSize)
        {
            this.itemSize = itemSize;
            return this;
        }
        public GLBufferAttribute setCount(int count)
        {
            this.count = count;
            return this;
        }
        #endregion

    }
}
