using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace LightCAD.Three
{
    public interface IUniform
    {
        string id { get; }
        void setValue(object v, WebGLTextures textures);
    }

    public class Uniform
    {
        public object value;
        public JsObj<object> properties;
        public bool? needsUpdate;
        public int __offset;
        public IList __data;
        public bool checkUBOCahce = true;//by yf 是否用UBO缓存判断刷新数据是否更改，如果false 就只通过needsUpdate判断
        public static Dictionary<string, FieldInfo> fields = typeof(Uniform).GetFields().ToDictionary(f => f.Name, f => f);
        public Uniform()
        {
        }
        public Uniform(object value, bool needsUpdate = true)
        {
            this.value = value;
            this.needsUpdate = needsUpdate;
        }
        public Uniform clone()
        {
            if (this.value == null)
                return null;
            if (this.value.HasMethod("clone"))
                return new Uniform(this.value.InvokeMethod("clone"));
            else
                return new Uniform(this.value);

        }
    }

    public class Uniforms : JsObj<Uniform>
    {
        public Uniform mapTransform { get => this["mapTransform"]; set { this["mapTransform"] = value; } }
        public Uniform alphaMapTransform { get => this["alphaMapTransform"]; set { this["alphaMapTransform"] = value; } }
        public Uniform bumpMapTransform { get => this["bumpMapTransform"]; set { this["bumpMapTransform"] = value; } }
        public Uniform normalMapTransform { get => this["normalMapTransform"]; set { this["normalMapTransform"] = value; } }
        public Uniform displacementMapTransform { get => this["displacementMapTransform"]; set { this["displacementMapTransform"] = value; } }
        public Uniform emissiveMapTransform { get => this["emissiveMapTransform"]; set { this["emissiveMapTransform"] = value; } }
        public Uniform specularMapTransform { get => this["specularMapTransform"]; set { this["specularMapTransform"] = value; } }
        public Uniform lightMapTransform { get => this["lightMapTransform"]; set { this["lightMapTransform"] = value; } }
        public Uniform aoMapTransform { get => this["aoMapTransform"]; set { this["aoMapTransform"] = value; } }
        public Uniform metalnessMapTransform { get => this["metalnessMapTransform"]; set { this["metalnessMapTransform"] = value; } }
        public Uniform roughnessMapTransform { get => this["roughnessMapTransform"]; set { this["roughnessMapTransform"] = value; } }
        public Uniform sheenColorMapTransform { get => this["sheenColorMapTransform"]; set { this["sheenColorMapTransform"] = value; } }
        public Uniform sheenRoughnessMapTransform { get => this["sheenRoughnessMapTransform"]; set { this["sheenRoughnessMapTransform"] = value; } }
        public Uniform clearcoatMapTransform { get => this["clearcoatMapTransform"]; set { this["clearcoatMapTransform"] = value; } }
        public Uniform clearcoatRoughnessMapTransform { get => this["clearcoatRoughnessMapTransform"]; set { this["clearcoatRoughnessMapTransform"] = value; } }
        public Uniform clearcoatNormalMapTransform { get => this["clearcoatNormalMapTransform"]; set { this["clearcoatNormalMapTransform"] = value; } }
        public Uniform iridescenceMapTransform { get => this["iridescenceMapTransform"]; set { this["iridescenceMapTransform"] = value; } }
        public Uniform iridescenceThicknessMapTransform { get => this["iridescenceThicknessMapTransform"]; set { this["iridescenceThicknessMapTransform"] = value; } }
        public Uniform transmissionMapTransform { get => this["transmissionMapTransform"]; set { this["transmissionMapTransform"] = value; } }
        public Uniform thicknessMapTransform { get => this["thicknessMapTransform"]; set { this["thicknessMapTransform"] = value; } }
        public Uniform specularColorMapTransform { get => this["specularColorMapTransform"]; set { this["specularColorMapTransform"] = value; } }
        public Uniform specularIntensityMapTransform { get => this["specularIntensityMapTransform"]; set { this["specularIntensityMapTransform"] = value; } }



        //common
        public Uniform diffuse { get => this["diffuse"]; set { this["diffuse"] = value; } }
        public Uniform opacity { get => this["opacity"]; set { this["opacity"] = value; } }
        public Uniform map { get => this["map"]; set { this["map"] = value; } }
        public Uniform uvTransform { get => this["uvTransform"]; set { this["uvTransform"] = value; } }
        public Uniform uv2Transform { get => this["uv2Transform"]; set { this["uv2Transform"] = value; } }
        public Uniform alphaMap { get => this["alphaMap"]; set { this["alphaMap"] = value; } }
        public Uniform alphaTest { get => this["alphaTest"]; set { this["alphaTest"] = value; } }

        //specular
        public Uniform specularMap { get => this["specularMap"]; set { this["specularMap"] = value; } }

        //env
        public Uniform envMap
        {
            get => this["envMap"];
            set
            {
                this["envMap"] = value;
            }
        }
        public Uniform flipEnvMap { get => this["flipEnvMap"]; set { this["flipEnvMap"] = value; } }
        public Uniform reflectivity { get => this["reflectivity"]; set { this["reflectivity"] = value; } }
        public Uniform ior { get => this["ior"]; set { this["ior"] = value; } }
        public Uniform refractionRatio { get => this["refractionRatio"]; set { this["refractionRatio"] = value; } }

        //ao
        public Uniform aoMap { get => this["aoMap"]; set { this["aoMap"] = value; } }
        public Uniform aoMapIntensity { get => this["aoMapIntensity"]; set { this["aoMapIntensity"] = value; } }


        public Uniform lightMap { get => this["lightMap"]; set { this["lightMap"] = value; } }
        public Uniform lightMapIntensity { get => this["lightMapIntensity"]; set { this["lightMapIntensity"] = value; } }

        public Uniform emissiveMap { get => this["emissiveMap"]; set { this["emissiveMap"] = value; } }

        //bump
        public Uniform bumpMap { get => this["bumpMap"]; set { this["bumpMap"] = value; } }
        public Uniform bumpScale { get => this["bumpScale"]; set { this["bumpScale"] = value; } }

        //normal
        public Uniform normalMap { get => this["normalMap"]; set { this["normalMap"] = value; } }
        public Uniform normalScale { get => this["normalScale"]; set { this["normalScale"] = value; } }


        public Uniform displacementMap { get => this["displacementMap"]; set { this["displacementMap"] = value; } }
        public Uniform displacementScale { get => this["displacementScale"]; set { this["displacementScale"] = value; } }
        public Uniform displacementBias { get => this["displacementBias"]; set { this["displacementBias"] = value; } }


        public Uniform gradientMap { get => this["gradientMap"]; set { this["gradientMap"] = value; } }
        public Uniform metalnessMap { get => this["metalnessMap"]; set { this["metalnessMap"] = value; } }
        public Uniform roughnessMap { get => this["roughnessMap"]; set { this["roughnessMap"] = value; } }


        public Uniform scale { get => this["scale"]; set { this["scale"] = value; } }
        public Uniform color { get => this["color"]; set { this["color"] = value; } }
        public Uniform size { get => this["size"]; set { this["size"] = value; } }
        public Uniform emissive { get => this["emissive"]; set { this["emissive"] = value; } }
        public Uniform specular { get => this["specular"]; set { this["specular"] = value; } }
        public Uniform shininess { get => this["shininess"]; set { this["shininess"] = value; } }
        public Uniform roughness { get => this["roughness"]; set { this["roughness"] = value; } }
        public Uniform metalness { get => this["metalness"]; set { this["metalness"] = value; } }
        public Uniform envMapIntensity { get => this["envMapIntensity"]; set { this["envMapIntensity"] = value; } }
        public Uniform sheenColor { get => this["sheenColor"]; set { this["sheenColor"] = value; } }
        public Uniform rotation { get => this["rotation"]; set { this["rotation"] = value; } }
        public Uniform sheenRoughness { get => this["sheenRoughness"]; set { this["sheenRoughness"] = value; } }
        public Uniform sheenColorMap { get => this["sheenColorMap"]; set { this["sheenColorMap"] = value; } }
        public Uniform sheenRoughnessMap { get => this["sheenRoughnessMap"]; set { this["sheenRoughnessMap"] = value; } }
        public Uniform clearcoat { get => this["clearcoat"]; set { this["clearcoat"] = value; } }
        public Uniform clearcoatRoughness { get => this["clearcoatRoughness"]; set { this["clearcoatRoughness"] = value; } }
        public Uniform clearcoatMap { get => this["clearcoatMap"]; set { this["clearcoatMap"] = value; } }
        public Uniform clearcoatRoughnessMap { get => this["clearcoatRoughnessMap"]; set { this["clearcoatRoughnessMap"] = value; } }
        public Uniform clearcoatNormalScale { get => this["clearcoatNormalScale"]; set { this["clearcoatNormalScale"] = value; } }
        public Uniform clearcoatNormalMap { get => this["clearcoatNormalMap"]; set { this["clearcoatNormalMap"] = value; } }
        public Uniform iridescence { get => this["iridescence"]; set { this["iridescence"] = value; } }
        public Uniform iridescenceIOR { get => this["iridescenceIOR"]; set { this["iridescenceIOR"] = value; } }
        public Uniform iridescenceThicknessMinimum { get => this["iridescenceThicknessMinimum"]; set { this["iridescenceThicknessMinimum"] = value; } }
        public Uniform iridescenceThicknessMaximum { get => this["iridescenceThicknessMaximum"]; set { this["iridescenceThicknessMaximum"] = value; } }
        public Uniform iridescenceMap { get => this["iridescenceMap"]; set { this["iridescenceMap"] = value; } }
        public Uniform iridescenceThicknessMap { get => this["iridescenceThicknessMap"]; set { this["iridescenceThicknessMap"] = value; } }
        public Uniform transmission { get => this["transmission"]; set { this["transmission"] = value; } }
        public Uniform transmissionSamplerMap { get => this["transmissionSamplerMap"]; set { this["transmissionSamplerMap"] = value; } }
        public Uniform transmissionSamplerSize { get => this["transmissionSamplerSize"]; set { this["transmissionSamplerSize"] = value; } }
        public Uniform transmissionMap { get => this["transmissionMap"]; set { this["transmissionMap"] = value; } }
        public Uniform thickness { get => this["thickness"]; set { this["thickness"] = value; } }
        public Uniform thicknessMap { get => this["thicknessMap"]; set { this["thicknessMap"] = value; } }
        public Uniform attenuationDistance { get => this["attenuationDistance"]; set { this["attenuationDistance"] = value; } }
        public Uniform attenuationColor { get => this["attenuationColor"]; set { this["attenuationColor"] = value; } }
        public Uniform specularIntensity { get => this["specularIntensity"]; set { this["specularIntensity"] = value; } }
        public Uniform specularColor { get => this["specularColor"]; set { this["specularColor"] = value; } }
        public Uniform specularIntensityMap { get => this["specularIntensityMap"]; set { this["specularIntensityMap"] = value; } }
        public Uniform specularColorMap { get => this["specularColorMap"]; set { this["specularColorMap"] = value; } }
        public Uniform matcap { get => this["matcap"]; set { this["matcap"] = value; } }
        public Uniform referencePosition { get => this["referencePosition"]; set { this["referencePosition"] = value; } }
        public Uniform nearDistance { get => this["nearDistance"]; set { this["nearDistance"] = value; } }
        public Uniform farDistance { get => this["farDistance"]; set { this["farDistance"] = value; } }


        //lights
        public Uniform ambientLightColor { get => this["ambientLightColor"]; set { this["ambientLightColor"] = value; } }
        public Uniform lightProbe { get => this["lightProbe"]; set { this["lightProbe"] = value; } }
        public Uniform directionalLights { get => this["directionalLights"]; set { this["directionalLights"] = value; } }
        public Uniform directionalLightShadows { get => this["directionalLightShadows"]; set { this["directionalLightShadows"] = value; } }
        public Uniform directionalShadowMap { get => this["directionalShadowMap"]; set { this["directionalShadowMap"] = value; } }
        public Uniform directionalShadowMatrix { get => this["directionalShadowMatrix"]; set { this["directionalShadowMatrix"] = value; } }
        public Uniform spotLights { get => this["spotLights"]; set { this["spotLights"] = value; } }
        public Uniform spotLightShadows { get => this["spotLightShadows"]; set { this["spotLightShadows"] = value; } }
        public Uniform spotLightMap { get => this["spotLightMap"]; set { this["spotLightMap"] = value; } }
        public Uniform spotShadowMap { get => this["spotShadowMap"]; set { this["spotShadowMap"] = value; } }
        public Uniform spotLightMatrix { get => this["spotLightMatrix"]; set { this["spotLightMatrix"] = value; } }
        public Uniform pointLights { get => this["pointLights"]; set { this["pointLights"] = value; } }
        public Uniform pointLightShadows { get => this["pointLightShadows"]; set { this["pointLightShadows"] = value; } }
        public Uniform pointShadowMap { get => this["pointShadowMap"]; set { this["pointShadowMap"] = value; } }
        public Uniform pointShadowMatrix { get => this["pointShadowMatrix"]; set { this["pointShadowMatrix"] = value; } }
        public Uniform hemisphereLights { get => this["hemisphereLights"]; set { this["hemisphereLights"] = value; } }
        public Uniform rectAreaLights { get => this["rectAreaLights"]; set { this["rectAreaLights"] = value; } }
        public Uniform ltc_1 { get => this["ltc_1"]; set { this["ltc_1"] = value; } }
        public Uniform ltc_2 { get => this["ltc_2"]; set { this["ltc_2"] = value; } }

        //fog
        public Uniform fogDensity { get => this["fogDensity"]; set { this["fogDensity"] = value; } }
        public Uniform fogNear { get => this["fogNear"]; set { this["fogNear"] = value; } }
        public Uniform fogFar { get => this["fogFar"]; set { this["fogFar"] = value; } }
        public Uniform fogColor { get => this["fogColor"]; set { this["fogColor"] = value; } }

        public Uniform center { get => this["center"]; set { this["center"] = value; } }
    }
}


