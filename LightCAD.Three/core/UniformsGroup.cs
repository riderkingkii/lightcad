using LightCAD.MathLib;
using LightCAD.Three.OpenGL;
using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class UniformsGroup : EventDispatcher, IDispose
    {
        /// <summary>
        /// 多线程的场景有问题
        /// </summary>
        private static int staticId;

        #region Properties
        public string name;
        public int usage;
        public int __size;
        public ListEx<Uniform> uniforms;
        public readonly int id;
        public int? __bindingPointIndex;
        public JsObj<int, object> __cache;
        #endregion

        #region constructor
        public UniformsGroup() : base()
        {
            this.id = UniformsGroup.staticId++;
            this.name = "";
            this.usage = gl.STATIC_DRAW;
            this.uniforms = new ListEx<Uniform>();
        }
        #endregion

        #region methods
        public UniformsGroup add(Uniform uniform)
        {
            this.uniforms.Push(uniform);
            return this;
        }
        public UniformsGroup remove(Uniform uniform)
        {
            int index = this.uniforms.IndexOf(uniform);
            if (index != -1) this.uniforms.Splice(index, 1);
            return this;
        }
        public UniformsGroup setName(string name)
        {
            this.name = name;
            return this;
        }
        public UniformsGroup setUsage(int value)
        {
            this.usage = value;
            return this;
        }
        public void dispose()
        {
            this.dispatchEvent(new EventArgs { type = "dispose" });
        }
        public UniformsGroup copy(UniformsGroup source)
        {
            this.name = source.name;
            this.usage = source.usage;
            ListEx<Uniform> uniformsSource = source.uniforms;
            this.uniforms.Clear();
            for (int i = 0, l = uniformsSource.Length; i < l; i++)
            {
                this.uniforms.Push(uniformsSource[i].clone());
            }
            return this;
        }
        public UniformsGroup clone()
        {
            return new UniformsGroup().copy(this);
        }
        #endregion

    }
}
