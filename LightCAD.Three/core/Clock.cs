using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class Clock
    {
        #region scope properties or methods
        private static double now()
        {
            return (DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds; // see #10732

        }
        #endregion

        #region Properties

        public bool autoStart;
        public double startTime;
        public double oldTime;
        public double elapsedTime;
        public bool running;

        #endregion

        #region constructor
        public Clock(bool autoStart = true)
        {
            this.autoStart = autoStart;
            this.startTime = 0;
            this.oldTime = 0;
            this.elapsedTime = 0;
            this.running = false;
        }
        #endregion

        #region methods
        public void start()
        {
            this.startTime = now();
            this.oldTime = this.startTime;
            this.elapsedTime = 0;
            this.running = true;
        }
        public void stop()
        {
            this.getElapsedTime();
            this.running = false;
            this.autoStart = false;
        }
        public double getElapsedTime()
        {
            this.getDelta();
            return this.elapsedTime;
        }
        public double getDelta()
        {
            double diff = 0;
            if (this.autoStart && !this.running)
            {
                this.start();
                return 0;
            }
            if (this.running)
            {
                var newTime = now();
                diff = (newTime - this.oldTime) / 1000;
                this.oldTime = newTime;
                this.elapsedTime += diff;
            }
            return diff;
        }
        #endregion

    }
}
