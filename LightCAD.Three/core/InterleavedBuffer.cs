using LightCAD.Three.OpenGL;
using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class InterleavedBuffer : Float32BufferAttribute
    {
        #region Properties

        public int stride;
        public string uuid;

        #endregion

        #region constructor
        public InterleavedBuffer(Array array = null, int stride = 0)
        {
            this.arrObj = array;
            this.stride = stride;
            this.count = array != null ? array.Length / stride : 0;
            this.usage = gl.STATIC_DRAW;
            this.updateRange = new UpdateRange() { offset = 0, count = -1 };
            this.version = 0;
            this.uuid = MathEx.GenerateUUID();
        }
        #endregion

        #region properties
        public override bool needsUpdate
        {
            set
            {
                if (value) this.version++;
            }
        }
        #endregion

        #region methods

        public override BufferAttribute setUsage(int value)
        {
            this.usage = value;
            return this;
        }
        public override BufferAttribute copy(BufferAttribute source)
        {
            this.arrObj = Array.CreateInstance(source.arrObj.GetType(), source.array.Length);
            Array.Copy(source.arrObj, this.arrObj, this.arrObj.Length);
            this.count = source.count;
            this.stride = (source as InterleavedBuffer).stride;
            this.usage = source.usage;
            return this;
        }
        public override BufferAttribute copyAt(int index1, BufferAttribute buffer, int index2)
        {
            index1 *= this.stride;
            index2 *= (buffer as InterleavedBuffer).stride;
            var arr = this.arrObj as IList;
            var bArr = buffer.arrObj as IList;
            for (int i = 0, l = this.stride; i < l; i++)
            {
                arr[index1 + i] = bArr[index2 + i];
            }
            return this;
        }
        public override BufferAttribute set(Array value, int offset = 0)
        {
            this.arrObj.set(value, offset);
            return this;
        }
        public override BufferAttribute clone()
        {
            var newArray = this.arrObj.Clone() as Array;
            var ib = new InterleavedBuffer(newArray, this.stride);
            ib.setUsage(this.usage);
            return ib;
        }

        #endregion

    }
}
