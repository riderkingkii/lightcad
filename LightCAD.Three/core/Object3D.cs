using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace LightCAD.Three
{
    public interface IDispose
    {
        void dispose();
    }
    public interface ISolid : IGeometry, IMaterialObject
    {

    }
    public interface IBounding
    {
        void computeBoundingBox();
        void computeBoundingSphere();
    }
    public interface IGeometry
    {
        BufferGeometry getGeometry();
        void setGeometry(BufferGeometry geo);
    }
    public class Object3D : EventDispatcher, IAnimationObject
    {
        /// <summary>
        /// 为MergeGroup的Children提供是否被合并
        /// </summary>
        public bool isMerged = false;
        /// <summary>
        /// 为MergeGroup的Children提供分类依据
        /// </summary>
        public string category = string.Empty;

        /// <summary>
        /// 可见性变更
        /// </summary>
        public bool visibleChanged
        {
            set
            {
                if (value)
                    this.dispatchEvent(new EventArgs() { type = "visibleChanged", target = this });
            }
        }

        /// <summary>
        /// 顶点属性变更
        /// </summary>
        public bool attributeChanged
        {
            set
            {
                if (value)
                    this.dispatchEvent(new EventArgs() { type = "attributeChanged", target = this });
            }
        }

        /// <summary>
        /// 材质变更
        /// </summary>
        public bool materialChanged
        {
            set
            {
                if (value)
                    this.dispatchEvent(new EventArgs() { type = "materialChanged", target = this });
            }
        }
        public bool matrixChanged
        {
            set
            {
                if (value)
                    this.dispatchEvent(new EventArgs() { type = "matrixChanged", target = this });
            }
        }

        public Color color;
        public bool colorChanged
        {
            set
            {
                if (value)
                    this.dispatchEvent(new EventArgs { type = "colorChanged", target = this });
            }
        }



        #region scope properties or methods
        //private static int _object3DId = 0;
        //private static Vector3 _v1 = new Vector3();
        //private static Quaternion _q1 = new Quaternion();
        //private static Matrix4 _m1 = new Matrix4();
        //private static Vector3 _target = new Vector3();
        //private static Vector3 _position = new Vector3();
        //private static Vector3 _scale = new Vector3();
        //private static Quaternion _quaternion = new Quaternion();
        private static Vector3 _xAxis = new Vector3(1, 0, 0);
        private static Vector3 _yAxis = new Vector3(0, 1, 0);
        private static Vector3 _zAxis = new Vector3(0, 0, 1);
        private static EventArgs _addedEvent = new EventArgs { type = "added" };
        private static EventArgs _removedEvent = new EventArgs { type = "removed" };
        private static Object3DContext getContext()
        {
            return ThreeThreadContext.GetCurrThreadContext().Object3DCtx;
        }
        #endregion

        #region Properties

        public static Vector3 DEFAULT_UP = new Vector3(0, 1, 0);
        public static bool DEFAULT_MATRIX_AUTO_UPDATE = true;
        public static bool DEFAULT_MATRIX_WORLD_AUTO_UPDATE = true;

        public readonly int id;
        public string uuid { get; set; }
        public string name;
        public string type;
        public Object3D parent;
        public ListEx<Object3D> children;
        public Vector3 up;
        public Matrix4 matrix;
        public Matrix4 matrixWorld;
        public bool matrixAutoUpdate;
        public bool matrixWorldNeedsUpdate;
        public bool matrixWorldAutoUpdate;
        public Layers layers;
        public bool visible;
        public bool castShadow;
        public bool receiveShadow;
        public bool frustumCulled;
        public int renderOrder;
        public ListEx<AnimationClip> animations;
        public JsObj<object> userData;

        public Vector3 position;
        public Euler rotation;
        public Quaternion quaternion;
        public Vector3 scale;
        public Matrix4 modelViewMatrix;
        public Matrix3 normalMatrix;

        public MeshDepthMaterial customDepthMaterial;
        public MeshDistanceMaterial customDistanceMaterial;
        public object this[string name]
        {
            get { return this.GetField(name); }
            set
            {
                this.SetField(name, value);
            }
        }

        #endregion
        /// <summary>
        ///Lbviewer的关联对象
        /// </summary>
        public object linkObject;
        public JsObj<object> ext;

        #region constructor
        private static ConcurrentDictionary<int, int> objIndex = new ConcurrentDictionary<int, int>();

        public Object3D()
        {
            this.id = ThreeThreadContext.NewId(objIndex);
            this.uuid = MathEx.GenerateUUID();
            this.name = "";
            this.type = "Object3D";
            this.parent = null;
            this.children = new ListEx<Object3D>();
            this.up = Object3D.DEFAULT_UP.Clone();
            this.position = new Vector3();
            this.rotation = new Euler();
            this.quaternion = new Quaternion();
            this.scale = new Vector3(1, 1, 1);
            this.modelViewMatrix = new Matrix4();
            this.normalMatrix = new Matrix3();
            this.ext = new JsObj<object>();
            void onRotationChange()
            {
                quaternion.SetFromEuler(rotation, false);
            }

            void onQuaternionChange()
            {
                rotation.SetFromQuaternion(quaternion, Euler.RotationOrders.None, false);
            }
            rotation.OnChange(onRotationChange);
            quaternion.OnChange(onQuaternionChange);
            this.matrix = new Matrix4();
            this.matrixWorld = new Matrix4();
            this.matrixAutoUpdate = Object3D.DEFAULT_MATRIX_AUTO_UPDATE;
            this.matrixWorldNeedsUpdate = false;
            this.matrixWorldAutoUpdate = Object3D.DEFAULT_MATRIX_WORLD_AUTO_UPDATE; // checked by the renderer
            this.layers = new Layers();
            this.visible = true;
            this.castShadow = false;
            this.receiveShadow = false;
            this.frustumCulled = true;
            this.renderOrder = 0;
            this.animations = new ListEx<AnimationClip>();
            this.userData = new JsObj<object>();
        }
        #endregion

        #region methods
        public Action<WebGLRenderer, Object3D, Camera, object, object, object> onBeforeRenderAction;
        public virtual void onBeforeRender(WebGLRenderer render, Object3D _object, Camera camera, object arg1 = null, object arg2 = null, object arg3 = null)
        {
            onBeforeRenderAction?.Invoke(render, _object, camera, arg1, arg2, arg3);
        }
        public Action<WebGLRenderer, Object3D, Camera, object, object, object> onAfterRenderAction;
        public void onAfterRender(WebGLRenderer render, Object3D _object, Camera camera, object arg1 = null, object arg2 = null, object arg3 = null)
        {
            onAfterRenderAction?.Invoke(render, _object, camera, arg1, arg2, arg3);
        }
        public void applyMatrix(Matrix4 matrix) => applyMatrix4(matrix);
        public void applyMatrix4(Matrix4 matrix)
        {
            if (this.matrixAutoUpdate) this.updateMatrix();
            this.matrix.Premultiply(matrix);
            this.matrix.Decompose(this.position, this.quaternion, this.scale);
        }
        public Object3D applyQuaternion(Quaternion q)
        {
            this.quaternion.Premultiply(q);
            return this;
        }
        public void setRotationFromAxisAngle(Vector3 axis, double angle)
        {
            // assumes axis is normalized
            this.quaternion.SetFromAxisAngle(axis, angle);
        }
        public void setRotationFromEuler(Euler euler)
        {
            this.quaternion.SetFromEuler(euler, true);
        }
        public void setRotationFromMatrix(Matrix4 m)
        {
            // assumes the upper 3x3 of m is a pure rotation matrix (i.e, unscaled)
            this.quaternion.SetFromRotationMatrix(m);
        }
        public void setRotationFromQuaternion(Quaternion q)
        {
            // assumes q is normalized
            this.quaternion.Copy(q);
        }
        public Object3D rotateOnAxis(Vector3 axis, double angle)
        {
            // rotate object on axis in object space
            // axis is assumed to be normalized
            var _q1 = getContext()._q1;
            _q1.SetFromAxisAngle(axis, angle);
            this.quaternion.Multiply(_q1);
            return this;
        }
        public Object3D rotateOnWorldAxis(Vector3 axis, double angle)
        {
            // rotate object on axis in world space
            // axis is assumed to be normalized
            // method assumes no rotated parent
            var _q1 = getContext()._q1;
            _q1.SetFromAxisAngle(axis, angle);
            this.quaternion.Premultiply(_q1);
            return this;
        }
        public Object3D rotateX(double angle)
        {
            return this.rotateOnAxis(_xAxis, angle);
        }
        public Object3D rotateY(double angle)
        {
            return this.rotateOnAxis(_yAxis, angle);
        }
        public Object3D rotateZ(double angle)
        {
            return this.rotateOnAxis(_zAxis, angle);
        }
        public Object3D translateOnAxis(Vector3 axis, double distance)
        {
            // translate object by distance along axis in object space
            // axis is assumed to be normalized
            var _v1 = getContext()._v1;
            _v1.Copy(axis).ApplyQuaternion(this.quaternion);
            this.position.Add(_v1.MulScalar(distance));
            return this;
        }
        public Object3D translateX(double distance)
        {
            return this.translateOnAxis(_xAxis, distance);
        }
        public Object3D translateY(double distance)
        {
            return this.translateOnAxis(_yAxis, distance);
        }
        public Object3D translateZ(double distance)
        {
            return this.translateOnAxis(_zAxis, distance);
        }
        public Vector3 localToWorld(Vector3 vector)
        {
            this.updateWorldMatrix(true, false);
            return vector.ApplyMatrix4(this.matrixWorld);
        }
        public Vector3 worldToLocal(Vector3 vector)
        {
            this.updateWorldMatrix(true, false);
            var _m1 = getContext()._m1;
            return vector.ApplyMatrix4(_m1.Copy(this.matrixWorld).Invert());
        }
        public void lookAt(double x, double y, double z)
        {
            var _target = getContext()._target;
            _target.Set(x, y, z);
            this.lookAt(_target);
        }

        public void lookAt(Vector3 target)
        {
            // This method does not support objects having non-uniformly-scaled parent(s)
            var _target = getContext()._target;
            _target.Copy(target);
            Object3D parent = this.parent;
            this.updateWorldMatrix(true, false);
            var _position = getContext()._position;
            _position.SetFromMatrixPosition(this.matrixWorld);
            var _m1 = getContext()._m1;
            if (this is Camera || this is Light)
            {
                var m12 = new Matrix4().MakeRotationAxis(new Vector3(0, 0, 1), Math.PI / 2);
                _m1.LookAt(_position, target, this.up);
            }
            else
            {
                _m1.LookAt(target, _position, this.up);
            }
            this.quaternion.SetFromRotationMatrix(_m1);
            if (parent != null)
            {
                var _q1 = getContext()._q1;
                _m1.ExtractRotation(parent.matrixWorld);
                _q1.SetFromRotationMatrix(_m1);
                this.quaternion.Premultiply(_q1.Invert());
            }
        }
        public virtual Object3D add(params Object3D[] _objects)
        {
            if (_objects.Length == 0)
                return this;
            if (_objects.Length > 1)
            {
                for (int i = 0; i < _objects.Length; i++)
                {
                    this.add(_objects[i]);
                }
                return this;
            }
            var _object = _objects[0];
            if (_object == this)
            {
                console.error("THREE.Object3D.add: object can\"t be added as a child of itself.", _objects);
                return this;
            }
            if (_object != null && _object is Object3D)
            {
                if (_object.parent != null)
                {
                    _object.parent.remove(_object);
                }
                _object.parent = this;
                this.children.Push(_object);
                _object.dispatchEvent(_addedEvent);
            }
            else
            {
                console.error("THREE.Object3D.add: object not an instance of THREE.Object3D.", _object);
            }
            return this;
        }
        public Object3D addNoEvents(params Object3D[] _objects)
        {
            for (int i = 0; i < _objects.Length; i++)
            {
                var _object = _objects[i];
                if (_object != null && _object is Object3D)
                {
                    if (_object.parent != null)
                    {
                        _object.parent.remove(_object);
                    }
                    _object.parent = this;
                    this.children.Push(_object);
                }
                else
                {
                    console.error("THREE.Object3D.add: object not an instance of THREE.Object3D.", _object);
                }
            }
            return this;
        }

        public virtual Object3D remove(params Object3D[] _objects)
        {
            if (_objects.Length > 1)
            {
                for (int i = 0; i < _objects.Length; i++)
                {
                    this.remove(_objects[i]);
                }
                return this;
            }
            var _object = _objects[0];
            int index = this.children.IndexOf(_object);
            if (index != -1)
            {
                _object.parent = null;
                this.children.Splice(index, 1);
                _object.dispatchEvent(_removedEvent);
            }
            return this;
        }

        public Object3D removeNoEvents(params Object3D[] _objects)
        {
            for (int i = 0; i < _objects.Length; i++)
            {
                var _object = _objects[i];
                int index = this.children.IndexOf(_object);
                if (index != -1)
                {
                    _object.parent = null;
                    this.children.Splice(index, 1);
                }
            }
            return this;
        }

        public Object3D removeFromParent()
        {
            if (this.parent != null)
            {
                this.parent.remove(this);
            }
            return this;
        }
        public Object3D clear()
        {
            for (int i = 0; i < this.children.Count; i++)
            {
                Object3D _object = this.children[i];
                _object.parent = null;
                _object.dispatchEvent(_removedEvent);
            }
            this.children.Clear();
            return this;
        }
        public Object3D attach(Object3D _object)
        {
            // adds object as a child of this, while maintaining the object"s world transform
            // Note: This method does not support scene graphs having non-uniformly-scaled nodes(s)
            this.updateWorldMatrix(true, false);
            var _m1 = getContext()._m1;
            _m1.Copy(this.matrixWorld).Invert();
            if (_object.parent != null)
            {
                _object.parent.updateWorldMatrix(true, false);
                _m1.Multiply(_object.parent.matrixWorld);
            }
            _object.applyMatrix4(_m1);
            this.add(_object);
            _object.updateWorldMatrix(false, true);
            return this;
        }
        public Object3D getObjectById(int id)
        {
            return this.getObjectByProperty("id", id.ToString());
        }
        public Object3D getObjectByName(string name)
        {
            return this.getObjectByProperty("name", name);
        }
        public Object3D getObjectByProperty(string name, string value)
        {
            var v = this[name];
            if (v is string && (string)v == value)
                return this;

            for (int i = 0, l = this.children.Count; i < l; i++)
            {
                var child = this.children[i];
                var _object = child.getObjectByProperty(name, value);
                if (_object != null)
                {
                    return _object;
                }
            }
            return null;
        }
        public ListEx<Object3D> getObjectsByProperty(string name, object value)
        {
            var result = new ListEx<Object3D>();
            if (this[name] == value)
                result.Push(this);

            for (int i = 0, l = this.children.Count; i < l; i++)
            {
                var childResult = this.children[i].getObjectsByProperty(name, value);
                if (childResult.Count > 0)
                {
                    result = result.Concat(childResult);
                }
            }
            return result;
        }
        public Vector3 getWorldPosition(Vector3 target = null)
        {
            target = target ?? new Vector3();
            this.updateWorldMatrix(true, false);
            return target.SetFromMatrixPosition(this.matrixWorld);
        }
        public Quaternion getWorldQuaternion(Quaternion target)
        {
            var ctx = getContext();
            var _position = ctx._position;
            var _scale = ctx._scale;
            this.updateWorldMatrix(true, false);
            this.matrixWorld.Decompose(_position, target, _scale);
            return target;
        }
        public Vector3 getWorldScale(Vector3 target)
        {
            var ctx = getContext();
            var _position = ctx._position;
            var _quaternion = ctx._quaternion;
            this.updateWorldMatrix(true, false);
            this.matrixWorld.Decompose(_position, _quaternion, target);
            return target;
        }
        public virtual Vector3 getWorldDirection(Vector3 target)
        {
            this.updateWorldMatrix(true, false);
            var e = this.matrixWorld.Elements;
            return target.Set(e[8], e[9], e[10]).Normalize();
        }
        public virtual void raycast(Raycaster raycaster, ListEx<Raycaster.Intersection> intersects = null, object vars = null)
        {
        }
        public virtual void traverse(Action<Object3D> callback)
        {
            callback(this);
            var children = this.children;
            for (int i = 0, l = children.Count; i < l; i++)
            {
                children[i].traverse(callback);
            }
        }
        public void traverseVisible(Action<Object3D> callback)
        {
            if (this.visible == false) return;
            callback(this);
            var children = this.children;
            for (int i = 0, l = children.Count; i < l; i++)
            {
                children[i].traverseVisible(callback);
            }
        }
        public void traverseAncestors(Action<Object3D> callback)
        {
            var parent = this.parent;
            if (parent != null)
            {
                callback(parent);
                parent.traverseAncestors(callback);
            }
        }
        public void updateMatrix()
        {
            this.matrix.Compose(this.position, this.quaternion, this.scale);
            this.matrixWorldNeedsUpdate = true;
        }
        public virtual void updateMatrixWorld(bool force = false)
        {
            if (this.matrixAutoUpdate) this.updateMatrix();
            if (this.matrixWorldNeedsUpdate || force)
            {
                if (this.parent == null)
                {
                    this.matrixWorld.Copy(this.matrix);
                }
                else
                {
                    this.matrixWorld.MultiplyMatrices(this.parent.matrixWorld, this.matrix);
                }
                this.matrixWorldNeedsUpdate = false;
                force = true;
            }
            // update children
            var children = this.children;
            for (int i = 0, l = children.Count; i < l; i++)
            {
                var child = children[i];
                if (child == null)
                    continue;

                if (child.matrixWorldAutoUpdate || force)
                {
                    child.updateMatrixWorld(force);
                }
            }
        }
        public virtual void updateWorldMatrix(bool updateParents, bool updateChildren)
        {
            if (updateParents && this.parent != null && this.parent.matrixWorldAutoUpdate)
            {
                parent.updateWorldMatrix(true, false);
            }
            if (this.matrixAutoUpdate) this.updateMatrix();
            if (this.parent == null)
            {
                this.matrixWorld.Copy(this.matrix);
            }
            else
            {
                this.matrixWorld.MultiplyMatrices(this.parent.matrixWorld, this.matrix);
            }
            // update children
            if (updateChildren)
            {
                for (int i = 0, l = this.children.Count; i < l; i++)
                {
                    var child = this.children[i];
                    if (child.matrixWorldAutoUpdate)
                    {
                        child.updateWorldMatrix(false, true);
                    }
                }
            }
        }
        public virtual Object3D clone(bool recursive = true)
        {
            //如果子类没有覆盖父类的clone那就 通过反射调用子类的copy
            var type = this.GetType();
            var ctros = type.GetConstructors();
            var paramInfos = ctros[0].GetParameters();
            var paramLen = paramInfos.Length;
            var paramArr = paramLen == 0 ? null : new object[paramLen];
            for (int i = 0; i < paramLen; i++)
            {
                var paramInfo = paramInfos[i];
                var defaultVal = paramInfo.HasDefaultValue ? paramInfo.DefaultValue : null;
                if (paramInfo.ParameterType.IsValueType)
                    defaultVal = Activator.CreateInstance(paramInfo.ParameterType);
                paramArr[i] = defaultVal;
            }
            var obj = ctros[0].Invoke(paramArr);
            var method = type.GetMethod("copy", new Type[] { type, typeof(bool) });
            if (method == null)
                method = type.GetMethod("copy", new Type[] { typeof(Object3D), typeof(bool) });
            return method.Invoke(obj, new object[] { this, recursive }) as Object3D;
            //return new Object3D().copy(this, recursive);
        }
        public virtual Object3D copy(Object3D source, bool recursive = true)
        {
            this.name = source.name;
            this.up.Copy(source.up);
            this.position.Copy(source.position);
            this.rotation.Order = source.rotation.Order;
            this.quaternion.Copy(source.quaternion);
            this.scale.Copy(source.scale);
            this.matrix.Copy(source.matrix);
            this.matrixWorld.Copy(source.matrixWorld);
            this.matrixAutoUpdate = source.matrixAutoUpdate;
            this.matrixWorldNeedsUpdate = source.matrixWorldNeedsUpdate;
            this.matrixWorldAutoUpdate = source.matrixWorldAutoUpdate;
            this.layers.mask = source.layers.mask;
            this.visible = source.visible;
            this.castShadow = source.castShadow;
            this.receiveShadow = source.receiveShadow;
            this.frustumCulled = source.frustumCulled;
            this.renderOrder = source.renderOrder;
            this.userData = source.userData.clone();
            this.color = source.color?.Clone();
            if (recursive)
            {
                for (int i = 0; i < source.children.Count; i++)
                {
                    var child = source.children[i];
                    this.add(child.clone());
                }
            }
            return this;
        }
        public Object3D removeAll(bool hasEvent = false)
        {
            for (int i = children.Count - 1; i >= 0; i--)
            {
                var child = this.children[i];
                child.parent = null;
                if (hasEvent)
                    child.dispatchEvent(new EventArgs { type = "removed", target = child });
                this.children.RemoveAt(i);
            }
            return this;
        }
        public void dispose()
        {
            this.removeAll();
        }
        #endregion

    }
}
