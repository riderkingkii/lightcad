using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class InstancedBufferAttribute : BufferAttribute
    {
        #region Properties

        public int meshPerAttribute;
        #endregion

        #region constructor
        public InstancedBufferAttribute(double[] array, int itemSize, bool normalized = true, int meshPerAttribute = 1)
        : base(array, itemSize, normalized)
        {
            this.meshPerAttribute = meshPerAttribute;
        }
        public InstancedBufferAttribute()
        : base()
        {

        }
        #endregion

        #region methods
        public InstancedBufferAttribute copy(InstancedBufferAttribute source)
        {
            base.copy(source);
            this.meshPerAttribute = source.meshPerAttribute;
            return this;
        }
        #endregion
    }
}
