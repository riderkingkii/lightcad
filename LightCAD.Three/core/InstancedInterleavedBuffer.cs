using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class InstancedInterleavedBuffer : InterleavedBuffer
    {
        #region Properties
        public int meshPerAttribute;

        #endregion

        #region constructor
        public InstancedInterleavedBuffer(double[] array = null, int stride = 0, int meshPerAttribute = 1)
            : base(array, stride)
        {
            this.meshPerAttribute = meshPerAttribute;
        }
        #endregion

        #region methods
        public InstancedInterleavedBuffer copy(InstancedInterleavedBuffer source)
        {
            base.copy(source);
            this.meshPerAttribute = source.meshPerAttribute;
            return this;
        }
        public override BufferAttribute clone()
        {
            var ib = new InstancedInterleavedBuffer();
            return ib.copy(this);
        }
        #endregion
    }
}
