using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class Layers
    {
        #region Properties
        public uint mask;
        #endregion

        #region constructor
        public Layers()
        {
            this.mask = 1 | 0;
        }
        #endregion

        #region methods
        public void set(int channel)
        {
            this.mask = (uint)(1 << channel | 0) >> 0;
        }
        public void enable(int channel)
        {
            this.mask |= 1u << channel | 0;
        }
        public void enableAll()
        {
            this.mask = 0xffffffff | 0;

        }
        public void toggle(int channel)
        {
            this.mask ^= 1u << channel | 0;
        }
        public void disable(int channel)
        {
            this.mask &= ~(1u << channel | 0);
        }
        public void disableAll()
        {
            this.mask = 0;
        }
        public bool test(Layers layers)
        {
            return (this.mask & layers.mask) != 0;
        }
        public bool isEnabled(int channel)
        {
            return (this.mask & (1 << channel | 0)) != 0;
        }
        #endregion

    }
}
