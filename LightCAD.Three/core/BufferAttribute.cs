#define NoDict
using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.MathEx;
using static LightCAD.MathLib.Constants;
using static LightCAD.Three.TypeUtils;
using static LightCAD.Three.HalfUtils;
using LightCAD.Three.OpenGL;
using LightCAD.MathLib;

namespace LightCAD.Three
{
    public class MorphAttributes : JsObj<ListEx<BufferAttribute>>
        {
#if NoDict
            private ListEx<BufferAttribute> _position;
            public ListEx<BufferAttribute> position
            {
                get => _position;
                set
                {
                    set("position", value);
                }
            }


            private ListEx<BufferAttribute> _normal;
            public ListEx<BufferAttribute> normal
            {
                get => _normal;
                set
                {
                    set("normal", value);
                }
            }


            private ListEx<BufferAttribute> _uv;
            public ListEx<BufferAttribute> uv
            {
                get => _uv;
                set
                {
                    set("uv", value);
                }
            }


            private ListEx<BufferAttribute> _color;
            public ListEx<BufferAttribute> color
            {
                get => _color;
                set
                {
                    set("color", value);
                }
            }
#else

            #region 字典
            public JsArr<BufferAttribute> position
            {
                get => this["position"];
                set
                {
                    this["position"] = value;
                }
            }
            public JsArr<BufferAttribute> normal
            {
                get => this["normal"];
                set
                {
                    this["normal"] = value;
                }
            }


            public JsArr<BufferAttribute> uv
            {
                get => this["uv"];
                set
                {
                    this["uv"] = value;
                }
            }
            public JsArr<BufferAttribute> color
            {
                get => this["color"];
                set
                {
                    this["color"] = value;
                }
            }
            #endregion
#endif
            public override ListEx<BufferAttribute> get(string name)
            {
#if NoDict
                switch (name)
                {
                    case "position":
                        return _position;
                    case "normal":
                        return _normal;
                    case "uv":
                        return _uv;
                    case "color":
                        return _color;
                    default:
                        return base.get(name);
                }
#else
                return base.get(name);
#endif
            }
            public override JsObj<ListEx<BufferAttribute>> set(string name, ListEx<BufferAttribute> val)
            {
#if NoDict
                switch (name)
                {
                    case "position":
                        _position = val;
                        break;
                    case "normal":
                        _normal = val;
                        break;
                    case "uv":
                        _uv = val;
                        break;
                    case "color":
                        _color = val;
                        break;
                    default:
                        break;
                }
#endif
                base.set(name, val);
                return this;
            }
        }
        public class BufferAttributes : JsObj<BufferAttribute>
        {
            private BufferAttribute _position;
            public BufferAttribute position
            {
                get => _position;
                set
                {
                    set("position", value);
                }
            }


            private BufferAttribute _normal;
            public BufferAttribute normal
            {
                get => _normal;
                set
                {
                    set("normal", value);
                }
            }
            private BufferAttribute _tangent;
            public BufferAttribute tangent
            {
                get => _tangent;
                set
                {
                    set("tangent", value);
                }
            }
            

            private BufferAttribute _uv;
            public BufferAttribute uv
            {
                get => _uv;
                set
                {
                    set("uv", value);
                }
            }
            private BufferAttribute _uv2;
            public BufferAttribute uv2
            {
                get => _uv2;
                set
                {
                    set("uv2", value);
                }
            }
            

            private BufferAttribute _color;
            public BufferAttribute color
            {
                get => _color;
                set
                {
                    set("color", value);
                }
            }
            private BufferAttribute _packColor;
            public BufferAttribute packColor
            {
                get => _packColor;
                set
                {
                    set("packColor", value);
                }
            }
            public override BufferAttribute get(string name)
            {
                switch (name)
                {
                    case "position":
                        return _position;
                    case "normal":
                        return _normal;
                    case "tangent":
                        return _tangent;
                    case "uv":
                        return _uv;
                    case "uv2":
                        return _uv2;
                    case "color":
                        return _color;
                    case "packColor":
                        return _packColor;
                    default:
                        return base.get(name);
                }
            }

            public override JsObj<BufferAttribute> set(string name, BufferAttribute val)
            {
                switch (name)
                {
                    case "position":
                        _position = val;
                        break;
                    case "normal":
                        _normal = val;
                        break;
                    case "tangent":
                        _tangent = val;
                        break;
                    case "uv":
                        _uv = val;
                        break;
                    case "uv2":
                        _uv2 = val;
                        break;
                    case "color":
                        _color = val;
                        break;
                    case "packColor":
                        _packColor = val;
                        break;
                    default:
                        break;
                }
                base.set(name, val);
                return this;
            }

            public override JsObj<BufferAttribute> remove(string name)
            {
                BufferAttribute val = null;
                switch (name)
                {
                    case "position":
                        _position = val;
                        break;
                    case "normal":
                        _normal = val;
                        break;
                    case "tangent":
                        _tangent = val;
                        break;
                    case "uv":
                        _uv = val;
                        break;
                    case "uv2":
                        _uv2 = val;
                        break;
                    case "color":
                        _color = val;
                        break;
                    case "packColor":
                        _packColor = val;
                        break;
                    default:
                        break;
                }
                return base.remove(name);
            }

            public override void Clear()
            {
                this._position = null;
                this._normal = null;
                this._tangent = null;
                this._uv = null;
                this._uv2 = null;
                this._color = null;
                this._packColor = null;
                base.Clear();
            }
        }
        public class BufferAttribute
        {
            public class UpdateRange
            {
                public int offset;
                public int count;
            }
            #region scope properties or methods
            //private static Vector3 _vector = new Vector3();
            //private static Vector2 _vector2 = new Vector2();
            private BufferAttributeContext getContext()
            {
                var tctx = ThreeThreadContext.GetCurrThreadContext();
                BufferAttributeContext _context = tctx.BufferAttrCtx;
                return _context;
            }
            #endregion

            #region Properties

            public string name;

            //在CPU内存中支持double，以完成精确计算
            //渲染到GPU时，转化为float[]
            public int itemSize;
            public bool normalized;
            public int usage;
            public UpdateRange updateRange;
            public int version;
            private int _count;
            internal Array arrObj;
            //public object data;
            #endregion

            #region constructor

            public BufferAttribute(Array arrObj = null, int itemSize = 0, bool normalized = false)
            {
                this.name = "";
                this.arrObj = arrObj;
                this.itemSize = itemSize;
                this.count = arrObj != null ? arrObj.Length / itemSize : 0;
                this.normalized = normalized;
                this.usage = gl.STATIC_DRAW;
                this.updateRange = new UpdateRange() { offset = 0, count = -1 };
                this.version = 0;
            }
            #endregion

            #region properties

            public virtual int count
            {
                get { return this._count; }
                set { this._count = value; }
            }

            public virtual double[] array
            {
                get { return (double[])this.arrObj; }
                set { this.arrObj = value; }
            }
            public virtual int[] intArray
            {
                get { return (int[])this.arrObj; }
                set { this.arrObj = value; }
            }

            public virtual bool needsUpdate
            {
                set
                {
                    if (value) this.version++;
                }
            }

            #endregion

            #region methods
            public Action onUploadCallbackAction;
            public virtual void onUploadCallback()
            {
                onUploadCallbackAction?.Invoke();
            }
            public virtual BufferAttribute setUsage(int value)
            {
                this.usage = value;
                return this;
            }
            public virtual BufferAttribute copy(BufferAttribute source)
            {
                this.name = source.name;
                if (source.arrObj.Length == 0)
                    this.arrObj = source.arrObj.Clone() as Array;
                else
                {
                    this.arrObj = Array.CreateInstance(source.arrObj.GetValue(0).GetType(), source.arrObj.Length);
                    Array.Copy(source.arrObj, this.arrObj, this.arrObj.Length);
                }
                this.itemSize = source.itemSize;
                this.count = source.count;
                this.normalized = source.normalized;
                this.usage = source.usage;
                return this;
            }
            public virtual BufferAttribute copyAt(int index1, BufferAttribute attribute, int index2)
            {
                index1 *= this.itemSize;
                index2 *= attribute.itemSize;
                if (this.arrObj is int[])
                {
                    for (int i = 0, l = this.itemSize; i < l; i++)
                    {
                        this.intArray[index1 + i] = attribute.intArray[index2 + i];
                    }
                }
                if (this.arrObj is double[])
                {
                    for (int i = 0, l = this.itemSize; i < l; i++)
                    {
                        this.array[index1 + i] = attribute.array[index2 + i];
                    }
                }
                return this;
            }
            public virtual BufferAttribute copyArray(Array array)
            {
                Array.Copy(array, this.arrObj, this.arrObj.Length);
                return this;
            }
            public virtual BufferAttribute applyMatrix3(Matrix3 m)
            {
                var _vector2 = getContext()._vector2;
                var _vector = getContext()._vector;
                if (this.itemSize == 2)
                {
                    for (int i = 0, l = this.count; i < l; i++)
                    {
                        _vector2.FromBufferAttribute(this, i);
                        _vector2.ApplyMatrix3(m);
                        this.setXY(i, _vector2.X, _vector2.Y);
                    }
                }
                else if (this.itemSize == 3)
                {
                    for (int i = 0, l = this.count; i < l; i++)
                    {
                        _vector.FromBufferAttribute(this, i);
                        _vector.ApplyMatrix3(m);
                        this.setXYZ(i, _vector.X, _vector.Y, _vector.Z);
                    }
                }
                return this;
            }
            public virtual BufferAttribute applyMatrix4(Matrix4 m)
            {
                var _vector = getContext()._vector;
                for (int i = 0, l = this.count; i < l; i++)
                {
                    _vector.FromBufferAttribute(this, i);
                    _vector.ApplyMatrix4(m);
                    this.setXYZ(i, _vector.X, _vector.Y, _vector.Z);
                }
                return this;
            }
            public virtual BufferAttribute applyNormalMatrix(Matrix3 m)
            {
                var _vector = getContext()._vector;
                for (int i = 0, l = this.count; i < l; i++)
                {
                    _vector.FromBufferAttribute(this, i);
                    _vector.ApplyNormalMatrix(m);
                    this.setXYZ(i, _vector.X, _vector.Y, _vector.Z);
                }
                return this;
            }
            public virtual BufferAttribute transformDirection(Matrix4 m)
            {
                var _vector = getContext()._vector;
                for (int i = 0, l = this.count; i < l; i++)
                {
                    _vector.FromBufferAttribute(this, i);
                    _vector.TransformDirection(m);
                    this.setXYZ(i, _vector.X, _vector.Y, _vector.Z);
                }
                return this;
            }
            public virtual BufferAttribute set(Array value, int offset = 0)
            {
                // Matching BufferAttribute constructor, do not Normalize the array.
                this.arrObj.set(value, offset);

                return this;
            }
            public virtual double getX(int index)
            {
                var x = this.array[index * this.itemSize];
                if (this.normalized) x = Denormalize(x, this.array);
                return x;
            }
            public int getIntX(int index)
            {
                var x = this.intArray[index * this.itemSize];
                if (this.normalized) x = (int)Denormalize(x, this.array);
                return x;
            }
            public virtual BufferAttribute setX(int index, double x)
            {
                if (this.normalized) x = Normalize(x, this.array);
                this.array[index * this.itemSize] = x;
                return this;
            }
            public virtual double getY(int index)
            {
                var y = this.array[index * this.itemSize + 1];
                if (this.normalized) y = Denormalize(y, this.array);
                return y;
            }
            public virtual int getIntY(int index)
            {
                var y = this.intArray[index * this.itemSize + 1];
                if (this.normalized) y = (int)Denormalize(y, this.intArray);
                return y;
            }
            public virtual BufferAttribute setY(int index, double y)
            {
                if (this.normalized) y = Normalize(y, this.array);
                this.array[index * this.itemSize + 1] = y;
                return this;
            }
            public virtual double getZ(int index)
            {
                var z = this.array[index * this.itemSize + 2];
                if (this.normalized) z = Denormalize(z, this.array);
                return z;
            }
            public virtual int getIntZ(int index)
            {
                var z = this.intArray[index * this.itemSize + 2];
                if (this.normalized) z = (int)Denormalize(z, this.intArray);
                return z;
            }
            public virtual BufferAttribute setZ(int index, double z)
            {
                if (this.normalized) z = Normalize(z, this.array);
                this.array[index * this.itemSize + 2] = z;
                return this;
            }
            public virtual double getW(int index)
            {
                var _index = index * this.itemSize + 3;
                if (_index >= this.array.Length)
                    return MathEx.NaN;
                var w = this.array[_index];
                if (this.normalized) w = Denormalize(w, this.array);
                return w;
            }
            public virtual int getIntW(int index)
            {
                var _index= index * this.itemSize + 3;
                if (_index >= this.array.Length)
                    return 0;
                var w = this.intArray[_index];
                if (this.normalized) w = (int)Denormalize(w, this.intArray);
                return w;
            }
            public virtual BufferAttribute setW(int index, double w)
            {
                if (this.normalized) w = Normalize(w, this.array);
                this.array[index * this.itemSize + 3] = w;
                return this;
            }
            public virtual BufferAttribute setXY(int index, double x, double y)
            {
                index *= this.itemSize;
                if (this.normalized)
                {
                    x = Normalize(x, this.array);
                    y = Normalize(y, this.array);
                }
                this.array[index + 0] = x;
                this.array[index + 1] = y;
                return this;
            }
            public virtual BufferAttribute setXYZ(int index, double x, double y, double z)
            {
                index *= this.itemSize;
                if (this.normalized)
                {
                    x = Normalize(x, this.array);
                    y = Normalize(y, this.array);
                    z = Normalize(z, this.array);
                }
                this.array[index + 0] = x;
                this.array[index + 1] = y;
                this.array[index + 2] = z;
                return this;
            }
            public virtual BufferAttribute setXYZW(int index, double x, double y, double z, double w)
            {
                index *= this.itemSize;
                if (this.normalized)
                {
                    x = Normalize(x, this.array);
                    y = Normalize(y, this.array);
                    z = Normalize(z, this.array);
                    w = Normalize(w, this.array);
                }
                this.array[index + 0] = x;
                this.array[index + 1] = y;
                this.array[index + 2] = z;
                this.array[index + 3] = w;
                return this;
            }
            public virtual BufferAttribute onUpload(Action callback)
            {
                this.onUploadCallbackAction = callback;
                return this;
            }

            public virtual BufferAttribute clone()
            {
                return new BufferAttribute(this.arrObj, this.itemSize).copy(this);
            }

            #endregion

        }

        public class Uint8BufferAttribute : BufferAttribute
        {
            public byte[] uint8Array;
            public Uint8BufferAttribute()
            {

            }
            public Uint8BufferAttribute(int length, int itemSize)
            : base(new byte[length], itemSize, false)

            {
                this.uint8Array = (byte[])arrObj;
            }
            public Uint8BufferAttribute(byte[] array, int itemSize)
           : base(array, itemSize, false)

            {
                this.uint8Array = (byte[])arrObj;
            }
            public new Byte[] array
            {
                get
                {
                    return (byte[])this.arrObj;
                }
            }

            public override BufferAttribute clone()
            {
                var newbuf = new Uint8BufferAttribute();
                newbuf.copy(this);
                newbuf.uint8Array = (byte[])newbuf.arrObj;
                return newbuf;
            }
        }
        public class Float16BufferAttribute : BufferAttribute
        {
            public UInt16[] arrayu16 => (UInt16[])this.arrObj;
            public Float16BufferAttribute(UInt16[] array, int itemSize, bool normalized) : base(array, itemSize, normalized)
            {
            }
            public override double getX(int index)
            {
                var x = fromHalfFloat(arrayu16[index * this.itemSize]);
                if (this.normalized) x = (float)Denormalize(x, this.array);
                return x;
            }

            public override BufferAttribute setX(int index, double x)
            {

                if (this.normalized) x = Normalize(x, this.arrayu16);

                this.array[index * this.itemSize] = toHalfFloat(x);

                return this;

            }

            public override double getY(int index)
            {

                var y = fromHalfFloat(this.arrayu16[index * this.itemSize + 1]);

                if (this.normalized) y = (float)Denormalize(y, this.arrayu16);

                return y;

            }

            public override BufferAttribute setY(int index, double y)
            {

                if (this.normalized) y = Normalize(y, this.arrayu16);

                this.arrayu16[index * this.itemSize + 1] = toHalfFloat(y);

                return this;

            }

            public override double getZ(int index)
            {

                var z = fromHalfFloat(this.arrayu16[index * this.itemSize + 2]);

                if (this.normalized) z = (float)Denormalize(z, this.array);

                return z;

            }

            public override BufferAttribute setZ(int index, double z)
            {

                if (this.normalized) z = Normalize(z, this.arrayu16);

                this.arrayu16[index * this.itemSize + 2] = toHalfFloat(z);

                return this;

            }

            public override double getW(int index)
            {

                var w = fromHalfFloat(this.arrayu16[index * this.itemSize + 3]);

                if (this.normalized) w = (float)Denormalize(w, this.arrayu16);

                return w;

            }

            public override BufferAttribute setW(int index, double w)
            {

                if (this.normalized) w = Normalize(w, this.arrayu16);

                this.arrayu16[index * this.itemSize + 3] = toHalfFloat(w);

                return this;

            }

            public override BufferAttribute setXY(int index, double x, double y)
            {

                index *= this.itemSize;

                if (this.normalized)
                {

                    x = Normalize(x, this.arrayu16);
                    y = Normalize(y, this.arrayu16);

                }

                this.arrayu16[index + 0] = toHalfFloat(x);
                this.arrayu16[index + 1] = toHalfFloat(y);

                return this;

            }

            public override BufferAttribute setXYZ(int index, double x, double y, double z)
            {

                index *= this.itemSize;

                if (this.normalized)
                {

                    x = Normalize(x, this.arrayu16);
                    y = Normalize(y, this.arrayu16);
                    z = Normalize(z, this.arrayu16);

                }

                this.arrayu16[index + 0] = toHalfFloat(x);
                this.arrayu16[index + 1] = toHalfFloat(y);
                this.arrayu16[index + 2] = toHalfFloat(z);

                return this;

            }

            public override BufferAttribute setXYZW(int index, double x, double y, double z, double w)
            {

                index *= this.itemSize;

                if (this.normalized)
                {

                    x = Normalize(x, this.arrayu16);
                    y = Normalize(y, this.arrayu16);
                    z = Normalize(z, this.arrayu16);
                    w = Normalize(w, this.arrayu16);

                }

                this.arrayu16[index + 0] = toHalfFloat(x);
                this.arrayu16[index + 1] = toHalfFloat(y);
                this.arrayu16[index + 2] = toHalfFloat(z);
                this.arrayu16[index + 3] = toHalfFloat(w);

                return this;

            }
        }

        public class Int16BufferAttribute : BufferAttribute
        {
            public short[] int16Array;
            public Int16BufferAttribute() { }
            public Int16BufferAttribute(int length, int itemSize)
            : base(new short[length], itemSize, false)

            {
                this.int16Array = (short[])arrObj;
            }
            public Int16BufferAttribute(short[] array, int itemSize)
            : base(array, itemSize, false)

            {
                this.int16Array = (short[])arrObj;
            }
            public new short[] array
            {
                get
                {
                    return (short[])this.arrObj;
                }
            }

            public override BufferAttribute clone()
            {
                var newbuf = new Int16BufferAttribute();
                newbuf.copy(this);
                newbuf.int16Array = (short[])newbuf.arrObj;
                return newbuf;
            }
        }
        public class Uint16BufferAttribute : BufferAttribute
        {
            public Uint16BufferAttribute(UInt16[] array, int itemSize, bool normalized = false) : base(array, itemSize, normalized)
            {
            }
        }
        public class Uint32BufferAttribute : BufferAttribute
        {
            //为了方便编程，实际使用int[]
            public Uint32BufferAttribute(int[] array = null, int itemSize = 0, bool normalized = false)
                : base(array, itemSize, normalized)
            {
            }
            public Uint32BufferAttribute(uint[] array = null, int itemSize = 0, bool normalized = false)
              : base(array, itemSize, normalized)
            {
            }
            public Uint32BufferAttribute(int count, int itemSize = 0, bool normalized = false)
                : this(new int[count], itemSize, normalized)
            {
            }
        }
        public class Float32BufferAttribute : BufferAttribute
        {
            //为了精确计算，实际使用double[]
            public Float32BufferAttribute(double[] array = null, int itemSize = 0, bool normalized = false)
                : base(array, itemSize, normalized)
            {
            }
            public Float32BufferAttribute(int count,int itemSize, bool normalized=false)
                : this(new double[count], itemSize, normalized)
            {
            }
        }
    public class Float32BufferAttribute2 : BufferAttribute
    {
        //为了精确计算，实际使用double[]
        public Float32BufferAttribute2(float[] array = null, int itemSize = 0, bool normalized = false)
            : base(array, itemSize, normalized)
        {
        }
        public Float32BufferAttribute2(int count, int itemSize, bool normalized = false)
            : this(new float[count], itemSize, normalized)
        {
        }
    }
}
