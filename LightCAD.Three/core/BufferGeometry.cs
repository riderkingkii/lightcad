using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using static LightCAD.MathLib.Constants;
using static LightCAD.MathLib.MathEx;

namespace LightCAD.Three
{
    public class BufferGeometry : EventDispatcher, IBounding, IDispose
    {
        private static BufferGeometry _defaultGeometry;
        public static BufferGeometry defaultGeometry
        {
            get
            {
                if (_defaultGeometry == null)
                {
                    _defaultGeometry = new BufferGeometry();

                    var float32Array = new double[] {
                                - 0.5, - 0.5, 0, 0, 0,
                                0.5, - 0.5, 0, 1, 0,
                                0.5, 0.5, 0, 1, 1,
                                - 0.5, 0.5, 0, 0, 1
                            };

                    var interleavedBuffer = new InterleavedBuffer(float32Array, 5);

                    _defaultGeometry.setIndex(new int[] { 0, 1, 2, 0, 2, 3 });
                    _defaultGeometry.setAttribute("position", new InterleavedBufferAttribute(interleavedBuffer, 3, 0, false));
                    _defaultGeometry.setAttribute("uv", new InterleavedBufferAttribute(interleavedBuffer, 2, 3, false));

                }
                return _defaultGeometry;
            }
            set
            {
                _defaultGeometry = null;
            }
        }

        public class DrawRange
        {
            public int start;
            public int count;
        }

        #region scope properties or methods

        //private static int _id = 0;
        //private static Matrix4 _m1 = new Matrix4();
        //private static Object3D _obj = new Object3D();
        //private static Vector3 _offset = new Vector3();
        //private static Box3 _box = new Box3();
        //private static Box3 _boxMorphTargets = new Box3();
        //private static Vector3 _vector = new Vector3();
        private BufferGeometryContext getContext()
        {
            return ThreeThreadContext.GetCurrThreadContext().BufferGeoCtx;
        }
        #endregion

        #region Properties
        //public bool isVirtual;
        public string uuid;
        public string name;
        public string type;
        public BufferAttribute index;
        public BufferAttributes attributes;
        public MorphAttributes morphAttributes;
        public bool morphTargetsRelative;
        public ListEx<GeometryGroup> groups;
        public Box3 boundingBox;
        public Sphere boundingSphere;
        public DrawRange drawRange;
        public JsObj<object> userData;
        public JsObj<object> ext;
        public int id { get; }

        #endregion
        #region constructor
        private static ConcurrentDictionary<int, int> objIndex = new ConcurrentDictionary<int, int>();
        public BufferGeometry() : base()
        {
            //this.isVirtual = false;
            this.id = ThreeThreadContext.NewId(objIndex);
            this.uuid = MathEx.GenerateUUID();
            this.name = "";
            this.type = "BufferGeometry";
            this.index = null;
            this.attributes = new BufferAttributes();
            this.morphAttributes = new MorphAttributes();
            this.morphTargetsRelative = false;
            this.groups = new ListEx<GeometryGroup>();
            this.boundingBox = null;
            this.boundingSphere = null;
            this.drawRange = new DrawRange { start = 0, count = Infinity };
            this.userData = new JsObj<object>();
            this.ext = new JsObj<object>();
        }
        #endregion

        #region methods
        public BufferAttribute getIndex()
        {
            return this.index;
        }
        public BufferGeometry setIndex(BufferAttribute index)
        {
            this.index = index;
            return this;
        }
        public BufferGeometry setIndex(int[] index)
        {
            this.index = new Uint32BufferAttribute(index, 1);
            return this;
        }
        public BufferAttribute getAttribute(string name)
        {
            return this.attributes[name];
        }
        public BufferGeometry setAttribute(string name, BufferAttribute attribute)
        {
            this.attributes[name] = attribute;
            return this;
        }
        public BufferGeometry setAttribute(string name, InterleavedBufferAttribute attribute)
        {
            this.attributes[name] = attribute;
            return this;
        }
        public BufferGeometry deleteAttribute(string name)
        {
            this.attributes.remove(name);
            return this;
        }
        public bool hasAttribute(string name)
        {
            return this.attributes[name] != null;
        }
        public void addGroup(int start, int count, int materialIndex = 0)
        {
            this.groups.Push(new GeometryGroup
            {
                Start = start,
                Count = count,
                MaterialIndex = materialIndex
            });
        }
        public void clearGroups()
        {
            this.groups?.Clear();
            this.groups = new ListEx<GeometryGroup>();
        }
        public void setDrawRange(int start, int count)
        {
            this.drawRange.start = start;
            this.drawRange.count = count;
        }
        public BufferGeometry applyMatrix(Matrix4 matrix)
        {
            return this.applyMatrix4(matrix);
        }
        public virtual BufferGeometry applyMatrix4(Matrix4 matrix)
        {
            var position = this.attributes.position;
            if (position != null)
            {
                position.applyMatrix4(matrix);
                position.needsUpdate = true;
            }
            var normal = this.attributes.normal;
            if (normal != null)
            {
                var normalMatrix = new Matrix3().GetNormalMatrix(matrix);
                normal.applyNormalMatrix(normalMatrix);
                normal.needsUpdate = true;
            }
            var tangent = this.attributes.get("tangent");
            if (tangent != null)
            {
                tangent.transformDirection(matrix);
                tangent.needsUpdate = true;
            }
            if (this.boundingBox != null)
            {
                this.computeBoundingBox();
            }
            if (this.boundingSphere != null)
            {
                this.computeBoundingSphere();
            }
            return this;
        }
        public BufferGeometry applyQuaternion(Quaternion q)
        {
            var _m1 = getContext()._m1;
            _m1.MakeRotationFromQuaternion(q);
            this.applyMatrix4(_m1);
            return this;
        }
        public BufferGeometry rotateX(double angle)
        {
            // rotate geometry around world x-axis
            var _m1 = getContext()._m1;
            _m1.MakeRotationX(angle);
            this.applyMatrix4(_m1);
            return this;
        }
        public BufferGeometry rotateY(double angle)
        {
            // rotate geometry around world y-axis
            var _m1 = getContext()._m1;
            _m1.MakeRotationY(angle);
            this.applyMatrix4(_m1);
            return this;
        }
        public BufferGeometry rotateZ(double angle)
        {
            // rotate geometry around world z-axis
            var _m1 = getContext()._m1;
            _m1.MakeRotationZ(angle);
            this.applyMatrix4(_m1);
            return this;
        }
        public BufferGeometry translate(double x, double y, double z)
        {
            // translate geometry
            var _m1 = getContext()._m1;
            _m1.MakeTranslation(x, y, z);
            this.applyMatrix4(_m1);
            return this;
        }
        public BufferGeometry translate(Vector3 vec)
        {
            // translate geometry
            var _m1 = getContext()._m1;
            _m1.MakeTranslation(vec.X, vec.Y, vec.Z);
            this.applyMatrix4(_m1);
            return this;
        }
        /// <summary>
        /// 提供不用矩阵直接修改position的快速平移方法
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public BufferGeometry translateQuick(double x, double y, double z)
        {
            var position = this.attributes.position;
            for (int i = 0; i < position.array.Length; i += 3)
            {
                position.array[i] += x;
                position.array[i + 1] += y;
                position.array[i + 2] += z;
            }
            position.needsUpdate = true;
            if (this.boundingBox != null)
            {
                this.computeBoundingBox();
            }
            if (this.boundingSphere != null)
            {
                this.computeBoundingSphere();
            }
            return this;
        }

        public BufferGeometry translateQuick(Vector3 v3)
        {
            return translateQuick(v3.X, v3.Y, v3.Z);
        }

        public BufferGeometry scale(double x, double y, double z)
        {
            // scale geometry
            var _m1 = getContext()._m1;
            _m1.MakeScale(x, y, z);
            this.applyMatrix4(_m1);
            return this;
        }
        public BufferGeometry lookAt(Vector3 vector)
        {
            var _obj = getContext()._obj;
            _obj.lookAt(vector);
            _obj.updateMatrix();
            this.applyMatrix4(_obj.matrix);
            return this;
        }
        public BufferGeometry center()
        {
            var _offset = getContext()._offset;
            this.computeBoundingBox();
            this.boundingBox.GetCenter(_offset).Negate();
            this.translate(_offset.X, _offset.Y, _offset.Z);
            return this;
        }
        public BufferGeometry setFromPoints(Vector2[] points)
        {
            var position = new ListEx<double>();
            for (int i = 0, l = points.Length; i < l; i++)
            {
                var point = points[i];
                position.Push(point.X, point.Y, 0);
            }
            this.setAttribute("position", new Float32BufferAttribute(position.ToArray(), 3));
            return this;
        }
        public BufferGeometry setFromPoints(ListEx<Vector2> points)
        {
            var position = new ListEx<double>();
            for (int i = 0, l = points.Length; i < l; i++)
            {
                var point = points[i];
                position.Push(point.X, point.Y, 0);
            }
            this.setAttribute("position", new Float32BufferAttribute(position.ToArray(), 3));
            return this;
        }
        public BufferGeometry setFromPoints(Vector3[] points)
        {
            var position = new ListEx<double>();
            for (int i = 0, l = points.Length; i < l; i++)
            {
                var point = points[i];
                position.Push(point.X, point.Y, point.Z);
            }
            this.setAttribute("position", new Float32BufferAttribute(position.ToArray(), 3));
            return this;
        }
        public BufferGeometry setFromPoints(ListEx<Vector3> points)
        {
            var position = new ListEx<double>();
            for (int i = 0, l = points.Length; i < l; i++)
            {
                var point = points[i];
                position.Push(point.X, point.Y, point.Z);
            }
            this.setAttribute("position", new Float32BufferAttribute(position.ToArray(), 3));
            return this;
        }
        public BufferGeometry setFromPoints(Vector4[] points)
        {
            var position = new ListEx<double>();
            for (int i = 0, l = points.Length; i < l; i++)
            {
                var point = points[i];
                position.Push(point.X, point.Y, point.Z);
            }
            this.setAttribute("position", new Float32BufferAttribute(position.ToArray(), 3));
            return this;
        }
        public BufferGeometry setFromPoints(ListEx<Vector4> points)
        {
            var position = new ListEx<double>();
            for (int i = 0, l = points.Length; i < l; i++)
            {
                var point = points[i];
                position.Push(point.X, point.Y, point.Z);
            }
            this.setAttribute("position", new Float32BufferAttribute(position.ToArray(), 3));
            return this;
        }
        public virtual void computeBoundingBox()
        {
            if (this.boundingBox == null)
            {
                this.boundingBox = new Box3();
            }
            var position = this.attributes.position;
            var morphAttributesPosition = this.morphAttributes.position;
            if (position != null && position is GLBufferAttribute)
            {
                console.error("THREE.BufferGeometry.computeBoundingBox(): GLBufferAttribute requires a manual bounding box. Alternatively set 'mesh.frustumCulled' to 'false'.", this);
                this.boundingBox.Set(
                    new Vector3(-Infinity, -Infinity, -Infinity),
                    new Vector3(+Infinity, +Infinity, +Infinity)
                );
                return;
            }
            if (position != null)
            {
                this.boundingBox.SetFromBufferAttribute(position);
                // process morph attributes if present
                if (morphAttributesPosition != null)
                {
                    var _box = getContext()._box;
                    var _vector = getContext()._vector;
                    for (int i = 0, il = morphAttributesPosition.Length; i < il; i++)
                    {
                        var morphAttribute = morphAttributesPosition[i];
                        _box.SetFromBufferAttribute(morphAttribute);
                        if (this.morphTargetsRelative)
                        {
                            _vector.AddVectors(this.boundingBox.Min, _box.Min);
                            this.boundingBox.ExpandByPoint(_vector);
                            _vector.AddVectors(this.boundingBox.Max, _box.Max);
                            this.boundingBox.ExpandByPoint(_vector);
                        }
                        else
                        {
                            this.boundingBox.ExpandByPoint(_box.Min);
                            this.boundingBox.ExpandByPoint(_box.Max);
                        }
                    }
                }
            }
            else
            {
                this.boundingBox.MakeEmpty();
            }
            if (IsNaN(this.boundingBox.Min.X) || IsNaN(this.boundingBox.Min.Y) || IsNaN(this.boundingBox.Min.Z))
            {
                console.error("THREE.BufferGeometry.computeBoundingBox(): Computed min/max have NaN values. The 'position' attribute is likely to have NaN values.", this);
            }
        }
        public virtual void computeBoundingSphere()
        {
            var ctx = getContext();
            var _box = ctx._box;
            var _boxMorphTargets = ctx._boxMorphTargets;
            var _vector = ctx._vector;
            var _offset = ctx._offset;
            if (this.boundingSphere == null)
            {
                this.boundingSphere = new Sphere();
            }
            var position = this.attributes.position;
            var morphAttributesPosition = this.morphAttributes.position;
            if (position != null && position is GLBufferAttribute)
            {
                console.error("THREE.BufferGeometry.computeBoundingSphere(): GLBufferAttribute requires a manual bounding sphere. Alternatively set 'mesh.frustumCulled' to 'false'.", this);
                this.boundingSphere.Set(new Vector3(), Infinity);
                return;
            }
            if (position != null)
            {

                // first, find the center of the bounding sphere
                var center = this.boundingSphere.Center;
                _box.SetFromBufferAttribute(position);
                // process morph attributes if present
                if (morphAttributesPosition != null)
                {
                    for (int i = 0, il = morphAttributesPosition.Length; i < il; i++)
                    {
                        var morphAttribute = morphAttributesPosition[i];
                        _boxMorphTargets.SetFromBufferAttribute(morphAttribute);
                        if (this.morphTargetsRelative)
                        {
                            _vector.AddVectors(_box.Min, _boxMorphTargets.Min);
                            _box.ExpandByPoint(_vector);
                            _vector.AddVectors(_box.Max, _boxMorphTargets.Max);
                            _box.ExpandByPoint(_vector);
                        }
                        else
                        {
                            _box.ExpandByPoint(_boxMorphTargets.Min);
                            _box.ExpandByPoint(_boxMorphTargets.Max);
                        }
                    }
                }
                _box.GetCenter(center);
                // second, try to find a boundingSphere with a radius smaller than the
                // boundingSphere of the boundingBox: sqrt(3) smaller in the best case
                double maxRadiusSq = 0;
                for (int i = 0, il = position.count; i < il; i++)
                {
                    _vector.FromBufferAttribute(position, i);
                    maxRadiusSq = Math.Max(maxRadiusSq, center.DistanceToSquared(_vector));
                }
                // process morph attributes if present
                if (morphAttributesPosition != null)
                {
                    for (int i = 0, il = morphAttributesPosition.Length; i < il; i++)
                    {
                        var morphAttribute = morphAttributesPosition[i];
                        var morphTargetsRelative = this.morphTargetsRelative;
                        for (int j = 0, jl = morphAttribute.count; j < jl; j++)
                        {
                            _vector.FromBufferAttribute(morphAttribute, j);
                            if (morphTargetsRelative)
                            {
                                _offset.FromBufferAttribute(position, j);
                                _vector.Add(_offset);
                            }
                            maxRadiusSq = Math.Max(maxRadiusSq, center.DistanceToSquared(_vector));
                        }
                    }
                }
                this.boundingSphere.Radius = Math.Sqrt(maxRadiusSq);
                if (IsNaN(this.boundingSphere.Radius))
                {
                    console.error("THREE.BufferGeometry.computeBoundingSphere(): Computed radius is NaN. The 'position' attribute is likely to have NaN values.", this);
                }
            }
        }
        public void clearBounding()
        {
            this.boundingBox = null;
            this.boundingSphere = null;
        }
        public void computeTangents()
        {
            var index = this.index;
            var attributes = this.attributes;
            // based on http://www.terathon.com/code/tangent.html
            // (per vertex tangents)
            if ((index == null ||
                 attributes.position == null ||
                 attributes.normal == null ||
                 attributes.uv == null))
            {
                console.error("THREE.BufferGeometry: .computeTangents() failed. Missing required attributes (index, position, normal or uv)");
                return;
            }
            var indices = index.intArray;
            var positions = attributes.position.array;
            var normals = attributes.normal.array;
            var uvs = attributes.uv.array;
            var nVertices = positions.Length / 3;
            if (this.hasAttribute("tangent") == false)
            {
                this.setAttribute("tangent", new Float32BufferAttribute(new double[4 * nVertices], 4));
            }
            var tangents = this.getAttribute("tangent").array;
            ListEx<Vector3> tan1 = new ListEx<Vector3>(), tan2 = new ListEx<Vector3>();
            for (int i = 0; i < nVertices; i++)
            {
                tan1[i] = new Vector3();
                tan2[i] = new Vector3();
            }
            Vector3 vA = new Vector3(), vB = new Vector3(), vC = new Vector3();
            Vector2 uvA = new Vector2(), uvB = new Vector2(), uvC = new Vector2();
            Vector3 sdir = new Vector3(), tdir = new Vector3();
            void handleTriangle(int a, int b, int c)
            {
                vA.FromArray(positions, a * 3);
                vB.FromArray(positions, b * 3);
                vC.FromArray(positions, c * 3);
                uvA.FromArray(uvs, a * 2);
                uvB.FromArray(uvs, b * 2);
                uvC.FromArray(uvs, c * 2);
                vB.Sub(vA);
                vC.Sub(vA);
                uvB.Sub(uvA);
                uvC.Sub(uvA);
                double r = 1.0 / (uvB.X * uvC.Y - uvC.X * uvB.Y);
                // silently ignore degenerate uv triangles having coincident or colinear vertices
                if (!IsFinite(r)) return;
                sdir.Copy(vB).MulScalar(uvC.Y).AddScaledVector(vC, -uvB.Y).MulScalar(r);
                tdir.Copy(vC).MulScalar(uvB.X).AddScaledVector(vB, -uvC.X).MulScalar(r);
                tan1[a].Add(sdir);
                tan1[b].Add(sdir);
                tan1[c].Add(sdir);
                tan2[a].Add(tdir);
                tan2[b].Add(tdir);
                tan2[c].Add(tdir);
            }
            var groups = this.groups;
            if (groups.Length == 0)
            {
                groups = new ListEx<GeometryGroup> {
                         new GeometryGroup{
                        Start= 0,
                        Count= indices.Length
                        }
                   };
            }
            for (int i = 0, il = groups.Length; i < il; ++i)
            {
                var group = groups[i];
                var start = group.Start;
                var count = group.Count;
                for (int j = start, jl = start + count; j < jl; j += 3)
                {
                    handleTriangle(
                        indices[j + 0],
                        indices[j + 1],
                        indices[j + 2]
                    );
                }
            }
            Vector3 tmp = new Vector3(), tmp2 = new Vector3();
            Vector3 n = new Vector3(), n2 = new Vector3();
            void handleVertex(int v)
            {
                n.FromArray(normals, v * 3);
                n2.Copy(n);
                var t = tan1[v];
                // Gram-Schmidt orthogonalize
                tmp.Copy(t);
                tmp.Sub(n.MulScalar(n.Dot(t))).Normalize();
                // Calculate handedness
                tmp2.CrossVectors(n2, t);
                double test = tmp2.Dot(tan2[v]);
                double w = (test < 0.0) ? -1.0 : 1.0;
                tangents[v * 4] = tmp.X;
                tangents[v * 4 + 1] = tmp.Y;
                tangents[v * 4 + 2] = tmp.Z;
                tangents[v * 4 + 3] = w;
            }
            for (int i = 0, il = groups.Length; i < il; ++i)
            {
                var group = groups[i];
                var start = group.Start;
                var count = group.Count;
                for (int j = start, jl = start + count; j < jl; j += 3)
                {
                    handleVertex(indices[j + 0]);
                    handleVertex(indices[j + 1]);
                    handleVertex(indices[j + 2]);
                }
            }
        }
        public void computeVertexNormals()
        {
            var index = this.index;
            var positionAttribute = this.getAttribute("position");
            if (positionAttribute != null)
            {
                var normalAttribute = this.getAttribute("normal");
                if (normalAttribute == null)
                {
                    normalAttribute = new Float32BufferAttribute(new double[positionAttribute.count * 3], 3);
                    this.setAttribute("normal", normalAttribute);
                }
                else
                {
                    // reset existing normals to zero
                    for (int i = 0, il = normalAttribute.count; i < il; i++)
                    {
                        normalAttribute.setXYZ(i, 0, 0, 0);
                    }
                }
                Vector3 pA = new Vector3(), pB = new Vector3(), pC = new Vector3();
                Vector3 nA = new Vector3(), nB = new Vector3(), nC = new Vector3();
                Vector3 cb = new Vector3(), ab = new Vector3();
                // indexed elements
                if (index != null)
                {
                    for (int i = 0, il = index.count; i < il; i += 3)
                    {
                        var vA = index.getIntX(i + 0);
                        var vB = index.getIntX(i + 1);
                        var vC = index.getIntX(i + 2);
                        pA.FromBufferAttribute(positionAttribute, vA);
                        pB.FromBufferAttribute(positionAttribute, vB);
                        pC.FromBufferAttribute(positionAttribute, vC);
                        cb.SubVectors(pC, pB);
                        ab.SubVectors(pA, pB);
                        cb.Cross(ab);
                        nA.FromBufferAttribute(normalAttribute, vA);
                        nB.FromBufferAttribute(normalAttribute, vB);
                        nC.FromBufferAttribute(normalAttribute, vC);
                        nA.Add(cb);
                        nB.Add(cb);
                        nC.Add(cb);
                        normalAttribute.setXYZ(vA, nA.X, nA.Y, nA.Z);
                        normalAttribute.setXYZ(vB, nB.X, nB.Y, nB.Z);
                        normalAttribute.setXYZ(vC, nC.X, nC.Y, nC.Z);
                    }
                }
                else
                {
                    // non-indexed elements (unconnected triangle soup)
                    for (int i = 0, il = positionAttribute.count; i < il; i += 3)
                    {
                        pA.FromBufferAttribute(positionAttribute, i + 0);
                        pB.FromBufferAttribute(positionAttribute, i + 1);
                        pC.FromBufferAttribute(positionAttribute, i + 2);
                        cb.SubVectors(pC, pB);
                        ab.SubVectors(pA, pB);
                        cb.Cross(ab);
                        normalAttribute.setXYZ(i + 0, cb.X, cb.Y, cb.Z);
                        normalAttribute.setXYZ(i + 1, cb.X, cb.Y, cb.Z);
                        normalAttribute.setXYZ(i + 2, cb.X, cb.Y, cb.Z);
                    }
                }
                this.normalizeNormals();
                normalAttribute.needsUpdate = true;
            }
        }
        public BufferGeometry merge()
        {
            console.error("THREE.BufferGeometry.merge() has been removed. Use THREE.BufferGeometryUtils.mergeBufferGeometries() instead.");
            return this;
        }
        public void normalizeNormals()
        {
            var ctx = getContext();
            var _vector = ctx._vector;
            var normals = this.attributes["normal"];
            for (int i = 0, il = normals.count; i < il; i++)
            {
                _vector.FromBufferAttribute(normals, i);
                _vector.Normalize();
                normals.setXYZ(i, _vector.X, _vector.Y, _vector.Z);
            }
        }
        public BufferGeometry toNonIndexed()
        {
            BufferAttribute convertBufferAttribute(BufferAttribute attribute, int[] tempIndices)
            {
                var array = attribute.array;
                var itemSize = attribute.itemSize;
                var normalized = attribute.normalized;
                var array2 = new double[tempIndices.Length * itemSize];
                int index = 0, index2 = 0;
                for (int i = 0, l = tempIndices.Length; i < l; i++)
                {
                    if (attribute is InterleavedBufferAttribute)
                    {
                        var interAttr = attribute as InterleavedBufferAttribute;
                        index = tempIndices[i] * interAttr.data.stride + interAttr.offset;
                    }
                    else
                    {
                        index = tempIndices[i] * itemSize;
                    }
                    for (int j = 0; j < itemSize; j++)
                    {
                        array2[index2++] = array[index++];
                    }
                }
                return new BufferAttribute(array2, itemSize, normalized);
            }
            //
            if (this.index == null)
            {
                console.warn("THREE.BufferGeometry.toNonIndexed(): BufferGeometry is already non-indexed.");
                return this;
            }
            var geometry2 = new BufferGeometry();
            var indices = this.index.intArray;
            var attributes = this.attributes;
            // attributes
            foreach (var item in attributes)
            {
                var name = item.Key;
                var attribute = attributes[name];
                var newAttribute = convertBufferAttribute(attribute, indices);
                geometry2.setAttribute(name, newAttribute);
            }
            // morph attributes
            var morphAttributes = this.morphAttributes;
            foreach (var item in morphAttributes)
            {
                var name = item.Key;
                var morphArray = new ListEx<BufferAttribute>();
                var morphAttribute = morphAttributes[name]; // morphAttribute: array of Float32BufferAttributes
                for (int i = 0, il = morphAttribute.Length; i < il; i++)
                {
                    var attribute = morphAttribute[i];
                    var newAttribute = convertBufferAttribute(attribute, indices);
                    morphArray.Push(newAttribute);
                }
                geometry2.morphAttributes[name] = morphArray;
            }
            geometry2.morphTargetsRelative = this.morphTargetsRelative;
            // groups
            var groups = this.groups;
            for (int i = 0, l = groups.Length; i < l; i++)
            {
                var group = groups[i];
                geometry2.addGroup(group.Start, group.Count, group.MaterialIndex);
            }
            return geometry2;
        }

        public BufferGeometry clone()
        {
            return new BufferGeometry().copy(this);
        }
        public BufferGeometry copy(BufferGeometry source)
        {
            // reset
            this.index = null;
            this.attributes = new BufferAttributes();
            this.morphAttributes = new MorphAttributes();
            this.groups = new ListEx<GeometryGroup>();
            this.boundingBox = null;
            this.boundingSphere = null;
            // used for storing cloned, shared data
            var data = new JsObj<object>();
            // name
            this.name = source.name;
            // index
            var index = source.index;
            if (index != null)
            {
                this.setIndex(index.clone());
            }
            // attributes
            var attributes = source.attributes;
            foreach (var item in attributes)
            {
                var name = item.Key;
                var attribute = attributes[name];
                this.setAttribute(name, attribute.clone());
            }
            // morph attributes
            var morphAttributes = source.morphAttributes;
            foreach (var item in morphAttributes)
            {
                var name = item.Key;
                var array = new ListEx<BufferAttribute>();
                var morphAttribute = morphAttributes[name]; // morphAttribute: array of Float32BufferAttributes

                for (int i = 0, l = morphAttribute.Length; i < l; i++)
                {
                    array.Push(morphAttribute[i].clone());
                }
                this.morphAttributes[name] = array;
            }
            this.morphTargetsRelative = source.morphTargetsRelative;
            // groups
            var groups = source.groups;
            for (int i = 0, l = groups.Length; i < l; i++)
            {
                var group = groups[i];
                this.addGroup(group.Start, group.Count, group.MaterialIndex);
            }
            // bounding box
            var boundingBox = source.boundingBox;
            if (boundingBox != null)
            {
                this.boundingBox = boundingBox.Clone();
            }
            // bounding sphere
            var boundingSphere = source.boundingSphere;
            if (boundingSphere != null)
            {
                this.boundingSphere = boundingSphere.Clone();
            }
            // draw range
            this.drawRange.start = source.drawRange.start;
            this.drawRange.count = source.drawRange.count;
            // user data
            this.userData = source.userData;
            return this;
        }
        public void dispose()
        {
            this.dispatchEvent(new EventArgs { type = "dispose" });
        }
        #endregion
    }
}
