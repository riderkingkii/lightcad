using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class LineLoop : Line
    {
        #region constructor
        public LineLoop(BufferGeometry geometry, params Material[] material)
            : base(geometry, material)
        {
            this.type = "LineLoop";
        }
        #endregion
    }
}
