﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public class Face3
    {
        public int a;
        public int b;
        public int c;
        public Vector3 normal = new Vector3();
        public int materialIndex = 0;
    }
}
