﻿//using OpenTK.Graphics.OpenGL;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading.Tasks;

//namespace LightCAD.Three
//{
//    public interface IMultiIndirectObject
//    {
//        void initIndexCmds();
//        GL.DrawElementsIndirectCommand[] getIndexCommands(MGeoGroup grp);
//    }
//    public class MultiIndirectMesh : MergedMesh, IMultiIndirectObject
//    {
//        private JsObj<MGeoGroup, GL.DrawElementsIndirectCommand[]> _grpIndexCommands;
//        public GL.DrawElementsIndirectCommand[] getIndexCommands(MGeoGroup grp)
//        {
//            if (grp == null)
//                return _grpIndexCommands[this.geometry.groups[0] as MGeoGroup];
//            return _grpIndexCommands[grp];
//        }

//        public MultiIndirectMesh(BufferGeometry geometry, params Material[] material) : base(geometry, material)
//        {
//            this.type = "MultiIndirectMesh";
//        }
//        public void initIndexCmds()
//        {
//            _grpIndexCommands = new JsObj<MGeoGroup, GL.DrawElementsIndirectCommand[]>();
//            var mergedGrps = this.geometry.groups;
//            for (int i = 0; i < mergedGrps.length; i++)
//            {
//                var indexCmds = new JsArr<GL.DrawElementsIndirectCommand>();
//                var mergedGrp = mergedGrps[i] as MGeoGroup;
//                if (this.materials[i].visible)
//                {
//                    var grpInfos = mergedGrp.grpInfos;
//                    for (int j = 0; j < grpInfos.length; j++)
//                    {
//                        var grpInfo = grpInfos[j];
//                        if (grpInfo.obj.visible)
//                        {
//                            var sgrp = grpInfo.childGroup;
//                            var cmd = new GL.DrawElementsIndirectCommand
//                            {
//                                count = Convert.ToUInt32(sgrp.count),
//                                instanceCount = 1,
//                                firstIndex = Convert.ToUInt32(grpInfo.startInMerge + mergedGrp.start),
//                                baseVertex = 0,//(uint)grpRangeIndexes.Min(),
//                                baseInstance = 0
//                            };
//                            indexCmds.push(cmd);
//                        }
//                        else
//                        { }
//                    }
//                }
//                _grpIndexCommands[mergedGrp] = indexCmds.ToArray();
//            }
//        }
//    }
//    public class MultiIndirectLine : MergedLine, IMultiIndirectObject
//    {
//        private JsObj<MGeoGroup, GL.DrawElementsIndirectCommand[]> _grpIndexCommands;
//        public GL.DrawElementsIndirectCommand[] getIndexCommands(MGeoGroup grp)
//        {
//            if (grp == null)
//                return _grpIndexCommands[this.geometry.groups[0] as MGeoGroup];
//            return _grpIndexCommands[grp];
//        }

//        public MultiIndirectLine(BufferGeometry geometry, params Material[] material) : base(geometry, material)
//        {
//            this.type = "MultiIndirectLine";
//        }
//        public void initIndexCmds()
//        {
//            _grpIndexCommands = new JsObj<MGeoGroup, GL.DrawElementsIndirectCommand[]>();
//            var mergedGrps = this.geometry.groups;
//            for (int i = 0; i < mergedGrps.length; i++)
//            {
//                var indexCmds = new JsArr<GL.DrawElementsIndirectCommand>();
//                var mergedGrp = mergedGrps[i] as MGeoGroup;
//                if (this.materials[i].visible)
//                {
//                    var grpInfos = mergedGrp.grpInfos;
//                    for (int j = 0; j < grpInfos.length; j++)
//                    {
//                        var grpInfo = grpInfos[j];
//                        if (grpInfo.obj.visible)
//                        {
//                            var sgrp = grpInfo.childGroup;
//                            var cmd = new GL.DrawElementsIndirectCommand
//                            {
//                                count = Convert.ToUInt32(sgrp.count),
//                                instanceCount = 1,
//                                firstIndex = Convert.ToUInt32(grpInfo.startInMerge + mergedGrp.start),
//                                baseVertex = 0,//(uint)grpRangeIndexes.Min(),
//                                baseInstance = 0
//                            };
//                            indexCmds.push(cmd);
//                        }
//                        else
//                        { }
//                    }
//                }
//                _grpIndexCommands[mergedGrp] = indexCmds.ToArray();
//            }
//        }
//    }
//}
