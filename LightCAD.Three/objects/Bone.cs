using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class Bone : Object3D
    {
        #region Properties
        #endregion

        #region constructor
        public Bone() : base()
        {
            this.type = "Bone";
        }
        #endregion
    }
}
