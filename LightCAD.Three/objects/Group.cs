using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class Group : Object3D
    {
        #region Properties
        #endregion

        #region constructor
        public Group() : base()
        {
            this.type = "Group";
        }
        #endregion
    }
}
