using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class LineSegments : Line
    {
        #region scope properties or methods
        //private static Vector3 _start = new Vector3();
        //private static Vector3 _end = new Vector3();
        private static LineSegmentsContext getContext()
            => ThreeThreadContext.GetCurrThreadContext().LineSegmentsCtx;
        #endregion

        #region Properties
        #endregion

        #region constructor
        public LineSegments(BufferGeometry geometry, params Material[] material) : base(geometry, material)
        {
            this.type = "LineSegments";
        }
        #endregion

        #region methods
        public override Line computeLineDistances()
        {
            var geometry = this.geometry;
            // we assume non-indexed geometry
            if (geometry.index == null)
            {
                var ctx = getContext();
                var _start = ctx._start;
                var _end = ctx._end;

                var positionAttribute = geometry.attributes.position;
                var lineDistances = new ListEx<double>();
                for (int i = 0, l = positionAttribute.count; i < l; i += 2)
                {
                    _start.FromBufferAttribute(positionAttribute, i);
                    _end.FromBufferAttribute(positionAttribute, i + 1);
                    lineDistances[i] = (i == 0) ? 0 : lineDistances[i - 1];
                    lineDistances[i + 1] = lineDistances[i] + _start.DistanceTo(_end);
                }
                geometry.setAttribute("lineDistance", new Float32BufferAttribute(lineDistances.ToArray(), 1));
            }
            else
            {
                console.warn("THREE.LineSegments.computeLineDistances(): Computation only possible with non-indexed BufferGeometry.");
            }
            return this;
        }
        #endregion
    }
}
