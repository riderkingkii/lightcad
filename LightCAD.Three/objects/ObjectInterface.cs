﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public interface IMaterialObject
    {
        Material getMaterial();
        void setMaterial(Material material);
        ListEx<Material> getMaterials();
        void setMaterials(ListEx<Material> materials);
        bool isMultiMaterial();
    }
    public interface IMorphTargets
    {
        ListEx<double> morphTargetInfluences { get; set; }
        JsObj<int> morphTargetDictionary { get; set; }
        void updateMorphTargets();
    }
}
