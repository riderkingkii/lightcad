using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class HemisphereLight : Light
    {
        #region Properties

        public Color groundColor;

        #endregion

        #region constructor
        public HemisphereLight(object skyColor = null, object groundColor = null, double intensity = 1)
            : base(skyColor, intensity)
        {
            this.type = "HemisphereLight";
            this.position.Copy(Object3D.DEFAULT_UP);
            this.updateMatrix();
            this.groundColor = new Color(groundColor);
        }
        #endregion

        #region methods
        public HemisphereLight copy(HemisphereLight source, bool recursive = false)
        {
            base.copy(source, recursive);
            this.groundColor.Copy(source.groundColor);
            return this;
        }
        #endregion

    }
}
