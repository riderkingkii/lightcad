using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class RectAreaLight : Light
    {
        #region Properties

        public double width;
        public double height;

        #endregion

        #region constructor
        public RectAreaLight(object color = null, double intensity = 1, double width = 10, double height = 10)
            : base(color, intensity)
        {
            this.type = "RectAreaLight";
            this.width = width;
            this.height = height;
        }
        #endregion

        #region properties
        public double power
        {
            get
            {
                // compute the light"s luminous power (in lumens) from its intensity (in nits)
                return this.intensity * this.width * this.height * MathEx.PI;
            }
            set
            {
                // set the light"s intensity (in nits) from the desired luminous power (in lumens)
                this.intensity = power / (this.width * this.height * MathEx.PI);
            }
        }
        #endregion

        #region methods
        public RectAreaLight copy(RectAreaLight source)
        {
            base.copy(source);
            this.width = source.width;
            this.height = source.height;
            return this;
        }
        #endregion

    }
}
