using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class DirectionalLightShadow : LightShadow
    {
        #region Properties

        #endregion

        #region constructor
        public DirectionalLightShadow()
            : base(new OrthographicCamera(-5, 5, 5, -5, 0.5, 500))
        {
        }
        public override LightShadow clone()
        {
            var newShadow = new DirectionalLightShadow();
            newShadow.copy(this);
            return newShadow;
        }
        #endregion

    }
}
