using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class SpotLightShadow : LightShadow
    {
        #region Properties

        public double focus;

        #endregion

        #region constructor
        public SpotLightShadow()
            : base(new PerspectiveCamera(50, 1, 0.5, 500))
        {
            this.focus = 1;
        }
        #endregion

        #region methods
        public void updateMatrices(SpotLight light)
        {
            var camera = (PerspectiveCamera)this.camera;
            var fov = MathEx.RAD2DEG * 2 * light.angle * this.focus;
            var aspect = this.mapSize.Width / this.mapSize.Height;
            var distance = light.distance;
            var far = distance == 0 ? camera.far : distance;
            if (fov != camera.fov || aspect != camera.aspect || far != camera.far)
            {
                camera.fov = fov;
                camera.aspect = aspect;
                camera.far = far;
                camera.updateProjectionMatrix();
            }
            base.updateMatrices(light);
        }
        public SpotLightShadow copy(SpotLightShadow source)
        {
            base.copy(source);
            this.focus = source.focus;
            return this;
        }
        public override LightShadow clone()
        {
            var newShadow = new SpotLightShadow();
            newShadow.copy(this);
            return newShadow;
        }
        #endregion

    }
}
