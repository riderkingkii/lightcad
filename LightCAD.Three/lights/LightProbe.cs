using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class LightProbe : Light
    {
        #region Properties

        public SphericalHarmonics3 sh;

        #endregion

        #region constructor
        public LightProbe(SphericalHarmonics3 sh = null, double intensity = 1)
            : base(null, intensity)
        {
            if (sh == null) sh = new SphericalHarmonics3();
            this.sh = sh;
        }
        #endregion

        #region methods
        public LightProbe copy(LightProbe source)
        {
            base.copy(source);
            this.sh.Copy(source.sh);
            return this;
        }

        #endregion

    }
}
