using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class SpotLight : Light, ILightShadow, ILightDistance, ILightMap
    {
        #region Properties


        public double distance;
        public double angle;
        public double penumbra;
        public double decay;
        public Texture map;
        //public SpotLightShadow shadow;

        private object _power;

        #endregion

        #region constructor
        public SpotLight(object color = null, double intensity = 1, double distance = 0, double angle = Math.PI / 3, double penumbra = 0, double decay = 2)
            : base(color, intensity)
        {
            this.type = "SpotLight";
            this.position.Copy(Object3D.DEFAULT_UP);
            this.updateMatrix();
            this.target = new Object3D();
            this.distance = distance;
            this.angle = angle;
            this.penumbra = penumbra;
            this.decay = decay;
            this.map = null;
            this.shadow = new SpotLightShadow();
        }
        #endregion

        #region properties
        public double power
        {
            get
            {
                // compute the light"s luminous power (in lumens) from its intensity (in candela)
                // by convention for a spotlight, luminous power (lm) = π * luminous intensity (cd)
                return this.intensity * MathEx.PI;
            }
            set
            {
                // set the light"s intensity (in candela) from the desired luminous power (in lumens)
                this.intensity = power / MathEx.PI;
            }
        }
        #endregion

        #region methods
        public LightShadow getShadow()
        {
            return this.shadow;
        }
        public void setShadow(LightShadow shadow)
        {
            this.shadow = shadow as SpotLightShadow;
        }
        public override void dispose()
        {
            this.shadow.dispose();
        }
        public SpotLight copy(SpotLight source, bool recursive = false)
        {
            base.copy(source, recursive);
            this.distance = source.distance;
            this.angle = source.angle;
            this.penumbra = source.penumbra;
            this.decay = source.decay;
            this.target = source.target.clone();
            this.shadow = (SpotLightShadow)source.shadow.clone();
            return this;
        }

        public void setDistance(double val)
        {
            this.distance = val;
        }

        public double getDistance()
        {
            return this.distance;
        }

        public void setMap(Texture val)
        {
            this.map = val;
        }

        public Texture getMap()
        {
            return this.map;
        }
        #endregion

    }
}
