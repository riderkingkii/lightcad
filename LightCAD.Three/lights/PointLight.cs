using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class PointLight : Light, ILightShadow
    {
        #region Properties

        public double distance;
        public double decay;
        //public PointLightShadow shadow;

        #endregion

        #region constructor
        public PointLight(object color, double intensity = 1, double distance = 0, double decay = 2)
            : base(color, intensity)
        {
            this.type = "PointLight";
            this.distance = distance;
            this.decay = decay;
            this.shadow = new PointLightShadow();
        }
        #endregion

        #region properties
        public double power
        {
            get
            {
                // compute the light"s luminous power (in lumens) from its intensity (in candela)
                // for an isotropic light source, luminous power (lm) = 4 π luminous intensity (cd)
                return this.intensity * 4 * MathEx.PI;
            }
            set
            {
                // set the light"s intensity (in candela) from the desired luminous power (in lumens)
                this.intensity = value / (4 * MathEx.PI);
            }
        }
        #endregion

        #region methods
        public LightShadow getShadow()
        {
            return this.shadow;
        }
        public void setShadow(LightShadow shadow)
        {
            this.shadow = shadow as PointLightShadow;
        }
        public override void dispose()
        {
            this.shadow.dispose();
        }
        public PointLight copy(PointLight source, bool recursive = false)
        {
            base.copy(source, recursive);
            this.distance = source.distance;
            this.decay = source.decay;
            this.shadow = (PointLightShadow)source.shadow.clone();
            return this;
        }
        #endregion

    }
}
