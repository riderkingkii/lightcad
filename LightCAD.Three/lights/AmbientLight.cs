using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class AmbientLight : Light
    {
        #region Properties

        #endregion

        #region constructor
        public AmbientLight(object color = null, double intensity = 1)
            : base(color, intensity)
        {
            this.type = "AmbientLight";
        }
        #endregion

    }
}
