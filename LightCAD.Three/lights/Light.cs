using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public interface ILightDistance
    {
        void setDistance(double val);
        double getDistance();
    }
    public interface ILightShadow
    {
        void setShadow(LightShadow val);
        LightShadow getShadow();
    }
    public interface ILightMap
    {
        void setMap(Texture val);
        Texture getMap();
    }
    public class Light : Object3D, IDispose
    {
        #region Properties

        public Color color;
        public double intensity;

        public Object3D target;
        public LightShadow shadow;
        #endregion

        #region constructor
        public Light(object color, double intensity = 1)
        {
            this.type = "Light";
            this.color = new Color(color);
            this.intensity = intensity;
        }
        #endregion

        #region methods
        public virtual void dispose()
        {
        }
        public Light copy(Light source, bool recursive = true)
        {
            base.copy(source, recursive);
            this.color.Copy(source.color);
            this.intensity = source.intensity;
            return this;
        }
        #endregion

    }
}
