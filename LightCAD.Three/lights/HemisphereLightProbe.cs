using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class HemisphereLightProbe : LightProbe
    {
        #region Properties

        #endregion

        #region constructor
        public HemisphereLightProbe(object skyColor, object groundColor, double intensity = 1)
            : base(null, intensity)
        {
            var color1 = new Color().Set(skyColor);
            var color2 = new Color().Set(groundColor);
            var sky = new Vector3(color1.R, color1.G, color1.B);
            var ground = new Vector3(color2.R, color2.G, color2.B);
            // without extra factor of PI in the shader, should = 1 / Math.Sqrt( MathEx.PI );
            var c0 = Math.Sqrt(MathEx.PI);
            var c1 = c0 * Math.Sqrt(0.75);
            this.sh.Coefficients[0].Copy(sky).Add(ground).MulScalar(c0);
            this.sh.Coefficients[1].Copy(sky).Sub(ground).MulScalar(c1);
        }
        #endregion

    }
}
