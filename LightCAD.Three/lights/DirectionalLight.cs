using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class DirectionalLight : Light, ILightShadow
    {
        #region Properties

        //public DirectionalLightShadow shadow;

        #endregion

        #region constructor
        public DirectionalLight(object color = null, double intensity = 1)
            : base(color, intensity)
        {
            this.type = "DirectionalLight";
            this.position.Copy(Object3D.DEFAULT_UP);
            this.updateMatrix();
            this.target = new Object3D();
            this.shadow = new DirectionalLightShadow();
        }
        #endregion

        #region methods
        public LightShadow getShadow()
        {
            return this.shadow;
        }
        public void setShadow(LightShadow shadow)
        {
            this.shadow = shadow as DirectionalLightShadow;
        }
        public override void dispose()
        {
            this.shadow.dispose();
        }
        public DirectionalLight copy(DirectionalLight source)
        {
            base.copy(source);
            this.target = source.target.clone();
            this.shadow = (DirectionalLightShadow)source.shadow.clone();
            return this;
        }
        #endregion

    }
}
