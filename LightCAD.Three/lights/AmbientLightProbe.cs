using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
        public class AmbientLightProbe : LightProbe
        {
            #region Properties

            #endregion

            #region constructor
            public AmbientLightProbe(object color=null, double intensity = 1)
                :base(null,intensity)
            {
                var color1 = new Color();
                if(color!=null) color1.Set(color);
                // without extra factor of PI in the shader, would be 2 / Math.Sqrt( MathEx.PI );
                this.sh.Coefficients[0].Set(color1.R, color1.G, color1.B).MulScalar(2 * Math.Sqrt(MathEx.PI));
            }
            #endregion

        }
}
