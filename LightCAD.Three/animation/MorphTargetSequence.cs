﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public class MorphTargetSequence
    {
        public string name;
        public ListEx<double> vertices;
        public ListEx<double> normals;
    }
}
