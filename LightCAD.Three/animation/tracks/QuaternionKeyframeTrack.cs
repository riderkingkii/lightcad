using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class QuaternionKeyframeTrack : KeyframeTrack
    {
        //public static Func<JObject, KeyframeTrack> parser;

        public static Func<double[], Interpolant> InterpolantFactoryMethodSmoothFunc;

        public QuaternionKeyframeTrack(string name, double[] times, double[] values, int? interpolation = null)
             : base(name, times, values, interpolation)
        {
            this.ValueTypeName = "quaternion";
            // ValueBufferType is inherited
            this.DefaultInterpolation = InterpolateLinear;
        }

        #region methods
        public override Interpolant InterpolantFactoryMethodLinear(double[] result)
        {
            return new QuaternionLinearInterpolant(this.times, this.values, this.getValueSize(), result);
        }
        public override Interpolant InterpolantFactoryMethodSmooth(double[] result)
        {
            if (InterpolantFactoryMethodSmoothFunc != null)
                return InterpolantFactoryMethodSmoothFunc(result);
            else
                return null;
        }
        #endregion

    }
}
