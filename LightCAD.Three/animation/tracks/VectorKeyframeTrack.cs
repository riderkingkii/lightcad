using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class VectorKeyframeTrack : KeyframeTrack
    {
        //public static Func<JObject, KeyframeTrack> parser;


        public VectorKeyframeTrack(string name, double[] times, double[] values, int? interpolation = null)
              : base(name, times, values, interpolation)
        {
            this.ValueTypeName = "vector";
        }
    }
}
