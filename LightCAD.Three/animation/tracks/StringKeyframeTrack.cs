using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;

namespace LightCAD.Three
{
    public class StringKeyframeTrack : KeyframeTrack
    {
        //public static Func<JObject, KeyframeTrack> parser;

        public static Func<double[], Interpolant> InterpolantFactoryMethodLinearFunc;
        public static Func<double[], Interpolant> InterpolantFactoryMethodSmoothFunc;

        public StringKeyframeTrack(string name, double[] times, double[] values, int? interpolation = null)
              : base(name, times, values, interpolation)
        {
            this.ValueTypeName = "string";
            this.ValueBufferType = "Array";
            this.DefaultInterpolation = InterpolateDiscrete;
        }
        public override Interpolant InterpolantFactoryMethodLinear(double[] result)
        {
            if (InterpolantFactoryMethodLinearFunc != null)
                return InterpolantFactoryMethodLinearFunc(result);
            else
                return null;
        }
        public override Interpolant InterpolantFactoryMethodSmooth(double[] result)
        {
            if (InterpolantFactoryMethodSmoothFunc != null)
                return InterpolantFactoryMethodSmoothFunc(result);
            else
                return null;
        }
    }
}
