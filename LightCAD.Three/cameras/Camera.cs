using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class Camera : Object3D
    {
        #region Properties

        public Matrix4 matrixWorldInverse;
        public Matrix4 projectionMatrix;
        public Matrix4 projectionMatrixInverse;
        public double zoom;
        public Vector4 viewport;
        public double near;
        public double far;
        #endregion

        #region constructor
        public Camera()
        {
            this.type = "Camera";
            this.matrixWorldInverse = new Matrix4();
            this.projectionMatrix = new Matrix4();
            this.projectionMatrixInverse = new Matrix4();
        }
        #endregion

        #region methods
        public virtual Camera copy(Camera source, bool recursive = true)
        {
            base.copy(source, recursive);
            this.matrixWorldInverse.Copy(source.matrixWorldInverse);
            this.projectionMatrix.Copy(source.projectionMatrix);
            this.projectionMatrixInverse.Copy(source.projectionMatrixInverse);
            return this;
        }
        public override Vector3 getWorldDirection(Vector3 target = null)
        {
            target = target ?? new Vector3();
            this.updateWorldMatrix(true, false);
            var e = this.matrixWorld.Elements;
            return target.Set(-e[8], -e[9], -e[10]).Normalize();
        }
        public override void updateMatrixWorld(bool force = false)
        {
            base.updateMatrixWorld(force);
            this.matrixWorldInverse.Copy(this.matrixWorld).Invert();
        }
        public override void updateWorldMatrix(bool updateParents, bool updateChildren)
        {
            base.updateWorldMatrix(updateParents, updateChildren);
            this.matrixWorldInverse.Copy(this.matrixWorld).Invert();
        }

        public virtual void updateProjectionMatrix()
        {

        }
        public virtual Camera clone()
        {
            return new Camera().copy(this);
        }
        #endregion

    }
}
