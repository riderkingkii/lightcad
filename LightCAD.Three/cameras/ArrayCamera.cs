using LightCAD.MathLib;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LightCAD.Three
{
    public class ArrayCamera : PerspectiveCamera
    {
        #region Properties

        public ListEx<PerspectiveCamera> cameras;

        #endregion

        #region constructor
        public ArrayCamera(ListEx<PerspectiveCamera> array = null)
        {
            this.cameras = array;
        }
        #endregion
    }
}
