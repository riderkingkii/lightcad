﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public class View
    {
        public bool enabled;
        public double fullWidth;
        public double fullHeight;
        public double offsetX;
        public double offsetY;
        public double width;
        public double height;

        public View clone()
        {
            return new View()
            {
                enabled = this.enabled,
                fullHeight = this.fullHeight,
                fullWidth = this.fullWidth,
                offsetX = this.offsetX,
                offsetY = this.offsetY,
                width = this.width,
                height = this.height,
            };
        }
    }
}
