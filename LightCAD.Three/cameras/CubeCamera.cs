using System;
using System.Collections;
using System.Collections.Generic;
using static LightCAD.MathLib.Constants;


namespace LightCAD.Three
{
    public class CubeCamera : Object3D
    {
        #region scope properties or methods
        private static double fov = -90; // negative fov is not an error
        private static double aspect = 1;
        #endregion

        #region Properties

        public WebGLCubeRenderTarget renderTarget;

        #endregion

        #region constructor
        public CubeCamera(double near, double far, WebGLCubeRenderTarget renderTarget)
        {
            this.type = "CubeCamera";
            this.renderTarget = renderTarget;
            var cameraPX = new PerspectiveCamera(fov, aspect, near, far);
            cameraPX.layers = this.layers;
            cameraPX.up.Set(0, 1, 0);
            cameraPX.lookAt(1, 0, 0);
            this.add(cameraPX);
            var cameraNX = new PerspectiveCamera(fov, aspect, near, far);
            cameraNX.layers = this.layers;
            cameraNX.up.Set(0, 1, 0);
            cameraNX.lookAt(-1, 0, 0);
            this.add(cameraNX);
            var cameraPY = new PerspectiveCamera(fov, aspect, near, far);
            cameraPY.layers = this.layers;
            cameraPY.up.Set(0, 0, -1);
            cameraPY.lookAt(0, 1, 0);
            this.add(cameraPY);
            var cameraNY = new PerspectiveCamera(fov, aspect, near, far);
            cameraNY.layers = this.layers;
            cameraNY.up.Set(0, 0, 1);
            cameraNY.lookAt(0, -1, 0);
            this.add(cameraNY);
            var cameraPZ = new PerspectiveCamera(fov, aspect, near, far);
            cameraPZ.layers = this.layers;
            cameraPZ.up.Set(0, 1, 0);
            cameraPZ.lookAt(0, 0, 1);
            this.add(cameraPZ);
            var cameraNZ = new PerspectiveCamera(fov, aspect, near, far);
            cameraNZ.layers = this.layers;
            cameraNZ.up.Set(0, 1, 0);
            cameraNZ.lookAt(0, 0, -1);
            this.add(cameraNZ);
        }
        #endregion

        #region methods
        public void update(WebGLRenderer renderer, Object3D scene)
        {
            if (this.parent == null) this.updateMatrixWorld();
            var renderTarget = this.renderTarget;
            var cameraPX = this.children[0] as PerspectiveCamera;
            var cameraNX = this.children[1] as PerspectiveCamera;
            var cameraPY = this.children[2] as PerspectiveCamera;
            var cameraNY = this.children[3] as PerspectiveCamera;
            var cameraPZ = this.children[4] as PerspectiveCamera;
            var cameraNZ = this.children[5] as PerspectiveCamera;
            var currentRenderTarget = renderer.getRenderTarget();
            var currentToneMapping = renderer.toneMapping;
            renderer.toneMapping = NoToneMapping;
            var generateMipmaps = renderTarget.texture.generateMipmaps;
            renderTarget.texture.generateMipmaps = false;
            renderer.setRenderTarget(renderTarget, 0);
            renderer.render(scene, cameraPX);
            renderer.setRenderTarget(renderTarget, 1);
            renderer.render(scene, cameraNX);
            renderer.setRenderTarget(renderTarget, 2);
            renderer.render(scene, cameraPY);
            renderer.setRenderTarget(renderTarget, 3);
            renderer.render(scene, cameraNY);
            renderer.setRenderTarget(renderTarget, 4);
            renderer.render(scene, cameraPZ);
            renderTarget.texture.generateMipmaps = generateMipmaps;
            renderer.setRenderTarget(renderTarget, 5);
            renderer.render(scene, cameraNZ);
            renderer.setRenderTarget(currentRenderTarget);
            renderer.toneMapping = currentToneMapping;
            renderTarget.texture.needsPMREMUpdate = true;
        }
        #endregion

    }
}
