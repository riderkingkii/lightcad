using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;


namespace LightCAD.Three
{
    public class ImageLoader : Loader<Image>
    {
        #region constructor
        public ImageLoader(LoadingManager manager) : base(manager)
        {

        }
        #endregion

        #region methods
        public override Image load(string url, Action<Image> onLoad = null, onProcessDelegate onProgress = null, onErrorDelegate onError = null)
        {
            if (this.path != null) url = this.path + url;
            url = this.manager.resolveURL(url);
            var scope = this;
            var cached = Cache.get(url) as Image;
            if (cached != null)
            {
                scope.manager.itemStart(url);
                //window.setTimeout(() =>
                //{
                //    if (onLoad != null)
                //        onLoad(cached);
                //    scope.manager.itemEnd(url);
                //}, 0);
                //客户端就不异步了
                if (onLoad != null)
                    onLoad(cached);
                scope.manager.itemEnd(url);
                return cached;
            }
            Image image = new Image();
            void onImageLoad(EventArgs e)
            {
                removeEventListeners();
                image.complete = true;
                Cache.add(url, this);
                if (onLoad != null) onLoad(image);
                scope.manager.itemEnd(url);
            }
            void onImageError(EventArgs e)
            {
                removeEventListeners();
                if (onError != null) onError(e as ErrorEvent);
                scope.manager.itemError(url);
                scope.manager.itemEnd(url);
            }
            void removeEventListeners()
            {
                image.removeEventListener("load", onImageLoad);
                image.removeEventListener("error", onImageError);
            }
            image.addEventListener("load", onImageLoad);
            image.addEventListener("error", onImageError);
            if (url.slice(0, 5) != "data:")
            {
                if (this.crossOrigin != null) image.crossOrigin = this.crossOrigin;
            }
            scope.manager.itemStart(url);
            image.loadLocal(url);
            return image;
        }
        #endregion

    }
}
