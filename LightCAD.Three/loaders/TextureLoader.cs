
using System;
using System.Collections;
using System.Collections.Generic;


namespace LightCAD.Three
{
    public class TextureLoader : Loader<Texture>
    {
        #region constructor
        public TextureLoader(LoadingManager manager=null):base(manager)
        {
        }
        #endregion

        #region methods
        public override Texture  load(string url, Action<Texture> onLoad=null, onProcessDelegate onProgress=null, onErrorDelegate onError=null)
        {
            var texture = new Texture();
            var loader = new ImageLoader(this.manager);
            loader.setCrossOrigin(this.crossOrigin);
            loader.setPath(this.path);
            loader.load(url, (image)=> {
                texture.image = image;
                texture.format = Constants.BGRAFormat;//windows��bitmap��bgra��ʽ
                texture.needsUpdate = true;
                image.flipY();

                if (onLoad != null)
                {
                    onLoad(texture);
                }
            }, onProgress, onError );
            return texture;
        }
        #endregion

    }
}
