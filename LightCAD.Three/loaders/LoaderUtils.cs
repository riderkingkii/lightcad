using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;


namespace LightCAD.Three
{
    public class LoaderUtils
    {
        #region methods
        public static string decodeText(byte[] array)
        {
            return Encoding.UTF8.GetString(array, 0, array.Length);
            //if (typeof TextDecoder != "undefined")
            //{
            //    return new TextDecoder().decode(array);
            //}
            //// Avoid the String.fromCharCode.apply(null, array) shortcut, which
            //// throws a "maximum call stack size exceeded" error for large arrays.
            //var s = "";
            //for (int i = 0, il = array.Length; i < il; i++)
            //{
            //    // Implicitly assumes little-endian.
            //    s += String.fromCharCode(array[i]);
            //}
            //try
            //{
            //    // merges multi-byte utf-8 characters.
            //    return decodeURIComponent(escape(s));
            //}
            //catch (Exception ex)
            //{ // see #16358
            //    return s;
            //}
        }
        public static string extractUrlBase(string url)
        {
            var index = url.LastIndexOf("/");
            if (index == -1) return "./";
            return url.slice(0, index + 1);
        }
        public static string resolveURL(string url, string path)
        {
            // Invalid URL
            if (url == "") return "";
            // Host Relative URL
            if (new Regex(@"^https?:\\/\\//i.test( path ) && /^\\/").IsMatch(url))
                path = path.replace(new Regex(@"(^https ?:\/\/[^\/] +).*", RegexOptions.IgnoreCase), "$1");
            // Absolute URL http://,https://,//
            if (new Regex(@"^(https?:)?\/\/", RegexOptions.IgnoreCase).IsMatch(url))
                return url;
            // Data URI
            if (new Regex(@"^data:.*,.*$", RegexOptions.IgnoreCase).IsMatch(url)) return url;
            // Blob URL
            if (new Regex(@"^blob:.*$", RegexOptions.IgnoreCase).IsMatch(url)) return url;
            // Relative URL
            return path + url;
        }
        #endregion
    }
}
