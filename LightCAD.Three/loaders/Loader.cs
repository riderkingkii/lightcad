using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace LightCAD.Three
{
        public delegate void onProcessDelegate(ProgressEvent _event);
        public delegate void onErrorDelegate(ErrorEvent _event);
        public class Loader<T>
        {
            #region Properties

            public LoadingManager manager;
            public string crossOrigin;
            public bool withCredentials;
            public string path;
            public string resourcePath;
            public string requestHeader;

            #endregion

            #region constructor
            public Loader(LoadingManager manager)
            {
                this.manager = (manager != null) ? manager : LoadingManager.DefaultLoadingManager;
                this.crossOrigin = "anonymous";
                this.withCredentials = false;
                this.path = "";
                this.resourcePath = "";
                this.requestHeader = "";
            }
            #endregion

            #region methods
            public virtual T load(string url, Action<T> onLoad, onProcessDelegate onProgress,onErrorDelegate onError)
            {
                return default;
            }

            public virtual T load<T2>(string url, Action<T, object> onLoad, onProcessDelegate onProgress, onErrorDelegate onError) where T2 : Loader<T>
            {
                return default;
            }
            public virtual async Task loadAsync(string url, Action<T> onLoad, onProcessDelegate onProgress, onErrorDelegate onError)
            {
                var scope = this;
                await Task.Run(() => scope.load(url, onLoad, onProgress, onError));
            }
            public virtual object parse(object data)
            {
                return null;
            }
            public Loader<T> setCrossOrigin(string crossOrigin)
            {
                this.crossOrigin = crossOrigin;
                return this;
            }
            public Loader<T> setWithCredentials(bool value)
            {
                this.withCredentials = value;
                return this;
            }
            public Loader<T> setPath(string path)
            {
                this.path = path;
                return this;
            }
            public Loader<T> setResourcePath(string resourcePath)
            {
                this.resourcePath = resourcePath;
                return this;
            }
            public Loader<T> setRequestHeader(string requestHeader)
            {
                this.requestHeader = requestHeader;
                return this;
            }
            #endregion

        }
}
