using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;


namespace LightCAD.Three
{
    public class ImageBitmapLoader : Loader<Object>//Bitmap
    {
        public class Options
        {
            public string premultiplyAlpha;
        }

        public Options options;
        #region constructor
        public ImageBitmapLoader(LoadingManager manager) : base(manager)
        {
            this.options = new Options
            {
                premultiplyAlpha = "none"
            };
        }
        #endregion

        #region methods
        public ImageBitmapLoader setOptions(Options options)
        {
            this.options = options;

            return this;
        }

        //public override Bitmap load(string url, Action<Bitmap> onLoad = null, onProcessDelegate onProgress = null, onErrorDelegate onError = null)
        //{
        //    if (url == null) url = string.Empty;
        //    if (this.path != null) url = this.path + url;

        //    url = this.manager.resolveURL(url);
        //    var scope = this;
        //    var cached = Cache.get(url) as Bitmap;
        //    if (cached != null)
        //    {
        //        scope.manager.itemStart(url);
        //        //window.setTimeout(() =>
        //        //{
        //        //    if (onLoad != null)
        //        //        onLoad(cached);
        //        //    scope.manager.itemEnd(url);
        //        //}, 0);
        //        //客户端就不异步了
        //        if (onLoad != null)
        //            onLoad(cached);
        //        scope.manager.itemEnd(url);
        //        return cached;
        //    }

        //    var fetchOptions = new JsObj<string>();
        //    fetchOptions["credentials"] = (this.crossOrigin == "anonymous") ? "same-origin" : "include";
        //    fetchOptions["headers"] = this.requestHeader;
        //    try
        //    {
        //        Bitmap bitmap = new Bitmap(url);
        //        Cache.add(url, bitmap);
        //        if (onLoad != null)
        //        {
        //            onLoad(bitmap);
        //        }

        //        scope.manager.itemEnd(url);
        //        return bitmap;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (onError != null)
        //        {
        //            onError(new ErrorEvent() { exception = ex });
        //        }
        //        scope.manager.itemError(url);
        //        scope.manager.itemEnd(url);
        //        return null;
        //    }
        //}
      
        #endregion

    }
}
