﻿using LightCAD.MathLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public class CubeTextureLoader : Loader<CubeTexture>
    {
        public CubeTextureLoader(LoadingManager manager = null) : base(manager)
        {
        }
        public CubeTexture load(ListEx<string> urls, Action<CubeTexture> onLoad = null, onProcessDelegate onProgress = null, onErrorDelegate onError = null)
        {
            var texture = new CubeTexture();
            var loader = new ImageLoader(this.manager);
            loader.setCrossOrigin(this.crossOrigin);
            loader.setPath(this.path);
            var loaded = 0;
            void loadTexture(int i)
            {
                loader.load(urls[i], (image) =>
                {
                    texture.images[i] = image;
                    loaded++;
                    if (loaded == 6)
                    {
                        texture.needsUpdate = true;
                        texture.format = Constants.BGRAFormat;
                        if (onLoad != null) onLoad(texture);
                    }
                }, null, onError);
            }

            for (var i = 0; i < urls.Length; ++i)
            {
                loadTexture(i);
            }
            return texture;

        }
    }
}
