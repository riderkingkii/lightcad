﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightCAD.Three
{
    public static class Cache
    {

        public static bool enabled = false;

        public static JsObj<object> files = new JsObj<object>();

        public static void add(string key, object file)
        {

            if (enabled == false) return;

            // console.log( 'THREE.Cache', 'Adding key:', key );

            files[key] = file;

        }

        public static object get(string key)
        {
            if (enabled == false)
                return null;
            // console.log( 'THREE.Cache', 'Checking key:', key );
            return files[key];
        }

        public static void remove(string key)
        {
            files.remove(key);
        }

        public static void clear()
        {
            files = new JsObj<object>();
        }
    };
}
