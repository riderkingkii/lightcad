﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    public static class IOArrayExt
    {
        public static float[] ToArray(this IIOArray ioArr, float[] array = null, int offset = 0)
        {
            var tempArry = new double[array.Length];
            ioArr.ToArray(tempArry, offset);
            for (int i = offset; i < array.Length; i++)
            {
                array[i] = (float)tempArry[i];
            }
            return array;
        }
    }
}
