﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three
{
    [StructLayout(LayoutKind.Explicit, Pack = 2)]
    public struct UnionBuffer 
    {
        /// <summary>
        /// Number of Bytes
        /// </summary>
        [FieldOffset(0)]
        public sbyte[] sbyteBuffer;
        [FieldOffset(0)]
        public byte[] byteBuffer;
        [FieldOffset(0)]
        public float[] floatBuffer;
        [FieldOffset(0)]
        public double[] doubleBuffer;
        [FieldOffset(0)]
        public short[] shortBuffer;
        [FieldOffset(0)]
        public ushort[] ushortBuffer;
        [FieldOffset(0)]
        public int[] intBuffer;
        [FieldOffset(0)]
        public uint[] uintBuffer;
        [FieldOffset(0)]
        public long[] longBuffer;
        [FieldOffset(0)]
        public ulong[] ulongBuffer;


        public static Array bytesConvert(string type, byte[] buffer)
        {
            UnionBuffer unionBuffer;
            unionBuffer.sbyteBuffer = null;

            unionBuffer.floatBuffer = null;
            unionBuffer.doubleBuffer = null;

            unionBuffer.shortBuffer = null;
            unionBuffer.ushortBuffer = null;
            
            unionBuffer.intBuffer = null;
            unionBuffer.uintBuffer = null;

            unionBuffer.longBuffer = null;
            unionBuffer.ulongBuffer = null;

            unionBuffer.byteBuffer = buffer;

            if (type == "SByte")
            {
                var arr = new sbyte[buffer.Length];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.sbyteBuffer[i];
                return arr;
            }
            else if (type == "Int16")
            {
                var arr = new short[buffer.Length / 2];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.shortBuffer[i];
                return arr;
            }
            else if (type == "UInt16")
            {
                var arr = new ushort[buffer.Length / 2];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.ushortBuffer[i];
                return arr;
            }
            else if (type == "Single")
            {
                var arr = new float[buffer.Length / 4];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.floatBuffer[i];
                return arr;
            }
            else if (type == "Double")
            {
                var arr = new double[buffer.Length / 8];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.doubleBuffer[i];
                return arr;
            }
            else if (type == "Int32")
            {
                var arr = new int[buffer.Length / 4];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.intBuffer[i];
                return arr;
            }
            else if (type == "UInt32")
            {
                var arr = new uint[buffer.Length / 4];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.uintBuffer[i];
                return arr;
            }
            else if (type == "Int64")
            {
                var arr = new long[buffer.Length / 8];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.longBuffer[i];
                return arr;
            }
            else if (type == "UInt64")
            {
                var arr = new ulong[buffer.Length / 8];
                for (var i = 0; i < arr.Length; i++) arr[i] = unionBuffer.ulongBuffer[i];
                return arr;
            }
            else
                return buffer;
        }


    }
}
