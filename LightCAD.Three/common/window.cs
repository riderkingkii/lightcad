﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LightCAD.Three
{
    public static class window
    {
        public static int innerWidth;
        public static int innerHeight;

        public static double devicePixelRatio = 1.0;
        private static int timerId = 10000000;
        private static Dictionary<int, Timer> timers = new Dictionary<int, Timer>();
        public static int setTimeout(Action callback, int interval = 0)
        {
            if (interval <= 0) interval = 1;
            var timer = new Timer(interval);
            timer.Elapsed += (s, e) =>
            {
                timer.Stop();
                timer.Dispose();
                callback();
            };
            timer.Start();
            var id = timerId++;
            timers.Add(id, timer);
            return id;
        }

        public static void clearTimeout(int handle)
        {
            if (timers.ContainsKey(handle))
            {
                var timer = timers[handle];
                timer.Stop();
                timer.Dispose();
            }
        }

        public static int setInterval(Action callback, int interval = 0)
        {
            var timer = new Timer(interval);
            timer.Elapsed += (s, e) =>
            {
                callback();
            };
            timer.Start();
            var id = timerId++;
            timers.Add(id, timer);
            return id;
        }
        public static void clearInterval(int handle)
        {
            if (timers.ContainsKey(handle))
            {
                var timer = timers[handle];
                timer.Stop();
                timer.Dispose();
            }
        }

        public static void requestAnimationFrame(object control)//GLControl
        {
            setTimeout(() => {
               // control.Invalidate();
            }, 1);
        }
    }
}
