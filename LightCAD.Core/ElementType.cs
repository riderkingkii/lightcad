﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public class ElementType
    {
        public LcGuid Guid { get; set; }
        public Type ClassType { get; set; }
        public string Name { get; set; }
        public string DispalyName { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        public  string ToDisplayName()
        {
            return this.DispalyName;
        }
    }

    public class ElementTypeCollection : KeyedCollection<LcGuid, ElementType>
    {
        protected override LcGuid GetKeyForItem(ElementType item)
        {
            return item.Guid;
        }
    }
}
