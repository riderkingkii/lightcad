﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public interface IElementSet
    {
        ElementCollection Elements { get; }

        /// <summary>
        /// 标识:是否触发元素更改事件
        /// 若为true,则在插入或删除元素时触发<see cref="Document.ObjectChanged"/>事件,以便用户可以监听到元素的变化,并注册相应的EventHandler处理;
        /// 若为false,则不触发<see cref="Document.ObjectChanged"/>事件,以便提高性能,或者在批量插入或删除元素等场景使用
        /// </summary>
        bool IsFireChangedEvent { get; set; }

        void InsertElement(LcElement element);
        bool RemoveElement(LcElement element);
    }
}
