﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Core
{
    public enum AssociateType
    {
        None,
        Cross,
        Embed,
        Contain,
        Connect
    }
    public class AssociateElement
    {
        public LcElement Element { get; set; }
        public AssociateType Type { get; set; }
        public object Tag { get; set; }
        public string Name { get; set; }
    }
    public class AssociatePoint
    {
        public string Name { get; set; }
        public Vector2 Point { get; set; }
        public List<AssociateElement> Associates { get; set; }
        public object Tag { get; set; }
    }

    public class InvokeRecord
    {
        public object Input { get; set; }
        public DateTime StartTime { get; set; }
        public IComponentInstance Invoker { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsSucc { get; set; }
        public string Error { get; set; }
        public object Output { get; set; }
    }
    public class InvokeToken
    {
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public IComponentInstance Source { get; set; }
        public List<InvokeRecord> Records { get; } = new List<InvokeRecord>();
        public DateTime EndTime { get; set; }
    }


    public interface IComponentInstance:IElement3d
    {
        void Invoke(string methodName, InvokeToken token);
        List<AssociatePoint> AssociatePoints { get; }
        List<AssociateElement> AssociateElements { get; }
        LcComponentDefinition Definition { get; }
        Solid3dCollection Solids { get; }
        Curve2dGroupCollection Curves { get; }
        LcParameterSet Parameters { get; set; }
        Transform3d Transform3d { get; }
        void ResetCache();
        void ReadProperties(ref JsonElement jele);
        void WriteProperties(Utf8JsonWriter writer, JsonSerializerOptions soptions);
    }

}
