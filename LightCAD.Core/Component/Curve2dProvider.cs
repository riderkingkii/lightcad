﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public class Curve2dProvider
    {
        /// <summary>
        ///二维表达提供者的名称 
        /// </summary>
        public string Name { get; protected set; }
        public Func<LcParameterSet, Curve2dGroupCollection> GetCurve2dsFunc;
        public Func<LcComponentDefinition, bool> IsSupportComponentFunc;
        public Curve2dProvider(string name)
        {
            this.Name = name;
        }

    }
    public class Curve2dGroup
    {
        public string Name;
        public ListEx<Curve2d> Curve2ds;

        public Curve2dGroup Clone()
        {
            Curve2dGroup clone = new Curve2dGroup()
            {
                Name = this.Name,
                Curve2ds = this.Curve2ds.Select(c => c.Clone()).ToListEx()
            };
            return clone;
        }
    }
    public class Curve2dGroupCollection : KeyedCollection<string, Curve2dGroup>
    {
        protected override string GetKeyForItem(Curve2dGroup item)
        {
            return item.Name;
        }

        public void AddRange(List<Curve2dGroup> grps)
        {
            foreach (var grp in grps)
            {
                this.Add(grp);
            }
        }
    }
}
