﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Core
{

    public class Shape2dCreator
    {
        public ListEx<Curve2d> Curves { get; }
        public Shape2dCreator()
        {
            Curves = new ListEx<Curve2d>();
        }
        public Shape2dCreator(ListEx<Curve2d> curves)
        {
            this.Curves = curves;
        }
        public Vector2 Point(double x, double y)
        {
            return new Vector2(x, y);
        }

        public Vector2 RotatePoint(Vector2 p0, Vector2 p1, double angleDegree)
        {
            return Vector2.Rotate(p0, p1, angleDegree);
        }

        public void DrawArc(Vector2 center, double radius, double startAngle, double endAngle)
        {
            var arc = new Arc2d
            {
                 Center = center,
                 Radius = radius,
                 StartAngle = Utils.DegreeToRadian( startAngle),
                 EndAngle = Utils.DegreeToRadian(endAngle),
            };
            Curves.Add(arc);
        }

        public void DrawLine(Vector2 start, Vector2 end)
        {
            var line = new Line2d
            {
                Start = start,
                End = end
            };
            Curves.Add(line);
        }
        public void DrawRect(Vector2 min, Vector2 max) 
        {
            var rect = new Rect2d() { Min = min, Max = max };
            Curves.Add(rect);
        }

        public void DrawPolygon(params Vector2[] points)
        {
            var polygon = new Polygon2d
            {
                Points = points,
            };
            Curves.Add(polygon);
        }
    }
}
