﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public class Solid3dProvider
    {
        public string Name { get; private set; }
        public Func<LcParameterSet, Solid3dCollection> GetSolid3dsFunc;
        /// <summary>
        /// 分配材质
        /// </summary>
        public Func<IComponentInstance, Solid3d, LcMaterial[]> AssignMaterialsFunc;
        public Func<LcComponentDefinition, bool> IsSupportComponentFunc;
        public Solid3dProvider(string name)
        {
            Name = name;
        }
    }
    public class Solid3dCollection : KeyedCollection<string, Solid3d>
    {
        protected override string GetKeyForItem(Solid3d item)
        {
            return item.Name;
        }
    }
}
