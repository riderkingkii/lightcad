﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LightCAD.Core
{
    public class LcBuilding : ModelSite
    {
        private LcLevelCollection levels = new LcLevelCollection();
        public LcLevelCollection Levels 
        {
            get
            {
                return levels;
            }
            set 
            {
                this.AddGroups(value.ToArray());
            }
        }

        public override ListEx<ModelGroup> Groups => Levels.ToArray().Cast<ModelGroup>().ToListEx();


        public override ModelSite AddGroups(params ModelGroup[] groups)
        {
            for (int i = 0; i < groups.Length; i++)
            {
                this.Levels.Add(groups[i] as LcLevel);
                groups[i].Parent = this;
                groups[i].DirtyType = DirtyType.Add;
            }
            return this;
        }
        public override void ReadObjectRefs<T>(ICollection<T> objs)
        {
            base.ReadObjectRefs(objs);
            foreach (var lvl in this.Levels)
            {
                lvl.ReadObjectRefs(objs);
            }
        }
    }

    public class BuildingCollection : KeyedCollection<long, LcBuilding>
    {
        protected override long GetKeyForItem(LcBuilding item)
        {
            return item.Id;
        }
    }
}