﻿using LightCAD.Core.Elements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LightCAD.Core
{
    public enum LcLevelType
    {
        /// <summary>
        /// 屋顶层
        /// </summary>
        RFloor,
        /// <summary>
        /// 地上楼层
        /// </summary>
        Floor,
        /// <summary>
        /// 地下楼层
        /// </summary>
        BFloor,
    }
    public class LcLevel :ModelGroup
    {
        private double elevation;
        /// <summary>
        /// 从零高度计算的绝对高度
        /// </summary>
        public double Elevation { get => elevation; set { this.elevation = value;this.Transform.Position.Z = elevation;this.DirtyType = DirtyType.Change; } }
        public double S_Elevation { get; set; }
        /// <summary>
        /// 本层高度
        /// </summary>
        public double Height { get; set; }
        public double S_Height { get; set; }

        public LcLevelType Type { get; set; }
        public int Index { get; set; }
        public List<LcDrawing> Drawings { get; set; }

        public LcLevel() { Drawings = new List<LcDrawing> { }; }
        public override void ReadObjectRefs<T>(ICollection<T> objs)
        {
            base.ReadObjectRefs(objs);
            var drawings = new List<LcDrawing>();
            foreach (var item in this.Drawings)
            {
                var drawing = this.Document.ModelSpace.Elements.FirstOrDefault(d => d.Id == item.Id) as LcDrawing;
                drawings.Add(drawing);
            }
            this.Drawings = drawings;
        }
       
    }

    public class LcLevelCollection : KeyedCollection<string, LcLevel>
    {
        protected override string GetKeyForItem(LcLevel item)
        {
            return item.Name;
        }
    }
}