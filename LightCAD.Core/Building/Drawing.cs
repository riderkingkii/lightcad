﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 图框引用
    /// </summary>
    public class LcDrawing : LcBlockRef
    {
        public LcDrawingFrame DrawingFrame
        {
            get
            {
                return this.Block as LcDrawingFrame;
            }
        }
        public LcBasePoint? buildingBasePoint { get; set; }
        //private LcAxisGrid axisGrid;
        public Dictionary<long,LcElement> Elements { get; set; }

        public void ResetContent()
        {
            this.Elements = new  Dictionary<long, LcElement> ();
            this.buildingBasePoint = null;
            //this.axisGrid = null;
        }
        public LcBasePoint GetBasePointElement()
        {
            return null;
        }

        public LcAxisGrid GetAxisGridElement()
        {
            return null;
        }

        public LcDrawing()
        {
            this.Type = BuiltinElementType.Drawing;
            this.Elements = new Dictionary<long, LcElement>();
        }
        public LcDrawing(LcDrawingFrame block) : this()
        {
            this.Block = block;
        }

        public override void ReadObjectRefs<T>(ICollection<T> objs)
        {
            //base.ReadObjectRefs(objs);
            var eles = new Dictionary<long, LcElement>();
            foreach (var id in this.Elements.Keys)
            {
                var ele = objs.FirstOrDefault(e => e.Id == id) as LcElement;
                if (ele != null)
                {
                    eles.Add(ele.Id, ele);
                }
            }
            this.Elements = eles;
        }

        public override void WriteProperties(Utf8JsonWriter writer, JsonSerializerOptions soptions)
        {
            base.WriteProperties(writer, soptions);
            if (this.buildingBasePoint != null)
            {
                writer.WritePropertyName(nameof(this.buildingBasePoint));
                writer.WriteElementObject(this.buildingBasePoint, soptions);
            }
            if (this.Elements != null && this.Elements.Count > 0)
            {
                writer.WriteCollectionProperty(nameof(Elements), this.Elements.Keys.ToList(), soptions);
                //writer.WriteElementSetProperty(nameof(Elements), this.Elementss.ToList(), soptions);
            }
        }
        public override void ReadProperties(ref JsonElement jele)
        {
            base.ReadProperties(ref jele);

            if (jele.TryGetProperty(nameof(this.buildingBasePoint), out JsonElement jsonEle))
            {
                var basePoint = this.Document.CreateObject<LcBasePoint>();
                basePoint.ReadProperties(ref jsonEle);
                this.buildingBasePoint = basePoint;
            }

            //Todo: axisGrid

            if (jele.TryGetProperty(nameof(this.Elements), out JsonElement eleJsonEle))
            {
                var ids = jele.ReadListProperty<long>(nameof(this.Elements));
                foreach (var id in ids)
                {
                    //var ele = this.Document.ModelSpace.Elements.FirstOrDefault(e => e.Id == id);
                    //var ele = this.Document.CreateObject<LcElement>();
                    //ele.Id = id;
                    //if (ele != null)
                    //{
                        this.Elements.Add(id, null);
                    //}
                }
                //var ele = this.Document.CreateObject
                //this.Elements = ids.Select(id => new LcElement() { Id = id })
                //this.Elements = eles.ToDictionary(K => K.Id, V => V);

                LcDocument.ResetObjectRelation(this.Document.ModelSpace.Elements.ToList());
            }
        }
    }
}