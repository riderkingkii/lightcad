﻿using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class LcSolidProfile : LcObject
    {
        public string Uuid { get; set; }
    }

    public class SolidProfileCollection : KeyedCollection<string, LcSolidProfile>
    {
        protected override string GetKeyForItem(LcSolidProfile item)
        {
            return item.Uuid;
        }
    }
}