﻿using LightCAD.Core.Elements;
using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class LcDrawingFrame : LcBlock
    {
       
    }

    public class LcDrawingFrameCollection : KeyedCollection<long, LcDrawingFrame>
    {
        protected override long GetKeyForItem(LcDrawingFrame item)
        {
            return item.Id;
        }
    }
}