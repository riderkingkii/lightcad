﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Drawing;
using System.Linq;

namespace LightCAD.Core
{
    public class LcLayer : LcObject
    {
        public bool IsStatus { get; set; } = true;
        public string Name { get; set; }
        public string Description { get; set; }
        public uint Color { get; set; }
        public bool HasOverrides { get; }//只读是方法
        public bool IsFrozen { get; set; }
        public bool IsHidden { get; set; }
        public bool IsLocked { get; set; }
        public bool IsOff { get; set; }
        public bool IsPlottable { get; set; }//是否打印
        public bool IsReconciled { get; set; }
        public bool IsUsed { get; }//只读是方法
        public int LineTypeId { get; set; }//线型Id
        public string LineTypeName { get; set; }//线型名称
        public LineWeight LineWeight { get; set; }//线宽
        public int MaterialId { get; set; }
        public string PlotStyleName { get; set; }//打印样式

        public int PlotStyleId { get; set; }//打印样式Id

        public double Transparency { get; set; }
        public int ViewportVisibilityDefault { get; set; }

    }

    public class LayerCollection : KeyedCollection<long, LcLayer>
    {
        protected override long GetKeyForItem(LcLayer item)
        {
            return item.Id;
        }
        protected override void RemoveItem(int index)
        {
            
            base.RemoveItem(index);
        }

        public LcLayer GetByName(string layer)
        {
           return this.FirstOrDefault(x => x.Name == layer);
        }
    }
}