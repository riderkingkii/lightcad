﻿using System.Collections.Generic;

namespace LightCAD.Core
{
    /// <summary>
    /// 裁剪区域
    /// </summary>
    public class LcClipArea : LcObject
    {
    }

    public class LcClipAreaCollection : List<LcClipArea>
    {
    }
}