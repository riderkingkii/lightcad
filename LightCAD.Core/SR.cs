﻿namespace LightCAD.Core
{
    internal static partial class SR
    {
        /// <summary>An error occurred at {0}, ({1}, {2}).</summary>
        internal static string @Xml_ErrorFilePosition => GetResourceString("Xml_ErrorFilePosition", @"An error occurred at {0}, ({1}, {2}).");
        internal static string @PropDef_NotFound => GetResourceString("PropDef_NotFound", @"Property named '{0}' definition not found.");
        internal static string @ElementNotInContainer => GetResourceString("ElementNotInContainer", @"Element ObjectId '{0}' is not belong container.");

    }
}
