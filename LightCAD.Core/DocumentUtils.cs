﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;
using System.Formats.Tar;
using LightCAD.Core.Element3d;
using System.Security.AccessControl;
using LightCAD.Core.Elements;

namespace LightCAD.Core
{
    public class XrefInfo
    {
        public string FilePath { get; set; }
        public XrefStatus Status { get; set; }
        public bool IsOverlay { get; set; }
        public XrefInfo[] Children { get; set; }

    }

    public class XrefGraph
    {
        public Dictionary<string, XrefInfo[]> DrawingXrefs = new Dictionary<string, XrefInfo[]>();

        public string RootilePath { get; set; }
        public XrefInfo[] RootXrefs { get; set; }

    }
    public class ComponentInfo
    {

    }
    public class ComponentGraph
    {

    }
    public static class DocumentUtils
    {
        public const string ZipDrawingName = "drawing.json";
        public static LcDocument Load(string filePath, bool asBlock = false)
        {
            var start = DateTime.Now;

            //var zipFile = ZipFile.OpenRead(filePath);
            //var zipDict = zipFile.Entries.ToDictionary((e) => e.FullName);
            //var drawingEntry = zipDict[ZipDrawingName];
            //var stream = drawingEntry.Open();
            var json = File.OpenRead(filePath);
            var doc = LcDocument.FromJsonStream(json);
            doc.FilePath = filePath;

            //Debug.Print("Load:" + (DateTime.Now - start).TotalSeconds.ToString());

            return doc;

        }
        public static void Save(LcDocument doc, string asFilePath = null, bool asBlock = false)
        {
            var start = DateTime.Now;

            string filePath = doc.FilePath;
            if (asFilePath != null) filePath = asFilePath;

            var options = new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                WriteIndented = true
            };
            //using (FileStream zipFile = new FileStream(filePath, FileMode.OpenOrCreate))
            //using (ZipArchive archive = new ZipArchive(zipFile, ZipArchiveMode.Create))
            //{
            //    ZipArchiveEntry readMeEntry = archive.CreateEntry(ZipDrawingName);
            //    using (StreamWriter writer = new StreamWriter(readMeEntry.Open()))
            //    {
            //        var stream = new MemoryStream();
            //        LcDocument.ToJsonStream(doc, stream, true);
            //        var json = Encoding.UTF8.GetString(stream.ToArray());
            //        writer.Write(json);
            //    }
            //}
            var stream = new MemoryStream();
            LcDocument.ToJsonStream(doc, stream, true);
            var json = Encoding.UTF8.GetString(stream.ToArray());
            File.WriteAllText(filePath, json);
            Debug.Print("Save:" + (DateTime.Now - start).TotalSeconds.ToString());

        }

        public static void LoadCom(LcDocument doc, string filePath)
        {
            var start = DateTime.Now;
            var json = File.OpenRead(filePath);
            LcDocument.FromComponentJsonStream(doc, json);
            Debug.Print("Load:" + (DateTime.Now - start).TotalSeconds.ToString());
        }

        public static void SaveCom(LcDocument doc, string asFilePath = null)
        {
            var start = DateTime.Now;

            string filePath = doc.FilePath;
            if (asFilePath != null) filePath = asFilePath;

            var stream = new MemoryStream();

            LcDocument.ToComponentJsonStream(doc, stream, true);
            var json = Encoding.UTF8.GetString(stream.ToArray());
            File.WriteAllText(filePath, json);
            Debug.Print("Save:" + (DateTime.Now - start).TotalSeconds.ToString());
        }

        public static XrefGraph LoadXrefGraph(string filePath)
        {
            return null;
        }
        public static ComponentGraph LoadComponentGraph(string filePath)
        {
            return null;
        }
    }
}
