﻿using System.Collections.ObjectModel;
using LightCAD.MathLib;

namespace LightCAD.Core
{
    public class LcUCS : LcObject
    {
        public string Name { get; set; }

        /// <summary>
        /// 是否正交显示
        /// </summary>
        public bool IsPlan { get; set; } = false; 

        public Vector2 Origin { get; internal set; } = new Vector2(0, 0);


        public Vector2 XAxis { get; internal set; } = new Vector2(1, 0);
        public Vector2 YAxis { get; internal set; } = new Vector2(0, 1);

        public Matrix3 Matrix { get; set; } = new Matrix3(
                                    1.0, 0.0, 0.0,
                                    0.0, 1.0, 0.0,
                                    0.0, 0.0, 1.0);

        public Matrix3 InverseMatrix { get; set; } = new Matrix3(
                                  1.0, 0.0, 0.0,
                                  0.0, 1.0, 0.0,
                                  0.0, 0.0, 1.0);



        public void Set(Vector2 origin, Vector2 xAxis, Vector2 yAxis)
        {
            Origin = origin;
            XAxis = xAxis;
            YAxis = yAxis;

            Matrix = Matrix3.GetCoord(origin, xAxis, yAxis);

            InverseMatrix = Matrix.Clone().Invert();
        }
        public void SetIdentity()
        {
            Origin = Vector2.Zero;
            XAxis = Vector2.UnitX;
            YAxis = Vector2.UnitY;

            Matrix = Matrix3.Identity;
            InverseMatrix = Matrix3.Identity;
        }
        public bool IsIdentity()
        {
            return (this.XAxis == Vector2.UnitX && this.YAxis == Vector2.UnitY && this.Origin == Vector2.Zero);
        }
    }

    public class UCSCollection : KeyedCollection<long, LcUCS>
    {
        protected override long GetKeyForItem(LcUCS item)
        {
            return item.Id;
        }
    }
}