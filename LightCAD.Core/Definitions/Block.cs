﻿using LightCAD.Core.Elements;
using System.Collections.ObjectModel;
using System.Reflection.Metadata;
using LightCAD.MathLib;

namespace LightCAD.Core
{
    public class LcBlock : ElementSpace
    {
        public static void Save(LcBlock block, string filePath)
        {
        }
        public static LcBlock Load(string filePath)
        {
            //TODO:Load(filePath,asBlock);如果作为块加载，很多内容忽略
            var doc = DocumentUtils.Load(filePath, true);
            //Merge doc.Layers
            //Merge doc.LineType
            //Merge......

            return null;
        }
         

        private Box2 boundingBox;
        public string Name { get; set; }
        public string Description { get; set; }
        public Vector2 BasePoint { get; set; }
        public bool AllowAttribute { get; set; }

        public Box2 BoundingBox
        {
            get
            {
                if (boundingBox == null)
                {
                    boundingBox = GetBoundingBox();
                }
                return boundingBox;
            }
        }
        public void ResetBoundingBox()
        {
            this.boundingBox = null;
        }
        public virtual Box2 GetBoundingBox()
        {
            return Box2.Zero;
        }

        public LcAttribute[] GetAttributes()
        {
            return null;
        }
    }

    public class LcBlockCollection : KeyedCollection<long, LcBlock>
    {
        protected override long GetKeyForItem(LcBlock item)
        {
            return item.Id;
        }
    }
}