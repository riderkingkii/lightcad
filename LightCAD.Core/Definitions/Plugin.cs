﻿using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class PluginDefinition : LcObject
    {
        public string Uuid { get; set; }
    }

    public class LcPluginDefinitionMap : KeyedCollection<string, PluginDefinition>
    {
        protected override string GetKeyForItem(PluginDefinition item)
        {
            return item.Uuid;
        }
    }
    ///// <summary>
    ///// 由CSharp语言开发的DLL组件
    ///// </summary>
    //public class DllPlugin : Plugin
    //{
    //    public string Uuid { get; set; }
    //}
    ///// <summary>
    ///// 由CSharp语言开发的脚本插件
    ///// </summary>
    //public class CsPlugin : Plugin
    //{
    //    public string Uuid { get; set; }
    //}
    ///// <summary>
    ///// 由Javascript语言开发的脚本插件
    ///// https://github.com/microsoft/ClearScript
    ///// </summary>
    //public class JsPlugin : Plugin
    //{
    //    public string Uuid { get; set; }
    //}
    ///// <summary>
    ///// 由Python语言开发的脚本插件
    ///// https://blog.csdn.net/stone0823/article/details/101794145
    ///// </summary>
    //public class PyPlugin : Plugin
    //{
    //    public string Uuid { get; set; }
    //}
    ///// <summary>
    ///// 动态组件，由约束、控制点、参数定义的组件
    ///// </summary>
    //public class DynamicPlugin : Plugin
    //{
    //    public string Uuid { get; set; }
    //}
}