﻿using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class LcDynamicBlock : LcBlock
    {
        public string Uuid { get; set; }
    }

    public class LcDynamicBlockCollection : KeyedCollection<string, LcDynamicBlock>
    {
        protected override string GetKeyForItem(LcDynamicBlock item)
        {
            return item.Uuid;
        }
    }
}