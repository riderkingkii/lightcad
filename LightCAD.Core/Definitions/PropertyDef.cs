﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public enum LcValueType
    {
        Boolean,
        Integer,
        Float,
        String,
        Json,
        Xml,
        DateTime,
        ByteArray
    }
    public class LcPropertyDefinition
    {
        public string Name { get; set; }
        public string Display { get; set; }
        public string Descr { get; set; }
        public string Format { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public LcValueType ValueType { get; set; }
        public string Unit { get; set; }
        public object DefaultValue { get; set; }
        public bool IsEffectRender { get; set; }
        public bool IsClassScope { get; set; }
        public object ValueForClass { get; set; }
    }
    public class PropertyDefinitionCollection : KeyedCollection<string, LcPropertyDefinition>
    {
        protected override string GetKeyForItem(LcPropertyDefinition item)
        {
            return item.Name;
        }
    }
    public class LcPropertyGroupDefinition
    {
        public string ObjectName { get; internal set; }
        public string Name { get; internal set; }
        public string Display { get; set; }
        public bool MergeDisplay { get; set; }

        public PropertyDefinitionCollection PropDefs { get; set; }
        public LcPropertyGroupDefinition()
        {
            this.PropDefs = new PropertyDefinitionCollection();
        }
        public LcPropertyGroupDefinition(string objectName, string name, string display) : this()
        {
            this.Name = name;
            this.ObjectName = objectName;
            this.Display = display;
        }

    }

    public class LcObjectPropertiesDefinition
    {
        public string ObjectName { get; internal set; }
        public Dictionary<string, LcPropertyGroupDefinition> GroupDefs { get; set; }
        public LcObjectPropertiesDefinition(string objectName)
        {
            ObjectName = objectName;
        }

        public LcPropertyGroupDefinition AddGroup(string name, string display)
        {
            if (GroupDefs == null)
            {
                this.GroupDefs = new Dictionary<string, LcPropertyGroupDefinition>();
            }
            var propGrp = new LcPropertyGroupDefinition(this.ObjectName, name, display);
            this.GroupDefs.Add(name, propGrp);
            return propGrp;
        }
    }


    public class ObjectPropertiesDefinitionMap : KeyedCollection<string, LcObjectPropertiesDefinition>
    {
        protected override string GetKeyForItem(LcObjectPropertiesDefinition item)
        {
            return item.ObjectName;
        }
    }
}