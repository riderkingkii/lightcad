﻿using System.Collections.ObjectModel;
using System.Reflection.Metadata;

namespace LightCAD.Core
{

    public enum XrefStatus
    {
        NotAnXref = 0,
        Resolved = 1,
        Unloaded = 2,
        Unreferenced = 3,
        FileNotFound = 4,
        Unresolved = 5
    };

   
    public class LcXref : LcBlock
    {
        public bool IsRelativePath { get; set; }
        public string FilePath { get; set; }
        public bool IsOverlay { get; set; }
        /// <summary>
        /// 后续考虑Document
        /// </summary>
        public LcDocument XrefDoc { get; set; }
        

    }

    public class XrefCollection : KeyedCollection<long, LcXref>
    {
        protected override long GetKeyForItem(LcXref item)
        {
            return item.Id;
        }
    }
}