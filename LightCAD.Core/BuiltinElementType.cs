﻿using LightCAD.Core.Element3d;
using LightCAD.Core.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public static class BuiltinElementType
    {

        public static ElementType Line = new ElementType
        {
            Guid = Guid.ParseExact("{2C38B1ED-A011-44C6-A0ED-523A14136910}", "B").ToLcGuid(),
            Name = "LINE",
            DispalyName = "直线",
            ClassType = typeof(LcLine)
        };
        public static ElementType Circle = new ElementType
        {
            Guid = Guid.ParseExact("{70DAC6C6-5866-4F7E-9AF8-5292AE6A6BF5}", "B").ToLcGuid(),
            Name = "CIRCLE",
            DispalyName = "圆",
            ClassType = typeof(LcCircle)
        };
        public static ElementType Arc = new ElementType
        {
            Guid = Guid.ParseExact("{3C4C74E0-C1B8-44A6-AD39-6B9662273395}", "B").ToLcGuid(),
            Name = "ARC",
            DispalyName = "圆弧",
        };
        public static ElementType Ray = new ElementType
        {
            Guid = Guid.ParseExact("{FB2AD397-F061-4F2F-AF9D-9467EFC72E46}", "B").ToLcGuid(),
            Name = "RAY",
            DispalyName = "射线",
            ClassType = typeof(LcRay)
        };
        public static ElementType XLine = new ElementType
        {
            Guid = Guid.ParseExact("{FCE8DE74-EE09-41C1-B147-E4119BFDB16A}", "B").ToLcGuid(),
            Name = "XLINE",
            DispalyName = "构造线",
            ClassType = typeof(LcXLine)
        };
        public static ElementType Ellipse = new ElementType
        {
            Guid = Guid.ParseExact("{838D898E-D993-4965-A46C-91757970E351}", "B").ToLcGuid(),
            Name = "ELLIPSE",
            DispalyName = "椭圆",
            ClassType = typeof(LcEllipse)
        };
        public static ElementType BlockRef = new ElementType
        {
            Guid = Guid.ParseExact("{A7AABBE6-3247-4072-AC22-FB9BDF57FD6C}", "B").ToLcGuid(),
            Name = "BLOCKREF",
            DispalyName = "块参照",
            ClassType = typeof(LcBlockRef)
        };
        public static ElementType Group = new ElementType
        {
            Guid = Guid.ParseExact("{CB7D6837-1318-4BA5-8F13-053E66963230}", "B").ToLcGuid(),
            Name = "GROUP",
            DispalyName = "组",
            ClassType = typeof(LcGroup)
        };
        public static ElementType PloyLine = new ElementType
        {
            Guid = Guid.ParseExact("{4EAD3C04-19DA-4E6D-9447-87FBEE7205DF}", "B").ToLcGuid(),
            Name = "POLYLINE",
            DispalyName = "多段线",
            ClassType = typeof(LcPolyLine)
        };

        public static ElementType Rectang = new ElementType
        {
            Guid = Guid.ParseExact("{4EAD3C04-19DA-4E6D-9447-87FBEE8205DF}", "B").ToLcGuid(),
            Name = "RECTANG",
            DispalyName = "矩形多段线",
            ClassType = typeof(LcPolyLine)
        };

        public static ElementType Polygon = new ElementType
        {
            Guid = Guid.ParseExact("{4EAD3C04-19DA-4E6D-9447-87FBEE9205DF}", "B").ToLcGuid(),
            Name = "POLYGON",
            DispalyName = "多边形",
            ClassType = typeof(LcPolyLine)
        };




        public static ElementType RoLine = new ElementType
        {
            Guid = Guid.ParseExact("{6F9619FF-8B86-D011-B42D-00C04FC964FF}", "B").ToLcGuid(),
            Name = "ROLLINE",
            DispalyName = "圆头线",
        };

        public static ElementType DimDiametric = new ElementType
        {
            Guid = Guid.ParseExact("{D20EA4E1-3957-11D2-A40B-0C5020524153}", "B").ToLcGuid(),
            Name = "DDILiINE",
            DispalyName = "直径标注",
        };

        public static ElementType DimArcLenght = new ElementType
        {
            Guid = Guid.ParseExact("{D20EA5E1-3957-11D2-A40B-0C5020524378}", "B").ToLcGuid(),
            Name = "DIMARC",
            DispalyName = "弧长标注",
        };

        public static ElementType DimOrdinate = new ElementType
        {
            Guid = Guid.ParseExact("{D20EA5E1-3957-11D2-A40B-0C5020522590}", "B").ToLcGuid(),
            Name = "DIMORDINATE",
            DispalyName = "坐标标注",
        };

        public static ElementType LcText = new ElementType
        {
            Guid = Guid.ParseExact("{45B098C1-CA00-4901-B448-C89DC043A189}", "B").ToLcGuid(),
            Name = "Text",
            DispalyName = "文字",
            ClassType = typeof(LcText)
        };
        public static ElementType DimJogged = new ElementType
        {
            Guid = Guid.ParseExact("{45B098C1-CA00-4901-B448-C89DC043A865}", "B").ToLcGuid(),
            Name = "DIMJOGGED",
            DispalyName = "折弯",
        };

        public static ElementType Point = new ElementType
        {
            Guid = Guid.ParseExact("{66B098C1-CA00-4901-B448-C89DC783A183}", "B").ToLcGuid(),
            Name = "POINT",
            DispalyName = "点",
            ClassType = typeof(Point)
        };


        public static ElementType BasePoint = new ElementType
        {
            Guid = Guid.ParseExact("{66B098C1-CA00-4901-B448-C89DC043A183}", "B").ToLcGuid(),
            Name = "BASEPOINT",
            DispalyName = "定位点",
            ClassType = typeof(LcBasePoint)
        };
        public static ElementType DirectComponent = new ElementType
        {
            Guid = Guid.ParseExact("{238CD690-0F68-4EBE-A7AB-D9901226C364}", "B").ToLcGuid(),
            Name = "DIRECTCOMPONENT",
            DispalyName = "直接组件",
            ClassType = typeof(DirectComponent)
        };
        public static ElementType ComponentRef = new ElementType
        {
            Guid = Guid.ParseExact("{D4475D15-179F-4B45-B3B1-2FC8E018C7DA}", "B").ToLcGuid(),
            Name = "COMPONENTREF",
            DispalyName = "组件参照",
            ClassType = typeof(LcComponentInstance)
        };
        public static ElementType DimRotated = new ElementType
        {
            Guid = Guid.ParseExact("{149C5A9E-5653-48F0-A7DE-59102103C5A2}", "B").ToLcGuid(),
            Name = "DIMROTATED",
            DispalyName = "线性标注",
        };
        public static ElementType DimAligned = new ElementType
        {
            Guid = Guid.ParseExact("{DA98D0FA-71D3-4F5C-9B58-757DA34BB6DE}", "B").ToLcGuid(),
            Name = "DIMALIGNED",
            DispalyName = "对齐标注",
        };
        public static ElementType QLeader = new ElementType
        {
            Guid = Guid.ParseExact("{2C38B1ED-A011-44C6-A0ED-523A14130571}", "B").ToLcGuid(),
            Name = "QLEADER",
            DispalyName = "引线",
        };
        public static ElementType DimRadial = new ElementType
        {
            Guid = Guid.ParseExact("{2C38B1ED-A011-44C6-A0ED-523A14139281}", "B").ToLcGuid(),
            Name = "DIMRADIAL",
            DispalyName = "半径标注",
        };
        public static ElementType Drawing = new ElementType
        {
            Guid = Guid.ParseExact(" {306237F7-076F-B036-84EE-DB231CF0101A}", "B").ToLcGuid(),
            Name = "DRAWING",
            DispalyName = "图框",
            ClassType = typeof(LcDrawing)
        };
        public static ElementType DimArc = new ElementType
        {
            Guid = Guid.ParseExact("{70012FDD-640F-C28D-14A4-E9ED5B4DDB28}", "B").ToLcGuid(),
            Name = "DimArc",
            DispalyName = "弧线标注",
        };
        public static ElementType DimAngularArc = new ElementType
        {
            Guid = Guid.ParseExact("{ADA176AD-AAD6-A98C-341E-B57A3A24143B}", "B").ToLcGuid(),
            Name = "DimAngularArc",
            DispalyName = "角度标注",
        };

        public static ElementType MText = new ElementType
        {
            Guid = Guid.ParseExact("{D20EA5E1-3957-11D2-A40B-0C5020524678}", "B").ToLcGuid(),
            Name = "MText",
            DispalyName = "多行文本",
        };


        public static ElementType AxisGrid = new ElementType
        {
            Guid = Guid.ParseExact("{86364F56-712E-1CA4-1237-90FFD655493E}", "B").ToLcGuid(),
            Name = "AxisGrid",
            DispalyName = "轴网",
            ClassType = typeof(LcAxisGrid)

        };
        public static ElementType AxisLine = new ElementType
        {
            Guid = Guid.ParseExact("{2D35FD98-25FC-8F93-49C4-DAEEB3D7B528}", "B").ToLcGuid(),
            Name = "AxisLine",
            DispalyName = "轴线",
            ClassType = typeof(LcAxisLine)

        };
        public static ElementType AxisCircle = new ElementType
        {
            Guid = Guid.ParseExact("{10B5863C-1265-1AA1-0F33-A1C617CE54A1}", "B").ToLcGuid(),
            Name = "AxisCircle",
            DispalyName = "轴圈",
            ClassType = typeof(LcAxisCircle)

        };
        public static ElementType Join = new ElementType
        {
            Guid = Guid.ParseExact("{E73F3A18-2571-7F59-1AB4-08FDB3FBDE89}", "B").ToLcGuid(),
            Name = "Join",
            DispalyName = "合并",
        };

        public static ElementType RasterImage = new ElementType
        {
            Guid = Guid.ParseExact("{A0D6B655-2D40-42CE-BA74-5121A41CD851}", "B").ToLcGuid(),
            Name = "RasterImage",
            DispalyName = "光栅图像",
        };


        public static ElementType Hatch = new ElementType
        {
            Guid = Guid.ParseExact("{76CFAD87-1716-EDA6-A599-07310EA10220}", "B").ToLcGuid(),
            Name = "Hatch",
            DispalyName = "填充",
        };

public static ElementType Solid3d = new ElementType
        {
            Guid = Guid.ParseExact("{389606B2-6A72-1B65-9700-A4B8FEFB3812}", "B").ToLcGuid(),
            Name = "实体",
            DispalyName = "实体",
            ClassType = typeof(LcSolid3d)
        };
        public static ElementType Section = new ElementType
        {
            Guid = Guid.ParseExact("{2C38B1ED-A011-44C6-A0ED-523A14131272}", "B").ToLcGuid(),
            Name = "Section",
            DispalyName = "截面轮廓",
            ClassType = typeof(LcSection)
        };
        public static ElementType Cube = new ElementType() 
        {
            Guid = Guid.ParseExact("{48BFDED5-350F-9D9E-EA0D-79371E18829E}", "B").ToLcGuid(),
            Name = "Cube",
            DispalyName = "立方体",
            ClassType = typeof(LcCubeInstance)
        };
        public static ElementType Cuboid = new ElementType()
        {
            Guid = Guid.ParseExact("{3595891E-A4C5-4696-8EB1-30CC1DBD455E}", "B").ToLcGuid(),
            Name = "Cuboid",
            DispalyName = "长方体",
            ClassType = typeof(LcCuboidInstance)
        };
        public static ElementType Extrude = new ElementType()
        {
            Guid = Guid.ParseExact("{CB799A93-5BA9-4D46-BC5F-3AE58241355C}", "B").ToLcGuid(),
            Name = nameof(LcExtrudeInstance),
            DispalyName = "拉伸体",
            ClassType = typeof(LcExtrudeInstance)
        };
        public static ElementType Loft = new ElementType()
        {
            Guid = Guid.ParseExact("{4F659381-A515-4A39-850F-E29697F664EF}", "B").ToLcGuid(),
            Name = nameof(LcLoftInstance),
            DispalyName = "放样体",
            ClassType = typeof(LcLoftInstance)
        };

        public static ElementType Table = new ElementType
        {
            Guid = Guid.ParseExact("{58C3196D-4A22-4B5E-BDA5-F437AFF6FA68}", "B").ToLcGuid(),
            Name = "Table",
            DispalyName = "表格",
        };

        public static ElementType TableCell = new ElementType
        {
            Guid = Guid.ParseExact("{9B2C6541-CC08-4621-BEDD-7BE882870C30}", "B").ToLcGuid(),
            Name = "TableCell",
            DispalyName = "单元格",
        };



        public static ElementType[] All = new ElementType[]
        {
            Line, 
            Circle, 
            Arc, 
            Ray, 
            XLine, 
            Ellipse, 
            BlockRef, 
            Drawing,
            Group, 
            PloyLine, 
            BasePoint, 
            QLeader, 
            DimRadial,
            DimDiametric,
            DimArcLenght,
            DimOrdinate,
            LcText,
            DimArc,
			MText,
			DimAngularArc,
            AxisGrid,
            AxisLine,
            AxisCircle,
            Join,
            RasterImage,
            Solid3d, 
            Section,
            Cube,
            Cuboid,
            Extrude,
            Loft,
            Table,
            TableCell
        };
    }
}
