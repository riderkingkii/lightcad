﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using LightCAD.MathLib;
namespace LightCAD.Core.Elements
{
    public class LcBasePoint : LcElement 
    {
        public Vector2 Point { get; set; }
        public double  Radius { get; set; }

        public LcBasePoint()
        {
            this.Type = BuiltinElementType.BasePoint;
        }

        public LcBasePoint(Vector2 point)
        {
            this.Point = point;
        }

        public override LcElement Clone()
        {
            var clone = Document.CreateObject<LcBasePoint>();
            clone.Copy(this);
            clone.Initilize(this.Document);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            base.Copy(src);
            var basePoint = ((LcBasePoint)src);
            this.Point = basePoint.Point;
        }
        public void Set(Vector2 point = null, bool fireChangedEvent = true)
        {
            //PropertySetter:Point,Radius            
            if (!fireChangedEvent)
            {
                if (point != null) this.Point = point;
            }
            else
            {
                bool chg_Point = (point != null && point != this.Point);
                if (chg_Point)
                {
                    OnPropertyChangedBefore(nameof(Point), this.Point, point);
                    var oldValue = this.Point;
                    this.Point = point;
                    OnPropertyChangedAfter(nameof(Point), oldValue, this.Point);
                }
            }
        }
        public override Box2 GetBoundingBox()
        {
            return new Box2(new Vector2(this.Point.X - this.Radius, this.Point.Y - this.Radius),
                new Vector2(this.Point.X + this.Radius, this.Point.Y + this.Radius));
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            var mcenter = matrix.MultiplyPoint(this.Point);
            var medge = matrix.MultiplyPoint(this.Point + new Vector2(0, this.Radius));
            var mradius = Vector2.Distance(mcenter, medge);

            return new Box2(new Vector2(mcenter.X - mradius, mcenter.Y - mradius),
                new Vector2(mcenter.X + mradius, mcenter.Y + mradius));
        }
        public override void Move(Vector2 startPoint, Vector2 endPoint)
        {
            var old=new Vector2().Copy(this.Point);
            this.Point.Add(endPoint - startPoint);
            this.ResetBoundingBox();
            this.OnPropertyChangedAfter(nameof(Point), old, this.Point);
        }

        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            
            Box2 thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            
            Vector2 start = this.Point + new Vector2(this.Radius, 0);
            Vector2 end = this.Point - new Vector2(this.Radius, 0);
            if (GeoUtils.IsPolygonIntersectLine(testPoly.Points,start,end))
            {
                return true;
            }
             start = this.Point + new Vector2(0, this.Radius);
             end = this.Point - new Vector2( 0, this.Radius);
            if (GeoUtils.IsPolygonIntersectLine(testPoly.Points, start, end))
            {
                return true;
            }
            if (GeoUtils.IsPolygonIntersectCircle(testPoly.Points, this.Point, this.Radius/3))
            {
                return true;
            }
            if (GeoUtils.IsPolygonIntersectCircle(testPoly.Points, this.Point, (this.Radius/3)*2))
            {
                return true;
            }
            return false;
        }


        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            //testPoly.IsConvex==false 不是凸多边形的情况目前没有考虑
            Box2 thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsCircleInPolygon(this.Point, this.Radius, testPoly.Points);
        }

        public override void WriteProperties(Utf8JsonWriter writer, JsonSerializerOptions soptions)
        {
            base.WriteProperties(writer, soptions);

            writer.WriteVector2dProperty(nameof(Point), this.Point);
            writer.WriteNumberProperty(nameof(Radius), this.Radius);
        }

        public override void ReadProperties(ref JsonElement jele)
        {
            base.ReadProperties(ref jele);

            if (jele.TryGetProperty(nameof(this.Point), out JsonElement nameProp))
                this.Point = jele.ReadVector2dProperty(nameof(this.Point));

            this.Radius = jele.ReadDoubleProperty(nameof(Radius));
        }
    }
}
