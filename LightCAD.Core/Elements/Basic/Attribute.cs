﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 属性标签
    /// </summary>
    public class LcAttribute : LcElement
    {
        public LcAttribute()
        {
        }

        public override LcElement Clone()
        {
            var clone = new LcAttribute();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var attr = ((LcAttribute)src);
        }


    }
}
