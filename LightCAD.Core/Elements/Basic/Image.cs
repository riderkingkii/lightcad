﻿namespace LightCAD.Core.Elements
{
    public class Image : LcElement
    {
        public Image()
        {
        }

        public override LcElement Clone()
        {
            var clone = new Image();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var img = (Image)src;
        }


    }
}
