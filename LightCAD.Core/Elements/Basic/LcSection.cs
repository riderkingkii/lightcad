using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using LightCAD.MathLib;

namespace LightCAD.Core.Elements
{
    public class LcSection : LcCurve2d
    {
        public string Uuid { get; set; }

        public string Name { get; set; }
        //private List<Curve2d> segments;
        public List<Curve2d> Segments { get; set; } = new List<Curve2d>();
        public List<List<Curve2d>> InnerSegments { get; set; } = new List<List<Curve2d>>() { };
        private Vector2 min = new Vector2();
        private Vector2 max = new Vector2();
        public LcSection()
        {
            this.Type = BuiltinElementType.Section;
            this.Curve = new Line2d(new Vector2(),new Vector2() );
        }
        public LcSection(bool IsClosed, List<Curve2d> _segments) : this()
        {
            ResetSegments( );
        }
        public override void Translate(Vector2 vector)
        {
            this.Segments.Select(n=>n.Translate(vector));
            this.InnerSegments.Select(n =>n.Select(m=>m.Translate(vector)));
        }
        public override void Translate(double dx, double dy)
        {
            this.Segments.Select(n => n.Translate(dx,dy));
            this.InnerSegments.Select(n => n.Select(m => m.Translate(dx, dy)));
        }
        public void ResetSegments( )
        {
            if (Segments.Count==0)
            {
                return;
            }
            var ps = Segments.SelectMany(n => {
                if (n is Line2d line)
                    return new List<Vector2> { line.Start, line.End };
                else
                {
                    var arc = n as Arc2d;           
                    return arc.GetPoints(GetArcDiv(arc)).ToList();
                }
            }).ToList();
            min = new Vector2(ps.Min(n => n.X), ps.Min(n => n.Y));
            max = new Vector2(ps.Max(n => n.X), ps.Max(n => n.Y));
        }
        private int GetArcDiv(Arc2d arc)
        {
            var count = (int)Math.Ceiling( normalize(arc.IsClockwise ? arc.StartAngle - arc.EndAngle : arc.EndAngle - arc.StartAngle) * arc.Radius / 10);
            count = count < 32 ? 32 : count;
            return count;
        }
        /// <summary>
        /// 将角度标准化为0~2π之间
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        private double normalize(double angle)
        {
            while (angle < 0) angle += Utils.TwoPI;
            while (angle > Utils.TwoPI) angle -= Utils.TwoPI;
            return angle;
        }
        public override LcElement Clone()
        {
            var clone = new LcSection();
            clone.Copy(this);
            clone.Initilize(this.Document);
            return clone;
        }
        public override void Copy(LcElement src)
        {
            base.Copy(src);
            var pline = ((LcSection)src);
            this.Segments = pline.Segments.Select(n=>n.Clone()).ToList();
            this.InnerSegments = pline.InnerSegments.Select(n => n.Select(m=>m.Clone()).ToList()).ToList();
        }

        public override void WriteProperties(Utf8JsonWriter writer, JsonSerializerOptions soptions)
        {
            //base.WriteProperties(writer, soptions);

            //writer.WriteCollectionProperty(nameof(this.Segments), this.Segments, soptions);
            //writer.WriteCollectionProperty(nameof(this.InnerSegments), this.InnerSegments, soptions);

        }

        public override void ReadProperties(ref JsonElement jele)
        {
            //base.ReadProperties(ref jele);
            //this.Segments = (List<Curve2d>)jele.ReadListProperty<Curve2d>(nameof(this.Segments));
            //this.InnerSegments = (List<List<Curve2d>>)jele.ReadListProperty<Curve2d>(nameof(this.InnerSegments));
        }
        public override Box2 GetBoundingBox()
        {
            ResetSegments();
             return new Box2(min,max);
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            ResetSegments();
            return new Box2(matrix.MultiplyPoint(min), matrix.MultiplyPoint(max));
        }
        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                //如果元素盒子，与多边形盒子不相交，那就可能不相交
                return false;
            }
            for (var i = 0; i < this.Segments.Count; i++)
            {
                var curve = Segments[i];
                if (curve is Line2d line)
                {
                    if (GeoUtils.IsPolygonIntersectLine(testPoly.Points,line.Start,line.End))
                    {
                        return true;
                    }
                }
                else
                {
                    var arc = curve as Arc2d;
                    var points = arc.GetPoints(GetArcDiv(arc));
                    for (var j=1;j<points.Count();j++)
                    {
                        if (GeoUtils.IsPolygonIntersectLine(testPoly.Points, points[j-1], points[j]))
                        {
                            return true;
                        }
                    }
                }
            }
            for (var i = 0; i < this.InnerSegments.Count; i++)
            {
                var curves= InnerSegments[i];

                foreach (var curve in curves)
                {
                    if (curve is Line2d line)
                    {
                        if (GeoUtils.IsPolygonIntersectLine(testPoly.Points, line.Start, line.End))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        var arc = curve as Arc2d;
                        var points = arc.GetPoints(GetArcDiv(arc));
                        for (var j = 1; j < points.Count(); j++)
                        {
                            if (GeoUtils.IsPolygonIntersectLine(testPoly.Points, points[j - 1], points[j]))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                //如果元素盒子，与多边形盒子不相交，那就可能不相交
                return false;
            }
            for (var i = 0; i < this.Segments.Count; i++)
            {
                var curve = Segments[i];
                if (curve is Line2d line)
                {
                    if (GeoUtils.IsPolygonContainsLine(testPoly.Points, line.Start, line.End))
                    {
                        return false;
                    }
                }
                else
                {
                    var arc = curve as Arc2d;
                    var points = arc.GetPoints(GetArcDiv(arc));
                    for (var j = 1; j < points.Count(); j++)
                    {
                        if (GeoUtils.IsPolygonContainsLine(testPoly.Points, points[j - 1], points[j]))
                        {
                            return false;
                        }
                    }
                }
            }
            for (var i = 0; i < this.InnerSegments.Count; i++)
            {
                var curves = InnerSegments[i];
                foreach (var curve in  curves)
                {
                    if (curve is Line2d line)
                    {
                        if (GeoUtils.IsPolygonContainsLine(testPoly.Points, line.Start, line.End))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        var arc = curve as Arc2d;
                        var points = arc.GetPoints(GetArcDiv(arc));
                        for (var j = 1; j < points.Count(); j++)
                        {
                            if (GeoUtils.IsPolygonContainsLine(testPoly.Points, points[j - 1], points[j]))
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;

        }
    }
}