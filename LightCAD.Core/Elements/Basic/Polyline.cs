﻿namespace LightCAD.Core.Elements
{
    public class LcPolyline : LcElement
    {

        public LcPolyline()
        {
        }

        public override LcElement Clone()
        {
            var clone = new LcPolyline();
            clone.Copy(this);
            return clone;

        }

        public override void Copy(LcElement src)
        {
            var pline = ((LcPolyline)src);

        }


    }
}
