using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using LightCAD.MathLib;

namespace LightCAD.Core.Elements
{
    public class LcQLeader : LcElement
    {
        [JsonInclude, JsonPropertyName("S")]
        [JsonConverter(typeof(Vector2dConverter))]
        public Vector2 Start;
        [JsonInclude, JsonPropertyName("E")]
        [JsonConverter(typeof(Vector2dConverter))]
        public Vector2 End;

        public List<QleaderSegment> Segments;
        public List<Vector2> ArrowPoints;
        public LcQLeader()
        {
            this.Type = BuiltinElementType.QLeader;
        }
        public LcQLeader(List<QleaderSegment> Segments, List<Vector2> ArrowPoints) : this()
        {
            this.Segments = Segments;
            this.ArrowPoints = ArrowPoints;
            this.Start= this.Segments.FirstOrDefault().Start;
            this.End = this.Segments.LastOrDefault().End;
        }
        public override Box2 GetBoundingBox()
        {
            return new Box2().SetFromPoints(this.Start, this.End);
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            return new Box2().SetFromPoints(matrix.MultiplyPoint(this.Start), matrix.MultiplyPoint(this.End));
        }
        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                //如果元素盒子，与多边形盒子不相交，那就可能不相交
                return false;
            }
            return GeoUtils.IsPolygonIntersectLine(testPoly.Points, this.Start, this.End)
                || GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        }

        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        }
        public override LcElement Clone()
        {
            throw new NotImplementedException();
        }
        public override void Copy(LcElement src)
        {
            base.Copy(src);
            var qleader = ((LcQLeader)src);
            this.Segments = qleader.Segments;
        }
        public class QleaderSegment
        {
            public Vector2 Start;
            public Vector2 End;
            public double StartAngle;
            public double EndAngle;
        }
    }
}