﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml.Linq;
using LightCAD.Core.Elements;
using LightCAD.MathLib;
namespace LightCAD.Core.Elements
{
    public class LcGroup : LcComposite
    {
        private bool isSelected;
        public List<long> eleIds { get; set; }
        public bool IsPlck { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public override bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                this.isSelected = value;
                foreach (var ele in this.Elements)
                {
                    ele.IsSelected = value;
                }
            }
        }

        public LcGroup()
        {
            this.Type = BuiltinElementType.Group;
            this.IsFireChangedEvent = true;
            eleIds=new List<long>();
        }


        public override void Copy(LcElement src)
        {
            base.Copy(src);
            var grp = src as LcGroup;
            this.Name = grp.Name;
            this.Description = grp.Description;
        }
        public override LcElement Clone()
        {
            var clone = this.Document.CreateObject<LcGroup>();
            clone.Copy(this);
            return clone;
        }

        public override void WriteProperties(Utf8JsonWriter writer, JsonSerializerOptions soptions)
        {
            if (!string.IsNullOrEmpty(this.Name))
                writer.WriteStringProperty(nameof(Name), this.Name);

            if (!string.IsNullOrEmpty(this.Description))
                writer.WriteStringProperty(nameof(Description), this.Description);

            writer.WritePropertyName(nameof(Elements));
            writer.WriteStartArray();
            foreach (var subEle in this.Elements)
            {
                writer.WriteElementObject(subEle, soptions);
            }
            writer.WriteEndArray();
        }
        public override void ReadProperties(ref JsonElement jele)
        {
            if (jele.TryGetProperty(nameof(this.Name), out JsonElement nameProp))
                this.Name = nameProp.GetString();

            if (jele.TryGetProperty(nameof(this.Description), out JsonElement descrProp))
                this.Description = descrProp.GetString();

            var arr = jele.GetProperty(nameof(Elements)).EnumerateArray().ToArray();
            for (var i = 0; i < arr.Length; i++)
            {
                var ele = arr[i].ReadElementObject(this.Document);
                this.Elements.Add(ele);
            }
        }

        public void Set(string name = null, bool fireChangedEvent = true)
        {
            //PropertySetter:Start,End
            if (!fireChangedEvent)
            {
                if (Name != null) this.Name = name;
            }
            else
            {
                bool chg_start = (Name != null && name != this.Name);
                if (chg_start)
                {
                    OnPropertyChangedBefore(nameof(Name), this.Name, name);
                    var oldValue = this.Name;
                    this.Name = name;
                    OnPropertyChangedAfter(nameof(Name), oldValue, this.Name);
                }
            }
        }
    }

    public class GroupCollection : KeyedCollection<long, LcGroup>
    {
        protected override long GetKeyForItem(LcGroup item)
        {
            return item.Id;
        }
    }
}