using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using LightCAD.MathLib;
namespace LightCAD.Core.Elements
{
    public class LcCircle : LcCurve2d
    {

        public Vector2 Center { get => this.Circle.Center; set => this.Circle.Center = value; }
        public double Radius { get => this.Circle.Radius; set => this.Circle.Radius = value; }
        public Circle2d Circle => this.Curve as Circle2d;
        public LcCircle()
        {
            this.Type = BuiltinElementType.Circle;
            this.Curve = new Circle2d();
        }
        public LcCircle(Vector2 center, double radius) : this()
        {
            this.Center = center;
            this.Radius = radius;
        }
        public LcCircle(double centerX, double centerY, double radius) : this()
        {
            this.Center.X = centerX;
            this.Center.Y = centerY;
            this.Radius = radius;
        }

        public override LcElement Clone()
        {
            var clone = this.Document.CreateObject<LcCircle>();
            clone.Copy(this);
            clone.Initilize(this.Document);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            base.Copy(src);
            var circle = ((LcCircle)src);
            this.Center = circle.Center;
            this.Radius = circle.Radius;
        }

        public void Set(Vector2 center = null, double? radius = null, bool fireChangedEvent = true)
        {
            //PropertySetter:Center,Radius            
            if (!fireChangedEvent)
            {
                if (center != null) this.Center = center;
                if (radius != null) this.Radius = radius.Value;
            }
            else
            {
                bool chg_center = (center != null && center != this.Center);
                if (chg_center)
                {
                    OnPropertyChangedBefore(nameof(Center), this.Center, center);
                    var oldValue = this.Center;
                    this.Center = center;
                    OnPropertyChangedAfter(nameof(Center), oldValue, this.Center);
                }
                bool chg_radius = (radius != null && radius != this.Radius);
                if (chg_radius)
                {
                    OnPropertyChangedBefore(nameof(Radius), this.Radius, radius);
                    var oldValue = this.Radius;
                    this.Radius = radius.Value;
                    OnPropertyChangedAfter(nameof(Radius), oldValue, this.Radius);
                }
            }
        }

        public override LcElement ApplyMatrix(Matrix3 matrix)
        {
            var leftp = Center - new Vector2(Radius, 0);
            this.Center = matrix.MultiplyPoint(this.Center);
            this.Radius = (matrix.MultiplyPoint(leftp) - this.Center).Length();
            return this;
        }
        public override Curve2d[] GetCurves()
        {
            return new Curve2d[] { new Circle2d {  Source = this } };
        }
        public override Box2 GetBoundingBox()
        {
            return new Box2(new Vector2(this.Center.X - this.Radius, this.Center.Y - this.Radius),
                new Vector2(this.Center.X + this.Radius, this.Center.Y + this.Radius));
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            var mcenter = matrix.MultiplyPoint(this.Center);
            var medge = matrix.MultiplyPoint(this.Center + new Vector2(0, this.Radius));
            var mradius = Vector2.Distance(mcenter, medge);

            return new Box2(new Vector2(mcenter.X - mradius, mcenter.Y - mradius),
                new Vector2(mcenter.X + mradius, mcenter.Y + mradius));
        }

        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {

            Box2 thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsPolygonIntersectCircle(testPoly.Points, this.Center, this.Radius);
        }


        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            //testPoly.IsConvex==false 不是凸多边形的情况目前没有考虑
            Box2 thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsCircleInPolygon(this.Center, this.Radius, testPoly.Points);
        }

        public override void Translate(double dx, double dy)
        {
            var tc = new Vector2(this.Center.X + dx, this.Center.Y + dy);
            Set(center: tc);
            ResetBoundingBox();
        }

        public override void Scale(Vector2 basePoint, double scaleFactor)
        {
            Matrix3 matrix3 = Matrix3.GetScale(scaleFactor, basePoint);
            Set(center: matrix3.MultiplyPoint(this.Center), radius: this.Radius * scaleFactor);
            ResetBoundingBox();
        }
        public override void Scale(Vector2 basePoint, Vector2 scaleVector)
        {
          //  var LcCircle = element as LcCircle;
            Matrix3 matrix3 = Matrix3.GetScale(scaleVector, basePoint);
            Set(center: matrix3.MultiplyPoint(this.Center), radius: this.Radius * Vector2.Distance(basePoint, scaleVector));
            ResetBoundingBox();
        }

        public override void Rotate(Vector2 basePoint, double rotateAngle)
        {
            Matrix3 matrix3 = Matrix3.RotateInRadian(rotateAngle, basePoint);
            this.Set(center: matrix3.MultiplyPoint(this.Center));
            ResetBoundingBox();
        }
        public override void Mirror(Vector2 axisStart, Vector2 axisEnd)
        {
            Matrix3 matrix3 = Matrix3.GetMirror(axisEnd, axisStart);
            this.Set(center:matrix3.MultiplyPoint(this.Center));
            ResetBoundingBox();
        }
        public override void WriteProperties(Utf8JsonWriter writer, JsonSerializerOptions soptions)
        {
            writer.WriteVector2dProperty(nameof(this.Center), this.Center);
            writer.WriteNumberProperty(nameof(this.Radius), this.Radius);
        }

        public override void ReadProperties(ref JsonElement jele)
        {
            this.Center = jele.ReadVector2dProperty(nameof(Center));
            this.Radius = jele.ReadDoubleProperty(nameof(Radius));
        }


        public override List<Vector2> GetCrossVectorByLine(Line2d element)
        {
            List<Vector2> points = GeoUtils.GetCrossCircleLine(element.Start, element.End, this.Center, this.Radius);
            return points;
            //List<Vector2> points2 = new List<Vector2>();
            //if (points.Count != 0)
            //{
            //    foreach (Vector2 point in points)
            //    {
            //        if (GeoUtils.IsPointOnLineExtension(element.Start, element.End, point))
            //        {
            //        }
            //        else
            //        {
            //            points2.Add(point);
            //        }
            //    }
            //}
            //return points2;
        }
 
        public override List<Vector2> GetCrossVectorByCircle(Circle2d ele)
        {
            List<Vector2> points = GeoUtils.CircleCrossCircle(this.Circle, ele);
            return points;
        }
        public override List<Vector2> GetCrossVectorByArc(Arc2d arc2D)  //相当于是圆与圆之间交点的计算
        {
            List<Vector2> points = GeoUtils.CircleCrossArc(this.Circle, arc2D);

            //List<Vector2> endpoints = new List<Vector2>();

            //foreach (var item in points)
            //{
            //     //&& GeoUtils.IsPointOnArcExtension(arc2D, item)


            //List<Vector2> endpoints = new List<Vector2>();
            //foreach (var item in points)
            //{
            //    //&& GeoUtils.IsPointOnArcExtension(arc2D, item)

            //    if (!Arc2d.PointInArc(item, arc2D.Startp, arc2D.Endp, arc2D.Center))
            //    {
            //        endpoints.Add(item);
            //    }
            //}

            //return endpoints;
            return points;
        }
        public override bool PointInElement(Vector2 point, double epsilon = 0.001)
        {
            Vector2 center = this.Center;
    
            if (Math.Abs(Math.Sqrt(Math.Pow(center.X - point.X, 2) + Math.Pow(center.Y - point.Y, 2)) - this.Radius)<epsilon)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

}