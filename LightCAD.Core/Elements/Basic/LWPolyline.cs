﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 无宽度多段线
    /// </summary>
    public class LWPolyline : LcElement
    {

        public LWPolyline()
        {
        }


        public override LcElement Clone()
        {
            var clone = new LWPolyline();
            clone.Copy(this);
            return clone;

        }

        public override void Copy(LcElement src)
        {
            var lwline = ((LWPolyline)src);
        }


    }
}
