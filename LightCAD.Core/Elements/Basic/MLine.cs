﻿namespace LightCAD.Core.Elements
{
    public class MLine : LcElement
    {
        public MLine()
        {
        }

        public override LcElement Clone()
        {
            var clone = new MLine();
            clone.Copy(this);
            return clone;

        }

        public override void Copy(LcElement src)
        {
            var mline = ((MLine)src);
        }


    }
}
