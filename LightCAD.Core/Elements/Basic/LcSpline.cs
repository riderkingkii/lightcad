﻿namespace LightCAD.Core.Elements
{
    public class LcSpline : LcCurve2d
    {
        public LcSpline()
        {
        }

        public override LcElement Clone()
        {
            var clone = new LcSpline();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var spline = ((LcSpline)src);
        }


    }
}
