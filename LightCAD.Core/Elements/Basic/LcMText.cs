﻿namespace LightCAD.Core.Elements
{
    public class LcMText : LcElement,ILcText
    {
        public Vector2 Firstpoint;
        public Vector2 Endpoint;
        public Box2 Box2D;
        public string Mtext;
        public string Typeface;
        public LcMText()
        {
            this.Type = BuiltinElementType.MText;
        }

        public override LcElement Clone()
        {
            var clone = new LcMText();
            clone.Copy(this);
            return clone;

        }

        public override void Copy(LcElement src)
        {
            var mtext = ((LcMText)src);
        }

        public void Set(Vector2 start = null, Vector2 end = null, bool fireChangedEvent = true)
        {
            //PropertySetter:Center,Radius            
            if (!fireChangedEvent)
            {
                if (start != null) this.Firstpoint = start;
                if (end != null) this.Endpoint = end;
            }
            else
            {
                bool chg_start = (start != null && start != this.Firstpoint);
                if (chg_start)
                {
                    OnPropertyChangedBefore(nameof(start), this.Firstpoint, start);
                    var oldValue = this.Firstpoint;
                    this.Firstpoint = start;
                    OnPropertyChangedAfter(nameof(start), oldValue, this.Firstpoint);
                }
                bool chg_end = (end != null && end != this.Endpoint);
                if (chg_end)
                {
                    OnPropertyChangedBefore(nameof(end), this.Endpoint, end);
                    var oldValue = this.Endpoint;
                    this.Endpoint = end;
                    OnPropertyChangedAfter(nameof(end), oldValue, this.Endpoint);
                }
                this.Box2D= new Box2().SetFromPoints(this.Firstpoint, this.Endpoint);
            }
        }

        public override Box2 GetBoundingBox()
        {
            return new Box2().SetFromPoints(this.Firstpoint, this.Endpoint);
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            return new Box2().SetFromPoints(matrix.MultiplyPoint(this.Firstpoint), matrix.MultiplyPoint(this.Endpoint));
        }

        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                //如果元素盒子，与多边形盒子不相交，那就可能不相交
                return false;
            }
            return GeoUtils.IsPolygonIntersectLine(testPoly.Points, this.Firstpoint, this.Endpoint)
                || GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Firstpoint, this.Endpoint);
        }

        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Firstpoint, this.Endpoint);
        }


    }
}
