﻿namespace LightCAD.Core.Elements
{
    public class Solid : LcElement
    {
        public Solid()
        {
        }


        public override LcElement Clone()
        {
            var clone = new Solid();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var solid = ((Solid)src);
        }


    }
}
