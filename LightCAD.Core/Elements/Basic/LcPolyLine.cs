using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using LightCAD.MathLib;

namespace LightCAD.Core.Elements
{
    public class LcPolyLine : LcElement
    {
        public int CurrentIndex = 0; //属性面板显示使用
        public bool IsClosed { get ; set ; }
        public List<Curve2d> Curve2ds { get => this.PolyLine.Curve2ds; set => this.PolyLine.Curve2ds = value; }
        public Polyline2d PolyLine ;
        public LcPolyLine()
        {
            this.Type = BuiltinElementType.PloyLine;
            this.PolyLine = new Polyline2d();
        }

        public Vector2 GetCurrentPoint()
        {
            Vector2 point = null;
            if (this.CurrentIndex == this.Curve2ds.Count)
            {
                point = this.Curve2ds[this.CurrentIndex - 1].GetPoints().Last();
            }
            else
            {
                point = this.Curve2ds[this.CurrentIndex].GetPoints().First();
            }
            return point;
        }

        public void SetCurrentCurvePos(Vector2 pos)
        {
            if (this.CurrentIndex == 0)
            {
                GetStart(this.Curve2ds[0]).Copy(pos);
            }
            else if (this.CurrentIndex == this.Curve2ds.Count)
            {
                GetEnd(this.Curve2ds[this.CurrentIndex - 1]).Copy(pos);
            }
            else
            {
                GetEnd(this.Curve2ds[this.CurrentIndex - 1]).Copy(pos);
                GetStart(this.Curve2ds[this.CurrentIndex]).Copy(pos);
            }
        }
        public static Vector2 GetStart(Curve2d cur2d)
        {
            if (cur2d is Line2d)
            {
             return (cur2d as Line2d).Start;
            }
            else
            {
                return (cur2d as Arc2d).Startp;
            }
            
        }
        public static Vector2 GetEnd(Curve2d cur2d)
        {
            if (cur2d is Line2d)
            {
                return (cur2d as Line2d).End;
            }
            else
            {
                return (cur2d as Arc2d).Endp;
            }
        }
        public LcPolyLine(bool IsClosed, List<Curve2d> Curve2ds) : this()
        {
            this.IsClosed = IsClosed;
            this.Curve2ds = Curve2ds;
        }
        public override LcElement Clone()
        {
            var clone = Document.CreateObject<LcPolyLine>();
            clone.Copy(this);
            return clone;
        }
        public override void Copy(LcElement src)
        {
            base.Copy(src);
            var pline = ((LcPolyLine)src);
            this.IsClosed = pline.IsClosed;
            this.Curve2ds = pline.Curve2ds;
        }
        public enum PlineSegmentType
        {
            Line = 0,
            Arc = 1,

        }
        public class PlineSegment
        {
            public PlineSegmentType Type;
            public Vector2 Start;
            public Vector2 End;
            public Vector2 Center;
            public double StartAngle;
            public double EndAngle;
            public double Angle;
            public double StartWidth;
            public double EndWidth;
            public double Radius;
        }

        public override Box2 GetBoundingBox()
        {
            Vector2 s = new Vector2();
            Vector2 e = new Vector2();
            Box2 box2 = new Box2();
            List<Box2> boxes = new List<Box2>();
            List<Vector2> points = new List<Vector2>();
            foreach (Curve2d curve2d in this.Curve2ds)
            {
                if (curve2d.Type ==  Curve2dType.Arc2d)
                {
                    Arc2d arc2 = curve2d as Arc2d;
                    LcArc lcArc = new LcArc();
                    lcArc.Center = arc2.Center;
                    lcArc.Radius = arc2.Radius;
                    lcArc.StartAngle = arc2.StartAngle;
                    lcArc.EndAngle = arc2.EndAngle;
                    lcArc.Startp = arc2.Startp;
                    lcArc.Endp = arc2.Endp;
                    lcArc.Midp = arc2.Midp;
                    box2 = GetArcBoundingBox(lcArc);
                }
                else
                {
                    Line2d line2D = curve2d as Line2d;
                    LcLine lcLine = new LcLine();
                    lcLine.Start = line2D.Start;
                    lcLine.End = line2D.End;
                    box2 = new Box2().SetFromPoints(lcLine.Start, lcLine.End);
                }
                boxes.Add(box2);
            }
            foreach (Box2 box in boxes)
            {
                points.Add(box.LeftBottom);
                points.Add(box.RightTop);
            }
            List<Vector2> sortPointsByX = points.OrderBy(p => p.X).ToList();
            List<Vector2> sortPointsByY = points.OrderBy(p => p.Y).ToList();
            double minx = sortPointsByX[0].X;
            double miny = sortPointsByY[0].Y;
            double maxx = sortPointsByX[sortPointsByX.Count-1].X;
            double maxy = sortPointsByY[sortPointsByY.Count-1].Y;
            return new Box2().SetFromPoints(new Vector2(minx, miny), new Vector2(maxx, maxy));
        }
        //public override Box2 GetBoundingBox(Matrix3 matrix)
        //{
        //    //var mcenter = matrix.MultiplyPoint(this.Center);
        //    //var medge = matrix.MultiplyPoint(this.Center + new Vector2(0, this.Radius));
        //    //var mradius = Vector2.Distance(mcenter, medge);

        //    //return new Box2(new Vector2(mcenter.X - mradius, mcenter.Y - mradius),
        //    //    new Vector2(mcenter.X + mradius, mcenter.Y + mradius));
        //}
        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            Box2 thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            else
            {
                return true;
            }
            //Vector2[] vector2s = new Vector2[4] { thisBox.LeftTop, thisBox.LeftBottom, thisBox.RightBottom, thisBox.RightTop };
            //return GeoUtils.IsPolygonContainsPolygon(testPoly.Points, vector2s);
        }
        public override bool IncludedByBox(Polygon2d testPoly,List<RefChildElement> includeChildren =null)
        {
            Box2 thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            Vector2[] vector2s = new Vector2[4] { thisBox.LeftTop, thisBox.LeftBottom, thisBox.RightBottom, thisBox.RightTop };
            return GeoUtils.IsPolygonContainsPolygon(testPoly.Points, vector2s);
        }

        public override void WriteProperties(Utf8JsonWriter writer, JsonSerializerOptions soptions)
        {
            base.WriteProperties(writer, soptions);

            writer.WriteBoolProperty(nameof(this.IsClosed), this.IsClosed);
            writer.WriteCollectionProperty(nameof(this.Curve2ds), this.Curve2ds, soptions);
        }

        public override void ReadProperties(ref JsonElement jele)
        {
            base.ReadProperties(ref jele);

            this.IsClosed = jele.ReadBoolProperty(nameof(this.IsClosed)) ?? this.IsClosed;
            this.Curve2ds = (List<Curve2d>)jele.ReadListProperty<Curve2d>(nameof(this.Curve2ds));
        }

        public override List<Vector2> GetCrossVectorByLine(Line2d line2D)
        {
            Vector2 point = new Vector2();
            List<Vector2> points = new List<Vector2>();
            foreach (var element in this.Curve2ds) //遍历选中的多段线的每一段
            {
                if (element.Type == Curve2dType.Line2d)
                {
                    Line2d line2 = new Line2d(line2D.Start, line2D.End);
                    points.Add(GeoUtils.GetCrossVector(element as Line2d, line2));
                }
                if(element.Type == Curve2dType.Arc2d)
                {
                    points.AddRange(GeoUtils.GetCrossLineArc(line2D.Start, line2D.End, (element as Arc2d).Center, (element as Arc2d).Radius));
                }
            }
            List<Vector2> newPoints = new List<Vector2>();
            List<Vector2> endPoints = new List<Vector2>();
            foreach (var item in points)
            {
                foreach(var element in this.Curve2ds)
                {
                    if(element is Line2d)
                    {
                        //判断点是否在线上
                        if (item != null)
                        {
                            if (GeoUtils.IsPointOnLineExtension(line2D.Start, line2D.End, item) == false&& !GeoUtils.IsPointOnLineExtension(line2D.Start, line2D.End, point))
                            {
                                newPoints.Add(item);
                            }
                        }
                    }
                    else      //如果element is Arc2d
                    {
                        if (item != null)
                        {
                            if(!Arc2d.PointInArc(item, (element as Arc2d).Startp, (element as Arc2d).Endp, (element as Arc2d).Center) && !GeoUtils.IsPointOnLineExtension(line2D.Start, line2D.End, point))
                            {
                                newPoints.Add(item);
                            }
                        }
                    }
                }
            }
            //在这里加判断多段线是不是闭合的，然后判断交点在不在每一段多段线上（如闭合，则交点不在多段线上的不能延伸过去）
            if (this.IsClosed)//如果多段线是闭合的
            {
                foreach (var item in newPoints)
                {
                    foreach (var element in this.Curve2ds)
                    {
                            if (element is Line2d)
                            {
                                if (GeoUtils.IsPointOnLineExtension((element as Line2d).Start, (element as Line2d).End, item) == true)
                                {
                                    endPoints.Add(item);
                                }
                            }
                            if (element is Arc2d)
                            {
                                if (Arc2d.PointInArc(item, (element as Arc2d).Startp, (element as Arc2d).Endp, (element as Arc2d).Center))
                                {
                                    endPoints.Add(item);
                                }
                            }
                        
                    }
                }
                return endPoints;
            }
            else
            {
                return newPoints;
            }
        }

        public override List<Vector2> GetCrossVectorByArc(Arc2d arc)
        {
            Vector2 point = new Vector2();
            List<Vector2> points = new List<Vector2>();
            foreach (var element in this.Curve2ds)
            {
                if (element.Type == Curve2dType.Line2d)
                {
                    points.AddRange(GeoUtils.GetCrossLineArc((element as Line2d).Start, (element as Line2d).End, arc.Center, arc.Radius));
                }
                if(element.Type == Curve2dType.Arc2d)
                {
                    points.AddRange(GeoUtils.ArcCrossArc(element as Arc2d, arc));
                }
            }
            List<Vector2> newPoints = new List<Vector2>();
            List<Vector2> endPoints = new List<Vector2>();
            foreach (var item in points)
            {
                foreach (var element in this.Curve2ds)
                {
                    if (element is Line2d)
                    {
                        //判断点是否在线上
                        if (item != null)
                        {
                            //if(GeoUtils.IsTopInLine((element as Line2d).Start, (element as Line2d).End, item)==0)
                            if (GeoUtils.IsPointOnLineExtension((element as Line2d).Start, (element as Line2d).End, item) == false && !GeoUtils.IsPointOnArcExtension(arc, item))
                            {
                                newPoints.Add(item);
                            }
                        }
                    }
                    else      //如果element is Arc2d
                    {
                        if (item != null)
                        {
                            if (!Arc2d.PointInArc(item, (element as Arc2d).Startp, (element as Arc2d).Endp, (element as Arc2d).Center) && !GeoUtils.IsPointOnArcExtension(arc, item))
                            {
                                newPoints.Add(item);
                            }
                        }
                    }
                }
            }
            return newPoints;
            ////在这里加判断多段线是不是闭合的，然后判断交点在不在每一段多段线上（如闭合，则交点不在多段线上的不能延伸过去）
            //if (this.IsClosed)//如果多段线是闭合的
            //{
            //    foreach (var item in newPoints)
            //    {
            //        foreach (var element in this.Curve2ds)
            //        {
            //            if (element is Line2d)
            //            {
            //                if (GeoUtils.IsPointOnLineExtension((element as Line2d).Start, (element as Line2d).End, item) == true)
            //                {
            //                    endPoints.Add(item);
            //                }
            //            }
            //            if (element is Arc2d)
            //            {
            //                if (Arc2d.PointInArc(item, (element as Arc2d).Startp, (element as Arc2d).Endp, (element as Arc2d).Center))
            //                {
            //                    endPoints.Add(item);
            //                }
            //            }

            //        }
            //    }
            //    return endPoints;
            //}
            //else
            //{
            //    return newPoints;
            //}
        }
        public override List<Vector2> GetCrossVectorByCircle(Circle2d circle2D)
        {
            Vector2 point = new Vector2();
            List<Vector2> points = new List<Vector2>();
            foreach (var element in this.Curve2ds)
            {
                if (element.Type == Curve2dType.Line2d)
                {
                    points.AddRange(GeoUtils.GetCrossCircleLine((element as Line2d).Start, (element as Line2d).End, circle2D.Center, circle2D.Radius));
                }
                if (element.Type == Curve2dType.Arc2d)
                {
                   points.AddRange(GeoUtils.CircleCrossArc(circle2D,(element as Arc2d)));
                }
            }
            return points;

        }

        public double GetDegreesByTwoLine(Vector2 line1start, Vector2 line1End, Vector2 line2start, Vector2 line2End)
        {
            double x1 = line1start.X;
            double y1 = line1start.Y;
            double x2 = line1End.X;
            double y2 = line1End.Y;
            double x3 = line2start.X;
            double y3 = line2start.Y;
            double x4 = line2End.X;
            double y4 = line2End.Y;

            // 计算线段的向量表示
            double v1x = x2 - x1;
            double v1y = y2 - y1;
            double v2x = x4 - x3;
            double v2y = y4 - y3;

            // 计算向量的内积
            double dotProduct = v1x * v2x + v1y * v2y;


            // 计算向量的长度
            double magnitudeV1 = Math.Sqrt(v1x * v1x + v1y * v1y);
            double magnitudeV2 = Math.Sqrt(v2x * v2x + v2y * v2y);
            double angleDegrees = Math.Acos(dotProduct / (magnitudeV1 * magnitudeV2)) * 180 / Math.PI;
            if (y4 < y1)
            {
                angleDegrees = 360 - angleDegrees;
            }

            //// 计算夹角余弦值
            //double cosine = dotProduct / (magnitudeV1 * magnitudeV2);

            //// 将夹角余弦值转换为角度
            //double angleRadians = Math.Acos(cosine);
            //double angleDegrees = angleRadians * 180 / Math.PI;
            return angleDegrees;
        }
        //    [JsonInclude, JsonPropertyName("E")]
        //    [JsonConverter(typeof(Vector2dConverter))]
        //    public Vector2d plEndPoint;
        //    public List<LcElement> plEntities;
        //    //private VdConstPlineFlag mFlag;
        //    //private vdHatchProperties mHatchProperties;
        //    //private VdConstSplineFlag mSplineFlag;
        //    [JsonInclude, JsonPropertyName("S")]
        //    [JsonConverter(typeof(Vector2dConverter))]
        //    public Vector2d plStartPoint;
        //    ////private Vertexes mVertexes;
        //    //private Double mWeights;
        //    public LcPolyLine()
        //    {
        //        this.Type = ElementType.PloyLine;
        //    }
        //    public LcPolyLine(Vector2d start, Vector2d end) : this()
        //    {
        //        this.plStartPoint = start;
        //        this.plEndPoint = end;
        //    }
        //    public LcPolyLine(double startX, double startY, double endX, double endY) : this()
        //    {
        //        this.plStartPoint.X = startX;
        //        this.plStartPoint.Y = startY;
        //        this.plEndPoint.X = endX;
        //        this.plEndPoint.Y = endY;
        //    }
        //    //private double GetAngel(Vector2d p, Vector2d basep)
        //    //{
        //    //    int X = Convert.ToInt32(p.X);
        //    //    int Y = Convert.ToInt32(p.Y);

        //    //    Vector2d a1 = new Vector2d(X, Y);
        //    //    Vector2d a2 = new Vector2d(X, 300);
        //    //    Vector2d b1 = new Vector2d(X, Y);
        //    //    Vector2d b2 = new Vector2d(basep.X, basep.Y);
        //    //    var a = Math.Atan2(a2.Y - a1.Y, a2.X - a1.X);
        //    //    var b = Math.Atan2(b2.Y - b1.Y, b2.X - b1.X);
        //    //    double angle = 180 * (b - a) / Math.PI;
        //    //    return (angle > 0 ? angle : angle + 360);
        //    //}

        //    public override LcElement Clone()
        //    {
        //        var clone = Document.CreateObject<LcPolyLine>();
        //        clone.Copy(this);
        //        clone.Initilize(this.Document);
        //        return clone;

        //    }

        //    public override void Copy(LcElement src)
        //    {
        //        base.Copy(src);
        //        var line = ((LcPolyLine)src);
        //        this.plStartPoint = line.plStartPoint;
        //        this.plEndPoint = line.plEndPoint;
        //        this.plEntities = line.plEntities;
        //    }

        //    public void Set(Vector2d? start = null, Vector2d? end = null, bool fireChangedEvent = true)
        //    {
        //        //PropertySetter:Start,End
        //        if (!fireChangedEvent)
        //        {
        //            if (start != null) this.Start = start;
        //            if (end != null) this.End = end;
        //        }
        //        else
        //        {
        //            bool chg_start = (start != null && start != this.Start);
        //            if (chg_start)
        //            {
        //                OnPropertyChangedBefore(nameof(Start), this.Start, start);
        //                var oldValue = this.Start;
        //                this.Start = start;
        //                OnPropertyChangedAfter(nameof(Start), oldValue, this.Start);
        //            }
        //            bool chg_end = (end != null && end != this.End);
        //            if (chg_end)
        //            {
        //                OnPropertyChangedBefore(nameof(End), this.End, end);
        //                var oldValue = this.End;
        //                this.End = end;
        //                OnPropertyChangedAfter(nameof(End), oldValue, this.End);
        //            }
        //        }
        //    }
        //    public override Box2d GetBoundingBox()
        //    {
        //        return new Box2d(this.Start, this.End);
        //    }
        //    public override  Box2d GetBoundingBox(Matrix3 matrix)
        //    {
        //        return new Box2d(matrix.MultiplyPoint(this.Start),matrix.MultiplyPoint(this.End));
        //    }
        //    public override bool IntersectWithBox(Polygon2d testPoly, List<LcElement> intersectChildren = null)
        //    {
        //        var thisBox = this.BoundingBox;
        //        if (!thisBox.IntersectWith(testPoly.BoundingBox))
        //        {
        //            //如果元素盒子，与多边形盒子不相交，那就可能不相交
        //            return false;
        //        }
        //        return GeoUtils.IsPolygonIntersectLine(testPoly.Points, this.Start, this.End)
        //            || GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        //    }

        //    public override bool IncludedByBox(Polygon2d testPoly, List<LcElement> includedChildren = null)
        //    {
        //        var thisBox = this.BoundingBox;
        //        if (!thisBox.IntersectWith(testPoly.BoundingBox))
        //        {
        //            return false;
        //        }
        //        return GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        //    }

        //public override void Translate(double dx, double dy)
        //{
        //    var ts = new Vector2(this.Start.X + dx, this.Start.Y + dy);
        //    var te = new Vector2(this.End.X + dx, this.End.Y + dy);
        //    Set(ts, te);
        //    ResetBoundingBox();
        //}
        //public override void Move(Vector2 startPoint, Vector2 endPoint)
        //{
        //    Matrix3 matrix3 = Matrix3.GetMove(startPoint, endPoint);
        //    this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
        //    ResetBoundingBox();
        //}
        //public override void Scale(Vector2 basePoint, double scaleFactor)
        //{
        //    Matrix3 matrix3 = Matrix3.Scale(scaleFactor, basePoint);
        //    this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
        //    ResetBoundingBox();
        //}
        //public override void Scale(Vector2 basePoint, Vector2 scaleVector)
        //{

        //    Matrix3 matrix3 = Matrix3.Scale(scaleVector, basePoint);
        //    this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
        //    ResetBoundingBox();
        //}
        //public override void Rotate(Vector2 basePoint, double rotateAngle)
        //{
        //    Matrix3 matrix3 = Matrix3.RotateInRadian(rotateAngle, basePoint);
        //    this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
        //    ResetBoundingBox();
        //}
        //public override void Mirror(Vector2 axisStart, Vector2 axisEnd)
        //{
        //    Matrix3 matrix3 = Matrix3.Mirror(axisEnd, axisStart);
        //    this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
        //    ResetBoundingBox();
        //    }
        //}
        //}
        /// <summary>
        /// 获取圆弧的包围盒
        /// </summary>
        /// <param name="lcArc"></param>
        /// <returns></returns>
        public Box2 GetArcBoundingBox(LcArc lcArc)
        {
            Vector2 centerX = new(lcArc.Center.X + 500, lcArc.Center.Y);
            double cMaxX = 0;
            double cMaxY = 0;
            double cMinX = 0;
            double cMinY = 0;
            double start = GetDegreesByTwoLine(lcArc.Center, centerX, lcArc.Center, lcArc.Startp);
            double end = GetDegreesByTwoLine(lcArc.Center, centerX, lcArc.Center, lcArc.Endp);
            double Radius = lcArc.Radius;
            if (start <= 90 && start >= 0 && end <= 90 && end >= 0) //起点端点都在    
            {
                if (start < end)
                {
                    cMaxX = lcArc.Startp.X;
                    cMinX = lcArc.Endp.X;
                    cMaxY = lcArc.Endp.Y;
                    cMinY = lcArc.Startp.Y;
                }
                if (end < start)
                {
                    cMaxY = lcArc.Center.Y + Radius;
                    cMinY = lcArc.Center.Y - Radius;
                    cMaxX = lcArc.Center.X + Radius;
                    cMinX = lcArc.Center.X - Radius;
                }
            }
            if (start > 90 && start <= 180 && end > 90 && end <= 180)
            {
                if (start < end)
                {
                    cMaxX = lcArc.Startp.X;
                    cMinX = lcArc.Endp.X;
                    cMinY = lcArc.Endp.Y;
                    cMaxY = lcArc.Startp.Y;
                }
                if (end < start)
                {
                    cMaxY = lcArc.Center.Y + Radius;
                    cMinY = lcArc.Center.Y - Radius;
                    cMaxX = lcArc.Center.X + Radius;
                    cMinX = lcArc.Center.X - Radius;
                }
            }
            if (start > 180 && start <= 270 && end > 180 && end <= 270)
            {
                if (start < end)
                {
                    cMinX = lcArc.Startp.X;
                    cMaxX = lcArc.Endp.X;
                    cMinY = lcArc.Endp.Y;
                    cMaxY = lcArc.Startp.Y;
                }
                if (end < start)
                {
                    cMaxY = lcArc.Center.Y + Radius;
                    cMinY = lcArc.Center.Y - Radius;
                    cMaxX = lcArc.Center.X + Radius;
                    cMinX = lcArc.Center.X - Radius;
                }
            }
            if (start > 270 && start <= 360 && end > 270 && end <= 360)
            {
                if (start < end)
                {
                    cMinX = lcArc.Startp.X;
                    cMaxX = lcArc.Endp.X;
                    cMaxY = lcArc.Endp.Y;
                    cMinY = lcArc.Startp.Y;
                }
                if (end < start)
                {
                    cMaxY = lcArc.Center.Y + Radius;
                    cMinY = lcArc.Center.Y - Radius;
                    cMaxX = lcArc.Center.X + Radius;
                    cMinX = lcArc.Center.X - Radius;
                }
            }
            if (start <= 90 && start >= 0 && end > 90 && end <= 180) //端点在第二象限 起点第一象限
            {
                cMaxX = lcArc.Startp.X;
                cMaxY = lcArc.Center.Y + Radius;
                cMinX = lcArc.Endp.X;
                if (lcArc.Startp.Y <= lcArc.Endp.Y)
                {
                    cMinY = lcArc.Startp.Y;
                }
                if (lcArc.Startp.Y > lcArc.Endp.Y)
                {
                    cMinY = lcArc.Endp.Y;
                }
            }
            if (start > 90 && start <= 180 && end <= 90 && end >= 0) //起点在第二象限，端点在第一象限
            {
                cMaxX = lcArc.Center.X + Radius;
                cMinX = lcArc.Center.X - Radius;
                cMinY = lcArc.Center.Y - Radius;
                if (lcArc.Startp.Y >= lcArc.Endp.Y)
                {
                    cMaxY = lcArc.Startp.Y;
                }
                else
                {
                    cMaxY = lcArc.Endp.Y;
                }
            }
            if (start <= 90 && start >= 0 && end > 180 && end <= 270) //起点第一象限，端点在第三象限
            {
                cMaxX = lcArc.Startp.X;
                cMaxY = lcArc.Center.Y + Radius;
                cMinX = lcArc.Center.X - Radius;
                cMinY = lcArc.Endp.Y;
            }
            if (start > 180 && start <= 270 && end > 0 && end <= 90) //起点在第三象限 端点在第一象限
            {
                cMaxX = lcArc.Center.X + Radius;
                cMinX = lcArc.Startp.X;
                cMaxY = lcArc.Endp.Y;
                cMinY = lcArc.Center.Y - Radius;
            }
            if (start <= 90 && start >= 0 && end > 270 && end <= 360)//起点在第一象限 端点在第四象限
            {
                if (lcArc.Startp.X >= lcArc.Endp.X)
                {
                    cMaxX = lcArc.Startp.X;
                }
                else
                {
                    cMaxX = lcArc.Endp.X;
                }
                cMaxY = lcArc.Center.Y + Radius;
                cMinX = lcArc.Center.X - Radius;
                cMinY = lcArc.Center.Y - Radius;
            }
            if (start > 270 && start <= 360 && end >= 0 && end <= 90) //起点在第四象限，端点在第一象限
            {
                cMaxX = lcArc.Center.X + Radius;
                if (lcArc.Startp.X <= lcArc.Endp.X)
                {
                    cMinX = lcArc.Startp.X;
                }
                else
                {
                    cMinX = lcArc.Endp.X;
                }
                cMaxY = lcArc.Endp.Y;
                cMinY = lcArc.Startp.Y;
            }
            if (start > 90 && start <= 180 && end > 180 && end <= 270) //起点第二象限，端点在第三象限
            {
                if (lcArc.Startp.X >= lcArc.Endp.X)
                {
                    cMaxX = lcArc.Startp.X;
                }
                else
                {
                    cMaxX = lcArc.Endp.X;
                }
                cMinX = lcArc.Center.X - Radius;
                cMinY = lcArc.Endp.Y;
                cMaxY = lcArc.Startp.Y;
            }
            if (start > 180 && start <= 270 && end > 90 && end <= 180) //起点第三象限，端点在第二象限
            {
                cMaxX = lcArc.Center.X + Radius;
                cMaxY = lcArc.Center.Y + Radius;
                if (lcArc.Startp.X <= lcArc.Endp.X)
                {
                    cMinX = lcArc.Startp.X;
                }
                else
                {
                    cMinX = lcArc.Endp.X;
                }
                cMinY = lcArc.Center.Y - Radius;
            }
            if (start > 90 && start <= 180 && end > 270 && end <= 360) //起点第二象限，端点在第四象限
            {
                cMaxX = lcArc.Endp.X;
                cMinX = lcArc.Center.X - Radius;
                cMaxY = lcArc.Startp.Y;
                cMinY = lcArc.Center.Y - Radius;
            }
            if (start > 270 && start <= 360 && end > 90 && end <= 180) //起点在第四象限，端点在第二象限
            {
                cMaxX = lcArc.Center.X + Radius;
                cMinX = lcArc.Endp.X;
                cMaxY = lcArc.Center.Y + Radius;
                cMinY = lcArc.Startp.Y;
            }
            if (start > 180 && start <= 270 && end > 270 && end <= 360) //起点在第三象限，端点在第四象限
            {
                cMaxX = lcArc.Endp.X;
                cMinX = lcArc.Startp.X;
                if (lcArc.Startp.Y >= lcArc.Endp.Y)
                {
                    cMaxY = lcArc.Startp.Y;
                }
                else
                {
                    cMaxY = lcArc.Endp.Y;
                }
                cMinY = lcArc.Center.Y - Radius;
            }
            if (start > 270 && start <= 360 && end > 180 && end <= 270) //起点在第四象限，端点在第三象限
            {
                cMaxX = lcArc.Center.X + Radius;
                cMinX = lcArc.Center.X - Radius;
                cMaxY = lcArc.Center.Y + Radius;
                if (lcArc.Startp.Y >= lcArc.Endp.Y)
                {
                    cMinY = lcArc.Endp.Y;
                }
                else
                {
                    cMinY = lcArc.Startp.Y;
                }
            }
            return new Box2().SetFromPoints(new Vector2(cMinX, cMinY), new Vector2(cMaxX, cMaxY));
        }
    }
}