﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Elements.Basic
{
    public class LcPoint : LcCurve2d
    {
        public Point2d Point2d=> (this.Curve as Point2d);
        public Vector2 Point { get => Point2d.XY; set { this.Point2d.XY = value; } }
        public LcPoint()
        {
            this.Type = BuiltinElementType.Point;
            this.Curve = new Point2d();
        }
        public LcPoint(Vector2 point) : this()
        {
            this.Point = point;
        }

        public void Set(Vector2 point = null, bool fireChangedEvent = true)
        {
            //PropertySetter:Start,End
            if (!fireChangedEvent)
            {
                if (point != null) this.Point = point;
            }
            else
            {
                {
                    OnPropertyChangedBefore(nameof(Point), this.Point, point);
                    var oldValue = this.Point;
                    this.Point = point;
                    OnPropertyChangedAfter(nameof(Point), oldValue, this.Point);
                }
            }
        }
    }
}
