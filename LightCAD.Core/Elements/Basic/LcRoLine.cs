using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using LightCAD.MathLib;

namespace LightCAD.Core.Elements
{
    public class LcRoLine : LcElement
    {
        public List<RolineSegment> Segments;
        public LcRoLine()
        {
            this.Type = BuiltinElementType.RoLine;
        }
        public LcRoLine( List<RolineSegment> Segments) : this()
        {
            this.Segments = Segments;
        }
        public override LcElement Clone()
        {
            throw new NotImplementedException();
        }
        public override void Copy(LcElement src)
        {
            base.Copy(src);
            var pline = ((LcRoLine)src);
            this.Segments = pline.Segments;
        }
       
        public class RolineSegment
        {
           
            public Vector2 Start;
            public Vector2 End;
            public Vector2 Second;
        }
        public override Box2 GetBoundingBox()
        {
            return new Box2().SetFromPoints(this.Segments.FirstOrDefault().Start, this.Segments.LastOrDefault().End);
        }
    }

    //    [JsonInclude, JsonPropertyName("E")]
    //    [JsonConverter(typeof(Vector2dConverter))]
    //    public Vector2d plEndPoint;
    //    public List<LcElement> plEntities;
    //    //private VdConstPlineFlag mFlag;
    //    //private vdHatchProperties mHatchProperties;
    //    //private VdConstSplineFlag mSplineFlag;
    //    [JsonInclude, JsonPropertyName("S")]
    //    [JsonConverter(typeof(Vector2dConverter))]
    //    public Vector2d plStartPoint;
    //    ////private Vertexes mVertexes;
    //    //private Double mWeights;
    //    public LcPolyLine()
    //    {
    //        this.Type = ElementType.PloyLine;
    //    }
    //    public LcPolyLine(Vector2d start, Vector2d end) : this()
    //    {
    //        this.plStartPoint = start;
    //        this.plEndPoint = end;
    //    }
    //    public LcPolyLine(double startX, double startY, double endX, double endY) : this()
    //    {
    //        this.plStartPoint.X = startX;
    //        this.plStartPoint.Y = startY;
    //        this.plEndPoint.X = endX;
    //        this.plEndPoint.Y = endY;
    //    }
    //    //private double GetAngel(Vector2d p, Vector2d basep)
    //    //{
    //    //    int X = Convert.ToInt32(p.X);
    //    //    int Y = Convert.ToInt32(p.Y);

    //    //    Vector2d a1 = new Vector2d(X, Y);
    //    //    Vector2d a2 = new Vector2d(X, 300);
    //    //    Vector2d b1 = new Vector2d(X, Y);
    //    //    Vector2d b2 = new Vector2d(basep.X, basep.Y);
    //    //    var a = Math.Atan2(a2.Y - a1.Y, a2.X - a1.X);
    //    //    var b = Math.Atan2(b2.Y - b1.Y, b2.X - b1.X);
    //    //    double angle = 180 * (b - a) / Math.PI;
    //    //    return (angle > 0 ? angle : angle + 360);
    //    //}

    //    public override LcElement Clone()
    //    {
    //        var clone = Document.CreateObject<LcPolyLine>();
    //        clone.Copy(this);
    //        clone.Initilize(this.Document);
    //        return clone;

    //    }

    //    public override void Copy(LcElement src)
    //    {
    //        base.Copy(src);
    //        var line = ((LcPolyLine)src);
    //        this.plStartPoint = line.plStartPoint;
    //        this.plEndPoint = line.plEndPoint;
    //        this.plEntities = line.plEntities;
    //    }

    //    public void Set(Vector2d? start = null, Vector2d? end = null, bool fireChangedEvent = true)
    //    {
    //        //PropertySetter:Start,End
    //        if (!fireChangedEvent)
    //        {
    //            if (start != null) this.Start = start;
    //            if (end != null) this.End = end;
    //        }
    //        else
    //        {
    //            bool chg_start = (start != null && start != this.Start);
    //            if (chg_start)
    //            {
    //                OnPropertyChangedBefore(nameof(Start), this.Start, start);
    //                var oldValue = this.Start;
    //                this.Start = start;
    //                OnPropertyChangedAfter(nameof(Start), oldValue, this.Start);
    //            }
    //            bool chg_end = (end != null && end != this.End);
    //            if (chg_end)
    //            {
    //                OnPropertyChangedBefore(nameof(End), this.End, end);
    //                var oldValue = this.End;
    //                this.End = end;
    //                OnPropertyChangedAfter(nameof(End), oldValue, this.End);
    //            }
    //        }
    //    }
    //    public override Box2d GetBoundingBox()
    //    {
    //        return new Box2d(this.Start, this.End);
    //    }
    //    public override  Box2d GetBoundingBox(Matrix3 matrix)
    //    {
    //        return new Box2d(matrix.MultiplyPoint(this.Start),matrix.MultiplyPoint(this.End));
    //    }
    //    public override bool IntersectWithBox(Polygon2d testPoly, List<LcElement> intersectChildren = null)
    //    {
    //        var thisBox = this.BoundingBox;
    //        if (!thisBox.IntersectWith(testPoly.BoundingBox))
    //        {
    //            //如果元素盒子，与多边形盒子不相交，那就可能不相交
    //            return false;
    //        }
    //        return GeoUtils.IsPolygonIntersectLine(testPoly.Points, this.Start, this.End)
    //            || GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
    //    }

    //    public override bool IncludedByBox(Polygon2d testPoly, List<LcElement> includedChildren = null)
    //    {
    //        var thisBox = this.BoundingBox;
    //        if (!thisBox.IntersectWith(testPoly.BoundingBox))
    //        {
    //            return false;
    //        }
    //        return GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
    //    }

    //    public override void Translate(double dx, double dy)
    //    {
    //        var ts = new Vector2d(this.Start.X + dx, this.Start.Y + dy);
    //        var te = new Vector2d(this.End.X + dx, this.End.Y + dy);
    //        Set(ts, te);
    //        ResetBoundingBox();
    //    }
    //    public override void Move(Vector2d startPoint, Vector2d endPoint)
    //    {
    //        Matrix3 matrix3 = Matrix3.Move(startPoint, endPoint);
    //        this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
    //        ResetBoundingBox();
    //    }
    //    public override void Scale(Vector2d basePoint, double scaleFactor)
    //    {
    //        Matrix3 matrix3 = Matrix3.Scale(scaleFactor, basePoint);
    //        this.Set( matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
    //        ResetBoundingBox();
    //    }
    //    public override void Scale(Vector2d basePoint, Vector2d scaleVector)
    //    {

    //        Matrix3 matrix3 = Matrix3.Scale(scaleVector,basePoint);
    //        this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
    //        ResetBoundingBox();
    //    }
    //    public override void Rotate(Vector2d basePoint, double rotateAngle)
    //    {
    //        Matrix3 matrix3 = Matrix3.RotateInRadian(rotateAngle, basePoint);
    //        this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
    //        ResetBoundingBox();
    //    }
    //    public override void Mirror(Vector2d axisStart, Vector2d axisEnd)
    //    {
    //        Matrix3 matrix3 = Matrix3.Mirror(axisEnd, axisStart);
    //        this.Set(matrix3.MultiplyPoint(this.Start), matrix3.MultiplyPoint(this.End));
    //        ResetBoundingBox();
    //    }
    //}
}