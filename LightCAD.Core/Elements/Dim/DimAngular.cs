﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    ///  角度标注
    /// </summary>
    public class DimAngular : LcElement
    {
        public DimAngular()
        {
        }


        public override LcElement Clone()
        {
            var clone = new DimAngular();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var dim = ((DimAngular)src);
        }


    }
}
