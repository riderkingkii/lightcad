﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using LightCAD.MathLib;
namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 折弯半径标注，测量大半径的尺寸
    /// </summary>
    public class DimRadialLarge : LcElement
    {
        public Vector2 P;

        [JsonInclude, JsonPropertyName("S")]
        [JsonConverter(typeof(Vector2dConverter))]
        public Vector2 Start;
        [JsonInclude, JsonPropertyName("E")]
        [JsonConverter(typeof(Vector2dConverter))]
        public Vector2 End;
      
        public Vector2 Second;
        public Vector2 Three;

        public LcText Dimtext;
        public DimRadialLarge()
        {
            this.Type = BuiltinElementType.DimJogged;
        }


        public override LcElement Clone()
        {
            var clone = new DimRadialLarge();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var dim = ((DimRadialLarge)src);
        }

        public override Box2 GetBoundingBox()
        {
            return new Box2().SetFromPoints(this.Start, this.End);
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            return new Box2().SetFromPoints(matrix.MultiplyPoint(this.Start), matrix.MultiplyPoint(this.End));
        }

        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                //如果元素盒子，与多边形盒子不相交，那就可能不相交
                return false;
            }
            return GeoUtils.IsPolygonIntersectLine(testPoly.Points, this.Start, this.End)
                || GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        }

        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        }

    }
}
