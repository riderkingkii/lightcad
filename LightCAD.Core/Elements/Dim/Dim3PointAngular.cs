﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    ///  三点角度标注
    /// </summary>
    public class Dim3PointAngular : LcElement
    {
        public Dim3PointAngular()
        {
        }

        public override LcElement Clone()
        {
            var clone = new Dim3PointAngular();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var dim = ((Dim3PointAngular)src);
        }


    }
}
