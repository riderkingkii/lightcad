﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using LightCAD.MathLib;
namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 坐标标注
    /// </summary>
    public class DimOrdinate : LcElement
    {

        [JsonInclude, JsonPropertyName("S")]
        [JsonConverter(typeof(Vector2dConverter))]
        public Vector2 Start;
        [JsonInclude, JsonPropertyName("E")]
        [JsonConverter(typeof(Vector2dConverter))]
        public Vector2 End;

        public Vector2 Second;
        public LcText Dimtext;


        public DimOrdinate()
        {
            this.Type = BuiltinElementType.DimOrdinate;
        }

        public DimOrdinate(Vector2 start, Vector2 end) : this()
        {
            this.Start = start;
            this.End = end;
            //this.Dimtext = dimtext;
        }

        public void Set( Vector2 start = null, Vector2 end = null, bool fireChangedEvent = true)
        {
            //PropertySetter:Center,Radius            
            if (!fireChangedEvent)
            {
                if (start != null) this.Start = start;
                if (end != null) this.End = end;
            }
            else
            {
                bool chg_start = (start != null && start != this.Start);
                if (chg_start)
                {
                    OnPropertyChangedBefore(nameof(start), this.Start, start);
                    var oldValue = this.Start;
                    this.Start = start;
                    OnPropertyChangedAfter(nameof(start), oldValue, this.Start);
                }
                bool chg_end = (end != null && end != this.End);
                if (chg_end)
                {
                    OnPropertyChangedBefore(nameof(end), this.End, end);
                    var oldValue = this.End;
                    this.End = end;
                    OnPropertyChangedAfter(nameof(end), oldValue, this.End);
                }
            }
        }

        public override LcElement Clone()
        {
            var clone = new DimOrdinate();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var dim = ((DimOrdinate)src);
            this.Start = dim.Start;
            this.End = dim.End;
        }

        public override Box2 GetBoundingBox()
        {
            return new Box2().SetFromPoints(this.Start, this.End);
        }
        public override Box2 GetBoundingBox(Matrix3 matrix)
        {
            return new Box2().SetFromPoints(matrix.MultiplyPoint(this.Start), matrix.MultiplyPoint(this.End));
        }

        public override bool IntersectWithBox(Polygon2d testPoly, List<RefChildElement> intersectChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                //如果元素盒子，与多边形盒子不相交，那就可能不相交
                return false;
            }
            return GeoUtils.IsPolygonIntersectLine(testPoly.Points, this.Start, this.End)
                || GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        }

        public override bool IncludedByBox(Polygon2d testPoly, List<RefChildElement> includedChildren = null)
        {
            var thisBox = this.BoundingBox;
            if (!thisBox.IntersectsBox(testPoly.BoundingBox))
            {
                return false;
            }
            return GeoUtils.IsPolygonContainsLine(testPoly.Points, this.Start, this.End);
        }

    }
}
