﻿using LightCAD.MathLib;

namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 弧长标注
    /// </summary>
    public class DimArcLength : LcElement
    {
        public Vector2 P;
        public DimArcLength()
        {
        }


        public override LcElement Clone()
        {
            var clone = new DimArcLength();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var dim = ((DimArcLength)src);
        }


    }
}
