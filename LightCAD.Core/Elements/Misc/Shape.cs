﻿namespace LightCAD.Core.Elements
{
    public class Shape : LcElement
    {
        public Shape()
        {
        }


        public override LcElement Clone()
        {
            var clone = new Shape();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var dim = ((Shape)src);
        }

    }
}
