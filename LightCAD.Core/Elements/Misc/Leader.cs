﻿namespace LightCAD.Core.Elements
{
    public class Leader : LcElement
    {
        public Leader()
        {
        }


        public override LcElement Clone()
        {
            var clone = new Leader();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var leader = ((Leader)src);
        }


    }
}
