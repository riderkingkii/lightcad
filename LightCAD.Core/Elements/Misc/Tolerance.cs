﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 公差符号
    /// </summary>
    public class Tolerance : LcElement
    {
        public Tolerance()
        {
        }

        public override LcElement Clone()
        {
            var clone = new Tolerance();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var tol = ((Tolerance)src);
        }

    }
}
