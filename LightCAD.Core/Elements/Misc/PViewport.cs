﻿namespace LightCAD.Core.Elements
{
    public class PViewport : LcElement
    {
        public PViewport()
        {
        }

        public override LcElement Clone()
        {
            var clone = new PViewport();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var vp = ((PViewport)src);
        }

    }
}
