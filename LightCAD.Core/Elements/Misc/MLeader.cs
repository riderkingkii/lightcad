﻿namespace LightCAD.Core.Elements
{
    public class MLeader : LcElement
    {
        public MLeader()
        {
        }

        public override LcElement Clone()
        {
            var clone = new MLeader();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var mleader = ((MLeader)src);
        }

    }
}
