﻿namespace LightCAD.Core.Elements
{
    public class Table : LcElement
    {
        public Table()
        {
        }

        public override LcElement Clone()
        {
            var clone = new Table();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var table = ((Table)src);
        }

    }
}
