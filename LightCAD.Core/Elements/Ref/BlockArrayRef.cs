﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 块阵列引用
    /// </summary>
    public class LcBlockArrayRef : LcElement
    {
        public LcBlockArrayRef()
        {
        }

        public override LcElement Clone()
        {
            var clone = new LcBlockArrayRef();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var blkary = ((LcBlockArrayRef)src);
        }


    }
}
