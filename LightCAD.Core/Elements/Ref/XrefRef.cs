﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 外参引用
    /// </summary>
    public class XrefRef : LcElement
    {
        public XrefRef()
        {
        }


        public override LcElement Clone()
        {
            var clone = new XrefRef();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var xref = ((XrefRef)src);
        }


    }
}
