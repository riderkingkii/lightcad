﻿namespace LightCAD.Core.Elements
{
    /// <summary>
    /// 轮廓引用
    /// </summary>
    public class ProfileRef : LcElement
    {
        public ProfileRef()
        {
        }

        public override LcElement Clone()
        {
            var clone = new ProfileRef();
            clone.Copy(this);
            return clone;
        }

        public override void Copy(LcElement src)
        {
            var pfref = ((ProfileRef)src);
        }

    }
}
