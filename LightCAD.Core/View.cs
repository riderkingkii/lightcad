﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
   public class View:LcObject
    {
        public string Name { get; set; }
    }
    public class ViewCollection : KeyedCollection<string, View>
    {
        protected override string GetKeyForItem(View item)
        {
            return item.Name;
        }
    }
}
