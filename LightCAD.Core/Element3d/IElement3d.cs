﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public interface IElement3d
    {
        object Rt3DAction { get;  }
        ListEx<Vector3> Snap3dPoints { get; }

    }
}
