﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Element3d
{
    public class LcEllipsoid : LcSolid3d
    {
        public Ellipsoid3d Ellipsoid => this.Solid as Ellipsoid3d;
        public LcEllipsoid()
        {
            this.Solid = new Ellipsoid3d();

        }
        public LcEllipsoid(double aRadius = 10, double bRadius = 10, double cRadius = 10,double startAngle=0,double endAngle=Utils.TwoPI , params LcMaterial[] mats) : this()
        {
            this.Ellipsoid.SetSize(aRadius, bRadius, cRadius, startAngle,endAngle);
            this.Materials = mats;
        }

    }
}
