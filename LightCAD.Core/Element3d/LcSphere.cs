﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Element3d
{
    public class LcSphere : LcSolid3d
    {
        public Sphere3d Sphere => this.Solid as Sphere3d;
        public LcSphere()
        {
            this.Solid = new Sphere3d();

        }
        public LcSphere(double radius,double startAngle,double endAngle ,params LcMaterial[] mats) : this()
        {
            this.Sphere.SetSize(radius,startAngle,endAngle);
            this.Materials = mats;
        }

    }
}
