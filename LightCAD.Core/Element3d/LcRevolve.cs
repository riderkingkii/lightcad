﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Element3d
{
    /// <summary>
    /// 回转体
    /// </summary>
    public class LcRevolve : LcSolid3d
    {
        public Revolve3d revolve =>this.Solid as Revolve3d;
        public LcRevolve() 
        {
            this.Solid = new Revolve3d();
            
        }
        public LcRevolve(PlanarSurface3d profile, Vector3 origin, Vector3 axis, double startAngle, double endAngle, bool isClosed = true, params LcMaterial[] mats) :this()
        {
            this.revolve.SetSize(profile, origin, axis, startAngle, endAngle, isClosed);
            this.Materials = mats;
        }
    }
}
