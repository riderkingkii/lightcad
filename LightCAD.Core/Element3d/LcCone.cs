﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Element3d
{
    /// <summary>
    /// 圆锥体
    /// </summary>
    public class LcCone : LcSolid3d
    {
        public Cone3d cone =>this.Solid as Cone3d;
        public LcCone() : base()
        {
            this.Solid = new Cone3d();
            
        }
        public LcCone(double radius, double height, params LcMaterial[] mats) :this()
        {
            this.cone.SetSize(radius, height);
            this.Materials = mats;
        }
    }
}
