﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Element3d
{
    /// <summary>
    /// 圆柱
    /// </summary>
    public class LcCylinder : LcSolid3d
    {
        public Cylinder3d cylinder => this.Solid as Cylinder3d;
        public LcCylinder()
        {
            this.Solid = new Cylinder3d();

        }
        public LcCylinder(double radiusTop, double radiusBottom, double height, double thetaStart = 0, double thetaLength = Math.PI * 2, int radialSegments = 32,  bool openEnded = false, params LcMaterial[] mats) : this()
        {
            this.cylinder.SetSize(radiusTop, radiusBottom, height, thetaStart, thetaLength, radialSegments,  openEnded,new Vector3(0,0,1));
            this.Materials = mats;
        }

    }
}
