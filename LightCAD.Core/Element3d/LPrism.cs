﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Element3d
{
    /// <summary>
    /// 棱柱
    /// </summary>
    public class LcPrism : LcSolid3d
    {
        public Prism3d prism => this.Solid as Prism3d;
        public LcPrism()
        {
            this.Solid = new Prism3d();

        }
        public LcPrism(int side,  double topRadius, double bottomRadius, double height,bool isInnerCircle, bool openEnded, params LcMaterial[] mats) : this()
        {
            this.prism.SetSize(side, topRadius, bottomRadius, height, isInnerCircle, openEnded);
            this.Materials = mats;
        }
        public LcPrism(int side, double radius, double height, bool isInnerCircle, bool openEnded, params LcMaterial[] mats) : this()
        {
            this.prism.SetSize(side, radius, radius, height, isInnerCircle, openEnded);
            this.Materials = mats;
        } 
    }
}
