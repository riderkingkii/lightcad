﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Element3d
{
    public class CuboidAttribute : CptAttribute
    {
        public CuboidAttribute(string category, string subCategorys) : base(category, subCategorys)
        {
        }
        public const string BuiltinUuid = "{8807F432-5692-4682-BD96-E55B2D99E972}";
        public override LcComponentDefinition GetBuiltinCpt(string subCategory)
        {
            return new LcCuboidDef(BuiltinUuid, "内置", "长方体");
        }
    }

    [CuboidAttribute("长方体", "长方体")]
    public class LcCuboidDef : LcComponentDefinition
    {
        static LcCuboidDef()
        {
            CptTypeParamsDef<LcCuboidDef>.ParamsDef = new ParameterSetDefinition
            {
                new ParameterDefinition
                {
                    DataType = LcDataType.String,
                    Name = "MaterialId",
                    DisplayName = "材质",
                },
            };
        }
        public LcCuboidDef()
        {
            this.TypeParameters = new LcParameterSet(CptTypeParamsDef<LcCuboidDef>.ParamsDef);
        }

        public LcCuboidDef(string uuid, string name, string subCategory) : base(uuid, name, "长方体", subCategory, new LcParameterSet(CptTypeParamsDef<LcCuboidDef>.ParamsDef))
        {
            if (subCategory == "长方体")
            {
                this.Parameters = new ParameterSetDefinition
                {
                    new ParameterDefinition
                    {
                        Name = "Width",
                        DataType = LcDataType.Double,
                    },
                    new ParameterDefinition
                    {
                        Name = "Height",
                        DataType = LcDataType.Double,
                    },
                    new ParameterDefinition
                    {
                        Name = "Depth",
                        DataType = LcDataType.Double,
                    }
                };

                this.Curve2dProvider = new Curve2dProvider("立方体")
                {
                    GetCurve2dsFunc = (p) =>
                    {
                        var width = p.GetValue<double>("Width");
                        var height = p.GetValue<double>("Height");
                        var depth = p.GetValue<double>("Depth");
                        var box = new Box2(new Vector2(-width / 2, -height / 2), new Vector2(width / 2, height / 2));
                        return new Curve2dGroupCollection() { new Curve2dGroup()
                        {
                            Curve2ds=new ListEx<Curve2d>
                            {
                                new Line2d(box.LeftBottom,box.RightBottom),
                                new Line2d(box.RightBottom,box.RightTop),
                                new Line2d(box.RightTop,box.LeftTop),
                                new Line2d(box.LeftTop,box.LeftBottom),
                            }
                        } };
                    }
                };

                this.Solid3dProvider = new Solid3dProvider("长方体")
                {
                    GetSolid3dsFunc = (p) =>
                    {
                        var width = p.GetValue<double>("Width");
                        var height = p.GetValue<double>("Height");
                        var depth = p.GetValue<double>("Depth");
                        return new Solid3dCollection { new Cuboid3d(width, height, depth) };
                    }
                };
            }
        }
    }

    public class LcCuboidInstance : LcComponentInstance
    {
        public LcCuboidInstance(double width, double height, double depth, LcCuboidDef cuboid): base(cuboid)
        {
            this.Type = BuiltinElementType.Cuboid;
            this.Parameters.SetValue("Width", width);
            this.Parameters.SetValue("Height", height);
            this.Parameters.SetValue("Depth", depth);
        }
    }
}