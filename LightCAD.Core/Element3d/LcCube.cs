﻿namespace LightCAD.Core
{
    public class CubeAttribute : CptAttribute
    {
        public CubeAttribute(string category, string subCategorys) : base(category, subCategorys)
        {
        }
        public const string BuiltinUuid = "{0F72DD6E-2972-5775-D10F-56E8D20397E9}";
        public override LcComponentDefinition GetBuiltinCpt(string subCategory)
        {
            return new LcCubeDef(BuiltinUuid, "内置", "立方体");
        }
    }
    [CubeAttribute("立方体", "立方体")]
    public class LcCubeDef : LcComponentDefinition
    {
        static LcCubeDef()
        {
            CptTypeParamsDef<LcCubeDef>.ParamsDef = new ParameterSetDefinition { new ParameterDefinition { DataType = LcDataType.String, Name = "MaterialId", DisplayName = "材质", } };
        }
        public LcCubeDef()
        {
            this.TypeParameters = new LcParameterSet(CptTypeParamsDef<LcCubeDef>.ParamsDef);
        }
        public LcCubeDef(string uuid, string name, string subCategory) : base(uuid, name, "立方体", subCategory,new LcParameterSet(CptTypeParamsDef<LcCubeDef>.ParamsDef))
        {
            if (SubCategory == "立方体")
            {
                this.Parameters = new ParameterSetDefinition
                {
                    new ParameterDefinition { Name = "Size", DataType = LcDataType.Double } ,
                };
                this.Curve2dProvider = new Curve2dProvider("立方体")
                {
                    GetCurve2dsFunc = (p) =>
                    {
                        var size = p.GetValue<double>("Size");
                        var box = new Box2(new Vector2(-size/2, -size/2), new Vector2(size/2, size/2));
                        return new Curve2dGroupCollection() { new Curve2dGroup()
                        {
                            Curve2ds=new ListEx<Curve2d>
                            {
                                new Line2d(box.LeftBottom,box.RightBottom),
                                new Line2d(box.RightBottom,box.RightTop),
                                new Line2d(box.RightTop,box.LeftTop),
                                new Line2d(box.LeftTop,box.LeftBottom),
                            }
                        } };
                    }
                };
                this.Solid3dProvider = new Solid3dProvider("立方体")
                {
                    GetSolid3dsFunc = (p) =>
                    {
                        var size = p.GetValue<double>("Size");
                        return new Solid3dCollection { new Cube3d(size) };
                    }
                };
            }
        }
    }

    public class LcCubeInstance : LcComponentInstance
    {
        public LcCubeInstance(double size,LcCubeDef cube) :base(cube)
        {
            this.Type = BuiltinElementType.Cube;
            this.Parameters.SetValue("Size", size);
        }
     }
}
