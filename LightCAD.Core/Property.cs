﻿using System;
using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class LcProperty
    {
        public string Name { get; internal set; }
        public object Value { get; set; }
        public object? Extent { get; set; }

        public void Set(object value, object? extent = null)
        {
            this.Value = value;
            this.Extent = extent;
        }

        public LcProperty Clone()
        {
            var newProp = new LcProperty();
            newProp.Name = Name;
            newProp.Value = Value;
            newProp.Extent = Extent;
            return newProp;
        }
    }
    public class PropertyCollection : KeyedCollection<string, LcProperty>
    {
        protected override string GetKeyForItem(LcProperty item)
        {
            return item.Name;
        }
        public string GetKey()
        {
            return null;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
    public class LcPropertyGroup
    {
        public PropertyCollection Properties { get; set; }
        public LcPropertyGroupDefinition Definition { get; set; }

        internal LcProperty SetProperty(string name, object value, object? extent = null)
        {
            var prop = GetProperty(name);
            prop.Set(value, extent);
            return prop;
        }
        internal LcProperty GetProperty(string name)
        {
            if (this.Properties == null)
            {
                this.Properties = new PropertyCollection();
            }
            LcProperty prop;
            if (this.Properties.TryGetValue(name, out prop))
            {
                return prop;
            }

            if (Definition.PropDefs.Contains(name))
            {
                throw new ArgumentException(String.Format(SR.PropDef_NotFound, name));
            }

            prop = new LcProperty { Name = name };
            this.Properties.Add(prop);

            return prop;
        }

        public LcPropertyGroup Clone()
        {
            var newPgrp = new LcPropertyGroup();
            newPgrp.Definition = this.Definition;
            if (this.Properties != null)
            {
                newPgrp.Properties = new PropertyCollection();
                foreach (var prop in this.Properties)
                {
                    newPgrp.Properties.Add(prop.Clone());
                }
            }
            return newPgrp;
        }
    }


    public class LcPropertyGroupCollection : KeyedCollection<string, LcPropertyGroup>
    {
        protected override string GetKeyForItem(LcPropertyGroup item)
        {
            return item.Definition.Name;
        }
    }



}