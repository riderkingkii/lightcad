﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LightCAD.Core
{
    public class LcPaper : ElementSpace
    {
        public string Name { get; set; }
    }
    public class LcPaperSpace : LcObject
    {
        [JsonInclude]
        public List<LcPaper> Papers { get; internal set; }

        public LcPaperSpace()
        {
        }
        public LcPaperSpace(LcDocument document)
        {
            this.Document = document;
            this.Papers = new List<LcPaper>();
        }
        public LcPaper CreatePaper(string name)
        {
            var paper = new LcPaper();
            paper.Name = name;
            return paper;
        }
    }

}
