﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class LcLineType : LcObject
    {
        /// <summary>
        /// 线型id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 线型名称
        /// </summary>
        public string LineTypeName { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string LineTypeExplain { get; set; }
        /// <summary>
        /// 线型定义
        /// </summary>
        public string LineTypeDefinition { get; set; }

        public List<bool> IsUcsOrienteds = new List<bool>();
        public List<int> numbers = new List<int>();
        public List<Vector2> offsets = new List<Vector2>();
        public List<double> rotations = new List<double>();
        public List<double> scales = new List<double>();
        public List<long> styleIds = new List<long>();
        public List<string> texts = new List<string>();
        public List<double> lengths = new List<double>();

        public static LcLineType LoadLinToPath(string linetypename,
                                    string linetypeexplain,
                                    List<double> lengths = null)
        {
            LcLineType linetypeEntity = new LcLineType();
            byte[] buffer = Guid.NewGuid().ToByteArray();
            int guid = (int)BitConverter.ToInt64(buffer, 0);
            linetypeEntity.Id = guid;
            linetypeEntity.LineTypeName = linetypename;
            linetypeEntity.LineTypeExplain = linetypeexplain;
            string LineTypeDefinition = "";
            foreach (var item in lengths)
            {

                if ((item < 0 && item > -1) || (item > 0 && item < 1))
                {
                    List<string> floatsp = item.ToString().split(".");
                    LineTypeDefinition += (floatsp[0] == "0" ? "." : "-.") + floatsp[1] + ",";
                }
                else
                {
                    LineTypeDefinition += item + ",";
                }
            }
            if (!string.IsNullOrEmpty(LineTypeDefinition))
            {
                LineTypeDefinition = "A," + LineTypeDefinition.TrimEnd(',');
            }
            linetypeEntity.LineTypeDefinition = LineTypeDefinition;
            linetypeEntity.lengths = lengths;
            return linetypeEntity;
        }
    }

    public class LcLineTypeCollection : KeyedCollection<string, LcLineType>
    {
        protected override string GetKeyForItem(LcLineType item)
        {
            return item.LineTypeName;
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            OnPropertyChanged(new EventArgs());
        }
        protected override void InsertItem(int index, LcLineType item)
        {
            base.InsertItem(index, item);
            OnPropertyChanged(new EventArgs());
        }
        protected override void ClearItems()
        {
            base.ClearItems();
            OnPropertyChanged(new EventArgs());
        }

        protected override void SetItem(int index, LcLineType item)
        {
            base.SetItem(index, item);
            OnPropertyChanged(new EventArgs());
        }


        public static event EventHandler LineTypeChanged;
        private void OnPropertyChanged(EventArgs eventArgs)
        {
            if (LineTypeChanged != null)//判断事件是否有处理函数 
            {
                LineTypeChanged?.Invoke(this, eventArgs);
            }
        }
    }
}