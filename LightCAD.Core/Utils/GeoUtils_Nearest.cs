﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Core
{
    public enum NearestType
    {
        Start,
        End,
        Center,
        OnCurve,
    }
    public class NearestPoint
    {
        public Vector2 Point { get; set; }
        public double Distance { get; set; }
        public NearestType Type { get; set; }
    }
    public partial class LcGeoUtils
    {
        //根据测试点，寻找最近的点

        public static NearestPoint LineNearestPoint(Vector2 testPoint, Vector2 start,Vector2 end)
        {
            return null;
        }
        public static NearestPoint CircleNearestPoint(Vector2 testPoint, Vector2 center, double radius)
        {
            return null;
        }
    }
}
