﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;
namespace LightCAD.Core
{

    public partial class LcGeoUtils
    {

        public static bool IsPolygonIntersectRay(Vector2[] polygon, Elements.LcRay ray)
        {
            int count = polygon.Length;

            for (int i = 0; i < count; i++)
            {
                var p1 = polygon[i];
                var p2 = polygon[(i + 1) % count];

                if (IsLineIntersectRay(p1, p2, ray))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsPolygonIntersectPolygon(Vector2[] polygon1, Vector2[] polygon2)
        {
            int count = polygon1.Length;
            if (polygon1.Last().Equals(polygon1.First()))
            {
                count -= 1;
            }

            for (int i = 0; i < count; i++)
            {
                var p1 = polygon1[i];
                var p2 = polygon1[(i + 1) % count];

                if (GeoUtils.IsPolygonIntersectLine(polygon2, p1, p2))
                {
                    return true;
                }
            }
            return false;
        }


        public static bool IsLineIntersectRay(Vector2 p1, Vector2 p2, Elements.LcRay ray)
        {
            var inter = IntersectRayLine(ray, p1, p2);
            return inter.Equals(Vector2.Zero);
        }

        public static bool IsPolygonIntersectXLine(Vector2[] polygon, Elements.LcXLine xline)
        {
            int count = polygon.Length;

            for (int i = 0; i < count; i++)
            {
                var p1 = polygon[i];
                var p2 = polygon[(i + 1) % count];

                if (IsLineIntersectXLine(p1, p2, xline))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsLineIntersectXLine(Vector2 p1, Vector2 p2, Elements.LcXLine xline)
        {
            var inter = IntersectXLineLine(xline, p1, p2);
            return inter.Equals(Vector2.Zero);
        }


       
    }
}