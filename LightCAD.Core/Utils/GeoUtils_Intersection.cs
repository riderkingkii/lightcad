﻿using LightCAD.Core.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;

namespace LightCAD.Core
{
    public class Intersection
    {
        public string status { get; set; }
        public List<Vector2> points { get; } = new List<Vector2>();
        public Intersection() { }
        public Intersection(string status)
        {
            this.status = status;
        }

        public void appendPoints(IEnumerable<Vector2> newPoints)
        {
            points.AddRange(newPoints);
        }
    }
    public static partial class LcGeoUtils
    {
        public static Vector2[] ClosePolygon(Vector2[] points)
        {
            var copy = new List<Vector2>(points);
            copy.Add(points[0]);
            return copy.ToArray();
        }

        /**
    *  intersectCircleLine
    *
    *  @param {module:kld-intersections.Point2D} c
    *  @param {number} r
    *  @param {module:kld-intersections.Point2D} a1
    *  @param {module:kld-intersections.Point2D} a2
    *  @returns {module:kld-intersections.Intersection}
    */
        public static Intersection IntersectCircleLine(Vector2 c, double r, Vector2 a1, Vector2 a2)
        {
            var result = new Intersection();
            var a = (a2.X - a1.X) * (a2.X - a1.X) +
                     (a2.Y - a1.Y) * (a2.Y - a1.Y);
            var b = 2 * ((a2.X - a1.X) * (a1.X - c.X) +
                           (a2.Y - a1.Y) * (a1.Y - c.Y));
            var cc = c.X * c.X + c.Y * c.Y + a1.X * a1.X + a1.Y * a1.Y -
                     2 * (c.X * a1.X + c.Y * a1.Y) - r * r;
            var deter = b * b - 4 * a * cc;

            if (deter < 0)
            {
                result = new Intersection("Outside");
            }
            else if (deter == 0)
            {
                result = new Intersection("Tangent");
                // NOTE: should calculate this point
            }
            else
            {
                var e = Math.Sqrt(deter);
                var u1 = (-b + e) / (2 * a);
                var u2 = (-b - e) / (2 * a);

                if ((u1 < 0 || u1 > 1) && (u2 < 0 || u2 > 1))
                {
                    if ((u1 < 0 && u2 < 0) || (u1 > 1 && u2 > 1))
                    {
                        result = new Intersection("Outside");
                    }
                    else
                    {
                        result = new Intersection("Inside");
                    }
                }
                else
                {
                    result = new Intersection("Intersection");

                    if (0 <= u1 && u1 <= 1)
                    {
                        result.points.Add(a1.Lerp(a2, u1));
                    }

                    if (0 <= u2 && u2 <= 1)
                    {
                        result.points.Add(a1.Lerp(a2, u2));
                    }
                }
            }

            return result;
        }



        /**
    *  intersectCirclePolygon
    *
    *  @param {module:kld-intersections.Point2D} c
    *  @param {number} r
    *  @param {Array<module:kld-intersections.Point2D>} points
    *  @returns {module:kld-intersections.Intersection}
    */
        public static Intersection IntersectCirclePolygon(Vector2 c, double r, Vector2[] points)
        {
            return IntersectCirclePolyline(c, r, ClosePolygon(points));
        }

        /**
         *  intersectCirclePolyline
         *
         *  @param {module:kld-intersections.Point2D} c
         *  @param {number} r
         *  @param {Array<module:kld-intersections.Point2D>} points
         *  @returns {module:kld-intersections.Intersection}
         */
        public static Intersection IntersectCirclePolyline(Vector2 c, double r, Vector2[] points)
        {
            var result = new Intersection("None");
            Intersection inter = null;

            for (var i = 0; i < points.Length - 1; i++)
            {
                var a1 = points[i];
                var a2 = points[i + 1];

                inter = IntersectCircleLine(c, r, a1, a2);
                result.appendPoints(inter.points);
            }

            if (result.points.Count > 0)
            {
                result.status = "Intersection";
            }
            else if (inter != null)
            {
                result.status = inter.status;
            }

            return result;
        }

        /**
    *  intersectLineLine
    *
    *  @param {module:kld-intersections.Point2D} a1
    *  @param {module:kld-intersections.Point2D} a2
    *  @param {module:kld-intersections.Point2D} b1
    *  @param {module:kld-intersections.Point2D} b2
    *  @returns {module:kld-intersections.Intersection}
    */
        public static Intersection IntersectLineLine(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2)
        {
            Intersection result;

            var ua_t = (b2.X - b1.X) * (a1.Y - b1.Y) - (b2.Y - b1.Y) * (a1.X - b1.X);
            var ub_t = (a2.X - a1.X) * (a1.Y - b1.Y) - (a2.Y - a1.Y) * (a1.X - b1.X);
            var u_b = (b2.Y - b1.Y) * (a2.X - a1.X) - (b2.X - b1.X) * (a2.Y - a1.Y);

            if (u_b != 0)
            {
                var ua = ua_t / u_b;
                var ub = ub_t / u_b;

                if (0 <= ua && ua <= 1 && 0 <= ub && ub <= 1)
                {
                    result = new Intersection("Intersection");
                    result.points.Add(
                        new Vector2(
                            a1.X + ua * (a2.X - a1.X),
                            a1.Y + ua * (a2.Y - a1.Y)
                        )
                    );
                }
                else
                {
                    result = new Intersection("None");
                }
            }
            else if (ua_t == 0 || ub_t == 0)
            {
                result = new Intersection("Coincident");
            }
            else
            {
                result = new Intersection("Parallel");
            }

            return result;
        }

        /**
         *  intersectLinePolygon
         *
         *  @param {module:kld-intersections.Point2D} a1
         *  @param {module:kld-intersections.Point2D} a2
         *  @param {Array<module:kld-intersections.Point2D>} points
         *  @returns {module:kld-intersections.Intersection}
         */
        public static Intersection IntersectLinePolygon(Vector2 a1, Vector2 a2, Vector2[] points)
        {
            return IntersectLinePolyline(a1, a2, ClosePolygon(points));
        }

        /**
         *  intersectLinePolyline
         *
         *  @param {module:kld-intersections.Point2D} a1
         *  @param {module:kld-intersections.Point2D} a2
         *  @param {Array<module:kld-intersections.Point2D>} points
         *  @returns {module:kld-intersections.Intersection}
         */
        public static Intersection IntersectLinePolyline(Vector2 a1, Vector2 a2, Vector2[] points)
        {
            var result = new Intersection("None");

            for (var i = 0; i < points.Length - 1; i++)
            {
                var b1 = points[i];
                var b2 = points[i + 1];
                var inter = IntersectLineLine(a1, a2, b1, b2);

                result.appendPoints(inter.points);
            }

            if (result.points.Count > 0)
            {
                result.status = "Intersection";
            }

            return result;
        }

        /**
         *  intersectLineRectangle
         *
         *  @param {module:kld-intersections.Point2D} a1
         *  @param {module:kld-intersections.Point2D} a2
         *  @param {module:kld-intersections.Point2D} r1
         *  @param {module:kld-intersections.Point2D} r2
         *  @returns {module:kld-intersections.Intersection}
         */
        public static Intersection IntersectLineBox(Vector2 a1, Vector2 a2, Box2 box)
        {
            var result = new Intersection("None");
            var lb = box.LeftBottom;
            var lt = box.LeftTop;
            var rb = box.RightBottom;
            var rt = box.RightTop;

            var inter1 = IntersectLineLine(lt, rt, a1, a2);
            var inter2 = IntersectLineLine(rt, rb, a1, a2);
            var inter3 = IntersectLineLine(rb, lb, a1, a2);
            var inter4 = IntersectLineLine(lb, lt, a1, a2);


            result.appendPoints(inter1.points);
            result.appendPoints(inter2.points);
            result.appendPoints(inter3.points);
            result.appendPoints(inter4.points);

            if (result.points.Count > 0)
            {
                result.status = "Intersection";
            }

            return result;
        }
        public static Vector2 IntersectRayLine(LcRay ray, Vector2 segmentStart, Vector2 segmentEnd)
        {
            Vector2 rayOrigin = ray.StartPoint;
            Vector2 rayDirection = ray.Direction;
            float epsilon = 0.0001f; // 用于比较浮点数时的误差范围

            // 计算射线的单位方向向量
            Vector2 rayNormalizedDirection = rayDirection.Normalize();

            // 计算线段的方向向量和垂直向量
            Vector2 segmentDirection = (segmentEnd - segmentStart).Normalize();
            Vector2 segmentNormal = new Vector2(-segmentDirection.Y, segmentDirection.X);

            if (rayDirection.Equals(segmentDirection) || rayDirection.Equals(-segmentDirection))
            {
                return Vector2.Zero;
            }

            // 计算射线与线段平面的法向量投影长度
            double normalProjectionLength = -Vector2.Dot(segmentNormal, (rayOrigin - segmentStart));

            //if (Math.Abs(normalProjectionLength) < epsilon)
            //{
            //    // 射线和线段平行或共面
            //    return Vector2d.Zero;
            //}

            // 计算射线与线段的交点
            double rayDistance = Vector2.Dot(segmentNormal, rayNormalizedDirection);
            double intersectionDistance = normalProjectionLength / rayDistance;

            if (intersectionDistance < 0)
            {
                // 射线方向与线段相反，无交点
                return Vector2.Zero;
            }

            Vector2 intersectionPoint = rayOrigin + intersectionDistance * rayNormalizedDirection;

            // 检查交点是否在线段上
            double segmentLength = (segmentEnd - segmentStart).LengthSq();
            double distanceToStart = (intersectionPoint - segmentStart).LengthSq();
            double distanceToEnd = (intersectionPoint - segmentEnd).LengthSq();

            if (distanceToStart > segmentLength || distanceToEnd > segmentLength)
            {
                // 交点不在线段上
                return Vector2.Zero;
            }

            return intersectionPoint;
        }

        public static Vector2 IntersectXLineLine(LcXLine xline, Vector2 segmentStart, Vector2 segmentEnd)
        {
            Vector2 lineStart = xline.StartPoint;
            Vector2 lineDirection = xline.Direction;

            double x1 = lineStart.X;
            double y1 = lineStart.Y;
            double x2 = lineStart.X + lineDirection.X;
            double y2 = lineStart.Y + lineDirection.Y;

            double x3 = segmentStart.X;
            double y3 = segmentStart.Y;
            double x4 = segmentEnd.X;
            double y4 = segmentEnd.Y;

            // 计算直线的参数
            double a1 = y2 - y1;
            double b1 = x1 - x2;

            // 计算线段的参数
            double a2 = y4 - y3;
            double b2 = x3 - x4;

            // 计算交点坐标
            double determinant = a1 * b2 - a2 * b1;

            if (Math.Abs(determinant) < 0.00001)
            {
                // 直线和线段平行或重合，没有交点
                return Vector2.Zero;
            }
            else
            {
                // 计算t和u参数
                double c1 = a1 * x1 + b1 * y1;
                double c2 = a2 * x3 + b2 * y3;

                // 计算交点坐标
                double intersectionX = (b2 * c1 - b1 * c2) / determinant;
                double intersectionY = (a1 * c2 - a2 * c1) / determinant;

                // 检查交点是否在给定的线段上
                if (intersectionX >= Math.Min(x3, x4) && intersectionX <= Math.Max(x3, x4) &&
                    intersectionY >= Math.Min(y3, y4) && intersectionY <= Math.Max(y3, y4))
                {
                    return new Vector2(intersectionX, intersectionY);
                }
                else
                {
                    // 交点不在给定的线段上
                    return Vector2.Zero;
                }
            }
        }
        public static List<Vector2> IntersectRayBox(LcRay ray, Box2 box)
        {
            //var result = new Intersection("None");
            var lb = box.LeftBottom;
            var lt = box.LeftTop;
            var rb = box.RightBottom;
            var rt = box.RightTop;

            var interP1 = IntersectRayLine(ray, lt, rt);
            var interP2 = IntersectRayLine(ray, rt, rb);
            var interP3 = IntersectRayLine(ray, rb, lb);
            var interP4 = IntersectRayLine(ray, lb, lt);

            //result.appendPoints(inter1.points);
            //result.appendPoints(inter2.points);
            //result.appendPoints(inter3.points);
            //result.appendPoints(inter4.points);

            //if (result.points.Count > 0)
            //{
            //    result.status = "Intersection";
            //}

            var result = new List<Vector2>();
            if (!interP1.Equals(Vector2.Zero))
                result.Add(interP1);

            if (!interP2.Equals(Vector2.Zero))
                result.Add(interP2);

            if (!interP3.Equals(Vector2.Zero))
                result.Add(interP3);

            if (!interP4.Equals(Vector2.Zero))
                result.Add(interP4);

            return result;
        }

        public static List<Vector2> IntersectXLineBox(LcXLine xline, Box2 box)
        {
            //var result = new Intersection("None");
            var lb = box.LeftBottom;
            var lt = box.LeftTop;
            var rb = box.RightBottom;
            var rt = box.RightTop;

            var interP1 = IntersectXLineLine(xline, lt, rt);
            var interP2 = IntersectXLineLine(xline, rt, rb);
            var interP3 = IntersectXLineLine(xline, rb, lb);
            var interP4 = IntersectXLineLine(xline, lb, lt);

            //result.appendPoints(inter1.points);
            //result.appendPoints(inter2.points);
            //result.appendPoints(inter3.points);
            //result.appendPoints(inter4.points);

            //if (result.points.Count > 0)
            //{
            //    result.status = "Intersection";
            //}

            var result = new List<Vector2>();
            if (!interP1.Equals(Vector2.Zero))
                result.Add(interP1);

            if (!interP2.Equals(Vector2.Zero))
                result.Add(interP2);

            if (!interP3.Equals(Vector2.Zero))
                result.Add(interP3);

            if (!interP4.Equals(Vector2.Zero))
                result.Add(interP4);

            return result;
        }
        /// <summary>
        /// 计算两条直线的交点
        /// </summary>
        /// <param name="lineFirstStar">L1的点1坐标</param>
        /// <param name="lineFirstEnd">L1的点2坐标</param>
        /// <param name="lineSecondStar">L2的点1坐标</param>
        /// <param name="lineSecondEnd">L2的点2坐标</param>
        /// <returns></returns>
        public static Vector2 GetIntersection(Vector2 lineFirstStar, Vector2 lineFirstEnd, Vector2 lineSecondStar, Vector2 lineSecondEnd)
        {

            Vector2 reVector2d = new Vector2(0, 0);
            double a = 0, b = 0;
            int state = 0;
            if (lineFirstStar.X != lineFirstEnd.X)
            {
                a = (lineFirstEnd.Y - lineFirstStar.Y) / (lineFirstEnd.X - lineFirstStar.X);
                state |= 1;
            }
            if (lineSecondStar.X != lineSecondEnd.X)
            {
                b = (lineSecondEnd.Y - lineSecondStar.Y) / (lineSecondEnd.X - lineSecondStar.X);
                state |= 2;
            }
            switch (state)
            {
                case 0: //L1与L2都平行Y轴
                    {
                        if (lineFirstStar.X == lineSecondStar.X)
                        {
                            //throw new Exception("两条直线互相重合，且平行于Y轴，无法计算交点。");
                            return new Vector2(0, 0);
                        }
                        else
                        {
                            //throw new Exception("两条直线互相平行，且平行于Y轴，无法计算交点。");
                            return new Vector2(0, 0);
                        }
                    }
                case 1: //L1存在斜率, L2平行Y轴
                    {
                        double x = lineSecondStar.X;
                        double y = (lineFirstStar.X - x) * (-a) + lineFirstStar.Y;
                        return new Vector2(x, y);
                    }
                case 2: //L1 平行Y轴，L2存在斜率
                    {
                        double x = lineFirstStar.X;
                        //网上有相似代码的，这一处是错误的。你可以对比case 1 的逻辑 进行分析
                        //源code:lineSecondStar * x + lineSecondStar * lineSecondStar.X + p3.Y;
                        double y = (lineSecondStar.X - x) * (-b) + lineSecondStar.Y;
                        return new Vector2(x, y);
                    }
                case 3: //L1，L2都存在斜率
                    {
                        if (a == b)
                        {
                            // throw new Exception("两条直线平行或重合，无法计算交点。");
                            return new Vector2(0, 0);
                        }
                        double x = (a * lineFirstStar.X - b * lineSecondStar.X - lineFirstStar.Y + lineSecondStar.Y) / (a - b);
                        double y = a * x - a * lineFirstStar.X + lineFirstStar.Y;
                        return new Vector2(x, y);
                    }
            }
            // throw new Exception("不可能发生的情况");
            return new Vector2(0, 0);
        }


    }
}
