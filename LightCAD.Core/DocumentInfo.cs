﻿namespace LightCAD.Core
{
    public class LcDocumentInfo : LcObject
    {
        public string Author { get; set; }

        public string Comments { get; set; }

        public string HyperlinkBase { get; set; }

        public string Keywords { get; set; }

        public string LastSavedBy { get; set; }

        public string RevisionNumber { get; set; }

        public string Subject { get; set; }

        public string Title { get; set; }
    }
}