﻿using LightCAD.Core.Elements;
using System;
using System.Text.Json.Serialization;
using LightCAD.MathLib;
using LightCAD.Core.Elements.Basic;

namespace LightCAD.Core
{
    [JsonDerivedType(typeof(LcModelSpace), typeDiscriminator: nameof(LcModelSpace))]
    [JsonDerivedType(typeof(LcBlock), typeDiscriminator: nameof(LcBlock))]
    public class ElementSpace : LcObject, IElementSet
    {
        //程序集的IsFireChangedEvent主要控制向文档触发事件

        /// <summary>
        /// 所有元素集合，IElementContainer成员
        /// </summary>
        [JsonInclude]
        public ElementCollection Elements { get; internal set; } = new ElementCollection();

        
        public void InsertElement(LcElement element)
        {
            element.Parent = this;
            if (!IsFireChangedEvent)
            {
                element.Document = this.Document;
                this.Elements.Add(element);
                //如果元素集没有触发事件，根据元素本身触发事件，元素只触发After事件
                if (element.IsFireChangedEvent)
                {
                    var args = new ObjectChangedEventArgs
                    {
                         ElementSet=this,
                        Type = ObjectChangeType.Insert,
                        Target = element
                    };
                    element.OnObjectChangedAfter(args);
                }
            }
            else
            {
                var args = new ObjectChangedEventArgs
                {
                    ElementSet = this,
                    Type = ObjectChangeType.Insert,
                    Target = element
                };
                if (element.IsFireChangedEvent) 
                    element.OnObjectChangedBefore(args);
                if (args.IsCancel) return;

                this.OnObjectChangedBefore(args);
                if (args.IsCancel) return;

                this.Document.OnObjectChangedBefore(args);
                if (args.IsCancel) return;

                element.Document = this.Document;
                this.Elements.Add(element);


                if (element.IsFireChangedEvent)
                    element.OnObjectChangedAfter(args);

                this.OnObjectChangedAfter(args);
                this.Document.OnObjectChangedAfter(args);
            }
        }

        public void InsertElements(IEnumerable<LcElement> elements)
        {
            foreach (var ele in elements)
            {
                InsertElement(ele);
            }
        }
        public bool RemoveElement(LcElement element)
        {
            if (element == null || this != element.Parent)
            {
                return false;
                throw new ArgumentException(SR.ElementNotInContainer, nameof(element));
            }

            if (!IsFireChangedEvent)
            {
                var removed=this.Elements.Remove(element);
                element.Parent = null;
                //如果元素集没有触发事件，根据元素本身触发事件，元素只触发After事件
                if (element.IsFireChangedEvent)
                {
                    var args = new ObjectChangedEventArgs
                    {
                        ElementSet = this,
                        Type = ObjectChangeType.Remove,
                        Target = element
                    };
                    element.OnObjectChangedAfter(args);
                }
                return removed;
            }
            else
            {
                if (!this.Elements.Contains(element))
                {
                    return false;
                }

                var args = new ObjectChangedEventArgs
                {
                    ElementSet = this,
                    Type = ObjectChangeType.Remove,
                    Target = element
                };
                
                if(element.IsFireChangedEvent) 
                    element.OnObjectChangedBefore(args);
                if (args.IsCancel) return false;

                this.OnObjectChangedBefore(args);
                if (args.IsCancel) return false;

                this.Document.OnObjectChangedBefore(args);
                if (args.IsCancel) return false;

                element.OnRemoveBefore();
                this.Elements.Remove(element);
                element.Parent = null;
                element.OnRemoveAfter();

                if (element.IsFireChangedEvent) 
                    element.OnObjectChangedAfter(args);

                this.OnObjectChangedAfter(args);
                this.Document.OnObjectChangedAfter(args);
                return true;
            }
        }

        public void RemoveElements(IEnumerable<LcElement> elements)
        {
            foreach (var ele in elements)
            {
                RemoveElement(ele);
            }
        }
        public LcClipAreaCollection ClipAreas { get; set; }

        public void MoveElement(LcElement element, double dx, double dy)
        {
            element.Translate(dx, dy);
        }

        public LcCircle AddCircle(Vector2 center, double radius)
        {
            var circle = this.Document.CreateObject<LcCircle>();
            circle.Center = center;
            circle.Radius = radius;
            this.InsertElement(circle);
            return circle;
        }

        public DimRadial AddDimRadial(Vector2 start, Vector2 end,LcText dimtext)
        {
            var dim = this.Document.CreateObject<DimRadial>();
            dim.Start = start;
            dim.End = end;
            dim.Dimtext = dimtext;
            this.InsertElement(dim);
            return dim;
        }
        /// <summary>
        /// 根据椭圆的中心，旋转角度，X轴长度，Y轴长度
        /// </summary>
        /// <param name="center"></param>
        /// <param name="rotation"></param>
        /// <param name="radiusX"></param>
        /// <param name="radiusY"></param>
        /// <returns></returns>
        public LcEllipse AddEllipse(Vector2 center,double rotation, double radiusX,double radiusY)
        {
            var ellipse = this.Document.CreateObject<LcEllipse>();
            ellipse.Center = center;
            ellipse.Rotation = rotation;
            ellipse.RadiusX = radiusX;
            ellipse.RadiusY = radiusY;
            this.InsertElement(ellipse);
            return ellipse;
        }
        public LcEllipse AddEllipse(Vector2 center, double rotation, double radiusX, double radiusY,double startAngle,double endAngle)
        {
            var ellipse = this.Document.CreateObject<LcEllipse>();
            ellipse.Center = center;
            ellipse.Rotation = rotation;
            ellipse.RadiusX = radiusX;
            ellipse.RadiusY = radiusY;
            ellipse.StartAngle = startAngle;
            ellipse.EndAngle = endAngle;
            this.InsertElement(ellipse);
            return ellipse;
        }

        public LcPoint AddPoint(Vector2 point)
        {
            var points = this.Document.CreateObject<LcPoint>();
            points.Point = point.Clone();
            this.InsertElement(points);
            return points;
        }

        public LcLine AddLine(Vector2 start, Vector2 end)
        {
            var line = this.Document.CreateObject<LcLine>();
            line.Start = start.Clone();
            line.End = end.Clone();
            this.InsertElement(line);
            return line;
        }

        public DimDiametric AddDimDiametric(Vector2 start, Vector2 end)
        {
            var dim= this.Document.CreateObject<DimDiametric>();
            dim.Start = start;
            dim.End = end;
            //dim.Dimtext = dimtext;
            this.InsertElement(dim);
            return dim;
        }

        public LcRay AddRay(Vector2 start, Vector2 end)
        {
            var dir = (end - start).Normalize();
            var ray = this.Document.CreateObject<LcRay>();
            ray.StartPoint = start;
            ray.Direction = dir;
            this.InsertElement(ray);
            return ray;
        }

        public LcXLine AddXLine(Vector2 start, Vector2 end)
        {
            var dir = (end - start).Normalize();
            var xline = this.Document.CreateObject<LcXLine>();
            xline.StartPoint = start;
            xline.Direction = dir;
            this.InsertElement(xline);
            return xline;
        }

        public LcLine AddQLeader(Vector2 start, Vector2 second, Vector2 end)
        {
            var line = this.Document.CreateObject<LcLine>();
            line.Start = start;
            line.End = end;
            this.InsertElement(line);
            return line;
        }


    }
}