﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public static  class MaterialManager 
    {
        private static MaterialCollection materials = new MaterialCollection();
        public const string DefaultUuid = "0";
        public const string MasonryUuid = "288CC2B9-7DA0-BB0B-40BC-EC658CBAB2A0";
        public const string ConcreteUuid = "0F626F79-F2DF-AEE9-011D-81ED8B29460A";
        public const string WoodUuid = "DCF9ACE2-B105-9AE7-C68C-2C7D1FD7DF6A";
        public const string GlassUuid = "AC5D2585-056B-4A56-9BE4-366FECE411A1";
        public const string Metal1Uuid = "3285AF4F-9738-4747-9570-07925CBE729A";
        public const string Metal2Uuid = "76D50488-ADA7-40FF-B033-CAE6F130D002";
        public const string PtxUuid = "8c4640cc-d42a-4dfc-86b5-fb6d6cd5b063";

        public static List<LcMaterial> WallMaterials = new List<LcMaterial>();
        public static LcMaterial DefaultMat => materials[DefaultUuid].Clone();
        public static void Initilize() 
        {
            materials.Add(new LcMaterial() { Color = new Color(0xaaaaaa), Opcity = 1, Uuid = DefaultUuid, Name = "默认" } );
            materials.Add(new LcMaterial() { Color = new Color(0x00f000), Opcity = 0.4, Uuid = PtxUuid, Name = "碰头线" });
            //materials.Add(new LcMaterial() { Color = new Color(0xaaaaaa), Opcity = 1, Uuid = MasonryUuid, Name = "砌体", Texture = @".\Resources\Texture\MasonryWall.png" });
            materials.Add(new LcMaterial() { Color = new Color(0xaaaaaa), Opcity = 1, Uuid = MasonryUuid, Name = "砌体", Texture = @".\Resources\Texture\Concrete.jpg" });
            materials.Add(new LcMaterial() { Color = new Color(0xffffff), Opcity = 1, Uuid = ConcreteUuid, Name = "混凝土",Texture = @".\Resources\Texture\Concrete.jpg" });
            materials.Add(new LcMaterial() { Color = new Color(0xffffff), Opcity = 1, Uuid = WoodUuid, Name = "木质", Texture = @".\Resources\Texture\Wood3.jpg" });
            materials.Add(new LcMaterial() { Color = new Color(0xffffff), Opcity = 0.3, Uuid = GlassUuid, Name = "玻璃", Roughness = 0 , Shininess =12, Specular =new Color(0x4488ee) });
            materials.Add(new LcMaterial() { Color = new Color(0xffffff), Opcity = 1, Uuid = Metal2Uuid, Name = "金属", Texture = @".\Resources\Texture\Metal2.jpg", Metalness=0.4F, Roughness = 0.2F });
            materials.Add(new LcMaterial() { Color = new Color(0xffffff), Opcity = 1, Uuid = Metal1Uuid, Name = "金属", Texture = @".\Resources\Texture\Metal.jpg", Metalness = 0.4F, Roughness = 0.2F });
            WallMaterials.Add(new LcMaterial() { Color = new Color(0xaaaaaa), Opcity = 1, Uuid = DefaultUuid, Name = "默认" });
            WallMaterials.Add(new LcMaterial() { Color = new Color(0xaaaaaa), Opcity = 1, Uuid = MasonryUuid, Name = "砌体", Texture = @".\Resources\Texture\MasonryWall.png" });
            WallMaterials.Add(new LcMaterial() { Color = new Color(0xffffff), Opcity = 1, Uuid = ConcreteUuid, Name = "混凝土", Texture = @".\Resources\Texture\Concrete.jpg" });
        }
        public static LcMaterial GetMaterial(string uuid)=> materials.Contains(uuid)?materials[uuid].Clone(): null;
    }
    public class LcMaterial
    {
        public string Uuid;
        public Color Color = new Color(0xFFFFFF);
        public double Opcity = 1;
        public string Name;
        public string Texture;
        public float Metalness = 0;
        public float Roughness = 0.8F;
        public Color Specular = new Color(0xFFFFFF);
        public double Shininess = 0;
        public LcMaterial Clone() 
        {
            return new LcMaterial 
            {
                Uuid = Uuid,
                Color= Color,
                Opcity = Opcity,
                Name = Name,
                Texture = Texture,
                Metalness = Metalness,
                Roughness = Roughness
            };
        }
        public int HashCode => (this.Uuid?? "" + this.Color?.ToString()?? "" + this.Opcity+this.Name?? "" + this.Texture??"" + this.Metalness ?? ""+ this.Roughness??"").GetHashCode();
        
    }
    public class MaterialCollection : KeyedCollection<string, LcMaterial>
    {
        protected override string GetKeyForItem(LcMaterial item)
        {
            return item.Uuid;
        }
    }
}
