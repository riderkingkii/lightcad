﻿using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class TextStyle : LcObject
    {
        public string Uuid { get; set; }
    }

    public class TextStyleCollection : KeyedCollection<string, TextStyle>
    {
        protected override string GetKeyForItem(TextStyle item)
        {
            return item.Uuid;
        }
    }
}