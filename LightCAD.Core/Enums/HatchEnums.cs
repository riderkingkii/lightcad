﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    [Flags]
    public enum HatchBoundaryPathTypeFlags
    {
        /// <summary>
        /// Default.
        /// </summary>
        Default = 0,

        /// <summary>
        /// External.
        /// </summary>
        External = 1,

        /// <summary>
        /// Polyline.
        /// </summary>
        Polyline = 2,

        /// <summary>
        /// Derived.
        /// </summary>
        Derived = 4,

        /// <summary>
        /// Text box.
        /// </summary>
        Textbox = 8,

        /// <summary>
        /// Outermost.
        /// </summary>
        Outermost = 16
    }
    public enum HatchStyle
    {
        /// <summary>
        /// Hatch "odd parity" area.
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Hatch outermost area only.
        /// </summary>
        Outer = 1,

        /// <summary>
        /// Hatch through entire area.
        /// </summary>
        Ignore = 2
    }
    /// <summary>
    /// Hatch pattern fill type.
    /// </summary>
    public enum HatchFillType
    {
        /// <summary>
        /// Pattern fill.
        /// </summary>
        PatternFill = 0,

        /// <summary>
        /// Solid fill.
        /// </summary>
        SolidFill = 1
    }

    /// <summary>
    /// Hatch pattern type.
    /// </summary>
    public enum HatchType
    {
        /// <summary>
        /// User defined.
        /// </summary>
        UserDefined = 0,

        /// <summary>
        /// Predefined.
        /// </summary>
        Predefined = 1,

        /// <summary>
        /// Custom.
        /// </summary>
        Custom = 2
    }
}
