﻿namespace LightCAD.Core
{
    public enum ActiveSpace
    {
        ModelSpace,
        PaperSpace,
    }
    
}
