﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    /// <summary>
    /// 元素在运行时的状态
    /// </summary>
    [Flags]
    public enum ElementStatus
    {
        Normal = 0,
        Disabled=1,
        Dragging = 2,
        Selected = 4,
        InputSelected = 8,
        RefEditingSelected = 16,
        Hovered = 32,
    }
}
