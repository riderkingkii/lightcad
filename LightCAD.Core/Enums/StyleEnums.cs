﻿using LightCAD.Core.Elements;

namespace LightCAD.Core
{

    public class ValueFrom
    {
        public const string ByLayer = "ByLayer";
        public const string ByBlock = "ByBlock";
        public const string ByColor = "ByColor";
        //public const string BySelf = "BySelf";
    }

    public enum ElementCategory
    {

    }
   
    public enum LineWeight
    {
        ByLayer,
        ByBlock,
        Default,
        LW000,
        LW005,
        LW009,
        LW013,
        LW015,
        LW018,
        LW020,
        LW025,
        LW030,
        LW035,
        LW040,
        LW050,
        LW053,
        LW060,
        LW070,
        LW080,
        LW090,
        LW100,
        LW106,
        LW120,
        LW140,
        LW158,
        LW200,
        LW211
    }

    public enum ObjectChangeType
    {
        Insert = 0,
        Remove = 1,
    }

    public static class EnumExt
    {
       
    }
}
