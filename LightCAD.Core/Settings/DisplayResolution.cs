﻿namespace LightCAD.Core.Settings
{
    /// <summary>
    /// 显示精度
    /// </summary>
    public class DisplayResolution
    {
        public int ArcCircleSmoothness = 100;
    }
}
