﻿namespace LightCAD.Core
{
    public class IdCreator
    {
        public long ObjectIdIndex { get; internal set; } = 1000000;

        public long GetObjectId()
        {
            return ++ObjectIdIndex;
        }

        public void RestObjectIdMax(long objectId)
        {
            if (objectId > ObjectIdIndex)
            {
                ObjectIdIndex = objectId;
            }
        }
    }
}