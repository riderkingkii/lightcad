﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    /// <summary>
    /// 唯一ID，通过这样封装的数据比较速度最快
    /// #Ques 这个的预期使用场景是什么?  by:Zcb 2023-07-19 00:01
    /// </summary>
    public struct LcGuid
    {
        public static LcGuid Parse(string str)
        {
            var parts = str.Split('-');
            var high = ulong.Parse(parts[0]);
            var low = ulong.Parse(parts[1]);
            return new LcGuid(high, low);
        }
        public ulong High;
        public ulong Low;
        public LcGuid() { }
        public LcGuid(ulong high, ulong low)
        {
            this.High= high;
            this.Low= low;
        }
        

        public override bool Equals([NotNullWhen(true)] object? obj)
        {
            if (obj == null) return false;
            if(!(obj is LcGuid)) return false;
            var lcGuid=(LcGuid)obj;
            return this.High==lcGuid.High && this.Low==lcGuid.Low;
        }
        public bool Equals(LcGuid lcGuid)
        {
            return this.High == lcGuid.High && this.Low == lcGuid.Low;
        }

       public  ulong[]  ToArray()
        {
            return new ulong[] { this.High, this.Low };
        }
        public string ToString()
        {
            return  this.High+"-"+this.Low ;
        }

        public static bool operator ==(LcGuid lhs, LcGuid rhs)
        {
            return lhs.Equals(rhs);
        }

        public static bool operator !=(LcGuid lhs, LcGuid rhs)
        {
            return !(lhs == rhs);
        }
    }

    public static class GuidExt
    {
        public static LcGuid ToLcGuid(this Guid guid)
        {
            byte[] bytes=guid.ToByteArray();
            ulong high=BitConverter.ToUInt64(bytes, 0);
            ulong low = BitConverter.ToUInt64(bytes, 8);
            return new LcGuid(high, low);
        }
        public static Guid TocGuid(this LcGuid lcGuid)
        {
            var highBytes = BitConverter.GetBytes(lcGuid.High);
            var lowBytes = BitConverter.GetBytes(lcGuid.Low);
            var bytes = new byte[16];
            Array.Copy(highBytes, bytes, 8);
            Array.Copy(lowBytes, 0, bytes, 8, 8);
            return new Guid(bytes);
        }
    }
}
