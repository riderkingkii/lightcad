﻿using System.Collections.ObjectModel;

namespace LightCAD.Core
{
    public class Resource : LcObject
    {
        public string Uuid { get; set; }
    }

    public class ResourceCollection : KeyedCollection<string, Resource>
    {
        protected override string GetKeyForItem(Resource item)
        {
            return item.Uuid;
        }
    }
}