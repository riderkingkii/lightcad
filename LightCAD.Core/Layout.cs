﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public enum LayoutType
    {
        One,
        HorzTwo,
        VertTwo,
        Four
    }
    public class LcLayout:LcObject
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public LayoutType Type { get; set; }
        public LcViewport[] Viewports { get; set; }
        public LcLayout() { }
        public void Set(string name,LayoutType type)
        {
            this.Name = name;
            Type = type;
            if (type == LayoutType.One)
                Viewports = new LcViewport[1];
            else if (type == LayoutType.HorzTwo || type == LayoutType.VertTwo)
                Viewports = new LcViewport[2];
            else if (type == LayoutType.Four)
                Viewports = new LcViewport[4];

            for (var i = 0; i < Viewports.Length; i++)
            {
                Viewports[i] = this.Document.CreateObject<LcViewport>();
            }
        }
    }

    public class LayoutCollection : KeyedCollection<string, LcLayout>
    {
        protected override string GetKeyForItem(LcLayout item)
        {
            return item.Id.ToString();
        }
        public LcLayout GetByName(string name)
        {
            foreach(var item in this)
            {
                if(item.Name == name) return item;
            }
            return null;
        }
    }
}
