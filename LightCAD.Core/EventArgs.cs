﻿using System;

namespace LightCAD.Core
{
    /// <summary>
    /// document event for objects
    /// </summary>
    public class ObjectChangedEventArgs : EventArgs
    {
        public ElementSpace ElementSet { get; set; }
        public ObjectChangeType Type { get; set; }
        public LcObject Target { get; set; }
        public bool IsCancel { get; set; }
    }

    /// <summary>
    /// document event for object properties
    /// </summary>
    public class PropertyChangedEventArgs : EventArgs
    {
        public LcObject Object { get; set; }
        public string PropertyName { get; set; }
        public LcProperty ExtProperty { get; set; }
        public LcPropertyGroup ExtPropertyGroup { get; set; }
        public object OldValue { get; set; }
        public object NewValue { get; set; }
    }
}