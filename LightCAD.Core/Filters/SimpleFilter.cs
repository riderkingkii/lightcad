﻿using LightCAD.Core.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core.Filters
{


    public class SimpleFilter
    {
        public ElementSpace Container { get; private set; }
        public SimpleFilter(ElementSpace container)
        {
            this.Container = container;
        }
        public List<T> GetGroup <T>(string name) where T: LcElement
        {
            List<T> Groups = new List<T>();
            foreach (var element in this.Container.Elements)
            {
                if (element.Type ==  BuiltinElementType.Group)
                {
                    LcGroup group =  element as LcGroup;
                    if (group.Name == name)
                    {
                        Groups.Add((T)element);
                    }
                }
            }
            return Groups;
        }
    }
}
