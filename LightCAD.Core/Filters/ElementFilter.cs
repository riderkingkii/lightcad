﻿using LightCAD.Core.Elements;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LightCAD.Core
{

    public class ElementFilter
    {
        public static Tuple<Dictionary<LcDrawing, List<LcElement>>, List<LcElement>> GroupByDrawings(ElementCollection elements)
        {
            //获取模型空间所有图纸
            var drawings = elements.Where((e) => e is LcDrawing).ToArray();

            //通过图纸进行元素分组
            var groups = new Dictionary<LcDrawing, List<LcElement>>();

            //不在图纸里的元素
            var noDrawingElements = new List<LcElement>();

            foreach (var ele in elements)
            {
                if (ele is LcDrawing) continue;
                if (drawings.Length > 0)
                {
                    var isAdded = false;
                    foreach (LcDrawing drawing in drawings)
                    {
                        if (ele.BoundingBox.IntersectsBox(drawing.BoundingBox))
                        {
                            if (!groups.TryGetValue(drawing, out List<LcElement> eles))
                            {
                                eles = new List<LcElement>();
                                groups.Add(drawing, eles);
                            }
                            eles.Add(ele);
                            isAdded = true;
                        }
                    }
                    if (!isAdded) noDrawingElements.Add(ele);
                }
                else
                {
                    noDrawingElements.Add(ele);
                }

            }

            return Tuple.Create(groups, noDrawingElements);
        }


    }
}
