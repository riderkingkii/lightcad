﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public class ElementList : IList<LcElement>
    {
        private List<LcElement> _list = new List<LcElement>();

        public LcElement this[int index] { get => ((IList<LcElement>)_list)[index]; set => ((IList<LcElement>)_list)[index] = value; }

        public int Count => ((ICollection<LcElement>)_list).Count;

        public bool IsReadOnly => ((ICollection<LcElement>)_list).IsReadOnly;

        public void Add(LcElement item)
        {
            ((ICollection<LcElement>)_list).Add(item);
        }

        public void Clear()
        {
            ((ICollection<LcElement>)_list).Clear();
        }

        public bool Contains(LcElement item)
        {
            return ((ICollection<LcElement>)_list).Contains(item);
        }

        public void CopyTo(LcElement[] array, int arrayIndex)
        {
            ((ICollection<LcElement>)_list).CopyTo(array, arrayIndex);
        }

        public IEnumerator<LcElement> GetEnumerator()
        {
            return ((IEnumerable<LcElement>)_list).GetEnumerator();
        }

        public int IndexOf(LcElement item)
        {
            return ((IList<LcElement>)_list).IndexOf(item);
        }

        public void Insert(int index, LcElement item)
        {
            ((IList<LcElement>)_list).Insert(index, item);
        }

        public bool Remove(LcElement item)
        {
            return ((ICollection<LcElement>)_list).Remove(item);
        }

        public void RemoveAt(int index)
        {
            ((IList<LcElement>)_list).RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_list).GetEnumerator();
        }
    }

}
