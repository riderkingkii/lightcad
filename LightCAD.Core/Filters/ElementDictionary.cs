﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public class ElementDictionary : IDictionary<long, LcElement>
    {
        private Dictionary<long, LcElement> _dict = new Dictionary<long, LcElement>();

        public LcElement this[long key] { get => ((IDictionary<long, LcElement>)_dict)[key]; set => ((IDictionary<long, LcElement>)_dict)[key] = value; }

        public ICollection<long> Keys => ((IDictionary<long, LcElement>)_dict).Keys;

        public ICollection<LcElement> Values => ((IDictionary<long, LcElement>)_dict).Values;

        public int Count => ((ICollection<KeyValuePair<long, LcElement>>)_dict).Count;

        public bool IsReadOnly => ((ICollection<KeyValuePair<long, LcElement>>)_dict).IsReadOnly;

        public void Add(long key, LcElement value)
        {
            ((IDictionary<long, LcElement>)_dict).Add(key, value);
        }

        public void Add(KeyValuePair<long, LcElement> item)
        {
            ((ICollection<KeyValuePair<long, LcElement>>)_dict).Add(item);
        }

        public void Clear()
        {
            ((ICollection<KeyValuePair<long, LcElement>>)_dict).Clear();
        }

        public bool Contains(KeyValuePair<long, LcElement> item)
        {
            return ((ICollection<KeyValuePair<long, LcElement>>)_dict).Contains(item);
        }

        public bool ContainsKey(long key)
        {
            return ((IDictionary<long, LcElement>)_dict).ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<long, LcElement>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<long, LcElement>>)_dict).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<long, LcElement>> GetEnumerator()
        {
            return ((IEnumerable<KeyValuePair<long, LcElement>>)_dict).GetEnumerator();
        }

        public bool Remove(long key)
        {
            return ((IDictionary<long, LcElement>)_dict).Remove(key);
        }

        public bool Remove(KeyValuePair<long, LcElement> item)
        {
            return ((ICollection<KeyValuePair<long, LcElement>>)_dict).Remove(item);
        }

        public bool TryGetValue(long key, [MaybeNullWhen(false)] out LcElement value)
        {
            return ((IDictionary<long, LcElement>)_dict).TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_dict).GetEnumerator();
        }
    }
}
