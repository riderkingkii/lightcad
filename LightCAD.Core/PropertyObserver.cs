﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Core
{
    public enum PropertyType
    {
        String,
        Array,
        Double,
        Int
    }
    // 基本元素相关属性
    public class PropertyObserver
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string CategoryName { get; set; }

        public string CategoryDisplayName { get; set; }

        /// <summary>
        /// 数据类型
        /// </summary>
        public PropertyType PropType { get; set; } = PropertyType.String;

        /// <summary>
        /// PropType为Array的数据源
        /// </summary>
        public Func<LcElement, object> Source { get; set; }

        public Func<LcElement, object> Getter { get; set; }

        public Action<LcElement, object> Setter { get; set; }

        public PropertyObserver() { }

        // public virtual Dictionary<string, object> GetPropertyObservers()
        // {
        // //定义一套基本的属性列表，通过反射从HostElement取对应的属性值
        // //没有该属性的显示为null或者不显示
        // return new Dictionary<string, object>();
        // }
        public void SetProperty(string key, string value) { }
    }
}