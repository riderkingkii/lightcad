﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using LightCAD.MathLib;

namespace LightCAD.Core
{

    /// <summary>
    /// 视口用于模型空间的多窗口显示
    /// </summary>
    public class LcViewport : LcObject
    {
        public bool IsActive { get; internal set; }
        /// <summary>
        /// 视口名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 圆弧的光滑度
        /// </summary>
        public int ArcSmoothness { get; set; } = 200;

        /// <summary>
        /// 视口屏幕中心坐标
        /// </summary>
        public Vector2 Center { get; set; } = new Vector2(0, 0);

        /// <summary>
        /// 视口高度
        /// </summary>
        public double Height { get; set; } = 0;

        /// <summary>
        /// 视口宽度
        /// </summary>
        public double Width { get; set; } = 0;

        /// <summary>
        /// 世界坐标系
        /// </summary>
        public Vector2 WorldCenter { get; set; }= new Vector2(0, 0);

        /// <summary>
        /// 世界用户坐标系
        /// </summary>
        public LcUCS Ucs { get; set; }=new LcUCS();

        /// <summary>
        /// 屏幕/世界缩放比例
        /// </summary>
        public double Scale { get; set; } = 1;
      
        /// <summary>
        /// 是否打开网格
        /// </summary>
        public bool GridOn { get; set; } = true;

        /// <summary>
        /// 是否打开捕捉
        /// </summary>
        public bool SnapOn { get; set; } = false;

        /// <summary>
        /// 是否显示用户坐标系图标
        /// </summary>
        public bool UCSIconOn { get; set; } = true;

        /// <summary>
        /// 用户坐标系图标是否显示在原点
        /// </summary>
        public bool UCSIconAtOrigin { get; set; } = true;

    }



  
}




/*

AmbientLightColor 访问视口的环境光颜色。
------------ AnnotationScale 访问与视口关联的 AnnotationScale
BackClipDistance 访问从目标到后剪裁平面的距离（沿着相机目标线）。 正值表示... 更多
BackClipOn 存取后剪裁平面当前是否在视口中打开，否则返回 false。请参阅 DVIEW 中的... 更多
------------ Background 访问当前视口背景的对象 ID。 如果没有定义背景，则返回 NULL。
亮度访问此视口的亮度系数。
------------ CenterPoint 在 WCS 坐标（在图纸空间内）中访问视口实体的中心点。
------------ CircleSides 访问视口的圆形缩放百分比。圆形缩放百分比控制曲面细分的边数... 更多
Contrast 访问视口的对比度系数。
------------ CustomScale 访问视口的自定义比例。 自定义比例定义了图纸空间中的单位与……中的单位的关系。
DefaultLightingOn 存取是否有任何类型的默认灯亮起。
DefaultLightingType 访问视口中使用的默认照明类型。
------------ EffectivePlotStyleSheet 访问布局的打印样式表名称。
Elevation  访问此视口的 ucs 平面的海拔。
FastZoomOn 此属性始终返回 true 且已过时，将在未来版本中删除。
FrontClipAtEyeOn 存取前裁剪平面当前是否位于相机，否则返回 false。请参阅 DVIEW 中的... 更多
FrontClipDistance 访问从目标到前裁剪平面的距离（沿着相机目标线）。 正值表示... 更多
FrontClipOn 存取当前在视口中打开的前剪裁平面，否则返回 false。请参阅 DVIEW 中的... 更多
GridAdaptive 存取网格是否适应在视口中显示比 GRIDUNIT 设置更少的线。
GridBoundToLimits 存取网格绘制是否超出视口中 WCS 和用户定义的 UCS 的限制，否则为 false ... 更多
------------ GridFollow 存取网格是否会跟随视口中的动态 UCS 更改。
------------ GridIncrement 访问 Vector2d，其中 X 值表示网格的 X 间距（以绘图单位为单位）和... 更多
------------ GridMajor 访问视口中每条主要网格线之间的次要网格线的数量。
------------ GridOn 存取网格当前是否在视口中打开，否则返回 false。请参阅 AutoCAD 中的 GRID ... 更多
GridSubdivisionRestricted 存取是否允许在视口中的次要网格间距下方进行细分。如果 GridAdaptive 返回，则忽略此属性... 更多
Height 高度 以绘图单位访问视口实体窗口的高度。
HiddenLinesRemoved 访问视口设置为在绘图期间删除隐藏线，否则返回 false。有关视口的更多信息... 更多
LensLength 访问在视口中启用透视模式时使用的镜头长度。请参阅 AutoCAD 命令参考中的 DVIEW ... 更多
LinkedToSheetView 存取视口是否链接到工作表视图。
------------ Locked 存取视口的比例是否锁定。 当视口被锁定时，其相对于图纸空间的缩放因子不能... 更多
NonRectClipEntityId 访问视口的单个剪辑实体。
NonRectClipOn 存取当前是否为此视口打开了非矩形裁剪。
------------ Number 访问视口 ID 号。 这是 AutoCAD CVPORT 系统变量在...时报告的数字。
------------ On 存取网格当前是否在视口中打开，否则返回 false。请参阅 AutoCAD 中的 GRID ... 更多
PerspectiveOn 存取透视模式当前是否在视口中打开，否则返回 false。请参阅 DVIEW ... 更多
PlotAsRaster 存取视口是否将根据视口的当前阴影图设置绘制为光栅。
------------ PlotStyleSheet 访问应用于此视口中对象的样式表。
PlotWireframe 存取视口是否将根据视口的当前阴影图设置绘制为矢量输出.... 更多
ShadePlot 访问当前视口的阴影图模式。 阴影绘图模式指定当前视口将如何绘制……更多
ShadePlotId 访问与此视口关联的阴影对象。 阴影对象是一种视觉样式或渲染预设，它定义了... 更多
SnapAngle 访问视口的捕捉角度设置（以弧度为单位）。捕捉角度在 UCS XY 平面内，... 更多
SnapBasePoint 访问视口的捕捉基点（在 UCS 坐标中）。有关捕捉基点的更多信息，请参阅附录中的 SNAPBASE... 更多
SnapIncrement 访问视口的捕捉基点（在 UCS 坐标中）。有关捕捉基点的更多信息，请参阅附录中的 SNAPBASE ... 更多

SnapIsometric 存取捕捉模式当前是否在视口中设置为等轴测，否则返回 false。请参阅 SNAP ... 更多
SnapIsoPair 访问视口的捕捉 IsoPair 设置。捕捉 IsoPair 指示视口的当前等距平面。可能... 更多
SnapOn 存取当前是否在视口中打开捕捉模式，否则返回 false。请参阅 AutoCAD 中的 SNAP ... 更多
StandardScale 此函数使用 StdScaleType 枚举定义的标准比例返回视口的比例。枚举  StandardScaleType
SunId 访问与此视口关联的 Sun 对象。 如果未设置 sun 对象，则返回 Null。
Thumbnail 访问缩略图的打包 BITMAPINFO 结构，缩略图数据紧随其后在内存中。 如果没有设置缩略图，它将被设置为空。
ToneOperatorParameters 访问此视口的色调参数。

Transparent 透明 未实现。
------------ TwistAngle 访问视口中视图的扭曲角度（以弧度为单位）。
扭曲角是围绕视图视线的旋转（即从相机到目标的矢量，它始终垂直于显示器，从显示器的前面看向后面）。 零角度水平向右（即显示坐标系的正 X 轴）。
有关视图扭曲角的详细信息，请参见 AutoCAD 命令参考中的 DVIEW。

------------ UcsFollowModeOn 存取视口是否设置为每当 UCS 在视口中更改时生成并显示平面图，否则返回 false。
有关 UCSFOLLOW 模式的更多信息，请参见 AutoCAD 命令参考附录 B（系统变量）中的 UCSFOLLOW。

------------ Ucs 访问表示分配给视口的 UCS 的 CoordinateSystem3d 值。
------------ UcsIconAtOrigin 评估视口当前是否设置为使 UCS 图标遵循当前 UCS 原点，只要原点在视口内足够远以允许图标正确显示。 如果图标未设置为遵循视口中的 UCS 原点，则返回 false。
有关 UCS 图标跟随原点的详细信息，请参见 AutoCAD 命令参考中的 UCSICON。

------------ UcsIconVisible 存取视口当前是否设置为显示 UCS 图标，否则返回 false。请参阅 UCSICON ... 更多
------------ UcsName 如果此视口的 ucs 是命名的 ucs，则访问 UcsTableRecord 的对象 ID，否则返回空 ID。
UcsOrthographic 检索正交 UCS。
UcsPerViewport 存取视口的 UCSVP 值。 如果 ucsvp 为真，则只要视口处于活动状态，与此视口一起保存的 UCS 就会变为活动状态。
ViewCenter 访问视口中视图的视图中心（在显示坐标系坐标中）。
------------ ViewDirection 访问从视图目标到视图相机的矢量（在模型空间 WCS 坐标中）。 这也代表了... 更多
------------ ViewHeight 访问视口内模型空间视图的高度（在显示坐标系坐标中）。 在视口内缩小视图会增加此值，而放大会减少此值。
ViewOrthographic 访问正交视图。
------------ ViewTarget 访问视图目标的位置（在模型空间 WCS 坐标中）。
VisualStyleId 访问对此视口有效的视觉样式的对象 ID。
------------ Width 以绘图单位访问视口实体窗口的宽度。 这是视口本身在图纸空间中的宽度，而不是视口内模型空间视图的宽度。

*/


