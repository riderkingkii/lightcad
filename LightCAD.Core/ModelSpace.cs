﻿namespace LightCAD.Core
{
    public class LcModelSpace : ElementSpace
    {
        public LcModelSpace()
        {
            this.Elements = new ElementCollection();
        }
        public LcModelSpace(LcDocument document) : this()
        {
            this.Document = document;
        }
    }

}
