using LightCAD.UI;

namespace LightCAD.WinForm
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
       {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            App.Current.Initialize(args);
            var mainWin=App.Current.CreateMainWindow();
            mainWin.WindowMax();

            //mainWin.SetSplashScreen(() =>
            //{
            //    mainWin.Visible = false;
            //    mainWin.Width = 1280;
            //    mainWin.Height = 720;
            //    mainWin.WindowCenter();
            //    mainWin.Visible = true;
            //    mainWin.WindowMax();
            //});

            Application.Run();

            //Application.Run(new MainFormEx());
        }
    }
}