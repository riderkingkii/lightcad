﻿using LightCAD.Core;
using LightCAD.MathLib;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing
{
    public class SnapSettings
    {

        /// <summary>
        /// 捕捉的最大距离，屏幕距离
        /// </summary>
        public static float MaxDistance = 10;
        public static float RefPointSize = 9;
        public static SKPaint RefPointPen = new SKPaint { Color = SKColors.LightGreen, IsStroke = true,StrokeWidth=1f };
        public static float SnapPointSize = 11;
        public static SKPaint SnapPointPen = new SKPaint { Color = SKColors.LightGreen, IsStroke = true, StrokeWidth = 2f };
        public static SKPaint SnapOuterPointPen = new SKPaint { Color = SKColors.White, IsStroke = true, StrokeWidth = 2f };
        public static SKPaint SnapExtLinePen = new SKPaint { Color = SKColors.LightGreen, IsStroke = true, StrokeWidth = 1f,PathEffect= SKPathEffect.CreateDash(new float[] { 5, 5 }, 0) };

        public static SnapSettings Current { get; } = new SnapSettings();


        public bool TempEnabled { get; set; } = true;

        public bool Enabled { get; set; } = true;

        /// <summary>
        /// 是否捕捉间距
        /// </summary>
        public bool SpacingOn { get; set; } = false;
        /// <summary>
        /// 间隔X轴间距
        /// </summary>
        public double SpacingX { get; set; } = 10;
        /// <summary>
        /// 间隔Y轴间距
        /// </summary>
        public double SpacingY { get; set; } = 10;
        /// <summary>
        /// 是否强制间距相等
        /// </summary>
        public bool SpacingXYEqual { get; set; } = true;

        /// <summary>
        /// 是否启动栅格捕捉
        /// </summary>
        public bool GridOn { get; set; } = false;
        /// <summary>
        /// 是否启动模型空间栅格捕捉
        /// </summary>
        public bool GridOnModelSpace { get; set; } = false;
        /// <summary>
        /// 是否启动块编辑栅格捕捉，打开块编辑器的情况下
        /// </summary>
        public bool GridOnBlockEditor { get; set; } = false;
        /// <summary>
        /// 是否启动图纸空间栅格捕捉
        /// </summary>
        public bool GridOnPaperSpace { get; set; } = false;
        /// <summary>
        /// 栅格X轴间距
        /// </summary>
        public double GridSpaceX { get; set; } = 10;
        /// <summary>
        /// 栅格Y轴间距
        /// </summary>
        public double GridSpaceY { get; set; } = 10;
        /// <summary>
        /// 栅格单元的栅格数量
        /// </summary>
        public double GridUnitCount { get; set; } = 5;
        /// <summary>
        /// 是否强制栅格间距相等
        /// </summary>
        public bool GridSpaceEqual { get; set; } = true;

        /// <summary>
        /// 是否开启极轴跟踪
        /// </summary>
        public bool PolarTracing { get; set; }
        /// <summary>
        /// 极轴角度
        /// </summary>
        public double PolarAngle { get; set; } = 90;
        //常用角度
        public List<double> PolarFreqAngles { get; set; } = new List<double> { 30, 45, 60, 90 };
        /// <summary>
        /// 角度测量是否采用绝对角度，如果False，则相对最近的线段PolarAngleMeasureLastSegment
        /// </summary>
        public bool PolarAngleMeasureAbsolute { get; set; } = true;



        /// <summary>
        /// 是否开启对象捕捉跟踪，自动有水平、线垂直线，其他根据捕捉设置垂线
        /// </summary>
        public bool ObjectSnapTracking { get; set; }

        /// <summary>
        /// 是否显示动态提示
        /// </summary>
        public bool DynamicPrompts { get; set; }
        /// <summary>
        /// 是否显示动态提示扩展信息
        /// </summary>
        public bool DynamicPromptsExt { get; set; }



        /// <summary>
        /// 是否开启二维对象捕捉
        /// </summary>
        public bool ObjectOn { get; set; } = true;
        /// <summary>
        /// 二维对象捕捉点类型
        /// </summary>
        public SnapPointType PointType { get; set; } = SnapPointType.Endpoint | SnapPointType.Midpoint | SnapPointType.Nearest | SnapPointType.ExtensionLine | SnapPointType.Center | SnapPointType.Quadrant | SnapPointType.Intersection;

        /// <summary>
        /// 是否开启三维对象捕捉
        /// </summary>
        public bool Object3dOn { get; set; } = false;
        /// <summary>
        /// 三维对象捕捉模式
        /// </summary>
        public Snap3dPointType Point3dType { get; set; } = Snap3dPointType.None;

        /// <summary>
        /// 检查当前捕捉是否可用
        /// </summary>
        /// <returns></returns>
        public bool CheckEnabled()
        {
            return this.TempEnabled && this.Enabled;
        }

        /// <summary>
        /// 从其他设置复制当前设置
        /// </summary>
        /// <param name="ss"></param>
        public void Copy(SnapSettings ss)
        {
            this.Enabled = ss.Enabled;
            this.SpacingOn = ss.SpacingOn;
            this.SpacingX = ss.SpacingX;
            this.SpacingY = ss.SpacingY;
            this.SpacingXYEqual = ss.SpacingXYEqual;

            this.GridOn = ss.GridOn;
            this.GridSpaceX = ss.GridSpaceX;
            this.GridSpaceY = ss.GridSpaceY;
            this.GridSpaceEqual = ss.GridSpaceEqual;
            this.GridOnModelSpace = ss.GridOnModelSpace;
            this.GridOnPaperSpace = ss.GridOnPaperSpace;
            this.GridOnBlockEditor = ss.GridOnBlockEditor;

            this.DynamicPrompts = ss.DynamicPrompts;
            this.DynamicPromptsExt = ss.DynamicPromptsExt;

            this.PolarTracing = ss.PolarTracing;
            this.PolarAngle = ss.PolarAngle;
            this.PolarAngleMeasureAbsolute = ss.PolarAngleMeasureAbsolute;
            this.PolarFreqAngles = ss.PolarFreqAngles.Clone<double>();

            this.ObjectOn = ss.ObjectOn;
            this.PointType = ss.PointType;
            this.Object3dOn = ss.Object3dOn;
            this.Point3dType = ss.Point3dType;
        }

        /// <summary>
        /// 将当前设置克隆为新对象
        /// </summary>
        /// <returns></returns>
        public SnapSettings Clone()
        {
            var ssClone = new SnapSettings();
            ssClone.Copy(this);
            return ssClone;
        }
    }
}
