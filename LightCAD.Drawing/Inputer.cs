﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LightCAD.Drawing
{
    public class InputResult
    {
        public object ValueX { get; set; }
        public string Option { get; set; }
        public object Extent { get; set; }
        public bool IsCancelled { get; set; }

        public InputResult()
        {
        }
        public InputResult(object value, string option)
        {
            ValueX = value;
            Option = option;
        }
        public InputResult(object value, string option, object extent)
        {
            this.ValueX = value;
            this.Option = option;
            this.Extent = extent;
        }

    }
    public class Inputer
    {
        protected CancellationTokenSource cts;
        protected bool isInputed;
        public bool isCancelled;
        protected DocumentRuntime docRt;
        protected ViewportRuntime vportRt;
        protected IDocumentEditor docEditor;
        protected ICommandControl commandCtrl;

        protected Inputer(IDocumentEditor docEditor)
        {
            this.docEditor= docEditor;  
            this.docRt = docEditor.DocRt;
            this.vportRt = (docEditor as DrawingEditRuntime).ActiveViewportRt ;
            this.commandCtrl = this.vportRt.CommandCenter.Commander;
        }
        protected void AttachEvents()
        {
            this.vportRt.Control.AttachEvents(
               this.OnViewportMouseEvent, this.OnViewportKeyDown,null, null, null
            );
            this.commandCtrl.AttachInputEvent(OnInputBoxKeyDown, OnInputBoxKeyUp,OnInputGotFocus,OnInputLossFocus);
        }



        protected void DetachEvents()
        {
            this.vportRt.Control.DetachEvents(
                this.OnViewportMouseEvent, this.OnViewportKeyDown,null, null, null
             );
            this.commandCtrl.DetachInputEvent(OnInputBoxKeyDown, OnInputBoxKeyUp, OnInputGotFocus, OnInputLossFocus);
        }

        protected virtual void OnViewportMouseEvent(string type, MouseEventRuntime e)
        {
            if (type == "Down") OnViewportMouseDown(e);
            else if (type == "Move") OnViewportMouseMove(e);
            else if (type == "Up") OnViewportMouseUp(e);
            else if (type == "Wheel") OnViewportMouseWheel(e);
            else if (type == "HoverStart") OnViewportMouseHoverStart(e);
            else if (type == "HoverEnd") OnViewportMouseHoverEnd(e);
        }
        protected virtual void OnViewportMouseHoverStart(MouseEventRuntime e)
        {
        }
        protected virtual void OnViewportMouseHoverEnd(MouseEventRuntime e)
        {
        }
        protected virtual void OnViewportMouseDown(MouseEventRuntime e)
        {
        }
        protected virtual void OnViewportMouseMove(MouseEventRuntime e)
        {
        }
        protected virtual void OnViewportMouseUp(MouseEventRuntime e)
        {
        }
        protected virtual void OnViewportMouseWheel(MouseEventRuntime e)
        {
        }
        protected virtual void OnViewportKeyDown(KeyEventRuntime e)
        {
             if(e.KeyCode == 27 || e.KeyCode==13 || e.KeyCode == 32)//Escape /Enter /Space
            {
                OnInputBoxKeyDown(new KeyEventRuntime { KeyCode=e.KeyCode, KeyModifiers=e.KeyModifiers });
            }
        }
        protected virtual void OnInputBoxKeyDown(KeyEventRuntime e)
        {
            if (e.KeyCode == 27)//Keys.Escape
            {
                this.Cancel();
            }
            else if (e.KeyCode == 13 || e.KeyCode == 32) 
            {
                var text = this.commandCtrl.GetInputText().Trim();
                var prompt = this.commandCtrl.GetPrompt().Trim();
                WriteInfo(prompt, text);
            }
        }
        protected virtual void OnInputBoxKeyUp(KeyEventRuntime e)
        {
        }
        public virtual void OnInputGotFocus(ICommandControl control, EventArgs args)
        {
        }
        public virtual void OnInputLossFocus(ICommandControl control, EventArgs args)
        {
        }
        protected async Task WaitFinish()
        {
            cts = new CancellationTokenSource();
            await Task.Run(() =>
            {
                while (!cts.Token.IsCancellationRequested && !isInputed)
                {
                }
            }, cts.Token);
        }

        protected void Finish()
        {
            this.isInputed = true;
        }
        protected void Cancel()
        {
            this.isCancelled = true;
            this.cts.Cancel();
        }
        public void Prompt(string prompt)
        {
            this.commandCtrl.Prompt(prompt);
        }
        public void WriteInfo(string prompt,string input = "", string result="")
        {
            this.commandCtrl.WriteInfo(prompt+ input + result);
        }
        public void Reset()
        {
            this.isCancelled = false;
            this.isInputed = false;
        }
    }
}
