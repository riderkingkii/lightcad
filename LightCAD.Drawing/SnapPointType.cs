﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing
{

    [Flags]
    public enum SnapPointType
    {
        /// <summary>
        /// 无
        /// </summary>
        None = 0,
        /// <summary>
        /// 端点
        /// </summary>
        Endpoint = 1,
        /// <summary>
        /// 中点
        /// </summary>
        Midpoint = 2,
        /// <summary>
        /// 圆心
        /// </summary>
        Center = 4,
        /// <summary>
        /// 节点
        /// </summary>
        Node = 8,
        /// <summary>
        /// 象限点，圆的四方向点
        /// </summary>
        Quadrant = 16,
        /// <summary>
        /// 交点
        /// </summary>
        Intersection = 32,
        /// <summary>
        /// 插入点
        /// </summary>
        Insertion = 64,
        /// <summary>
        /// 垂足
        /// </summary>
        Perpendicular = 128,
        /// <summary>
        /// 切点
        /// </summary>
        Tangent = 256,
        /// <summary>
        /// 最近点
        /// </summary>
        Nearest = 512,
        /// <summary>
        /// 几何中心
        /// </summary>
        GeometricCenter = 1024,
        /// <summary>
        /// 延长线
        /// </summary>
        ExtensionLine = 2048,
        /// <summary>
        /// 平行线
        /// </summary>
        ParallelLine = 4096,
        /// <summary>
        /// 切线
        /// </summary>
        TangentLine = 8192,
        /// <summary>
        /// 所有类型
        /// </summary>
        All= Endpoint|Midpoint| Center|Node| Quadrant| Intersection| Perpendicular| Tangent|Nearest| GeometricCenter| ExtensionLine| ParallelLine| TangentLine,
    }

    public static class SnapPointTypeExt
    {
        public static bool Has(this  SnapPointType type, SnapPointType match)
        {
            return (type & match) == match;
        }
    }

    [Flags]
    public enum Snap3dPointType
    {
        None = 0,
        /// <summary>
        /// 顶点
        /// </summary>
        Vertex = 1,
        /// <summary>
        /// 边上的中点
        /// </summary>
        MidOnEdge = 2,
        /// <summary>
        /// 边上的点
        /// </summary>
        OnEdge = 4,
        /// <summary>
        /// 面上的点
        /// </summary>
        OnFace = 8,
        /// <summary>
        /// 节点
        /// </summary>
        Knot = 16,
        /// <summary>
        /// 垂足
        /// </summary>
        Perpendicular = 32,
        /// <summary>
        /// 外观交点
        /// </summary>
        AppearenceIntersection = 64,
        /// <summary>
        /// 所有
        /// </summary>
        All= Vertex| MidOnEdge| OnEdge| OnFace| Knot| Perpendicular| AppearenceIntersection
    }

}