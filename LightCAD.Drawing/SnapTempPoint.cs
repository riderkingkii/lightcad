﻿using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.MathLib;
namespace LightCAD.Drawing
{
    /// <summary>
    /// 捕捉临时点，MoveOver之后添加，再次MoveOver取消
    /// </summary>
    public class SnapTempPoint
    {
        public LcElement Element { get; set; }
        public Vector2 Point { get; set; }
        public string Location { get; set; }
    }
}
