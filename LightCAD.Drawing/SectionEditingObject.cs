﻿using LightCAD.Core.Elements;
using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightCAD.Runtime;
using LightCAD.MathLib;
using LightCAD.Core.Element3d;
using static System.Collections.Specialized.BitVector32;

namespace LightCAD.Drawing
{
    public class SectionEditingObject : ElementSpace
    {
        public ViewportRuntime VportRt { get; set; }
        public LcSection Target { get; set; }
 
        public void SyncElements()
        {
            if (Target == null)
                return;
            var Segments = Target.Segments;
            var InnerSegmentLists = Target.InnerSegments;
            var isClockWise = ShapeUtils.isClockWise(Segments.SelectMany(n => n.GetPoints((n is Arc2d ? 32 : 1))).ToListEx());
            var newShape = new List<Curve2d>();
            // var offset = this.InsertPoint.Clone().Negate();
            for (var i = Segments.Count() - 1; i >= 0; i--)
            {
                var cur = Segments[i];
                //cur.Translate(offset);
                if (isClockWise)
                {
                    cur.Reverse();
                    newShape.Add(cur.Clone());
                }
            }
            if (isClockWise)
                Segments = newShape;
            for (var k = 0; k < InnerSegmentLists.Count; k++)
            {
                isClockWise = ShapeUtils.isClockWise(InnerSegmentLists[k].SelectMany(n => n.GetPoints((n is Arc2d ? 32 : 1))).ToListEx());
                var newInnerShape = new List<Curve2d>();
                for (var i = InnerSegmentLists[k].Count() - 1; i >= 0; i--)
                {
                    var cur = InnerSegmentLists[k][i];
                    //cur.Translate(offset);
                    if (!isClockWise)
                    {
                        cur.Reverse();
                        newInnerShape.Add(cur.Clone());
                    }
                }
                if (!isClockWise)
                    InnerSegmentLists[k] = newInnerShape;
            }
            Target.Segments = Segments;
            Target.InnerSegments = InnerSegmentLists;
            Target.OnPropertyChangedAfter("Section", null, null);
        }

        public void Dispose()
        {
        }


    }
}
