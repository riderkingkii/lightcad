﻿using LightCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Drawing
{

    public class DrawingEditRuntime : IDocumentEditor
    {
        public IDrawingEditControl Control { get; set; }

        public ViewportRuntime ActiveViewportRt { get; set; }

        public string Name { get; private set; } = "绘图";

        public DocumentRuntime DocRt { get; private set; }

        public EditorType EditorType { get; set; } = EditorType.Drawing;

        public CommandCenter CommandCenter { get; set; }

        public DrawingEditRuntime(DocumentRuntime docRt, IDrawingEditControl control, ICommandControl commandControl)
        {
            this.DocRt = docRt;
            this.Control = control;
            this.CommandCenter = new CommandCenter(this, commandControl);
            this.ActiveViewportRt =new ViewportRuntime(this, control.ViewPort,control);
            this.Control.RefreshViewport();
           
        }

        public void CancelAll()
        {
            this.ActiveViewportRt.CancelAll();
        }

        public void SelectAll()
        {
            this.ActiveViewportRt.SelectAll();
        }

        public void CommandExecute(LcCommand cmd)
        {
            this.ActiveViewportRt.CommandExecute(cmd);
        }

        public void SetActive(bool active)
        {
            this.DocRt.Document.SetActiveViewport(this.ActiveViewportRt.Viewport.Id);
            this.CommandCenter.SetActive();
            this.Control.SetActive(active);
        }
    }
}
