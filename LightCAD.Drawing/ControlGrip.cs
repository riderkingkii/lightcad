﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkiaSharp;

using LightCAD.MathLib;
using LightCAD.Core;
using LightCAD.Core.Elements;

namespace LightCAD.Drawing
{
    public enum GripShape
    {
        Rect,
        Triangle,
        Stripe
    }
    public class ControlGrip
    {
        public const float GripSize = 4;
        public static SKColor GripColor = SKColors.Blue;
        public static SKColor HoveredColor = SKColors.Red;
        public static SKColor SelectedColor = SKColors.Red;

        public string Name { get; set; }
        public int Index { get; set; }
        public GripShape Shape { get; set; }
        public LcElement Element { get; set; }

        public double Angle { get; set; }
        public Vector2 Position { get; set; }

        public bool IsSelected { get; set; }

        public bool IsHovered { get; set; }
        public object Data { get; set; }

        public bool IntersectWithBox(Box2 box)
        {
            var thisBox = new Box2(new Vector2(Position.X - GripSize, Position.Y - GripSize), GripSize * 2, GripSize * 2);
            return thisBox.IntersectsBox(box);
        }
    }


}
