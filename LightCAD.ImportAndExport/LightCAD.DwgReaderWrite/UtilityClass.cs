﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Teigha.Colors;
using Teigha.DatabaseServices;

namespace LightCAD.ImpExpDwg
{
    internal class UtilityClass
    {
        public static Database ReadDwgFile(string dwgPath)
        {
            Database db = new Database(false, true);
            db.ReadDwgFile(dwgPath,
           FileOpenMode.OpenTryForReadShare, false, "", false);
            db.CloseInput(true);
            return db;
        }
        public static Database WriteDwgFile(string dwgPath)
        {
            Database db = new Database(false, true);
            db.ReadDwgFile(dwgPath,
           FileOpenMode.OpenForReadAndWriteNoShare, false, "", false);
            db.CloseInput(true);
            return db;
        }
        public static LayerTableRecord AddLayer(Database db, Transaction tx, string layerName, short color, string linetype, string lineWeight)
        {
            try
            {
                color = Math.Abs(color);
                if (string.IsNullOrEmpty(layerName))
                {
                    return null;
                }
                layerName = layerName.Trim();
                LayerTableRecord acLyrTblRec = null;

                LayerTable acLyrTbl = tx.GetObject(db.LayerTableId, OpenMode.ForRead) as LayerTable;
                // 以读模式打开线型表
                LinetypeTable acLinTbl = tx.GetObject(db.LinetypeTableId, OpenMode.ForRead) as LinetypeTable;
                if (acLyrTbl.Has(layerName) == false)
                {
                    acLyrTblRec = new LayerTableRecord();
                    // 给图层名赋值
                    acLyrTblRec.Name = layerName;
                    if (layerName.StartsWith("DEFPOINTS"))
                    {
                        acLyrTblRec.IsPlottable = false;
                    }

                    if (color <= 0 || color > 255)
                    {
                        color = 255;
                    }
                    acLyrTblRec.Color = Color.FromColorIndex(ColorMethod.ByAci, color);

                    if (!string.IsNullOrEmpty(linetype))
                    {
                        if (acLinTbl.Has(linetype))
                            acLyrTblRec.LinetypeObjectId = acLinTbl[linetype];
                    }

                    if (!string.IsNullOrEmpty(lineWeight))
                        acLyrTblRec.LineWeight = (LineWeight)Enum.Parse(typeof(LineWeight), lineWeight);

                    // 以写模式升级打开图层表
                    acLyrTbl.UpgradeOpen();

                    // 添加新图层到图层表，记录事务
                    acLyrTbl.Add(acLyrTblRec);
                    tx.AddNewlyCreatedDBObject(acLyrTblRec, true);
                }
                else
                {
                    acLyrTblRec = tx.GetObject(acLyrTbl[layerName], OpenMode.ForWrite) as LayerTableRecord;
                }

                return acLyrTblRec;
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

        public static string GetObjectExtProp(DBObject Obj, string propName)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                var newPropName = "DXMX_" + propName + "_0";
                var result = Obj.GetXDataForApplication(newPropName);
                if (result != null)
                {
                    var txt = result.AsArray()[1].Value.ToString();
                    if (txt.Length >= 18 && txt.Substring(15, 3) == "|&|")
                    {
                        var hashcode = txt.Substring(0, 15);
                        var value = txt.Substring(18);
                        return value;
                    }

                    var values = result.AsArray();
                    for (int i = 1; i < values.Length; i++)
                    {
                        sb.Append(values[i].Value.ToString());
                    }
                    return sb.ToString();
                }
                else
                {
                    #region 旧的属性读取方式

                    //兼容读取旧的数据
                    //var cacheValue = LoadExtPropObjValue(Obj, propName, null);
                    //if (!string.IsNullOrEmpty(cacheValue))
                    //{
                    //    return cacheValue;
                    //}

                    result = Obj.GetXDataForApplication(propName);
                    if (result != null)
                    {
                        var txt = result.AsArray()[1].Value.ToString();
                        if (txt.Length >= 18 && txt.Substring(15, 3) == "|&|")
                        {
                            var hashcode = txt.Substring(0, 15);
                            var value = txt.Substring(18);

                            try
                            {
                                //if (value.GetHashCode() == Convert.ToInt32(hashcode))
                                //{
                                //    //将旧的属性写入方法替换未新的
                                //    SetObjectExtProp(Obj, propName, value);
                                //    return value;
                                //}
                                return value;
                            }
                            catch { return ""; }
                        }
                        else
                        {
                            //将旧的属性写入方法替换未新的
                            //SetObjectExtProp(Obj, propName, txt);

                            return txt;
                        }
                    }
                    else
                        return "";

                    #endregion 旧的属性读取方式
                }
            }
            catch (System.Exception exp)
            {
            }

            return "";
        }
   
    }
}
