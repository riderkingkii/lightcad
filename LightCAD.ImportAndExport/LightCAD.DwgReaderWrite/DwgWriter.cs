﻿using LightCAD.Core;
using LightCAD.Core.Elements;
using System.Drawing;
using Teigha.DatabaseServices;
using Teigha.Runtime;

namespace LightCAD.ImpExpDwg
{
    public class DwgWriter
    {
        private static string modelDwgPath { get => System.AppDomain.CurrentDomain.BaseDirectory + "//ModelDwg//acadiso空模板.dwg"; }
        public static void Export(string saveAsPath, LcDocument lcDocument)
        {
            using (Services services = new Services())
            {
                //注册码加载
                Teigha.Runtime.Services.odActivate(ActivationData.userInfo, ActivationData.userSignature);
                ReaderWriteException.Init();
                Database database = UtilityClass.ReadDwgFile(modelDwgPath);
                using (var tran = database.TransactionManager.StartTransaction())
                {
                    BlockTable blockTable = (BlockTable)database.BlockTableId.GetObject(OpenMode.ForWrite);
                    var modelSpace = (BlockTableRecord)blockTable[BlockTableRecord.ModelSpace].GetObject(OpenMode.ForWrite);
                    LinetypeTable acLinTbl = (LinetypeTable)database.LinetypeTableId.GetObject(OpenMode.ForWrite);
                    LayerTable layerTable = (LayerTable)database.LayerTableId.GetObject(OpenMode.ForWrite);
                    //图层
                    foreach (var lcLayer in lcDocument.Layers)
                    {
                        if (layerTable.Has(lcLayer.Name))
                        {
                            continue;
                        }
                        LayerTableRecord layerTableRecord = new LayerTableRecord();
                        layerTableRecord.Name = lcLayer.Name;
                        layerTableRecord.IsOff = lcLayer.IsOff;
                        layerTableRecord.IsFrozen = lcLayer.IsFrozen;
                        layerTableRecord.IsLocked = lcLayer.IsLocked;
                        layerTableRecord.Color = Teigha.Colors.Color.FromColor(Color.FromArgb((int)lcLayer.Color));
                        layerTable.Add(layerTableRecord);
                    }
                    //块定义处理
                    foreach (var lcBlock in lcDocument.Blocks)
                    {
                        if (blockTable.Has(lcBlock.Name))
                        {
                            continue;
                        }
                        BlockTableRecord block = new BlockTableRecord();
                        block.Name = lcBlock.Name.Replace("*", "");
                        foreach (var lcElement in lcBlock.Elements)
                        {
                            var entity = ConvertToAutoCAD.ConvertLcElementToEntity(database, lcElement);
                            if (entity != null)
                            {
                                block.AppendEntity(entity);
                            }
                        }
                        blockTable.Add(block);
                    }
                    foreach (LcElement lcElement in lcDocument.ModelSpace.Elements)
                    {
                        try
                        {
                            var entity = ConvertToAutoCAD.ConvertLcElementToEntity(database, lcElement);
                            if (entity != null)
                            {
                                if (lcElement.Color != "ByLayer")
                                {
                                    var html = ColorTranslator.FromHtml(lcElement.Color);
                                    entity.Color = Teigha.Colors.Color.FromColor(html);
                                    var layerRef = UtilityClass.AddLayer(database, tran, lcElement.Layer, (short)entity.ColorIndex, lcElement.LineType, lcElement.LineWeight);
                                    entity.LayerId = layerRef.Id;
                                }

                                if (acLinTbl.Has(lcElement.LineType))
                                {
                                    entity.LinetypeId = acLinTbl[lcElement.LineType];
                                }

                                modelSpace.AppendEntity(entity);
                                tran.AddNewlyCreatedDBObject(entity, true);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            ReaderWriteException.Add(ex);
                        }
                    }
                    tran.Commit();
                }
                database.SaveAs(saveAsPath, DwgVersion.AC1800);
                ReaderWriteException.OutputMessage();
            }
        }


    }
}