///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2002-2023, Open Design Alliance (the "Alliance").
// All rights reserved.
//
// This software and its documentation and related materials are owned by
// the Alliance. The software may only be incorporated into application
// programs owned by members of the Alliance, subject to a signed
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable
// trade secrets of the Alliance and its suppliers. The software is also
// protected by copyright law and international treaty provisions. Application
// programs incorporating this software must include the following statement
// with their copyright notices:
//
//   This application incorporates Open Design Alliance software pursuant to a
//   license agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2023 by Open Design Alliance.
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

using System;
#pragma warning disable 618

public sealed class ActivationData
{
public const string userInfo = "WWljaHVuIFFpbmxvbmcgTmV0d29yayBUZWNobm9sb2d5IENvLiwgTHRkLg==";
public const string userSignature = "DLarf+qsQDTVxNy0T0THl3TXsqYNCzd2QsYZdjLt+ThP7TxjcwSq16zump2S5pvNkKvR6PEpVnd+gof4pm6jvjexUXjlUSjyw1RBBQCX9aEHCthvlu+Q6RG4VzAOBFoy+FXkD0H5ElEhoP9s72CERIeGf3umWsE4xjJn0RLSDkDBIza8231OgGEeMRZxfVY/i4IBJDOgIw+IGuaaswovch/KvWUkScl4NpNvzn1u/40kTf1QgpUWRm4oLPnL6KSR4487KDSjadJvegqlx6SneX/nqZeOPDt605fTYQxHXw3HbM257GphrhoBSSFrI3jM0jZNdKF4onRwbtMc9ACoivkM+f0kRY0yM45sgM7U8ad3EN2Lw9lsrBuqHldnS1AsCiDj+S53ZXENU9rBaW3NHefbeFPzh2a5JjaNFcMfUXNsYSZ1JdnhLpb+R0C02ICLTz45Hpn1cUjGi2mBOkSUIVy7tOtkGiV3pLhqUpBE4cyXBHNr8k2qTRVBP4m+14pQYfcvt6BXo9BYKUXx3gaf7ybTVFRLswwOkG3xtDdcgndrSivasm7E6dxed756b8ievyIMVoKsi7IpSxtwor7kjFbYPgvmav+4v4hslXemy2JJNOeLB+wAM7AaU0V+JJw4o2YNIL94fxS7xk5nFqBieg/CRI/wEbhf0VVL59UgxFc=";
};
