﻿
namespace LightCAD.ImpExpDwg
{

    public class ImpExpDwgPlugin : ILcPlugin
    {
        public void InitUI()
        {
        }

        public void Loaded()
        {
            LcDocument.RegistElementTypes(ArchElementType.All);
            LcRuntime.RegistAssemblies.Add("QdArch");
            WallAction.Initilize();
            DoorAction.Initilize();
            WindowAction.Initilize();
            SlabAction.Initilize();
            StairAction.Initilize();
            WindPipeAction.Initilize();
        }
    }
}
