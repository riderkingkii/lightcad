﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.ImpExpDwg
{
    /// <summary>
    /// 错误信息 
    /// </summary>
    public class ReaderWriteException
    {
        public static List<Exception> Exceptions { get; set; }
        public static void Add(Exception exception)
        {
            if (Exceptions == null)
            {
                Exceptions = new List<Exception>();
            }
            if (!Exceptions.Contains(exception))
            {
                Exceptions.Add(exception);
            }
        }
        public static void Init()
        {
            Exceptions = new List<Exception>();
        }
        public static void OutputMessage()
        {

            foreach (Exception exception in Exceptions)
            {
                Debug.Assert(false, exception.Message, exception.StackTrace);
            }

            ///输出后清除缓存
            Init();
        }
    }
}
