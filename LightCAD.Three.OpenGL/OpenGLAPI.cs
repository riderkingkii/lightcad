﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD.Three.OpenGL
{
    class OpenGLAPI
    {
        //[Slot(8)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginPerfMonitorAMD(UInt32 monitor);
        //[Slot(198)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeletePerfMonitorsAMD(Int32 n, [CountAttribute(Parameter = "n")] UInt32* monitors);
        //[Slot(271)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndPerfMonitorAMD(UInt32 monitor);
        //[Slot(309)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenPerfMonitorsAMD(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* monitors);
        //[Slot(486)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfMonitorCounterDataAMD(UInt32 monitor, System.Int32 pname, Int32 dataSize, [OutAttribute, CountAttribute(Parameter = "dataSize")] UInt32* data, [OutAttribute, CountAttribute(Count = 1)] Int32* bytesWritten);
        //[Slot(487)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetPerfMonitorCounterInfoAMD(UInt32 group, UInt32 counter, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] IntPtr data);
        //[Slot(488)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfMonitorCountersAMD(UInt32 group, [OutAttribute, CountAttribute(Count = 1)] Int32* numCounters, [OutAttribute, CountAttribute(Count = 1)] Int32* maxActiveCounters, Int32 counterSize, [OutAttribute, CountAttribute(Parameter = "counterSize")] UInt32* counters);
        //[Slot(489)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfMonitorCounterStringAMD(UInt32 group, UInt32 counter, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr counterString);
        //[Slot(490)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfMonitorGroupsAMD([OutAttribute, CountAttribute(Count = 1)] Int32* numGroups, Int32 groupsSize, [OutAttribute, CountAttribute(Parameter = "groupsSize")] UInt32* groups);
        //[Slot(491)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfMonitorGroupStringAMD(UInt32 group, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr groupString);
        //[Slot(1012)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSelectPerfMonitorCountersAMD(UInt32 monitor, bool enable, UInt32 group, Int32 numCounters, [OutAttribute, CountAttribute(Parameter = "numCounters")] UInt32* counterList);
        //[Slot(42)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendEquationiARB(UInt32 buf, System.Int32 mode);
        //[Slot(45)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendEquationSeparateiARB(UInt32 buf, System.Int32 modeRGB, System.Int32 modeAlpha);
        //[Slot(48)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendFunciARB(UInt32 buf, System.Int32 src, System.Int32 dst);
        //[Slot(51)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendFuncSeparateiARB(UInt32 buf, System.Int32 srcRGB, System.Int32 dstRGB, System.Int32 srcAlpha, System.Int32 dstAlpha);
        //[Slot(57)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBufferPageCommitmentARB(System.Int32 target, IntPtr offset, IntPtr size, bool commit);
        //[Slot(103)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCompileShaderIncludeARB(UInt32 shader, Int32 count, [CountAttribute(Parameter = "count")] IntPtr path, [CountAttribute(Parameter = "count")] Int32* length);
        //[Slot(179)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe IntPtr glCreateSyncFromCLeventARB([OutAttribute] IntPtr* context, [OutAttribute] IntPtr* @event, UInt32 flags);
        //[Slot(185)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDebugMessageCallbackARB(DebugProcArb callback, [CountAttribute(Computed = "callback")] IntPtr userParam);
        //[Slot(188)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDebugMessageControlARB(System.Int32 source, System.Int32 type, System.Int32 severity, Int32 count, [CountAttribute(Parameter = "count")] UInt32* ids, bool enabled);
        //[Slot(191)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDebugMessageInsertARB(System.Int32 source, System.Int32 type, UInt32 id, System.Int32 severity, Int32 length, [CountAttribute(Parameter = "length")] IntPtr buf);
        //[Slot(196)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDeleteNamedStringARB(Int32 namelen, [CountAttribute(Parameter = "namelen")] IntPtr name);
        //[Slot(229)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDispatchComputeGroupSizeARB(UInt32 num_groups_x, UInt32 num_groups_y, UInt32 num_groups_z, UInt32 group_size_x, UInt32 group_size_y, UInt32 group_size_z);
        //[Slot(234)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawArraysInstancedARB(System.Int32 mode, Int32 first, Int32 count, Int32 primcount);
        //[Slot(247)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsInstancedARB(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices, Int32 primcount);
        //[Slot(276)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEvaluateDepthValuesARB();
        //[Slot(290)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glFramebufferSampleLocationsfvARB(System.Int32 target, UInt32 start, Int32 count, Single* v);
        //[Slot(296)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTextureARB(System.Int32 target, System.Int32 attachment, UInt32 texture, Int32 level);
        //[Slot(297)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTextureFaceARB(System.Int32 target, System.Int32 attachment, UInt32 texture, Int32 level, System.Int32 face);
        //[Slot(299)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTextureLayerARB(System.Int32 target, System.Int32 attachment, UInt32 texture, Int32 level, Int32 layer);
        //[Slot(352)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe Int32 glGetDebugMessageLogARB(UInt32 count, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* sources, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* types, [OutAttribute, CountAttribute(Parameter = "count")] UInt32* ids, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* severities, [OutAttribute, CountAttribute(Parameter = "count")] Int32* lengths, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr messageLog);
        //[Slot(370)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glGetGraphicsResetStatusARB();
        //[Slot(375)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int64 glGetImageHandleARB(UInt32 texture, Int32 level, bool layered, Int32 layer, System.Int32 format);
        //[Slot(423)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedStringARB(Int32 namelen, [CountAttribute(Parameter = "namelen")] IntPtr name, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* stringlen, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr @string);
        //[Slot(424)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedStringivARB(Int32 namelen, [CountAttribute(Parameter = "namelen")] IntPtr name, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(426)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnColorTableARB(System.Int32 target, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr table);
        //[Slot(428)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnCompressedTexImageARB(System.Int32 target, Int32 lod, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr img);
        //[Slot(430)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnConvolutionFilterARB(System.Int32 target, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr image);
        //[Slot(433)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnHistogramARB(System.Int32 target, bool reset, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr values);
        //[Slot(435)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnMapdvARB(System.Int32 target, System.Int32 query, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Double* v);
        //[Slot(437)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnMapfvARB(System.Int32 target, System.Int32 query, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Single* v);
        //[Slot(439)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnMapivARB(System.Int32 target, System.Int32 query, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Int32* v);
        //[Slot(441)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnMinmaxARB(System.Int32 target, bool reset, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr values);
        //[Slot(443)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPixelMapfvARB(System.Int32 map, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Single* values);
        //[Slot(445)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPixelMapuivARB(System.Int32 map, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] UInt32* values);
        //[Slot(447)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPixelMapusvARB(System.Int32 map, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] UInt16* values);
        //[Slot(449)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPolygonStippleARB(Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Byte* pattern);
        //[Slot(451)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnSeparableFilterARB(System.Int32 target, System.Int32 format, System.Int32 type, Int32 rowBufSize, [OutAttribute, CountAttribute(Parameter = "rowBufSize")] IntPtr row, Int32 columnBufSize, [OutAttribute, CountAttribute(Parameter = "columnBufSize")] IntPtr column, [OutAttribute, CountAttribute(Count = 0)] IntPtr span);
        //[Slot(453)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnTexImageARB(System.Int32 target, Int32 level, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr img);
        //[Slot(455)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformdvARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Double* @params);
        //[Slot(457)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformfvARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Single* @params);
        //[Slot(459)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformi64vARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Int64* @params);
        //[Slot(461)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformivARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Int32* @params);
        //[Slot(463)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformui64vARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] UInt64* @params);
        //[Slot(465)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformuivARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] UInt32* @params);
        //[Slot(547)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int64 glGetTextureHandleARB(UInt32 texture);
        //[Slot(563)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int64 glGetTextureSamplerHandleARB(UInt32 texture, UInt32 sampler);
        //[Slot(573)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformi64vARB(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] Int64* @params);
        //[Slot(579)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformui64vARB(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] UInt64* @params);
        //[Slot(596)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribLui64vARB(UInt32 index, System.Int32 pname, [OutAttribute] UInt64* @params);
        //[Slot(620)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsImageHandleResidentARB(UInt64 handle);
        //[Slot(623)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsNamedStringARB(Int32 namelen, [CountAttribute(Parameter = "namelen")] IntPtr name);
        //[Slot(637)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsTextureHandleResidentARB(UInt64 handle);
        //[Slot(648)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeImageHandleNonResidentARB(UInt64 handle);
        //[Slot(650)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeImageHandleResidentARB(UInt64 handle, System.Int32 access);
        //[Slot(654)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeTextureHandleNonResidentARB(UInt64 handle);
        //[Slot(656)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeTextureHandleResidentARB(UInt64 handle);
        //[Slot(689)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMaxShaderCompilerThreadsARB(UInt32 count);
        //[Slot(695)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMinSampleShadingARB(Single value);
        //[Slot(701)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawArraysIndirectCountARB(System.Int32 mode, IntPtr indirect, IntPtr drawcount, Int32 maxdrawcount, Int32 stride);
        //[Slot(708)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawElementsIndirectCountARB(System.Int32 mode, System.Int32 type, IntPtr indirect, IntPtr drawcount, Int32 maxdrawcount, Int32 stride);
        //[Slot(744)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferPageCommitmentARB(UInt32 buffer, IntPtr offset, IntPtr size, bool commit);
        //[Slot(758)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedFramebufferSampleLocationsfvARB(UInt32 framebuffer, UInt32 start, Int32 count, Single* v);
        //[Slot(785)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedStringARB(System.Int32 type, Int32 namelen, [CountAttribute(Parameter = "namelen")] IntPtr name, Int32 stringlen, [CountAttribute(Parameter = "stringlen")] IntPtr @string);
        //[Slot(832)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPrimitiveBoundingBoxARB(Single minX, Single minY, Single minZ, Single minW, Single maxX, Single maxY, Single maxZ, Single maxW);
        //[Slot(836)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramParameteriARB(UInt32 program, System.Int32 pname, Int32 value);
        //[Slot(848)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1i64ARB(UInt32 program, Int32 location, Int64 x);
        //[Slot(850)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1i64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Int64* value);
        //[Slot(856)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1ui64ARB(UInt32 program, Int32 location, UInt64 x);
        //[Slot(858)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1ui64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt64* value);
        //[Slot(872)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2i64ARB(UInt32 program, Int32 location, Int64 x, Int64 y);
        //[Slot(874)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2i64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Int64* value);
        //[Slot(880)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2ui64ARB(UInt32 program, Int32 location, UInt64 x, UInt64 y);
        //[Slot(882)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2ui64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] UInt64* value);
        //[Slot(896)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3i64ARB(UInt32 program, Int32 location, Int64 x, Int64 y, Int64 z);
        //[Slot(898)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3i64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Int64* value);
        //[Slot(904)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3ui64ARB(UInt32 program, Int32 location, UInt64 x, UInt64 y, UInt64 z);
        //[Slot(906)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3ui64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] UInt64* value);
        //[Slot(920)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4i64ARB(UInt32 program, Int32 location, Int64 x, Int64 y, Int64 z, Int64 w);
        //[Slot(922)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4i64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Int64* value);
        //[Slot(928)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4ui64ARB(UInt32 program, Int32 location, UInt64 x, UInt64 y, UInt64 z, UInt64 w);
        //[Slot(930)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4ui64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] UInt64* value);
        //[Slot(935)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniformHandleui64ARB(UInt32 program, Int32 location, UInt64 value);
        //[Slot(937)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformHandleui64vARB(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt64* values);
        //[Slot(986)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glReadnPixelsARB(Int32 x, Int32 y, Int32 width, Int32 height, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr data);
        //[Slot(1020)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSpecializeShaderARB(UInt32 shader, IntPtr pEntryPoint, UInt32 numSpecializationConstants, UInt32* pConstantIndex, UInt32* pConstantValue);
        //[Slot(1038)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexBufferARB(System.Int32 target, System.Int32 internalformat, UInt32 buffer);
        //[Slot(1054)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexPageCommitmentARB(System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, bool commit);
        //[Slot(1118)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1i64ARB(Int32 location, Int64 x);
        //[Slot(1120)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1i64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] Int64* value);
        //[Slot(1124)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1ui64ARB(Int32 location, UInt64 x);
        //[Slot(1126)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1ui64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] UInt64* value);
        //[Slot(1134)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2i64ARB(Int32 location, Int64 x, Int64 y);
        //[Slot(1136)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2i64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Int64* value);
        //[Slot(1140)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2ui64ARB(Int32 location, UInt64 x, UInt64 y);
        //[Slot(1142)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2ui64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] UInt64* value);
        //[Slot(1150)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3i64ARB(Int32 location, Int64 x, Int64 y, Int64 z);
        //[Slot(1152)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3i64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Int64* value);
        //[Slot(1156)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3ui64ARB(Int32 location, UInt64 x, UInt64 y, UInt64 z);
        //[Slot(1158)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3ui64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] UInt64* value);
        //[Slot(1166)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4i64ARB(Int32 location, Int64 x, Int64 y, Int64 z, Int64 w);
        //[Slot(1168)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4i64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Int64* value);
        //[Slot(1172)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4ui64ARB(Int32 location, UInt64 x, UInt64 y, UInt64 z, UInt64 w);
        //[Slot(1174)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4ui64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] UInt64* value);
        //[Slot(1178)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniformHandleui64ARB(Int32 location, UInt64 value);
        //[Slot(1180)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformHandleui64vARB(Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt64* value);
        //[Slot(1278)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribDivisorARB(UInt32 index, UInt32 divisor);
        //[Slot(1308)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL1ui64ARB(UInt32 index, UInt64 x);
        //[Slot(1310)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL1ui64vARB(UInt32 index, UInt64* v);
        //[Slot(4)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glApplyFramebufferAttachmentCMAAINTEL();
        //[Slot(1)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glActiveShaderProgram(UInt32 pipeline, UInt32 program);
        //[Slot(3)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glActiveTexture(System.Int32 texture);
        //[Slot(5)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glAttachShader(UInt32 program, UInt32 shader);
        //[Slot(6)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginConditionalRender(UInt32 id, System.Int32 mode);
        //[Slot(10)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginQuery(System.Int32 target, UInt32 id);
        //[Slot(11)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginQueryIndexed(System.Int32 target, UInt32 index, UInt32 id);
        //[Slot(12)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginTransformFeedback(System.Int32 primitiveMode);
        //[Slot(13)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindAttribLocation(UInt32 program, UInt32 index, IntPtr name);
        //[Slot(14)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindBuffer(System.Int32 target, UInt32 buffer);
        //[Slot(15)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindBufferBase(System.Int32 target, UInt32 index, UInt32 buffer);
        //[Slot(16)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindBufferRange(System.Int32 target, UInt32 index, UInt32 buffer, IntPtr offset, IntPtr size);
        //[Slot(17)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glBindBuffersBase(System.Int32 target, UInt32 first, Int32 count, [CountAttribute(Parameter = "count")] UInt32* buffers);
        //[Slot(18)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glBindBuffersRange(System.Int32 target, UInt32 first, Int32 count, [CountAttribute(Parameter = "count")] UInt32* buffers, [CountAttribute(Parameter = "count")] IntPtr* offsets, [CountAttribute(Parameter = "count")] IntPtr* sizes);
        //[Slot(19)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindFragDataLocation(UInt32 program, UInt32 color, [CountAttribute(Computed = "name")] IntPtr name);
        //[Slot(20)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindFragDataLocationIndexed(UInt32 program, UInt32 colorNumber, UInt32 index, IntPtr name);
        //[Slot(21)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindFramebuffer(System.Int32 target, UInt32 framebuffer);
        //[Slot(22)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindImageTexture(UInt32 unit, UInt32 texture, Int32 level, bool layered, Int32 layer, System.Int32 access, System.Int32 format);
        //[Slot(23)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glBindImageTextures(UInt32 first, Int32 count, [CountAttribute(Parameter = "count")] UInt32* textures);
        //[Slot(25)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindProgramPipeline(UInt32 pipeline);
        //[Slot(27)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindRenderbuffer(System.Int32 target, UInt32 renderbuffer);
        //[Slot(28)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindSampler(UInt32 unit, UInt32 sampler);
        //[Slot(29)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glBindSamplers(UInt32 first, Int32 count, [CountAttribute(Parameter = "count")] UInt32* samplers);
        //[Slot(30)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindTexture(System.Int32 target, UInt32 texture);
        //[Slot(31)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glBindTextures(UInt32 first, Int32 count, [CountAttribute(Parameter = "count")] UInt32* textures);
        //[Slot(32)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindTextureUnit(UInt32 unit, UInt32 texture);
        //[Slot(33)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindTransformFeedback(System.Int32 target, UInt32 id);
        //[Slot(34)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindVertexArray(UInt32 array);
        //[Slot(35)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindVertexBuffer(UInt32 bindingindex, UInt32 buffer, IntPtr offset, Int32 stride);
        //[Slot(36)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glBindVertexBuffers(UInt32 first, Int32 count, [CountAttribute(Parameter = "count")] UInt32* buffers, [CountAttribute(Parameter = "count")] IntPtr* offsets, [CountAttribute(Parameter = "count")] Int32* strides);
        //[Slot(39)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendColor(Single red, Single green, Single blue, Single alpha);
        //[Slot(40)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendEquation(System.Int32 mode);
        //[Slot(41)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendEquationi(UInt32 buf, System.Int32 mode);
        //[Slot(43)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendEquationSeparate(System.Int32 modeRGB, System.Int32 modeAlpha);
        //[Slot(44)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendEquationSeparatei(UInt32 buf, System.Int32 modeRGB, System.Int32 modeAlpha);
        //[Slot(46)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendFunc(System.Int32 sfactor, System.Int32 dfactor);
        //[Slot(47)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendFunci(UInt32 buf, System.Int32 src, System.Int32 dst);
        //[Slot(49)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendFuncSeparate(System.Int32 sfactorRGB, System.Int32 dfactorRGB, System.Int32 sfactorAlpha, System.Int32 dfactorAlpha);
        //[Slot(50)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendFuncSeparatei(UInt32 buf, System.Int32 srcRGB, System.Int32 dstRGB, System.Int32 srcAlpha, System.Int32 dstAlpha);
        //[Slot(53)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlitFramebuffer(Int32 srcX0, Int32 srcY0, Int32 srcX1, Int32 srcY1, Int32 dstX0, Int32 dstY0, Int32 dstX1, Int32 dstY1, System.Int32 mask, System.Int32 filter);
        //[Slot(54)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlitNamedFramebuffer(UInt32 readFramebuffer, UInt32 drawFramebuffer, Int32 srcX0, Int32 srcY0, Int32 srcX1, Int32 srcY1, Int32 dstX0, Int32 dstY0, Int32 dstX1, Int32 dstY1, System.Int32 mask, System.Int32 filter);
        //[Slot(56)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBufferData(System.Int32 target, IntPtr size, [CountAttribute(Parameter = "size")] IntPtr data, System.Int32 usage);
        //[Slot(58)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBufferStorage(System.Int32 target, IntPtr size, [CountAttribute(Parameter = "size")] IntPtr data, System.Int32 flags);
        //[Slot(59)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBufferSubData(System.Int32 target, IntPtr offset, IntPtr size, [CountAttribute(Parameter = "size")] IntPtr data);
        //[Slot(61)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glCheckFramebufferStatus(System.Int32 target);
        //[Slot(62)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glCheckNamedFramebufferStatus(UInt32 framebuffer, System.Int32 target);
        //[Slot(64)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClampColor(System.Int32 target, System.Int32 clamp);
        //[Slot(65)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClear(System.Int32 mask);
        //[Slot(66)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearBufferData(System.Int32 target, System.Int32 internalformat, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type")] IntPtr data);
        //[Slot(67)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearBufferfi(System.Int32 buffer, Int32 drawbuffer, Single depth, Int32 stencil);
        //[Slot(68)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glClearBufferfv(System.Int32 buffer, Int32 drawbuffer, [CountAttribute(Computed = "buffer")] Single* value);
        //[Slot(69)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glClearBufferiv(System.Int32 buffer, Int32 drawbuffer, [CountAttribute(Computed = "buffer")] Int32* value);
        //[Slot(70)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearBufferSubData(System.Int32 target, System.Int32 internalformat, IntPtr offset, IntPtr size, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type")] IntPtr data);
        //[Slot(71)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glClearBufferuiv(System.Int32 buffer, Int32 drawbuffer, [CountAttribute(Computed = "buffer")] UInt32* value);
        //[Slot(72)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearColor(Single red, Single green, Single blue, Single alpha);
        //[Slot(73)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearDepth(Double depth);
        //[Slot(74)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearDepthf(Single d);
        //[Slot(75)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearNamedBufferData(UInt32 buffer, System.Int32 internalformat, System.Int32 format, System.Int32 type, IntPtr data);
        //[Slot(77)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearNamedBufferSubData(UInt32 buffer, System.Int32 internalformat, IntPtr offset, IntPtr size, System.Int32 format, System.Int32 type, IntPtr data);
        //[Slot(79)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearNamedFramebufferfi(UInt32 framebuffer, System.Int32 buffer, Int32 drawbuffer, Single depth, Int32 stencil);
        //[Slot(80)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glClearNamedFramebufferfv(UInt32 framebuffer, System.Int32 buffer, Int32 drawbuffer, Single* value);
        //[Slot(81)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glClearNamedFramebufferiv(UInt32 framebuffer, System.Int32 buffer, Int32 drawbuffer, Int32* value);
        //[Slot(82)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glClearNamedFramebufferuiv(UInt32 framebuffer, System.Int32 buffer, Int32 drawbuffer, UInt32* value);
        //[Slot(83)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearStencil(Int32 s);
        //[Slot(84)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearTexImage(UInt32 texture, Int32 level, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type")] IntPtr data);
        //[Slot(85)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearTexSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type")] IntPtr data);
        //[Slot(87)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glClientWaitSync(IntPtr sync, System.Int32 flags, UInt64 timeout);
        //[Slot(88)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClipControl(System.Int32 origin, System.Int32 depth);
        //[Slot(90)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glColorMask(bool red, bool green, bool blue, bool alpha);
        //[Slot(91)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glColorMaski(UInt32 index, bool r, bool g, bool b, bool a);
        //[Slot(92)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glColorP3ui(System.Int32 type, UInt32 color);
        //[Slot(93)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glColorP3uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* color);
        //[Slot(94)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glColorP4ui(System.Int32 type, UInt32 color);
        //[Slot(95)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glColorP4uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* color);
        //[Slot(96)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glColorSubTable(System.Int32 target, Int32 start, Int32 count, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,count")] IntPtr data);
        //[Slot(97)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glColorTable(System.Int32 target, System.Int32 internalformat, Int32 width, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr table);
        //[Slot(98)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glColorTableParameterfv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(99)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glColorTableParameteriv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(102)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompileShader(UInt32 shader);
        //[Slot(110)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTexImage1D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data);
        //[Slot(111)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTexImage2D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data);
        //[Slot(112)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTexImage3D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data);
        //[Slot(113)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTexSubImage1D(System.Int32 target, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data);
        //[Slot(114)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTexSubImage2D(System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data);
        //[Slot(115)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTexSubImage3D(System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data);
        //[Slot(119)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureSubImage1D(UInt32 texture, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, Int32 imageSize, IntPtr data);
        //[Slot(121)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureSubImage2D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, Int32 imageSize, IntPtr data);
        //[Slot(123)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureSubImage3D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, Int32 imageSize, IntPtr data);
        //[Slot(127)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glConvolutionFilter1D(System.Int32 target, System.Int32 internalformat, Int32 width, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr image);
        //[Slot(128)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glConvolutionFilter2D(System.Int32 target, System.Int32 internalformat, Int32 width, Int32 height, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height")] IntPtr image);
        //[Slot(129)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glConvolutionParameterf(System.Int32 target, System.Int32 pname, Single @params);
        //[Slot(130)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glConvolutionParameterfv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(131)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glConvolutionParameteri(System.Int32 target, System.Int32 pname, Int32 @params);
        //[Slot(132)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glConvolutionParameteriv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(133)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyBufferSubData(System.Int32 readTarget, System.Int32 writeTarget, IntPtr readOffset, IntPtr writeOffset, IntPtr size);
        //[Slot(134)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyColorSubTable(System.Int32 target, Int32 start, Int32 x, Int32 y, Int32 width);
        //[Slot(135)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyColorTable(System.Int32 target, System.Int32 internalformat, Int32 x, Int32 y, Int32 width);
        //[Slot(136)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyConvolutionFilter1D(System.Int32 target, System.Int32 internalformat, Int32 x, Int32 y, Int32 width);
        //[Slot(137)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyConvolutionFilter2D(System.Int32 target, System.Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(138)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyImageSubData(UInt32 srcName, System.Int32 srcTarget, Int32 srcLevel, Int32 srcX, Int32 srcY, Int32 srcZ, UInt32 dstName, System.Int32 dstTarget, Int32 dstLevel, Int32 dstX, Int32 dstY, Int32 dstZ, Int32 srcWidth, Int32 srcHeight, Int32 srcDepth);
        //[Slot(144)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyNamedBufferSubData(UInt32 readBuffer, UInt32 writeBuffer, IntPtr readOffset, IntPtr writeOffset, IntPtr size);
        //[Slot(146)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTexImage1D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 border);
        //[Slot(147)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTexImage2D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border);
        //[Slot(148)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTexSubImage1D(System.Int32 target, Int32 level, Int32 xoffset, Int32 x, Int32 y, Int32 width);
        //[Slot(149)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTexSubImage2D(System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(150)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTexSubImage3D(System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(153)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureSubImage1D(UInt32 texture, Int32 level, Int32 xoffset, Int32 x, Int32 y, Int32 width);
        //[Slot(155)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureSubImage2D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(157)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureSubImage3D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(165)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateBuffers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* buffers);
        //[Slot(167)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateFramebuffers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* framebuffers);
        //[Slot(169)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glCreateProgram();
        //[Slot(170)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateProgramPipelines(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* pipelines);
        //[Slot(171)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateQueries(System.Int32 target, Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* ids);
        //[Slot(172)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateRenderbuffers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* renderbuffers);
        //[Slot(173)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateSamplers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* samplers);
        //[Slot(174)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glCreateShader(System.Int32 type);
        //[Slot(176)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glCreateShaderProgramv(System.Int32 type, Int32 count, [CountAttribute(Parameter = "count")] IntPtr strings);
        //[Slot(180)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateTextures(System.Int32 target, Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* textures);
        //[Slot(181)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateTransformFeedbacks(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* ids);
        //[Slot(182)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateVertexArrays(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* arrays);
        //[Slot(183)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCullFace(System.Int32 mode);
        //[Slot(184)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDebugMessageCallback(DebugProc callback, IntPtr userParam);
        //[Slot(187)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDebugMessageControl(System.Int32 source, System.Int32 type, System.Int32 severity, Int32 count, [CountAttribute(Parameter = "count")] UInt32* ids, bool enabled);
        //[Slot(190)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDebugMessageInsert(System.Int32 source, System.Int32 type, UInt32 id, System.Int32 severity, Int32 length, [CountAttribute(Computed = "buf,length")] IntPtr buf);
        //[Slot(193)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteBuffers(Int32 n, [CountAttribute(Parameter = "n")] UInt32* buffers);
        //[Slot(195)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteFramebuffers(Int32 n, [CountAttribute(Parameter = "n")] UInt32* framebuffers);
        //[Slot(200)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDeleteProgram(UInt32 program);
        //[Slot(201)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteProgramPipelines(Int32 n, [CountAttribute(Parameter = "n")] UInt32* pipelines);
        //[Slot(203)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteQueries(Int32 n, [CountAttribute(Parameter = "n")] UInt32* ids);
        //[Slot(204)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteRenderbuffers(Int32 n, [CountAttribute(Parameter = "n")] UInt32* renderbuffers);
        //[Slot(205)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteSamplers(Int32 count, [CountAttribute(Parameter = "count")] UInt32* samplers);
        //[Slot(206)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDeleteShader(UInt32 shader);
        //[Slot(208)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDeleteSync(IntPtr sync);
        //[Slot(209)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteTextures(Int32 n, [CountAttribute(Parameter = "n")] UInt32* textures);
        //[Slot(210)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteTransformFeedbacks(Int32 n, [CountAttribute(Parameter = "n")] UInt32* ids);
        //[Slot(211)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteVertexArrays(Int32 n, [CountAttribute(Parameter = "n")] UInt32* arrays);
        //[Slot(212)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDepthFunc(System.Int32 func);
        //[Slot(213)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDepthMask(bool flag);
        //[Slot(214)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDepthRange(Double near, Double far);
        //[Slot(215)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDepthRangeArrayv(UInt32 first, Int32 count, [CountAttribute(Computed = "count")] Double* v);
        //[Slot(216)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDepthRangef(Single n, Single f);
        //[Slot(217)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDepthRangeIndexed(UInt32 index, Double n, Double f);
        //[Slot(218)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDetachShader(UInt32 program, UInt32 shader);
        //[Slot(219)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisable(System.Int32 cap);
        //[Slot(222)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisablei(System.Int32 target, UInt32 index);
        //[Slot(224)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisableVertexArrayAttrib(UInt32 vaobj, UInt32 index);
        //[Slot(227)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisableVertexAttribArray(UInt32 index);
        //[Slot(228)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDispatchCompute(UInt32 num_groups_x, UInt32 num_groups_y, UInt32 num_groups_z);
        //[Slot(230)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDispatchComputeIndirect(IntPtr indirect);
        //[Slot(231)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawArrays(System.Int32 mode, Int32 first, Int32 count);
        //[Slot(232)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawArraysIndirect(System.Int32 mode, IntPtr indirect);
        //[Slot(233)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawArraysInstanced(System.Int32 mode, Int32 first, Int32 count, Int32 instancecount);
        //[Slot(235)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawArraysInstancedBaseInstance(System.Int32 mode, Int32 first, Int32 count, Int32 instancecount, UInt32 baseinstance);
        //[Slot(237)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawBuffer(System.Int32 buf);
        //[Slot(238)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDrawBuffers(Int32 n, [CountAttribute(Parameter = "n")] System.Int32* bufs);
        //[Slot(243)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElements(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices);
        //[Slot(244)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsBaseVertex(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices, Int32 basevertex);
        //[Slot(245)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsIndirect(System.Int32 mode, System.Int32 type, IntPtr indirect);
        //[Slot(246)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsInstanced(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices, Int32 instancecount);
        //[Slot(248)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsInstancedBaseInstance(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Parameter = "count")] IntPtr indices, Int32 instancecount, UInt32 baseinstance);
        //[Slot(249)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsInstancedBaseVertex(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices, Int32 instancecount, Int32 basevertex);
        //[Slot(250)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsInstancedBaseVertexBaseInstance(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Parameter = "count")] IntPtr indices, Int32 instancecount, Int32 basevertex, UInt32 baseinstance);
        //[Slot(252)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawRangeElements(System.Int32 mode, UInt32 start, UInt32 end, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices);
        //[Slot(253)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawRangeElementsBaseVertex(System.Int32 mode, UInt32 start, UInt32 end, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices, Int32 basevertex);
        //[Slot(254)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawTransformFeedback(System.Int32 mode, UInt32 id);
        //[Slot(255)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawTransformFeedbackInstanced(System.Int32 mode, UInt32 id, Int32 instancecount);
        //[Slot(256)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawTransformFeedbackStream(System.Int32 mode, UInt32 id, UInt32 stream);
        //[Slot(257)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawTransformFeedbackStreamInstanced(System.Int32 mode, UInt32 id, UInt32 stream, Int32 instancecount);
        //[Slot(260)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnable(System.Int32 cap);
        //[Slot(263)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnablei(System.Int32 target, UInt32 index);
        //[Slot(265)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnableVertexArrayAttrib(UInt32 vaobj, UInt32 index);
        //[Slot(268)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnableVertexAttribArray(UInt32 index);
        //[Slot(269)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndConditionalRender();
        //[Slot(273)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndQuery(System.Int32 target);
        //[Slot(274)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndQueryIndexed(System.Int32 target, UInt32 index);
        //[Slot(275)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndTransformFeedback();
        //[Slot(277)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glFenceSync(System.Int32 condition, System.Int32 flags);
        //[Slot(278)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFinish();
        //[Slot(279)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFlush();
        //[Slot(280)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFlushMappedBufferRange(System.Int32 target, IntPtr offset, IntPtr length);
        //[Slot(281)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFlushMappedNamedBufferRange(UInt32 buffer, IntPtr offset, IntPtr length);
        //[Slot(287)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferParameteri(System.Int32 target, System.Int32 pname, Int32 param);
        //[Slot(289)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferRenderbuffer(System.Int32 target, System.Int32 attachment, System.Int32 renderbuffertarget, UInt32 renderbuffer);
        //[Slot(292)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTexture(System.Int32 target, System.Int32 attachment, UInt32 texture, Int32 level);
        //[Slot(293)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTexture1D(System.Int32 target, System.Int32 attachment, System.Int32 textarget, UInt32 texture, Int32 level);
        //[Slot(294)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTexture2D(System.Int32 target, System.Int32 attachment, System.Int32 textarget, UInt32 texture, Int32 level);
        //[Slot(295)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTexture3D(System.Int32 target, System.Int32 attachment, System.Int32 textarget, UInt32 texture, Int32 level, Int32 zoffset);
        //[Slot(298)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTextureLayer(System.Int32 target, System.Int32 attachment, UInt32 texture, Int32 level, Int32 layer);
        //[Slot(301)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFrontFace(System.Int32 mode);
        //[Slot(302)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenBuffers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* buffers);
        //[Slot(303)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGenerateMipmap(System.Int32 target);
        //[Slot(305)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGenerateTextureMipmap(UInt32 texture);
        //[Slot(307)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenFramebuffers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* framebuffers);
        //[Slot(310)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenProgramPipelines(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* pipelines);
        //[Slot(312)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenQueries(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* ids);
        //[Slot(313)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenRenderbuffers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* renderbuffers);
        //[Slot(314)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenSamplers(Int32 count, [OutAttribute, CountAttribute(Parameter = "count")] UInt32* samplers);
        //[Slot(315)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenTextures(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* textures);
        //[Slot(316)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenTransformFeedbacks(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* ids);
        //[Slot(317)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenVertexArrays(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* arrays);
        //[Slot(318)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveAtomicCounterBufferiv(UInt32 program, UInt32 bufferIndex, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(319)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveAttrib(UInt32 program, UInt32 index, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Count = 1)] Int32* size, [OutAttribute, CountAttribute(Count = 1)] System.Int32* type, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr name);
        //[Slot(320)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveSubroutineName(UInt32 program, System.Int32 shadertype, UInt32 index, Int32 bufsize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufsize")] IntPtr name);
        //[Slot(321)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveSubroutineUniformiv(UInt32 program, System.Int32 shadertype, UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* values);
        //[Slot(322)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveSubroutineUniformName(UInt32 program, System.Int32 shadertype, UInt32 index, Int32 bufsize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufsize")] IntPtr name);
        //[Slot(323)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveUniform(UInt32 program, UInt32 index, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Count = 1)] Int32* size, [OutAttribute, CountAttribute(Count = 1)] System.Int32* type, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr name);
        //[Slot(324)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveUniformBlockiv(UInt32 program, UInt32 uniformBlockIndex, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "program,uniformBlockIndex,pname")] Int32* @params);
        //[Slot(325)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveUniformBlockName(UInt32 program, UInt32 uniformBlockIndex, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr uniformBlockName);
        //[Slot(326)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveUniformName(UInt32 program, UInt32 uniformIndex, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr uniformName);
        //[Slot(327)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetActiveUniformsiv(UInt32 program, Int32 uniformCount, [CountAttribute(Parameter = "uniformCount")] UInt32* uniformIndices, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "uniformCount,pname")] Int32* @params);
        //[Slot(328)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetAttachedShaders(UInt32 program, Int32 maxCount, [OutAttribute, CountAttribute(Count = 1)] Int32* count, [OutAttribute, CountAttribute(Parameter = "maxCount")] UInt32* shaders);
        //[Slot(329)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetAttribLocation(UInt32 program, IntPtr name);
        //[Slot(330)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetBooleani_v(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] bool* data);
        //[Slot(332)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetBooleanv(System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] bool* data);
        //[Slot(333)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetBufferParameteri64v(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int64* @params);
        //[Slot(334)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetBufferParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(336)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetBufferPointerv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] IntPtr @params);
        //[Slot(337)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetBufferSubData(System.Int32 target, IntPtr offset, IntPtr size, [OutAttribute, CountAttribute(Parameter = "size")] IntPtr data);
        //[Slot(338)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetColorTable(System.Int32 target, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,format,type")] IntPtr table);
        //[Slot(339)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetColorTableParameterfv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(340)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetColorTableParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(343)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetCompressedTexImage(System.Int32 target, Int32 level, [OutAttribute, CountAttribute(Computed = "target,level")] IntPtr img);
        //[Slot(344)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetCompressedTextureImage(UInt32 texture, Int32 level, Int32 bufSize, [OutAttribute] IntPtr pixels);
        //[Slot(346)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetCompressedTextureSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 bufSize, [OutAttribute] IntPtr pixels);
        //[Slot(347)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetConvolutionFilter(System.Int32 target, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,format,type")] IntPtr image);
        //[Slot(348)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetConvolutionParameterfv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(349)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetConvolutionParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(351)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe Int32 glGetDebugMessageLog(UInt32 count, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* sources, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* types, [OutAttribute, CountAttribute(Parameter = "count")] UInt32* ids, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* severities, [OutAttribute, CountAttribute(Parameter = "count")] Int32* lengths, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr messageLog);
        //[Slot(354)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetDoublei_v(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] Double* data);
        //[Slot(357)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetDoublev(System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Double* data);
        //[Slot(358)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glGetError();
        //[Slot(360)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFloati_v(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] Single* data);
        //[Slot(363)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFloatv(System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* data);
        //[Slot(364)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetFragDataIndex(UInt32 program, IntPtr name);
        //[Slot(365)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetFragDataLocation(UInt32 program, [CountAttribute(Computed = "name")] IntPtr name);
        //[Slot(366)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFramebufferAttachmentParameteriv(System.Int32 target, System.Int32 attachment, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(367)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFramebufferParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(369)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glGetGraphicsResetStatus();
        //[Slot(372)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetHistogram(System.Int32 target, bool reset, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,format,type")] IntPtr values);
        //[Slot(373)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetHistogramParameterfv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(374)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetHistogramParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(377)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetInteger64i_v(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] Int64* data);
        //[Slot(378)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetInteger64v(System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int64* data);
        //[Slot(379)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetIntegeri_v(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] Int32* data);
        //[Slot(383)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetIntegerv(System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* data);
        //[Slot(384)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetInternalformati64v(System.Int32 target, System.Int32 internalformat, System.Int32 pname, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Int64* @params);
        //[Slot(385)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetInternalformativ(System.Int32 target, System.Int32 internalformat, System.Int32 pname, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Int32* @params);
        //[Slot(387)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetMinmax(System.Int32 target, bool reset, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,format,type")] IntPtr values);
        //[Slot(388)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMinmaxParameterfv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(389)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMinmaxParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(390)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultisamplefv(System.Int32 pname, UInt32 index, [OutAttribute, CountAttribute(Computed = "pname")] Single* val);
        //[Slot(403)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedBufferParameteri64v(UInt32 buffer, System.Int32 pname, [OutAttribute] Int64* @params);
        //[Slot(404)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedBufferParameteriv(UInt32 buffer, System.Int32 pname, [OutAttribute] Int32* @params);
        //[Slot(407)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetNamedBufferPointerv(UInt32 buffer, System.Int32 pname, [OutAttribute] IntPtr @params);
        //[Slot(409)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetNamedBufferSubData(UInt32 buffer, IntPtr offset, IntPtr size, [OutAttribute] IntPtr data);
        //[Slot(411)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedFramebufferAttachmentParameteriv(UInt32 framebuffer, System.Int32 attachment, System.Int32 pname, [OutAttribute] Int32* @params);
        //[Slot(413)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedFramebufferParameteriv(UInt32 framebuffer, System.Int32 pname, [OutAttribute] Int32* param);
        //[Slot(421)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedRenderbufferParameteriv(UInt32 renderbuffer, System.Int32 pname, [OutAttribute] Int32* @params);
        //[Slot(425)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnColorTable(System.Int32 target, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr table);
        //[Slot(427)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnCompressedTexImage(System.Int32 target, Int32 lod, Int32 bufSize, [OutAttribute] IntPtr pixels);
        //[Slot(429)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnConvolutionFilter(System.Int32 target, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr image);
        //[Slot(432)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnHistogram(System.Int32 target, bool reset, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr values);
        //[Slot(434)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnMapdv(System.Int32 target, System.Int32 query, Int32 bufSize, [OutAttribute] Double* v);
        //[Slot(436)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnMapfv(System.Int32 target, System.Int32 query, Int32 bufSize, [OutAttribute] Single* v);
        //[Slot(438)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnMapiv(System.Int32 target, System.Int32 query, Int32 bufSize, [OutAttribute] Int32* v);
        //[Slot(440)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnMinmax(System.Int32 target, bool reset, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr values);
        //[Slot(442)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPixelMapfv(System.Int32 map, Int32 bufSize, [OutAttribute] Single* values);
        //[Slot(444)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPixelMapuiv(System.Int32 map, Int32 bufSize, [OutAttribute] UInt32* values);
        //[Slot(446)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPixelMapusv(System.Int32 map, Int32 bufSize, [OutAttribute] UInt16* values);
        //[Slot(448)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnPolygonStipple(Int32 bufSize, [OutAttribute] Byte* pattern);
        //[Slot(450)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnSeparableFilter(System.Int32 target, System.Int32 format, System.Int32 type, Int32 rowBufSize, [OutAttribute] IntPtr row, Int32 columnBufSize, [OutAttribute] IntPtr column, [OutAttribute] IntPtr span);
        //[Slot(452)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetnTexImage(System.Int32 target, Int32 level, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr pixels);
        //[Slot(454)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformdv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Double* @params);
        //[Slot(456)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformfv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Single* @params);
        //[Slot(460)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformiv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Int32* @params);
        //[Slot(464)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformuiv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] UInt32* @params);
        //[Slot(467)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetObjectLabel(System.Int32 identifier, UInt32 name, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr label);
        //[Slot(470)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetObjectPtrLabel(IntPtr ptr, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr label);
        //[Slot(497)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetPointerv(System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] IntPtr @params);
        //[Slot(499)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramBinary(UInt32 program, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Count = 1)] System.Int32* binaryFormat, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr binary);
        //[Slot(500)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramInfoLog(UInt32 program, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr infoLog);
        //[Slot(501)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramInterfaceiv(UInt32 program, System.Int32 programInterface, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(502)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramiv(UInt32 program, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(503)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramPipelineInfoLog(UInt32 pipeline, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr infoLog);
        //[Slot(505)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramPipelineiv(UInt32 pipeline, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(508)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetProgramResourceIndex(UInt32 program, System.Int32 programInterface, [CountAttribute(Computed = "name")] IntPtr name);
        //[Slot(509)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramResourceiv(UInt32 program, System.Int32 programInterface, UInt32 index, Int32 propCount, [CountAttribute(Parameter = "propCount")] System.Int32* props, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] Int32* @params);
        //[Slot(510)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetProgramResourceLocation(UInt32 program, System.Int32 programInterface, [CountAttribute(Computed = "name")] IntPtr name);
        //[Slot(511)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetProgramResourceLocationIndex(UInt32 program, System.Int32 programInterface, [CountAttribute(Computed = "name")] IntPtr name);
        //[Slot(512)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramResourceName(UInt32 program, System.Int32 programInterface, UInt32 index, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr name);
        //[Slot(513)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramStageiv(UInt32 program, System.Int32 shadertype, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] Int32* values);
        //[Slot(514)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetQueryBufferObjecti64v(UInt32 id, UInt32 buffer, System.Int32 pname, IntPtr offset);
        //[Slot(518)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetQueryIndexediv(System.Int32 target, UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(519)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetQueryiv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(520)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetQueryObjecti64v(UInt32 id, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int64* @params);
        //[Slot(521)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetQueryObjectiv(UInt32 id, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(522)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetQueryObjectui64v(UInt32 id, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt64* @params);
        //[Slot(523)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetQueryObjectuiv(UInt32 id, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(524)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetRenderbufferParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(525)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetSamplerParameterfv(UInt32 sampler, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(526)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetSamplerParameterIiv(UInt32 sampler, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(527)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetSamplerParameterIuiv(UInt32 sampler, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(528)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetSamplerParameteriv(UInt32 sampler, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(529)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetSeparableFilter(System.Int32 target, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,format,type")] IntPtr row, [OutAttribute, CountAttribute(Computed = "target,format,type")] IntPtr column, [OutAttribute, CountAttribute(Computed = "target,format,type")] IntPtr span);
        //[Slot(530)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetShaderInfoLog(UInt32 shader, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr infoLog);
        //[Slot(531)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetShaderiv(UInt32 shader, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(532)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetShaderPrecisionFormat(System.Int32 shadertype, System.Int32 precisiontype, [OutAttribute, CountAttribute(Count = 2)] Int32* range, [OutAttribute, CountAttribute(Count = 1)] Int32* precision);
        //[Slot(533)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetShaderSource(UInt32 shader, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr source);
        //[Slot(535)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glGetString(System.Int32 name);
        //[Slot(536)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glGetStringi(System.Int32 name, UInt32 index);
        //[Slot(537)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetSubroutineIndex(UInt32 program, System.Int32 shadertype, IntPtr name);
        //[Slot(538)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetSubroutineUniformLocation(UInt32 program, System.Int32 shadertype, IntPtr name);
        //[Slot(539)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetSynciv(IntPtr sync, System.Int32 pname, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] Int32* values);
        //[Slot(540)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetTexImage(System.Int32 target, Int32 level, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,level,format,type")] IntPtr pixels);
        //[Slot(541)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTexLevelParameterfv(System.Int32 target, Int32 level, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(542)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTexLevelParameteriv(System.Int32 target, Int32 level, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(543)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTexParameterfv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(544)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTexParameterIiv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(545)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTexParameterIuiv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(546)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTexParameteriv(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(549)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetTextureImage(UInt32 texture, Int32 level, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr pixels);
        //[Slot(551)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureLevelParameterfv(UInt32 texture, Int32 level, System.Int32 pname, [OutAttribute] Single* @params);
        //[Slot(553)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureLevelParameteriv(UInt32 texture, Int32 level, System.Int32 pname, [OutAttribute] Int32* @params);
        //[Slot(555)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameterfv(UInt32 texture, System.Int32 pname, [OutAttribute] Single* @params);
        //[Slot(557)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameterIiv(UInt32 texture, System.Int32 pname, [OutAttribute] Int32* @params);
        //[Slot(559)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameterIuiv(UInt32 texture, System.Int32 pname, [OutAttribute] UInt32* @params);
        //[Slot(561)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameteriv(UInt32 texture, System.Int32 pname, [OutAttribute] Int32* @params);
        //[Slot(565)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetTextureSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr pixels);
        //[Slot(566)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTransformFeedbacki_v(UInt32 xfb, System.Int32 pname, UInt32 index, [OutAttribute] Int32* param);
        //[Slot(567)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTransformFeedbacki64_v(UInt32 xfb, System.Int32 pname, UInt32 index, [OutAttribute] Int64* param);
        //[Slot(568)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTransformFeedbackiv(UInt32 xfb, System.Int32 pname, [OutAttribute] Int32* param);
        //[Slot(569)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTransformFeedbackVarying(UInt32 program, UInt32 index, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Count = 1)] Int32* size, [OutAttribute, CountAttribute(Count = 1)] System.Int32* type, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr name);
        //[Slot(570)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetUniformBlockIndex(UInt32 program, [CountAttribute(Computed = "")] IntPtr uniformBlockName);
        //[Slot(571)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformdv(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] Double* @params);
        //[Slot(572)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformfv(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] Single* @params);
        //[Slot(575)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformIndices(UInt32 program, Int32 uniformCount, [CountAttribute(Computed = "uniformCount")] IntPtr uniformNames, [OutAttribute, CountAttribute(Computed = "uniformCount")] UInt32* uniformIndices);
        //[Slot(576)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformiv(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] Int32* @params);
        //[Slot(577)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetUniformLocation(UInt32 program, IntPtr name);
        //[Slot(578)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformSubroutineuiv(System.Int32 shadertype, Int32 location, [OutAttribute, CountAttribute(Count = 1)] UInt32* @params);
        //[Slot(581)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformuiv(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] UInt32* @params);
        //[Slot(582)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexArrayIndexed64iv(UInt32 vaobj, UInt32 index, System.Int32 pname, [OutAttribute] Int64* param);
        //[Slot(583)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexArrayIndexediv(UInt32 vaobj, UInt32 index, System.Int32 pname, [OutAttribute] Int32* param);
        //[Slot(586)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexArrayiv(UInt32 vaobj, System.Int32 pname, [OutAttribute] Int32* param);
        //[Slot(589)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribdv(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Count = 4)] Double* @params);
        //[Slot(590)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribfv(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Count = 4)] Single* @params);
        //[Slot(591)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribIiv(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] Int32* @params);
        //[Slot(592)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribIuiv(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] UInt32* @params);
        //[Slot(593)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribiv(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Count = 4)] Int32* @params);
        //[Slot(594)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribLdv(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Double* @params);
        //[Slot(598)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetVertexAttribPointerv(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] IntPtr pointer);
        //[Slot(600)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glHint(System.Int32 target, System.Int32 mode);
        //[Slot(601)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glHistogram(System.Int32 target, Int32 width, System.Int32 internalformat, bool sink);
        //[Slot(605)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glInvalidateBufferData(UInt32 buffer);
        //[Slot(606)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glInvalidateBufferSubData(UInt32 buffer, IntPtr offset, IntPtr length);
        //[Slot(607)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glInvalidateFramebuffer(System.Int32 target, Int32 numAttachments, [CountAttribute(Parameter = "numAttachments")] System.Int32* attachments);
        //[Slot(608)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glInvalidateNamedFramebufferData(UInt32 framebuffer, Int32 numAttachments, System.Int32* attachments);
        //[Slot(609)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glInvalidateNamedFramebufferSubData(UInt32 framebuffer, Int32 numAttachments, System.Int32* attachments, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(610)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glInvalidateSubFramebuffer(System.Int32 target, Int32 numAttachments, [CountAttribute(Parameter = "numAttachments")] System.Int32* attachments, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(611)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glInvalidateTexImage(UInt32 texture, Int32 level);
        //[Slot(612)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glInvalidateTexSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth);
        //[Slot(613)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsBuffer(UInt32 buffer);
        //[Slot(616)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsEnabled(System.Int32 cap);
        //[Slot(617)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsEnabledi(System.Int32 target, UInt32 index);
        //[Slot(619)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsFramebuffer(UInt32 framebuffer);
        //[Slot(627)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsProgram(UInt32 program);
        //[Slot(628)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsProgramPipeline(UInt32 pipeline);
        //[Slot(630)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsQuery(UInt32 id);
        //[Slot(631)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsRenderbuffer(UInt32 renderbuffer);
        //[Slot(632)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsSampler(UInt32 sampler);
        //[Slot(633)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsShader(UInt32 shader);
        //[Slot(635)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsSync(IntPtr sync);
        //[Slot(636)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsTexture(UInt32 texture);
        //[Slot(639)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsTransformFeedback(UInt32 id);
        //[Slot(640)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsVertexArray(UInt32 array);
        //[Slot(642)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glLineWidth(Single width);
        //[Slot(643)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glLinkProgram(UInt32 program);
        //[Slot(645)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glLogicOp(System.Int32 opcode);
        //[Slot(658)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glMapBuffer(System.Int32 target, System.Int32 access);
        //[Slot(659)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glMapBufferRange(System.Int32 target, IntPtr offset, IntPtr length, System.Int32 access);
        //[Slot(660)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glMapNamedBuffer(UInt32 buffer, System.Int32 access);
        //[Slot(662)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glMapNamedBufferRange(UInt32 buffer, IntPtr offset, IntPtr length, System.Int32 access);
        //[Slot(691)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMemoryBarrier(System.Int32 barriers);
        //[Slot(692)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMemoryBarrierByRegion(System.Int32 barriers);
        //[Slot(693)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMinmax(System.Int32 target, System.Int32 internalformat, bool sink);
        //[Slot(694)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMinSampleShading(Single value);
        //[Slot(696)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiDrawArrays(System.Int32 mode, [CountAttribute(Computed = "count")] Int32* first, [CountAttribute(Computed = "drawcount")] Int32* count, Int32 drawcount);
        //[Slot(697)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawArraysIndirect(System.Int32 mode, [CountAttribute(Computed = "drawcount,stride")] IntPtr indirect, Int32 drawcount, Int32 stride);
        //[Slot(700)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawArraysIndirectCount(System.Int32 mode, IntPtr indirect, IntPtr drawcount, Int32 maxdrawcount, Int32 stride);
        //[Slot(702)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiDrawElements(System.Int32 mode, [CountAttribute(Computed = "drawcount")] Int32* count, System.Int32 type, [CountAttribute(Computed = "drawcount")] IntPtr indices, Int32 drawcount);
        //[Slot(703)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiDrawElementsBaseVertex(System.Int32 mode, [CountAttribute(Computed = "drawcount")] Int32* count, System.Int32 type, [CountAttribute(Computed = "drawcount")] IntPtr indices, Int32 drawcount, [CountAttribute(Computed = "drawcount")] Int32* basevertex);
        //[Slot(704)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawElementsIndirect(System.Int32 mode, System.Int32 type, [CountAttribute(Computed = "drawcount,stride")] IntPtr indirect, Int32 drawcount, Int32 stride);
        //[Slot(707)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawElementsIndirectCount(System.Int32 mode, System.Int32 type, IntPtr indirect, IntPtr drawcount, Int32 maxdrawcount, Int32 stride);
        //[Slot(710)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexCoordP1ui(System.Int32 texture, System.Int32 type, UInt32 coords);
        //[Slot(711)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexCoordP1uiv(System.Int32 texture, System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(712)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexCoordP2ui(System.Int32 texture, System.Int32 type, UInt32 coords);
        //[Slot(713)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexCoordP2uiv(System.Int32 texture, System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(714)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexCoordP3ui(System.Int32 texture, System.Int32 type, UInt32 coords);
        //[Slot(715)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexCoordP3uiv(System.Int32 texture, System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(716)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexCoordP4ui(System.Int32 texture, System.Int32 type, UInt32 coords);
        //[Slot(717)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexCoordP4uiv(System.Int32 texture, System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(742)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferData(UInt32 buffer, IntPtr size, IntPtr data, System.Int32 usage);
        //[Slot(746)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferStorage(UInt32 buffer, IntPtr size, [CountAttribute(Parameter = "size")] IntPtr data, System.Int32 flags);
        //[Slot(748)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferSubData(UInt32 buffer, IntPtr offset, IntPtr size, [CountAttribute(Computed = "size")] IntPtr data);
        //[Slot(751)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferDrawBuffer(UInt32 framebuffer, System.Int32 buf);
        //[Slot(752)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedFramebufferDrawBuffers(UInt32 framebuffer, Int32 n, System.Int32* bufs);
        //[Slot(753)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferParameteri(UInt32 framebuffer, System.Int32 pname, Int32 param);
        //[Slot(755)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferReadBuffer(UInt32 framebuffer, System.Int32 src);
        //[Slot(756)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferRenderbuffer(UInt32 framebuffer, System.Int32 attachment, System.Int32 renderbuffertarget, UInt32 renderbuffer);
        //[Slot(760)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTexture(UInt32 framebuffer, System.Int32 attachment, UInt32 texture, Int32 level);
        //[Slot(766)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTextureLayer(UInt32 framebuffer, System.Int32 attachment, UInt32 texture, Int32 level, Int32 layer);
        //[Slot(780)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedRenderbufferStorage(UInt32 renderbuffer, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(782)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedRenderbufferStorageMultisample(UInt32 renderbuffer, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(787)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNormalP3ui(System.Int32 type, UInt32 coords);
        //[Slot(788)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNormalP3uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(789)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glObjectLabel(System.Int32 identifier, UInt32 name, Int32 length, [CountAttribute(Computed = "label,length")] IntPtr label);
        //[Slot(791)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glObjectPtrLabel(IntPtr ptr, Int32 length, [CountAttribute(Computed = "label,length")] IntPtr label);
        //[Slot(793)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPatchParameterfv(System.Int32 pname, [CountAttribute(Computed = "pname")] Single* values);
        //[Slot(794)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPatchParameteri(System.Int32 pname, Int32 value);
        //[Slot(816)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPauseTransformFeedback();
        //[Slot(817)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPixelStoref(System.Int32 pname, Single param);
        //[Slot(818)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPixelStorei(System.Int32 pname, Int32 param);
        //[Slot(820)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPointParameterf(System.Int32 pname, Single param);
        //[Slot(821)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPointParameterfv(System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(822)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPointParameteri(System.Int32 pname, Int32 param);
        //[Slot(823)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPointParameteriv(System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(824)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPointSize(Single size);
        //[Slot(825)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPolygonMode(System.Int32 face, System.Int32 mode);
        //[Slot(826)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPolygonOffset(Single factor, Single units);
        //[Slot(827)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPolygonOffsetClamp(Single factor, Single units, Single clamp);
        //[Slot(829)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPopDebugGroup();
        //[Slot(833)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPrimitiveRestartIndex(UInt32 index);
        //[Slot(834)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramBinary(UInt32 program, System.Int32 binaryFormat, [CountAttribute(Parameter = "length")] IntPtr binary, Int32 length);
        //[Slot(835)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramParameteri(UInt32 program, System.Int32 pname, Int32 value);
        //[Slot(839)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1d(UInt32 program, Int32 location, Double v0);
        //[Slot(841)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1dv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Double* value);
        //[Slot(843)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1f(UInt32 program, Int32 location, Single v0);
        //[Slot(845)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1fv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Single* value);
        //[Slot(847)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1i(UInt32 program, Int32 location, Int32 v0);
        //[Slot(853)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1iv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Int32* value);
        //[Slot(855)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1ui(UInt32 program, Int32 location, UInt32 v0);
        //[Slot(861)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1uiv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt32* value);
        //[Slot(863)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2d(UInt32 program, Int32 location, Double v0, Double v1);
        //[Slot(865)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2dv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Double* value);
        //[Slot(867)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2f(UInt32 program, Int32 location, Single v0, Single v1);
        //[Slot(869)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2fv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Single* value);
        //[Slot(871)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2i(UInt32 program, Int32 location, Int32 v0, Int32 v1);
        //[Slot(877)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2iv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Int32* value);
        //[Slot(879)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2ui(UInt32 program, Int32 location, UInt32 v0, UInt32 v1);
        //[Slot(885)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2uiv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] UInt32* value);
        //[Slot(887)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3d(UInt32 program, Int32 location, Double v0, Double v1, Double v2);
        //[Slot(889)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3dv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Double* value);
        //[Slot(891)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3f(UInt32 program, Int32 location, Single v0, Single v1, Single v2);
        //[Slot(893)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3fv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Single* value);
        //[Slot(895)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3i(UInt32 program, Int32 location, Int32 v0, Int32 v1, Int32 v2);
        //[Slot(901)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3iv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Int32* value);
        //[Slot(903)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3ui(UInt32 program, Int32 location, UInt32 v0, UInt32 v1, UInt32 v2);
        //[Slot(909)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3uiv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] UInt32* value);
        //[Slot(911)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4d(UInt32 program, Int32 location, Double v0, Double v1, Double v2, Double v3);
        //[Slot(913)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4dv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Double* value);
        //[Slot(915)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4f(UInt32 program, Int32 location, Single v0, Single v1, Single v2, Single v3);
        //[Slot(917)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4fv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Single* value);
        //[Slot(919)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4i(UInt32 program, Int32 location, Int32 v0, Int32 v1, Int32 v2, Int32 v3);
        //[Slot(925)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4iv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Int32* value);
        //[Slot(927)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4ui(UInt32 program, Int32 location, UInt32 v0, UInt32 v1, UInt32 v2, UInt32 v3);
        //[Slot(933)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4uiv(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] UInt32* value);
        //[Slot(939)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*4")] Double* value);
        //[Slot(941)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*4")] Single* value);
        //[Slot(943)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x3dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Double* value);
        //[Slot(945)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x3fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Single* value);
        //[Slot(947)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x4dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Double* value);
        //[Slot(949)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x4fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Single* value);
        //[Slot(951)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*9")] Double* value);
        //[Slot(953)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*9")] Single* value);
        //[Slot(955)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x2dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Double* value);
        //[Slot(957)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x2fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Single* value);
        //[Slot(959)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x4dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Double* value);
        //[Slot(961)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x4fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Single* value);
        //[Slot(963)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*16")] Double* value);
        //[Slot(965)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*16")] Single* value);
        //[Slot(967)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x2dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Double* value);
        //[Slot(969)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x2fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Single* value);
        //[Slot(971)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x3dv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Double* value);
        //[Slot(973)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x3fv(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Single* value);
        //[Slot(977)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProvokingVertex(System.Int32 mode);
        //[Slot(979)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPushDebugGroup(System.Int32 source, UInt32 id, Int32 length, [CountAttribute(Computed = "message,length")] IntPtr message);
        //[Slot(982)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glQueryCounter(UInt32 id, System.Int32 target);
        //[Slot(984)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glReadBuffer(System.Int32 src);
        //[Slot(985)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glReadnPixels(Int32 x, Int32 y, Int32 width, Int32 height, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute] IntPtr data);
        //[Slot(988)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glReadPixels(Int32 x, Int32 y, Int32 width, Int32 height, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "format,type,width,height")] IntPtr pixels);
        //[Slot(989)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glReleaseShaderCompiler();
        //[Slot(990)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glRenderbufferStorage(System.Int32 target, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(991)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glRenderbufferStorageMultisample(System.Int32 target, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(993)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glResetHistogram(System.Int32 target);
        //[Slot(994)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glResetMinmax(System.Int32 target);
        //[Slot(996)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glResumeTransformFeedback();
        //[Slot(997)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSampleCoverage(Single value, bool invert);
        //[Slot(998)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSampleMaski(UInt32 maskNumber, UInt32 mask);
        //[Slot(999)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSamplerParameterf(UInt32 sampler, System.Int32 pname, Single param);
        //[Slot(1000)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSamplerParameterfv(UInt32 sampler, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* param);
        //[Slot(1001)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSamplerParameteri(UInt32 sampler, System.Int32 pname, Int32 param);
        //[Slot(1002)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSamplerParameterIiv(UInt32 sampler, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* param);
        //[Slot(1003)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSamplerParameterIuiv(UInt32 sampler, System.Int32 pname, [CountAttribute(Computed = "pname")] UInt32* param);
        //[Slot(1004)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSamplerParameteriv(UInt32 sampler, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* param);
        //[Slot(1005)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glScissor(Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(1006)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glScissorArrayv(UInt32 first, Int32 count, [CountAttribute(Computed = "count")] Int32* v);
        //[Slot(1007)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glScissorIndexed(UInt32 index, Int32 left, Int32 bottom, Int32 width, Int32 height);
        //[Slot(1008)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glScissorIndexedv(UInt32 index, [CountAttribute(Count = 4)] Int32* v);
        //[Slot(1010)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSecondaryColorP3ui(System.Int32 type, UInt32 color);
        //[Slot(1011)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSecondaryColorP3uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* color);
        //[Slot(1013)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSeparableFilter2D(System.Int32 target, System.Int32 internalformat, Int32 width, Int32 height, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "target,format,type,width")] IntPtr row, [CountAttribute(Computed = "target,format,type,height")] IntPtr column);
        //[Slot(1014)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glShaderBinary(Int32 count, [CountAttribute(Parameter = "count")] UInt32* shaders, System.Int32 binaryformat, [CountAttribute(Parameter = "length")] IntPtr binary, Int32 length);
        //[Slot(1015)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glShaderSource(UInt32 shader, Int32 count, [CountAttribute(Parameter = "count")] IntPtr @string, [CountAttribute(Parameter = "count")] Int32* length);
        //[Slot(1016)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glShaderStorageBlockBinding(UInt32 program, UInt32 storageBlockIndex, UInt32 storageBlockBinding);
        //[Slot(1019)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glSpecializeShader(UInt32 shader, IntPtr pEntryPoint, UInt32 numSpecializationConstants, UInt32* pConstantIndex, UInt32* pConstantValue);
        //[Slot(1024)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilFunc(System.Int32 func, Int32 @ref, UInt32 mask);
        //[Slot(1025)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilFuncSeparate(System.Int32 face, System.Int32 func, Int32 @ref, UInt32 mask);
        //[Slot(1026)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilMask(UInt32 mask);
        //[Slot(1027)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilMaskSeparate(System.Int32 face, UInt32 mask);
        //[Slot(1028)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilOp(System.Int32 fail, System.Int32 zfail, System.Int32 zpass);
        //[Slot(1029)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilOpSeparate(System.Int32 face, System.Int32 sfail, System.Int32 dpfail, System.Int32 dppass);
        //[Slot(1037)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexBuffer(System.Int32 target, System.Int32 internalformat, UInt32 buffer);
        //[Slot(1039)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexBufferRange(System.Int32 target, System.Int32 internalformat, UInt32 buffer, IntPtr offset, IntPtr size);
        //[Slot(1041)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexCoordP1ui(System.Int32 type, UInt32 coords);
        //[Slot(1042)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexCoordP1uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(1043)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexCoordP2ui(System.Int32 type, UInt32 coords);
        //[Slot(1044)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexCoordP2uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(1045)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexCoordP3ui(System.Int32 type, UInt32 coords);
        //[Slot(1046)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexCoordP3uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(1047)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexCoordP4ui(System.Int32 type, UInt32 coords);
        //[Slot(1048)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexCoordP4uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* coords);
        //[Slot(1049)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexImage1D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr pixels);
        //[Slot(1050)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexImage2D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height")] IntPtr pixels);
        //[Slot(1051)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexImage2DMultisample(System.Int32 target, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, bool fixedsamplelocations);
        //[Slot(1052)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexImage3D(System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height,depth")] IntPtr pixels);
        //[Slot(1053)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexImage3DMultisample(System.Int32 target, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, bool fixedsamplelocations);
        //[Slot(1055)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexParameterf(System.Int32 target, System.Int32 pname, Single param);
        //[Slot(1056)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexParameterfv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(1057)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexParameteri(System.Int32 target, System.Int32 pname, Int32 param);
        //[Slot(1058)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexParameterIiv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(1059)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexParameterIuiv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(1060)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTexParameteriv(System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(1061)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexStorage1D(System.Int32 target, Int32 levels, System.Int32 internalformat, Int32 width);
        //[Slot(1062)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexStorage2D(System.Int32 target, Int32 levels, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(1063)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexStorage2DMultisample(System.Int32 target, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, bool fixedsamplelocations);
        //[Slot(1064)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexStorage3D(System.Int32 target, Int32 levels, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth);
        //[Slot(1065)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexStorage3DMultisample(System.Int32 target, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, bool fixedsamplelocations);
        //[Slot(1066)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexSubImage1D(System.Int32 target, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr pixels);
        //[Slot(1067)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexSubImage2D(System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height")] IntPtr pixels);
        //[Slot(1068)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexSubImage3D(System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height,depth")] IntPtr pixels);
        //[Slot(1069)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureBarrier();
        //[Slot(1071)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureBuffer(UInt32 texture, System.Int32 internalformat, UInt32 buffer);
        //[Slot(1073)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureBufferRange(UInt32 texture, System.Int32 internalformat, UInt32 buffer, IntPtr offset, IntPtr size);
        //[Slot(1079)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureParameterf(UInt32 texture, System.Int32 pname, Single param);
        //[Slot(1081)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameterfv(UInt32 texture, System.Int32 pname, Single* param);
        //[Slot(1083)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureParameteri(UInt32 texture, System.Int32 pname, Int32 param);
        //[Slot(1085)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameterIiv(UInt32 texture, System.Int32 pname, Int32* @params);
        //[Slot(1087)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameterIuiv(UInt32 texture, System.Int32 pname, UInt32* @params);
        //[Slot(1089)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameteriv(UInt32 texture, System.Int32 pname, Int32* param);
        //[Slot(1092)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage1D(UInt32 texture, Int32 levels, System.Int32 internalformat, Int32 width);
        //[Slot(1094)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage2D(UInt32 texture, Int32 levels, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(1096)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage2DMultisample(UInt32 texture, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, bool fixedsamplelocations);
        //[Slot(1098)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage3D(UInt32 texture, Int32 levels, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth);
        //[Slot(1100)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage3DMultisample(UInt32 texture, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, bool fixedsamplelocations);
        //[Slot(1102)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureSubImage1D(UInt32 texture, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, System.Int32 type, IntPtr pixels);
        //[Slot(1104)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureSubImage2D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, System.Int32 type, IntPtr pixels);
        //[Slot(1106)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureSubImage3D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, System.Int32 type, IntPtr pixels);
        //[Slot(1108)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureView(UInt32 texture, System.Int32 target, UInt32 origtexture, System.Int32 internalformat, UInt32 minlevel, UInt32 numlevels, UInt32 minlayer, UInt32 numlayers);
        //[Slot(1109)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTransformFeedbackBufferBase(UInt32 xfb, UInt32 index, UInt32 buffer);
        //[Slot(1110)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTransformFeedbackBufferRange(UInt32 xfb, UInt32 index, UInt32 buffer, IntPtr offset, IntPtr size);
        //[Slot(1111)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTransformFeedbackVaryings(UInt32 program, Int32 count, [CountAttribute(Parameter = "count")] IntPtr varyings, System.Int32 bufferMode);
        //[Slot(1113)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1d(Int32 location, Double x);
        //[Slot(1114)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1dv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] Double* value);
        //[Slot(1115)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1f(Int32 location, Single v0);
        //[Slot(1116)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1fv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] Single* value);
        //[Slot(1117)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1i(Int32 location, Int32 v0);
        //[Slot(1122)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1iv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] Int32* value);
        //[Slot(1123)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1ui(Int32 location, UInt32 v0);
        //[Slot(1128)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1uiv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] UInt32* value);
        //[Slot(1129)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2d(Int32 location, Double x, Double y);
        //[Slot(1130)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2dv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Double* value);
        //[Slot(1131)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2f(Int32 location, Single v0, Single v1);
        //[Slot(1132)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2fv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Single* value);
        //[Slot(1133)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2i(Int32 location, Int32 v0, Int32 v1);
        //[Slot(1138)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2iv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Int32* value);
        //[Slot(1139)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2ui(Int32 location, UInt32 v0, UInt32 v1);
        //[Slot(1144)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2uiv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] UInt32* value);
        //[Slot(1145)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3d(Int32 location, Double x, Double y, Double z);
        //[Slot(1146)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3dv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Double* value);
        //[Slot(1147)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3f(Int32 location, Single v0, Single v1, Single v2);
        //[Slot(1148)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3fv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Single* value);
        //[Slot(1149)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3i(Int32 location, Int32 v0, Int32 v1, Int32 v2);
        //[Slot(1154)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3iv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Int32* value);
        //[Slot(1155)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3ui(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2);
        //[Slot(1160)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3uiv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] UInt32* value);
        //[Slot(1161)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4d(Int32 location, Double x, Double y, Double z, Double w);
        //[Slot(1162)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4dv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Double* value);
        //[Slot(1163)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4f(Int32 location, Single v0, Single v1, Single v2, Single v3);
        //[Slot(1164)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4fv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Single* value);
        //[Slot(1165)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4i(Int32 location, Int32 v0, Int32 v1, Int32 v2, Int32 v3);
        //[Slot(1170)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4iv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Int32* value);
        //[Slot(1171)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4ui(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2, UInt32 v3);
        //[Slot(1176)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4uiv(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] UInt32* value);
        //[Slot(1177)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniformBlockBinding(UInt32 program, UInt32 uniformBlockIndex, UInt32 uniformBlockBinding);
        //[Slot(1182)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix2dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*4")] Double* value);
        //[Slot(1183)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix2fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*4")] Single* value);
        //[Slot(1184)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix2x3dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Double* value);
        //[Slot(1185)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix2x3fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Single* value);
        //[Slot(1186)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix2x4dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Double* value);
        //[Slot(1187)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix2x4fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Single* value);
        //[Slot(1188)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix3dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*9")] Double* value);
        //[Slot(1189)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix3fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*9")] Single* value);
        //[Slot(1190)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix3x2dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Double* value);
        //[Slot(1191)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix3x2fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Single* value);
        //[Slot(1192)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix3x4dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Double* value);
        //[Slot(1193)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix3x4fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Single* value);
        //[Slot(1194)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix4dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*16")] Double* value);
        //[Slot(1195)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix4fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*16")] Single* value);
        //[Slot(1196)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix4x2dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Double* value);
        //[Slot(1197)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix4x2fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Single* value);
        //[Slot(1198)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix4x3dv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Double* value);
        //[Slot(1199)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformMatrix4x3fv(Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Single* value);
        //[Slot(1200)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformSubroutinesuiv(System.Int32 shadertype, Int32 count, [CountAttribute(Parameter = "count")] UInt32* indices);
        //[Slot(1203)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glUnmapBuffer(System.Int32 target);
        //[Slot(1204)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glUnmapNamedBuffer(UInt32 buffer);
        //[Slot(1206)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUseProgram(UInt32 program);
        //[Slot(1207)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUseProgramStages(UInt32 pipeline, System.Int32 stages, UInt32 program);
        //[Slot(1210)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glValidateProgram(UInt32 program);
        //[Slot(1211)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glValidateProgramPipeline(UInt32 pipeline);
        //[Slot(1213)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayAttribBinding(UInt32 vaobj, UInt32 attribindex, UInt32 bindingindex);
        //[Slot(1214)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayAttribFormat(UInt32 vaobj, UInt32 attribindex, Int32 size, System.Int32 type, bool normalized, UInt32 relativeoffset);
        //[Slot(1215)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayAttribIFormat(UInt32 vaobj, UInt32 attribindex, Int32 size, System.Int32 type, UInt32 relativeoffset);
        //[Slot(1216)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayAttribLFormat(UInt32 vaobj, UInt32 attribindex, Int32 size, System.Int32 type, UInt32 relativeoffset);
        //[Slot(1217)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayBindingDivisor(UInt32 vaobj, UInt32 bindingindex, UInt32 divisor);
        //[Slot(1221)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayElementBuffer(UInt32 vaobj, UInt32 buffer);
        //[Slot(1237)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexBuffer(UInt32 vaobj, UInt32 bindingindex, UInt32 buffer, IntPtr offset, Int32 stride);
        //[Slot(1238)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexArrayVertexBuffers(UInt32 vaobj, UInt32 first, Int32 count, UInt32* buffers, IntPtr* offsets, Int32* strides);
        //[Slot(1240)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib1d(UInt32 index, Double x);
        //[Slot(1241)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib1dv(UInt32 index, [CountAttribute(Count = 1)] Double* v);
        //[Slot(1242)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib1f(UInt32 index, Single x);
        //[Slot(1243)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib1fv(UInt32 index, [CountAttribute(Count = 1)] Single* v);
        //[Slot(1244)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib1s(UInt32 index, Int16 x);
        //[Slot(1245)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib1sv(UInt32 index, [CountAttribute(Count = 1)] Int16* v);
        //[Slot(1246)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib2d(UInt32 index, Double x, Double y);
        //[Slot(1247)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib2dv(UInt32 index, [CountAttribute(Count = 2)] Double* v);
        //[Slot(1248)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib2f(UInt32 index, Single x, Single y);
        //[Slot(1249)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib2fv(UInt32 index, [CountAttribute(Count = 2)] Single* v);
        //[Slot(1250)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib2s(UInt32 index, Int16 x, Int16 y);
        //[Slot(1251)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib2sv(UInt32 index, [CountAttribute(Count = 2)] Int16* v);
        //[Slot(1252)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib3d(UInt32 index, Double x, Double y, Double z);
        //[Slot(1253)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib3dv(UInt32 index, [CountAttribute(Count = 3)] Double* v);
        //[Slot(1254)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib3f(UInt32 index, Single x, Single y, Single z);
        //[Slot(1255)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib3fv(UInt32 index, [CountAttribute(Count = 3)] Single* v);
        //[Slot(1256)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib3s(UInt32 index, Int16 x, Int16 y, Int16 z);
        //[Slot(1257)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib3sv(UInt32 index, [CountAttribute(Count = 3)] Int16* v);
        //[Slot(1258)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4bv(UInt32 index, [CountAttribute(Count = 4)] SByte* v);
        //[Slot(1259)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib4d(UInt32 index, Double x, Double y, Double z, Double w);
        //[Slot(1260)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4dv(UInt32 index, [CountAttribute(Count = 4)] Double* v);
        //[Slot(1261)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib4f(UInt32 index, Single x, Single y, Single z, Single w);
        //[Slot(1262)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4fv(UInt32 index, [CountAttribute(Count = 4)] Single* v);
        //[Slot(1263)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4iv(UInt32 index, [CountAttribute(Count = 4)] Int32* v);
        //[Slot(1264)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4Nbv(UInt32 index, [CountAttribute(Count = 4)] SByte* v);
        //[Slot(1265)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4Niv(UInt32 index, [CountAttribute(Count = 4)] Int32* v);
        //[Slot(1266)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4Nsv(UInt32 index, [CountAttribute(Count = 4)] Int16* v);
        //[Slot(1267)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib4Nub(UInt32 index, Byte x, Byte y, Byte z, Byte w);
        //[Slot(1268)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4Nubv(UInt32 index, [CountAttribute(Count = 4)] Byte* v);
        //[Slot(1269)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4Nuiv(UInt32 index, [CountAttribute(Count = 4)] UInt32* v);
        //[Slot(1270)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4Nusv(UInt32 index, [CountAttribute(Count = 4)] UInt16* v);
        //[Slot(1271)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttrib4s(UInt32 index, Int16 x, Int16 y, Int16 z, Int16 w);
        //[Slot(1272)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4sv(UInt32 index, [CountAttribute(Count = 4)] Int16* v);
        //[Slot(1273)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4ubv(UInt32 index, [CountAttribute(Count = 4)] Byte* v);
        //[Slot(1274)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4uiv(UInt32 index, [CountAttribute(Count = 4)] UInt32* v);
        //[Slot(1275)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttrib4usv(UInt32 index, [CountAttribute(Count = 4)] UInt16* v);
        //[Slot(1276)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribBinding(UInt32 attribindex, UInt32 bindingindex);
        //[Slot(1277)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribDivisor(UInt32 index, UInt32 divisor);
        //[Slot(1279)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribFormat(UInt32 attribindex, Int32 size, System.Int32 type, bool normalized, UInt32 relativeoffset);
        //[Slot(1281)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI1i(UInt32 index, Int32 x);
        //[Slot(1282)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI1iv(UInt32 index, [CountAttribute(Count = 1)] Int32* v);
        //[Slot(1283)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI1ui(UInt32 index, UInt32 x);
        //[Slot(1284)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI1uiv(UInt32 index, [CountAttribute(Count = 1)] UInt32* v);
        //[Slot(1285)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI2i(UInt32 index, Int32 x, Int32 y);
        //[Slot(1286)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI2iv(UInt32 index, [CountAttribute(Count = 2)] Int32* v);
        //[Slot(1287)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI2ui(UInt32 index, UInt32 x, UInt32 y);
        //[Slot(1288)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI2uiv(UInt32 index, [CountAttribute(Count = 2)] UInt32* v);
        //[Slot(1289)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI3i(UInt32 index, Int32 x, Int32 y, Int32 z);
        //[Slot(1290)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI3iv(UInt32 index, [CountAttribute(Count = 3)] Int32* v);
        //[Slot(1291)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI3ui(UInt32 index, UInt32 x, UInt32 y, UInt32 z);
        //[Slot(1292)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI3uiv(UInt32 index, [CountAttribute(Count = 3)] UInt32* v);
        //[Slot(1293)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI4bv(UInt32 index, [CountAttribute(Count = 4)] SByte* v);
        //[Slot(1294)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI4i(UInt32 index, Int32 x, Int32 y, Int32 z, Int32 w);
        //[Slot(1295)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI4iv(UInt32 index, [CountAttribute(Count = 4)] Int32* v);
        //[Slot(1296)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI4sv(UInt32 index, [CountAttribute(Count = 4)] Int16* v);
        //[Slot(1297)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI4ubv(UInt32 index, [CountAttribute(Count = 4)] Byte* v);
        //[Slot(1298)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribI4ui(UInt32 index, UInt32 x, UInt32 y, UInt32 z, UInt32 w);
        //[Slot(1299)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI4uiv(UInt32 index, [CountAttribute(Count = 4)] UInt32* v);
        //[Slot(1300)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribI4usv(UInt32 index, [CountAttribute(Count = 4)] UInt16* v);
        //[Slot(1301)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribIFormat(UInt32 attribindex, Int32 size, System.Int32 type, UInt32 relativeoffset);
        //[Slot(1303)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribIPointer(UInt32 index, Int32 size, System.Int32 type, Int32 stride, [CountAttribute(Computed = "size,type,stride")] IntPtr pointer);
        //[Slot(1304)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL1d(UInt32 index, Double x);
        //[Slot(1305)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL1dv(UInt32 index, [CountAttribute(Count = 1)] Double* v);
        //[Slot(1312)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL2d(UInt32 index, Double x, Double y);
        //[Slot(1313)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL2dv(UInt32 index, [CountAttribute(Count = 2)] Double* v);
        //[Slot(1318)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL3d(UInt32 index, Double x, Double y, Double z);
        //[Slot(1319)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL3dv(UInt32 index, [CountAttribute(Count = 3)] Double* v);
        //[Slot(1324)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL4d(UInt32 index, Double x, Double y, Double z, Double w);
        //[Slot(1325)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL4dv(UInt32 index, [CountAttribute(Count = 4)] Double* v);
        //[Slot(1330)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribLFormat(UInt32 attribindex, Int32 size, System.Int32 type, UInt32 relativeoffset);
        //[Slot(1332)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribLPointer(UInt32 index, Int32 size, System.Int32 type, Int32 stride, [CountAttribute(Parameter = "size")] IntPtr pointer);
        //[Slot(1333)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribP1ui(UInt32 index, System.Int32 type, bool normalized, UInt32 value);
        //[Slot(1334)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribP1uiv(UInt32 index, System.Int32 type, bool normalized, [CountAttribute(Count = 1)] UInt32* value);
        //[Slot(1335)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribP2ui(UInt32 index, System.Int32 type, bool normalized, UInt32 value);
        //[Slot(1336)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribP2uiv(UInt32 index, System.Int32 type, bool normalized, [CountAttribute(Count = 1)] UInt32* value);
        //[Slot(1337)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribP3ui(UInt32 index, System.Int32 type, bool normalized, UInt32 value);
        //[Slot(1338)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribP3uiv(UInt32 index, System.Int32 type, bool normalized, [CountAttribute(Count = 1)] UInt32* value);
        //[Slot(1339)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribP4ui(UInt32 index, System.Int32 type, bool normalized, UInt32 value);
        //[Slot(1340)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribP4uiv(UInt32 index, System.Int32 type, bool normalized, [CountAttribute(Count = 1)] UInt32* value);
        //[Slot(1341)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribPointer(UInt32 index, Int32 size, System.Int32 type, bool normalized, Int32 stride, [CountAttribute(Computed = "size,type,stride")] IntPtr pointer);
        //[Slot(1342)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexBindingDivisor(UInt32 bindingindex, UInt32 divisor);
        //[Slot(1344)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexP2ui(System.Int32 type, UInt32 value);
        //[Slot(1345)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexP2uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* value);
        //[Slot(1346)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexP3ui(System.Int32 type, UInt32 value);
        //[Slot(1347)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexP3uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* value);
        //[Slot(1348)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexP4ui(System.Int32 type, UInt32 value);
        //[Slot(1349)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexP4uiv(System.Int32 type, [CountAttribute(Count = 1)] UInt32* value);
        //[Slot(1350)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glViewport(Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(1351)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glViewportArrayv(UInt32 first, Int32 count, [CountAttribute(Computed = "count")] Single* v);
        //[Slot(1352)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glViewportIndexedf(UInt32 index, Single x, Single y, Single w, Single h);
        //[Slot(1353)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glViewportIndexedfv(UInt32 index, [CountAttribute(Count = 4)] Single* v);
        //[Slot(1356)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glWaitSync(IntPtr sync, System.Int32 flags, UInt64 timeout);
        //[Slot(0)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glActiveProgramEXT(UInt32 program);
        //[Slot(2)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glActiveShaderProgramEXT(UInt32 pipeline, UInt32 program);
        //[Slot(24)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindMultiTextureEXT(System.Int32 texunit, System.Int32 target, UInt32 texture);
        //[Slot(26)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBindProgramPipelineEXT(UInt32 pipeline);
        //[Slot(63)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glCheckNamedFramebufferStatusEXT(UInt32 framebuffer, System.Int32 target);
        //[Slot(76)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearNamedBufferDataEXT(UInt32 buffer, System.Int32 internalformat, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type")] IntPtr data);
        //[Slot(78)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClearNamedBufferSubDataEXT(UInt32 buffer, System.Int32 internalformat, IntPtr offset, IntPtr size, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type")] IntPtr data);
        //[Slot(86)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glClientAttribDefaultEXT(System.Int32 mask);
        //[Slot(104)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedMultiTexImage1DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(105)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedMultiTexImage2DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(106)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedMultiTexImage3DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(107)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedMultiTexSubImage1DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(108)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedMultiTexSubImage2DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(109)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedMultiTexSubImage3DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(116)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureImage1DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(117)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureImage2DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(118)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureImage3DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(120)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureSubImage1DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(122)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureSubImage2DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(124)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompressedTextureSubImage3DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr bits);
        //[Slot(139)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyMultiTexImage1DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 border);
        //[Slot(140)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyMultiTexImage2DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border);
        //[Slot(141)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyMultiTexSubImage1DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 x, Int32 y, Int32 width);
        //[Slot(142)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyMultiTexSubImage2DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(143)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyMultiTexSubImage3DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(151)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureImage1DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 border);
        //[Slot(152)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureImage2DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border);
        //[Slot(154)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureSubImage1DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 x, Int32 y, Int32 width);
        //[Slot(156)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureSubImage2DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(158)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyTextureSubImage3DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 x, Int32 y, Int32 width, Int32 height);
        //[Slot(175)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glCreateShaderProgramEXT(System.Int32 type, IntPtr @string);
        //[Slot(177)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glCreateShaderProgramvEXT(System.Int32 type, Int32 count, [CountAttribute(Parameter = "count")] IntPtr strings);
        //[Slot(202)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteProgramPipelinesEXT(Int32 n, [CountAttribute(Parameter = "n")] UInt32* pipelines);
        //[Slot(220)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisableClientStateiEXT(System.Int32 array, UInt32 index);
        //[Slot(221)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisableClientStateIndexedEXT(System.Int32 array, UInt32 index);
        //[Slot(223)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisableIndexedEXT(System.Int32 target, UInt32 index);
        //[Slot(225)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisableVertexArrayAttribEXT(UInt32 vaobj, UInt32 index);
        //[Slot(226)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDisableVertexArrayEXT(UInt32 vaobj, System.Int32 array);
        //[Slot(236)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawArraysInstancedEXT(System.Int32 mode, Int32 start, Int32 count, Int32 primcount);
        //[Slot(251)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawElementsInstancedEXT(System.Int32 mode, Int32 count, System.Int32 type, [CountAttribute(Computed = "count,type")] IntPtr indices, Int32 primcount);
        //[Slot(261)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnableClientStateiEXT(System.Int32 array, UInt32 index);
        //[Slot(262)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnableClientStateIndexedEXT(System.Int32 array, UInt32 index);
        //[Slot(264)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnableIndexedEXT(System.Int32 target, UInt32 index);
        //[Slot(266)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnableVertexArrayAttribEXT(UInt32 vaobj, UInt32 index);
        //[Slot(267)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEnableVertexArrayEXT(UInt32 vaobj, System.Int32 array);
        //[Slot(282)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFlushMappedNamedBufferRangeEXT(UInt32 buffer, IntPtr offset, IntPtr length);
        //[Slot(285)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferDrawBufferEXT(UInt32 framebuffer, System.Int32 mode);
        //[Slot(286)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glFramebufferDrawBuffersEXT(UInt32 framebuffer, Int32 n, [CountAttribute(Parameter = "n")] System.Int32* bufs);
        //[Slot(288)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferReadBufferEXT(UInt32 framebuffer, System.Int32 mode);
        //[Slot(304)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGenerateMultiTexMipmapEXT(System.Int32 texunit, System.Int32 target);
        //[Slot(306)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGenerateTextureMipmapEXT(UInt32 texture, System.Int32 target);
        //[Slot(311)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenProgramPipelinesEXT(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* pipelines);
        //[Slot(331)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetBooleanIndexedvEXT(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] bool* data);
        //[Slot(342)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetCompressedMultiTexImageEXT(System.Int32 texunit, System.Int32 target, Int32 lod, [OutAttribute, CountAttribute(Computed = "target,lod")] IntPtr img);
        //[Slot(345)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetCompressedTextureImageEXT(UInt32 texture, System.Int32 target, Int32 lod, [OutAttribute, CountAttribute(Computed = "target,lod")] IntPtr img);
        //[Slot(355)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetDoublei_vEXT(System.Int32 pname, UInt32 index, [OutAttribute, CountAttribute(Computed = "pname")] Double* @params);
        //[Slot(356)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetDoubleIndexedvEXT(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] Double* data);
        //[Slot(361)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFloati_vEXT(System.Int32 pname, UInt32 index, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(362)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFloatIndexedvEXT(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] Single* data);
        //[Slot(368)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFramebufferParameterivEXT(UInt32 framebuffer, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(380)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetIntegerIndexedvEXT(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Computed = "target")] Int32* data);
        //[Slot(391)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexEnvfvEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(392)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexEnvivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(393)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexGendvEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Double* @params);
        //[Slot(394)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexGenfvEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(395)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexGenivEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(396)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetMultiTexImageEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,level,format,type")] IntPtr pixels);
        //[Slot(397)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexLevelParameterfvEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(398)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexLevelParameterivEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(399)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexParameterfvEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(400)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexParameterIivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(401)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexParameterIuivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(402)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetMultiTexParameterivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(405)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedBufferParameterivEXT(UInt32 buffer, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(408)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetNamedBufferPointervEXT(UInt32 buffer, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] IntPtr @params);
        //[Slot(410)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetNamedBufferSubDataEXT(UInt32 buffer, IntPtr offset, IntPtr size, [OutAttribute, CountAttribute(Computed = "size")] IntPtr data);
        //[Slot(412)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedFramebufferAttachmentParameterivEXT(UInt32 framebuffer, System.Int32 attachment, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(414)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedFramebufferParameterivEXT(UInt32 framebuffer, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(415)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedProgramivEXT(UInt32 program, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] Int32* @params);
        //[Slot(416)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedProgramLocalParameterdvEXT(UInt32 program, System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Count = 4)] Double* @params);
        //[Slot(417)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedProgramLocalParameterfvEXT(UInt32 program, System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Count = 4)] Single* @params);
        //[Slot(418)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedProgramLocalParameterIivEXT(UInt32 program, System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Count = 4)] Int32* @params);
        //[Slot(419)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedProgramLocalParameterIuivEXT(UInt32 program, System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Count = 4)] UInt32* @params);
        //[Slot(420)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetNamedProgramStringEXT(UInt32 program, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "program,pname")] IntPtr @string);
        //[Slot(422)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedRenderbufferParameterivEXT(UInt32 renderbuffer, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(468)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetObjectLabelEXT(System.Int32 type, UInt32 @object, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr label);
        //[Slot(495)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetPointeri_vEXT(System.Int32 pname, UInt32 index, [OutAttribute, CountAttribute(Count = 1)] IntPtr @params);
        //[Slot(496)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetPointerIndexedvEXT(System.Int32 target, UInt32 index, [OutAttribute, CountAttribute(Count = 1)] IntPtr data);
        //[Slot(504)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramPipelineInfoLogEXT(UInt32 pipeline, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr infoLog);
        //[Slot(506)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramPipelineivEXT(UInt32 pipeline, System.Int32 pname, [OutAttribute] Int32* @params);
        //[Slot(550)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetTextureImageEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 format, System.Int32 type, [OutAttribute, CountAttribute(Computed = "target,level,format,type")] IntPtr pixels);
        //[Slot(552)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureLevelParameterfvEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(554)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureLevelParameterivEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(556)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameterfvEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(558)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameterIivEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(560)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameterIuivEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(562)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetTextureParameterivEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(584)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexArrayIntegeri_vEXT(UInt32 vaobj, UInt32 index, System.Int32 pname, [OutAttribute] Int32* param);
        //[Slot(585)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexArrayIntegervEXT(UInt32 vaobj, System.Int32 pname, [OutAttribute] Int32* param);
        //[Slot(587)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetVertexArrayPointeri_vEXT(UInt32 vaobj, UInt32 index, System.Int32 pname, [OutAttribute] IntPtr param);
        //[Slot(588)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetVertexArrayPointervEXT(UInt32 vaobj, System.Int32 pname, [OutAttribute, CountAttribute(Count = 1)] IntPtr param);
        //[Slot(603)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glInsertEventMarkerEXT(Int32 length, IntPtr marker);
        //[Slot(618)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsEnabledIndexedEXT(System.Int32 target, UInt32 index);
        //[Slot(629)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsProgramPipelineEXT(UInt32 pipeline);
        //[Slot(641)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glLabelObjectEXT(System.Int32 type, UInt32 @object, Int32 length, IntPtr label);
        //[Slot(661)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glMapNamedBufferEXT(UInt32 buffer, System.Int32 access);
        //[Slot(663)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glMapNamedBufferRangeEXT(UInt32 buffer, IntPtr offset, IntPtr length, System.Int32 access);
        //[Slot(664)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixFrustumEXT(System.Int32 mode, Double left, Double right, Double bottom, Double top, Double zNear, Double zFar);
        //[Slot(667)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixLoaddEXT(System.Int32 mode, [CountAttribute(Count = 16)] Double* m);
        //[Slot(668)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixLoadfEXT(System.Int32 mode, [CountAttribute(Count = 16)] Single* m);
        //[Slot(669)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixLoadIdentityEXT(System.Int32 mode);
        //[Slot(671)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixLoadTransposedEXT(System.Int32 mode, [CountAttribute(Count = 16)] Double* m);
        //[Slot(672)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixLoadTransposefEXT(System.Int32 mode, [CountAttribute(Count = 16)] Single* m);
        //[Slot(675)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixMultdEXT(System.Int32 mode, [CountAttribute(Count = 16)] Double* m);
        //[Slot(676)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixMultfEXT(System.Int32 mode, [CountAttribute(Count = 16)] Single* m);
        //[Slot(678)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixMultTransposedEXT(System.Int32 mode, [CountAttribute(Count = 16)] Double* m);
        //[Slot(679)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixMultTransposefEXT(System.Int32 mode, [CountAttribute(Count = 16)] Single* m);
        //[Slot(680)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixOrthoEXT(System.Int32 mode, Double left, Double right, Double bottom, Double top, Double zNear, Double zFar);
        //[Slot(681)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixPopEXT(System.Int32 mode);
        //[Slot(682)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixPushEXT(System.Int32 mode);
        //[Slot(683)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixRotatedEXT(System.Int32 mode, Double angle, Double x, Double y, Double z);
        //[Slot(684)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixRotatefEXT(System.Int32 mode, Single angle, Single x, Single y, Single z);
        //[Slot(685)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixScaledEXT(System.Int32 mode, Double x, Double y, Double z);
        //[Slot(686)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixScalefEXT(System.Int32 mode, Single x, Single y, Single z);
        //[Slot(687)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixTranslatedEXT(System.Int32 mode, Double x, Double y, Double z);
        //[Slot(688)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMatrixTranslatefEXT(System.Int32 mode, Single x, Single y, Single z);
        //[Slot(709)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexBufferEXT(System.Int32 texunit, System.Int32 target, System.Int32 internalformat, UInt32 buffer);
        //[Slot(718)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexCoordPointerEXT(System.Int32 texunit, Int32 size, System.Int32 type, Int32 stride, [CountAttribute(Computed = "size,type,stride")] IntPtr pointer);
        //[Slot(719)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexEnvfEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, Single param);
        //[Slot(720)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexEnvfvEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(721)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexEnviEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, Int32 param);
        //[Slot(722)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexEnvivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(723)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexGendEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, Double param);
        //[Slot(724)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexGendvEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, [CountAttribute(Computed = "pname")] Double* @params);
        //[Slot(725)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexGenfEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, Single param);
        //[Slot(726)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexGenfvEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(727)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexGeniEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, Int32 param);
        //[Slot(728)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexGenivEXT(System.Int32 texunit, System.Int32 coord, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(729)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexImage1DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr pixels);
        //[Slot(730)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexImage2DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height")] IntPtr pixels);
        //[Slot(731)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexImage3DEXT(System.Int32 texunit, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height,depth")] IntPtr pixels);
        //[Slot(732)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexParameterfEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, Single param);
        //[Slot(733)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexParameterfvEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(734)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexParameteriEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, Int32 param);
        //[Slot(735)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexParameterIivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(736)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexParameterIuivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(737)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMultiTexParameterivEXT(System.Int32 texunit, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(738)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexRenderbufferEXT(System.Int32 texunit, System.Int32 target, UInt32 renderbuffer);
        //[Slot(739)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexSubImage1DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr pixels);
        //[Slot(740)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexSubImage2DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height")] IntPtr pixels);
        //[Slot(741)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiTexSubImage3DEXT(System.Int32 texunit, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height,depth")] IntPtr pixels);
        //[Slot(743)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferDataEXT(UInt32 buffer, IntPtr size, [CountAttribute(Computed = "size")] IntPtr data, System.Int32 usage);
        //[Slot(745)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferPageCommitmentEXT(UInt32 buffer, IntPtr offset, IntPtr size, bool commit);
        //[Slot(747)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferStorageEXT(UInt32 buffer, IntPtr size, [CountAttribute(Parameter = "size")] IntPtr data, System.Int32 flags);
        //[Slot(749)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedBufferSubDataEXT(UInt32 buffer, IntPtr offset, IntPtr size, [CountAttribute(Computed = "size")] IntPtr data);
        //[Slot(750)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedCopyBufferSubDataEXT(UInt32 readBuffer, UInt32 writeBuffer, IntPtr readOffset, IntPtr writeOffset, IntPtr size);
        //[Slot(754)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferParameteriEXT(UInt32 framebuffer, System.Int32 pname, Int32 param);
        //[Slot(757)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferRenderbufferEXT(UInt32 framebuffer, System.Int32 attachment, System.Int32 renderbuffertarget, UInt32 renderbuffer);
        //[Slot(761)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTexture1DEXT(UInt32 framebuffer, System.Int32 attachment, System.Int32 textarget, UInt32 texture, Int32 level);
        //[Slot(762)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTexture2DEXT(UInt32 framebuffer, System.Int32 attachment, System.Int32 textarget, UInt32 texture, Int32 level);
        //[Slot(763)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTexture3DEXT(UInt32 framebuffer, System.Int32 attachment, System.Int32 textarget, UInt32 texture, Int32 level, Int32 zoffset);
        //[Slot(764)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTextureEXT(UInt32 framebuffer, System.Int32 attachment, UInt32 texture, Int32 level);
        //[Slot(765)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTextureFaceEXT(UInt32 framebuffer, System.Int32 attachment, UInt32 texture, Int32 level, System.Int32 face);
        //[Slot(767)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedFramebufferTextureLayerEXT(UInt32 framebuffer, System.Int32 attachment, UInt32 texture, Int32 level, Int32 layer);
        //[Slot(768)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedProgramLocalParameter4dEXT(UInt32 program, System.Int32 target, UInt32 index, Double x, Double y, Double z, Double w);
        //[Slot(769)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedProgramLocalParameter4dvEXT(UInt32 program, System.Int32 target, UInt32 index, [CountAttribute(Count = 4)] Double* @params);
        //[Slot(770)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedProgramLocalParameter4fEXT(UInt32 program, System.Int32 target, UInt32 index, Single x, Single y, Single z, Single w);
        //[Slot(771)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedProgramLocalParameter4fvEXT(UInt32 program, System.Int32 target, UInt32 index, [CountAttribute(Count = 4)] Single* @params);
        //[Slot(772)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedProgramLocalParameterI4iEXT(UInt32 program, System.Int32 target, UInt32 index, Int32 x, Int32 y, Int32 z, Int32 w);
        //[Slot(773)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedProgramLocalParameterI4ivEXT(UInt32 program, System.Int32 target, UInt32 index, [CountAttribute(Count = 4)] Int32* @params);
        //[Slot(774)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedProgramLocalParameterI4uiEXT(UInt32 program, System.Int32 target, UInt32 index, UInt32 x, UInt32 y, UInt32 z, UInt32 w);
        //[Slot(775)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedProgramLocalParameterI4uivEXT(UInt32 program, System.Int32 target, UInt32 index, [CountAttribute(Count = 4)] UInt32* @params);
        //[Slot(776)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedProgramLocalParameters4fvEXT(UInt32 program, System.Int32 target, UInt32 index, Int32 count, [CountAttribute(Parameter = "count*4")] Single* @params);
        //[Slot(777)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedProgramLocalParametersI4ivEXT(UInt32 program, System.Int32 target, UInt32 index, Int32 count, [CountAttribute(Parameter = "count*4")] Int32* @params);
        //[Slot(778)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedProgramLocalParametersI4uivEXT(UInt32 program, System.Int32 target, UInt32 index, Int32 count, [CountAttribute(Parameter = "count*4")] UInt32* @params);
        //[Slot(779)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedProgramStringEXT(UInt32 program, System.Int32 target, System.Int32 format, Int32 len, [CountAttribute(Parameter = "len")] IntPtr @string);
        //[Slot(781)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedRenderbufferStorageEXT(UInt32 renderbuffer, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(783)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedRenderbufferStorageMultisampleCoverageEXT(UInt32 renderbuffer, Int32 coverageSamples, Int32 colorSamples, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(784)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNamedRenderbufferStorageMultisampleEXT(UInt32 renderbuffer, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(828)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPolygonOffsetClampEXT(Single factor, Single units, Single clamp);
        //[Slot(831)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPopGroupMarkerEXT();
        //[Slot(837)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramParameteriEXT(UInt32 program, System.Int32 pname, Int32 value);
        //[Slot(840)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1dEXT(UInt32 program, Int32 location, Double x);
        //[Slot(842)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1dvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Double* value);
        //[Slot(844)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1fEXT(UInt32 program, Int32 location, Single v0);
        //[Slot(846)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1fvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Single* value);
        //[Slot(852)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1iEXT(UInt32 program, Int32 location, Int32 v0);
        //[Slot(854)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1ivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Int32* value);
        //[Slot(860)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1uiEXT(UInt32 program, Int32 location, UInt32 v0);
        //[Slot(862)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1uivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt32* value);
        //[Slot(864)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2dEXT(UInt32 program, Int32 location, Double x, Double y);
        //[Slot(866)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2dvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Double* value);
        //[Slot(868)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2fEXT(UInt32 program, Int32 location, Single v0, Single v1);
        //[Slot(870)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2fvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Single* value);
        //[Slot(876)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2iEXT(UInt32 program, Int32 location, Int32 v0, Int32 v1);
        //[Slot(878)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2ivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Int32* value);
        //[Slot(884)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2uiEXT(UInt32 program, Int32 location, UInt32 v0, UInt32 v1);
        //[Slot(886)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2uivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] UInt32* value);
        //[Slot(888)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3dEXT(UInt32 program, Int32 location, Double x, Double y, Double z);
        //[Slot(890)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3dvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Double* value);
        //[Slot(892)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3fEXT(UInt32 program, Int32 location, Single v0, Single v1, Single v2);
        //[Slot(894)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3fvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Single* value);
        //[Slot(900)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3iEXT(UInt32 program, Int32 location, Int32 v0, Int32 v1, Int32 v2);
        //[Slot(902)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3ivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Int32* value);
        //[Slot(908)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3uiEXT(UInt32 program, Int32 location, UInt32 v0, UInt32 v1, UInt32 v2);
        //[Slot(910)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3uivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] UInt32* value);
        //[Slot(912)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4dEXT(UInt32 program, Int32 location, Double x, Double y, Double z, Double w);
        //[Slot(914)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4dvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Double* value);
        //[Slot(916)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4fEXT(UInt32 program, Int32 location, Single v0, Single v1, Single v2, Single v3);
        //[Slot(918)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4fvEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Single* value);
        //[Slot(924)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4iEXT(UInt32 program, Int32 location, Int32 v0, Int32 v1, Int32 v2, Int32 v3);
        //[Slot(926)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4ivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Int32* value);
        //[Slot(932)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4uiEXT(UInt32 program, Int32 location, UInt32 v0, UInt32 v1, UInt32 v2, UInt32 v3);
        //[Slot(934)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4uivEXT(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] UInt32* value);
        //[Slot(940)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*4")] Double* value);
        //[Slot(942)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*4")] Single* value);
        //[Slot(944)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x3dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Double* value);
        //[Slot(946)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x3fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Single* value);
        //[Slot(948)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x4dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Double* value);
        //[Slot(950)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix2x4fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Single* value);
        //[Slot(952)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*9")] Double* value);
        //[Slot(954)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*9")] Single* value);
        //[Slot(956)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x2dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Double* value);
        //[Slot(958)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x2fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*6")] Single* value);
        //[Slot(960)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x4dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Double* value);
        //[Slot(962)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix3x4fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Single* value);
        //[Slot(964)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*16")] Double* value);
        //[Slot(966)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*16")] Single* value);
        //[Slot(968)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x2dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Double* value);
        //[Slot(970)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x2fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*8")] Single* value);
        //[Slot(972)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x3dvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Double* value);
        //[Slot(974)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformMatrix4x3fvEXT(UInt32 program, Int32 location, Int32 count, bool transpose, [CountAttribute(Parameter = "count*12")] Single* value);
        //[Slot(978)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPushClientAttribDefaultEXT(System.Int32 mask);
        //[Slot(981)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPushGroupMarkerEXT(Int32 length, IntPtr marker);
        //[Slot(983)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glRasterSamplesEXT(UInt32 samples, bool fixedsamplelocations);
        //[Slot(1072)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureBufferEXT(UInt32 texture, System.Int32 target, System.Int32 internalformat, UInt32 buffer);
        //[Slot(1074)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureBufferRangeEXT(UInt32 texture, System.Int32 target, System.Int32 internalformat, UInt32 buffer, IntPtr offset, IntPtr size);
        //[Slot(1075)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureImage1DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr pixels);
        //[Slot(1076)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureImage2DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height")] IntPtr pixels);
        //[Slot(1077)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureImage3DEXT(UInt32 texture, System.Int32 target, Int32 level, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height,depth")] IntPtr pixels);
        //[Slot(1078)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexturePageCommitmentEXT(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, bool commit);
        //[Slot(1080)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureParameterfEXT(UInt32 texture, System.Int32 target, System.Int32 pname, Single param);
        //[Slot(1082)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameterfvEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* @params);
        //[Slot(1084)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureParameteriEXT(UInt32 texture, System.Int32 target, System.Int32 pname, Int32 param);
        //[Slot(1086)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameterIivEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(1088)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameterIuivEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] UInt32* @params);
        //[Slot(1090)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTextureParameterivEXT(UInt32 texture, System.Int32 target, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* @params);
        //[Slot(1091)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureRenderbufferEXT(UInt32 texture, System.Int32 target, UInt32 renderbuffer);
        //[Slot(1093)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage1DEXT(UInt32 texture, System.Int32 target, Int32 levels, System.Int32 internalformat, Int32 width);
        //[Slot(1095)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage2DEXT(UInt32 texture, System.Int32 target, Int32 levels, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(1097)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage2DMultisampleEXT(UInt32 texture, System.Int32 target, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, bool fixedsamplelocations);
        //[Slot(1099)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage3DEXT(UInt32 texture, System.Int32 target, Int32 levels, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth);
        //[Slot(1101)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureStorage3DMultisampleEXT(UInt32 texture, System.Int32 target, Int32 samples, System.Int32 internalformat, Int32 width, Int32 height, Int32 depth, bool fixedsamplelocations);
        //[Slot(1103)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureSubImage1DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 width, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width")] IntPtr pixels);
        //[Slot(1105)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureSubImage2DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height")] IntPtr pixels);
        //[Slot(1107)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureSubImage3DEXT(UInt32 texture, System.Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, System.Int32 format, System.Int32 type, [CountAttribute(Computed = "format,type,width,height,depth")] IntPtr pixels);
        //[Slot(1205)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glUnmapNamedBufferEXT(UInt32 buffer);
        //[Slot(1208)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUseProgramStagesEXT(UInt32 pipeline, System.Int32 stages, UInt32 program);
        //[Slot(1209)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUseShaderProgramEXT(System.Int32 type, UInt32 program);
        //[Slot(1212)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glValidateProgramPipelineEXT(UInt32 pipeline);
        //[Slot(1218)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayBindVertexBufferEXT(UInt32 vaobj, UInt32 bindingindex, UInt32 buffer, IntPtr offset, Int32 stride);
        //[Slot(1219)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayColorOffsetEXT(UInt32 vaobj, UInt32 buffer, Int32 size, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1220)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayEdgeFlagOffsetEXT(UInt32 vaobj, UInt32 buffer, Int32 stride, IntPtr offset);
        //[Slot(1222)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayFogCoordOffsetEXT(UInt32 vaobj, UInt32 buffer, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1223)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayIndexOffsetEXT(UInt32 vaobj, UInt32 buffer, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1224)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayMultiTexCoordOffsetEXT(UInt32 vaobj, UInt32 buffer, System.Int32 texunit, Int32 size, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1225)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayNormalOffsetEXT(UInt32 vaobj, UInt32 buffer, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1226)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArraySecondaryColorOffsetEXT(UInt32 vaobj, UInt32 buffer, Int32 size, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1227)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayTexCoordOffsetEXT(UInt32 vaobj, UInt32 buffer, Int32 size, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1228)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribBindingEXT(UInt32 vaobj, UInt32 attribindex, UInt32 bindingindex);
        //[Slot(1229)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribDivisorEXT(UInt32 vaobj, UInt32 index, UInt32 divisor);
        //[Slot(1230)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribFormatEXT(UInt32 vaobj, UInt32 attribindex, Int32 size, System.Int32 type, bool normalized, UInt32 relativeoffset);
        //[Slot(1231)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribIFormatEXT(UInt32 vaobj, UInt32 attribindex, Int32 size, System.Int32 type, UInt32 relativeoffset);
        //[Slot(1232)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribIOffsetEXT(UInt32 vaobj, UInt32 buffer, UInt32 index, Int32 size, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1233)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribLFormatEXT(UInt32 vaobj, UInt32 attribindex, Int32 size, System.Int32 type, UInt32 relativeoffset);
        //[Slot(1234)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribLOffsetEXT(UInt32 vaobj, UInt32 buffer, UInt32 index, Int32 size, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1235)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexAttribOffsetEXT(UInt32 vaobj, UInt32 buffer, UInt32 index, Int32 size, System.Int32 type, bool normalized, Int32 stride, IntPtr offset);
        //[Slot(1236)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexBindingDivisorEXT(UInt32 vaobj, UInt32 bindingindex, UInt32 divisor);
        //[Slot(1239)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexArrayVertexOffsetEXT(UInt32 vaobj, UInt32 buffer, Int32 size, System.Int32 type, Int32 stride, IntPtr offset);
        //[Slot(1359)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glWindowRectanglesEXT(System.Int32 mode, Int32 count, [CountAttribute(Computed = "count")] Int32* box);
        //[Slot(9)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginPerfQueryINTEL(UInt32 queryHandle);
        //[Slot(168)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreatePerfQueryINTEL(UInt32 queryId, [OutAttribute] UInt32* queryHandle);
        //[Slot(199)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDeletePerfQueryINTEL(UInt32 queryHandle);
        //[Slot(272)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndPerfQueryINTEL(UInt32 queryHandle);
        //[Slot(359)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetFirstPerfQueryIdINTEL([OutAttribute] UInt32* queryId);
        //[Slot(431)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNextPerfQueryIdINTEL(UInt32 queryId, [OutAttribute] UInt32* nextQueryId);
        //[Slot(485)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfCounterInfoINTEL(UInt32 queryId, UInt32 counterId, UInt32 counterNameLength, [OutAttribute, CountAttribute(Parameter = "counterNameLength")] IntPtr counterName, UInt32 counterDescLength, [OutAttribute, CountAttribute(Parameter = "counterDescLength")] IntPtr counterDesc, [OutAttribute] UInt32* counterOffset, [OutAttribute] UInt32* counterDataSize, [OutAttribute] UInt32* counterTypeEnum, [OutAttribute] UInt32* counterDataTypeEnum, [OutAttribute] UInt64* rawCounterMaxValue);
        //[Slot(492)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfQueryDataINTEL(UInt32 queryHandle, UInt32 flags, Int32 dataSize, [OutAttribute] IntPtr data, [OutAttribute] UInt32* bytesWritten);
        //[Slot(493)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfQueryIdByNameINTEL(IntPtr queryName, [OutAttribute] UInt32* queryId);
        //[Slot(494)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPerfQueryInfoINTEL(UInt32 queryId, UInt32 queryNameLength, [OutAttribute, CountAttribute(Parameter = "queryNameLength")] IntPtr queryName, [OutAttribute] UInt32* dataSize, [OutAttribute] UInt32* noCounters, [OutAttribute] UInt32* noInstances, [OutAttribute] UInt32* capsMask);
        //[Slot(37)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendBarrierKHR();
        //[Slot(186)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDebugMessageCallbackKHR(DebugProcKhr callback, IntPtr userParam);
        //[Slot(189)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDebugMessageControlKHR(System.Int32 source, System.Int32 type, System.Int32 severity, Int32 count, UInt32* ids, bool enabled);
        //[Slot(192)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDebugMessageInsertKHR(System.Int32 source, System.Int32 type, UInt32 id, System.Int32 severity, Int32 length, IntPtr buf);
        //[Slot(353)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe Int32 glGetDebugMessageLogKHR(UInt32 count, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* sources, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* types, [OutAttribute, CountAttribute(Parameter = "count")] UInt32* ids, [OutAttribute, CountAttribute(Parameter = "count")] System.Int32* severities, [OutAttribute, CountAttribute(Parameter = "count")] Int32* lengths, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr messageLog);
        //[Slot(371)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glGetGraphicsResetStatusKHR();
        //[Slot(458)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformfvKHR(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Single* @params);
        //[Slot(462)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformivKHR(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Int32* @params);
        //[Slot(466)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetnUniformuivKHR(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] UInt32* @params);
        //[Slot(469)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetObjectLabelKHR(System.Int32 identifier, UInt32 name, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr label);
        //[Slot(471)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetObjectPtrLabelKHR(IntPtr ptr, Int32 bufSize, [OutAttribute, CountAttribute(Count = 1)] Int32* length, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr label);
        //[Slot(498)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glGetPointervKHR(System.Int32 pname, [OutAttribute] IntPtr @params);
        //[Slot(690)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMaxShaderCompilerThreadsKHR(UInt32 count);
        //[Slot(790)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glObjectLabelKHR(System.Int32 identifier, UInt32 name, Int32 length, IntPtr label);
        //[Slot(792)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glObjectPtrLabelKHR(IntPtr ptr, Int32 length, IntPtr label);
        //[Slot(830)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPopDebugGroupKHR();
        //[Slot(980)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPushDebugGroupKHR(System.Int32 source, UInt32 id, Int32 length, IntPtr message);
        //[Slot(987)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glReadnPixelsKHR(Int32 x, Int32 y, Int32 width, Int32 height, System.Int32 format, System.Int32 type, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] IntPtr data);
        //[Slot(7)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginConditionalRenderNV(UInt32 id, System.Int32 mode);
        //[Slot(38)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendBarrierNV();
        //[Slot(52)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBlendParameteriNV(System.Int32 pname, Int32 value);
        //[Slot(55)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBufferAddressRangeNV(System.Int32 pname, UInt32 index, UInt64 address, IntPtr length);
        //[Slot(60)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCallCommandListNV(UInt32 list);
        //[Slot(89)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glColorFormatNV(Int32 size, System.Int32 type, Int32 stride);
        //[Slot(100)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCommandListSegmentsNV(UInt32 list, UInt32 segments);
        //[Slot(101)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCompileCommandListNV(UInt32 list);
        //[Slot(125)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glConservativeRasterParameterfNV(System.Int32 pname, Single value);
        //[Slot(126)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glConservativeRasterParameteriNV(System.Int32 pname, Int32 param);
        //[Slot(145)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCopyPathNV(UInt32 resultPath, UInt32 srcPath);
        //[Slot(159)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCoverageModulationNV(System.Int32 components);
        //[Slot(160)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCoverageModulationTableNV(Int32 n, [CountAttribute(Parameter = "n")] Single* v);
        //[Slot(161)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCoverFillPathInstancedNV(Int32 numPaths, System.Int32 pathNameType, [CountAttribute(Computed = "numPaths,pathNameType,paths")] IntPtr paths, UInt32 pathBase, System.Int32 coverMode, System.Int32 transformType, [CountAttribute(Computed = "numPaths,transformType")] Single* transformValues);
        //[Slot(162)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCoverFillPathNV(UInt32 path, System.Int32 coverMode);
        //[Slot(163)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCoverStrokePathInstancedNV(Int32 numPaths, System.Int32 pathNameType, [CountAttribute(Computed = "numPaths,pathNameType,paths")] IntPtr paths, UInt32 pathBase, System.Int32 coverMode, System.Int32 transformType, [CountAttribute(Computed = "numPaths,transformType")] Single* transformValues);
        //[Slot(164)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glCoverStrokePathNV(UInt32 path, System.Int32 coverMode);
        //[Slot(166)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateCommandListsNV(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* lists);
        //[Slot(178)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glCreateStatesNV(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* states);
        //[Slot(194)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteCommandListsNV(Int32 n, [CountAttribute(Parameter = "n")] UInt32* lists);
        //[Slot(197)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDeletePathsNV(UInt32 path, Int32 range);
        //[Slot(207)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteStatesNV(Int32 n, [CountAttribute(Parameter = "n")] UInt32* states);
        //[Slot(239)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDrawCommandsAddressNV(System.Int32 primitiveMode, UInt64* indirects, Int32* sizes, UInt32 count);
        //[Slot(240)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDrawCommandsNV(System.Int32 primitiveMode, UInt32 buffer, IntPtr* indirects, Int32* sizes, UInt32 count);
        //[Slot(241)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDrawCommandsStatesAddressNV(UInt64* indirects, Int32* sizes, UInt32* states, UInt32* fbos, UInt32 count);
        //[Slot(242)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDrawCommandsStatesNV(UInt32 buffer, IntPtr* indirects, Int32* sizes, UInt32* states, UInt32* fbos, UInt32 count);
        //[Slot(258)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glDrawVkImageNV(UInt64 vkImage, UInt32 sampler, Single x0, Single y0, Single x1, Single y1, Single z, Single s0, Single t0, Single s1, Single t1);
        //[Slot(259)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEdgeFlagFormatNV(Int32 stride);
        //[Slot(270)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndConditionalRenderNV();
        //[Slot(283)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFogCoordFormatNV(System.Int32 type, Int32 stride);
        //[Slot(284)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFragmentCoverageColorNV(UInt32 color);
        //[Slot(291)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glFramebufferSampleLocationsfvNV(System.Int32 target, UInt32 start, Int32 count, Single* v);
        //[Slot(308)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGenPathsNV(Int32 range);
        //[Slot(335)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetBufferParameterui64vNV(System.Int32 target, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt64* @params);
        //[Slot(341)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int32 glGetCommandHeaderNV(System.Int32 tokenID, UInt32 size);
        //[Slot(350)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetCoverageModulationTableNV(Int32 bufsize, [OutAttribute] Single* v);
        //[Slot(376)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int64 glGetImageHandleNV(UInt32 texture, Int32 level, bool layered, Int32 layer, System.Int32 format);
        //[Slot(381)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetIntegerui64i_vNV(System.Int32 value, UInt32 index, [OutAttribute, CountAttribute(Computed = "value")] UInt64* result);
        //[Slot(382)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetIntegerui64vNV(System.Int32 value, [OutAttribute, CountAttribute(Computed = "value")] UInt64* result);
        //[Slot(386)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetInternalformatSampleivNV(System.Int32 target, System.Int32 internalformat, Int32 samples, System.Int32 pname, Int32 bufSize, [OutAttribute, CountAttribute(Parameter = "bufSize")] Int32* @params);
        //[Slot(406)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetNamedBufferParameterui64vNV(UInt32 buffer, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt64* @params);
        //[Slot(472)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathColorGenfvNV(System.Int32 color, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* value);
        //[Slot(473)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathColorGenivNV(System.Int32 color, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* value);
        //[Slot(474)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathCommandsNV(UInt32 path, [OutAttribute, CountAttribute(Computed = "path")] Byte* commands);
        //[Slot(475)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathCoordsNV(UInt32 path, [OutAttribute, CountAttribute(Computed = "path")] Single* coords);
        //[Slot(476)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathDashArrayNV(UInt32 path, [OutAttribute, CountAttribute(Computed = "path")] Single* dashArray);
        //[Slot(477)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Single glGetPathLengthNV(UInt32 path, Int32 startSegment, Int32 numSegments);
        //[Slot(478)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathMetricRangeNV(System.Int32 metricQueryMask, UInt32 firstPathName, Int32 numPaths, Int32 stride, [OutAttribute, CountAttribute(Computed = "metricQueryMask,numPaths,stride")] Single* metrics);
        //[Slot(479)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathMetricsNV(System.Int32 metricQueryMask, Int32 numPaths, System.Int32 pathNameType, [CountAttribute(Computed = "numPaths,pathNameType,paths")] IntPtr paths, UInt32 pathBase, Int32 stride, [OutAttribute, CountAttribute(Computed = "metricQueryMask,numPaths,stride")] Single* metrics);
        //[Slot(480)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathParameterfvNV(UInt32 path, System.Int32 pname, [OutAttribute, CountAttribute(Count = 4)] Single* value);
        //[Slot(481)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathParameterivNV(UInt32 path, System.Int32 pname, [OutAttribute, CountAttribute(Count = 4)] Int32* value);
        //[Slot(482)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathSpacingNV(System.Int32 pathListMode, Int32 numPaths, System.Int32 pathNameType, [CountAttribute(Computed = "numPaths,pathNameType,paths")] IntPtr paths, UInt32 pathBase, Single advanceScale, Single kerningScale, System.Int32 transformType, [OutAttribute, CountAttribute(Computed = "pathListMode,numPaths")] Single* returnedSpacing);
        //[Slot(483)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathTexGenfvNV(System.Int32 texCoordSet, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Single* value);
        //[Slot(484)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetPathTexGenivNV(System.Int32 texCoordSet, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int32* value);
        //[Slot(507)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetProgramResourcefvNV(UInt32 program, System.Int32 programInterface, UInt32 index, Int32 propCount, System.Int32* props, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] Single* @params);
        //[Slot(534)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int16 glGetStageIndexNV(System.Int32 shadertype);
        //[Slot(548)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int64 glGetTextureHandleNV(UInt32 texture);
        //[Slot(564)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern Int64 glGetTextureSamplerHandleNV(UInt32 texture, UInt32 sampler);
        //[Slot(574)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformi64vNV(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] Int64* @params);
        //[Slot(580)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetUniformui64vNV(UInt32 program, Int32 location, [OutAttribute, CountAttribute(Computed = "program,location")] UInt64* @params);
        //[Slot(595)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribLi64vNV(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] Int64* @params);
        //[Slot(597)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetVertexAttribLui64vNV(UInt32 index, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt64* @params);
        //[Slot(599)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern IntPtr glGetVkProcAddrNV([CountAttribute(Computed = "name")] IntPtr name);
        //[Slot(602)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glIndexFormatNV(System.Int32 type, Int32 stride);
        //[Slot(604)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glInterpolatePathsNV(UInt32 resultPath, UInt32 pathA, UInt32 pathB, Single weight);
        //[Slot(614)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsBufferResidentNV(System.Int32 target);
        //[Slot(615)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsCommandListNV(UInt32 list);
        //[Slot(621)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsImageHandleResidentNV(UInt64 handle);
        //[Slot(622)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsNamedBufferResidentNV(UInt32 buffer);
        //[Slot(624)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsPathNV(UInt32 path);
        //[Slot(625)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsPointInFillPathNV(UInt32 path, UInt32 mask, Single x, Single y);
        //[Slot(626)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsPointInStrokePathNV(UInt32 path, Single x, Single y);
        //[Slot(634)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsStateNV(UInt32 state);
        //[Slot(638)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern byte glIsTextureHandleResidentNV(UInt64 handle);
        //[Slot(644)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glListDrawCommandsStatesClientNV(UInt32 list, UInt32 segment, IntPtr indirects, Int32* sizes, UInt32* states, UInt32* fbos, UInt32 count);
        //[Slot(646)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeBufferNonResidentNV(System.Int32 target);
        //[Slot(647)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeBufferResidentNV(System.Int32 target, System.Int32 access);
        //[Slot(649)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeImageHandleNonResidentNV(UInt64 handle);
        //[Slot(651)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeImageHandleResidentNV(UInt64 handle, System.Int32 access);
        //[Slot(652)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeNamedBufferNonResidentNV(UInt32 buffer);
        //[Slot(653)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeNamedBufferResidentNV(UInt32 buffer, System.Int32 access);
        //[Slot(655)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeTextureHandleNonResidentNV(UInt64 handle);
        //[Slot(657)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMakeTextureHandleResidentNV(UInt64 handle);
        //[Slot(665)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixLoad3x2fNV(System.Int32 matrixMode, Single* m);
        //[Slot(666)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixLoad3x3fNV(System.Int32 matrixMode, Single* m);
        //[Slot(670)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixLoadTranspose3x3fNV(System.Int32 matrixMode, Single* m);
        //[Slot(673)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixMult3x2fNV(System.Int32 matrixMode, Single* m);
        //[Slot(674)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixMult3x3fNV(System.Int32 matrixMode, Single* m);
        //[Slot(677)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glMatrixMultTranspose3x3fNV(System.Int32 matrixMode, Single* m);
        //[Slot(698)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawArraysIndirectBindlessCountNV(System.Int32 mode, IntPtr indirect, Int32 drawCount, Int32 maxDrawCount, Int32 stride, Int32 vertexBufferCount);
        //[Slot(699)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawArraysIndirectBindlessNV(System.Int32 mode, IntPtr indirect, Int32 drawCount, Int32 stride, Int32 vertexBufferCount);
        //[Slot(705)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawElementsIndirectBindlessCountNV(System.Int32 mode, System.Int32 type, IntPtr indirect, Int32 drawCount, Int32 maxDrawCount, Int32 stride, Int32 vertexBufferCount);
        //[Slot(706)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glMultiDrawElementsIndirectBindlessNV(System.Int32 mode, System.Int32 type, IntPtr indirect, Int32 drawCount, Int32 stride, Int32 vertexBufferCount);
        //[Slot(759)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glNamedFramebufferSampleLocationsfvNV(UInt32 framebuffer, UInt32 start, Int32 count, Single* v);
        //[Slot(786)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glNormalFormatNV(System.Int32 type, Int32 stride);
        //[Slot(795)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPathColorGenNV(System.Int32 color, System.Int32 genMode, System.Int32 colorFormat, [CountAttribute(Computed = "genMode,colorFormat")] Single* coeffs);
        //[Slot(796)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPathCommandsNV(UInt32 path, Int32 numCommands, [CountAttribute(Parameter = "numCommands")] Byte* commands, Int32 numCoords, System.Int32 coordType, [CountAttribute(Computed = "numCoords,coordType")] IntPtr coords);
        //[Slot(797)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathCoordsNV(UInt32 path, Int32 numCoords, System.Int32 coordType, [CountAttribute(Computed = "numCoords,coordType")] IntPtr coords);
        //[Slot(798)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathCoverDepthFuncNV(System.Int32 func);
        //[Slot(799)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPathDashArrayNV(UInt32 path, Int32 dashCount, [CountAttribute(Parameter = "dashCount")] Single* dashArray);
        //[Slot(800)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathFogGenNV(System.Int32 genMode);
        //[Slot(801)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glPathGlyphIndexArrayNV(UInt32 firstPathName, System.Int32 fontTarget, IntPtr fontName, System.Int32 fontStyle, UInt32 firstGlyphIndex, Int32 numGlyphs, UInt32 pathParameterTemplate, Single emScale);
        //[Slot(802)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glPathGlyphIndexRangeNV(System.Int32 fontTarget, IntPtr fontName, System.Int32 fontStyle, UInt32 pathParameterTemplate, Single emScale, UInt32 baseAndCount);
        //[Slot(803)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathGlyphRangeNV(UInt32 firstPathName, System.Int32 fontTarget, [CountAttribute(Computed = "fontTarget,fontName")] IntPtr fontName, System.Int32 fontStyle, UInt32 firstGlyph, Int32 numGlyphs, System.Int32 handleMissingGlyphs, UInt32 pathParameterTemplate, Single emScale);
        //[Slot(804)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathGlyphsNV(UInt32 firstPathName, System.Int32 fontTarget, [CountAttribute(Computed = "fontTarget,fontName")] IntPtr fontName, System.Int32 fontStyle, Int32 numGlyphs, System.Int32 type, [CountAttribute(Computed = "numGlyphs,type,charcodes")] IntPtr charcodes, System.Int32 handleMissingGlyphs, UInt32 pathParameterTemplate, Single emScale);
        //[Slot(805)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern System.Int32 glPathMemoryGlyphIndexArrayNV(UInt32 firstPathName, System.Int32 fontTarget, IntPtr fontSize, IntPtr fontData, Int32 faceIndex, UInt32 firstGlyphIndex, Int32 numGlyphs, UInt32 pathParameterTemplate, Single emScale);
        //[Slot(806)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathParameterfNV(UInt32 path, System.Int32 pname, Single value);
        //[Slot(807)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPathParameterfvNV(UInt32 path, System.Int32 pname, [CountAttribute(Computed = "pname")] Single* value);
        //[Slot(808)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathParameteriNV(UInt32 path, System.Int32 pname, Int32 value);
        //[Slot(809)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPathParameterivNV(UInt32 path, System.Int32 pname, [CountAttribute(Computed = "pname")] Int32* value);
        //[Slot(810)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathStencilDepthOffsetNV(Single factor, Single units);
        //[Slot(811)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathStencilFuncNV(System.Int32 func, Int32 @ref, UInt32 mask);
        //[Slot(812)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathStringNV(UInt32 path, System.Int32 format, Int32 length, [CountAttribute(Parameter = "length")] IntPtr pathString);
        //[Slot(813)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPathSubCommandsNV(UInt32 path, Int32 commandStart, Int32 commandsToDelete, Int32 numCommands, [CountAttribute(Parameter = "numCommands")] Byte* commands, Int32 numCoords, System.Int32 coordType, [CountAttribute(Computed = "numCoords,coordType")] IntPtr coords);
        //[Slot(814)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glPathSubCoordsNV(UInt32 path, Int32 coordStart, Int32 numCoords, System.Int32 coordType, [CountAttribute(Computed = "numCoords,coordType")] IntPtr coords);
        //[Slot(815)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glPathTexGenNV(System.Int32 texCoordSet, System.Int32 genMode, Int32 components, [CountAttribute(Computed = "genMode,components")] Single* coeffs);
        //[Slot(819)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe byte glPointAlongPathNV(UInt32 path, Int32 startSegment, Int32 numSegments, Single distance, [OutAttribute, CountAttribute(Count = 1)] Single* x, [OutAttribute, CountAttribute(Count = 1)] Single* y, [OutAttribute, CountAttribute(Count = 1)] Single* tangentX, [OutAttribute, CountAttribute(Count = 1)] Single* tangentY);
        //[Slot(838)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramPathFragmentInputGenNV(UInt32 program, Int32 location, System.Int32 genMode, Int32 components, Single* coeffs);
        //[Slot(849)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1i64NV(UInt32 program, Int32 location, Int64 x);
        //[Slot(851)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1i64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] Int64* value);
        //[Slot(857)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform1ui64NV(UInt32 program, Int32 location, UInt64 x);
        //[Slot(859)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform1ui64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt64* value);
        //[Slot(873)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2i64NV(UInt32 program, Int32 location, Int64 x, Int64 y);
        //[Slot(875)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2i64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Int64* value);
        //[Slot(881)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform2ui64NV(UInt32 program, Int32 location, UInt64 x, UInt64 y);
        //[Slot(883)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform2ui64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] UInt64* value);
        //[Slot(897)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3i64NV(UInt32 program, Int32 location, Int64 x, Int64 y, Int64 z);
        //[Slot(899)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3i64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Int64* value);
        //[Slot(905)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform3ui64NV(UInt32 program, Int32 location, UInt64 x, UInt64 y, UInt64 z);
        //[Slot(907)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform3ui64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] UInt64* value);
        //[Slot(921)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4i64NV(UInt32 program, Int32 location, Int64 x, Int64 y, Int64 z, Int64 w);
        //[Slot(923)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4i64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Int64* value);
        //[Slot(929)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniform4ui64NV(UInt32 program, Int32 location, UInt64 x, UInt64 y, UInt64 z, UInt64 w);
        //[Slot(931)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniform4ui64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] UInt64* value);
        //[Slot(936)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniformHandleui64NV(UInt32 program, Int32 location, UInt64 value);
        //[Slot(938)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformHandleui64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt64* values);
        //[Slot(975)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glProgramUniformui64NV(UInt32 program, Int32 location, UInt64 value);
        //[Slot(976)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glProgramUniformui64vNV(UInt32 program, Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt64* value);
        //[Slot(992)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glRenderbufferStorageMultisampleCoverageNV(System.Int32 target, Int32 coverageSamples, Int32 colorSamples, System.Int32 internalformat, Int32 width, Int32 height);
        //[Slot(995)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glResolveDepthValuesNV();
        //[Slot(1009)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSecondaryColorFormatNV(Int32 size, System.Int32 type, Int32 stride);
        //[Slot(1017)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSignalVkFenceNV(UInt64 vkFence);
        //[Slot(1018)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSignalVkSemaphoreNV(UInt64 vkSemaphore);
        //[Slot(1021)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStateCaptureNV(UInt32 state, System.Int32 mode);
        //[Slot(1022)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glStencilFillPathInstancedNV(Int32 numPaths, System.Int32 pathNameType, [CountAttribute(Computed = "numPaths,pathNameType,paths")] IntPtr paths, UInt32 pathBase, System.Int32 fillMode, UInt32 mask, System.Int32 transformType, [CountAttribute(Computed = "numPaths,transformType")] Single* transformValues);
        //[Slot(1023)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilFillPathNV(UInt32 path, System.Int32 fillMode, UInt32 mask);
        //[Slot(1030)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glStencilStrokePathInstancedNV(Int32 numPaths, System.Int32 pathNameType, [CountAttribute(Computed = "numPaths,pathNameType,paths")] IntPtr paths, UInt32 pathBase, Int32 reference, UInt32 mask, System.Int32 transformType, [CountAttribute(Computed = "numPaths,transformType")] Single* transformValues);
        //[Slot(1031)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilStrokePathNV(UInt32 path, Int32 reference, UInt32 mask);
        //[Slot(1032)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glStencilThenCoverFillPathInstancedNV(Int32 numPaths, System.Int32 pathNameType, IntPtr paths, UInt32 pathBase, System.Int32 fillMode, UInt32 mask, System.Int32 coverMode, System.Int32 transformType, Single* transformValues);
        //[Slot(1033)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilThenCoverFillPathNV(UInt32 path, System.Int32 fillMode, UInt32 mask, System.Int32 coverMode);
        //[Slot(1034)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glStencilThenCoverStrokePathInstancedNV(Int32 numPaths, System.Int32 pathNameType, IntPtr paths, UInt32 pathBase, Int32 reference, UInt32 mask, System.Int32 coverMode, System.Int32 transformType, Single* transformValues);
        //[Slot(1035)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glStencilThenCoverStrokePathNV(UInt32 path, Int32 reference, UInt32 mask, System.Int32 coverMode);
        //[Slot(1036)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glSubpixelPrecisionBiasNV(UInt32 xbits, UInt32 ybits);
        //[Slot(1040)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTexCoordFormatNV(Int32 size, System.Int32 type, Int32 stride);
        //[Slot(1070)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glTextureBarrierNV();
        //[Slot(1112)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glTransformPathNV(UInt32 resultPath, UInt32 srcPath, System.Int32 transformType, [CountAttribute(Computed = "transformType")] Single* transformValues);
        //[Slot(1119)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1i64NV(Int32 location, Int64 x);
        //[Slot(1121)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1i64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] Int64* value);
        //[Slot(1125)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform1ui64NV(Int32 location, UInt64 x);
        //[Slot(1127)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform1ui64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] UInt64* value);
        //[Slot(1135)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2i64NV(Int32 location, Int64 x, Int64 y);
        //[Slot(1137)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2i64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] Int64* value);
        //[Slot(1141)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform2ui64NV(Int32 location, UInt64 x, UInt64 y);
        //[Slot(1143)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform2ui64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*2")] UInt64* value);
        //[Slot(1151)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3i64NV(Int32 location, Int64 x, Int64 y, Int64 z);
        //[Slot(1153)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3i64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] Int64* value);
        //[Slot(1157)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform3ui64NV(Int32 location, UInt64 x, UInt64 y, UInt64 z);
        //[Slot(1159)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform3ui64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*3")] UInt64* value);
        //[Slot(1167)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4i64NV(Int32 location, Int64 x, Int64 y, Int64 z, Int64 w);
        //[Slot(1169)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4i64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] Int64* value);
        //[Slot(1173)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniform4ui64NV(Int32 location, UInt64 x, UInt64 y, UInt64 z, UInt64 w);
        //[Slot(1175)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniform4ui64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*4")] UInt64* value);
        //[Slot(1179)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniformHandleui64NV(Int32 location, UInt64 value);
        //[Slot(1181)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformHandleui64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count")] UInt64* value);
        //[Slot(1201)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glUniformui64NV(Int32 location, UInt64 value);
        //[Slot(1202)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glUniformui64vNV(Int32 location, Int32 count, [CountAttribute(Parameter = "count*1")] UInt64* value);
        //[Slot(1280)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribFormatNV(UInt32 index, Int32 size, System.Int32 type, bool normalized, Int32 stride);
        //[Slot(1302)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribIFormatNV(UInt32 index, Int32 size, System.Int32 type, Int32 stride);
        //[Slot(1306)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL1i64NV(UInt32 index, Int64 x);
        //[Slot(1307)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL1i64vNV(UInt32 index, [CountAttribute(Count = 1)] Int64* v);
        //[Slot(1309)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL1ui64NV(UInt32 index, UInt64 x);
        //[Slot(1311)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL1ui64vNV(UInt32 index, [CountAttribute(Count = 1)] UInt64* v);
        //[Slot(1314)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL2i64NV(UInt32 index, Int64 x, Int64 y);
        //[Slot(1315)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL2i64vNV(UInt32 index, [CountAttribute(Count = 2)] Int64* v);
        //[Slot(1316)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL2ui64NV(UInt32 index, UInt64 x, UInt64 y);
        //[Slot(1317)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL2ui64vNV(UInt32 index, [CountAttribute(Count = 2)] UInt64* v);
        //[Slot(1320)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL3i64NV(UInt32 index, Int64 x, Int64 y, Int64 z);
        //[Slot(1321)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL3i64vNV(UInt32 index, [CountAttribute(Count = 3)] Int64* v);
        //[Slot(1322)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL3ui64NV(UInt32 index, UInt64 x, UInt64 y, UInt64 z);
        //[Slot(1323)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL3ui64vNV(UInt32 index, [CountAttribute(Count = 3)] UInt64* v);
        //[Slot(1326)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL4i64NV(UInt32 index, Int64 x, Int64 y, Int64 z, Int64 w);
        //[Slot(1327)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL4i64vNV(UInt32 index, [CountAttribute(Count = 4)] Int64* v);
        //[Slot(1328)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribL4ui64NV(UInt32 index, UInt64 x, UInt64 y, UInt64 z, UInt64 w);
        //[Slot(1329)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glVertexAttribL4ui64vNV(UInt32 index, [CountAttribute(Count = 4)] UInt64* v);
        //[Slot(1331)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexAttribLFormatNV(UInt32 index, Int32 size, System.Int32 type, Int32 stride);
        //[Slot(1343)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glVertexFormatNV(Int32 size, System.Int32 type, Int32 stride);
        //[Slot(1354)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glViewportPositionWScaleNV(UInt32 index, Single xcoeff, Single ycoeff);
        //[Slot(1355)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glViewportSwizzleNV(UInt32 index, System.Int32 swizzlex, System.Int32 swizzley, System.Int32 swizzlez, System.Int32 swizzlew);
        //[Slot(1357)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glWaitVkSemaphoreNV(UInt64 vkSemaphore);
        //[Slot(1358)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glWeightPathsNV(UInt32 resultPath, Int32 numPaths, [CountAttribute(Parameter = "numPaths")] UInt32* paths, [CountAttribute(Parameter = "numPaths")] Single* weights);
        //[Slot(300)]
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glFramebufferTextureMultiviewOVR(System.Int32 target, System.Int32 attachment, UInt32 texture, Int32 level, Int32 baseViewIndex, Int32 numViews);

    }
}
