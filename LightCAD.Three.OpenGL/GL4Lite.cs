﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace LightCAD.Three.OpenGL
{
    public class GLInt
    {
        public int Value;
    }
    public class DebugLog
    {
        public MethodBase Method;
        public object[] ResultAndParams;
        public int[] ConstIndics;
        public ErrorCode Error;
        public string StackTrace;


        public DebugLog Constants(params int[] constIndics)
        {
            ConstIndics = constIndics;
            return this;
        }
        /// <summary>
        /// Args[0]
        /// </summary>
        /// <returns></returns>
        public object GetResult()
        {
            return ResultAndParams[0];
        }
        public string GetClassName()
        {
            return Method.DeclaringType.Name;
        }
        public string GetMethodName()
        {
            return Method.Name;
        }
        public List<KeyValuePair<string, object>> GetParamsAndValues()
        {
            var dict = new List<KeyValuePair<string, object>>();
            var pms = this.Method.GetParameters();
            for (var i = 0; i < pms.Length; i++)
            {
                dict.Add(new KeyValuePair<string, object>(pms[i].Name, this.ResultAndParams[i + 1]));
            }
            return dict;
        }
        public string GetParamsInfo()
        {
            var pvList = GetParamsAndValues();
            var sb = new StringBuilder();
            for (var i = 0; i < pvList.Count; i++)
            {
                if (i != 0)
                {
                    sb.Append(", ");
                }
                    var pv = pvList[i];
                var str = pv.Value == null ? "null" : pv.Value.ToString();
                if (str.Length > 30) str = str.Substring(0, 30)+" ...";
                sb.Append($"{pv.Key}:[{str}]");
            }
            return sb.ToString();
        }
        public string Status
        {
            get
            {
                if (Error == ErrorCode.NoError)
                    return "SUCC";
                else
                    return "ERROR";
            }
        }
        public string InvokeInfo
        {
            get
            {
                var result = this.GetResult();
                var resultStr = result == null ? "void" : result.ToString();
                if (resultStr.Length > 30) resultStr = resultStr.Substring(0,30) + " ...";
                return $"{this.GetClassName()}.{this.GetMethodName()}( {this.GetParamsInfo()} ) -> [{resultStr}]";
            }
        }
    }
    public class WebGLActiveInfo
    {
        public string name;
        public int type;
        public int size;
    }
    /// <summary>
    /// Provides access to OpenGL 4.x methods for the core profile.
    /// </summary>
    public sealed partial class gl : GraphicsBindingsBase
    {
        //public static IGLControl2 canvas;
        private const string Library = "opengl32.dll";
        private static readonly object sync_root = new object();

        private static IntPtr[] EntryPoints;
        private static string[] EntryPointNames;
        //private static int[] EntryPointNameOffsets;

        public static bool DebugEnabled = false;
        public static List<DebugLog> DebugLogs = new List<DebugLog>();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public gl()
        {
            _EntryPointsInstance = EntryPoints;
            _EntryPointNamesInstance = EntryPointNames;
            //_EntryPointNameOffsetsInstance = EntryPointNameOffsets;
        }

        /// <summary>
        /// Returns a synchronization token unique for the GL class.
        /// </summary>
        protected override object SyncRoot
        {
            get { return sync_root; }
        }

#pragma warning disable 3019
#pragma warning disable 1591
#pragma warning disable 1572
#pragma warning disable 1573

        private static DebugLog DebugMethod(params object[] resultAndParams)
        {
             var stackTrace = new StackTrace();
            var frame = stackTrace.GetFrame(1);
            var method = frame.GetMethod();
            var frames = stackTrace.GetFrames();
            frames=frames.Skip(1).Take(11).ToArray();
            frames = frames.Reverse().ToArray();
            var stackTraceStr = "";
            foreach (var item in  frames)
            {
                var m = item.GetMethod();
                stackTraceStr += m.DeclaringType.Name+"."+m.Name+"()"+"/";
            }
            stackTraceStr.Remove(stackTraceStr.Length - 1);
            var dlog = new DebugLog()
            {
                Method = method,
                ResultAndParams = resultAndParams,
                Error = gl.getError(),
                StackTrace= stackTraceStr
            };
            if (dlog.Error != gl.NO_ERROR)
            {
                //StringBuilder stringBuilder = new StringBuilder();
                //gl.DebugLogs.ForEach(l =>
                //{
                //    var methodName = l.GetMethodName();
                //    stringBuilder.AppendLine(methodName + ":" + l.GetParamsInfo());
                //});
                //var str=stringBuilder.ToString();
            }
            DebugLogs.Add(dlog);
            return dlog;
        }
        public static int getProgramParameter(int program, int pname)
        {
            var result = 0;
            if (pname == gl.ACTIVE_UNIFORMS
                || pname == gl.LINK_STATUS
                || pname == gl.ACTIVE_ATTRIBUTES)
                result = GetProgramiv(program, pname);
            else
                result = -1;

            if (DebugEnabled) DebugMethod(result, program, pname).Constants(1);
            return result;
        }

        public static int getShaderParameter(int shader, int pname)
        {
            var data = new int[1];
            gl.getShader(shader, pname, data);
            var result = data[0];

            if (DebugEnabled) DebugMethod(result, shader, pname).Constants(1);
            return result;

        }

        public static object getParameter(int pname)
        {
            if (pname == gl.ACTIVE_UNIFORMS
                || pname == VERSION
                || pname== MAX_TEXTURE_MAX_ANISOTROPY
                || pname == MAX_COMBINED_TEXTURE_IMAGE_UNITS
                || pname == MAX_TEXTURE_IMAGE_UNITS
                || pname == MAX_VERTEX_ATTRIBS
                || pname == MAX_VERTEX_TEXTURE_IMAGE_UNITS
                || pname == MAX_TEXTURE_SIZE
                || pname == MAX_CUBE_MAP_TEXTURE_SIZE
                || pname == MAX_VERTEX_ATTRIBS
                || pname == MAX_VERTEX_UNIFORM_VECTORS
                || pname == MAX_VARYING_VECTORS
                || pname == MAX_FRAGMENT_UNIFORM_VECTORS
                || pname == MAX_SAMPLES
                || pname == MAX_UNIFORM_BUFFER_BINDINGS
               )
            {
                var result = GetInteger(pname);

                if (DebugEnabled) DebugMethod(result, pname).Constants(0);
                return result;
            }
            else if (pname == NUM_COMPRESSED_TEXTURE_FORMATS)
            {
                int formatCount;
                GetInteger(NUM_COMPRESSED_TEXTURE_FORMATS, out formatCount);
                int[] formats = new int[formatCount];
                GetInteger(COMPRESSED_TEXTURE_FORMATS, formats);

                if (DebugEnabled) DebugMethod(formats, pname).Constants(0);
                return formats;
            }
            else if (pname == SCISSOR_BOX || pname == VIEWPORT)
            {
                int[] boxes = new int[4];
                GetInteger(SCISSOR_BOX, boxes);

                if (DebugEnabled) DebugMethod(boxes, pname).Constants(0);
                return boxes;
            }
            if (DebugEnabled) DebugMethod(-1, pname).Constants(0);
            return -1;

        }







        // Note: Mono 1.9.1 truncates StringBuilder results (for 'out string' parameters).
        // We work around this issue by doubling the StringBuilder capacity.

        public static int createBuffer()
        {
            var result = GenBuffer();
            if (DebugEnabled) DebugMethod(result);
            return result;

        }
        /// <summary>[requires: v1.5]
        /// Generate buffer object names
        /// </summary>
        [AutoGenerated(Category = "VERSION_1_5", Version = "1.5", EntryPoint = "glGenBuffers")]
        [CLSCompliant(false)]
        public unsafe static Int32 GenBuffer()
        {
            var buffers = new int[1];
            fixed (int* ptr = buffers)
            {
                genBuffers(1, ptr);
            }
            return (int)buffers[0];
        }



        public unsafe static void GenBuffers(Int32 n, out Int32 buffer)
        {
            var buffers = new int[n];
            fixed (int* ptr = buffers)
            {
                genBuffers(n, ptr);
            }
            buffer = (int)buffers[0];
        }
        public static void bufferData(int target, byte[] data, int usage)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var size = data.Length;
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, data, usage).Constants(2);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(int target, sbyte[] data, int usage)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var size = data.Length;
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, data, usage).Constants(2);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(int target, float[] data, int usage)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var size = data.Length * 4;
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, data, usage).Constants(2);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(int target, short[] data, int usage)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var size = data.Length * 2;
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, data, usage).Constants(2);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(int target, ushort[] data, int usage)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var size = data.Length * 2;
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, data, usage).Constants(2);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(int target, int[] data, int usage)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var size = data.Length * 4;
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, data, usage).Constants(2);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(int target, uint[] data, int usage)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var size = data.Length * 4;
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, data, usage).Constants(2);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(int target, int size, Array data, int usage = (int)BufferUsageHint.StaticDraw)
        {

            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, size, data, usage).Constants(3);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }
        public static void bufferData(BufferTarget target, int size, IntPtr data, BufferUsageHint usage)
        {
            bufferData((int)target, size, data, (int)usage);
        }
        public static void bufferData(BufferTarget target, int size, object data, BufferUsageHint usage)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                bufferData(target, size, handle.AddrOfPinnedObject(), usage);
                handle.Free();
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }


        public unsafe static void deleteBuffer(int buffer)
        {
            var buffers = new int[] { buffer };
            fixed (int* ptr = buffers)
            {
                deleteBuffers(1, ptr);
                if (DebugEnabled) DebugMethod(null, buffer);
            }
        }

        public static void clearColor(Color color)
        {
            gl.clearColor(color.R / 255.0f, color.G / 255.0f, color.B / 255.0f, color.A / 255.0f);
        }

        public static void clearColor(Color4 color)
        {
            gl.clearColor(color.R, color.G, color.B, color.A);
        }

        public static void blendColor(Color color)
        {
            gl.BlendColor(color.R / 255.0f, color.G / 255.0f, color.B / 255.0f, color.A / 255.0f);
        }

        public static void blendColor(Color4 color)
        {
            gl.BlendColor(color.R, color.G, color.B, color.A);
        }

       
        public static void Uniform4(int location, Color4 color)
        {
            gl.uniform4f(location, color.R, color.G, color.B, color.A);
        }

        
        public class ShaderPrecisionFormat
        {
            public int rangeMin;
            public int rangeMax;
            public int precision;
        }
        public unsafe static ShaderPrecisionFormat getShaderPrecisionFormat(ShaderType shadertype, ShaderPrecision precisiontype)
        {
            return getShaderPrecisionFormat((int)shadertype, (int)precisiontype);
        }
        public unsafe static ShaderPrecisionFormat getShaderPrecisionFormat(int shadertype, int precisiontype)
        {
            var range = new int[2];
            int precesion = 0;
            fixed (int* rangePtr = range)
            {
                gl.GetShaderPrecisionFormat(shadertype, precisiontype, rangePtr, &precesion);
            }
            var result = new ShaderPrecisionFormat()
            {
                rangeMin = range[0],
                rangeMax = range[1],
                precision = precesion
            };
            if (DebugEnabled) DebugMethod(result, shadertype, precisiontype).Constants(0, 1);

            return result;
        }

        

        public static void ProgramUniform4(int program, int location, Color4 color)
        {
            gl.ProgramUniform4(program, location, color.R, color.G, color.B, color.A);
        }

       

        public static WebGLActiveInfo getActiveAttrib(int program, int index)
        {
            int length;
            GetProgramiv(program, OpenGL.GetProgramParameterName.ActiveAttributeMaxLength, out length);
            var strarr = new byte[length * 2];
            int size = 0;
            int type = 0;
            string name = null;
            var handle = GCHandle.Alloc(strarr, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                var strptr = handle.AddrOfPinnedObject();
                getActiveAttrib(program, index, length == 0 ? 1 : length * 2, out length, out size, out type, strptr);
                name = Marshal.PtrToStringAnsi(strptr);
                handle.Free();
            }
            var result = new WebGLActiveInfo()
            {
                name = name,
                size = size,
                type = type
            };

            if (DebugEnabled) DebugMethod(result, program, index);
            return result;
        }

        public static string getActiveUniform(int program, int uniformIndex, out int size, out ActiveUniformType type)
        {
            int length;
            GetProgramiv(program, OpenGL.GetProgramParameterName.ActiveUniformMaxLength, out length);

            var info = getActiveUniform(program, uniformIndex);
            size = info.size;
            type = (ActiveUniformType)info.type;
            return info.name;
        }

        public static void shaderSource(Int32 shader, System.String source)
        {
            unsafe
            {

                var lenght = source.Length;
                var strarr = new string[] { source };
                gl.shaderSource(shader, 1, strarr, &lenght);

                if (DebugEnabled) DebugMethod(null, shader, source);
            }
        }

        public static string getShaderInfoLog(Int32 shader)
        {
            string info;
            getShaderInfoLog(shader, out info);

            if (DebugEnabled) DebugMethod(info, shader);
            return info;
        }
        public static void getShaderInfoLog(Int32 shader, out string info)
        {
            unsafe
            {
                int length;
                gl.getShader(shader, ShaderParameter.InfoLogLength, out length);
                if (length == 0)
                {
                    info = String.Empty;
                    return;
                }
                info = gl.getShaderInfoLog(shader, length * 2, length);
            }
        }

        public static string getProgramInfoLog(Int32 program)
        {
            string info;
            getProgramInfoLog(program, out info);
            if (DebugEnabled) DebugMethod(info, program);
            return info;
        }

        public static void getProgramInfoLog(Int32 program, out string info)
        {
            unsafe
            {
                info = null;

                int length;
                gl.GetProgramiv(program, OpenGL.GetProgramParameterName.InfoLogLength, out length);
                if (length == 0)
                {
                    info = String.Empty;
                    return;
                }
                var infoBuf = new byte[length * 2];
                var handle = GCHandle.Alloc(infoBuf, GCHandleType.Pinned);
                if (handle.IsAllocated)
                {
                    var ptr = handle.AddrOfPinnedObject();
                    gl.getProgramInfoLog(program, length * 2, &length, ptr);
                    info = Marshal.PtrToStringAnsi(ptr);
                    handle.Free();
                }
            }
        }

     

        public static void vertexAttribPointer(int index, int size, VertexAttribPointerType type, bool normalized, int stride, int offset)
        {
            vertexAttribPointer(index, size, (int)type, normalized, stride, (IntPtr)offset);
        }
        public static void vertexAttribPointer(int index, int size, VertexAttribPointerType type, bool normalized, int stride, float[] data)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                vertexAttribPointer(index, size, (int)type, normalized, stride, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }
        public static void vertexAttribPointer(int index, int size, int type, bool normalized, int stride, int offset)
        {
            vertexAttribPointer(index, size, type, normalized, stride, (IntPtr)offset);
  
            if (DebugEnabled) DebugMethod(null, index,size,type,normalized,stride,offset).Constants(2);
        }
        public static void vertexAttribPointer(int index, int size, int type, bool normalized, int stride, float[] data)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                vertexAttribPointer(index, size, type, normalized, stride, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }

        public static void drawElements(BeginMode mode, int count, DrawElementsType type, int offset)
        {
            drawElements((PrimitiveType)mode, count, type, new IntPtr(offset));
        }
 
        public static void viewport(Size size)
        {
            gl.viewport(0, 0, size.Width, size.Height);
        }

        public static void viewport(Point location, Size size)
        {
            gl.viewport(location.X, location.Y, size.Width, size.Height);
        }

        public static void viewport(Rectangle rectangle)
        {
            gl.viewport(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }
#if MINIMAL
        public static void Viewport(OpenTK.Point location, OpenTK.Size size)
        {
            GL.Viewport(location.X, location.Y, size.Width, size.Height);
        }

        public static void Viewport(OpenTK.Rectangle rectangle)
        {
            GL.Viewport(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }
#endif

#pragma warning restore 3019
#pragma warning restore 1591
#pragma warning restore 1572
#pragma warning restore 1573
    }

    [CLSCompliant(false)]
    public partial class gl
    {
        static gl()
        {
            EntryPointNames = new string[]
            {
"glActiveProgramEXT",
"glActiveShaderProgram",
"glActiveShaderProgramEXT",
"glActiveTexture",
"glApplyFramebufferAttachmentCMAAINTEL",
"glAttachShader",
"glBeginConditionalRender",
"glBeginConditionalRenderNV",
"glBeginPerfMonitorAMD",
"glBeginPerfQueryINTEL",
"glBeginQuery",
"glBeginQueryIndexed",
"glBeginTransformFeedback",
"glBindAttribLocation",
"glBindBuffer",
"glBindBufferBase",
"glBindBufferRange",
"glBindBuffersBase",
"glBindBuffersRange",
"glBindFragDataLocation",
"glBindFragDataLocationIndexed",
"glBindFramebuffer",
"glBindImageTexture",
"glBindImageTextures",
"glBindMultiTextureEXT",
"glBindProgramPipeline",
"glBindProgramPipelineEXT",
"glBindRenderbuffer",
"glBindSampler",
"glBindSamplers",
"glBindTexture",
"glBindTextures",
"glBindTextureUnit",
"glBindTransformFeedback",
"glBindVertexArray",
"glBindVertexBuffer",
"glBindVertexBuffers",
"glBlendBarrierKHR",
"glBlendBarrierNV",
"glBlendColor",
"glBlendEquation",
"glBlendEquationi",
"glBlendEquationiARB",
"glBlendEquationSeparate",
"glBlendEquationSeparatei",
"glBlendEquationSeparateiARB",
"glBlendFunc",
"glBlendFunci",
"glBlendFunciARB",
"glBlendFuncSeparate",
"glBlendFuncSeparatei",
"glBlendFuncSeparateiARB",
"glBlendParameteriNV",
"glBlitFramebuffer",
"glBlitNamedFramebuffer",
"glBufferAddressRangeNV",
"glBufferData",
"glBufferPageCommitmentARB",
"glBufferStorage",
"glBufferSubData",
"glCallCommandListNV",
"glCheckFramebufferStatus",
"glCheckNamedFramebufferStatus",
"glCheckNamedFramebufferStatusEXT",
"glClampColor",
"glClear",
"glClearBufferData",
"glClearBufferfi",
"glClearBufferfv",
"glClearBufferiv",
"glClearBufferSubData",
"glClearBufferuiv",
"glClearColor",
"glClearDepth",
"glClearDepthf",
"glClearNamedBufferData",
"glClearNamedBufferDataEXT",
"glClearNamedBufferSubData",
"glClearNamedBufferSubDataEXT",
"glClearNamedFramebufferfi",
"glClearNamedFramebufferfv",
"glClearNamedFramebufferiv",
"glClearNamedFramebufferuiv",
"glClearStencil",
"glClearTexImage",
"glClearTexSubImage",
"glClientAttribDefaultEXT",
"glClientWaitSync",
"glClipControl",
"glColorFormatNV",
"glColorMask",
"glColorMaski",
"glColorP3ui",
"glColorP3uiv",
"glColorP4ui",
"glColorP4uiv",
"glColorSubTable",
"glColorTable",
"glColorTableParameterfv",
"glColorTableParameteriv",
"glCommandListSegmentsNV",
"glCompileCommandListNV",
"glCompileShader",
"glCompileShaderIncludeARB",
"glCompressedMultiTexImage1DEXT",
"glCompressedMultiTexImage2DEXT",
"glCompressedMultiTexImage3DEXT",
"glCompressedMultiTexSubImage1DEXT",
"glCompressedMultiTexSubImage2DEXT",
"glCompressedMultiTexSubImage3DEXT",
"glCompressedTexImage1D",
"glCompressedTexImage2D",
"glCompressedTexImage3D",
"glCompressedTexSubImage1D",
"glCompressedTexSubImage2D",
"glCompressedTexSubImage3D",
"glCompressedTextureImage1DEXT",
"glCompressedTextureImage2DEXT",
"glCompressedTextureImage3DEXT",
"glCompressedTextureSubImage1D",
"glCompressedTextureSubImage1DEXT",
"glCompressedTextureSubImage2D",
"glCompressedTextureSubImage2DEXT",
"glCompressedTextureSubImage3D",
"glCompressedTextureSubImage3DEXT",
"glConservativeRasterParameterfNV",
"glConservativeRasterParameteriNV",
"glConvolutionFilter1D",
"glConvolutionFilter2D",
"glConvolutionParameterf",
"glConvolutionParameterfv",
"glConvolutionParameteri",
"glConvolutionParameteriv",
"glCopyBufferSubData",
"glCopyColorSubTable",
"glCopyColorTable",
"glCopyConvolutionFilter1D",
"glCopyConvolutionFilter2D",
"glCopyImageSubData",
"glCopyMultiTexImage1DEXT",
"glCopyMultiTexImage2DEXT",
"glCopyMultiTexSubImage1DEXT",
"glCopyMultiTexSubImage2DEXT",
"glCopyMultiTexSubImage3DEXT",
"glCopyNamedBufferSubData",
"glCopyPathNV",
"glCopyTexImage1D",
"glCopyTexImage2D",
"glCopyTexSubImage1D",
"glCopyTexSubImage2D",
"glCopyTexSubImage3D",
"glCopyTextureImage1DEXT",
"glCopyTextureImage2DEXT",
"glCopyTextureSubImage1D",
"glCopyTextureSubImage1DEXT",
"glCopyTextureSubImage2D",
"glCopyTextureSubImage2DEXT",
"glCopyTextureSubImage3D",
"glCopyTextureSubImage3DEXT",
"glCoverageModulationNV",
"glCoverageModulationTableNV",
"glCoverFillPathInstancedNV",
"glCoverFillPathNV",
"glCoverStrokePathInstancedNV",
"glCoverStrokePathNV",
"glCreateBuffers",
"glCreateCommandListsNV",
"glCreateFramebuffers",
"glCreatePerfQueryINTEL",
"glCreateProgram",
"glCreateProgramPipelines",
"glCreateQueries",
"glCreateRenderbuffers",
"glCreateSamplers",
"glCreateShader",
"glCreateShaderProgramEXT",
"glCreateShaderProgramv",
"glCreateShaderProgramvEXT",
"glCreateStatesNV",
"glCreateSyncFromCLeventARB",
"glCreateTextures",
"glCreateTransformFeedbacks",
"glCreateVertexArrays",
"glCullFace",
"glDebugMessageCallback",
"glDebugMessageCallbackARB",
"glDebugMessageCallbackKHR",
"glDebugMessageControl",
"glDebugMessageControlARB",
"glDebugMessageControlKHR",
"glDebugMessageInsert",
"glDebugMessageInsertARB",
"glDebugMessageInsertKHR",
"glDeleteBuffers",
"glDeleteCommandListsNV",
"glDeleteFramebuffers",
"glDeleteNamedStringARB",
"glDeletePathsNV",
"glDeletePerfMonitorsAMD",
"glDeletePerfQueryINTEL",
"glDeleteProgram",
"glDeleteProgramPipelines",
"glDeleteProgramPipelinesEXT",
"glDeleteQueries",
"glDeleteRenderbuffers",
"glDeleteSamplers",
"glDeleteShader",
"glDeleteStatesNV",
"glDeleteSync",
"glDeleteTextures",
"glDeleteTransformFeedbacks",
"glDeleteVertexArrays",
"glDepthFunc",
"glDepthMask",
"glDepthRange",
"glDepthRangeArrayv",
"glDepthRangef",
"glDepthRangeIndexed",
"glDetachShader",
"glDisable",
"glDisableClientStateiEXT",
"glDisableClientStateIndexedEXT",
"glDisablei",
"glDisableIndexedEXT",
"glDisableVertexArrayAttrib",
"glDisableVertexArrayAttribEXT",
"glDisableVertexArrayEXT",
"glDisableVertexAttribArray",
"glDispatchCompute",
"glDispatchComputeGroupSizeARB",
"glDispatchComputeIndirect",
"glDrawArrays",
"glDrawArraysIndirect",
"glDrawArraysInstanced",
"glDrawArraysInstancedARB",
"glDrawArraysInstancedBaseInstance",
"glDrawArraysInstancedEXT",
"glDrawBuffer",
"glDrawBuffers",
"glDrawCommandsAddressNV",
"glDrawCommandsNV",
"glDrawCommandsStatesAddressNV",
"glDrawCommandsStatesNV",
"glDrawElements",
"glDrawElementsBaseVertex",
"glDrawElementsIndirect",
"glDrawElementsInstanced",
"glDrawElementsInstancedARB",
"glDrawElementsInstancedBaseInstance",
"glDrawElementsInstancedBaseVertex",
"glDrawElementsInstancedBaseVertexBaseInstance",
"glDrawElementsInstancedEXT",
"glDrawRangeElements",
"glDrawRangeElementsBaseVertex",
"glDrawTransformFeedback",
"glDrawTransformFeedbackInstanced",
"glDrawTransformFeedbackStream",
"glDrawTransformFeedbackStreamInstanced",
"glDrawVkImageNV",
"glEdgeFlagFormatNV",
"glEnable",
"glEnableClientStateiEXT",
"glEnableClientStateIndexedEXT",
"glEnablei",
"glEnableIndexedEXT",
"glEnableVertexArrayAttrib",
"glEnableVertexArrayAttribEXT",
"glEnableVertexArrayEXT",
"glEnableVertexAttribArray",
"glEndConditionalRender",
"glEndConditionalRenderNV",
"glEndPerfMonitorAMD",
"glEndPerfQueryINTEL",
"glEndQuery",
"glEndQueryIndexed",
"glEndTransformFeedback",
"glEvaluateDepthValuesARB",
"glFenceSync",
"glFinish",
"glFlush",
"glFlushMappedBufferRange",
"glFlushMappedNamedBufferRange",
"glFlushMappedNamedBufferRangeEXT",
"glFogCoordFormatNV",
"glFragmentCoverageColorNV",
"glFramebufferDrawBufferEXT",
"glFramebufferDrawBuffersEXT",
"glFramebufferParameteri",
"glFramebufferReadBufferEXT",
"glFramebufferRenderbuffer",
"glFramebufferSampleLocationsfvARB",
"glFramebufferSampleLocationsfvNV",
"glFramebufferTexture",
"glFramebufferTexture1D",
"glFramebufferTexture2D",
"glFramebufferTexture3D",
"glFramebufferTextureARB",
"glFramebufferTextureFaceARB",
"glFramebufferTextureLayer",
"glFramebufferTextureLayerARB",
"glFramebufferTextureMultiviewOVR",
"glFrontFace",
"glGenBuffers",
"glGenerateMipmap",
"glGenerateMultiTexMipmapEXT",
"glGenerateTextureMipmap",
"glGenerateTextureMipmapEXT",
"glGenFramebuffers",
"glGenPathsNV",
"glGenPerfMonitorsAMD",
"glGenProgramPipelines",
"glGenProgramPipelinesEXT",
"glGenQueries",
"glGenRenderbuffers",
"glGenSamplers",
"glGenTextures",
"glGenTransformFeedbacks",
"glGenVertexArrays",
"glGetActiveAtomicCounterBufferiv",
"glGetActiveAttrib",
"glGetActiveSubroutineName",
"glGetActiveSubroutineUniformiv",
"glGetActiveSubroutineUniformName",
"glGetActiveUniform",
"glGetActiveUniformBlockiv",
"glGetActiveUniformBlockName",
"glGetActiveUniformName",
"glGetActiveUniformsiv",
"glGetAttachedShaders",
"glGetAttribLocation",
"glGetBooleani_v",
"glGetBooleanIndexedvEXT",
"glGetBooleanv",
"glGetBufferParameteri64v",
"glGetBufferParameteriv",
"glGetBufferParameterui64vNV",
"glGetBufferPointerv",
"glGetBufferSubData",
"glGetColorTable",
"glGetColorTableParameterfv",
"glGetColorTableParameteriv",
"glGetCommandHeaderNV",
"glGetCompressedMultiTexImageEXT",
"glGetCompressedTexImage",
"glGetCompressedTextureImage",
"glGetCompressedTextureImageEXT",
"glGetCompressedTextureSubImage",
"glGetConvolutionFilter",
"glGetConvolutionParameterfv",
"glGetConvolutionParameteriv",
"glGetCoverageModulationTableNV",
"glGetDebugMessageLog",
"glGetDebugMessageLogARB",
"glGetDebugMessageLogKHR",
"glGetDoublei_v",
"glGetDoublei_vEXT",
"glGetDoubleIndexedvEXT",
"glGetDoublev",
"glGetError",
"glGetFirstPerfQueryIdINTEL",
"glGetFloati_v",
"glGetFloati_vEXT",
"glGetFloatIndexedvEXT",
"glGetFloatv",
"glGetFragDataIndex",
"glGetFragDataLocation",
"glGetFramebufferAttachmentParameteriv",
"glGetFramebufferParameteriv",
"glGetFramebufferParameterivEXT",
"glGetGraphicsResetStatus",
"glGetGraphicsResetStatusARB",
"glGetGraphicsResetStatusKHR",
"glGetHistogram",
"glGetHistogramParameterfv",
"glGetHistogramParameteriv",
"glGetImageHandleARB",
"glGetImageHandleNV",
"glGetInteger64i_v",
"glGetInteger64v",
"glGetIntegeri_v",
"glGetIntegerIndexedvEXT",
"glGetIntegerui64i_vNV",
"glGetIntegerui64vNV",
"glGetIntegerv",
"glGetInternalformati64v",
"glGetInternalformativ",
"glGetInternalformatSampleivNV",
"glGetMinmax",
"glGetMinmaxParameterfv",
"glGetMinmaxParameteriv",
"glGetMultisamplefv",
"glGetMultiTexEnvfvEXT",
"glGetMultiTexEnvivEXT",
"glGetMultiTexGendvEXT",
"glGetMultiTexGenfvEXT",
"glGetMultiTexGenivEXT",
"glGetMultiTexImageEXT",
"glGetMultiTexLevelParameterfvEXT",
"glGetMultiTexLevelParameterivEXT",
"glGetMultiTexParameterfvEXT",
"glGetMultiTexParameterIivEXT",
"glGetMultiTexParameterIuivEXT",
"glGetMultiTexParameterivEXT",
"glGetNamedBufferParameteri64v",
"glGetNamedBufferParameteriv",
"glGetNamedBufferParameterivEXT",
"glGetNamedBufferParameterui64vNV",
"glGetNamedBufferPointerv",
"glGetNamedBufferPointervEXT",
"glGetNamedBufferSubData",
"glGetNamedBufferSubDataEXT",
"glGetNamedFramebufferAttachmentParameteriv",
"glGetNamedFramebufferAttachmentParameterivEXT",
"glGetNamedFramebufferParameteriv",
"glGetNamedFramebufferParameterivEXT",
"glGetNamedProgramivEXT",
"glGetNamedProgramLocalParameterdvEXT",
"glGetNamedProgramLocalParameterfvEXT",
"glGetNamedProgramLocalParameterIivEXT",
"glGetNamedProgramLocalParameterIuivEXT",
"glGetNamedProgramStringEXT",
"glGetNamedRenderbufferParameteriv",
"glGetNamedRenderbufferParameterivEXT",
"glGetNamedStringARB",
"glGetNamedStringivARB",
"glGetnColorTable",
"glGetnColorTableARB",
"glGetnCompressedTexImage",
"glGetnCompressedTexImageARB",
"glGetnConvolutionFilter",
"glGetnConvolutionFilterARB",
"glGetNextPerfQueryIdINTEL",
"glGetnHistogram",
"glGetnHistogramARB",
"glGetnMapdv",
"glGetnMapdvARB",
"glGetnMapfv",
"glGetnMapfvARB",
"glGetnMapiv",
"glGetnMapivARB",
"glGetnMinmax",
"glGetnMinmaxARB",
"glGetnPixelMapfv",
"glGetnPixelMapfvARB",
"glGetnPixelMapuiv",
"glGetnPixelMapuivARB",
"glGetnPixelMapusv",
"glGetnPixelMapusvARB",
"glGetnPolygonStipple",
"glGetnPolygonStippleARB",
"glGetnSeparableFilter",
"glGetnSeparableFilterARB",
"glGetnTexImage",
"glGetnTexImageARB",
"glGetnUniformdv",
"glGetnUniformdvARB",
"glGetnUniformfv",
"glGetnUniformfvARB",
"glGetnUniformfvKHR",
"glGetnUniformi64vARB",
"glGetnUniformiv",
"glGetnUniformivARB",
"glGetnUniformivKHR",
"glGetnUniformui64vARB",
"glGetnUniformuiv",
"glGetnUniformuivARB",
"glGetnUniformuivKHR",
"glGetObjectLabel",
"glGetObjectLabelEXT",
"glGetObjectLabelKHR",
"glGetObjectPtrLabel",
"glGetObjectPtrLabelKHR",
"glGetPathColorGenfvNV",
"glGetPathColorGenivNV",
"glGetPathCommandsNV",
"glGetPathCoordsNV",
"glGetPathDashArrayNV",
"glGetPathLengthNV",
"glGetPathMetricRangeNV",
"glGetPathMetricsNV",
"glGetPathParameterfvNV",
"glGetPathParameterivNV",
"glGetPathSpacingNV",
"glGetPathTexGenfvNV",
"glGetPathTexGenivNV",
"glGetPerfCounterInfoINTEL",
"glGetPerfMonitorCounterDataAMD",
"glGetPerfMonitorCounterInfoAMD",
"glGetPerfMonitorCountersAMD",
"glGetPerfMonitorCounterStringAMD",
"glGetPerfMonitorGroupsAMD",
"glGetPerfMonitorGroupStringAMD",
"glGetPerfQueryDataINTEL",
"glGetPerfQueryIdByNameINTEL",
"glGetPerfQueryInfoINTEL",
"glGetPointeri_vEXT",
"glGetPointerIndexedvEXT",
"glGetPointerv",
"glGetPointervKHR",
"glGetProgramBinary",
"glGetProgramInfoLog",
"glGetProgramInterfaceiv",
"glGetProgramiv",
"glGetProgramPipelineInfoLog",
"glGetProgramPipelineInfoLogEXT",
"glGetProgramPipelineiv",
"glGetProgramPipelineivEXT",
"glGetProgramResourcefvNV",
"glGetProgramResourceIndex",
"glGetProgramResourceiv",
"glGetProgramResourceLocation",
"glGetProgramResourceLocationIndex",
"glGetProgramResourceName",
"glGetProgramStageiv",
"glGetQueryBufferObjecti64v",
"glGetQueryBufferObjectiv",
"glGetQueryBufferObjectui64v",
"glGetQueryBufferObjectuiv",
"glGetQueryIndexediv",
"glGetQueryiv",
"glGetQueryObjecti64v",
"glGetQueryObjectiv",
"glGetQueryObjectui64v",
"glGetQueryObjectuiv",
"glGetRenderbufferParameteriv",
"glGetSamplerParameterfv",
"glGetSamplerParameterIiv",
"glGetSamplerParameterIuiv",
"glGetSamplerParameteriv",
"glGetSeparableFilter",
"glGetShaderInfoLog",
"glGetShaderiv",
"glGetShaderPrecisionFormat",
"glGetShaderSource",
"glGetStageIndexNV",
"glGetString",
"glGetStringi",
"glGetSubroutineIndex",
"glGetSubroutineUniformLocation",
"glGetSynciv",
"glGetTexImage",
"glGetTexLevelParameterfv",
"glGetTexLevelParameteriv",
"glGetTexParameterfv",
"glGetTexParameterIiv",
"glGetTexParameterIuiv",
"glGetTexParameteriv",
"glGetTextureHandleARB",
"glGetTextureHandleNV",
"glGetTextureImage",
"glGetTextureImageEXT",
"glGetTextureLevelParameterfv",
"glGetTextureLevelParameterfvEXT",
"glGetTextureLevelParameteriv",
"glGetTextureLevelParameterivEXT",
"glGetTextureParameterfv",
"glGetTextureParameterfvEXT",
"glGetTextureParameterIiv",
"glGetTextureParameterIivEXT",
"glGetTextureParameterIuiv",
"glGetTextureParameterIuivEXT",
"glGetTextureParameteriv",
"glGetTextureParameterivEXT",
"glGetTextureSamplerHandleARB",
"glGetTextureSamplerHandleNV",
"glGetTextureSubImage",
"glGetTransformFeedbacki_v",
"glGetTransformFeedbacki64_v",
"glGetTransformFeedbackiv",
"glGetTransformFeedbackVarying",
"glGetUniformBlockIndex",
"glGetUniformdv",
"glGetUniformfv",
"glGetUniformi64vARB",
"glGetUniformi64vNV",
"glGetUniformIndices",
"glGetUniformiv",
"glGetUniformLocation",
"glGetUniformSubroutineuiv",
"glGetUniformui64vARB",
"glGetUniformui64vNV",
"glGetUniformuiv",
"glGetVertexArrayIndexed64iv",
"glGetVertexArrayIndexediv",
"glGetVertexArrayIntegeri_vEXT",
"glGetVertexArrayIntegervEXT",
"glGetVertexArrayiv",
"glGetVertexArrayPointeri_vEXT",
"glGetVertexArrayPointervEXT",
"glGetVertexAttribdv",
"glGetVertexAttribfv",
"glGetVertexAttribIiv",
"glGetVertexAttribIuiv",
"glGetVertexAttribiv",
"glGetVertexAttribLdv",
"glGetVertexAttribLi64vNV",
"glGetVertexAttribLui64vARB",
"glGetVertexAttribLui64vNV",
"glGetVertexAttribPointerv",
"glGetVkProcAddrNV",
"glHint",
"glHistogram",
"glIndexFormatNV",
"glInsertEventMarkerEXT",
"glInterpolatePathsNV",
"glInvalidateBufferData",
"glInvalidateBufferSubData",
"glInvalidateFramebuffer",
"glInvalidateNamedFramebufferData",
"glInvalidateNamedFramebufferSubData",
"glInvalidateSubFramebuffer",
"glInvalidateTexImage",
"glInvalidateTexSubImage",
"glIsBuffer",
"glIsBufferResidentNV",
"glIsCommandListNV",
"glIsEnabled",
"glIsEnabledi",
"glIsEnabledIndexedEXT",
"glIsFramebuffer",
"glIsImageHandleResidentARB",
"glIsImageHandleResidentNV",
"glIsNamedBufferResidentNV",
"glIsNamedStringARB",
"glIsPathNV",
"glIsPointInFillPathNV",
"glIsPointInStrokePathNV",
"glIsProgram",
"glIsProgramPipeline",
"glIsProgramPipelineEXT",
"glIsQuery",
"glIsRenderbuffer",
"glIsSampler",
"glIsShader",
"glIsStateNV",
"glIsSync",
"glIsTexture",
"glIsTextureHandleResidentARB",
"glIsTextureHandleResidentNV",
"glIsTransformFeedback",
"glIsVertexArray",
"glLabelObjectEXT",
"glLineWidth",
"glLinkProgram",
"glListDrawCommandsStatesClientNV",
"glLogicOp",
"glMakeBufferNonResidentNV",
"glMakeBufferResidentNV",
"glMakeImageHandleNonResidentARB",
"glMakeImageHandleNonResidentNV",
"glMakeImageHandleResidentARB",
"glMakeImageHandleResidentNV",
"glMakeNamedBufferNonResidentNV",
"glMakeNamedBufferResidentNV",
"glMakeTextureHandleNonResidentARB",
"glMakeTextureHandleNonResidentNV",
"glMakeTextureHandleResidentARB",
"glMakeTextureHandleResidentNV",
"glMapBuffer",
"glMapBufferRange",
"glMapNamedBuffer",
"glMapNamedBufferEXT",
"glMapNamedBufferRange",
"glMapNamedBufferRangeEXT",
"glMatrixFrustumEXT",
"glMatrixLoad3x2fNV",
"glMatrixLoad3x3fNV",
"glMatrixLoaddEXT",
"glMatrixLoadfEXT",
"glMatrixLoadIdentityEXT",
"glMatrixLoadTranspose3x3fNV",
"glMatrixLoadTransposedEXT",
"glMatrixLoadTransposefEXT",
"glMatrixMult3x2fNV",
"glMatrixMult3x3fNV",
"glMatrixMultdEXT",
"glMatrixMultfEXT",
"glMatrixMultTranspose3x3fNV",
"glMatrixMultTransposedEXT",
"glMatrixMultTransposefEXT",
"glMatrixOrthoEXT",
"glMatrixPopEXT",
"glMatrixPushEXT",
"glMatrixRotatedEXT",
"glMatrixRotatefEXT",
"glMatrixScaledEXT",
"glMatrixScalefEXT",
"glMatrixTranslatedEXT",
"glMatrixTranslatefEXT",
"glMaxShaderCompilerThreadsARB",
"glMaxShaderCompilerThreadsKHR",
"glMemoryBarrier",
"glMemoryBarrierByRegion",
"glMinmax",
"glMinSampleShading",
"glMinSampleShadingARB",
"glMultiDrawArrays",
"glMultiDrawArraysIndirect",
"glMultiDrawArraysIndirectBindlessCountNV",
"glMultiDrawArraysIndirectBindlessNV",
"glMultiDrawArraysIndirectCount",
"glMultiDrawArraysIndirectCountARB",
"glMultiDrawElements",
"glMultiDrawElementsBaseVertex",
"glMultiDrawElementsIndirect",
"glMultiDrawElementsIndirectBindlessCountNV",
"glMultiDrawElementsIndirectBindlessNV",
"glMultiDrawElementsIndirectCount",
"glMultiDrawElementsIndirectCountARB",
"glMultiTexBufferEXT",
"glMultiTexCoordP1ui",
"glMultiTexCoordP1uiv",
"glMultiTexCoordP2ui",
"glMultiTexCoordP2uiv",
"glMultiTexCoordP3ui",
"glMultiTexCoordP3uiv",
"glMultiTexCoordP4ui",
"glMultiTexCoordP4uiv",
"glMultiTexCoordPointerEXT",
"glMultiTexEnvfEXT",
"glMultiTexEnvfvEXT",
"glMultiTexEnviEXT",
"glMultiTexEnvivEXT",
"glMultiTexGendEXT",
"glMultiTexGendvEXT",
"glMultiTexGenfEXT",
"glMultiTexGenfvEXT",
"glMultiTexGeniEXT",
"glMultiTexGenivEXT",
"glMultiTexImage1DEXT",
"glMultiTexImage2DEXT",
"glMultiTexImage3DEXT",
"glMultiTexParameterfEXT",
"glMultiTexParameterfvEXT",
"glMultiTexParameteriEXT",
"glMultiTexParameterIivEXT",
"glMultiTexParameterIuivEXT",
"glMultiTexParameterivEXT",
"glMultiTexRenderbufferEXT",
"glMultiTexSubImage1DEXT",
"glMultiTexSubImage2DEXT",
"glMultiTexSubImage3DEXT",
"glNamedBufferData",
"glNamedBufferDataEXT",
"glNamedBufferPageCommitmentARB",
"glNamedBufferPageCommitmentEXT",
"glNamedBufferStorage",
"glNamedBufferStorageEXT",
"glNamedBufferSubData",
"glNamedBufferSubDataEXT",
"glNamedCopyBufferSubDataEXT",
"glNamedFramebufferDrawBuffer",
"glNamedFramebufferDrawBuffers",
"glNamedFramebufferParameteri",
"glNamedFramebufferParameteriEXT",
"glNamedFramebufferReadBuffer",
"glNamedFramebufferRenderbuffer",
"glNamedFramebufferRenderbufferEXT",
"glNamedFramebufferSampleLocationsfvARB",
"glNamedFramebufferSampleLocationsfvNV",
"glNamedFramebufferTexture",
"glNamedFramebufferTexture1DEXT",
"glNamedFramebufferTexture2DEXT",
"glNamedFramebufferTexture3DEXT",
"glNamedFramebufferTextureEXT",
"glNamedFramebufferTextureFaceEXT",
"glNamedFramebufferTextureLayer",
"glNamedFramebufferTextureLayerEXT",
"glNamedProgramLocalParameter4dEXT",
"glNamedProgramLocalParameter4dvEXT",
"glNamedProgramLocalParameter4fEXT",
"glNamedProgramLocalParameter4fvEXT",
"glNamedProgramLocalParameterI4iEXT",
"glNamedProgramLocalParameterI4ivEXT",
"glNamedProgramLocalParameterI4uiEXT",
"glNamedProgramLocalParameterI4uivEXT",
"glNamedProgramLocalParameters4fvEXT",
"glNamedProgramLocalParametersI4ivEXT",
"glNamedProgramLocalParametersI4uivEXT",
"glNamedProgramStringEXT",
"glNamedRenderbufferStorage",
"glNamedRenderbufferStorageEXT",
"glNamedRenderbufferStorageMultisample",
"glNamedRenderbufferStorageMultisampleCoverageEXT",
"glNamedRenderbufferStorageMultisampleEXT",
"glNamedStringARB",
"glNormalFormatNV",
"glNormalP3ui",
"glNormalP3uiv",
"glObjectLabel",
"glObjectLabelKHR",
"glObjectPtrLabel",
"glObjectPtrLabelKHR",
"glPatchParameterfv",
"glPatchParameteri",
"glPathColorGenNV",
"glPathCommandsNV",
"glPathCoordsNV",
"glPathCoverDepthFuncNV",
"glPathDashArrayNV",
"glPathFogGenNV",
"glPathGlyphIndexArrayNV",
"glPathGlyphIndexRangeNV",
"glPathGlyphRangeNV",
"glPathGlyphsNV",
"glPathMemoryGlyphIndexArrayNV",
"glPathParameterfNV",
"glPathParameterfvNV",
"glPathParameteriNV",
"glPathParameterivNV",
"glPathStencilDepthOffsetNV",
"glPathStencilFuncNV",
"glPathStringNV",
"glPathSubCommandsNV",
"glPathSubCoordsNV",
"glPathTexGenNV",
"glPauseTransformFeedback",
"glPixelStoref",
"glPixelStorei",
"glPointAlongPathNV",
"glPointParameterf",
"glPointParameterfv",
"glPointParameteri",
"glPointParameteriv",
"glPointSize",
"glPolygonMode",
"glPolygonOffset",
"glPolygonOffsetClamp",
"glPolygonOffsetClampEXT",
"glPopDebugGroup",
"glPopDebugGroupKHR",
"glPopGroupMarkerEXT",
"glPrimitiveBoundingBoxARB",
"glPrimitiveRestartIndex",
"glProgramBinary",
"glProgramParameteri",
"glProgramParameteriARB",
"glProgramParameteriEXT",
"glProgramPathFragmentInputGenNV",
"glProgramUniform1d",
"glProgramUniform1dEXT",
"glProgramUniform1dv",
"glProgramUniform1dvEXT",
"glProgramUniform1f",
"glProgramUniform1fEXT",
"glProgramUniform1fv",
"glProgramUniform1fvEXT",
"glProgramUniform1i",
"glProgramUniform1i64ARB",
"glProgramUniform1i64NV",
"glProgramUniform1i64vARB",
"glProgramUniform1i64vNV",
"glProgramUniform1iEXT",
"glProgramUniform1iv",
"glProgramUniform1ivEXT",
"glProgramUniform1ui",
"glProgramUniform1ui64ARB",
"glProgramUniform1ui64NV",
"glProgramUniform1ui64vARB",
"glProgramUniform1ui64vNV",
"glProgramUniform1uiEXT",
"glProgramUniform1uiv",
"glProgramUniform1uivEXT",
"glProgramUniform2d",
"glProgramUniform2dEXT",
"glProgramUniform2dv",
"glProgramUniform2dvEXT",
"glProgramUniform2f",
"glProgramUniform2fEXT",
"glProgramUniform2fv",
"glProgramUniform2fvEXT",
"glProgramUniform2i",
"glProgramUniform2i64ARB",
"glProgramUniform2i64NV",
"glProgramUniform2i64vARB",
"glProgramUniform2i64vNV",
"glProgramUniform2iEXT",
"glProgramUniform2iv",
"glProgramUniform2ivEXT",
"glProgramUniform2ui",
"glProgramUniform2ui64ARB",
"glProgramUniform2ui64NV",
"glProgramUniform2ui64vARB",
"glProgramUniform2ui64vNV",
"glProgramUniform2uiEXT",
"glProgramUniform2uiv",
"glProgramUniform2uivEXT",
"glProgramUniform3d",
"glProgramUniform3dEXT",
"glProgramUniform3dv",
"glProgramUniform3dvEXT",
"glProgramUniform3f",
"glProgramUniform3fEXT",
"glProgramUniform3fv",
"glProgramUniform3fvEXT",
"glProgramUniform3i",
"glProgramUniform3i64ARB",
"glProgramUniform3i64NV",
"glProgramUniform3i64vARB",
"glProgramUniform3i64vNV",
"glProgramUniform3iEXT",
"glProgramUniform3iv",
"glProgramUniform3ivEXT",
"glProgramUniform3ui",
"glProgramUniform3ui64ARB",
"glProgramUniform3ui64NV",
"glProgramUniform3ui64vARB",
"glProgramUniform3ui64vNV",
"glProgramUniform3uiEXT",
"glProgramUniform3uiv",
"glProgramUniform3uivEXT",
"glProgramUniform4d",
"glProgramUniform4dEXT",
"glProgramUniform4dv",
"glProgramUniform4dvEXT",
"glProgramUniform4f",
"glProgramUniform4fEXT",
"glProgramUniform4fv",
"glProgramUniform4fvEXT",
"glProgramUniform4i",
"glProgramUniform4i64ARB",
"glProgramUniform4i64NV",
"glProgramUniform4i64vARB",
"glProgramUniform4i64vNV",
"glProgramUniform4iEXT",
"glProgramUniform4iv",
"glProgramUniform4ivEXT",
"glProgramUniform4ui",
"glProgramUniform4ui64ARB",
"glProgramUniform4ui64NV",
"glProgramUniform4ui64vARB",
"glProgramUniform4ui64vNV",
"glProgramUniform4uiEXT",
"glProgramUniform4uiv",
"glProgramUniform4uivEXT",
"glProgramUniformHandleui64ARB",
"glProgramUniformHandleui64NV",
"glProgramUniformHandleui64vARB",
"glProgramUniformHandleui64vNV",
"glProgramUniformMatrix2dv",
"glProgramUniformMatrix2dvEXT",
"glProgramUniformMatrix2fv",
"glProgramUniformMatrix2fvEXT",
"glProgramUniformMatrix2x3dv",
"glProgramUniformMatrix2x3dvEXT",
"glProgramUniformMatrix2x3fv",
"glProgramUniformMatrix2x3fvEXT",
"glProgramUniformMatrix2x4dv",
"glProgramUniformMatrix2x4dvEXT",
"glProgramUniformMatrix2x4fv",
"glProgramUniformMatrix2x4fvEXT",
"glProgramUniformMatrix3dv",
"glProgramUniformMatrix3dvEXT",
"glProgramUniformMatrix3fv",
"glProgramUniformMatrix3fvEXT",
"glProgramUniformMatrix3x2dv",
"glProgramUniformMatrix3x2dvEXT",
"glProgramUniformMatrix3x2fv",
"glProgramUniformMatrix3x2fvEXT",
"glProgramUniformMatrix3x4dv",
"glProgramUniformMatrix3x4dvEXT",
"glProgramUniformMatrix3x4fv",
"glProgramUniformMatrix3x4fvEXT",
"glProgramUniformMatrix4dv",
"glProgramUniformMatrix4dvEXT",
"glProgramUniformMatrix4fv",
"glProgramUniformMatrix4fvEXT",
"glProgramUniformMatrix4x2dv",
"glProgramUniformMatrix4x2dvEXT",
"glProgramUniformMatrix4x2fv",
"glProgramUniformMatrix4x2fvEXT",
"glProgramUniformMatrix4x3dv",
"glProgramUniformMatrix4x3dvEXT",
"glProgramUniformMatrix4x3fv",
"glProgramUniformMatrix4x3fvEXT",
"glProgramUniformui64NV",
"glProgramUniformui64vNV",
"glProvokingVertex",
"glPushClientAttribDefaultEXT",
"glPushDebugGroup",
"glPushDebugGroupKHR",
"glPushGroupMarkerEXT",
"glQueryCounter",
"glRasterSamplesEXT",
"glReadBuffer",
"glReadnPixels",
"glReadnPixelsARB",
"glReadnPixelsKHR",
"glReadPixels",
"glReleaseShaderCompiler",
"glRenderbufferStorage",
"glRenderbufferStorageMultisample",
"glRenderbufferStorageMultisampleCoverageNV",
"glResetHistogram",
"glResetMinmax",
"glResolveDepthValuesNV",
"glResumeTransformFeedback",
"glSampleCoverage",
"glSampleMaski",
"glSamplerParameterf",
"glSamplerParameterfv",
"glSamplerParameteri",
"glSamplerParameterIiv",
"glSamplerParameterIuiv",
"glSamplerParameteriv",
"glScissor",
"glScissorArrayv",
"glScissorIndexed",
"glScissorIndexedv",
"glSecondaryColorFormatNV",
"glSecondaryColorP3ui",
"glSecondaryColorP3uiv",
"glSelectPerfMonitorCountersAMD",
"glSeparableFilter2D",
"glShaderBinary",
"glShaderSource",
"glShaderStorageBlockBinding",
"glSignalVkFenceNV",
"glSignalVkSemaphoreNV",
"glSpecializeShader",
"glSpecializeShaderARB",
"glStateCaptureNV",
"glStencilFillPathInstancedNV",
"glStencilFillPathNV",
"glStencilFunc",
"glStencilFuncSeparate",
"glStencilMask",
"glStencilMaskSeparate",
"glStencilOp",
"glStencilOpSeparate",
"glStencilStrokePathInstancedNV",
"glStencilStrokePathNV",
"glStencilThenCoverFillPathInstancedNV",
"glStencilThenCoverFillPathNV",
"glStencilThenCoverStrokePathInstancedNV",
"glStencilThenCoverStrokePathNV",
"glSubpixelPrecisionBiasNV",
"glTexBuffer",
"glTexBufferARB",
"glTexBufferRange",
"glTexCoordFormatNV",
"glTexCoordP1ui",
"glTexCoordP1uiv",
"glTexCoordP2ui",
"glTexCoordP2uiv",
"glTexCoordP3ui",
"glTexCoordP3uiv",
"glTexCoordP4ui",
"glTexCoordP4uiv",
"glTexImage1D",
"glTexImage2D",
"glTexImage2DMultisample",
"glTexImage3D",
"glTexImage3DMultisample",
"glTexPageCommitmentARB",
"glTexParameterf",
"glTexParameterfv",
"glTexParameteri",
"glTexParameterIiv",
"glTexParameterIuiv",
"glTexParameteriv",
"glTexStorage1D",
"glTexStorage2D",
"glTexStorage2DMultisample",
"glTexStorage3D",
"glTexStorage3DMultisample",
"glTexSubImage1D",
"glTexSubImage2D",
"glTexSubImage3D",
"glTextureBarrier",
"glTextureBarrierNV",
"glTextureBuffer",
"glTextureBufferEXT",
"glTextureBufferRange",
"glTextureBufferRangeEXT",
"glTextureImage1DEXT",
"glTextureImage2DEXT",
"glTextureImage3DEXT",
"glTexturePageCommitmentEXT",
"glTextureParameterf",
"glTextureParameterfEXT",
"glTextureParameterfv",
"glTextureParameterfvEXT",
"glTextureParameteri",
"glTextureParameteriEXT",
"glTextureParameterIiv",
"glTextureParameterIivEXT",
"glTextureParameterIuiv",
"glTextureParameterIuivEXT",
"glTextureParameteriv",
"glTextureParameterivEXT",
"glTextureRenderbufferEXT",
"glTextureStorage1D",
"glTextureStorage1DEXT",
"glTextureStorage2D",
"glTextureStorage2DEXT",
"glTextureStorage2DMultisample",
"glTextureStorage2DMultisampleEXT",
"glTextureStorage3D",
"glTextureStorage3DEXT",
"glTextureStorage3DMultisample",
"glTextureStorage3DMultisampleEXT",
"glTextureSubImage1D",
"glTextureSubImage1DEXT",
"glTextureSubImage2D",
"glTextureSubImage2DEXT",
"glTextureSubImage3D",
"glTextureSubImage3DEXT",
"glTextureView",
"glTransformFeedbackBufferBase",
"glTransformFeedbackBufferRange",
"glTransformFeedbackVaryings",
"glTransformPathNV",
"glUniform1d",
"glUniform1dv",
"glUniform1f",
"glUniform1fv",
"glUniform1i",
"glUniform1i64ARB",
"glUniform1i64NV",
"glUniform1i64vARB",
"glUniform1i64vNV",
"glUniform1iv",
"glUniform1ui",
"glUniform1ui64ARB",
"glUniform1ui64NV",
"glUniform1ui64vARB",
"glUniform1ui64vNV",
"glUniform1uiv",
"glUniform2d",
"glUniform2dv",
"glUniform2f",
"glUniform2fv",
"glUniform2i",
"glUniform2i64ARB",
"glUniform2i64NV",
"glUniform2i64vARB",
"glUniform2i64vNV",
"glUniform2iv",
"glUniform2ui",
"glUniform2ui64ARB",
"glUniform2ui64NV",
"glUniform2ui64vARB",
"glUniform2ui64vNV",
"glUniform2uiv",
"glUniform3d",
"glUniform3dv",
"glUniform3f",
"glUniform3fv",
"glUniform3i",
"glUniform3i64ARB",
"glUniform3i64NV",
"glUniform3i64vARB",
"glUniform3i64vNV",
"glUniform3iv",
"glUniform3ui",
"glUniform3ui64ARB",
"glUniform3ui64NV",
"glUniform3ui64vARB",
"glUniform3ui64vNV",
"glUniform3uiv",
"glUniform4d",
"glUniform4dv",
"glUniform4f",
"glUniform4fv",
"glUniform4i",
"glUniform4i64ARB",
"glUniform4i64NV",
"glUniform4i64vARB",
"glUniform4i64vNV",
"glUniform4iv",
"glUniform4ui",
"glUniform4ui64ARB",
"glUniform4ui64NV",
"glUniform4ui64vARB",
"glUniform4ui64vNV",
"glUniform4uiv",
"glUniformBlockBinding",
"glUniformHandleui64ARB",
"glUniformHandleui64NV",
"glUniformHandleui64vARB",
"glUniformHandleui64vNV",
"glUniformMatrix2dv",
"glUniformMatrix2fv",
"glUniformMatrix2x3dv",
"glUniformMatrix2x3fv",
"glUniformMatrix2x4dv",
"glUniformMatrix2x4fv",
"glUniformMatrix3dv",
"glUniformMatrix3fv",
"glUniformMatrix3x2dv",
"glUniformMatrix3x2fv",
"glUniformMatrix3x4dv",
"glUniformMatrix3x4fv",
"glUniformMatrix4dv",
"glUniformMatrix4fv",
"glUniformMatrix4x2dv",
"glUniformMatrix4x2fv",
"glUniformMatrix4x3dv",
"glUniformMatrix4x3fv",
"glUniformSubroutinesuiv",
"glUniformui64NV",
"glUniformui64vNV",
"glUnmapBuffer",
"glUnmapNamedBuffer",
"glUnmapNamedBufferEXT",
"glUseProgram",
"glUseProgramStages",
"glUseProgramStagesEXT",
"glUseShaderProgramEXT",
"glValidateProgram",
"glValidateProgramPipeline",
"glValidateProgramPipelineEXT",
"glVertexArrayAttribBinding",
"glVertexArrayAttribFormat",
"glVertexArrayAttribIFormat",
"glVertexArrayAttribLFormat",
"glVertexArrayBindingDivisor",
"glVertexArrayBindVertexBufferEXT",
"glVertexArrayColorOffsetEXT",
"glVertexArrayEdgeFlagOffsetEXT",
"glVertexArrayElementBuffer",
"glVertexArrayFogCoordOffsetEXT",
"glVertexArrayIndexOffsetEXT",
"glVertexArrayMultiTexCoordOffsetEXT",
"glVertexArrayNormalOffsetEXT",
"glVertexArraySecondaryColorOffsetEXT",
"glVertexArrayTexCoordOffsetEXT",
"glVertexArrayVertexAttribBindingEXT",
"glVertexArrayVertexAttribDivisorEXT",
"glVertexArrayVertexAttribFormatEXT",
"glVertexArrayVertexAttribIFormatEXT",
"glVertexArrayVertexAttribIOffsetEXT",
"glVertexArrayVertexAttribLFormatEXT",
"glVertexArrayVertexAttribLOffsetEXT",
"glVertexArrayVertexAttribOffsetEXT",
"glVertexArrayVertexBindingDivisorEXT",
"glVertexArrayVertexBuffer",
"glVertexArrayVertexBuffers",
"glVertexArrayVertexOffsetEXT",
"glVertexAttrib1d",
"glVertexAttrib1dv",
"glVertexAttrib1f",
"glVertexAttrib1fv",
"glVertexAttrib1s",
"glVertexAttrib1sv",
"glVertexAttrib2d",
"glVertexAttrib2dv",
"glVertexAttrib2f",
"glVertexAttrib2fv",
"glVertexAttrib2s",
"glVertexAttrib2sv",
"glVertexAttrib3d",
"glVertexAttrib3dv",
"glVertexAttrib3f",
"glVertexAttrib3fv",
"glVertexAttrib3s",
"glVertexAttrib3sv",
"glVertexAttrib4bv",
"glVertexAttrib4d",
"glVertexAttrib4dv",
"glVertexAttrib4f",
"glVertexAttrib4fv",
"glVertexAttrib4iv",
"glVertexAttrib4Nbv",
"glVertexAttrib4Niv",
"glVertexAttrib4Nsv",
"glVertexAttrib4Nub",
"glVertexAttrib4Nubv",
"glVertexAttrib4Nuiv",
"glVertexAttrib4Nusv",
"glVertexAttrib4s",
"glVertexAttrib4sv",
"glVertexAttrib4ubv",
"glVertexAttrib4uiv",
"glVertexAttrib4usv",
"glVertexAttribBinding",
"glVertexAttribDivisor",
"glVertexAttribDivisorARB",
"glVertexAttribFormat",
"glVertexAttribFormatNV",
"glVertexAttribI1i",
"glVertexAttribI1iv",
"glVertexAttribI1ui",
"glVertexAttribI1uiv",
"glVertexAttribI2i",
"glVertexAttribI2iv",
"glVertexAttribI2ui",
"glVertexAttribI2uiv",
"glVertexAttribI3i",
"glVertexAttribI3iv",
"glVertexAttribI3ui",
"glVertexAttribI3uiv",
"glVertexAttribI4bv",
"glVertexAttribI4i",
"glVertexAttribI4iv",
"glVertexAttribI4sv",
"glVertexAttribI4ubv",
"glVertexAttribI4ui",
"glVertexAttribI4uiv",
"glVertexAttribI4usv",
"glVertexAttribIFormat",
"glVertexAttribIFormatNV",
"glVertexAttribIPointer",
"glVertexAttribL1d",
"glVertexAttribL1dv",
"glVertexAttribL1i64NV",
"glVertexAttribL1i64vNV",
"glVertexAttribL1ui64ARB",
"glVertexAttribL1ui64NV",
"glVertexAttribL1ui64vARB",
"glVertexAttribL1ui64vNV",
"glVertexAttribL2d",
"glVertexAttribL2dv",
"glVertexAttribL2i64NV",
"glVertexAttribL2i64vNV",
"glVertexAttribL2ui64NV",
"glVertexAttribL2ui64vNV",
"glVertexAttribL3d",
"glVertexAttribL3dv",
"glVertexAttribL3i64NV",
"glVertexAttribL3i64vNV",
"glVertexAttribL3ui64NV",
"glVertexAttribL3ui64vNV",
"glVertexAttribL4d",
"glVertexAttribL4dv",
"glVertexAttribL4i64NV",
"glVertexAttribL4i64vNV",
"glVertexAttribL4ui64NV",
"glVertexAttribL4ui64vNV",
"glVertexAttribLFormat",
"glVertexAttribLFormatNV",
"glVertexAttribLPointer",
"glVertexAttribP1ui",
"glVertexAttribP1uiv",
"glVertexAttribP2ui",
"glVertexAttribP2uiv",
"glVertexAttribP3ui",
"glVertexAttribP3uiv",
"glVertexAttribP4ui",
"glVertexAttribP4uiv",
"glVertexAttribPointer",
"glVertexBindingDivisor",
"glVertexFormatNV",
"glVertexP2ui",
"glVertexP2uiv",
"glVertexP3ui",
"glVertexP3uiv",
"glVertexP4ui",
"glVertexP4uiv",
"glViewport",
"glViewportArrayv",
"glViewportIndexedf",
"glViewportIndexedfv",
"glViewportPositionWScaleNV",
"glViewportSwizzleNV",
"glWaitSync",
"glWaitVkSemaphoreNV",
"glWeightPathsNV",
"glWindowRectanglesEXT"
            };

            EntryPoints = new IntPtr[EntryPointNames.Length];
        }

        public static System.Delegate GetFunc(string funcName, Type funcType)
        {
            var idx = Array.IndexOf(EntryPointNames, funcName);
            if (idx < 0)
            {
                throw new Exception("not support function " + funcName);
            }
            if (EntryPoints[idx] == IntPtr.Zero)
            {
                LoadFuncAddress(idx);
            }
            var intPtr = EntryPoints[idx];
            return Marshal.GetDelegateForFunctionPointer(intPtr, funcType);
        }
        public static Func<IntPtr, IntPtr> GetAddressFunc;

        public static void LoadFuncAddress(int i)
        {
            try
            {
                var funcName = EntryPointNames[i];// + "\0"
                var ptrStr = System.Runtime.InteropServices.Marshal.StringToHGlobalAnsi(funcName);
                var ptr = GetAddressFunc(ptrStr);
                EntryPoints[i] = ptr;
            }
            catch
            {
                throw;
            }
        }

        public static string[] Supports;
        public static bool getSupport(string funcName)
        {
            if (Supports == null)
            {
                var items = new List<string>();
                var extNum = gl.GetInteger(GetPName.NumExtensions);
                for (var i = 0; i < extNum; i++)
                {
                    var str = GetStringIdx((int)EXTENSIONS, i);
                    items.Add(str);
                }
                Supports = items.ToArray();
            }
            return Array.IndexOf(Supports, funcName) >= 0;
        }
        public static String GetVersion()
        {
            return GetString(gl.VERSION);
        }
        /// <summary>[requires: v1.0]
        /// Return a string describing the current GL connection
        /// </summary>
        /// <param name="name">
        /// Specifies a symbolic constant, one of Vendor, Renderer, Version, or ShadingLanguageVersion. Additionally, glGetStringi accepts the Extensions token.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetString")]
        private delegate IntPtr delegate_GetString(int name);
        private static delegate_GetString func_GetString;
        public static String GetString(int name)
        {
            if (func_GetString == null)
            {
                func_GetString = (delegate_GetString)GetFunc("glGetString", typeof(delegate_GetString));
            }
            var ptr = func_GetString(name);
            return Marshal.PtrToStringAnsi(ptr);
        }
        public static String GetString(StringName name)
        {
            return GetString((int)name);
        }

        /// <summary>[requires: v3.0]
        /// Return a string describing the current GL connection
        /// </summary>
        /// <param name="name">
        /// Specifies a symbolic constant, one of Vendor, Renderer, Version, or ShadingLanguageVersion. Additionally, glGetStringi accepts the Extensions token.
        /// </param>
        /// <param name="index">
        /// For glGetStringi, specifies the index of the string to return.
        /// </param>
       // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glGetStringi")]
        private delegate IntPtr delegate_GetStringi(int name, Int32 index);
        private static delegate_GetStringi func_GetStringi;
        public static IntPtr GetStringi(int name, Int32 index)
        {
            if (func_GetStringi == null)
            {
                func_GetStringi = (delegate_GetStringi)GetFunc("glGetStringi", typeof(delegate_GetStringi));
            }
            return func_GetStringi(name, index);
        }
        public static String GetStringIdx(int name, Int32 index)
        {
            var ptr = GetStringi(name, index);
            return Marshal.PtrToStringAnsi(ptr);
        }
        public static String GetStringi(StringNameIndexed name, Int32 index)
        {
            return GetStringIdx((int)name, index);
        }


        private delegate void delegate_ActiveTexture(int texture);
        private static delegate_ActiveTexture func_ActiveTexture;
        /// <summary>[requires: v1.3] EntryPoint = "glActiveTexture"
        /// Select active texture unit
        /// </summary>
        /// <param name="texture">
        /// Specifies which texture unit to make active. The number of texture units is implementation dependent, but must be at least 80. texture must be one of Texturei, where i ranges from zero to the value of MaxCombinedTextureImageUnits minus one. The initial value is Texture0.
        /// </param>
        public static void activeTexture(int texture)
        {
            if (func_ActiveTexture == null)
            {
                func_ActiveTexture = (delegate_ActiveTexture)GetFunc("glActiveTexture", typeof(delegate_ActiveTexture));
            }
            func_ActiveTexture(texture);

            if (DebugEnabled) DebugMethod(null, texture);

        }
        public static void activeTexture(TextureUnit texture)
        {
            if (func_ActiveTexture == null)
            {
                func_ActiveTexture = (delegate_ActiveTexture)GetFunc("glActiveTexture", typeof(delegate_ActiveTexture));
            }
            func_ActiveTexture((int)texture);
        }


        private delegate void delegate_AttachShader(Int32 program, Int32 shader);
        private static delegate_AttachShader func_AttachShader;
        /// <summary>[requires: v2.0]
        /// Attaches a shader object to a program object
        /// </summary>
        /// <param name="program">
        /// Specifies the program object to which a shader object will be attached.
        /// </param>
        /// <param name="shader">
        /// Specifies the shader object that is to be attached.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glAttachShader")]
        [CLSCompliant(false)]
        public static void attachShader(Int32 program, Int32 shader)
        {
            if (func_AttachShader == null)
            {
                func_AttachShader = (delegate_AttachShader)GetFunc("glAttachShader", typeof(delegate_AttachShader));
            }
            func_AttachShader(program, shader);

            if (DebugEnabled) DebugMethod(null, program,shader);
        }


        private delegate void delegate_BindBuffer(int target, int buffer);
        private static delegate_BindBuffer func_BindBuffer;
        /// <summary>[requires: v1.5]
        /// Bind a named buffer object
        /// </summary>
        /// <param name="target">
        /// Specifies the target to which the buffer object is bound. The symbolic constant must be ArrayBuffer, AtomicCounterBuffer, CopyReadBuffer, CopyWriteBuffer, DrawIndirectBuffer, DispatchIndirectBuffer, ElementArrayBuffer, PixelPackBuffer, PixelUnpackBuffer, QueryBuffer, ShaderStorageBuffer, TextureBuffer, TransformFeedbackBuffer, or UniformBuffer.
        /// </param>
        /// <param name="buffer">
        /// Specifies the name of a buffer object.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_5", Version = "1.5", EntryPoint = "glBindBuffer")]
        [CLSCompliant(false)]
        public static void bindBuffer(int target, Int32 buffer)
        {
            if (func_BindBuffer == null)
            {
                func_BindBuffer = (delegate_BindBuffer)GetFunc("glBindBuffer", typeof(delegate_BindBuffer));
            }
            func_BindBuffer(target, buffer);
            if (DebugEnabled) DebugMethod(null, target, buffer);
        }
        public static void bindBuffer(BufferTarget target, Int32 buffer)
        {
            if (func_BindBuffer == null)
            {
                func_BindBuffer = (delegate_BindBuffer)GetFunc("glBindBuffer", typeof(delegate_BindBuffer));
            }
            func_BindBuffer((int)target, buffer);
        }

        private delegate void delegate_BindBufferBase(int target, int index, int buffer);
        private static delegate_BindBufferBase func_BindBufferBase;
        /// <summary>[requires: v3.0 or ARB_uniform_buffer_object|VERSION_3_0|VERSION_3_1]
        /// Bind a buffer object to an indexed buffer target
        /// </summary>
        /// <param name="target">
        /// Specify the target of the bind operation. target must be one of AtomicCounterBuffer, TransformFeedbackBuffer, UniformBuffer or ShaderStorageBuffer.
        /// </param>
        /// <param name="index">
        /// Specify the index of the binding point within the array specified by target.
        /// </param>
        /// <param name="buffer">
        /// The name of a buffer object to bind to the specified binding point.
        /// </param>
        // [AutoGenerated(Category = "ARB_uniform_buffer_object|VERSION_3_0|VERSION_3_1", Version = "3.0", EntryPoint = "glBindBufferBase")]
        [CLSCompliant(false)]
        public static void bindBufferBase(int target, Int32 index, Int32 buffer)
        {
            if (func_BindBufferBase == null)
            {
                func_BindBufferBase = (delegate_BindBufferBase)GetFunc("glBindBufferBase", typeof(delegate_BindBufferBase));
            }
            func_BindBufferBase(target, index, buffer);
            if (DebugEnabled) DebugMethod(null, target,index, buffer);
        }
        public static void bindBufferBase(BufferTarget target, Int32 index, Int32 buffer)
        {
            if (func_BindBufferBase == null)
            {
                func_BindBufferBase = (delegate_BindBufferBase)GetFunc("glBindBufferBase", typeof(delegate_BindBufferBase));
            }
            func_BindBufferBase((int)target, index, buffer);
        }

        /// <summary>[requires: v1.1]
        /// Bind a named texture to a texturing target
        /// </summary>
        /// <param name="target">
        /// Specifies the target to which the texture is bound. Must be one of Texture1D, Texture2D, Texture3D, Texture1DArray, Texture2DArray, TextureRectangle, TextureCubeMap, TextureCubeMapArray, TextureBuffer, Texture2DMultisample or Texture2DMultisampleArray.
        /// </param>
        /// <param name="texture">
        /// Specifies the name of a texture.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glBindTexture")]
        private delegate void delegate_BindTexture(int target, Int32 texture);
        private static delegate_BindTexture func_BindTexture;
        [CLSCompliant(false)]
        public static void bindTexture(int target, Int32 texture)
        {
            if (func_BindTexture == null)
            {
                func_BindTexture = (delegate_BindTexture)GetFunc("glBindTexture", typeof(delegate_BindTexture));
            }
            func_BindTexture(target, texture);
            if (DebugEnabled) DebugMethod(null, target, texture);
        }
        public static void bindTexture(TextureTarget target, Int32 texture)
        {
            if (func_BindTexture == null)
            {
                func_BindTexture = (delegate_BindTexture)GetFunc("glBindTexture", typeof(delegate_BindTexture));
            }
            func_BindTexture((int)target, texture);
        }





        /// <summary>[requires: v3.0 or ARB_vertex_array_object|VERSION_3_0]
        /// Bind a vertex array object
        /// </summary>
        /// <param name="array">
        /// Specifies the name of the vertex array to bind.
        /// </param>
        // [AutoGenerated(Category = "ARB_vertex_array_object|VERSION_3_0", Version = "3.0", EntryPoint = "glBindVertexArray")]
        private delegate void delegate_BindVertexArray(int array);
        private static delegate_BindVertexArray func_BindVertexArray;
        [CLSCompliant(false)]
        public static void BindVertexArray(Int32 array)
        {
            if (func_BindVertexArray == null)
            {
                func_BindVertexArray = (delegate_BindVertexArray)GetFunc("glBindVertexArray", typeof(delegate_BindVertexArray));
            }
            func_BindVertexArray((int)array);
            if (DebugEnabled) DebugMethod(null, array);
        }



        /// <summary>[requires: v1.0]
        /// Specify pixel arithmetic
        /// </summary>
        /// <param name="sfactor">
        /// Specifies how the red, green, blue, and alpha source blending factors are computed. The initial value is One.
        /// </param>
        /// <param name="dfactor">
        /// Specifies how the red, green, blue, and alpha destination blending factors are computed. The following symbolic constants are accepted: Zero, One, SrcColor, OneMinusSrcColor, DstColor, OneMinusDstColor, SrcAlpha, OneMinusSrcAlpha, DstAlpha, OneMinusDstAlpha. ConstantColor, OneMinusConstantColor, ConstantAlpha, and OneMinusConstantAlpha. The initial value is Zero.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glBlendFunc")]
        private delegate void delegate_BlendFunc(int sfactor, int dfactor);
        private static delegate_BlendFunc func_BlendFunc;
        public static void blendFunc(int sfactor, int dfactor)
        {
            if (func_BlendFunc == null)
            {
                func_BlendFunc = (delegate_BlendFunc)GetFunc("glBlendFunc", typeof(delegate_BlendFunc));
            }
            func_BlendFunc(sfactor, dfactor);
            if (DebugEnabled) DebugMethod(null, sfactor, dfactor);
        }
        public static void blendFunc(BlendingFactor sfactor, BlendingFactor dfactor)
        {
            blendFunc((int)sfactor, (int)dfactor);

        }

        /// <summary>[requires: v2.0]
        /// Set the RGB blend equation and the alpha blend equation separately
        /// </summary>
        /// <param name="modeRGB">
        /// specifies the RGB blend equation, how the red, green, and blue components of the source and destination colors are combined. It must be FuncAdd, FuncSubtract, FuncReverseSubtract, Min, Max.
        /// </param>
        /// <param name="modeAlpha">
        /// specifies the alpha blend equation, how the alpha component of the source and destination colors are combined. It must be FuncAdd, FuncSubtract, FuncReverseSubtract, Min, Max.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glBlendEquationSeparate")]
        private delegate void delegate_BlendEquationSeparate(int modeRGB, int modeAlpha);
        private static delegate_BlendEquationSeparate func_BlendEquationSeparate;
        public static void blendEquationSeparate(int modeRGB, int modeAlpha)
        {
            if (func_BlendEquationSeparate == null)
            {
                func_BlendEquationSeparate = (delegate_BlendEquationSeparate)GetFunc("glBlendEquationSeparate", typeof(delegate_BlendEquationSeparate));
            }
            func_BlendEquationSeparate(modeRGB, modeAlpha);
            if (DebugEnabled) DebugMethod(null, modeRGB, modeAlpha);

        }

        public static void blendEquationSeparate(BlendEquationMode modeRGB, BlendEquationMode modeAlpha)
        {
            blendEquationSeparate((int)modeRGB, (int)modeAlpha);
        }



        /// <summary>[requires: v1.5]
        /// Creates and initializes a buffer object's data store
        /// </summary>
        /// <param name="target">
        /// Specifies the target buffer object. The symbolic constant must be ArrayBuffer, AtomicCounterBuffer, CopyReadBuffer, CopyWriteBuffer, DrawIndirectBuffer, DispatchIndirectBuffer, ElementArrayBuffer, PixelPackBuffer, PixelUnpackBuffer, QueryBuffer, ShaderStorageBuffer, TextureBuffer, TransformFeedbackBuffer, or UniformBuffer.
        /// </param>
        /// <param name="size">
        /// Specifies the size in bytes of the buffer object's new data store.
        /// </param>
        /// <param name="data">[length: size]
        /// Specifies a pointer to data that will be copied into the data store for initialization, or Null if no data is to be copied.
        /// </param>
        /// <param name="usage">
        /// Specifies the expected usage pattern of the data store. The symbolic constant must be StreamDraw, StreamRead, StreamCopy, StaticDraw, StaticRead, StaticCopy, DynamicDraw, DynamicRead, or DynamicCopy.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_5", Version = "1.5", EntryPoint = "glBufferData")]
        private delegate void delegate_BufferData(int target, int size, IntPtr data, int usage);
        private static delegate_BufferData func_BufferData;
        public static void bufferData(int target, int size, IntPtr data, int usage)
        {
            if (func_BufferData == null)
            {
                func_BufferData = (delegate_BufferData)GetFunc("glBufferData", typeof(delegate_BufferData));
            }
            func_BufferData(target, size, data, usage);
            if (DebugEnabled) DebugMethod(null, target, size,data,usage).Constants(3);
        }




        /// <summary>[requires: v1.5]
        /// Updates a subset of a buffer object's data store
        /// </summary>
        /// <param name="target">
        /// Specifies the target buffer object. The symbolic constant must be ArrayBuffer, AtomicCounterBuffer, CopyReadBuffer, CopyWriteBuffer, DrawIndirectBuffer, DispatchIndirectBuffer, ElementArrayBuffer, PixelPackBuffer, PixelUnpackBuffer, QueryBuffer, ShaderStorageBuffer, TextureBuffer, TransformFeedbackBuffer, or UniformBuffer.
        /// </param>
        /// <param name="offset">
        /// Specifies the offset into the buffer object's data store where data replacement will begin, measured in bytes.
        /// </param>
        /// <param name="size">
        /// Specifies the size in bytes of the data store region being replaced.
        /// </param>
        /// <param name="data">[length: size]
        /// Specifies a pointer to the new data that will be copied into the data store.
        /// </param>
        // [AutoGenerated(Category = "VERSION_1_5", Version = "1.5", EntryPoint = "glBufferSubData")]
        private delegate void delegate_BufferSubData(int target, int offset, int size, IntPtr data);
        private static delegate_BufferSubData func_BufferSubData;
        public static void bufferSubData(int target, int offset, int size, IntPtr data)
        {
            if (func_BufferSubData == null)
            {
                func_BufferSubData = (delegate_BufferSubData)GetFunc("glBufferSubData", typeof(delegate_BufferSubData));
            }
            func_BufferSubData(target, offset, size, data);
        }
        public static void bufferSubData(BufferTarget target, int offset, int size, IntPtr data)
        {
            bufferSubData((int)target, offset, size, data);
        }

        public static void bufferSubData(int target, int offset, int size, Array data)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                bufferSubData(target, offset, size, handle.AddrOfPinnedObject());
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, offset, size, data);
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }


        /// <summary>[requires: v1.0]
        /// Clear buffers to preset values
        /// </summary>
        /// <param name="mask">
        /// Bitwise OR of masks that indicate the buffers to be cleared. The three masks are ColorBufferBit, DepthBufferBit, and StencilBufferBit.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glClear")]
        private delegate void delegate_Clear(int mask);
        private static delegate_Clear func_Clear;
        public static void clear(int mask)
        {
            if (func_Clear == null)
            {
                func_Clear = (delegate_Clear)GetFunc("glClear", typeof(delegate_Clear));
            }
            func_Clear(mask);
            if (DebugEnabled) DebugMethod(null, mask);
        }

        public static void clear(ClearBufferMask mask)
        {
            clear((int)mask);
        }


        /// <summary>[requires: v1.0]
        /// Specify clear values for the color buffers
        /// </summary>
        /// <param name="red">
        /// Specify the red, green, blue, and alpha values used when the color buffers are cleared. The initial values are all 0.
        /// </param>
        /// <param name="green">
        /// Specify the red, green, blue, and alpha values used when the color buffers are cleared. The initial values are all 0.
        /// </param>
        /// <param name="blue">
        /// Specify the red, green, blue, and alpha values used when the color buffers are cleared. The initial values are all 0.
        /// </param>
        /// <param name="alpha">
        /// Specify the red, green, blue, and alpha values used when the color buffers are cleared. The initial values are all 0.
        /// </param>
        // [AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glClearColor")]
        private delegate Boolean delegate_ClearColor(Single red, Single green, Single blue, Single alpha);
        private static delegate_ClearColor func_ClearColor;
        public static void clearColor(Single red, Single green, Single blue, Single alpha)
        {
            if (func_ClearColor == null)
            {
                func_ClearColor = (delegate_ClearColor)GetFunc("glClearColor", typeof(delegate_ClearColor));
            }
            func_ClearColor(red, green, blue, alpha);
            if (DebugEnabled) DebugMethod(null, red,green,blue,alpha);

        }




        /// <summary>[requires: v2.0]
        /// Compiles a shader object
        /// </summary>
        /// <param name="shader">
        /// Specifies the shader object to be compiled.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glCompileShader")]
        private delegate void delegate_CompileShader(Int32 shader);
        private static delegate_CompileShader func_CompileShader;
        [CLSCompliant(false)]
        public static void compileShader(Int32 shader)
        {
            if (func_CompileShader == null)
            {
                func_CompileShader = (delegate_CompileShader)GetFunc("glCompileShader", typeof(delegate_CompileShader));
            }
            func_CompileShader(shader);
            if (DebugEnabled) DebugMethod(null, shader);
        }




        /// <summary>[requires: v2.0]
        /// Creates a program object
        /// </summary>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glCreateProgram")]
        private delegate Int32 delegate_CreateProgram();
        private static delegate_CreateProgram func_CreateProgram;
        public static Int32 createProgram()
        {

            if (func_CreateProgram == null)
            {
                func_CreateProgram = (delegate_CreateProgram)GetFunc("glCreateProgram", typeof(delegate_CreateProgram));
            }
            var result= func_CreateProgram();

            if (DebugEnabled) DebugMethod(result);
            return result;
        }




        /// <summary>[requires: v4.1 or ARB_separate_shader_objects|VERSION_4_1]
        /// Create a stand-alone program from an array of null-terminated source code strings
        /// </summary>
        /// <param name="type">
        /// Specifies the type of shader to create.
        /// </param>
        /// <param name="count">
        /// Specifies the number of source code strings in the array strings.
        /// </param>
        /// <param name="strings">[length: count]
        /// Specifies the address of an array of pointers to source code strings from which to create the program object.
        /// </param>
        //[AutoGenerated(Category = "ARB_separate_shader_objects|VERSION_4_1", Version = "4.1", EntryPoint = "glCreateShaderProgramv")]
        private delegate Int32 delegate_CreateShaderProgram(ShaderType type, Int32 count, [CountAttribute(Parameter = "count")] String[] strings);
        private static delegate_CreateShaderProgram func_CreateShaderProgram;
        public static Int32 CreateShaderProgram(ShaderType type, Int32 count, String[] strings)
        {
            if (func_CreateShaderProgram == null)
            {
                func_CreateShaderProgram = (delegate_CreateShaderProgram)GetFunc("glCreateShaderProgramv", typeof(delegate_CreateShaderProgram));
            }
            return func_CreateShaderProgram(type, count, strings);
        }




        /// <summary>[requires: v2.0]
        /// Creates a shader object
        /// </summary>
        /// <param name="type">
        /// Specifies the type of shader to be created. Must be one of ComputeShader, VertexShader, TessControlShader, TessEvaluationShader, GeometryShader, or FragmentShader.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glCreateShader")]
        private delegate Int32 delegate_CreateShader(int type);
        private static delegate_CreateShader func_CreateShader;
        public static Int32 createShader(int type)
        {
            if (func_CreateShader == null)
            {
                func_CreateShader = (delegate_CreateShader)GetFunc("glCreateShader", typeof(delegate_CreateShader));
            }
            var result= func_CreateShader(type);
            if (DebugEnabled) DebugMethod(result,type);
            return result;
        }

        public static Int32 createShader(ShaderType type)
        {

            return createShader((int)type);
        }


        /// <summary>[requires: v1.5]
        /// Delete named buffer objects
        /// </summary>
        /// <param name="buffers">[length: n]
        /// Specifies an array of buffer objects to be deleted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_5", Version = "1.5", EntryPoint = "glDeleteBuffers")]
        private unsafe delegate void delegate_DeleteBuffers(Int32 n, Int32* buffers);
        private static delegate_DeleteBuffers func_DeleteBuffers;
        [CLSCompliant(false)]
        public unsafe static void deleteBuffers(Int32 n, Int32* buffers)
        {
            if (func_DeleteBuffers == null)
            {
                func_DeleteBuffers = (delegate_DeleteBuffers)GetFunc("glDeleteBuffers", typeof(delegate_DeleteBuffers));
            }
            func_DeleteBuffers(n, buffers);
        }



        /// <summary>[requires: v2.0]
        /// Deletes a program object
        /// </summary>
        /// <param name="program">
        /// Specifies the program object to be deleted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glDeleteProgram")]
        private delegate void delegate_DeleteProgram(Int32 program);
        private static delegate_DeleteProgram func_DeleteProgram;
        [CLSCompliant(false)]
        public static void deleteProgram(Int32 program)
        {
            if (func_DeleteProgram == null)
            {
                func_DeleteProgram = (delegate_DeleteProgram)GetFunc("glDeleteProgram", typeof(delegate_DeleteProgram));
            }
            func_DeleteProgram(program);
            if (DebugEnabled) DebugMethod(null, program);
        }



        /// <summary>[requires: v2.0]
        /// Deletes a shader object
        /// </summary>
        /// <param name="shader">
        /// Specifies the shader object to be deleted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glDeleteShader")]
        private delegate void delegate_DeleteShader(Int32 shader);
        private static delegate_DeleteShader func_DeleteShader;
        [CLSCompliant(false)]
        public static void deleteShader(Int32 shader)
        {
            if (func_DeleteShader == null)
            {
                func_DeleteShader = (delegate_DeleteShader)GetFunc("glDeleteShader", typeof(delegate_DeleteShader));
            }
            func_DeleteShader(shader);
            if (DebugEnabled) DebugMethod(null, shader);
        }



        /// <summary>[requires: v1.1]
        /// Delete named textures
        /// </summary>
        /// <param name="n">
        /// Specifies the number of textures to be deleted.
        /// </param>
        /// <param name="textures">[length: n]
        /// Specifies an array of textures to be deleted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glDeleteTextures")]
        private unsafe delegate void delegate_DeleteTextures(Int32 n, Int32* textures);
        private static delegate_DeleteTextures func_DeleteTextures;
        [CLSCompliant(false)]
        public static unsafe void deleteTextures(Int32 n, Int32* textures)
        {
            if (func_DeleteTextures == null)
            {
                func_DeleteTextures = (delegate_DeleteTextures)GetFunc("glDeleteTextures", typeof(delegate_DeleteTextures));
            }
            func_DeleteTextures(n, textures);
        }
        /// <summary>[requires: v1.1]
        /// Delete named textures
        /// </summary>
        /// <param name="textures">[length: n]
        /// Specifies an array of textures to be deleted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glDeleteTextures")]
        public static void deleteTexture(Int32 texture)
        {
            deleteTextures(1, texture);
            if (DebugEnabled) DebugMethod(null, texture);
        }


        /// <summary>[requires: v1.1]
        /// Delete named textures
        /// </summary>
        /// <param name="n">
        /// Specifies the number of textures to be deleted.
        /// </param>
        /// <param name="textures">[length: n]
        /// Specifies an array of textures to be deleted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glDeleteTextures")]
        public static void deleteTextures(Int32 n, Int32[] textures)
        {
            unsafe
            {
                fixed (Int32* p_textures = textures)
                {
                    deleteTextures(n, p_textures);
                }
            }
        }

        /// <summary>[requires: v1.1]
        /// Delete named textures
        /// </summary>
        /// <param name="n">
        /// Specifies the number of textures to be deleted.
        /// </param>
        /// <param name="textures">[length: n]
        /// Specifies an array of textures to be deleted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glDeleteTextures")]
        public static void deleteTextures(Int32 n, Int32 texture)
        {
            var arr = new int[1] { texture };
            deleteTextures(n, arr);
        }


        /// <summary>[requires: v2.0]
        /// Detaches a shader object from a program object to which it is attached
        /// </summary>
        /// <param name="program">
        /// Specifies the program object from which to detach the shader object.
        /// </param>
        /// <param name="shader">
        /// Specifies the shader object to be detached.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glDetachShader")]
        private delegate void delegate_DetachShader(Int32 program, Int32 shader);
        private static delegate_DetachShader func_DetachShader;
        [CLSCompliant(false)]
        public static void DetachShader(Int32 program, Int32 shader)
        {
            if (func_DetachShader == null)
            {
                func_DetachShader = (delegate_DetachShader)GetFunc("glDetachShader", typeof(delegate_DetachShader));
            }
            func_DetachShader(program, shader);
        }



        /// <summary>[requires: v1.0]</summary>
        /// <param name="cap"></param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glDisable")]
        private delegate void delegate_Disable(int cap);
        private static delegate_Disable func_Disable;
        public static void disable(int cap)
        {
            if (func_Disable == null)
            {
                func_Disable = (delegate_Disable)GetFunc("glDisable", typeof(delegate_Disable));
            }
            func_Disable(cap);
            if (DebugEnabled) DebugMethod(null, cap).Constants(0);
        }
        public static void disable(EnableCap cap)
        {
            disable((int)cap);
        }

        /// <summary>[requires: v3.0]</summary>
        /// <param name="target"></param>
        /// <param name="index"></param>
        //[AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glDisablei")]
        private delegate void delegate_Disablei(IndexedEnableCap cap, Int32 index);
        private static delegate_Disablei func_Disablei;
        [CLSCompliant(false)]
        public static void Disable(IndexedEnableCap cap, Int32 index)
        {
            if (func_Disablei == null)
            {
                func_Disablei = (delegate_Disablei)GetFunc("glDisablei", typeof(delegate_Disablei));
            }
            func_Disablei(cap, index);
        }



        /// <summary>[requires: v1.1]
        /// Render primitives from array data
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency and Patches are accepted.
        /// </param>
        /// <param name="first">
        /// Specifies the starting index in the enabled arrays.
        /// </param>
        /// <param name="count">
        /// Specifies the number of indices to be rendered.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glDrawArrays")]
        private delegate void delegate_DrawArrays(int mode, Int32 first, Int32 count);
        private static delegate_DrawArrays func_DrawArrays;
        public static void drawArrays(int mode, Int32 first, Int32 count)
        {
            if (func_DrawArrays == null)
            {
                func_DrawArrays = (delegate_DrawArrays)GetFunc("glDrawArrays", typeof(delegate_DrawArrays));
            }
            func_DrawArrays(mode, first, count);
            if (DebugEnabled) DebugMethod(null, mode, first,count).Constants(0);
        }

        public static void drawArrays(PrimitiveType mode, Int32 first, Int32 count)
        {
            drawArrays((int)mode, first, count);
        }

        /// <summary>[requires: v1.1]
        /// Render primitives from array data
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency and Patches are accepted.
        /// </param>
        /// <param name="count">
        /// Specifies the number of elements to be rendered.
        /// </param>
        /// <param name="type">
        /// Specifies the type of the values in indices. Must be one of UnsignedByte, UnsignedShort, or UnsignedInt.
        /// </param>
        /// <param name="indices">[length: COMPSIZE(count,type)]
        /// Specifies a pointer to the location where the indices are stored.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glDrawElements")]
        private delegate void delegate_DrawElements(int mode, Int32 count, int type, IntPtr indices);
        private static delegate_DrawElements func_DrawElements;
        public static void drawElements(int mode, Int32 count, int type, IntPtr indices)
        {
            if (func_DrawElements == null)
            {
                func_DrawElements = (delegate_DrawElements)GetFunc("glDrawElements", typeof(delegate_DrawElements));
            }
            func_DrawElements(mode, count, type, indices);
            if (DebugEnabled) DebugMethod(null, mode, count, type, indices).Constants(0);
        }

        public static void drawElements(PrimitiveType mode, Int32 count, DrawElementsType type, IntPtr indices)
        {
            drawElements((int)mode, count, (int)type, indices);
        }

        public static void drawElements(int mode, Int32 count, int type, int indexCount)
        {
            drawElements((int)mode, count, (int)type, (IntPtr)indexCount);
        }
        /// <summary>[requires: v1.1]
        /// Render primitives from array data
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency and Patches are accepted.
        /// </param>
        /// <param name="count">
        /// Specifies the number of elements to be rendered.
        /// </param>
        /// <param name="type">
        /// Specifies the type of the values in indices. Must be one of UnsignedByte, UnsignedShort, or UnsignedInt.
        /// </param>
        /// <param name="indices">[length: COMPSIZE(count,type)]
        /// Specifies a pointer to the location where the indices are stored.
        /// </param>
        [AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glDrawElements")]
        public static void drawElements(PrimitiveType mode, Int32 count, DrawElementsType type, Array indices)
        {
            var handle = GCHandle.Alloc(indices, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                drawElements(mode, count, type, handle.AddrOfPinnedObject());
                handle.Free();
            }
            else
            {
                throw new Exception("buffer data is not allocated.");
            }
        }




        /// <summary>[requires: v1.0]
        /// Enable or disable server-side GL capabilities
        /// </summary>
        /// <param name="cap">
        /// Specifies a symbolic constant indicating a GL capability.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glEnable")]
        private delegate void delegate_Enable(int cap);
        private static delegate_Enable func_Enable;
        public static void enable(int cap)
        {
            if (func_Enable == null)
            {
                func_Enable = (delegate_Enable)GetFunc("glEnable", typeof(delegate_Enable));
            }
            func_Enable(cap);
            if (DebugEnabled) DebugMethod(null, cap).Constants(0);
        }

        public static void enable(EnableCap cap)
        {
            enable((int)cap);
        }


        /// <summary>[requires: v3.0]
        /// Enable or disable server-side GL capabilities
        /// </summary>
        /// <param name="target">
        /// Specifies a symbolic constant indicating a GL capability.
        /// </param>
        /// <param name="index">
        /// Specifies the index of the switch to disable (for glEnablei and glDisablei only).
        /// </param>
        //[AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glEnablei")]
        private delegate void delegate_Enablei(IndexedEnableCap target, Int32 index);
        private static delegate_Enablei func_Enablei;
        [CLSCompliant(false)]
        public static void Enable(IndexedEnableCap target, Int32 index)
        {
            if (func_Enablei == null)
            {
                func_Enablei = (delegate_Enablei)GetFunc("glEnablei", typeof(delegate_Enablei));
            }
            func_Enablei(target, index);
        }


        /// <summary>[requires: v2.0]
        /// Enable or disable a generic vertex attribute array
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be enabled or disabled.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glEnableVertexAttribArray")]
        private delegate void delegate_EnableVertexAttribArray(Int32 index);
        private static delegate_EnableVertexAttribArray func_EnableVertexAttribArray;
        public static void enableVertexAttribArray(Int32 index)
        {
            if (func_EnableVertexAttribArray == null)
            {
                func_EnableVertexAttribArray = (delegate_EnableVertexAttribArray)GetFunc("glEnableVertexAttribArray", typeof(delegate_EnableVertexAttribArray));
            }
            func_EnableVertexAttribArray(index);
            if (DebugEnabled) DebugMethod(null, index);
        }



        /// <summary>[requires: v1.5]
        /// Generate buffer object names
        /// </summary>
        /// <param name="n">
        /// Specifies the number of buffer object names to be generated.
        /// </param>
        /// <param name="buffers">[length: n]
        /// Specifies an array in which the generated buffer object names are stored.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_5", Version = "1.5", EntryPoint = "glGenBuffers")]
        private unsafe delegate void delegate_GenBuffers(Int32 n, Int32* buffers);
        private static delegate_GenBuffers func_GenBuffers;
        public unsafe static void genBuffers(Int32 n, Int32* buffers)
        {
            if (func_GenBuffers == null)
            {
                func_GenBuffers = (delegate_GenBuffers)GetFunc("glGenBuffers", typeof(delegate_GenBuffers));
            }
            func_GenBuffers(n, buffers);
        }



        /// <summary>[requires: v1.1]
        /// Generate texture names
        /// </summary>
        /// <param name="n">
        /// Specifies the number of texture names to be generated.
        /// </param>
        /// <param name="textures">[length: n]
        /// Specifies an array in which the generated texture names are stored.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glGenTextures")]
        private unsafe delegate void delegate_GenTextures(Int32 n, Int32* textures);
        private static delegate_GenTextures func_GenTextures;
        [CLSCompliant(false)]
        public static unsafe void GenTextures(Int32 n, Int32* textures)
        {
            if (func_GenTextures == null)
            {
                func_GenTextures = (delegate_GenTextures)GetFunc("glGenTextures", typeof(delegate_GenTextures));
            }
            func_GenTextures(n, textures);
        }
        public static unsafe void GenTextures(int n, out int txt)
        {
            var array = new int[n];
            fixed (int* arrPtr = array)
            {
                GenTextures(n, arrPtr);
                txt = array[0];
            }
        }



        /// <summary>[requires: v3.0 or ARB_vertex_array_object|VERSION_3_0]
        /// Generate vertex array object names
        /// </summary>
        /// <param name="n">
        /// Specifies the number of vertex array object names to generate.
        /// </param>
        /// <param name="arrays">[length: n]
        /// Specifies an array in which the generated vertex array object names are stored.
        /// </param>
        // [AutoGenerated(Category = "ARB_vertex_array_object|VERSION_3_0", Version = "3.0", EntryPoint = "glGenVertexArrays")]
        private delegate void delegate_GenVertexArrays(Int32 n, Int32[] arrays);
        private static delegate_GenVertexArrays func_GenVertexArrays;
        [CLSCompliant(false)]
        public static void GenVertexArrays(Int32 n, Int32[] arrays)
        {
            if (func_GenVertexArrays == null)
            {
                func_GenVertexArrays = (delegate_GenVertexArrays)GetFunc("glGenVertexArrays", typeof(delegate_GenVertexArrays));
            }
            func_GenVertexArrays(n, arrays);
        }
        public static void GenVertexArrays(Int32 n, out int varr)
        {
            var array = new int[n];
            GenVertexArrays(n, array);
            varr = array[0];
            if (DebugEnabled) DebugMethod(null, n, varr);
        }
        private unsafe delegate void delegate_DeleteVertexArrays(int count,Int32* arrays);
        private unsafe static delegate_DeleteVertexArrays func_DeleteVertexArrays;
        public unsafe static void DeleteVertexArray(int arrays)
        {
            if (func_DeleteVertexArrays == null)
            {
                func_DeleteVertexArrays = (delegate_DeleteVertexArrays)GetFunc("glDeleteVertexArrays", typeof(delegate_DeleteVertexArrays));
            }
            var iarr = new int[1];
            iarr[0] = arrays;
            fixed (int* ptrArr = iarr)
            {
                func_DeleteVertexArrays(1,ptrArr);
            }
            if (DebugEnabled) DebugMethod(null, arrays);
        }

        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glDrawBuffers")]
        // [CLSCompliant(false)]
        // public static void DrawBuffers(Int32 n, [CountAttribute(Parameter = "n")] DrawBuffersEnum[] bufs) { throw new BindingsNotRewrittenException(); }
        private delegate void delegate_DrawBuffers(Int32 n, Int32[] buffers);
        private static delegate_DrawBuffers func_DrawBuffers;
        public static void DrawBuffers(Int32 n, Int32[] buffers)
        {
            if (func_DrawBuffers == null)
            {
                func_DrawBuffers = (delegate_DrawBuffers)GetFunc("glDrawBuffers", typeof(delegate_DrawBuffers));
            }
            func_DrawBuffers(n, buffers);
        }
        public static void drawBuffers(Int32 n, Int32[] buffers)
        {
            DrawBuffers(n, buffers);
            if (DebugEnabled) DebugMethod(null, n,buffers);
        }

        /// <summary>[requires: v2.0]
        /// Returns the location of an attribute variable
        /// </summary>
        /// <param name="program">
        /// Specifies the program object to be queried.
        /// </param>
        /// <param name="name">
        /// Points to a null terminated string containing the name of the attribute variable whose location is to be queried.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetAttribLocation")]
        private delegate Int32 delegate_GetAttribLocation(Int32 program, String name);
        private static delegate_GetAttribLocation func_GetAttribLocation;
        [CLSCompliant(false)]
        public static Int32 getAttribLocation(Int32 program, String name)
        {
            if (func_GetAttribLocation == null)
            {
                func_GetAttribLocation = (delegate_GetAttribLocation)GetFunc("glGetAttribLocation", typeof(delegate_GetAttribLocation));
            }
            var result= func_GetAttribLocation(program, name);
            if (DebugEnabled) DebugMethod(result, program, name);
            return result;
        }



        /// <summary>[requires: v1.0]</summary>
        /// <param name="pname"></param>
        /// <param name="data">[length: COMPSIZE(pname)]</param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetFloatv")]
        private unsafe delegate void delegate_GetFloat(int pname, Single* data);
        private static delegate_GetFloat func_GetFloat;
        [CLSCompliant(false)]
        public unsafe static void GetFloat(int pname, Single* data)
        {
            if (func_GetFloat == null)
            {
                func_GetFloat = (delegate_GetFloat)GetFunc("glGetFloatv", typeof(delegate_GetFloat));
            }
            func_GetFloat(pname, data);
        }
        public unsafe static void GetFloat(GetPName pname, Single* data)
        {
            GetFloat((int)pname, data);
        }
        public unsafe static void GetFloat(GetPName pname, out float data)
        {
            var array = new float[1];
            fixed (float* ptrArr = array)
            {
                GetFloat(pname, ptrArr);
                data = array[0];
            }
        }

        /// <summary>[requires: v1.0]</summary>
        /// <param name="pname"></param>
        // [AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetIntegerv")]
        [CLSCompliant(false)]
        public static Int32 GetInteger(int pname)
        {
            var data = new Int32[1];
            GetInteger(pname, data);
            return data[0];
        }
        /// <summary>[requires: v1.0]</summary>
        /// <param name="pname"></param>
        // [AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetIntegerv")]
        [CLSCompliant(false)]
        public static Int32 GetInteger(GetPName pname)
        {
            var data = new Int32[1];
            GetInteger((int)pname, data);
            return data[0];
        }

        /// <summary>[requires: v1.0]</summary>
        /// <param name="pname"></param>
        /// <param name="data">[length: COMPSIZE(pname)]</param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetIntegerv")]
        private delegate void delegate_GetInteger(int pname, Int32[] data);
        private static delegate_GetInteger func_GetInteger;
        [CLSCompliant(false)]
        public static void GetInteger(int pname, Int32[] data)
        {
            if (func_GetInteger == null)
            {
                func_GetInteger = (delegate_GetInteger)GetFunc("glGetIntegerv", typeof(delegate_GetInteger));
            }
            func_GetInteger(pname, data);
        }
        /// <summary>[requires: v1.0]</summary>
        /// <param name="pname"></param>
        /// <param name="data">[length: COMPSIZE(pname)]</param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetIntegerv")]
        [CLSCompliant(false)]
        public static void GetInteger(int pname, out Int32 data)
        {
            var arr = new int[1];
            GetInteger(pname, arr);
            data = arr[0];
        }

        /// <summary>[requires: v1.0]</summary>
        /// <param name="pname"></param>
        /// <param name="data">[length: COMPSIZE(pname)]</param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetIntegerv")]
        [CLSCompliant(false)]
        public static void GetInteger(GetPName pname, out Int32 data)
        {
            var arr = new int[1];
            GetInteger((int)pname, arr);
            data = arr[0];
        }



        /// <summary>[requires: v2.0]
        /// Returns a parameter from a program object
        /// </summary>
        /// <param name="program">
        /// Specifies the program object to be queried.
        /// </param>
        /// <param name="pname">
        /// Specifies the object parameter. Accepted symbolic names are DeleteStatus, LinkStatus, ValidateStatus, InfoLogLength, AttachedShaders, ActiveAtomicCounterBuffers, ActiveAttributes, ActiveAttributeMaxLength, ActiveUniforms, ActiveUniformBlocks, ActiveUniformBlockMaxNameLength, ActiveUniformMaxLength, ComputeWorkGroupSizeProgramBinaryLength, TransformFeedbackBufferMode, TransformFeedbackVaryings, TransformFeedbackVaryingMaxLength, GeometryVerticesOut, GeometryInputType, and GeometryOutputType.
        /// </param>
        /// <param name="@params">[length: COMPSIZE(pname)]
        /// Returns the requested object parameter.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetProgramiv")]
        private unsafe delegate void delegate_GetProgram(Int32 program, int pname, Int32* @params);
        private static delegate_GetProgram func_GetProgram;
        [CLSCompliant(false)]
        public static unsafe void GetProgramiv(Int32 program, int pname, Int32* @params)
        {
            if (func_GetProgram == null)
            {
                func_GetProgram = (delegate_GetProgram)GetFunc("glGetProgramiv", typeof(delegate_GetProgram));
            }
            func_GetProgram(program, pname, @params);
        }

        [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetProgramiv")]
        [CLSCompliant(false)]
        public static void GetProgramiv(Int32 program, GetProgramParameterName pname, Int32[] @params)
        {
            unsafe
            {
                fixed (int* p_params = @params)
                {
                    GetProgramiv(program, (int)pname, p_params);
                }
            }

        }


        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetProgramiv")]
        [CLSCompliant(false)]
        public static void GetProgramiv(Int32 program, GetProgramParameterName pname, out Int32 @params)
        {
            var args = new int[1];
            GetProgramiv(program, pname, args);
            @params = args[0];
        }

        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetProgramiv")]
        [CLSCompliant(false)]
        public static int GetProgramiv(Int32 program, int pname)
        {
            var args = new int[1];
            GetProgramiv(program, (GetProgramParameterName)pname, args);
            return args[0];
        }


        /// <summary>[requires: v2.0]
        /// Returns a parameter from a shader object
        /// </summary>
        /// <param name="shader">
        /// Specifies the shader object to be queried.
        /// </param>
        /// <param name="pname">
        /// Specifies the object parameter. Accepted symbolic names are ShaderType, DeleteStatus, CompileStatus, InfoLogLength, ShaderSourceLength.
        /// </param>
        /// <param name="@params">[length: COMPSIZE(pname)]
        /// Returns the requested object parameter.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetShaderiv")]
        private unsafe delegate void delegate_GetShader(Int32 shader, int pname, Int32* @params);
        private static delegate_GetShader func_GetShader;
        public unsafe static void getShader(Int32 shader, int pname, Int32* @params)
        {
            if (func_GetShader == null)
            {
                func_GetShader = (delegate_GetShader)GetFunc("glGetShaderiv", typeof(delegate_GetShader));
            }
            func_GetShader(shader, pname, @params);
        }

        public static unsafe void getShader(Int32 shader, int pname, int[] @params)
        {
            fixed (int* ptr = @params)
            {
                getShader(shader, pname, ptr);
            }
        }
        public static void getShader(Int32 shader, ShaderParameter pname, int[] @params)
        {
            getShader(shader, (int)pname, @params);
        }
        public static void getShader(Int32 shader, ShaderParameter pname, out int @params)
        {
            var parr = new int[1];
            getShader(shader, (int)pname, parr);
            @params = parr[0];
        }


        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetShaderSource")]
        // [CLSCompliant(false)]
        private unsafe delegate void delegate_GetShaderSource(Int32 shader, Int32 bufSize, out Int32 length, out String source);
        private static delegate_GetShaderSource func_GetShaderSource;
        public static void GetShaderSource(Int32 shader, Int32 bufSize, out Int32 length, out String source)
        {
            if (func_GetShaderSource == null)
            {
                func_GetShaderSource = (delegate_GetShaderSource)GetFunc("glGetShaderSource", typeof(delegate_GetShaderSource));
            }
            func_GetShaderSource(shader, bufSize, out length, out source);
        }
        public static string getShaderSource(Int32 shader, Int32 bufSize = 100)
        {
            GetShaderSource(shader, bufSize, out var length, out var source);
            if (DebugEnabled) DebugMethod(source, shader, bufSize);
            return source;
        }
        /// <summary>[requires: v4.1 or ARB_ES2_compatibility|VERSION_4_1]
        /// Retrieve the range and precision for numeric formats supported by the shader compiler
        /// </summary>
        /// <param name="shadertype">
        /// Specifies the type of shader whose precision to query. shaderType must be VertexShader or FragmentShader.
        /// </param>
        /// <param name="precisiontype">
        /// Specifies the numeric format whose precision and range to query.
        /// </param>
        /// <param name="range">[length: 2]
        /// Specifies the address of array of two integers into which encodings of the implementation's numeric range are returned.
        /// </param>
        /// <param name="precision">[length: 1]
        /// Specifies the address of an integer into which the numeric precision of the implementation is written.
        /// </param>
        //[AutoGenerated(Category = "ARB_ES2_compatibility|VERSION_4_1", Version = "4.1", EntryPoint = "glGetShaderPrecisionFormat")]

        private unsafe delegate void delegate_GetShaderPrecisionFormat(int shadertype, int precisiontype, int* range, int* precision);
        private static delegate_GetShaderPrecisionFormat func_GetShaderPrecisionFormat;
        [CLSCompliant(false)]
        public static unsafe void GetShaderPrecisionFormat(int shadertype, int precisiontype, int* range, int* precision)
        {
            if (func_GetShaderPrecisionFormat == null)
            {
                func_GetShaderPrecisionFormat = (delegate_GetShaderPrecisionFormat)GetFunc("glGetShaderPrecisionFormat", typeof(delegate_GetShaderPrecisionFormat));
            }
            func_GetShaderPrecisionFormat(shadertype, precisiontype, range, precision);
        }




        /// <summary>[requires: v1.0]
        /// Return a texture image
        /// </summary>
        /// <param name="target">
        /// Specifies which texture is to be obtained. Texture1D, Texture2D, Texture3D, Texture1DArray, Texture2DArray, TextureRectangle, TextureCubeMapPositiveX, TextureCubeMapNegativeX, TextureCubeMapPositiveY, TextureCubeMapNegativeY, TextureCubeMapPositiveZ, and TextureCubeMapNegativeZ are accepted.
        /// </param>
        /// <param name="level">
        /// Specifies the level-of-detail number of the desired image. Level 0 is the base image level. Level  is the th mipmap reduction image.
        /// </param>
        /// <param name="format">
        /// Specifies a pixel format for the returned data. The supported formats are StencilIndex, DepthComponent, DepthStencil, Red, Green, Blue, Rg, Rgb, Rgba, Bgr, Bgra, RedInteger, GreenInteger, BlueInteger, RgInteger, RgbInteger, RgbaInteger, BgrInteger, BgraInteger.
        /// </param>
        /// <param name="type">
        /// Specifies a pixel type for the returned data. The supported types are UnsignedByte, Byte, UnsignedShort, Short, UnsignedInt, Int, HalfFloat, Float, UnsignedByte332, UnsignedByte233Rev, UnsignedShort565, UnsignedShort565Rev, UnsignedShort4444, UnsignedShort4444Rev, UnsignedShort5551, UnsignedShort1555Rev, UnsignedInt8888, UnsignedInt8888Rev, UnsignedInt1010102, UnsignedInt2101010Rev, UnsignedInt248, UnsignedInt10F11F11FRev, UnsignedInt5999Rev, and Float32UnsignedInt248Rev.
        /// </param>
        /// <param name="pixels">[length: COMPSIZE(target,level,format,type)]
        /// Returns the texture image. Should be a pointer to an array of the type specified by type.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetTexImage")]
        private delegate void delegate_GetTexImage(TextureTarget target, Int32 level, PixelFormat format, PixelType type, IntPtr pixels);
        private static delegate_GetTexImage func_GetTexImage;
        public static void GetTexImage(TextureTarget target, Int32 level, PixelFormat format, PixelType type, IntPtr pixels)
        {
            if (func_GetTexImage == null)
            {
                func_GetTexImage = (delegate_GetTexImage)GetFunc("glGetTexImage", typeof(delegate_GetTexImage));
            }
            func_GetTexImage(target, level, format, type, pixels);
        }



        /// <summary>[requires: v2.0]
        /// Returns the location of a uniform variable
        /// </summary>
        /// <param name="program">
        /// Specifies the program object to be queried.
        /// </param>
        /// <param name="name">
        /// Points to a null terminated string containing the name of the uniform variable whose location is to be queried.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetUniformLocation")]
        private delegate Int32 delegate_GetUniformLocation(Int32 program, String name);
        private static delegate_GetUniformLocation func_GetUniformLocation;
        [CLSCompliant(false)]
        public static Int32 getUniformLocation(Int32 program, String name)
        {
            if (func_GetUniformLocation == null)
            {
                func_GetUniformLocation = (delegate_GetUniformLocation)GetFunc("glGetUniformLocation", typeof(delegate_GetUniformLocation));
            }
            var result= func_GetUniformLocation(program, name);

            if (DebugEnabled) DebugMethod(result, program, name);
            return result;
        }


        /// <summary>[requires: v4.3 or ARB_invalidate_subdata|VERSION_4_3]
        /// Invalidate the content of a buffer object's data store
        /// </summary>
        /// <param name="buffer">
        /// The name of a buffer object whose data store to invalidate.
        /// </param>
        //[AutoGenerated(Category = "ARB_invalidate_subdata|VERSION_4_3", Version = "4.3", EntryPoint = "glInvalidateBufferData")]
        private delegate void delegate_InvalidateBufferData(Int32 buffer);
        private static delegate_InvalidateBufferData func_InvalidateBufferData;
        [CLSCompliant(false)]
        public static void InvalidateBufferData(Int32 buffer)
        {
            if (func_InvalidateBufferData == null)
            {
                func_InvalidateBufferData = (delegate_InvalidateBufferData)GetFunc("glInvalidateBufferData", typeof(delegate_InvalidateBufferData));
            }
            func_InvalidateBufferData(buffer);
        }




        /// <summary>[requires: v1.0]
        /// Test whether a capability is enabled
        /// </summary>
        /// <param name="cap">
        /// Specifies a symbolic constant indicating a GL capability.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glIsEnabled")]
        private delegate bool delegate_IsEnabled(EnableCap cap);
        private static delegate_IsEnabled func_IsEnabled;

        public static bool IsEnabled(EnableCap cap)
        {
            if (func_IsEnabled == null)
            {
                func_IsEnabled = (delegate_IsEnabled)GetFunc("glIsEnabled", typeof(delegate_IsEnabled));
            }
            return func_IsEnabled(cap);
        }




        /// <summary>[requires: v2.0]
        /// Links a program object
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program object to be linked.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glLinkProgram")]
        private delegate void delegate_LinkProgram(Int32 program);
        private static delegate_LinkProgram func_LinkProgram;
        [CLSCompliant(false)]
        public static void linkProgram(Int32 program)
        {
            if (func_LinkProgram == null)
            {
                func_LinkProgram = (delegate_LinkProgram)GetFunc("glLinkProgram", typeof(delegate_LinkProgram));
            }
            func_LinkProgram(program);
            if (DebugEnabled) DebugMethod(null, program);
        }



        /// <summary>[requires: v2.0]
        /// Replaces the source code in a shader object
        /// </summary>
        /// <param name="shader">
        /// Specifies the handle of the shader object whose source code is to be replaced.
        /// </param>
        /// <param name="count">
        /// Specifies the number of elements in the string and length arrays.
        /// </param>
        /// <param name="@string">[length: count]
        /// Specifies an array of pointers to strings containing the source code to be loaded into the shader.
        /// </param>
        /// <param name="length">[length: count]
        /// Specifies an array of string lengths.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glShaderSource")]
        private unsafe delegate void delegate_ShaderSource(Int32 shader, Int32 count, string[] @string, Int32* length);
        private static delegate_ShaderSource func_ShaderSource;
        [CLSCompliant(false)]
        public static unsafe void shaderSource(Int32 shader, Int32 count, string[] @string, Int32* length)
        {
            if (func_ShaderSource == null)
            {
                func_ShaderSource = (delegate_ShaderSource)GetFunc("glShaderSource", typeof(delegate_ShaderSource));
            }
            func_ShaderSource(shader, count, @string, length);

        }




        /// <summary>[requires: v1.0]
        /// Specify a two-dimensional texture image
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture. Must be Texture2D, ProxyTexture2D, Texture1DArray, ProxyTexture1DArray, TextureRectangle, ProxyTextureRectangle, TextureCubeMapPositiveX, TextureCubeMapNegativeX, TextureCubeMapPositiveY, TextureCubeMapNegativeY, TextureCubeMapPositiveZ, TextureCubeMapNegativeZ, or ProxyTextureCubeMap.
        /// </param>
        /// <param name="level">
        /// Specifies the level-of-detail number. Level 0 is the base image level. Level n is the nth mipmap reduction image. If target is TextureRectangle or ProxyTextureRectangle, level must be 0.
        /// </param>
        /// <param name="internalformat">
        /// Specifies the number of color components in the texture. Must be one of base internal formats given in Table 1, one of the sized internal formats given in Table 2, or one of the compressed internal formats given in Table 3, below.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the texture image. All implementations support texture images that are at least 1024 texels wide.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the texture image, or the number of layers in a texture array, in the case of the Texture1DArray and ProxyTexture1DArray targets. All implementations support 2D texture images that are at least 1024 texels high, and texture arrays that are at least 256 layers deep.
        /// </param>
        /// <param name="border">
        /// This value must be 0.
        /// </param>
        /// <param name="format">
        /// Specifies the format of the pixel data. The following symbolic values are accepted: Red, Rg, Rgb, Bgr, Rgba, Bgra, RedInteger, RgInteger, RgbInteger, BgrInteger, RgbaInteger, BgraInteger, StencilIndex, DepthComponent, DepthStencil.
        /// </param>
        /// <param name="type">
        /// Specifies the data type of the pixel data. The following symbolic values are accepted: UnsignedByte, Byte, UnsignedShort, Short, UnsignedInt, Int, Float, UnsignedByte332, UnsignedByte233Rev, UnsignedShort565, UnsignedShort565Rev, UnsignedShort4444, UnsignedShort4444Rev, UnsignedShort5551, UnsignedShort1555Rev, UnsignedInt8888, UnsignedInt8888Rev, UnsignedInt1010102, and UnsignedInt2101010Rev.
        /// </param>
        /// <param name="pixels">[length: COMPSIZE(format,type,width,height)]
        /// Specifies a pointer to the image data in memory.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glTexImage2D")]
        private delegate void delegate_TexImage2D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 border, int format, int type, IntPtr pixels);
        private static delegate_TexImage2D func_TexImage2D;
        public static void texImage2D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 border, int format, int type, IntPtr pixels)
        {
            if (func_TexImage2D == null)
            {
                func_TexImage2D = (delegate_TexImage2D)GetFunc("glTexImage2D", typeof(delegate_TexImage2D));
            }
            func_TexImage2D(target, level, internalformat, width, height, border, format, type, pixels);
            if (DebugEnabled) DebugMethod(null, target, level, internalformat, width, height, border, format, type, pixels);
        }
        public static void texImage2D(TextureTarget target, Int32 level, PixelInternalFormat internalformat, Int32 width, Int32 height, Int32 border, PixelFormat format, PixelType type, IntPtr pixels)
        {
            texImage2D((int)target, level, (int)internalformat, width, height, border, (int)format, (int)type, pixels);
        }
        public static void texImage2D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 border, int format, int type, byte[] pixels)
        {
            var handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                texImage2D(target, level, internalformat, width, height, border, format, type, handle.AddrOfPinnedObject());
                handle.Free();
                if (DebugEnabled) DebugMethod(null, target, level, internalformat, width, height, border, format, type, pixels);
            }
        }

        /// <summary>[requires: v1.0]
        /// Set texture parameters
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture, which must be either Texture1D, Texture2D, Texture3D, Texture1DArray, Texture2DArray, TextureRectangle, or TextureCubeMap.
        /// </param>
        /// <param name="pname">
        /// Specifies the symbolic name of a single-valued texture parameter. pname can be one of the following: DepthStencilTextureMode, TextureBaseLevel, TextureCompareFunc, TextureCompareMode, TextureLodBias, TextureMinFilter, TextureMagFilter, TextureMinLod, TextureMaxLod, TextureMaxLevel, TextureSwizzleR, TextureSwizzleG, TextureSwizzleB, TextureSwizzleA, TextureWrapS, TextureWrapT, or TextureWrapR.  For the vector commands (glTexParameter*v), pname can also be one of TextureBorderColor or TextureSwizzleRgba.
        /// </param>
        /// <param name="param">
        /// For the scalar commands, specifies the value of pname.
        /// </param>
        // [AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glTexParameteri")]
        private delegate void delegate_TexParameter(TextureTarget target, TextureParameterName pname, Int32 param);
        private static delegate_TexParameter func_TexParameter;
        public static void TexParameter(TextureTarget target, TextureParameterName pname, Int32 param)
        {
            if (func_TexParameter == null)
            {
                func_TexParameter = (delegate_TexParameter)GetFunc("glTexParameteri", typeof(delegate_TexParameter));
            }
            func_TexParameter(target, pname, param);
        }



        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform1f")]
        private delegate void delegate_Uniform1(Int32 location, Single f0);
        private static delegate_Uniform1 func_Uniform1;
        public static void uniform1f(Int32 location, Single f0)
        {
            if (func_Uniform1 == null)
            {
                func_Uniform1 = (delegate_Uniform1)GetFunc("glUniform1f", typeof(delegate_Uniform1));
            }
            func_Uniform1(location, f0);
            if (DebugEnabled) DebugMethod(null, location, f0 );
        }

        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform2f")]
        private delegate void delegate_Uniform2f(Int32 location, Single v0, Single v1);
        private static delegate_Uniform2f func_Uniform2f;
        public static void uniform2f(Int32 location, Single f0, Single f1)
        {
            if (func_Uniform2f == null)
            {
                func_Uniform2f = (delegate_Uniform2f)GetFunc("glUniform2f", typeof(delegate_Uniform2f));
            }
            func_Uniform2f(location, f0, f1);
            if (DebugEnabled) DebugMethod(null, location, f0, f1);
        }



        /// <summary>[requires: v2.0]</summary>
        /// <param name="location"></param>
        /// <param name="count"></param>
        /// <param name="transpose"></param>
        /// <param name="value">[length: count*16]</param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniformMatrix4fv")]
        private unsafe delegate void delegate_UniformMatrix4fv(Int32 location, Int32 count, bool transpose, Single* value);
        private static delegate_UniformMatrix4fv func_UniformMatrix4fv;
        [CLSCompliant(false)]
        public static unsafe void uniformMatrix4fv(Int32 location, Int32 count, bool transpose, Single* value)
        {
            if (func_UniformMatrix4fv == null)
            {
                func_UniformMatrix4fv = (delegate_UniformMatrix4fv)GetFunc("glUniformMatrix4fv", typeof(delegate_UniformMatrix4fv));
            }
            func_UniformMatrix4fv(location, count, transpose, value);
        }

        public unsafe static void uniformMatrix4fv(Int32 location, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
            {
                uniformMatrix4fv(location, 1, transpose, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, transpose, value);
            }
        }



        /// <summary>[requires: v2.0]
        /// Installs a program object as part of current rendering state
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program object whose executables are to be used as part of current rendering state.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUseProgram")]
        private delegate void delegate_UseProgram(Int32 program);
        private static delegate_UseProgram func_UseProgram;
        [CLSCompliant(false)]
        public static void useProgram(Int32 program)
        {
            if (func_UseProgram == null)
            {
                func_UseProgram = (delegate_UseProgram)GetFunc("glUseProgram", typeof(delegate_UseProgram));
            }
            func_UseProgram(program);
            if (DebugEnabled) DebugMethod(null, program );
        }




        /// <summary>[requires: v2.0]
        /// Define an array of generic vertex attribute data
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="size">
        /// Specifies the number of components per generic vertex attribute. Must be 1, 2, 3, 4. Additionally, the symbolic constant Bgra is accepted by glVertexAttribPointer. The initial value is 4.
        /// </param>
        /// <param name="type">
        /// Specifies the data type of each component in the array. The symbolic constants Byte, UnsignedByte, Short, UnsignedShort, Int, and UnsignedInt are accepted by glVertexAttribPointer and glVertexAttribIPointer. Additionally HalfFloat, Float, Double, Fixed, Int2101010Rev, UnsignedInt2101010Rev and UnsignedInt10F11F11FRev are accepted by glVertexAttribPointer. Double is also accepted by glVertexAttribLPointer and is the only token accepted by the type parameter for that function. The initial value is Float.
        /// </param>
        /// <param name="normalized">
        /// For glVertexAttribPointer, specifies whether fixed-point data values should be normalized (True) or converted directly as fixed-point values (False) when they are accessed.
        /// </param>
        /// <param name="stride">
        /// Specifies the byte offset between consecutive generic vertex attributes. If stride is 0, the generic vertex attributes are understood to be tightly packed in the array. The initial value is 0.
        /// </param>
        /// <param name="pointer">[length: COMPSIZE(size,type,stride)]
        /// Specifies a offset of the first component of the first generic vertex attribute in the array in the data store of the buffer currently bound to the ArrayBuffer target. The initial value is 0.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttribPointer")]
        private delegate void delegate_VertexAttribPointer(Int32 index, Int32 size, int type, bool normalized, Int32 stride, IntPtr pointer);
        private static delegate_VertexAttribPointer func_VertexAttribPointer;
        [CLSCompliant(false)]
        public static void vertexAttribPointer(Int32 index, Int32 size, int type, bool normalized, Int32 stride, IntPtr pointer)
        {
            if (func_VertexAttribPointer == null)
            {
                func_VertexAttribPointer = (delegate_VertexAttribPointer)GetFunc("glVertexAttribPointer", typeof(delegate_VertexAttribPointer));
            }
            func_VertexAttribPointer(index, size, type, normalized, stride, pointer);
        }

        // /// <summary>[requires: v3.0]</summary>
        // /// <param name="index"></param>
        // /// <param name="size"></param>
        // /// <param name="type"></param>
        // /// <param name="stride"></param>
        // /// <param name="pointer">[length: COMPSIZE(size,type,stride)]</param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glVertexAttribIPointer")]
        // [CLSCompliant(false)]
        private delegate void delegate_VertexAttribIPointer(Int32 index, Int32 size, Int32 type, Int32 stride, IntPtr pointer);
        private static delegate_VertexAttribIPointer func_VertexAttribIPointer;
        [CLSCompliant(false)]
        //public static void vertexAttribIPointer(Int32 index, Int32 size, int type, bool normalized, Int32 stride, IntPtr pointer)
        public static void vertexAttribIPointer(Int32 index, Int32 size, Int32 type, Int32 stride, IntPtr pointer)
        {
            if (func_VertexAttribIPointer == null)
            {
                func_VertexAttribIPointer = (delegate_VertexAttribIPointer)GetFunc("glVertexAttribIPointer", typeof(delegate_VertexAttribIPointer));
            }
            func_VertexAttribIPointer(index, size, type, stride, pointer);
        }

        public static void vertexAttribIPointer(Int32 index, Int32 size, Int32 type, Int32 stride, int offset)
        {
            vertexAttribIPointer(index, size, type, stride, (IntPtr)offset);
            if (DebugEnabled) DebugMethod(null, index, size, type, stride, offset);
        }

        /// <summary>[requires: v1.0]
        /// Set the viewport
        /// </summary>
        /// <param name="x">
        /// Specify the lower left corner of the viewport rectangle, in pixels. The initial value is (0,0).
        /// </param>
        /// <param name="y">
        /// Specify the lower left corner of the viewport rectangle, in pixels. The initial value is (0,0).
        /// </param>
        /// <param name="width">
        /// Specify the width and height of the viewport. When a GL context is first attached to a window, width and height are set to the dimensions of that window.
        /// </param>
        /// <param name="height">
        /// Specify the width and height of the viewport. When a GL context is first attached to a window, width and height are set to the dimensions of that window.
        /// </param>
        // [AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glViewport")]
        private delegate void delegate_Viewport(Int32 x, Int32 y, Int32 width, Int32 height);
        private static delegate_Viewport func_Viewport;
        public static void viewport(Int32 x, Int32 y, Int32 width, Int32 height)
        {
            if (func_Viewport == null)
            {
                func_Viewport = (delegate_Viewport)GetFunc("glViewport", typeof(delegate_Viewport));
            }
            func_Viewport(x, y, width, height);
            if (DebugEnabled) DebugMethod(null, x, y, width, height);
        }


        /// <summary>[requires: v1.0]
        /// Return error information
        /// </summary>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glGetError")]
        private delegate ErrorCode delegate_GetError();
        private static delegate_GetError func_GetError;
        public static ErrorCode getError()
        {
            if (func_GetError == null)
            {
                func_GetError = (delegate_GetError)GetFunc("glGetError", typeof(delegate_GetError));
            }
            return (ErrorCode)func_GetError();

        }


        /// <summary>[requires: v2.0]
        /// Specifies the value of a generic vertex attribute
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="x">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        /// <param name="y">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        /// <param name="z">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        /// <param name="w">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttrib4f")]
        private delegate void delegate_VertexAttrib4(Int32 index, Single x, Single y, Single z, Single w);
        private static delegate_VertexAttrib4 func_VertexAttrib4;
        [CLSCompliant(false)]
        public static void VertexAttrib4(Int32 index, Single x, Single y, Single z, Single w)
        {
            if (func_VertexAttrib4 == null)
            {
                func_VertexAttrib4 = (delegate_VertexAttrib4)GetFunc("glVertexAttrib4f", typeof(delegate_VertexAttrib4));
            }
            func_VertexAttrib4(index, x, y, z, w);
        }



        /// <summary>[requires: v2.0]
        /// Specifies the value of a generic vertex attribute
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="x">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        /// <param name="y">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        /// <param name="z">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        private delegate void delegate_VertexAttrib3(Int32 index, Single x, Single y, Single z);
        private static delegate_VertexAttrib3 func_VertexAttrib3;
        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttrib3f")]
        [CLSCompliant(false)]
        public static void VertexAttrib3(Int32 index, Single x, Single y, Single z)
        {
            if (func_VertexAttrib3 == null)
            {
                func_VertexAttrib3 = (delegate_VertexAttrib3)GetFunc("glVertexAttrib3f", typeof(delegate_VertexAttrib3));
            }
            func_VertexAttrib3(index, x, y, z);
        }



        /// <summary>[requires: v2.0]
        /// Specifies the value of a generic vertex attribute
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="x">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
        /// <param name="y">
        /// For the scalar commands, specifies the new values to be used for the specified vertex attribute.
        /// </param>
       //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttrib2f")]
        private delegate void delegate_VertexAttrib2(Int32 index, Single x, Single y);
        private static delegate_VertexAttrib2 func_VertexAttrib2;
        [CLSCompliant(false)]
        public static void VertexAttrib2(Int32 index, Single x, Single y)
        {

            if (func_VertexAttrib2 == null)
            {
                func_VertexAttrib2 = (delegate_VertexAttrib2)GetFunc("glVertexAttrib2f", typeof(delegate_VertexAttrib2));
            }
            func_VertexAttrib2(index, x, y);
        }


        /// <summary>[requires: v2.0]</summary>
        /// <param name="location"></param>
        /// <param name="count"></param>
        /// <param name="transpose"></param>
        /// <param name="value">[length: count*9]</param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniformMatrix3fv")]
        private unsafe delegate void delegate_UniformMatrix3fv(Int32 location, Int32 count, bool transpose, Single* value);
        private static delegate_UniformMatrix3fv func_UniformMatrix3fv;
        [CLSCompliant(false)]
        public static unsafe void uniformMatrix3fv(Int32 location, Int32 count, bool transpose, Single* value)
        {
            if (func_UniformMatrix3fv == null)
            {
                func_UniformMatrix3fv = (delegate_UniformMatrix3fv)GetFunc("glUniformMatrix3fv", typeof(delegate_UniformMatrix3fv));
            }
            func_UniformMatrix3fv(location, count, transpose, value);

        }
        public unsafe static void uniformMatrix3fv(Int32 location, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
            {
                uniformMatrix3fv(location, 1, transpose, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, transpose, value);
            }
        }

        /// <summary>[requires: v1.4 or ARB_imaging|VERSION_1_4]
        /// Set the blend color
        /// </summary>
        /// <param name="red">
        /// specify the components of BlendColor
        /// </param>
        /// <param name="green">
        /// specify the components of BlendColor
        /// </param>
        /// <param name="blue">
        /// specify the components of BlendColor
        /// </param>
        /// <param name="alpha">
        /// specify the components of BlendColor
        /// </param>
        //[AutoGenerated(Category = "ARB_imaging|VERSION_1_4", Version = "1.4", EntryPoint = "glBlendColor")]
        private unsafe delegate void delegate_BlendColor(Single red, Single green, Single blue, Single alpha);
        private static delegate_BlendColor func_BlendColor;
        public static void BlendColor(Single red, Single green, Single blue, Single alpha)
        {
            if (func_BlendColor == null)
            {
                func_BlendColor = (delegate_BlendColor)GetFunc("glBlendColor", typeof(delegate_BlendColor));
            }
            func_BlendColor(red, green, blue, alpha);
        }


        /// <summary>[requires: v2.0]
        /// Returns the information log for a program object
        /// </summary>
        /// <param name="program">
        /// Specifies the program object whose information log is to be queried.
        /// </param>
        /// <param name="bufSize">
        /// Specifies the size of the character buffer for storing the returned information log.
        /// </param>
        /// <param name="length">[length: 1]
        /// Returns the length of the string returned in infoLog (excluding the null terminator).
        /// </param>
        /// <param name="infoLog">[length: bufSize]
        /// Specifies an array of characters that is used to return the information log.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetProgramInfoLog")]
        private unsafe delegate void delegate_GetProgramInfoLog(Int32 program, Int32 bufSize, Int32* length, IntPtr infoLog);
        private static delegate_GetProgramInfoLog func_GetProgramInfoLog;
        [CLSCompliant(false)]
        public static unsafe void getProgramInfoLog(Int32 program, Int32 bufSize, Int32* length, IntPtr infoLog)
        {
            if (func_GetProgramInfoLog == null)
            {
                func_GetProgramInfoLog = (delegate_GetProgramInfoLog)GetFunc("glGetProgramInfoLog", typeof(delegate_GetProgramInfoLog));
            }
            func_GetProgramInfoLog(program, bufSize, length, infoLog);
        }


        //public unsafe static void getProgramInfoLog(Int32 program, Int32 bufSize, out Int32 length, out String infoLog)
        //{
        //    int len = 0;
        //    getProgramInfoLog(program, bufSize, &len, out infoLog);
        //    length = len;

        //}


        /// <summary>[requires: v2.0]</summary>
        /// <param name="location"></param>
        /// <param name="count"></param>
        /// <param name="transpose"></param>
        /// <param name="value">[length: count*4]</param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniformMatrix2fv")]
        private unsafe delegate void delegate_UniformMatrix2fv(Int32 location, Int32 count, bool transpose, Single* value);
        private static delegate_UniformMatrix2fv func_UniformMatrix2fv;
        public static unsafe void uniformMatrix2fv(Int32 location, Int32 count, bool transpose, Single* value)
        {
            if (func_UniformMatrix2fv == null)
            {
                func_UniformMatrix2fv = (delegate_UniformMatrix2fv)GetFunc("glUniformMatrix2fv", typeof(delegate_UniformMatrix2fv));
            }
            func_UniformMatrix2fv(location, count, transpose, value);
        }
        public unsafe static void uniformMatrix2fv(Int32 location, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
            {
                uniformMatrix2fv(location, 1, transpose, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, transpose, value);
            }
        }


        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v3">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform4f")]
        private unsafe delegate void delegate_Uniform4f(Int32 location, Single v0, Single v1, Single v2, Single v3);
        private static delegate_Uniform4f func_Uniform4f;
        public static void uniform4f(Int32 location, Single v0, Single v1, Single v2, Single v3)
        {
            if (func_Uniform4f == null)
            {
                func_Uniform4f = (delegate_Uniform4f)GetFunc("glUniform4f", typeof(delegate_Uniform4f));
            }
            func_Uniform4f(location, v0, v1, v2, v3);
            if (DebugEnabled) DebugMethod(null, location, v0, v1, v2, v3);
        }



        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*4]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform4fv")]
        private unsafe delegate void delegate_Uniform4fv(Int32 location, Int32 count, Single* value);
        private static delegate_Uniform4fv func_Uniform4fv;
        [CLSCompliant(false)]
        public static unsafe void uniform4fv(Int32 location, Int32 count, Single* value)
        {
            if (func_Uniform4fv == null)
            {
                func_Uniform4fv = (delegate_Uniform4fv)GetFunc("glUniform4fv", typeof(delegate_Uniform4fv));
            }
            func_Uniform4fv(location, count, value);
        }
        public unsafe static void uniform4fv(Int32 location, float[] value)
        {
            fixed (float* valuePtr = value)
            {
                uniform4fv(location, value.Length / 4, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }


        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform3f")]
        private unsafe delegate void delegate_Uniform3f(Int32 location, Single v0, Single v1, Single v2);
        private static delegate_Uniform3f func_Uniform3f;
        public static void uniform3f(Int32 location, Single v0, Single v1, Single v2)
        {
            if (func_Uniform3f == null)
            {
                func_Uniform3f = (delegate_Uniform3f)GetFunc("glUniform3f", typeof(delegate_Uniform3f));
            }
            func_Uniform3f(location, v0, v1, v2);
            if (DebugEnabled) DebugMethod(null, location, v0, v1, v2);
        }

        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*3]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
       // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform3fv")]
        private unsafe delegate void delegate_Uniform3fv(int location, int count, float* value);
        private static delegate_Uniform3fv func_Uniform3fv;
        public static unsafe void uniform3fv(int location, int count, float* value)
        {
            if (func_Uniform3fv == null)
            {
                func_Uniform3fv = (delegate_Uniform3fv)GetFunc("glUniform3fv", typeof(delegate_Uniform3fv));
            }
            func_Uniform3fv(location, count, value);
        }
        public unsafe static void uniform3fv(int location, float[] value)
        {
            fixed (float* ptr = value)
            {
                uniform3fv(location, value.Length / 3, ptr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }

        /// <summary>[requires: EXT_direct_state_access|EXT_separate_shader_objects|EXT_separate_shader_objects]</summary>
        /// <param name="program"></param>
        /// <param name="location"></param>
        /// <param name="count"></param>
        /// <param name="transpose"></param>
        /// <param name="value">[length: count*16]</param>
        //[AutoGenerated(Category = "EXT_direct_state_access|EXT_separate_shader_objects|EXT_separate_shader_objects", Version = "", EntryPoint = "glProgramUniformMatrix4fvEXT")]
        private unsafe delegate void delegate_ProgramUniformMatrix4(Int32 program, Int32 location, Int32 count, bool transpose, Single* value);
        private static delegate_ProgramUniformMatrix4 func_ProgramUniformMatrix4;
        [CLSCompliant(false)]
        public static unsafe void ProgramUniformMatrix4(Int32 program, Int32 location, Int32 count, bool transpose, Single* value)
        {
            if (func_ProgramUniformMatrix4 == null)
            {
                func_ProgramUniformMatrix4 = (delegate_ProgramUniformMatrix4)GetFunc("glProgramUniformMatrix4fvEXT", typeof(delegate_ProgramUniformMatrix4));
            }
            func_ProgramUniformMatrix4(program, location, count, transpose, value);
        }

        /// <summary>[requires: v4.1 or ARB_separate_shader_objects|VERSION_4_1]</summary>
        /// <param name="program"></param>
        /// <param name="location"></param>
        /// <param name="count"></param>
        /// <param name="transpose"></param>
        /// <param name="value">[length: count*9]</param>
        //[AutoGenerated(Category = "ARB_separate_shader_objects|VERSION_4_1", Version = "4.1", EntryPoint = "glProgramUniformMatrix3fv")]
        private unsafe delegate void delegate_ProgramUniformMatrix3(Int32 program, Int32 location, Int32 count, bool transpose, Single* value);
        private static delegate_ProgramUniformMatrix3 func_ProgramUniformMatrix3;
        [CLSCompliant(false)]
        public static unsafe void ProgramUniformMatrix3(Int32 program, Int32 location, Int32 count, bool transpose, Single* value)
        {
            if (func_ProgramUniformMatrix3 == null)
            {
                func_ProgramUniformMatrix3 = (delegate_ProgramUniformMatrix3)GetFunc("glProgramUniformMatrix3fv", typeof(delegate_ProgramUniformMatrix3));
            }
            func_ProgramUniformMatrix3(program, location, count, transpose, value);
        }



        /// <summary>[requires: EXT_direct_state_access|EXT_separate_shader_objects]</summary>
        /// <param name="program"></param>
        /// <param name="location"></param>
        /// <param name="count"></param>
        /// <param name="transpose"></param>
        /// <param name="value">[length: count*4]</param>
        //[AutoGenerated(Category = "EXT_direct_state_access|EXT_separate_shader_objects", Version = "", EntryPoint = "glProgramUniformMatrix2fvEXT")]
        private unsafe delegate void delegate_ProgramUniformMatrix2(Int32 program, Int32 location, Int32 count, bool transpose, Single* value);
        private static delegate_ProgramUniformMatrix2 func_ProgramUniformMatrix2;
        [CLSCompliant(false)]
        public static unsafe void ProgramUniformMatrix2(Int32 program, Int32 location, Int32 count, bool transpose, Single* value)
        {
            if (func_ProgramUniformMatrix2 == null)
            {
                func_ProgramUniformMatrix2 = (delegate_ProgramUniformMatrix2)GetFunc("glProgramUniformMatrix2fvEXT", typeof(delegate_ProgramUniformMatrix2));
            }
            func_ProgramUniformMatrix2(program, location, count, transpose, value);
        }



        /// <summary>[requires: v4.1 or ARB_separate_shader_objects|VERSION_4_1]
        /// Specify the value of a uniform variable for a specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program containing the uniform variable to be modified.
        /// </param>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v3">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
       // [AutoGenerated(Category = "ARB_separate_shader_objects|VERSION_4_1", Version = "4.1", EntryPoint = "glProgramUniform4f")]
        private unsafe delegate void delegate_ProgramUniform4(Int32 program, Int32 location, Single v0, Single v1, Single v2, Single v3);
        private static delegate_ProgramUniform4 func_ProgramUniform4;
        [CLSCompliant(false)]
        public static void ProgramUniform4(Int32 program, Int32 location, Single v0, Single v1, Single v2, Single v3)
        {
            if (func_ProgramUniform4 == null)
            {
                func_ProgramUniform4 = (delegate_ProgramUniform4)GetFunc("glProgramUniform4f", typeof(delegate_ProgramUniform4));
            }
            func_ProgramUniform4(program, location, v0, v1, v2, v3);
        }


        /// <summary>[requires: v4.1 or ARB_separate_shader_objects|VERSION_4_1]
        /// Specify the value of a uniform variable for a specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program containing the uniform variable to be modified.
        /// </param>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector commands (glProgramUniform*v), specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix commands (glProgramUniformMatrix*), specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*4]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "ARB_separate_shader_objects|VERSION_4_1", Version = "4.1", EntryPoint = "glProgramUniform4fv")]
        private unsafe delegate void delegate_ProgramUniform4fv(Int32 program, Int32 location, Int32 count, Single* value);
        private static delegate_ProgramUniform4fv func_ProgramUniform4fv;
        [CLSCompliant(false)]
        public static unsafe void ProgramUniform4(Int32 program, Int32 location, Int32 count, Single* value)
        {
            if (func_ProgramUniform4fv == null)
            {
                func_ProgramUniform4fv = (delegate_ProgramUniform4fv)GetFunc("glProgramUniform4fv", typeof(delegate_ProgramUniform4fv));
            }
            func_ProgramUniform4fv(program, location, count, value);
        }



        /// <summary>[requires: EXT_direct_state_access|EXT_separate_shader_objects]
        /// Specify the value of a uniform variable for a specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program containing the uniform variable to be modified.
        /// </param>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "EXT_direct_state_access|EXT_separate_shader_objects", Version = "", EntryPoint = "glProgramUniform3fEXT")]
        private unsafe delegate void delegate_ProgramUniform3(Int32 program, Int32 location, Single v0, Single v1, Single v2);
        private static delegate_ProgramUniform3 func_ProgramUniform3;
        [CLSCompliant(false)]
        public static void ProgramUniform3(Int32 program, Int32 location, Single v0, Single v1, Single v2)
        {
            if (func_ProgramUniform3 == null)
            {
                func_ProgramUniform3 = (delegate_ProgramUniform3)GetFunc("glProgramUniform3fEXT", typeof(delegate_ProgramUniform3));
            }
            func_ProgramUniform3(program, location, v0, v1, v2);
        }



        /// <summary>[requires: EXT_direct_state_access|EXT_separate_shader_objects]
        /// Specify the value of a uniform variable for a specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program containing the uniform variable to be modified.
        /// </param>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector commands (glProgramUniform*v), specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix commands (glProgramUniformMatrix*), specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*3]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "EXT_direct_state_access|EXT_separate_shader_objects", Version = "", EntryPoint = "glProgramUniform3fvEXT")]
        private unsafe delegate void delegate_ProgramUniform3fv(Int32 program, Int32 location, Int32 count, Single* value);
        private static delegate_ProgramUniform3fv func_ProgramUniform3fv;
        [CLSCompliant(false)]
        public static unsafe void ProgramUniform3(Int32 program, Int32 location, Int32 count, Single* value)
        {
            if (func_ProgramUniform3fv == null)
            {
                func_ProgramUniform3fv = (delegate_ProgramUniform3fv)GetFunc("glProgramUniform3fvEXT", typeof(delegate_ProgramUniform3fv));
            }
            func_ProgramUniform3fv(program, location, count, value);
        }


        /// <summary>[requires: EXT_direct_state_access|EXT_separate_shader_objects]
        /// Specify the value of a uniform variable for a specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program containing the uniform variable to be modified.
        /// </param>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "EXT_direct_state_access|EXT_separate_shader_objects", Version = "", EntryPoint = "glProgramUniform2fEXT")]
        private unsafe delegate void delegate_ProgramUniform2(int program, Int32 location, Single v0, Single v1);
        private static delegate_ProgramUniform2 func_ProgramUniform2;
        [CLSCompliant(false)]
        public static void ProgramUniform2(int program, Int32 location, Single v0, Single v1)
        {
            if (func_ProgramUniform2 == null)
            {
                func_ProgramUniform2 = (delegate_ProgramUniform2)GetFunc("glProgramUniform2fEXT", typeof(delegate_ProgramUniform2));
            }
            func_ProgramUniform2(program, location, v0, v1);
        }


        /// <summary>[requires: EXT_direct_state_access|EXT_separate_shader_objects]
        /// Specify the value of a uniform variable for a specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program containing the uniform variable to be modified.
        /// </param>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector commands (glProgramUniform*v), specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix commands (glProgramUniformMatrix*), specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*2]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "EXT_direct_state_access|EXT_separate_shader_objects", Version = "", EntryPoint = "glProgramUniform2fvEXT")]
        private unsafe delegate void delegate_ProgramUniform2fv(int program, Int32 location, Int32 count, Single* value);
        private static delegate_ProgramUniform2fv func_ProgramUniform2fv;
        [CLSCompliant(false)]
        public static unsafe void ProgramUniform2(Int32 program, Int32 location, Int32 count, Single* value)
        {
            if (func_ProgramUniform2fv == null)
            {
                func_ProgramUniform2fv = (delegate_ProgramUniform2fv)GetFunc("glProgramUniform2fvEXT", typeof(delegate_ProgramUniform2fv));
            }
            func_ProgramUniform2fv(program, location, count, value);
        }


        /// <summary>[requires: v2.0]
        /// Returns the information log for a shader object
        /// </summary>
        /// <param name="shader">
        /// Specifies the shader object whose information log is to be queried.
        /// </param>
        /// <param name="bufSize">
        /// Specifies the size of the character buffer for storing the returned information log.
        /// </param>
        /// <param name="length">[length: 1]
        /// Returns the length of the string returned in infoLog (excluding the null terminator).
        /// </param>
        /// <param name="infoLog">[length: bufSize]
        /// Specifies an array of characters that is used to return the information log.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetShaderInfoLog")]
        private unsafe delegate void delegate_GetShaderInfoLog(Int32 shader, Int32 bufSize, Int32* length, IntPtr infoLog);
        private static delegate_GetShaderInfoLog func_GetShaderInfoLog;
        [CLSCompliant(false)]
        public static unsafe void getShaderInfoLog(Int32 shader, Int32 bufSize, Int32* length, IntPtr infoLog)
        {
            if (func_GetShaderInfoLog == null)
            {
                func_GetShaderInfoLog = (delegate_GetShaderInfoLog)GetFunc("glGetShaderInfoLog", typeof(delegate_GetShaderInfoLog));
            }
            func_GetShaderInfoLog(shader, bufSize, length, infoLog);
        }

        public unsafe static string getShaderInfoLog(Int32 shader, Int32 bufSize, Int32 length)
        {

            var buf = new byte[length];
            var len = new int[] { length };
            fixed (int* len_ptr = len)
            {
                var handle = GCHandle.Alloc(buf, GCHandleType.Pinned);
                if (handle.IsAllocated)
                {
                    var buf_ptr = handle.AddrOfPinnedObject();
                    getShaderInfoLog(shader, bufSize, len_ptr, buf_ptr);
                    handle.Free();
                    return Marshal.PtrToStringAnsi(buf_ptr);
                }
            }
            return null;
        }


        /// <summary>[requires: v2.0]
        /// Returns information about an active uniform variable for the specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the program object to be queried.
        /// </param>
        /// <param name="index">
        /// Specifies the index of the uniform variable to be queried.
        /// </param>
        /// <param name="bufSize">
        /// Specifies the maximum number of characters OpenGL is allowed to write in the character buffer indicated by name.
        /// </param>
        /// <param name="length">[length: 1]
        /// Returns the number of characters actually written by OpenGL in the string indicated by name (excluding the null terminator) if a value other than Null is passed.
        /// </param>
        /// <param name="size">[length: 1]
        /// Returns the size of the uniform variable.
        /// </param>
        /// <param name="type">[length: 1]
        /// Returns the data type of the uniform variable.
        /// </param>
        /// <param name="name">[length: bufSize]
        /// Returns a null terminated string containing the name of the uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetActiveUniform")]
        private unsafe delegate void delegate_GetActiveUniform(Int32 program, Int32 index, Int32 bufSize, out Int32 length, out Int32 size, out int type, IntPtr name);
        private static delegate_GetActiveUniform func_GetActiveUniform;
        [CLSCompliant(false)]
        public static void getActiveUniform(Int32 program, Int32 index, Int32 bufSize, out Int32 length, out Int32 size, out int type, IntPtr name)
        {
            if (func_GetActiveUniform == null)
            {
                func_GetActiveUniform = (delegate_GetActiveUniform)GetFunc("glGetActiveUniform", typeof(delegate_GetActiveUniform));
            }
            func_GetActiveUniform(program, index, bufSize, out length, out size, out type, name);
        }

        public static WebGLActiveInfo getActiveUniform(Int32 program, Int32 index)
        {
            int length = 0;
            int size = 0;
            int type = 0;
            var buf = new byte[1024];
            var handle = GCHandle.Alloc(buf, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                var ptr = handle.AddrOfPinnedObject();
                getActiveUniform(program, index, 1024, out length, out size, out type, ptr);
                var name = Marshal.PtrToStringAnsi(ptr);
                handle.Free();
                var result = new WebGLActiveInfo()
                {
                    name = name,
                    size = size,
                    type = type
                };
                if (DebugEnabled) DebugMethod(result, program, index);
                return result;
            }
            else
            {
                if (DebugEnabled) DebugMethod("null", program, index);
                return null;
            }


        }

        /// <summary>[requires: v2.0]
        /// Returns information about an active attribute variable for the specified program object
        /// </summary>
        /// <param name="program">
        /// Specifies the program object to be queried.
        /// </param>
        /// <param name="index">
        /// Specifies the index of the attribute variable to be queried.
        /// </param>
        /// <param name="bufSize">
        /// Specifies the maximum number of characters OpenGL is allowed to write in the character buffer indicated by name.
        /// </param>
        /// <param name="length">[length: 1]
        /// Returns the number of characters actually written by OpenGL in the string indicated by name (excluding the null terminator) if a value other than Null is passed.
        /// </param>
        /// <param name="size">[length: 1]
        /// Returns the size of the attribute variable.
        /// </param>
        /// <param name="type">[length: 1]
        /// Returns the data type of the attribute variable.
        /// </param>
        /// <param name="name">[length: bufSize]
        /// Returns a null terminated string containing the name of the attribute variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glGetActiveAttrib")]
        private unsafe delegate void delegate_GetActiveAttrib(Int32 program, Int32 index, Int32 bufSize, out Int32 length, out Int32 size, out int type, IntPtr name);
        private static delegate_GetActiveAttrib func_GetActiveAttrib;
        public static void getActiveAttrib(Int32 program, Int32 index, Int32 bufSize, out Int32 length, out Int32 size, out int type, IntPtr name)
        {
            if (func_GetActiveAttrib == null)
            {
                func_GetActiveAttrib = (delegate_GetActiveAttrib)GetFunc("glGetActiveAttrib", typeof(delegate_GetActiveAttrib));
            }
            func_GetActiveAttrib(program, index, bufSize, out length, out size, out type, name);
        }



        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform1i")]
        private unsafe delegate void delegate_Uniform1i(Int32 location, Int32 v0);
        private static delegate_Uniform1i func_Uniform1i;
        public static void uniform1i(Int32 location, Int32 v0)
        {
            if (func_Uniform1i == null)
            {
                func_Uniform1i = (delegate_Uniform1i)GetFunc("glUniform1i", typeof(delegate_Uniform1i));
            }
            func_Uniform1i(location, v0);
            if (DebugEnabled) DebugMethod( null , location, v0);
        }

        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform2i")]
        private unsafe delegate void delegate_Uniform2i(Int32 location, Int32 v0, Int32 v1);
        private static delegate_Uniform2i func_Uniform2i;
        public static void uniform2i(Int32 location, Int32 v0, Int32 v1)
        {
            if (func_Uniform2i == null)
            {
                func_Uniform2i = (delegate_Uniform2i)GetFunc("glUniform2i", typeof(delegate_Uniform2i));
            }
            func_Uniform2i(location, v0, v1);
            if (DebugEnabled) DebugMethod(null, location, v0,v1);
        }

        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform3i")]
        private unsafe delegate void delegate_Uniform3i(Int32 location, Int32 v0, Int32 v1, Int32 v2);
        private static delegate_Uniform3i func_Uniform3i;
        public static void uniform3i(Int32 location, Int32 v0, Int32 v1, Int32 v2)
        {
            if (func_Uniform3i == null)
            {
                func_Uniform3i = (delegate_Uniform3i)GetFunc("glUniform3i", typeof(delegate_Uniform3i));
            }
            func_Uniform3i(location, v0, v1, v2);
            if (DebugEnabled) DebugMethod(null, location, v0, v1,v2);
        }

        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v3">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform4i")]
        private unsafe delegate void delegate_Uniform4i(Int32 location, Int32 v0, Int32 v1, Int32 v2, Int32 v3);
        private static delegate_Uniform4i func_Uniform4i;
        public static void uniform4i(Int32 location, Int32 v0, Int32 v1, Int32 v2, Int32 v3)
        {
            if (func_Uniform4i == null)
            {
                func_Uniform4i = (delegate_Uniform4i)GetFunc("glUniform4i", typeof(delegate_Uniform4i));
            }
            func_Uniform4i(location, v0, v1, v2, v3);
            if (DebugEnabled) DebugMethod(null, location, v0, v1, v2,v3);
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform1ui")]
        private unsafe delegate void delegate_Uniform1ui(Int32 location, UInt32 v0);
        private static delegate_Uniform1ui func_Uniform1ui;
        public static void uniform1ui(Int32 location, UInt32 v0)
        {
            if (func_Uniform1ui == null)
            {
                func_Uniform1ui = (delegate_Uniform1ui)GetFunc("glUniform1ui", typeof(delegate_Uniform1ui));
            }
            func_Uniform1ui(location, v0);
            if (DebugEnabled) DebugMethod(null, location, v0 );
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform2ui")]
        private unsafe delegate void delegate_Uniform2ui(Int32 location, UInt32 v0, UInt32 v1);
        private static delegate_Uniform2ui func_Uniform2ui;
        public static void uniform2ui(Int32 location, UInt32 v0, UInt32 v1)
        {
            if (func_Uniform2ui == null)
            {
                func_Uniform2ui = (delegate_Uniform2ui)GetFunc("glUniform2ui", typeof(delegate_Uniform2ui));
            }
            func_Uniform2ui(location, v0, v1);
            if (DebugEnabled) DebugMethod(null, location, v0,v1);
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform3ui")]
        private unsafe delegate void delegate_Uniform3ui(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2);
        private static delegate_Uniform3ui func_Uniform3ui;
        public static void uniform3ui(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2)
        {
            if (func_Uniform3ui == null)
            {
                func_Uniform3ui = (delegate_Uniform3ui)GetFunc("glUniform3i", typeof(delegate_Uniform3ui));
            }
            func_Uniform3ui(location, v0, v1, v2);
            if (DebugEnabled) DebugMethod(null, location, v0, v1,v2);
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="v0">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v1">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v2">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        /// <param name="v3">
        /// For the scalar commands, specifies the new values to be used for the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform4ui")]
        private unsafe delegate void delegate_Uniform4ui(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2, UInt32 v3);
        private static delegate_Uniform4ui func_Uniform4ui;
        public static void uniform4i(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2, UInt32 v3)
        {
            if (func_Uniform4ui == null)
            {
                func_Uniform4ui = (delegate_Uniform4ui)GetFunc("glUniform4ui", typeof(delegate_Uniform4ui));
            }
            func_Uniform4ui(location, v0, v1, v2, v3);
            if (DebugEnabled) DebugMethod(null, location, v0, v1,v2,v3);
        }

        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*2]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform2fv")]
        private unsafe delegate void delegate_Uniform2fv(Int32 location, Int32 count, Single* value);
        private static delegate_Uniform2fv func_Uniform2fv;
        public unsafe static void uniform2fv(Int32 location, Int32 count, Single* value)
        {
            if (func_Uniform2fv == null)
            {
                func_Uniform2fv = (delegate_Uniform2fv)GetFunc("glUniform2fv", typeof(delegate_Uniform2fv));
            }
            func_Uniform2fv(location, count, value);

        }
        public unsafe static void uniform2fv(Int32 location, float[] value)
        {
            fixed (float* valuePtr = value)
            {
                uniform2fv(location, value.Length / 2, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }



        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*1]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
       // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform1fv")]
        private unsafe delegate void delegate_Uniform1fv(Int32 location, Int32 count, Single* value);
        private static delegate_Uniform1fv func_Uniform1fv;
        public static unsafe void uniform1fv(Int32 location, Int32 count, Single* value)
        {
            if (func_Uniform1fv == null)
            {
                func_Uniform1fv = (delegate_Uniform1fv)GetFunc("glUniform1fv", typeof(delegate_Uniform1fv));
            }
            func_Uniform1fv(location, count, value);

        }
        public static unsafe void uniform1fv(Int32 location, float[] value)
        {
            fixed (float* valuePtr = value)
            {
                uniform1fv(location, value.Length, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }


        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*1]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform1iv")]
        private unsafe delegate void delegate_Uniform1iv(Int32 location, Int32 count, Int32* value);
        private static delegate_Uniform1iv func_Uniform1iv;
        public static unsafe void uniform1iv(Int32 location, Int32 count, Int32* value)
        {
            if (func_Uniform1iv == null)
            {
                func_Uniform1iv = (delegate_Uniform1iv)GetFunc("glUniform1iv", typeof(delegate_Uniform1iv));
            }
            func_Uniform1iv(location, count, value);
        }
        public unsafe static void uniform1iv(Int32 location, int[] value)
        {
            fixed (int* valuePtr = value)
            {
                uniform1iv(location, value.Length, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }


        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*2]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform2iv")]
        private unsafe delegate void delegate_Uniform2iv(Int32 location, Int32 count, Int32* value);
        private static delegate_Uniform2iv func_Uniform2iv;
        public static unsafe void uniform2iv(Int32 location, Int32 count, Int32* value)
        {
            if (func_Uniform2iv == null)
            {
                func_Uniform2iv = (delegate_Uniform2iv)GetFunc("glUniform2iv", typeof(delegate_Uniform2iv));
            }
            func_Uniform2iv(location, count, value);
        }
        public unsafe static void uniform2iv(Int32 location, int[] value)
        {
            fixed (int* valuePtr = value)
            {
                uniform2iv(location, value.Length / 2, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }
        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*3]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
       // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform3iv")]
        private unsafe delegate void delegate_Uniform3iv(Int32 location, Int32 count, Int32* value);
        private static delegate_Uniform3iv func_Uniform3iv;
        public static unsafe void uniform3iv(Int32 location, Int32 count, Int32* value)
        {
            if (func_Uniform3iv == null)
            {
                func_Uniform3iv = (delegate_Uniform3iv)GetFunc("glUniform3iv", typeof(delegate_Uniform3iv));
            }
            func_Uniform3iv(location, count, value);
        }
        public unsafe static void uniform3iv(Int32 location, int[] value)
        {
            fixed (int* valuePtr = value)
            {
                uniform3iv(location, value.Length / 3, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }


        /// <summary>[requires: v2.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*4]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glUniform4iv")]
        private unsafe delegate void delegate_Uniform4iv(Int32 location, Int32 count, Int32* value);
        private static delegate_Uniform4iv func_Uniform4iv;
        public static unsafe void uniform4iv(Int32 location, Int32 count, Int32* value)
        {
            if (func_Uniform4iv == null)
            {
                func_Uniform4iv = (delegate_Uniform4iv)GetFunc("glUniform4iv", typeof(delegate_Uniform4iv));
            }
            func_Uniform4iv(location, count, value);
        }
        public unsafe static void uniform4iv(Int32 location, int[] value)
        {
            fixed (int* valuePtr = value)
            {
                uniform4iv(location, value.Length / 4, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*1]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform1uiv")]
        private unsafe delegate void delegate_Uniform1uiv(Int32 location, Int32 count, UInt32* value);
        private static delegate_Uniform1uiv func_Uniform1uiv;
        public static unsafe void uniform1uiv(Int32 location, int count, UInt32* value)
        {
            if (func_Uniform1uiv == null)
            {
                func_Uniform1uiv = (delegate_Uniform1uiv)GetFunc("glUniform1uiv", typeof(delegate_Uniform1uiv));
            }
            func_Uniform1uiv(location, count, value);
        }
        public unsafe static void uniform1uiv(Int32 location, uint[] value)
        {
            fixed (uint* valuePtr = value)
            {
                uniform1uiv(location, value.Length, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*2]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform2uiv")]
        private unsafe delegate void delegate_Uniform2uiv(Int32 location, Int32 count, UInt32* value);
        private static delegate_Uniform2uiv func_Uniform2uiv;
        public static unsafe void uniform2uiv(Int32 location, Int32 count, UInt32* value)
        {
            if (func_Uniform2uiv == null)
            {
                func_Uniform2uiv = (delegate_Uniform2uiv)GetFunc("glUniform2uiv", typeof(delegate_Uniform2uiv));
            }
            func_Uniform2uiv(location, count, value);
        }
        public unsafe static void uniform2uiv(Int32 location, uint[] value)
        {
            fixed (uint* valuePtr = value)
            {
                uniform2uiv(location, value.Length / 2, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*3]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform3uiv")]
        private unsafe delegate void delegate_Uniform3uiv(Int32 location, Int32 count, UInt32* value);
        private static delegate_Uniform3uiv func_Uniform3uiv;
        public static unsafe void uniform3uiv(Int32 location, Int32 count, UInt32* value)
        {
            if (func_Uniform3uiv == null)
            {
                func_Uniform3uiv = (delegate_Uniform3uiv)GetFunc("glUniform3uiv", typeof(delegate_Uniform3uiv));
            }
            func_Uniform3uiv(location, count, value);
        }
        public unsafe static void uniform3uiv(Int32 location, uint[] value)
        {
            fixed (uint* valuePtr = value)
            {
                uniform3uiv(location, value.Length / 3, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }

        /// <summary>[requires: v3.0]
        /// Specify the value of a uniform variable for the current program object
        /// </summary>
        /// <param name="location">
        /// Specifies the location of the uniform variable to be modified.
        /// </param>
        /// <param name="count">
        /// For the vector (glUniform*v) commands, specifies the number of elements that are to be modified. This should be 1 if the targeted uniform variable is not an array, and 1 or more if it is an array.  For the matrix (glUniformMatrix*) commands, specifies the number of matrices that are to be modified. This should be 1 if the targeted uniform variable is not an array of matrices, and 1 or more if it is an array of matrices.
        /// </param>
        /// <param name="value">[length: count*4]
        /// For the vector and matrix commands, specifies a pointer to an array of count values that will be used to update the specified uniform variable.
        /// </param>
        // [AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glUniform4uiv")]
        private unsafe delegate void delegate_Uniform4uiv(Int32 location, Int32 count, UInt32* value);
        private static delegate_Uniform4uiv func_Uniform4uiv;
        public static unsafe void uniform4uiv(Int32 location, Int32 count, UInt32* value)
        {
            if (func_Uniform4uiv == null)
            {
                func_Uniform4uiv = (delegate_Uniform4uiv)GetFunc("glUniform4uiv", typeof(delegate_Uniform4uiv));
            }
            func_Uniform4uiv(location, count, value);
        }
        public unsafe static void uniform4uiv(Int32 location, uint[] value)
        {
            fixed (uint* valuePtr = value)
            {
                uniform4uiv(location, value.Length / 4, valuePtr);
                if (DebugEnabled) DebugMethod(null, location, value);
            }
        }


        /// <summary>[requires: v2.0]
        /// Associates a generic vertex attribute index with a named attribute variable
        /// </summary>
        /// <param name="program">
        /// Specifies the handle of the program object in which the association is to be made.
        /// </param>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be bound.
        /// </param>
        /// <param name="name">
        /// Specifies a null terminated string containing the name of the vertex shader attribute variable to which index is to be bound.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glBindAttribLocation")]
        private unsafe delegate void delegate_BindAttribLocation(Int32 program, Int32 index, String name);
        private static delegate_BindAttribLocation func_BindAttribLocation;
        public static void bindAttribLocation(Int32 program, Int32 index, String name)
        {
            if (func_BindAttribLocation == null)
            {
                func_BindAttribLocation = (delegate_BindAttribLocation)GetFunc("glBindAttribLocation", typeof(delegate_BindAttribLocation));
            }
            func_BindAttribLocation(program, index, name);
            if (DebugEnabled) DebugMethod(null, program, index, name);
        }


        /// <summary>[requires: v1.0]
        /// Enable and disable writing of frame buffer color components
        /// </summary>
        /// <param name="red">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        /// <param name="green">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        /// <param name="blue">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        /// <param name="alpha">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glColorMask")]
        private unsafe delegate void delegate_ColorMask(bool red, bool green, bool blue, bool alpha);
        private static delegate_ColorMask func_ColorMask;
        public static void colorMask(bool red, bool green, bool blue, bool alpha)
        {
            if (func_ColorMask == null)
            {
                func_ColorMask = (delegate_ColorMask)GetFunc("glColorMask", typeof(delegate_ColorMask));
            }
            func_ColorMask(red, green, blue, alpha);
            if (DebugEnabled) DebugMethod(null, red, green, blue, alpha);
        }

        /// <summary>[requires: v3.0]
        /// Enable and disable writing of frame buffer color components
        /// </summary>
        /// <param name="index">
        /// For glColorMaski, specifies the index of the draw buffer whose color mask to set.
        /// </param>
        /// <param name="r">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        /// <param name="g">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        /// <param name="b">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        /// <param name="a">
        /// Specify whether red, green, blue, and alpha are to be written into the frame buffer. The initial values are all True, indicating that the color components are written.
        /// </param>
        //[AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glColorMaski")]
        private unsafe delegate void delegate_ColorMaski(Int32 index, bool r, bool g, bool b, bool a);
        private static delegate_ColorMaski func_ColorMaski;
        public static void colorMaski(Int32 index, bool r, bool g, bool b, bool a)
        {
            if (func_ColorMaski == null)
            {
                func_ColorMaski = (delegate_ColorMaski)GetFunc("glColorMaski", typeof(delegate_ColorMaski));
            }
            func_ColorMaski(index, r, g, b, a);
        }

        /// <summary>[requires: v1.0]
        /// Specify the value used for depth buffer comparisons
        /// </summary>
        /// <param name="func">
        /// Specifies the depth comparison function. Symbolic constants Never, Less, Equal, Lequal, Greater, Notequal, Gequal, and Always are accepted. The initial value is Less.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glDepthFunc")]
        private unsafe delegate void delegate_DepthFunc(int func);
        private static delegate_DepthFunc func_DepthFunc;
        public static void depthFunc(int func)
        {
            if (func_DepthFunc == null)
            {
                func_DepthFunc = (delegate_DepthFunc)GetFunc("glDepthFunc", typeof(delegate_DepthFunc));
            }
            func_DepthFunc(func);
            if (DebugEnabled) DebugMethod(null, func).Constants(0);
        }
        public static void depthFunc(DepthFunction func)
        {
            depthFunc((int)func);
        }
        /// <summary>[requires: v1.0]
        /// Enable or disable writing into the depth buffer
        /// </summary>
        /// <param name="flag">
        /// Specifies whether the depth buffer is enabled for writing. If flag is False, depth buffer writing is disabled. Otherwise, it is enabled. Initially, depth buffer writing is enabled.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glDepthMask")]
        private unsafe delegate void delegate_DepthMask(bool flag);
        private static delegate_DepthMask func_DepthMask;
        public static void depthMask(bool flag)
        {
            if (func_DepthMask == null)
            {
                func_DepthMask = (delegate_DepthMask)GetFunc("glDepthMask", typeof(delegate_DepthMask));
            }
            func_DepthMask(flag);
            if (DebugEnabled) DebugMethod(null, flag);
        }


        /// <summary>[requires: v4.1 or ARB_ES2_compatibility|VERSION_4_1]
        /// Specify the clear value for the depth buffer
        /// </summary>
        /// <param name="d">
        /// Specifies the depth value used when the depth buffer is cleared. The initial value is 1.
        /// </param>
        //[AutoGenerated(Category = "ARB_ES2_compatibility|VERSION_4_1", Version = "4.1", EntryPoint = "glClearDepthf")]
        private unsafe delegate void delegate_ClearDepthf(Single d);
        private static delegate_ClearDepthf func_ClearDepthf;
        public static void clearDepth(Single d)
        {
            if (func_ClearDepthf == null)
            {
                func_ClearDepthf = (delegate_ClearDepthf)GetFunc("glClearDepthf", typeof(delegate_ClearDepthf));
            }
            func_ClearDepthf(d);
            if (DebugEnabled) DebugMethod(null, d);
        }

        public static void clearDepth()
        {
            clearDepth(0);
        }



        /// <summary>[requires: v1.0]
        /// Control the front and back writing of individual bits in the stencil planes
        /// </summary>
        /// <param name="mask">
        /// Specifies a bit mask to enable and disable writing of individual bits in the stencil planes. Initially, the mask is all 1's.
        /// </param>
        // [AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glStencilMask")]
        private unsafe delegate void delegate_StencilMask(UInt32 mask);
        private static delegate_StencilMask func_StencilMask;
        public static void stencilMask(UInt32 mask)
        {
            if (func_StencilMask == null)
            {
                func_StencilMask = (delegate_StencilMask)GetFunc("glStencilMask", typeof(delegate_StencilMask));
            }
            func_StencilMask(mask);
            if (DebugEnabled) DebugMethod(null, mask) ;
        }


        /// <summary>[requires: v1.0]
        /// Set front and back function and reference value for stencil testing
        /// </summary>
        /// <param name="func">
        /// Specifies the test function. Eight symbolic constants are valid: Never, Less, Lequal, Greater, Gequal, Equal, Notequal, and Always. The initial value is Always.
        /// </param>
        /// <param name="@ref">
        /// Specifies the reference value for the stencil test. ref is clamped to the range [0, 2 sup n - 1], where  is the number of bitplanes in the stencil buffer. The initial value is 0.
        /// </param>
        /// <param name="mask">
        /// Specifies a mask that is ANDed with both the reference value and the stored stencil value when the test is done. The initial value is all 1's.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glStencilFunc")]
        private unsafe delegate void delegate_StencilFunc(int func, Int32 @ref, UInt32 mask);
        private static delegate_StencilFunc func_StencilFunc;
        public static void stencilFunc(int func, Int32 @ref, UInt32 mask)
        {
            if (func_StencilFunc == null)
            {
                func_StencilFunc = (delegate_StencilFunc)GetFunc("glStencilFunc", typeof(delegate_StencilFunc));
            }
            func_StencilFunc(func, @ref, mask);
            if (DebugEnabled) DebugMethod(null, func, @ref, mask).Constants(0);
        }
        public static void stencilFunc(StencilFunction func, Int32 @ref, UInt32 mask)
        {
            stencilFunc((int)func, @ref, mask);
        }



        /// <summary>[requires: v1.0]
        /// Set front and back stencil test actions
        /// </summary>
        /// <param name="fail">
        /// Specifies the action to take when the stencil test fails. Eight symbolic constants are accepted: Keep, Zero, Replace, Incr, IncrWrap, Decr, DecrWrap, and Invert. The initial value is Keep.
        /// </param>
        /// <param name="zfail">
        /// Specifies the stencil action when the stencil test passes, but the depth test fails. dpfail accepts the same symbolic constants as sfail. The initial value is Keep.
        /// </param>
        /// <param name="zpass">
        /// Specifies the stencil action when both the stencil test and the depth test pass, or when the stencil test passes and either there is no depth buffer or depth testing is not enabled. dppass accepts the same symbolic constants as sfail. The initial value is Keep.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glStencilOp")]
        private unsafe delegate void delegate_StencilFuncStencilOp(int fail, int zfail, int zpass);
        private static delegate_StencilFuncStencilOp func_StencilOp;
        public static void stencilOp(int fail, int zfail, int zpass)
        {
            if (func_StencilOp == null)
            {
                func_StencilOp = (delegate_StencilFuncStencilOp)GetFunc("glStencilOp", typeof(delegate_StencilFuncStencilOp));
            }
            func_StencilOp(fail, zfail, zpass);
            if (DebugEnabled) DebugMethod(null, fail, zfail, zpass).Constants(0,1);
        }
        public static void stencilOp(StencilOp fail, StencilOp zfail, StencilOp zpass)
        {
            stencilOp((int)fail, (int)zfail, (int)zpass);
        }


        /// <summary>[requires: v1.0]
        /// Specify the clear value for the stencil buffer
        /// </summary>
        /// <param name="s">
        /// Specifies the index used when the stencil buffer is cleared. The initial value is 0.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glClearStencil")]
        private unsafe delegate void delegate_ClearStencil(Int32 s);
        private static delegate_ClearStencil func_ClearStencil;
        public static void clearStencil(Int32 s)
        {
            if (func_ClearStencil == null)
            {
                func_ClearStencil = (delegate_ClearStencil)GetFunc("glClearStencil", typeof(delegate_ClearStencil));
            }
            func_ClearStencil(s);
            if (DebugEnabled) DebugMethod(null, s);
        }


        /// <summary>[requires: v1.1]
        /// Generate texture names
        /// </summary>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glGenTextures")]
        private unsafe delegate void delegate_GenTexture(int count, Int32* textures);
        private static delegate_GenTexture func_GenTexture;
        public unsafe static void genTexture(int count, Int32* textures)
        {
            if (func_GenTexture == null)
            {
                func_GenTexture = (delegate_GenTexture)GetFunc("glGenTextures", typeof(delegate_GenTexture));
            }
            func_GenTexture(count, textures);
        }

        public unsafe static GLInt createTexture()
        {
            var txt = new int[1];
            fixed (int* txt_ptr = txt)
            {
                genTexture(1, txt_ptr);
            }
            if (DebugEnabled) DebugMethod(txt[0]);
            return new GLInt { Value = txt[0] };
        }


        ///// <summary>[requires: v3.0]</summary>
        ///// <param name="target"></param>
        ///// <param name="pname"></param>
        ///// <param name="@params">[length: COMPSIZE(pname)]</param>
        //[AutoGenerated(Category = "VERSION_3_0", Version = "3.0", EntryPoint = "glTexParameterIiv")]
        //[CLSCompliant(false)]
        //public unsafe static void texParameteri(int target, int pname, int* @params)
        //{
        //    throw new BindingsNotRewrittenException();
        //}
        //public unsafe static void texParameteri(TextureTarget target, TextureParameterName pname, int* @params)
        //{
        //    throw new BindingsNotRewrittenException();
        //}

        /// <summary>[requires: v1.0]
        /// Set texture parameters
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture, which must be either Texture1D, Texture2D, Texture3D, Texture1DArray, Texture2DArray, TextureRectangle, or TextureCubeMap.
        /// </param>
        /// <param name="pname">
        /// Specifies the symbolic name of a single-valued texture parameter. pname can be one of the following: DepthStencilTextureMode, TextureBaseLevel, TextureCompareFunc, TextureCompareMode, TextureLodBias, TextureMinFilter, TextureMagFilter, TextureMinLod, TextureMaxLod, TextureMaxLevel, TextureSwizzleR, TextureSwizzleG, TextureSwizzleB, TextureSwizzleA, TextureWrapS, TextureWrapT, or TextureWrapR.  For the vector commands (glTexParameter*v), pname can also be one of TextureBorderColor or TextureSwizzleRgba.
        /// </param>
        /// <param name="@params">[length: COMPSIZE(pname)]
        /// For the scalar commands, specifies the value of pname.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glTexParameteri")]
        private unsafe delegate void delegate_TexParameteri(int target, int pname, int @params);
        private static delegate_TexParameteri func_TexParameteri;
        public static unsafe void texParameteri(int target, int pname, int @params)
        {
            if (func_TexParameteri == null)
            {
                func_TexParameteri = (delegate_TexParameteri)GetFunc("glTexParameteri", typeof(delegate_TexParameteri));
            }
            func_TexParameteri(target, pname, @params);
            if (DebugEnabled) DebugMethod(null, target, pname, @params);
        }
        public static unsafe void texParameteri(TextureTarget target, TextureParameterName pname, int @params)
        {
            texParameteri((int)target, (int)pname, @params);
        }



        /// <summary>[requires: v2.0]</summary>
        /// <param name="index"></param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glDisableVertexAttribArray")]
        private unsafe delegate void delegate_DisableVertexAttribArray(Int32 index);
        private static delegate_DisableVertexAttribArray func_DisableVertexAttribArray;
        public static void disableVertexAttribArray(Int32 index)
        {
            if (func_DisableVertexAttribArray == null)
            {
                func_DisableVertexAttribArray = (delegate_DisableVertexAttribArray)GetFunc("glDisableVertexAttribArray", typeof(delegate_DisableVertexAttribArray));
            }
            func_DisableVertexAttribArray(index);
            if (DebugEnabled) DebugMethod(null, index);

        }



        /// <summary>[requires: ARB_draw_buffers_blend]
        /// Specify the equation used for both the RGB blend equation and the Alpha blend equation
        /// </summary>
        /// <param name="buf">
        /// for glBlendEquationi, specifies the index of the draw buffer for which to set the blend equation.
        /// </param>
        /// <param name="mode">
        /// specifies how source and destination colors are combined. It must be FuncAdd, FuncSubtract, FuncReverseSubtract, Min, Max.
        /// </param>
        //[AutoGenerated(Category = "ARB_draw_buffers_blend", Version = "", EntryPoint = "glBlendEquationiARB")]
        private unsafe delegate void delegate_BlendEquationi(Int32 buf, int mode);
        private static delegate_BlendEquationi func_BlendEquationi;
        public static void blendEquationi(Int32 buf, int mode)
        {
            if (func_BlendEquationi == null)
            {
                func_BlendEquationi = (delegate_BlendEquationi)GetFunc("glBlendEquationiARB", typeof(delegate_BlendEquationi));
            }
            func_BlendEquationi(buf, mode);
        }
        public static void blendEquationi(Int32 buf, BlendEquationMode mode)
        {
            blendEquationi(buf, (int)mode);
        }


        /// <summary>[requires: ARB_draw_buffers_blend]
        /// Specify the equation used for both the RGB blend equation and the Alpha blend equation
        /// </summary>
        /// <param name="buf">
        /// for glBlendEquationi, specifies the index of the draw buffer for which to set the blend equation.
        /// </param>
        /// <param name="mode">
        /// specifies how source and destination colors are combined. It must be FuncAdd, FuncSubtract, FuncReverseSubtract, Min, Max.
        /// </param>
        //[AutoGenerated(Category = "ARB_draw_buffers_blend", Version = "", EntryPoint = "glBlendEquation")]
        private unsafe delegate void delegate_BlendEquation(int mode);
        private static delegate_BlendEquation func_BlendEquation;
        public static void blendEquation(int mode)
        {
            if (func_BlendEquation == null)
            {
                func_BlendEquation = (delegate_BlendEquation)GetFunc("glBlendEquation", typeof(delegate_BlendEquation));
            }
            func_BlendEquation(mode);
            if (DebugEnabled) DebugMethod(null, mode).Constants(0);
        }
        public static void blendEquation(BlendEquationMode mode)
        {
            blendEquation((int)mode);
        }

        /// <summary>[requires: ARB_draw_buffers_blend]
        /// Specify pixel arithmetic for RGB and alpha components separately
        /// </summary>
        /// <param name="buf">
        /// For glBlendFuncSeparatei, specifies the index of the draw buffer for which to set the blend functions.
        /// </param>
        /// <param name="srcRGB">
        /// Specifies how the red, green, and blue blending factors are computed. The initial value is One.
        /// </param>
        /// <param name="dstRGB">
        /// Specifies how the red, green, and blue destination blending factors are computed. The initial value is Zero.
        /// </param>
        /// <param name="srcAlpha">
        /// Specified how the alpha source blending factor is computed. The initial value is One.
        /// </param>
        /// <param name="dstAlpha">
        /// Specified how the alpha destination blending factor is computed. The initial value is Zero.
        /// </param>
        //[AutoGenerated(Category = "ARB_draw_buffers_blend", Version = "", EntryPoint = "glBlendFuncSeparateiARB")]
        private unsafe delegate void delegate_BlendFuncSeparatei(Int32 buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha);
        private static delegate_BlendFuncSeparatei func_BlendFuncSeparatei;
        public static void blendFuncSeparatei(Int32 buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha)
        {
            if (func_BlendFuncSeparatei == null)
            {
                func_BlendFuncSeparatei = (delegate_BlendFuncSeparatei)GetFunc("glBlendFuncSeparateiARB", typeof(delegate_BlendFuncSeparatei));
            }
            func_BlendFuncSeparatei(buf, srcRGB, dstRGB, srcAlpha, dstAlpha);

        }

        public static void blendFuncSeparatei(Int32 buf, BlendingFactor srcRGB, BlendingFactor dstRGB, BlendingFactor srcAlpha, BlendingFactor dstAlpha)
        {
            blendFuncSeparatei(buf, (int)srcRGB, (int)dstRGB, (int)srcAlpha, (int)dstAlpha);
        }


        /// <summary>[requires: ARB_draw_buffers_blend]
        /// Specify pixel arithmetic for RGB and alpha components separately
        /// </summary>
        /// <param name="buf">
        /// For glBlendFuncSeparatei, specifies the index of the draw buffer for which to set the blend functions.
        /// </param>
        /// <param name="srcRGB">
        /// Specifies how the red, green, and blue blending factors are computed. The initial value is One.
        /// </param>
        /// <param name="dstRGB">
        /// Specifies how the red, green, and blue destination blending factors are computed. The initial value is Zero.
        /// </param>
        /// <param name="srcAlpha">
        /// Specified how the alpha source blending factor is computed. The initial value is One.
        /// </param>
        /// <param name="dstAlpha">
        /// Specified how the alpha destination blending factor is computed. The initial value is Zero.
        /// </param>
        //[AutoGenerated(Category = "ARB_draw_buffers_blend", Version = "", EntryPoint = "glBlendFuncSeparate")]
        private unsafe delegate void delegate_BlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha);
        private static delegate_BlendFuncSeparate func_BlendFuncSeparate;
        public static void blendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha)
        {
            if (func_BlendFuncSeparate == null)
            {
                func_BlendFuncSeparate = (delegate_BlendFuncSeparate)GetFunc("glBlendFuncSeparate", typeof(delegate_BlendFuncSeparate));
            }
            func_BlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha);
            if (DebugEnabled) DebugMethod(null, srcRGB, dstRGB, srcAlpha, dstAlpha).Constants(0,1,2,3);

        }

        public static void blendFuncSeparate(BlendingFactor srcRGB, BlendingFactor dstRGB, BlendingFactor srcAlpha, BlendingFactor dstAlpha)
        {
            blendFuncSeparate((int)srcRGB, (int)dstRGB, (int)srcAlpha, (int)dstAlpha);
        }


        /// <summary>[requires: v1.0]
        /// Define front- and back-facing polygons
        /// </summary>
        /// <param name="mode">
        /// Specifies the orientation of front-facing polygons. Cw and Ccw are accepted. The initial value is Ccw.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glFrontFace")]
        private unsafe delegate void delegate_FrontFace(int mode);
        private static delegate_FrontFace func_FrontFace;
        public static void frontFace(int mode)
        {
            if (func_FrontFace == null)
            {
                func_FrontFace = (delegate_FrontFace)GetFunc("glFrontFace", typeof(delegate_FrontFace));
            }
            func_FrontFace(mode);
            if (DebugEnabled) DebugMethod(null, mode).Constants(0);
        }

        public static void frontFace(FrontFaceDirection mode)
        {
            frontFace((int)mode);
        }

        /// <summary>[requires: v1.0]
        /// Specify whether front- or back-facing facets can be culled
        /// </summary>
        /// <param name="mode">
        /// Specifies whether front- or back-facing facets are candidates for culling. Symbolic constants Front, Back, and FrontAndBack are accepted. The initial value is Back.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glCullFace")]
        private unsafe delegate void delegate_CullFace(int mode);
        private static delegate_CullFace func_CullFace;
        public static void cullFace(int mode)
        {
            if (func_CullFace == null)
            {
                func_CullFace = (delegate_CullFace)GetFunc("glCullFace", typeof(delegate_CullFace));
            }
            func_CullFace(mode);
            if (DebugEnabled) DebugMethod(null, mode).Constants(0);
        }

        public static void cullFace(CullFaceMode mode)
        {
            cullFace((int)mode);
        }



        /// <summary>[requires: v1.0]
        /// Specify the width of rasterized lines
        /// </summary>
        /// <param name="width">
        /// Specifies the width of rasterized lines. The initial value is 1.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glLineWidth")]
        private unsafe delegate void delegate_LineWidth(Single width);
        private static delegate_LineWidth func_LineWidth;
        public static void lineWidth(Single width)
        {
            if (func_LineWidth == null)
            {
                func_LineWidth = (delegate_LineWidth)GetFunc("glLineWidth", typeof(delegate_LineWidth));
            }
            func_LineWidth(width);
            if (DebugEnabled) DebugMethod(null, width);

        }



        /// <summary>[requires: v1.1]
        /// Set the scale and units used to calculate depth values
        /// </summary>
        /// <param name="factor">
        /// Specifies a scale factor that is used to create a variable depth offset for each polygon. The initial value is 0.
        /// </param>
        /// <param name="units">
        /// Is multiplied by an implementation-specific value to create a constant depth offset. The initial value is 0.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glPolygonOffset")]
        private unsafe delegate void delegate_PolygonOffset(Single factor, Single units);
        private static delegate_PolygonOffset func_PolygonOffset;
        public static void polygonOffset(Single factor, Single units)
        {
            if (func_PolygonOffset == null)
            {
                func_PolygonOffset = (delegate_PolygonOffset)GetFunc("glPolygonOffset", typeof(delegate_PolygonOffset));
            }
            func_PolygonOffset(factor, units);
            if (DebugEnabled) DebugMethod(null, factor, units);
        }



        /// <summary>[requires: v1.3]
        /// Specify a two-dimensional texture image in a compressed format
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture. Must be Texture2D, ProxyTexture2D, Texture1DArray, ProxyTexture1DArray, TextureCubeMapPositiveX, TextureCubeMapNegativeX, TextureCubeMapPositiveY, TextureCubeMapNegativeY, TextureCubeMapPositiveZ, TextureCubeMapNegativeZ, or ProxyTextureCubeMap.
        /// </param>
        /// <param name="level">
        /// Specifies the level-of-detail number. Level 0 is the base image level. Level n is the nth mipmap reduction image.
        /// </param>
        /// <param name="internalformat">
        /// Specifies the format of the compressed image data stored at address data.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the texture image. All implementations support 2D texture and cube map texture images that are at least 16384 texels wide.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the texture image. All implementations support 2D texture and cube map texture images that are at least 16384 texels high.
        /// </param>
        /// <param name="border">
        /// This value must be 0.
        /// </param>
        /// <param name="imageSize">
        /// Specifies the number of unsigned bytes of image data starting at the address specified by data.
        /// </param>
        /// <param name="data">[length: imageSize]
        /// Specifies a pointer to the compressed image data in memory.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_3", Version = "1.3", EntryPoint = "glCompressedTexImage2D")]
        private unsafe delegate void delegate_CompressedTexImage2D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 border, Int32 imageSize, IntPtr data);
        private static delegate_CompressedTexImage2D func_CompressedTexImage2D;
        public static void compressedTexImage2D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 border, Int32 imageSize, IntPtr data)
        {
            if (func_CompressedTexImage2D == null)
            {
                func_CompressedTexImage2D = (delegate_CompressedTexImage2D)GetFunc("glCompressedTexImage2D", typeof(delegate_CompressedTexImage2D));
            }
            func_CompressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data);
            if (DebugEnabled) DebugMethod(null, target, level, internalformat, width, height, border, imageSize, data).Constants(2);
        }
        public static void compressedTexImage2D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 border, byte[] data)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                compressedTexImage2D(target, level, (int)internalformat, width, height, border, data.Length, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }
        public static void compressedTexImage2D(TextureTarget target, Int32 level, InternalFormat internalformat, Int32 width, Int32 height, Int32 border, Int32 imageSize, IntPtr data)
        {
            compressedTexImage2D((int)target, level, (int)internalformat, width, height, border, imageSize, data);
        }
        // [AutoGenerated(Category = "VERSION_1_3", Version = "1.3", EntryPoint = "glCompressedTexSubImage2D")]
        // public static void CompressedTexSubImage2D(TextureTarget target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, PixelFormat format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data) { throw new BindingsNotRewrittenException(); }
        private unsafe delegate void delegate_CompressedTexSubImage2D(int target, Int32 level, int xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 imageSize, IntPtr data);
        private static delegate_CompressedTexSubImage2D func_CompressedTexSubImage2D;
        public static void CompressedTexSubImage2D(int target, Int32 level, int xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 imageSize, IntPtr data)
        {
            if (func_CompressedTexSubImage2D == null)
            {
                func_CompressedTexSubImage2D = (delegate_CompressedTexSubImage2D)GetFunc("glCompressedTexSubImage2D", typeof(delegate_CompressedTexSubImage2D));
            }
            func_CompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data);
        }
        public static void compressedTexSubImage2D(int target, Int32 level, int xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 imageSize, IntPtr data)
        {
            CompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data);
            if (DebugEnabled) DebugMethod(null, target, level, xoffset, yoffset, width, height, format, imageSize, data).Constants(6);
        }
        public static void compressedTexSubImage2D(int target, Int32 level, int xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 imageSize, byte[] data)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                CompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, handle.AddrOfPinnedObject());
                handle.Free();
            }

        }


        // [AutoGenerated(Category = "VERSION_1_3", Version = "1.3", EntryPoint = "glCompressedTexSubImage3D")]
        // public static void CompressedTexSubImage3D(TextureTarget target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, PixelFormat format, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data) { throw new BindingsNotRewrittenException(); }
        private unsafe delegate void delegate_CompressedTexSubImage3D(int target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 imageSize, IntPtr data);
        private static delegate_CompressedTexSubImage3D func_CompressedTexSubImage3D;
        public static void CompressedTexSubImage3D(int target, Int32 level, int xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 imageSize, IntPtr data)
        {
            if (func_CompressedTexSubImage3D == null)
            {
                func_CompressedTexSubImage3D = (delegate_CompressedTexSubImage3D)GetFunc("glCompressedTexSubImage3D", typeof(delegate_CompressedTexSubImage2D));
            }
            func_CompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
        }
        public static void compressedTexSubImage3D(int target, Int32 level, int xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 imageSize, IntPtr data)
        {
            CompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
            if (DebugEnabled) DebugMethod(null, target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data).Constants(8);
        }
        public static void compressedTexSubImage3D(int target, Int32 level, int xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, byte[] data)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                CompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, data.Length, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }

        private unsafe delegate void delegate_CompressedTexImage3D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 imageSize, IntPtr data);
        private static delegate_CompressedTexImage3D func_CompressedTexImage3D;
        // [AutoGenerated(Category = "VERSION_1_3", Version = "1.3", EntryPoint = "glCompressedTexImage3D")]
        public static void CompressedTexImage3D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data)
        {
            if (func_CompressedTexImage3D == null)
            {
                func_CompressedTexImage3D = (delegate_CompressedTexImage3D)GetFunc("glCompressedTexImage3D", typeof(delegate_CompressedTexImage3D));
            }
            func_CompressedTexImage3D(target, level, internalformat, width, height, depth, border, imageSize, data);
            if (DebugEnabled) DebugMethod(null, target, level, internalformat, width, height, depth, border, imageSize, data).Constants(2);

        }
        public static void compressedTexImage3D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 imageSize, [CountAttribute(Parameter = "imageSize")] IntPtr data)
        {
            CompressedTexImage3D(target, level, internalformat, width, height, depth, border, imageSize, data);
        }
        // [AutoGenerated(Category = "ARB_texture_storage|VERSION_4_2", Version = "4.2", EntryPoint = "glTexStorage2D")]
        // public static void TexStorage2D(TextureTarget2d target, Int32 levels, SizedInternalFormat internalformat, Int32 width, Int32 height) { throw new BindingsNotRewrittenException(); }
        private unsafe delegate void delegate_TexStorage2D(int target, Int32 levels, Int32 internalformat, Int32 width, Int32 height);
        private static delegate_TexStorage2D func_TexStorage2D;
        public static void TexStorage2D(int target, Int32 levels, Int32 internalformat, Int32 width, Int32 height)
        {
            if (func_TexStorage2D == null)
            {
                func_TexStorage2D = (delegate_TexStorage2D)GetFunc("glTexStorage2D", typeof(delegate_TexStorage2D));
            }
            func_TexStorage2D(target, levels, internalformat, width, height);

        }
        public static void texStorage2D(int target, Int32 levels, Int32 internalformat, Int32 width, Int32 height)
        {
            TexStorage2D(target, levels, internalformat, width, height);
            if (DebugEnabled) DebugMethod(null, target, levels, internalformat, width, height).Constants(2);
        }

        // [AutoGenerated(Category = "ARB_texture_storage|VERSION_4_2", Version = "4.2", EntryPoint = "glTexStorage3D")]
        // public static void TexStorage3D(TextureTarget3d target, Int32 levels, SizedInternalFormat internalformat, Int32 width, Int32 height, Int32 depth) { throw new BindingsNotRewrittenException(); }

        private unsafe delegate void delegate_TexStorag3D(int target, Int32 levels, Int32 internalformat, Int32 width, Int32 height, Int32 depth);
        private static delegate_TexStorag3D func_TexStorage3D;
        public static void TexStorage3D(int target, Int32 levels, Int32 internalformat, Int32 width, Int32 height, Int32 depth)
        {
            if (func_TexStorage3D == null)
            {
                func_TexStorage3D = (delegate_TexStorag3D)GetFunc("glTexStorage3D", typeof(delegate_TexStorag3D));
            }
            func_TexStorage3D(target, levels, internalformat, width, height, depth);

        }
        public static void texStorage3D(int target, Int32 levels, Int32 internalformat, Int32 width, Int32 height, Int32 depth)
        {
            TexStorage3D(target, levels, internalformat, width, height, depth);
            if (DebugEnabled) DebugMethod(null, target, levels, internalformat, width, height, depth).Constants(2);
        }



        // [AutoGenerated(Category = "ARB_uniform_buffer_object|VERSION_3_1", Version = "3.1", EntryPoint = "glGetUniformBlockIndex")]
        // [CLSCompliant(false)]
        // public static Int32 GetUniformBlockIndex(Int32 program, [CountAttribute(Computed = "")] String uniformBlockName) { throw new BindingsNotRewrittenException(); }
        private unsafe delegate Int32 delegate_GetUniformBlockIndex(Int32 program, String uniformBlockName);
        private static delegate_GetUniformBlockIndex func_GetUniformBlockIndex;
        public static Int32 GetUniformBlockIndex(Int32 program, String uniformBlockName)
        {
            if (func_GetUniformBlockIndex == null)
            {
                func_GetUniformBlockIndex = (delegate_GetUniformBlockIndex)GetFunc("glGetUniformBlockIndex", typeof(delegate_GetUniformBlockIndex));
            }
            var result= func_GetUniformBlockIndex(program, uniformBlockName);
            if (DebugEnabled) DebugMethod(null, program, uniformBlockName);
            return result;
        }
        public static Int32 getUniformBlockIndex(Int32 program, String uniformBlockName)
        {
            return GetUniformBlockIndex(program, uniformBlockName);
            
        }
        // [AutoGenerated(Category = "ARB_uniform_buffer_object|VERSION_3_1", Version = "3.1", EntryPoint = "glUniformBlockBinding")]
        // [CLSCompliant(false)]
        // public static void UniformBlockBinding(Int32 program, Int32 uniformBlockIndex, Int32 uniformBlockBinding) { throw new BindingsNotRewrittenException(); }
        private unsafe delegate void delegate_UniformBlockBinding(Int32 program, Int32 uniformBlockIndex, Int32 uniformBlockBinding);
        private static delegate_UniformBlockBinding func_UniformBlockBinding;
        public static void UniformBlockBinding(Int32 program, Int32 uniformBlockIndex, Int32 uniformBlockBinding)
        {
            if (func_UniformBlockBinding == null)
            {
                func_UniformBlockBinding = (delegate_UniformBlockBinding)GetFunc("glUniformBlockBinding", typeof(delegate_UniformBlockBinding));
            }
            func_UniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding);
        }
        public static void uniformBlockBinding(Int32 program, Int32 uniformBlockIndex, Int32 uniformBlockBinding)
        {
            UniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding);
            if (DebugEnabled) DebugMethod(null, program, uniformBlockIndex, uniformBlockBinding);
        }
        /// <summary>[requires: v1.2]
        /// Specify a three-dimensional texture image
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture. Must be one of Texture3D, ProxyTexture3D, Texture2DArray or ProxyTexture2DArray.
        /// </param>
        /// <param name="level">
        /// Specifies the level-of-detail number. Level 0 is the base image level. Level  is the n sup th mipmap reduction image.
        /// </param>
        /// <param name="internalformat">
        /// Specifies the number of color components in the texture. Must be one of base internal formats given in Table 1, one of the sized internal formats given in Table 2, or one of the compressed internal formats given in Table 3, below.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the texture image. All implementations support 3D texture images that are at least 16 texels wide.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the texture image. All implementations support 3D texture images that are at least 256 texels high.
        /// </param>
        /// <param name="depth">
        /// Specifies the depth of the texture image, or the number of layers in a texture array. All implementations support 3D texture images that are at least 256 texels deep, and texture arrays that are at least 256 layers deep.
        /// </param>
        /// <param name="border">
        /// This value must be 0.
        /// </param>
        /// <param name="format">
        /// Specifies the format of the pixel data. The following symbolic values are accepted: Red, Rg, Rgb, Bgr, Rgba, Bgra, RedInteger, RgInteger, RgbInteger, BgrInteger, RgbaInteger, BgraInteger, StencilIndex, DepthComponent, DepthStencil.
        /// </param>
        /// <param name="type">
        /// Specifies the data type of the pixel data. The following symbolic values are accepted: UnsignedByte, Byte, UnsignedShort, Short, UnsignedInt, Int, Float, UnsignedByte332, UnsignedByte233Rev, UnsignedShort565, UnsignedShort565Rev, UnsignedShort4444, UnsignedShort4444Rev, UnsignedShort5551, UnsignedShort1555Rev, UnsignedInt8888, UnsignedInt8888Rev, UnsignedInt1010102, and UnsignedInt2101010Rev.
        /// </param>
        /// <param name="pixels">[length: COMPSIZE(format,type,width,height,depth)]
        /// Specifies a pointer to the image data in memory.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_2", Version = "1.2", EntryPoint = "glTexImage3D")]
        private delegate void delegate_TexImage3D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, int format, int type, IntPtr pixels);
        private static delegate_TexImage3D func_TexImage3D;
        public static void texImage3D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, int format, int type, IntPtr pixels)
        {
            if (func_TexImage3D == null)
            {
                func_TexImage3D = (delegate_TexImage3D)GetFunc("glTexImage3D", typeof(delegate_TexImage3D));
            }
            func_TexImage3D(target, level, internalformat, width, height, depth, border, format, type, pixels);
            if (DebugEnabled) DebugMethod(null, target, level, internalformat, width, height, depth, border, format, type, pixels).Constants(2,7,8);
        }
        public static void texImage3D(int target, Int32 level, int internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, int format, int type, byte[] pixels)
        {
            var handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                texImage3D(target, level, internalformat, width, height, depth, border, format, type, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }
        public static void texImage3D(TextureTarget target, Int32 level, PixelInternalFormat internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, PixelFormat format, PixelType type, IntPtr pixels)
        {
            texImage3D((int)target, level, (int)internalformat, width, height, depth, border, (int)format, (int)type, pixels);

        }

        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Generate mipmaps for a specified texture target
        /// </summary>
        /// <param name="target">
        /// Specifies the target to which the texture whose mimaps to generate is bound. target must be Texture1D, Texture2D, Texture3D, Texture1DArray, Texture2DArray or TextureCubeMap.
        /// </param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glGenerateMipmap")]
        private delegate void delegate_GenerateMipmap(int target);
        private static delegate_GenerateMipmap func_GenerateMipmap;
        public static void generateMipmap(int target)
        {
            if (func_GenerateMipmap == null)
            {
                func_GenerateMipmap = (delegate_GenerateMipmap)GetFunc("glGenerateMipmap", typeof(delegate_GenerateMipmap));
            }
            func_GenerateMipmap(target);
            if (DebugEnabled) DebugMethod(null, target ) ;
        }

        public static void generateMipmap(GenerateMipmapTarget target)
        {
            generateMipmap((int)target);
        }


        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Delete framebuffer objects
        /// </summary>
        /// <param name="framebuffers">[length: n]
        /// A pointer to an array containing n framebuffer objects to be deleted.
        /// </param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glDeleteFramebuffers")]
        private unsafe delegate void delegate_DeleteFramebuffers(int n, int* framebuffers);
        private static delegate_DeleteFramebuffers func_DeleteFramebuffers;

        public unsafe static void deleteFramebuffers(int n, int* framebuffers)
        {
            if (func_DeleteFramebuffers == null)
            {
                func_DeleteFramebuffers = (delegate_DeleteFramebuffers)GetFunc("glDeleteFramebuffers", typeof(delegate_DeleteFramebuffers));
            }
            func_DeleteFramebuffers(n, framebuffers);
        }
        public unsafe static void deleteFramebuffer(Int32 framebuffers)
        {
            var bufs = new int[1] { framebuffers };
            fixed (int* bufPtr = bufs)
            {
                deleteFramebuffers(1, bufPtr);
            }
            if (DebugEnabled) DebugMethod(null, framebuffers);

        }

        //[AutoGenerated(Category = "ARB_geometry_shader4", Version = "", EntryPoint = "glFramebufferTextureLayerARB")]
        //     [CLSCompliant(false)]
        //     public static void FramebufferTextureLayer(FramebufferTarget target, FramebufferAttachment attachment, Int32 texture, Int32 level, Int32 layer) { throw new BindingsNotRewrittenException(); }
        private unsafe delegate void delegate_FramebufferTextureLayer(Int32 target, Int32 attachment, Int32 texture, Int32 level, Int32 layer);
        private static delegate_FramebufferTextureLayer func_FramebufferTextureLayer;

        public static void FramebufferTextureLayer(Int32 target, Int32 attachment, Int32 texture, Int32 level, Int32 layer)
        {
            if (func_FramebufferTextureLayer == null)
            {
                func_FramebufferTextureLayer = (delegate_FramebufferTextureLayer)GetFunc("glFramebufferTextureLayerARB", typeof(delegate_FramebufferTextureLayer));
            }
            func_FramebufferTextureLayer(target, attachment, texture, level, layer);
        }

        public static void framebufferTextureLayer(Int32 target, Int32 attachment, Int32 texture, Int32 level, Int32 layer)
        {
            FramebufferTextureLayer(target, attachment, texture, level, layer);
            if (DebugEnabled) DebugMethod(null, target, attachment, texture, level, layer).Constants(0,1);
        }

        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Delete renderbuffer objects
        /// </summary>
        /// <param name="renderbuffers">[length: n]
        /// A pointer to an array containing n renderbuffer objects to be deleted.
        /// </param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glDeleteRenderbuffers")]
        private unsafe delegate void delegate_DeleteRenderbuffers(int n, int* renderbuffers);
        private static delegate_DeleteRenderbuffers func_DeleteRenderbuffers;

        public unsafe static void deleteRenderbuffers(int n, int* framebuffers)
        {
            if (func_DeleteRenderbuffers == null)
            {
                func_DeleteRenderbuffers = (delegate_DeleteRenderbuffers)GetFunc("glDeleteRenderbuffers", typeof(delegate_DeleteRenderbuffers));
            }
            func_DeleteRenderbuffers(n, framebuffers);
        }
        public unsafe static void deleteRenderbuffer(Int32 renderbuffers)
        {
            var bufs = new int[1] { renderbuffers };
            fixed (int* bufPtr = bufs)
            {
                deleteRenderbuffers(1, bufPtr);
            }
            if (DebugEnabled) DebugMethod(null, renderbuffers);

        }

        private unsafe delegate void delegate_PixelStorei(int pname, int param);
        private static delegate_PixelStorei func_PixelStorei;
        public static void pixelStorei(int pname, int param)
        {
            //not found in GL4
            if (func_PixelStorei == null)
            {
                func_PixelStorei = (delegate_PixelStorei)GetFunc("glPixelStorei", typeof(delegate_PixelStorei));
            }
            func_PixelStorei(pname, param);
            if (DebugEnabled) DebugMethod(null, pname, param).Constants(0);
        }

        /// <summary>[requires: v1.0]
        /// Set texture parameters
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture, which must be either Texture1D, Texture2D, Texture3D, Texture1DArray, Texture2DArray, TextureRectangle, or TextureCubeMap.
        /// </param>
        /// <param name="pname">
        /// Specifies the symbolic name of a single-valued texture parameter. pname can be one of the following: DepthStencilTextureMode, TextureBaseLevel, TextureCompareFunc, TextureCompareMode, TextureLodBias, TextureMinFilter, TextureMagFilter, TextureMinLod, TextureMaxLod, TextureMaxLevel, TextureSwizzleR, TextureSwizzleG, TextureSwizzleB, TextureSwizzleA, TextureWrapS, TextureWrapT, or TextureWrapR.  For the vector commands (glTexParameter*v), pname can also be one of TextureBorderColor or TextureSwizzleRgba.
        /// </param>
        /// <param name="param">
        /// For the scalar commands, specifies the value of pname.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glTexParameterf")]
        private unsafe delegate void delegate_TexParameterf(int target, int pname, Single param);
        private static delegate_TexParameterf func_TexParameterf;
        public static void texParameterf(int target, int pname, Single param)
        {
            if (func_TexParameterf == null)
            {
                func_TexParameterf = (delegate_TexParameterf)GetFunc("glTexParameterf", typeof(delegate_TexParameterf));
            }
            func_TexParameterf(target, pname, param);
            if (DebugEnabled) DebugMethod(null, target, pname, param);
        }

        public static void texParameterf(TextureTarget target, TextureParameterName pname, Single param)
        {
            texParameterf((int)target, (int)pname, param);
        }


        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Bind a framebuffer to a framebuffer target
        /// </summary>
        /// <param name="target">
        /// Specifies the framebuffer target of the binding operation.
        /// </param>
        /// <param name="framebuffer">
        /// Specifies the name of the framebuffer object to bind.
        /// </param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glBindFramebuffer")]
        private unsafe delegate void delegate_BindFramebuffer(int target, Int32 framebuffer);
        private static delegate_BindFramebuffer func_BindFramebuffer;
        public static void bindFramebuffer(int target, Int32 framebuffer)
        {
            if (func_BindFramebuffer == null)
            {
                func_BindFramebuffer = (delegate_BindFramebuffer)GetFunc("glBindFramebuffer", typeof(delegate_BindFramebuffer));
            }
            func_BindFramebuffer(target, framebuffer);
            if (DebugEnabled) DebugMethod(null, target, framebuffer);
        }
        public static void bindFramebuffer(FramebufferTarget target, Int32 framebuffer)
        {
            bindFramebuffer((int)target, framebuffer);

        }


        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]</summary>
        /// <param name="target"></param>
        /// <param name="attachment"></param>
        /// <param name="textarget"></param>
        /// <param name="texture"></param>
        /// <param name="level"></param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glFramebufferTexture2D")]
        private unsafe delegate void delegate_FramebufferTexture2D(int target, int attachment, int textarget, Int32 texture, Int32 level);
        private static delegate_FramebufferTexture2D func_FramebufferTexture2D;
        public static void framebufferTexture2D(int target, int attachment, int textarget, Int32 texture, Int32 level)
        {
            if (func_FramebufferTexture2D == null)
            {
                func_FramebufferTexture2D = (delegate_FramebufferTexture2D)GetFunc("glFramebufferTexture2D", typeof(delegate_FramebufferTexture2D));
            }
            func_FramebufferTexture2D(target, attachment, textarget, texture, level);
            if (DebugEnabled) DebugMethod(null, target, attachment, textarget, texture, level).Constants(0,1,2);
        }

        public static void framebufferTexture2D(FramebufferTarget target, FramebufferAttachment attachment, TextureTarget textarget, Int32 texture, Int32 level)
        {
            framebufferTexture2D((int)target, (int)attachment, (int)textarget, texture, level);
        }



        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Bind a renderbuffer to a renderbuffer target
        /// </summary>
        /// <param name="target">
        /// Specifies the renderbuffer target of the binding operation. target must be Renderbuffer.
        /// </param>
        /// <param name="renderbuffer">
        /// Specifies the name of the renderbuffer object to bind.
        /// </param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glBindRenderbuffer")]
        private unsafe delegate void delegate_BindRenderbuffer(int target, Int32 renderbuffer);
        private static delegate_BindRenderbuffer func_BindRenderbuffer;
        public static void bindRenderbuffer(int target, Int32 renderbuffer)
        {
            if (func_BindRenderbuffer == null)
            {
                func_BindRenderbuffer = (delegate_BindRenderbuffer)GetFunc("glBindRenderbuffer", typeof(delegate_BindRenderbuffer));
            }
            func_BindRenderbuffer(target, renderbuffer);
            if (DebugEnabled) DebugMethod(null, target, renderbuffer).Constants(0);

        }
        public static void bindRenderbuffer(RenderbufferTarget target, Int32 renderbuffer)
        {
            bindRenderbuffer((int)target, renderbuffer);
        }


        /// <summary>[requires: v4.5 or ARB_direct_state_access|VERSION_4_5]</summary>
        /// <param name="renderbuffer"></param>
        /// <param name="internalformat"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        //[AutoGenerated(Category = "ARB_direct_state_access|VERSION_4_5", Version = "4.5", EntryPoint = "glNamedRenderbufferStorage")]
        private unsafe delegate void delegate_NamedRenderbufferStorage(Int32 renderbuffer, int internalformat, Int32 width, Int32 height);
        private static delegate_NamedRenderbufferStorage func_NamedRenderbufferStorage;
        public static void namedRenderbufferStorage(Int32 renderbuffer, int internalformat, Int32 width, Int32 height)
        {

            if (func_NamedRenderbufferStorage == null)
            {
                func_NamedRenderbufferStorage = (delegate_NamedRenderbufferStorage)GetFunc("glNamedRenderbufferStorage", typeof(delegate_NamedRenderbufferStorage));
            }
            func_NamedRenderbufferStorage(renderbuffer, internalformat, width, height);

        }
        public static void namedRenderbufferStorage(Int32 renderbuffer, RenderbufferStorage internalformat, Int32 width, Int32 height)
        {
            namedRenderbufferStorage(renderbuffer, (int)internalformat, width, height);
        }

        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Establish data storage, format and dimensions of a renderbuffer object's image
        /// </summary>
        /// <param name="target">
        /// Specifies a binding to which the target of the allocation and must be Renderbuffer.
        /// </param>
        /// <param name="internalformat">
        /// Specifies the internal format to use for the renderbuffer object's image.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the renderbuffer, in pixels.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the renderbuffer, in pixels.
        /// </param>
        // [AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glRenderbufferStorage")]
        private unsafe delegate void delegate_RenderbufferStorage(int target, int internalformat, Int32 width, Int32 height);
        private static delegate_RenderbufferStorage func_RenderbufferStorage;
        public static void renderbufferStorage(int target, int internalformat, Int32 width, Int32 height)
        {
            if (func_RenderbufferStorage == null)
            {
                func_RenderbufferStorage = (delegate_RenderbufferStorage)GetFunc("glRenderbufferStorage", typeof(delegate_RenderbufferStorage));
            }
            func_RenderbufferStorage(target, internalformat, width, height);
            if (DebugEnabled) DebugMethod(null, target, internalformat, width, height).Constants(0,1);
        }
        public static void renderbufferStorage(RenderbufferTarget target, RenderbufferStorage internalformat, Int32 width, Int32 height)
        {
            renderbufferStorage((int)target, (int)internalformat, width, height);
        }

        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Establish data storage, format, dimensions and sample count of a renderbuffer object's image
        /// </summary>
        /// <param name="target">
        /// Specifies a binding to which the target of the allocation and must be Renderbuffer.
        /// </param>
        /// <param name="samples">
        /// Specifies the number of samples to be used for the renderbuffer object's storage.
        /// </param>
        /// <param name="internalformat">
        /// Specifies the internal format to use for the renderbuffer object's image.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the renderbuffer, in pixels.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the renderbuffer, in pixels.
        /// </param>
        // [AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glRenderbufferStorageMultisample")]
        private unsafe delegate void delegate_RenderbufferStorageMultisample(int target, Int32 samples, Int32 internalformat, Int32 width, Int32 height);
        private static delegate_RenderbufferStorageMultisample func_RenderbufferStorageMultisample;
        public static void renderbufferStorageMultisample(int target, Int32 samples, Int32 internalformat, Int32 width, Int32 height)
        {
            if (func_RenderbufferStorageMultisample == null)
            {
                func_RenderbufferStorageMultisample = (delegate_RenderbufferStorageMultisample)GetFunc("glRenderbufferStorageMultisample", typeof(delegate_RenderbufferStorageMultisample));
            }
            func_RenderbufferStorageMultisample(target, samples, internalformat, width, height);
            if (DebugEnabled) DebugMethod(null, target, samples, internalformat, width, height).Constants(0, 2);
        }
        public static void renderbufferStorageMultisample(RenderbufferTarget target, Int32 samples, RenderbufferStorage internalformat, Int32 width, Int32 height)
        {
            renderbufferStorageMultisample((int)target, samples, (int)internalformat, width, height);
        }



        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Attach a renderbuffer as a logical buffer to the currently bound framebuffer object
        /// </summary>
        /// <param name="target">
        /// Specifies the framebuffer target. target must be DrawFramebuffer, ReadFramebuffer, or Framebuffer. Framebuffer is equivalent to DrawFramebuffer.
        /// </param>
        /// <param name="attachment">
        /// Specifies the attachment point of the framebuffer.
        /// </param>
        /// <param name="renderbuffertarget">
        /// Specifies the renderbuffer target and must be Renderbuffer.
        /// </param>
        /// <param name="renderbuffer">
        /// Specifies the name of an existing renderbuffer object of type renderbuffertarget to attach.
        /// </param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glFramebufferRenderbuffer")]
        private unsafe delegate void delegate_FramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, Int32 renderbuffer);
        private static delegate_FramebufferRenderbuffer func_FramebufferRenderbuffer;
        public static void framebufferRenderbuffer(int target, int attachment, int renderbuffertarget, Int32 renderbuffer)
        {

            if (func_FramebufferRenderbuffer == null)
            {
                func_FramebufferRenderbuffer = (delegate_FramebufferRenderbuffer)GetFunc("glFramebufferRenderbuffer", typeof(delegate_FramebufferRenderbuffer));
            }
            func_FramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer);
            if (DebugEnabled) DebugMethod(null, target, attachment, renderbuffertarget, renderbuffer).Constants(0,1, 2);

        }
        public static void framebufferRenderbuffer(FramebufferTarget target, FramebufferAttachment attachment, RenderbufferTarget renderbuffertarget, Int32 renderbuffer)
        {

            framebufferRenderbuffer((int)target, (int)attachment, (int)renderbuffertarget, renderbuffer);
        }

        /// <summary>[requires: v4.3 or ARB_invalidate_subdata|VERSION_4_3]
        /// Invalidate the content some or all of a framebuffer object's attachments
        /// </summary>
        /// <param name="target">
        /// The target to which the framebuffer is attached. target must be Framebuffer, DrawFramebuffer, or ReadFramebuffer.
        /// </param>
        /// <param name="numAttachments">
        /// The number of entries in the attachments array.
        /// </param>
        /// <param name="attachments">[length: numAttachments]
        /// The address of an array identifying the attachments to be invalidated.
        /// </param>
        // [AutoGenerated(Category = "ARB_invalidate_subdata|VERSION_4_3", Version = "4.3", EntryPoint = "glInvalidateFramebuffer")]
        private unsafe delegate void delegate_InvalidateFramebuffer(int target, Int32 numAttachments, int[] attachments);
        private static delegate_InvalidateFramebuffer func_InvalidateFramebuffer;
        public static void invalidateFramebuffer(int target, int numAttachments, int[] attachments)
        {

            if (func_InvalidateFramebuffer == null)
            {
                func_InvalidateFramebuffer = (delegate_InvalidateFramebuffer)GetFunc("glInvalidateFramebuffer", typeof(delegate_InvalidateFramebuffer));
            }
            func_InvalidateFramebuffer(target, numAttachments, attachments);
            if (DebugEnabled) DebugMethod(null, target, numAttachments, attachments).Constants(0);

        }
        public static void invalidateFramebuffer(FramebufferTarget target, Int32 numAttachments, FramebufferAttachment[] attachments)
        {

            invalidateFramebuffer((int)target, (int)numAttachments, attachments.Cast<int>().ToArray());
        }


        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Generate renderbuffer object names
        /// </summary>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glGenRenderbuffers")]
        private unsafe delegate void delegate_GenRenderbuffers(int n, int* buffers);
        private static delegate_GenRenderbuffers func_GenRenderbuffers;
        public unsafe static void genRenderbuffer(int n, int* buffers)
        {
            if (func_GenRenderbuffers == null)
            {
                func_GenRenderbuffers = (delegate_GenRenderbuffers)GetFunc("glGenRenderbuffers", typeof(delegate_GenRenderbuffers));
            }
            func_GenRenderbuffers(n, buffers);
        }
        public unsafe static GLInt createRenderbuffer()
        {
            var buffers = new int[1];
            fixed (int* bufPtr = buffers)
            {
                genRenderbuffer(1, bufPtr);
            }
            if (DebugEnabled) DebugMethod(buffers[0]);
            return new GLInt { Value = buffers[0] };
        }


        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Generate framebuffer object names
        /// </summary>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glGenFramebuffers")]
        private unsafe delegate void delegate_GenFramebuffers(int n, int* framebuffers);
        private static delegate_GenFramebuffers func_GenFramebuffers;
        // public static unsafe void GenFramebuffers(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] Int32* framebuffers) { throw new BindingsNotRewrittenException(); }
        public unsafe static void genFramebuffers(int n, int* framebuffers)
        {
            if (func_GenFramebuffers == null)
            {
                func_GenFramebuffers = (delegate_GenFramebuffers)GetFunc("glGenFramebuffers", typeof(delegate_GenFramebuffers));
            }
            func_GenFramebuffers(n, framebuffers);
        }
        public unsafe static GLInt createFramebuffer()
        {
            var bufs = new int[1];
            fixed (int* bufPtr = bufs)
            {
                genFramebuffers(1, bufPtr);
            }
            if (DebugEnabled) DebugMethod(bufs[0]);
            return new GLInt { Value = bufs[0] };
        }

        // /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        // /// Copy a block of pixels from the read framebuffer to the draw framebuffer
        // /// </summary>
        // /// <param name="srcX0">
        // /// Specify the bounds of the source rectangle within the read buffer of the read framebuffer.
        // /// </param>
        // /// <param name="srcY0">
        // /// Specify the bounds of the source rectangle within the read buffer of the read framebuffer.
        // /// </param>
        // /// <param name="srcX1">
        // /// Specify the bounds of the source rectangle within the read buffer of the read framebuffer.
        // /// </param>
        // /// <param name="srcY1">
        // /// Specify the bounds of the source rectangle within the read buffer of the read framebuffer.
        // /// </param>
        // /// <param name="dstX0">
        // /// Specify the bounds of the destination rectangle within the write buffer of the write framebuffer.
        // /// </param>
        // /// <param name="dstY0">
        // /// Specify the bounds of the destination rectangle within the write buffer of the write framebuffer.
        // /// </param>
        // /// <param name="dstX1">
        // /// Specify the bounds of the destination rectangle within the write buffer of the write framebuffer.
        // /// </param>
        // /// <param name="dstY1">
        // /// Specify the bounds of the destination rectangle within the write buffer of the write framebuffer.
        // /// </param>
        // /// <param name="mask">
        // /// The bitwise OR of the flags indicating which buffers are to be copied. The allowed flags are ColorBufferBit, DepthBufferBit and StencilBufferBit.
        // /// </param>
        // /// <param name="filter">
        // /// Specifies the interpolation to be applied if the image is stretched. Must be Nearest or Linear.
        // /// </param>
        // [AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glBlitFramebuffer")]
        private unsafe delegate void delegate_BlitFramebuffer(Int32 srcX0, Int32 srcY0, Int32 srcX1, Int32 srcY1, Int32 dstX0, Int32 dstY0, Int32 dstX1, Int32 dstY1, int mask, int filter);
        private static delegate_BlitFramebuffer func_BlitFramebuffer;
        public unsafe static void blitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter)
        {
            if (func_BlitFramebuffer == null)
            {
                func_BlitFramebuffer = (delegate_BlitFramebuffer)GetFunc("glBlitFramebuffer", typeof(delegate_BlitFramebuffer));
            }
            func_BlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
            if (DebugEnabled) DebugMethod(null, srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
        }

        public unsafe static void blitFramebuffer(Int32 srcX0, Int32 srcY0, Int32 srcX1, Int32 srcY1, Int32 dstX0, Int32 dstY0, Int32 dstX1, Int32 dstY1, ClearBufferMask mask, BlitFramebufferFilter filter)
        {
            blitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, (int)mask, (int)filter);
        }

        /// <summary>[requires: v1.0]
        /// Define the scissor box
        /// </summary>
        /// <param name="x">
        /// Specify the lower left corner of the scissor box. Initially (0, 0).
        /// </param>
        /// <param name="y">
        /// Specify the lower left corner of the scissor box. Initially (0, 0).
        /// </param>
        /// <param name="width">
        /// Specify the width and height of the scissor box. When a GL context is first attached to a window, width and height are set to the dimensions of that window.
        /// </param>
        /// <param name="height">
        /// Specify the width and height of the scissor box. When a GL context is first attached to a window, width and height are set to the dimensions of that window.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glScissor")]
        private unsafe delegate void delegate_Scissor(Int32 x, Int32 y, Int32 width, Int32 height);
        private static delegate_Scissor func_Scissor;
        public static void scissor(Int32 x, Int32 y, Int32 width, Int32 height)
        {
            if (func_Scissor == null)
            {
                func_Scissor = (delegate_Scissor)GetFunc("glScissor", typeof(delegate_Scissor));
            }
            func_Scissor(x, y, width, height);
            if (DebugEnabled) DebugMethod(null, x, y, width, height);
        }








        /// <summary>[requires: v2.0]
        /// Specifies the value of a generic vertex attribute
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="v">[length: 1]
        /// For the vector commands (glVertexAttrib*v), specifies a pointer to an array of values to be used for the generic vertex attribute.
        /// </param>
        // [AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttrib1fv")]
        private unsafe delegate void delegate_VertexAttrib1fv(Int32 index, Single* v);
        private static delegate_VertexAttrib1fv func_VertexAttrib1fv;
        public static unsafe void vertexAttrib1fv(Int32 index, Single* v)
        {
            if (func_VertexAttrib1fv == null)
            {
                func_VertexAttrib1fv = (delegate_VertexAttrib1fv)GetFunc("glVertexAttrib1fv", typeof(delegate_VertexAttrib1fv));
            }
            func_VertexAttrib1fv(index, v);
        }
        public static unsafe void vertexAttrib1fv(Int32 index, float[] v)
        {
            fixed (float* vPtr = v)
            {
                vertexAttrib1fv(index, vPtr);
            }
            if (DebugEnabled) DebugMethod(null, index,v);
        }


        /// <summary>[requires: v2.0]
        /// Specifies the value of a generic vertex attribute
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="v">[length: 2]
        /// For the vector commands (glVertexAttrib*v), specifies a pointer to an array of values to be used for the generic vertex attribute.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttrib2fv")]
        private unsafe delegate void delegate_VertexAttrib2fv(Int32 index, Single* v);
        private static delegate_VertexAttrib2fv func_VertexAttrib2fv;
        public static unsafe void vertexAttrib2fv(Int32 index, Single* v)
        {
            if (func_VertexAttrib2fv == null)
            {
                func_VertexAttrib2fv = (delegate_VertexAttrib2fv)GetFunc("glVertexAttrib2fv", typeof(delegate_VertexAttrib2fv));
            }
            func_VertexAttrib2fv(index, v);
        }
        public static unsafe void vertexAttrib2fv(Int32 index, float[] v)
        {
            fixed (float* vPtr = v)
            {
                vertexAttrib2fv(index, vPtr);
            }
            if (DebugEnabled) DebugMethod(null, index, v);
        }

        /// <summary>[requires: v2.0]
        /// Specifies the value of a generic vertex attribute
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="v">[length: 2]
        /// For the vector commands (glVertexAttrib*v), specifies a pointer to an array of values to be used for the generic vertex attribute.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttrib3fv")]
        private unsafe delegate void delegate_VertexAttrib3fv(Int32 index, Single* v);
        private static delegate_VertexAttrib3fv func_VertexAttrib3fv;
        public static unsafe void vertexAttrib3fv(Int32 index, Single* v)
        {
            if (func_VertexAttrib3fv == null)
            {
                func_VertexAttrib3fv = (delegate_VertexAttrib3fv)GetFunc("glVertexAttrib3fv", typeof(delegate_VertexAttrib3fv));
            }
            func_VertexAttrib3fv(index, v);
        }
        public static unsafe void vertexAttrib3fv(Int32 index, float[] v)
        {
            fixed (float* vPtr = v)
            {
                vertexAttrib3fv(index, vPtr);
            }
            if (DebugEnabled) DebugMethod(null, index, v);
        }



        /// <summary>[requires: v2.0]
        /// Specifies the value of a generic vertex attribute
        /// </summary>
        /// <param name="index">
        /// Specifies the index of the generic vertex attribute to be modified.
        /// </param>
        /// <param name="v">[length: 2]
        /// For the vector commands (glVertexAttrib*v), specifies a pointer to an array of values to be used for the generic vertex attribute.
        /// </param>
        //[AutoGenerated(Category = "VERSION_2_0", Version = "2.0", EntryPoint = "glVertexAttrib4fv")]
        private unsafe delegate void delegate_VertexAttrib4fv(Int32 index, Single* v);
        private static delegate_VertexAttrib4fv func_VertexAttrib4fv;
        public static unsafe void vertexAttrib4fv(Int32 index, Single* v)
        {
            if (func_VertexAttrib4fv == null)
            {
                func_VertexAttrib4fv = (delegate_VertexAttrib4fv)GetFunc("glVertexAttrib4fv", typeof(delegate_VertexAttrib4fv));
            }
            func_VertexAttrib4fv(index, v);
        }
        public static unsafe void vertexAttrib4fv(Int32 index, float[] v)
        {
            fixed (float* vPtr = v)
            {
                vertexAttrib4fv(index, vPtr);
            }
            if (DebugEnabled) DebugMethod(null, index, v);
        }


        /// <summary>[requires: v1.0]
        /// Block until all GL execution is complete
        /// </summary>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glFinish")]
        private unsafe delegate void delegate_Finish();
        private static delegate_Finish func_Finish;
        public static void finish()
        {
            if (func_Finish == null)
            {
                func_Finish = (delegate_Finish)GetFunc("glFinish", typeof(delegate_Finish));
            }
            func_Finish();
        }

        // /// <summary>[requires: v1.0]
        // /// Force execution of GL commands in finite time
        // /// </summary>
        // [AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glFlush")]
        // public static void Flush() { throw new BindingsNotRewrittenException(); }
        private unsafe delegate void delegate_Flush();
        private static delegate_Flush func_Flush;
        public static void flush()
        {
            if (func_Flush == null)
            {
                func_Flush = (delegate_Flush)GetFunc("glFlush", typeof(delegate_Flush));
            }
            func_Flush();
        }


        /// <summary>[requires: v3.0 or ARB_framebuffer_object|VERSION_3_0]
        /// Check the completeness status of a framebuffer
        /// </summary>
        /// <param name="target">
        /// Specify the target of the framebuffer completeness check.
        /// </param>
        //[AutoGenerated(Category = "ARB_framebuffer_object|VERSION_3_0", Version = "3.0", EntryPoint = "glCheckFramebufferStatus")]
        private unsafe delegate int delegate_CheckFramebufferStatus(int target);
        private static delegate_CheckFramebufferStatus func_CheckFramebufferStatus;
        public static int checkFramebufferStatus(int target)
        {
            if (func_CheckFramebufferStatus == null)
            {
                func_CheckFramebufferStatus = (delegate_CheckFramebufferStatus)GetFunc("glCheckFramebufferStatus", typeof(delegate_CheckFramebufferStatus));
            }
            return func_CheckFramebufferStatus(target);
        }
        public static FramebufferErrorCode checkFramebufferStatus(FramebufferTarget target)
        {

            return (FramebufferErrorCode)checkFramebufferStatus((int)target);
        }



        /// <summary>[requires: v1.0]
        /// Read a block of pixels from the frame buffer
        /// </summary>
        /// <param name="x">
        /// Specify the window coordinates of the first pixel that is read from the frame buffer. This location is the lower left corner of a rectangular block of pixels.
        /// </param>
        /// <param name="y">
        /// Specify the window coordinates of the first pixel that is read from the frame buffer. This location is the lower left corner of a rectangular block of pixels.
        /// </param>
        /// <param name="width">
        /// Specify the dimensions of the pixel rectangle. width and height of one correspond to a single pixel.
        /// </param>
        /// <param name="height">
        /// Specify the dimensions of the pixel rectangle. width and height of one correspond to a single pixel.
        /// </param>
        /// <param name="format">
        /// Specifies the format of the pixel data. The following symbolic values are accepted: StencilIndex, DepthComponent, DepthStencil, Red, Green, Blue, Rgb, Bgr, Rgba, and Bgra.
        /// </param>
        /// <param name="type">
        /// Specifies the data type of the pixel data. Must be one of UnsignedByte, Byte, UnsignedShort, Short, UnsignedInt, Int, HalfFloat, Float, UnsignedByte332, UnsignedByte233Rev, UnsignedShort565, UnsignedShort565Rev, UnsignedShort4444, UnsignedShort4444Rev, UnsignedShort5551, UnsignedShort1555Rev, UnsignedInt8888, UnsignedInt8888Rev, UnsignedInt1010102, UnsignedInt2101010Rev, UnsignedInt248, UnsignedInt10F11F11FRev, UnsignedInt5999Rev, or Float32UnsignedInt248Rev.
        /// </param>
        /// <param name="pixels">[length: COMPSIZE(format,type,width,height)]
        /// Returns the pixel data.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glReadPixels")]
        private unsafe delegate void delegate_ReadPixels(Int32 x, Int32 y, Int32 width, Int32 height, int format, int type, IntPtr pixels);
        private static delegate_ReadPixels func_ReadPixels;
        public static void readPixels(Int32 x, Int32 y, Int32 width, Int32 height, int format, int type, IntPtr pixels)
        {
            if (func_ReadPixels == null)
            {
                func_ReadPixels = (delegate_ReadPixels)GetFunc("glReadPixels", typeof(delegate_ReadPixels));
            }
            func_ReadPixels(x, y, width, height, format, type, pixels);
        }

        public static void readPixels(Int32 x, Int32 y, Int32 width, Int32 height, PixelFormat format, PixelType type, IntPtr pixels)
        {
            readPixels(x, y, width, height, (int)format, (int)type, pixels);
        }
        public static void readPixels(Int32 x, Int32 y, Int32 width, Int32 height, int format, int type, byte[] pixels)
        {
            var handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                readPixels(x, y, width, height, format, type, handle.AddrOfPinnedObject());
                handle.Free();
            }
            if (DebugEnabled) DebugMethod(null, x, y, width, height, format, type, pixels);
        }
        public static void readPixels(Int32 x, Int32 y, Int32 width, Int32 height, int format, int type, uint[,] pixels)
        {
            var handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                readPixels(x, y, width, height, format, type, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }

        /// <summary>[requires: v1.1]
        /// Copy pixels into a 2D texture image
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture. Must be Texture2D, TextureCubeMapPositiveX, TextureCubeMapNegativeX, TextureCubeMapPositiveY, TextureCubeMapNegativeY, TextureCubeMapPositiveZ, or TextureCubeMapNegativeZ.
        /// </param>
        /// <param name="level">
        /// Specifies the level-of-detail number. Level 0 is the base image level. Level n is the nth mipmap reduction image.
        /// </param>
        /// <param name="internalformat">
        /// Specifies the internal format of the texture. Must be one of the following symbolic constants: CompressedRed, CompressedRg, CompressedRgb, CompressedRgba. CompressedSrgb, CompressedSrgbAlpha. DepthComponent, DepthComponent16, DepthComponent24, DepthComponent32, StencilIndex8, Red, Rg, Rgb, R3G3B2, Rgb4, Rgb5, Rgb8, Rgb10, Rgb12, Rgb16, Rgba, Rgba2, Rgba4, Rgb5A1, Rgba8, Rgb10A2, Rgba12, Rgba16, Srgb, Srgb8, SrgbAlpha, or Srgb8Alpha8.
        /// </param>
        /// <param name="x">
        /// Specify the window coordinates of the lower left corner of the rectangular region of pixels to be copied.
        /// </param>
        /// <param name="y">
        /// Specify the window coordinates of the lower left corner of the rectangular region of pixels to be copied.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the texture image.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the texture image.
        /// </param>
        /// <param name="border">
        /// Must be 0.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glCopyTexImage2D")]
        private delegate void delegate_CopyTexImage2D(int target, Int32 level, int internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border);
        private static delegate_CopyTexImage2D func_CopyTexImage2D;
        public static void copyTexImage2D(int target, Int32 level, int internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border)
        {
            if (func_CopyTexImage2D == null)
            {
                func_CopyTexImage2D = (delegate_CopyTexImage2D)GetFunc("glCopyTexImage2D", typeof(delegate_CopyTexImage2D));
            }
            func_CopyTexImage2D(target, level, internalformat, x, y, width, height, border);

        }
        public static void copyTexImage2D(TextureTarget target, Int32 level, InternalFormat internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border)
        {
            copyTexImage2D((int)target, level, (int)internalformat, x, y, width, height, border);
        }

        // [AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glCopyTexSubImage2D")]
        private delegate void delegate_CopyTexSubImage2D(Int32 target, Int32 level, int internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border);
        private static delegate_CopyTexSubImage2D func_CopyTexSubImage2D;
        public static void CopyTexSubImage2D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height)
        {
            if (func_CopyTexSubImage2D == null)
            {
                func_CopyTexSubImage2D = (delegate_CopyTexSubImage2D)GetFunc("glCopyTexSubImage2D", typeof(delegate_CopyTexSubImage2D));
            }
            func_CopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);

        }
        public static void copyTexSubImage2D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height)
        {
            CopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
            if (DebugEnabled) DebugMethod(null, target, level, xoffset, yoffset, x, y, width, height).Constants(0);
        }


        /// <summary>[requires: v1.1]
        /// Specify a two-dimensional texture subimage
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture. Must be Texture2D, TextureCubeMapPositiveX, TextureCubeMapNegativeX, TextureCubeMapPositiveY, TextureCubeMapNegativeY, TextureCubeMapPositiveZ, TextureCubeMapNegativeZ, or Texture1DArray.
        /// </param>
        /// <param name="level">
        /// Specifies the level-of-detail number. Level 0 is the base image level. Level n is the nth mipmap reduction image.
        /// </param>
        /// <param name="xoffset">
        /// Specifies a texel offset in the x direction within the texture array.
        /// </param>
        /// <param name="yoffset">
        /// Specifies a texel offset in the y direction within the texture array.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the texture subimage.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the texture subimage.
        /// </param>
        /// <param name="format">
        /// Specifies the format of the pixel data. The following symbolic values are accepted: Red, Rg, Rgb, Bgr, Rgba, Bgra, DepthComponent, and StencilIndex.
        /// </param>
        /// <param name="type">
        /// Specifies the data type of the pixel data. The following symbolic values are accepted: UnsignedByte, Byte, UnsignedShort, Short, UnsignedInt, Int, Float, UnsignedByte332, UnsignedByte233Rev, UnsignedShort565, UnsignedShort565Rev, UnsignedShort4444, UnsignedShort4444Rev, UnsignedShort5551, UnsignedShort1555Rev, UnsignedInt8888, UnsignedInt8888Rev, UnsignedInt1010102, and UnsignedInt2101010Rev.
        /// </param>
        /// <param name="pixels">[length: COMPSIZE(format,type,width,height)]
        /// Specifies a pointer to the image data in memory.
        /// </param>
            //[AutoGenerated(Category = "VERSION_1_1", Version = "1.1", EntryPoint = "glTexSubImage2D")]
        private delegate void delegate_TexSubImage2D(int target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, int format, int type, IntPtr pixels);
        private static delegate_TexSubImage2D func_TexSubImage2D;
        public static void texSubImage2D(int target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, int format, int type, IntPtr pixels)
        {
            if (func_TexSubImage2D == null)
            {
                func_TexSubImage2D = (delegate_TexSubImage2D)GetFunc("glTexSubImage2D", typeof(delegate_TexSubImage2D));
            }
            func_TexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
            if (DebugEnabled) DebugMethod(null, target, level, xoffset, yoffset, width, height, format, type, pixels).Constants(0);

        }
        public static void texSubImage2D(int target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, int format, int type, byte[] pixels)
        {
            var handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                texSubImage2D(target, level, xoffset, yoffset, width, height, format, type, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }

        public static void texSubImage2D(TextureTarget target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, PixelFormat format, PixelType type, byte[] pixels)
        {
            var handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                texSubImage2D((int)target, level, xoffset, yoffset, width, height, (int)format, (int)type, handle.AddrOfPinnedObject());
                handle.Free();
            }
        }



        /// <summary>[requires: v1.2]
        /// Specify a three-dimensional texture subimage
        /// </summary>
        /// <param name="target">
        /// Specifies the target texture. Must be Texture3D or Texture2DArray.
        /// </param>
        /// <param name="level">
        /// Specifies the level-of-detail number. Level 0 is the base image level. Level n is the nth mipmap reduction image.
        /// </param>
        /// <param name="xoffset">
        /// Specifies a texel offset in the x direction within the texture array.
        /// </param>
        /// <param name="yoffset">
        /// Specifies a texel offset in the y direction within the texture array.
        /// </param>
        /// <param name="zoffset">
        /// Specifies a texel offset in the z direction within the texture array.
        /// </param>
        /// <param name="width">
        /// Specifies the width of the texture subimage.
        /// </param>
        /// <param name="height">
        /// Specifies the height of the texture subimage.
        /// </param>
        /// <param name="depth">
        /// Specifies the depth of the texture subimage.
        /// </param>
        /// <param name="format">
        /// Specifies the format of the pixel data. The following symbolic values are accepted: Red, Rg, Rgb, Bgr, Rgba, DepthComponent, and StencilIndex.
        /// </param>
        /// <param name="type">
        /// Specifies the data type of the pixel data. The following symbolic values are accepted: UnsignedByte, Byte, UnsignedShort, Short, UnsignedInt, Int, Float, UnsignedByte332, UnsignedByte233Rev, UnsignedShort565, UnsignedShort565Rev, UnsignedShort4444, UnsignedShort4444Rev, UnsignedShort5551, UnsignedShort1555Rev, UnsignedInt8888, UnsignedInt8888Rev, UnsignedInt1010102, and UnsignedInt2101010Rev.
        /// </param>
        /// <param name="pixels">[length: COMPSIZE(format,type,width,height,depth)]
        /// Specifies a pointer to the image data in memory.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_2", Version = "1.2", EntryPoint = "glTexSubImage3D")]
        private delegate void delegate_TexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, IntPtr pixels);
        private static delegate_TexSubImage3D func_TexSubImage3D;
        public static void texSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, IntPtr pixels)
        {
            if (func_TexSubImage3D == null)
            {
                func_TexSubImage3D = (delegate_TexSubImage3D)GetFunc("glTexSubImage3D", typeof(delegate_TexSubImage3D));
            }
            func_TexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
            if (DebugEnabled) DebugMethod(null, target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels).Constants(0);
        }
        public static void texSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, byte[] pixels)
        {
            var handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                texSubImage3D((int)target, level, xoffset, yoffset, zoffset, width, height, depth, (int)format, (int)type, handle.AddrOfPinnedObject());
                handle.Free();
            }

        }



        /// <summary>[requires: v3.1]
        /// Draw multiple instances of a range of elements
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, TriangleStrip, TriangleFan, TrianglesLinesAdjacency, LineStripAdjacency, TrianglesAdjacency, TriangleStripAdjacency and Patches are accepted.
        /// </param>
        /// <param name="first">
        /// Specifies the starting index in the enabled arrays.
        /// </param>
        /// <param name="count">
        /// Specifies the number of indices to be rendered.
        /// </param>
        /// <param name="instancecount">
        /// Specifies the number of instances of the specified range of indices to be rendered.
        /// </param>
        //[AutoGenerated(Category = "VERSION_3_1", Version = "3.1", EntryPoint = "glDrawArraysInstanced")]
        private unsafe delegate void delegate_DrawArraysInstanced(int mode, Int32 first, Int32 count, Int32 instancecount);
        private static delegate_DrawArraysInstanced func_DrawArraysInstanced;
        public static void drawArraysInstanced(int mode, Int32 first, Int32 count, Int32 instancecount)
        {
            if (func_DrawArraysInstanced == null)
            {
                func_DrawArraysInstanced = (delegate_DrawArraysInstanced)GetFunc("glDrawArraysInstanced", typeof(delegate_DrawArraysInstanced));
            }
            func_DrawArraysInstanced(mode, first, count, instancecount);
            if (DebugEnabled) DebugMethod(null, mode, first, count, instancecount).Constants(0);
        }

        public static void drawArraysInstanced(PrimitiveType mode, Int32 first, Int32 count, Int32 instancecount)
        {
            drawArraysInstanced((int)mode, first, count, instancecount);
        }


        /// <summary>[requires: ARB_draw_instanced]
        /// Draw multiple instances of a range of elements
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, TriangleStrip, TriangleFan, TrianglesLinesAdjacency, LineStripAdjacency, TrianglesAdjacency, TriangleStripAdjacency and Patches are accepted.
        /// </param>
        /// <param name="first">
        /// Specifies the starting index in the enabled arrays.
        /// </param>
        /// <param name="count">
        /// Specifies the number of indices to be rendered.
        /// </param>
        /// <param name="primcount">
        /// Specifies the number of instances of the specified range of indices to be rendered.
        /// </param>
       // [AutoGenerated(Category = "ARB_draw_instanced", Version = "", EntryPoint = "glDrawArraysInstancedARB")]
        private unsafe delegate void delegate_DrawArraysInstancedARB(int mode, Int32 first, Int32 count, Int32 primcount);
        private static delegate_DrawArraysInstancedARB func_DrawArraysInstancedARB;
        public static void drawArraysInstancedARB(int mode, Int32 first, Int32 count, Int32 primcount)
        {
            if (func_DrawArraysInstancedARB == null)
            {
                func_DrawArraysInstancedARB = (delegate_DrawArraysInstancedARB)GetFunc("glDrawArraysInstancedARB", typeof(delegate_DrawArraysInstancedARB));
            }
            func_DrawArraysInstancedARB(mode, first, count, primcount);
            if (DebugEnabled) DebugMethod(null, mode, first, count, primcount).Constants(0);
        }
        public static void drawArraysInstancedARB(PrimitiveType mode, Int32 first, Int32 count, Int32 primcount)
        {
            drawArraysInstancedARB((int)mode, first, count, primcount);
        }




        /// <summary>[requires: v3.1]
        /// Draw multiple instances of a set of elements
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency and Patches are accepted.
        /// </param>
        /// <param name="count">
        /// Specifies the number of elements to be rendered.
        /// </param>
        /// <param name="type">
        /// Specifies the type of the values in indices. Must be one of UnsignedByte, UnsignedShort, or UnsignedInt.
        /// </param>
        /// <param name="indices">[length: COMPSIZE(count,type)]
        /// Specifies a pointer to the location where the indices are stored.
        /// </param>
        /// <param name="instancecount">
        /// Specifies the number of instances of the specified range of indices to be rendered.
        /// </param>
        //[AutoGenerated(Category = "VERSION_3_1", Version = "3.1", EntryPoint = "glDrawElementsInstanced")]
        private unsafe delegate void delegate_DrawElementsInstanced(int mode, Int32 count, int type, IntPtr indices, Int32 instancecount);
        private static delegate_DrawElementsInstanced func_DrawElementsInstanced;
        public static void drawElementsInstanced(int mode, Int32 count, int type, IntPtr indices, Int32 instancecount)
        {
            if (func_DrawElementsInstanced == null)
            {
                func_DrawElementsInstanced = (delegate_DrawElementsInstanced)GetFunc("glDrawElementsInstanced", typeof(delegate_DrawElementsInstanced));
            }
            func_DrawElementsInstanced(mode, count, type, indices, instancecount);

        }
        public static void drawElementsInstanced(PrimitiveType mode, Int32 count, DrawElementsType type, IntPtr indices, Int32 instancecount)
        {
            drawElementsInstanced((int)mode, count, (int)type, indices, instancecount);
        }
        public static void drawElementsInstanced(int mode, Int32 count, int type, int offset, int instanceCount)
        {
            drawElementsInstanced(mode, count, type, new IntPtr(offset), instanceCount);
            if (DebugEnabled) DebugMethod(null, mode, count, type, offset, instanceCount).Constants(0,2);
        }



        /// <summary>[requires: ARB_draw_instanced]
        /// Draw multiple instances of a set of elements
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency and Patches are accepted.
        /// </param>
        /// <param name="count">
        /// Specifies the number of elements to be rendered.
        /// </param>
        /// <param name="type">
        /// Specifies the type of the values in indices. Must be one of UnsignedByte, UnsignedShort, or UnsignedInt.
        /// </param>
        /// <param name="indices">[length: COMPSIZE(count,type)]
        /// Specifies a pointer to the location where the indices are stored.
        /// </param>
        /// <param name="primcount">
        /// Specifies the number of instances of the specified range of indices to be rendered.
        /// </param>
        //[AutoGenerated(Category = "ARB_draw_instanced", Version = "", EntryPoint = "glDrawElementsInstancedARB")]
        private unsafe delegate void delegate_DrawElementsInstancedARB(int mode, Int32 count, int type, IntPtr indices, Int32 primcount);
        private static delegate_DrawElementsInstancedARB func_DrawElementsInstancedARB;
        public static void drawElementsInstancedARB(int mode, Int32 count, int type, IntPtr indices, Int32 primcount)
        {
            if (func_DrawElementsInstancedARB == null)
            {
                func_DrawElementsInstancedARB = (delegate_DrawElementsInstancedARB)GetFunc("glDrawElementsInstancedARB", typeof(delegate_DrawElementsInstancedARB));
            }
            func_DrawElementsInstancedARB(mode, count, type, indices, primcount);
        }
        public static void drawElementsInstancedARB(PrimitiveType mode, Int32 count, DrawElementsType type, IntPtr indices, Int32 primcount)
        {
            drawElementsInstancedARB((int)mode, count, (int)type, indices, primcount);
        }
        public static void drawElementsInstancedARB(int mode, Int32 count, int type, int indices, int instancecount)
        {
            var idxarr = new int[] { indices };
            var handle = GCHandle.Alloc(idxarr, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                drawElementsInstanced(mode, count, type, handle.AddrOfPinnedObject(), instancecount);
                handle.Free();
            }
        }


        /// <summary>[requires: v3.3]
        /// Modify the rate at which generic vertex attributes advance during instanced rendering
        /// </summary>
        /// <param name="index">
        /// Specify the index of the generic vertex attribute.
        /// </param>
        /// <param name="divisor">
        /// Specify the number of instances that will pass between updates of the generic attribute at slot index.
        /// </param>
        //[AutoGenerated(Category = "VERSION_3_3", Version = "3.3", EntryPoint = "glVertexAttribDivisor")]
        // [CLSCompliant(false)]
        private unsafe delegate void delegate_VertexAttribDivisor(Int32 index, Int32 divisor);
        private static delegate_VertexAttribDivisor func_VertexAttribDivisor;
        public static void vertexAttribDivisor(Int32 index, Int32 divisor)
        {
            if (func_VertexAttribDivisor == null)
            {
                func_VertexAttribDivisor = (delegate_VertexAttribDivisor)GetFunc("glVertexAttribDivisor", typeof(delegate_VertexAttribDivisor));
            }
            func_VertexAttribDivisor(index, divisor);
            if (DebugEnabled) DebugMethod(null, index, divisor);
        }


        /// <summary>[requires: v1.0]
        /// Specify implementation-specific hints
        /// </summary>
        /// <param name="target">
        /// Specifies a symbolic constant indicating the behavior to be controlled. LineSmoothHint, PolygonSmoothHint, TextureCompressionHint, and FragmentShaderDerivativeHint are accepted.
        /// </param>
        /// <param name="mode">
        /// Specifies a symbolic constant indicating the desired behavior. Fastest, Nicest, and DontCare are accepted.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_0", Version = "1.0", EntryPoint = "glHint")]
        private delegate void delegate_Hint(Int32 index, Int32 divisor);
        private static delegate_Hint func_Hint;
        public static void hint(int target, int mode)
        {
            if (func_Hint == null)
            {
                func_Hint = (delegate_Hint)GetFunc("glHint", typeof(delegate_Hint));
            }
            func_Hint(target, mode);
        }
        public static void hint(HintTarget target, HintMode mode)
        {
            hint((int)target, (int)mode);
        }

        /// <summary>[requires: EXT_direct_state_access]</summary>
        /// <param name="texunit"></param>
        /// <param name="target"></param>
        /// <param name="pname"></param>
        /// <param name="param"></param>
        //[AutoGenerated(Category = "EXT_direct_state_access", Version = "", EntryPoint = "glMultiTexEnviEXT")]
        private delegate void delegate_MultiTexEnv(int texunit, int target, int pname, int param);
        private static delegate_MultiTexEnv func_MultiTexEnv;
        public static void multiTexEnv(int texunit, int target, int pname, int param)
        {
            if (func_MultiTexEnv == null)
            {
                func_MultiTexEnv = (delegate_MultiTexEnv)GetFunc("glMultiTexEnviEXT", typeof(delegate_MultiTexEnv));
            }
            func_MultiTexEnv(texunit, target, pname, param);
        }
        public static void multiTexEnv(TextureUnit texunit, TextureEnvTarget target, TextureEnvParameter pname, Int32 param)
        {
            multiTexEnv((int)texunit, (int)target, (int)pname, (int)param);
        }



        /// <summary>[requires: v1.4]
        /// Render multiple sets of primitives from array data
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency and Patches are accepted.
        /// </param>
        /// <param name="first">[length: COMPSIZE(count)]
        /// Points to an array of starting indices in the enabled arrays.
        /// </param>
        /// <param name="count">[length: COMPSIZE(drawcount)]
        /// Points to an array of the number of indices to be rendered.
        /// </param>
        /// <param name="drawcount">
        /// Specifies the size of the first and count
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_4", Version = "1.4", EntryPoint = "glMultiDrawArrays")]
        //[CLSCompliant(false)]
        private delegate void delegate_MultiDrawArrays(int mode, IntPtr first, IntPtr count, int drawcount);
        private static delegate_MultiDrawArrays func_MultiDrawArrays;
        public static void MultiDrawArrays(PrimitiveType mode, IntPtr first, IntPtr count, int drawcount)
        {
            if (func_MultiDrawArrays == null)
            {
                func_MultiDrawArrays = (delegate_MultiDrawArrays)GetFunc("glMultiDrawArrays", typeof(delegate_MultiDrawArrays));
            }
            func_MultiDrawArrays((int)mode, first, count, drawcount);
        }
        public static void MultiDrawArrays(PrimitiveType mode, int[] firsts, int[] counts, int drawcount)
        {
            var handleFirst = GCHandle.Alloc(firsts, GCHandleType.Pinned);
            var handleCount = GCHandle.Alloc(counts, GCHandleType.Pinned);
            if (handleFirst.IsAllocated && handleCount.IsAllocated)
            {
                MultiDrawArrays(mode, handleFirst.AddrOfPinnedObject(), handleCount.AddrOfPinnedObject(), drawcount);
                handleFirst.Free();
                handleCount.Free();
            }
            else
            {
                if (handleFirst.IsAllocated) handleFirst.Free();
                if (handleCount.IsAllocated) handleCount.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, firsts,counts, drawcount).Constants(0);

        }

        /// <summary>[requires: v1.4]
        /// Render multiple sets of primitives by specifying indices of array data elements
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency and Patches are accepted.
        /// </param>
        /// <param name="count">[length: COMPSIZE(drawcount)]
        /// Points to an array of the elements counts.
        /// </param>
        /// <param name="type">
        /// Specifies the type of the values in indices. Must be one of UnsignedByte, UnsignedShort, or UnsignedInt.
        /// </param>
        /// <param name="indices">[length: COMPSIZE(drawcount)]
        /// Specifies a pointer to the location where the indices are stored.
        /// </param>
        /// <param name="drawcount">
        /// Specifies the size of the count and indices arrays.
        /// </param>
        //[AutoGenerated(Category = "VERSION_1_4", Version = "1.4", EntryPoint = "glMultiDrawElements")]
        //[CLSCompliant(false)]
        private delegate void delegate_MultiDrawElements(int mode, IntPtr count, int type, IntPtr indices, int drawcount);
        private static delegate_MultiDrawElements func_MultiDrawElements;
        public static void MultiDrawElements(PrimitiveType mode, IntPtr count, DrawElementsType type, IntPtr indices, Int32 drawcount)
        {
            if (func_MultiDrawElements == null)
            {
                func_MultiDrawElements = (delegate_MultiDrawElements)GetFunc("glMultiDrawElements", typeof(delegate_MultiDrawElements));
            }
            func_MultiDrawElements((int)mode, count, (int)type, indices, drawcount);
        }

        public static void MultiDrawElements(PrimitiveType mode, int[] counts, int[][] indices, int drawcount)
        {

            var handleCount = GCHandle.Alloc(counts, GCHandleType.Pinned);
            var uintPtr = new IntPtr[indices.Length];
            var uintHandle = new GCHandle[indices.Length];
            for (var i = 0; i < indices.Length; i++)
            {
                uintHandle[i] = GCHandle.Alloc(indices[i], GCHandleType.Pinned);
                uintPtr[i] = uintHandle[i].AddrOfPinnedObject();
            }
            var handleIndices = GCHandle.Alloc(uintPtr, GCHandleType.Pinned);
            if (handleCount.IsAllocated && handleIndices.IsAllocated)
            {
                MultiDrawElements(mode, handleCount.AddrOfPinnedObject(), DrawElementsType.UnsignedInt, handleIndices.AddrOfPinnedObject(), drawcount);
                for (var i = 0; i < uintHandle.Length; i++)
                    uintHandle[i].Free();
                handleCount.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, counts, indices, drawcount).Constants(0);
        }
        public static void MultiDrawElements(PrimitiveType mode, int[] counts, ushort[][] indices, int drawcount)
        {

            var handleCount = GCHandle.Alloc(counts, GCHandleType.Pinned);
            var uintPtr = new IntPtr[indices.Length];
            var uintHandle = new GCHandle[indices.Length];
            for (var i = 0; i < indices.Length; i++)
            {
                uintHandle[i] = GCHandle.Alloc(indices[i], GCHandleType.Pinned);
                uintPtr[i] = uintHandle[i].AddrOfPinnedObject();
            }
            var handleIndices = GCHandle.Alloc(uintPtr, GCHandleType.Pinned);
            if (handleCount.IsAllocated && handleIndices.IsAllocated)
            {
                MultiDrawElements(mode, handleCount.AddrOfPinnedObject(), DrawElementsType.UnsignedShort, handleIndices.AddrOfPinnedObject(), drawcount);
                for (var i = 0; i < uintHandle.Length; i++)
                    uintHandle[i].Free();
                handleCount.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, counts, indices, drawcount).Constants(0);
        }



        [StructLayout(LayoutKind.Sequential)]
        public struct DrawArraysIndirectCommand
        {
            public uint count;
            public uint instanceCount;
            public uint first;
            public uint baseInstance;
        }


        /// <summary>[requires: v4.0 or ARB_draw_indirect|VERSION_4_0]
        /// Render primitives from array data, taking parameters from memory
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency, and Patches are accepted.
        /// </param>
        /// <param name="indirect">
        /// Specifies the address of a structure containing the draw parameters.
        /// </param>
        //[AutoGenerated(Category = "ARB_draw_indirect|VERSION_4_0", Version = "4.0", EntryPoint = "glDrawArraysIndirect")]
        private delegate void delegate_DrawArraysIndirect(int mode, IntPtr indirect);
        private static delegate_DrawArraysIndirect func_DrawArraysIndirect;
        public static void DrawArraysIndirect(PrimitiveType mode, IntPtr indirect)
        {
            if (func_DrawArraysIndirect == null)
            {
                func_DrawArraysIndirect = (delegate_DrawArraysIndirect)GetFunc("glDrawArraysIndirect", typeof(delegate_DrawArraysIndirect));
            }
            func_DrawArraysIndirect((int)mode, indirect);

        }
        public static void DrawArraysIndirect(PrimitiveType mode, DrawArraysIndirectCommand command)
        {
            var handle = GCHandle.Alloc(command, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                DrawArraysIndirect(mode, handle.AddrOfPinnedObject());
                handle.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, command).Constants(0);
        }



        [StructLayout(LayoutKind.Sequential)]
        public struct DrawElementsIndirectCommand
        {
            public uint count;
            public uint instanceCount;
            public uint firstIndex;
            public uint baseVertex;
            public uint baseInstance;
        }

        /// <summary>[requires: v4.0 or ARB_draw_indirect|VERSION_4_0]
        /// Render indexed primitives from array data, taking parameters from memory
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency, and Patches are accepted.
        /// </param>
        /// <param name="type">
        /// Specifies the type of data in the buffer bound to the ElementArrayBuffer binding.
        /// </param>
        /// <param name="indirect">
        /// Specifies the address of a structure containing the draw parameters.
        /// </param>
        //[AutoGenerated(Category = "ARB_draw_indirect|VERSION_4_0", Version = "4.0", EntryPoint = "glDrawElementsIndirect")]
        private delegate void delegate_DrawElementsIndirect(int mode, int type, IntPtr indirect);
        private static delegate_DrawElementsIndirect func_DrawElementsIndirect;
        public static void DrawElementsIndirect(PrimitiveType mode, DrawElementsType type, IntPtr indirect)
        {
            if (func_DrawElementsIndirect == null)
            {
                func_DrawElementsIndirect = (delegate_DrawElementsIndirect)GetFunc("glDrawElementsIndirect", typeof(delegate_DrawElementsIndirect));
            }
            func_DrawElementsIndirect((int)mode, (int)type, indirect);
        }


        public static void DrawElementsIndirect(PrimitiveType mode, DrawElementsType type, DrawElementsIndirectCommand command)
        {

            var handle = GCHandle.Alloc(command, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                DrawElementsIndirect(mode, type, handle.AddrOfPinnedObject());
                handle.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, type, command).Constants(0);
        }



        /// <summary>[requires: v4.3 or ARB_multi_draw_indirect|VERSION_4_3]
        /// Render multiple sets of primitives from array data, taking parameters from memory
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency, and Patches are accepted.
        /// </param>
        /// <param name="indirect">[length: COMPSIZE(drawcount,stride)]
        /// Specifies the address of an array of structures containing the draw parameters.
        /// </param>
        /// <param name="drawcount">
        /// Specifies the the number of elements in the array of draw parameter structures.
        /// </param>
        /// <param name="stride">
        /// Specifies the distance in basic machine units between elements of the draw parameter array.
        /// </param>
        //[AutoGenerated(Category = "ARB_multi_draw_indirect|VERSION_4_3", Version = "4.3", EntryPoint = "glMultiDrawArraysIndirect")]
        private delegate void delegate_MultiDrawArraysIndirect(int mode, IntPtr indirect, Int32 drawcount, Int32 stride);
        private static delegate_MultiDrawArraysIndirect func_MultiDrawArraysIndirect;
        public static void MultiDrawArraysIndirect(int mode, IntPtr indirect, int drawcount, int stride)
        {
            if (func_MultiDrawArraysIndirect == null)
            {
                func_MultiDrawArraysIndirect = (delegate_MultiDrawArraysIndirect)GetFunc("glMultiDrawArraysIndirect", typeof(delegate_MultiDrawArraysIndirect));
            }
            func_MultiDrawArraysIndirect((int)mode, indirect, drawcount, stride);
        }
        public static void MultiDrawArraysIndirect(int mode, [In, Out] DrawArraysIndirectCommand[] commands, Int32 stride)
        {
            var handle = GCHandle.Alloc(commands, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                MultiDrawArraysIndirect(mode, handle.AddrOfPinnedObject(), commands.Length, stride);
                handle.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, commands, stride).Constants(0);
        }
        public static void MultiDrawArraysIndirect(PrimitiveType mode, [In, Out] DrawArraysIndirectCommand[] commands, Int32 stride)
        {
            var handle = GCHandle.Alloc(commands, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                MultiDrawArraysIndirect((int)mode, handle.AddrOfPinnedObject(), commands.Length, stride);
                handle.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, commands, stride).Constants(0);
        }

        /// <summary>[requires: v4.3 or ARB_multi_draw_indirect|VERSION_4_3]
        /// Render indexed primitives from array data, taking parameters from memory
        /// </summary>
        /// <param name="mode">
        /// Specifies what kind of primitives to render. Symbolic constants Points, LineStrip, LineLoop, Lines, LineStripAdjacency, LinesAdjacency, TriangleStrip, TriangleFan, Triangles, TriangleStripAdjacency, TrianglesAdjacency, and Patches are accepted.
        /// </param>
        /// <param name="type">
        /// Specifies the type of data in the buffer bound to the ElementArrayBuffer binding.
        /// </param>
        /// <param name="indirect">[length: COMPSIZE(drawcount,stride)]
        /// Specifies the address of a structure containing an array of draw parameters.
        /// </param>
        /// <param name="drawcount">
        /// Specifies the number of elements in the array addressed by indirect.
        /// </param>
        /// <param name="stride">
        /// Specifies the distance in basic machine units between elements of the draw parameter array.
        /// </param>
        //[AutoGenerated(Category = "ARB_multi_draw_indirect|VERSION_4_3", Version = "4.3", EntryPoint = "glMultiDrawElementsIndirect")]
        private delegate void delegate_MultiDrawElementsIndirect(int mode, int type, IntPtr indirect, Int32 drawcount, Int32 stride);
        private static delegate_MultiDrawElementsIndirect func_MultiDrawElementsIndirect;
        public static void MultiDrawElementsIndirect(int mode, int type, IntPtr indirect, Int32 drawcount, Int32 stride)
        {
            if (func_MultiDrawElementsIndirect == null)
            {
                func_MultiDrawElementsIndirect = (delegate_MultiDrawElementsIndirect)GetFunc("glMultiDrawElementsIndirect", typeof(delegate_MultiDrawElementsIndirect));
            }
            func_MultiDrawElementsIndirect(mode, type, indirect, drawcount, stride);
        }
        public static void MultiDrawElementsIndirect(int mode, int type, [In, Out] DrawElementsIndirectCommand[] commands, Int32 stride)
        {
            var handle = GCHandle.Alloc(commands, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                MultiDrawElementsIndirect(mode, type, handle.AddrOfPinnedObject(), commands.Length, stride);
                handle.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, type, commands, stride).Constants(0);
        }
        public static void MultiDrawElementsIndirect(PrimitiveType mode, DrawElementsType type, [In, Out] DrawElementsIndirectCommand[] commands, Int32 stride)
        {
            var handle = GCHandle.Alloc(commands, GCHandleType.Pinned);
            if (handle.IsAllocated)
            {
                MultiDrawElementsIndirect((int)mode, (int)type, handle.AddrOfPinnedObject(), commands.Length, stride);
                handle.Free();
            }
            if (DebugEnabled) DebugMethod(null, mode, type, commands, stride).Constants(0);
        }


        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGenQueries(Int32 n, [OutAttribute, CountAttribute(Parameter = "n")] UInt32* ids);

        /// <summary>[requires: v1.5]
        /// Generate query object names
        /// </summary>
        /// <param name="n">
        /// Specifies the number of query object names to be generated.
        /// </param>
        /// <param name="ids">[length: n]
        /// Specifies an array in which the generated query object names are stored.
        /// </param>

        private unsafe delegate void delegate_GenQueries(int n, uint* ids);
        private static delegate_GenQueries func_GenQueries;
        public unsafe static void GenQueries(int n, uint[] ids)
        {
            if (func_GenQueries == null)
            {
                func_GenQueries = (delegate_GenQueries)GetFunc("glGenQueries", typeof(delegate_GenQueries));
            }
            fixed (uint* ptr = ids)
            {
                func_GenQueries(n, ptr);
            }
            if (DebugEnabled) DebugMethod(null, n, ids);
        }

        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glDeleteQueries(Int32 n, [CountAttribute(Parameter = "n")] UInt32* ids);
        /// <summary>[requires: v1.5]
        /// Delete named query objects
        /// </summary>
        /// <param name="n">
        /// Specifies the number of query objects to be deleted.
        /// </param>
        /// <param name="ids">[length: n]
        /// Specifies an array of query objects to be deleted.
        /// </param>
        private unsafe delegate void delegate_DeleteQueries(int n, uint* ids);
        private static delegate_DeleteQueries func_DeleteQueries;
        public unsafe static void DeleteQueries(int n, uint[] ids)
        {
            if (func_DeleteQueries == null)
            {
                func_DeleteQueries = (delegate_DeleteQueries)GetFunc("glDeleteQueries", typeof(delegate_DeleteQueries));
            }
            fixed (uint* ptr = ids)
            {
                func_DeleteQueries(n, ptr);
            }
            if (DebugEnabled) DebugMethod(null, n, ids);
        }




        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glBeginQuery(System.Int32 target, UInt32 id);
        /// <summary>[requires: v1.5]
        /// Delimit the boundaries of a query object
        /// </summary>
        /// <param name="target">
        /// Specifies the target type of query object established between glBeginQuery and the subsequent glEndQuery. The symbolic constant must be one of SamplesPassed, AnySamplesPassed, AnySamplesPassedConservative, PrimitivesGenerated, TransformFeedbackPrimitivesWritten, or TimeElapsed.
        /// </param>
        /// <param name="id">
        /// Specifies the name of a query object.
        /// </param>
        private delegate void delegate_BeginQuery(int target, uint id);
        private static delegate_BeginQuery func_BeginQuery;
        public static void BeginQuery(int target, uint id)
        {
            if (func_BeginQuery == null)
            {
                func_BeginQuery = (delegate_BeginQuery)GetFunc("glBeginQuery", typeof(delegate_BeginQuery));
            }
            func_BeginQuery(target, id);
            if (DebugEnabled) DebugMethod(null, target, id).Constants(0);

        }

        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern void glEndQuery(System.Int32 target);
        // /// <summary>[requires: v1.5]</summary>
        // /// <param name="target"></param>
        // [AutoGenerated(Category = "VERSION_1_5", Version = "1.5", EntryPoint = "glEndQuery")]
        // public static void EndQuery(QueryTarget target) { throw new BindingsNotRewrittenException(); }
        private delegate void delegate_EndQuery(int target);
        private static delegate_EndQuery func_EndQuery;
        public static void EndQuery(int target)
        {
            if (func_EndQuery == null)
            {
                func_EndQuery = (delegate_EndQuery)GetFunc("glEndQuery", typeof(delegate_EndQuery));
            }
            func_EndQuery(target);
            if (DebugEnabled) DebugMethod(null, target).Constants(0);

        }


        /// <summary>[requires: v1.5]
        /// Return parameters of a query object
        /// </summary>
        /// <param name="id">
        /// Specifies the name of a query object.
        /// </param>
        /// <param name="pname">
        /// Specifies the symbolic name of a query object parameter. Accepted values are QueryResult or QueryResultAvailable.
        /// </param>
        /// <param name="@params">[length: COMPSIZE(pname)]
        /// If a buffer is bound to the QueryResultBuffer target, then params is treated as an offset to a location within that buffer's data store to receive the result of the query. If no buffer is bound to QueryResultBuffer, then params is treated as an address in client memory of a variable to receive the resulting data.
        /// </param>
        //[DllImport(Library, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        //private static extern unsafe void glGetQueryObjectuiv(UInt32 id, System.Int32 pname, [OutAttribute, CountAttribute(Computed = "pname")] UInt32* @params);

        private unsafe delegate uint delegate_GetQueryObjectuiv(uint id,int pname,uint* value);
        private static delegate_GetQueryObjectuiv func_GetQueryObjectuiv;
        public unsafe static uint GetQueryObject(uint id,int pname)
        {
            if (func_GetQueryObjectuiv == null)
            {
                func_GetQueryObjectuiv = (delegate_GetQueryObjectuiv)GetFunc("glGetQueryObjectuiv", typeof(delegate_GetQueryObjectuiv));
            }
            var value = new uint[1];
            fixed (uint* ptr = value)
            {
                func_GetQueryObjectuiv(id,pname, ptr);
            }

            if (DebugEnabled) DebugMethod(value[0], id,pname,value).Constants(1);
            return value[0];

        }
    }



}
