using Avalonia.Controls;
using Aura.UI.Controls;
using LightCAD;
using Avalonia.Interactivity;
using Avalonia.Input;
using Avalonia.Collections;
using LightCAD.Core;
using LightCAD.Runtime;
using Avalonia;
using Avalonia.Media;
using Avalonia.Styling;
using Avalonia.VisualTree;
using System.Collections.ObjectModel;
using System;
using System.Linq;
using System.Diagnostics;
using Avalonia.Threading;


namespace LightCAD
{

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //this.Renderer.DrawDirtyRects = true;
            //this.Renderer.DrawFps = true;
            
            this.Opened += MainWindow_Opened;
            this.Activated += MainWindow_Activated;
            this.Deactivated += MainWindow_Deactivated;
        }

    

        private void MainWindow_Opened(object? sender, EventArgs e)
        {
            MainView.MainTab.InternalGrid.PointerPressed += (s, e) =>
            {
                this.BeginMoveDrag(e);
            };
            MainView.MainTab.InternalGrid.DoubleTapped += (s, e) =>
            {
                if (this.WindowState == WindowState.Normal)
                    this.WindowState = WindowState.Maximized;
                else
                    this.WindowState = WindowState.Normal;
            };
        }
        private void MainWindow_Activated(object? sender, EventArgs e)
        {
            var timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 250);
            timer.Tick += (s,e)=> {
                timer.Stop();
                if(this.WindowState != WindowState.Minimized)
                {
                    //Debug.Print("MainWindowWrap_Activated" + this.WindowState.ToString());
                    //WindowManager.SetActivated(mainWindow);
                    this.Background = Brushes.White;
                }
            };
            timer.Start();

        }

        private void MainWindow_Deactivated(object? sender, EventArgs e)
        {
            //Debug.Print("MainWindowWrap_Deactivated");
            //WindowManager.SetDeactivated(mainWindow);
            this.Background = new Avalonia.Media.SolidColorBrush(new Avalonia.Media.Color(255,240,240,240));

        }

        internal void SetActivated()
        {
            throw new NotImplementedException();
        }

        internal void SetDeactivated()
        {
            throw new NotImplementedException();
        }
    }
       
}