using Avalonia.Controls;
using Aura.UI.Controls;
using LightCAD;
using Avalonia.Interactivity;
using Avalonia.Input;
using Avalonia.Collections;
using LightCAD.Core;
using LightCAD.Runtime;
using Avalonia;
using Avalonia.Media;
using Avalonia.Styling;
using Avalonia.VisualTree;
using System.Collections.ObjectModel;
using System;
using System.Linq;
using SkiaSharp;
using System.Threading.Tasks;
using System.IO;
using LightCAD.UI;
using System.Reflection.Metadata;
using System.Diagnostics;
using System.IO.Compression;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace LightCAD
{

    public partial class MainView : UserControl
    {
        public MainWindow Window { get; set; }

        public MainView()
        {
            InitializeComponent();
            this.Initialized += MainWindow_Initialized;
        }

        private void MainWindow_Initialized(object? sender, EventArgs e)
        {
            Window = this.Parent as MainWindow;

        }


        private void MainWindow_Opened(object? sender, EventArgs e)
        {

        }

        private void TabMain_SelectionChanged(object? sender, SelectionChangedEventArgs e)
        {

        }


        private void LeftTopButton_Click(object? sender, RoutedEventArgs e)
        {
            var newWin = new MainWindow();
            newWin.Show();
        }

        public void ChangeDocument(LcDocument document)
        {

        }

        public LcDocument NewDrawing()
        {
            var doc = DocumentManager.Create();
            var header = Path.GetFileName(doc.FilePath);
            var mainView = App.Instance.ActiveView;
            mainView.MainTab.AddTab(new AuraTabItem { Header = header });
            mainView.ChangeDocument(doc);
            DocumentManager.SetCurrent(doc);
            return doc;
        }

        public async Task<LcDocument> OpenDrawing()
        {
            var win = App.Instance.ActiveWindow;
            var dlg = new OpenFileDialog();
            dlg.AllowMultiple = false;
            var files = await dlg.ShowAsync(win);
            if (files == null) return null;

            //从ZIP文件中读取图形JSON文件
            var filePath = files[0];
            var start = DateTime.Now;
            var doc = DocumentManager.LoadDocument(filePath);
            var header = Path.GetFileName(doc.FilePath);
            win.MainView.MainTab.AddTab(new AuraTabItem { Header = header });
            win.MainView.ChangeDocument(doc);
            DocumentManager.SetCurrent(doc);
            Debug.Print("OpenFile:" + (DateTime.Now - start).TotalSeconds.ToString());
            return doc;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isSaveAs"></param>
        /// <returns>Error</returns>
        public async Task<string> SaveDrawing(bool isSaveAs = false)
        {
            try
            {
                var doc = DocumentManager.Current;
                //如果是新建文档没有目录
                var filePath = doc.FilePath;
                if (isSaveAs || filePath == null || !Directory.Exists(Path.GetDirectoryName(filePath)))
                {
                    var dlg = new SaveFileDialog();
                    dlg.InitialFileName = filePath ?? DocumentManager.GetAutoName();
                    filePath = await dlg.ShowAsync(this.Window);
                    if (filePath == null) return SR.NoFilePath;
                }

                var start = DateTime.Now;
                DocumentManager.SaveDocument(doc, filePath);
                Debug.Print("SaveFile:" + (DateTime.Now - start).TotalSeconds.ToString());
                return null;
            }catch (Exception ex)
            {
                return ex.Message;
            }
           

        }
    }

}