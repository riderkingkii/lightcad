using AuraUtilities.Configuration;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.Markup.Xaml.Styling;
using Avalonia.Styling;
using Avalonia.Threading;
using LightCAD.Hosting;
using LightCAD.Runtime;
using LightCAD.UI;
using System;
using Weikio.PluginFramework.Abstractions;
using Weikio.PluginFramework.Catalogs;

namespace LightCAD
{

    public class App : Application
    {
        public static App Instance { get; private set; }
        public App() { 
            Instance = this;
        }
        public bool IsDesktop { get; set; }
        public override void Initialize()
        {
            // Styles.Insert(0, App.FluentDark);
            Styles.Insert(0, App.FluentLight);

            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            // NavigationViewStatic();
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                this.IsDesktop = true;
                desktop.MainWindow = WindowManager.CreateWindow();

                desktop.Exit += (s, e) =>
                {
                    //new SettingsProvider().Save(Settings);
                };
            }
            else if (ApplicationLifetime is ISingleViewApplicationLifetime single)
            {
                single.MainView = new MainView()
                {
                    //DataContext = new MainWindowViewModel()
                };
            }

            base.OnFrameworkInitializationCompleted();
        }


        public readonly static Styles FluentDark = new Styles
        {
            new StyleInclude(new Uri("avares://Aura.UI.Gallery/Styles"))
            {
                Source = new Uri("avares://Avalonia.Themes.Fluent/FluentDark.xaml")
            },
            new StyleInclude(new Uri("avares://Aura.UI.Gallery/Styles"))
            {
                Source = new Uri("avares://Aura.UI.FluentTheme/AuraUI.xaml")
            }
        };

        public readonly static Styles FluentLight = new Styles
        {
            new StyleInclude(new Uri("avares://Aura.UI.Gallery/Styles"))
            {
                Source = new Uri("avares://Avalonia.Themes.Fluent/FluentLight.xaml")
            },
            new StyleInclude(new Uri("avares://Aura.UI.Gallery/Styles"))
            {
                Source = new Uri("avares://Aura.UI.FluentTheme/AuraUI.xaml")
            }
        };

        public  MainWindow ActiveWindow => WindowManager.Current;

        public MainView ActiveView => (this.IsDesktop ? WindowManager.Current.MainView : (MainView)(ApplicationLifetime as ISingleViewApplicationLifetime).MainView);

    }

}