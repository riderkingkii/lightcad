﻿namespace LightCAD.Core
{
    internal static partial class SR
    {
        /// <summary>An error occurred at {0}, ({1}, {2}).</summary>
        internal static string @NoFilePath => GetResourceString("NoFilePath", @"Not specified file path.");

    }
}
