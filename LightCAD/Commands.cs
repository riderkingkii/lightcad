﻿
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Input.Platform;
using Avalonia.VisualTree;
using LightCAD.Core;
using LightCAD.Runtime;
using LightCAD.Runtime.Creator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;
using System.IO.Compression;
using System.IO;
using System.Diagnostics;
using Avalonia.ReactiveUI;
using Aura.UI.Controls;

namespace LightCAD
{
    public static class Commands
    {
        static Commands()
        {
            CommandCenter.Instance.CommandExecute += CommandExecute;
        }
        private static async void CommandExecute(object sender, CommandExecuteEventArgs eventArgs)
        {
            try
            {
                var cmdName = eventArgs.CommandName.ToLower();
                if (cmdName == "new")
                {
                    var doc=New();
                    eventArgs.Success = true;
                    eventArgs.Result = doc;
                }
                else if (cmdName == "open")
                {
                    var doc = Open();
                    eventArgs.Success = true;
                    eventArgs.Result = doc;
                }
                else if (cmdName == "save")
                {
                    var error = await Save();
                    eventArgs.Success = (error==null);
                    eventArgs.Error = error;
                }
                else if (cmdName == "saveas")
                {
                    var error = await SaveAs();
                    eventArgs.Success = (error == null);
                    eventArgs.Error = error;
                }
            }
            catch(Exception ex)
            {
                eventArgs.Success = false;
                eventArgs.Execption = ex;
            }
            
        }
        
        public static LcDocument New()
        {
            return App.Instance.ActiveView.NewDrawing();
        }
        public static async Task<LcDocument> Open()
        {
            return await App.Instance.ActiveView.OpenDrawing();
        }
        public static async Task<string> Save()
        {
            return await App.Instance.ActiveView.SaveDrawing();
        }
        public static async Task<string> SaveAs()
        {
            return await App.Instance.ActiveView.SaveDrawing(true);
        }
    }
}
