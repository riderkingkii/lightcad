﻿using Avalonia.Controls;
using LightCAD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightCAD
{
    public class WindowManager
    {
        public static List<MainWindow> Windows { get; } = new List<MainWindow>();
        public static MainWindow Current { get; private set; }
        public static MainWindow CreateWindow()
        {
            var window = new MainWindow();
            Windows.Add(window);
            window.Closed += Window_Closed;
            return window;
        }

        private static void Window_Closed(object? sender, EventArgs e)
        {
            var window = sender as MainWindow;
            Windows.Remove(window);
        }

        public static void SetActivated(MainWindow window)
        {
            window.SetActivated();
            SetCurrent(window);
        }

        public static void SetCurrent(MainWindow window)
        {
           Current = window;
        }

        public static void SetDeactivated(MainWindow window)
        {
            window.SetDeactivated();
        }
    }
}
