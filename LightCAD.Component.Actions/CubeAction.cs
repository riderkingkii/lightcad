﻿using LightCAD.Core;
using LightCAD.Core.Element3d;
using LightCAD.Drawing;
using LightCAD.Drawing.Actions;
using LightCAD.MathLib;
using LightCAD.Model;
using LightCAD.Runtime;

namespace LightCAD.Component.Actions
{

    public class CubeAction : ComponentInstance2dAction
    {
        public CubeAction() :base() 
        {
            
        }
        public CubeAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Cube"); 
        }
        public async void ExecCreate(string[] args = null) 
        {
            var cube = new LcCubeInstance(200, ComponentManager.GetCptDef<LcCubeDef>("立方体", "立方体", CubeAttribute.BuiltinUuid));
            cube.Initilize(this.docRt.Document);
            cube.Position.Set(-100, -100, -100);
            this.docRt.Document.ModelSpace.InsertElement(cube);
        }
    }
    public class Cube3dAction : ComponentInstance3dAction 
    {
        public Cube3dAction():base()
        { }
        public Cube3dAction(IDocumentEditor docEditor) : base(docEditor)
        {
            this.commandCtrl.WriteInfo("命令：Cube");
        }
        public async void ExecCreate(string[] args = null) 
        {
            var mats = new LcMaterial[6]
            {
                new LcMaterial() { Color = new Color(0xff0000) },
                new LcMaterial() { Color = new Color(0x00ff00) },
                new LcMaterial() { Color = new Color(0x0000ff) },
                new LcMaterial() { Color = new Color(0xff0000) },
                new LcMaterial() { Color = new Color(0x00ff00) },
                new LcMaterial() { Color = new Color(0x0000ff) }
            };
            var cube = new LcCubeInstance(200,ComponentManager.GetCptDef<LcCubeDef>("立方体", "立方体", CubeAttribute.BuiltinUuid));
            cube.Initilize(this.docRt.Document);
            cube.Position.Set(-100, -100, -100);
            (cube.Rt3DAction as IComponentAction).SetDocEditor(this.docEditor);
            this.docRt.Document.ModelSpace.InsertElement(cube);
        }
    }

    
}
