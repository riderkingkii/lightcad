﻿using LightCAD.Runtime;

namespace LightCAD.Component.Actions
{
    [CommandClass]
    public  class ActionCmds 
    {
        #region CreateElement3d
        [CommandMethod(Name = "Cube", ShortCuts = "CU", Category = "Model")]
        public CommandResult CubeByModel(IDocumentEditor docEditor, string[] args)
        {
            var cubeAction = new Cube3dAction(docEditor);
            cubeAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Cube", ShortCuts = "CU", Category = "Drawing")]
        public CommandResult CubeByDrawing(IDocumentEditor docEditor, string[] args)
        {
            var cubeAction = new CubeAction(docEditor);
            cubeAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Cuboid", ShortCuts = "CB", Category = "Model")]
        public CommandResult CuboidByModel(IDocumentEditor docEditor, string[] args)
        {
            var cuboidAction = new Cuboid3dAction(docEditor);
            cuboidAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Cuboid", ShortCuts = "CB", Category = "Drawing")]
        public CommandResult CuboidByDrawing(IDocumentEditor docEditor, string[] args)
        {
            var cuboidAction = new CuboidAction(docEditor);
            cuboidAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Extrude", ShortCuts = "Ext", Category = "Model")]
        public CommandResult ExtrudeByModel(IDocumentEditor docEditor, string[] args)
        {
            var extrudeAction = new Extrude3dAction(docEditor);
            extrudeAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Extrude", ShortCuts = "Ext", Category = "Drawing")]
        public CommandResult ExtrudeByDrawing(IDocumentEditor docEditor, string[] args)
        {
            var extrudeAction = new ExtrudeAction(docEditor);
            extrudeAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        
        [CommandMethod(Name = "Loft", ShortCuts = "LT", Category = "Model")]
        public CommandResult LoftByModel(IDocumentEditor docEditor, string[] args)
        {
            var loftAction = new Loft3dAction(docEditor);
            loftAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        [CommandMethod(Name = "Loft", ShortCuts = "LT", Category = "Drawing")]
        public CommandResult LoftByDrawing(IDocumentEditor docEditor, string[] args)
        {
            var loftAction = new LoftAction(docEditor);
            loftAction.ExecCreate(args);
            return CommandResult.Succ();
        }
        #endregion

    }
}
