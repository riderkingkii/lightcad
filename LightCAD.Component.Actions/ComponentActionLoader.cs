﻿using LightCAD.Core;

namespace LightCAD.Component.Actions
{
    public static class ComponentActionLoader
    {
        public static void Initilize() 
        {
            LcDocument.ElementActions.Add(BuiltinElementType.Cube, new CubeAction());
            LcDocument.Element3dActions.Add(BuiltinElementType.Cube, new Cube3dAction());
            LcDocument.ElementActions.Add(BuiltinElementType.Extrude, new ExtrudeAction());
            LcDocument.Element3dActions.Add(BuiltinElementType.Extrude, new Extrude3dAction());
            LcDocument.ElementActions.Add(BuiltinElementType.Cuboid, new CuboidAction());
            LcDocument.Element3dActions.Add(BuiltinElementType.Cuboid, new Cuboid3dAction());
            LcDocument.ElementActions.Add(BuiltinElementType.Loft, new LoftAction());
            LcDocument.Element3dActions.Add(BuiltinElementType.Loft, new Loft3dAction());
        }
    }
}