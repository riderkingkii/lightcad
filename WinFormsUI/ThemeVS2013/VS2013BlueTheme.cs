namespace WeifenLuo.WinFormsUI.Docking
{
    using ThemeVS2013;
    using WeifenLuo.WinFormsUI.ThemeVS2015;

    /// <summary>
    /// Visual Studio 2013 Light theme.
    /// </summary>
    public class VS2013BlueTheme : VS2013ThemeBase
    {
        public VS2013BlueTheme()
            : base(Decompress(ThemeVS2013.Resources.vs2013blue_vstheme))
        {
        }
    }
}
