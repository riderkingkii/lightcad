
using CefSharp;
using CefSharp.WinForms;
using System.Reflection;
using System.Runtime;

namespace LightCAD.Help
{
    public partial class HelpForm : Form
    {

        private ChromiumWebBrowser browser;
        public HelpForm()
        {
            InitializeComponent();
            InitBrowser();
        }

        private void InitBrowser()
        {
            if (!Cef.IsInitialized)
            {
                CefSettings settings = new CefSettings();
                //settings.CefCommandLineArgs.Add("enable-media-stream", "enable-media-stream");

                settings.CefCommandLineArgs.Add("--disable-web-security", "");
                settings.CefCommandLineArgs.Add("--user-data-dir", "C:\\MyChromeDevUserData");

                settings.IgnoreCertificateErrors = true;
                settings.LogSeverity = CefSharp.LogSeverity.Verbose;
                Cef.Initialize(settings);
            }

            string basePath = Directory.GetParent(Environment.ProcessPath).Parent.Parent.Parent.Parent.FullName;

            string url = Path.Combine(basePath, @"LightCAD.Help\Resources\Drawing.html");
            browser = new ChromiumWebBrowser(url);
            browser.Dock = DockStyle.Fill;
            //browser.MenuHandler = new 

            this.Controls.Add(browser);
        }
    }
}